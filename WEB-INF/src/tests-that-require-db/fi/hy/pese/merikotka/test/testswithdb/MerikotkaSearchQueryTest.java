package fi.hy.pese.merikotka.test.testswithdb;

import java.sql.SQLException;

import fi.hy.pese.framework.main.db.RowsToDataResultHandler;
import fi.hy.pese.framework.main.db._DAO;
import fi.hy.pese.framework.main.db.oraclesql.OracleSQL_DAO;
import fi.hy.pese.framework.main.db.oraclesql.Queries;
import fi.hy.pese.framework.main.general.data.Data;
import fi.hy.pese.framework.main.general.data.PesaDomainModelDatabaseStructure;
import fi.hy.pese.framework.main.general.data.PesaDomainModelRowFactory;
import fi.hy.pese.framework.main.general.data._Data;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.main.general.data._RowFactory;
import fi.hy.pese.framework.test.TestConfig;
import fi.hy.pese.merikotka.main.app.MerikotkaConst;
import fi.hy.pese.merikotka.main.app.MerikotkaDatabaseStructureCreator;
import fi.luomus.commons.config.Config;
import fi.luomus.commons.config.ConfigReader;
import fi.luomus.commons.db.connectivity.PreparedStatementStoringAndClosingTransactionConnection;
import fi.luomus.commons.db.connectivity.TransactionConnection;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class MerikotkaSearchQueryTest {

	public static Test suite() {
		return new TestSuite(MerikotkaSearchQueryTest.class.getDeclaredClasses());
	}

	public static class TablesSearchQueriesTesting extends TestCase {

		private TransactionConnection							con;
		private _Data<_PesaDomainModelRow>								data;
		private _DAO<_PesaDomainModelRow>								dao;

		@Override
		protected void setUp() throws Exception {
			Config config = new ConfigReader(TestConfig.getConfigFile("pese_merikotka.config"));
			con = new PreparedStatementStoringAndClosingTransactionConnection(config.connectionDescription());
			con.startTransaction();
			PesaDomainModelDatabaseStructure structure = MerikotkaDatabaseStructureCreator.loadFromDatabase(con);
			_RowFactory<_PesaDomainModelRow> rowFactory = new PesaDomainModelRowFactory(structure, "", "");
			data = new Data<>(rowFactory);
			dao = new OracleSQL_DAO<>(con, rowFactory);
		}

		@Override
		protected void tearDown() {
			try {
				con.rollbackTransaction();
			} catch (SQLException e) {
				fail("Couldn't rollback");
			}
			con.release();
		}

		// This test will fail if pesa id 1 is removed from the test database...
		public void test_search_by_pesaid() throws Exception {
			data.searchparameters().getPesa().get("pesa_id").setValue("1");
			Queries.executeSearch(con, data.searchparameters(), new RowsToDataResultHandler(data, dao), MerikotkaConst.JOINS, MerikotkaConst.ORDER_BY);
			assertTrue(data.results().size() > 1);
		}

	}

}
