package fi.hy.pese.framework.main.view;


/**
 * A viewer which the control uses to view a possible exception that was thrown during the processing of functionality
 */
public interface _ExceptionViewer {

	/**
	 * Views the exception to the user in some 'friendly' or desired manner
	 * @param condition
	 * @param data.out
	 */
	void view(Throwable condition);

}
