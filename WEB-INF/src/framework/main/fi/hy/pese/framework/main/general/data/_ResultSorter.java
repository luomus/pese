package fi.hy.pese.framework.main.general.data;

import java.util.List;

public interface _ResultSorter<K extends _Row> {

	List<K> sort(List<K> results);

}
