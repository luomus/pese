package fi.hy.pese.framework.main.app;

import fi.hy.pese.framework.main.app.functionality.validate._Validator;
import fi.hy.pese.framework.main.general.data._Row;

/**
 * Functionality to be executed for a certain page by the Functionalityrequest, which contains all the application and request defined resources,
 * including data, session, dao connection, configuration, etc.
 * @see Request
 */
public interface _Functionality<T extends _Row> {

	/**
	 * The validator called during the functionality execution
	 * @see _Validator
	 * @param request
	 * @return
	 * @throws Exception
	 */
	_Validator validator() throws Exception;

	/**
	 * Executed before validation
	 * @param request
	 * @throws Exception
	 */
	void preProsessingHook() throws Exception;

	/**
	 * Executed if validation did not find any errors
	 * @param request
	 * @throws Exception
	 */
	void afterSuccessfulValidation() throws Exception;

	/**
	 * Executed if validation found errors
	 * @param request
	 * @throws Exception
	 */
	void afterFailedValidation() throws Exception;

	/**
	 * Executed last, whether the validation succeeded or failed
	 * @param request
	 * @throws Exception
	 */
	void postProsessingHook() throws Exception;

	/**
	 * The request being used by this functionality
	 * @return
	 */
	_Request<T> functionalityRequest();

}
