package fi.hy.pese.framework.main.general.data;

import java.io.Serializable;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Vector;

import fi.hy.pese.framework.main.general.consts.Const;

public class DatabaseTableStructure implements Serializable {

	private static final long	serialVersionUID	= -3264631636299576493L;
	private final String									name;
	private LinkedHashMap<String, DatabaseColumnStructure>	columnStructureMap;
	private final Vector<String>							immutableKeys;
	private String											idColumn;
	private int												permissions;

	public DatabaseTableStructure(String name) throws IllegalArgumentException {
		ifNullThrowException(name, "Invalid name for the table");
		this.name = name;
		this.idColumn = "";
		this.permissions = _Table.TABLEMANAGEMENT_NO_PERMISSIONS;
		this.columnStructureMap = new LinkedHashMap<>();
		this.immutableKeys = new Vector<>(10);
		addColumn(Const.ROWID, _Column.ROWID);
	}

	public String getName() {
		return name;
	}

	public DatabaseColumnStructure getUniqueNumericIdColumn() throws IllegalArgumentException {
		try {
			return get(idColumn);
		} catch (IllegalArgumentException e) {
			throw new IllegalArgumentException("This table doesn't have id column");
		}
	}

	public boolean hasUniqueNumericIdColumn() {
		return idColumn.length() > 0;
	}

	public void setIdColumn(String name) throws IllegalStateException {
		if (get(name).getFieldType() != _Column.UNIQUE_NUMERIC_INCREASING_ID) throw new IllegalStateException("Id column should be defined using column's setTypeToUniqueNumericIncreasinKey() -method");
		this.idColumn = name;
	}

	public void addToImmutableKeys(String name) {
		if (get(name).getFieldType() != _Column.IMMUTABLE_PART_OF_A_KEY) throw new IllegalStateException("Key column should be defined using column's setTypeToImmutablePartOfAKey() -method");
		immutableKeys.add(name);
	}

	public boolean hasImmutableKeys() {
		return immutableKeys.size() > 0;
	}

	public Vector<String> getImmutableKeys() {
		return immutableKeys;
	}

	public boolean containsColumn(String name) {
		return columnStructureMap.containsKey(name);
	}

	public DatabaseColumnStructure get(String name) throws IllegalArgumentException {
		DatabaseColumnStructure structure = columnStructureMap.get(name);
		if (structure == null) throw new IllegalArgumentException("No such column: " + getName() + " : " + name);
		return structure;
	}

	public Collection<DatabaseColumnStructure> getColumns() {
		return columnStructureMap.values();
	}

	public int numberOfColumns() {
		return columnStructureMap.size();
	}

	public DatabaseColumnStructure addColumn(String name, int datatype, int... size) throws IllegalArgumentException {
		ifNullThrowException(name, "Invalid name for the column");
		if (columnStructureMap.containsKey(name)) throw new IllegalArgumentException("Column already exists");
		DatabaseColumnStructure column = new DatabaseColumnStructure(this, name, datatype, size);
		columnStructureMap.put(name, column);
		return column;
	}

	public void removeColumn(String name) throws IllegalArgumentException {
		DatabaseColumnStructure c = get(name);
		columnStructureMap.remove(c.getName());
	}

	private void ifNullThrowException(String name, String errortext) throws IllegalArgumentException {
		if (name == null || name.length() < 1) throw new IllegalArgumentException(errortext);
	}

	public int getTablemanagementPermissions() {
		return permissions;
	}

	public void setTablemanagementPermissions(int permissions) {
		this.permissions = permissions;
	}

	public void moveAsFirst(DatabaseColumnStructure columnToBeMoved) {
		LinkedHashMap<String, DatabaseColumnStructure> newMap = new LinkedHashMap<>();
		newMap.put(Const.ROWID, columnStructureMap.get(Const.ROWID));
		newMap.put(columnToBeMoved.getName(), columnToBeMoved);
		for (DatabaseColumnStructure c : columnStructureMap.values()) {
			if (c.getName().equals(Const.ROWID)) continue;
			if (c == columnToBeMoved) continue;
			newMap.put(c.getName(), c);
		}
		columnStructureMap = newMap;
	}

	public void reorder(List<String> newOrderOfColumns) {
		LinkedHashMap<String, DatabaseColumnStructure> newMap = new LinkedHashMap<>();
		newMap.put(Const.ROWID, columnStructureMap.get(Const.ROWID));
		for (String name : newOrderOfColumns) {
			newMap.put(name, this.get(name));
		}
		for (DatabaseColumnStructure c : columnStructureMap.values()) {
			if (newMap.containsKey(c.getName())) continue;
			newMap.put(c.getName(), c);
		}
		columnStructureMap = newMap;
	}

	public boolean isInitialized() {
		return !name.equals(_Table.UNINITIALIZED_TABLE);
	}

	public void customRowid(String name) {
		DatabaseColumnStructure column = new DatabaseColumnStructure(this, name, _Column.ROWID);
		columnStructureMap.put(Const.ROWID, column);
	}
}
