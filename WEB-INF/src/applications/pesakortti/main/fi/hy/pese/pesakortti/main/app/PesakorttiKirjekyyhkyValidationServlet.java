package fi.hy.pese.pesakortti.main.app;

import fi.hy.pese.framework.main.app.KirjekyyhkyValidatorServlet;
import fi.hy.pese.framework.main.app._ApplicationSpecificationFactory;
import fi.hy.pese.framework.main.app._Functionality;
import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data.PesaDomainModelDatabaseStructure;
import fi.hy.pese.framework.main.general.data.PesaDomainModelRowFactory;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.main.view.RedirectTo500ExceptionViewer;
import fi.hy.pese.framework.main.view._ExceptionViewer;
import fi.hy.pese.pesakortti.main.functionality.PesakorttiKirjekyyhkyValidationServiceFunctionality;
import fi.luomus.commons.db.connectivity.PreparedStatementStoringAndClosingTransactionConnection;
import fi.luomus.commons.db.connectivity.TransactionConnection;

public class PesakorttiKirjekyyhkyValidationServlet extends KirjekyyhkyValidatorServlet<_PesaDomainModelRow> {

	private static final long	serialVersionUID	= -3340867457607396429L;

	@Override
	protected _ApplicationSpecificationFactory<_PesaDomainModelRow> createFactory() throws Exception {
		PesakorttiKirjekyyhkyValidationServiceFactory factory = new PesakorttiKirjekyyhkyValidationServiceFactory("pese_pesakortti.config");

		TransactionConnection con = new PreparedStatementStoringAndClosingTransactionConnection(factory.config().connectionDescription());
		try {
			PesaDomainModelDatabaseStructure structure = PesakorttiDatabaseStructureCreator.loadFromDatabase(con);
			factory.setRowFactory(new PesaDomainModelRowFactory(structure, PesakorttiConst.JOINS, PesakorttiConst.ORDER_BY));
		} finally {
			con.release();
		}

		factory.loadUITexts();

		return factory;
	}

	private class PesakorttiKirjekyyhkyValidationServiceFactory extends PesakorttiFactory {

		public PesakorttiKirjekyyhkyValidationServiceFactory(String configFileName) throws Exception {
			super(configFileName);
		}

		@Override
		public String frontpage() {
			return Const.LOGIN_PAGE;
		}

		@Override
		public _Functionality<_PesaDomainModelRow> functionalityFor(String page, _Request<_PesaDomainModelRow> request) {
			return new PesakorttiKirjekyyhkyValidationServiceFunctionality(request);
		}

		@Override
		public _ExceptionViewer exceptionViewer(_Request<_PesaDomainModelRow> request) {
			return new RedirectTo500ExceptionViewer(request.redirecter());
		}

		@Override
		public void postProsessingHook(_Request<_PesaDomainModelRow> request) {
			// lets not load Kirjekyyhky count
		}
	}
}
