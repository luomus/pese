package fi.hy.pese.saaksi.main.functionality;

import java.util.Map;

import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.app.functionality.KirjekyyhkyManagementFunctionality;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.main.general.data._Table;
import fi.hy.pese.saaksi.main.app.SaaksiConst;

public class SaaksiKirjekyyhkyManagementFunctionality extends KirjekyyhkyManagementFunctionality {

	public SaaksiKirjekyyhkyManagementFunctionality(_Request<_PesaDomainModelRow> request) {
		super(request);
	}

	@Override
	protected void applicationSpecificConversions(Map<String, String> formData, _PesaDomainModelRow kirjekyyhkyparameters) {
		SaaksiConst.kirjekyyhkyModelToData(formData, kirjekyyhkyparameters);
		data.set("tark_vuosi", kirjekyyhkyparameters.getTarkastus().get("vuosi").getValue());
	}

	@Override
	protected void applicationSpecificReturnedEmptyProcedure(_PesaDomainModelRow kirjekyyhkyParameters) {
		_Table tarkastus = kirjekyyhkyParameters.getTarkastus();
		tarkastus.get("tark_pvm").setValue("1.1." + tarkastus.get("vuosi").getValue());
		tarkastus.get("tark_pvm_tark").setValue("4");
		tarkastus.get("vaihtop_laiminl").setValue("J");
	}

	@Override
	protected void applicationSpecificComparisonDataOperations(_PesaDomainModelRow kirjekyyhkyParameters, _PesaDomainModelRow comparisonParameters) {}

}
