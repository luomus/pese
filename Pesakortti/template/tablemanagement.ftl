
<#include "header.ftl">

<h1>${texts[data.table+"_management_title"]}</h1>

<#if (data.insertmodepage!"") == "yes">
	<p align="right">
		<a href="${system.baseURL}?page=${system.page}&amp;action=show_search&amp;table=${data.table}">
        	&raquo; Haku
    	</a>
    </p>
</#if>

<@errorlist errors texts />
<@statustexts system texts />

<#if system.action != "insert" && system.action != "insert_page">
<#------------------------------------ SEARCH UPDATE DELETE ---------------------------------------------->

<#----------------- link to add new entry page ------------------------------>

  <p align="right"> 
  <#if data.tablemanagement_permissions == TABLEMANAGEMENT_UPDATE_INSERT || data.tablemanagement_permissions == TABLEMANAGEMENT_UPDATE_INSERT_DELETE || data.tablemanagement_permissions == TABLEMANAGEMENT_INSERT_ONLY>
      <a href="${system.baseURL}?page=${system.page}&amp;action=insert_page&amp;table=${data.table}">
        &raquo; ${texts[data.table+"_management_add_link"]}
      </a>
  </#if>
  </p> 

<#----------------- --------------------------- ------------------------------>

<h2>${texts.management_search}</h2>

<#if system.action == "search">
  <@errorlist errors texts /> 
</#if>

<br />

<form action="${system.baseURL}" method="post" name="search">
<input type="hidden" name="page"   value="${system.page}" />
<input type="hidden" name="action" value="search" />
<input type="hidden" name="table"  value="${data.table}" />

<table>
  <thead>
  <tr>
    <#----------------- column descriptions ------------------------------>
    <#list searchparameters.getTableByName(data.table).columns as column>
      <#if column.name != "rowid" && column.fieldType != PASSWORD && column.fullname != "muutosloki.id">
        <th>${texts[ column.fullname ]!column.fullname}</th>
      </#if>
	</#list>
	<#------------------------------------------------------------------->
  </tr>
  </thead>
  <tbody>
  <tr>
    <#-------------------- search form columns --------------------------------->
    <#list searchparameters.getTableByName(data.table).columns as column> 
      
      <#if column.name != "rowid" && column.fieldType != PASSWORD && column.fullname != "muutosloki.id">
      
      <td>
        <#if column.inputType == "TEXT">
          <@inputText "searchparameters" column true errors />  
        </#if>
        <#if column.inputType == "TEXTAREA">
          <@inputText "searchparameters" column false errors />  
        </#if>
        <#if column.inputType == "SELECT"> <@selectionChosen selections "searchparameters" column 1 false true true errors /> </#if>
        <#if column.inputType == "DATE">
          <@date "searchparameters" column errors /> 
        </#if>
      </td>
      
      </#if>
	</#list>
	<#-------------------------------------------------------------------->
  </tr>
  </tbody>
</table>

<input type="submit" value="${texts.search}" />  
<input type="submit" value="${texts.clear}"  onclick="document.clear.submit(); return false; " /> 

</form>

<#----------------- clear button ------------------------------>
  <form action="${system.baseURL}" method="get" name="clear">
  <input type="hidden" name="page"  value="${system.page}" />
  <input type="hidden" name="table" value="${data.table}" />
  </form>
<#----------------- ------------ ------------------------------>



<br />
<br />
<#if system.action == "search">${results?size} ${texts.management_results_count}</#if>
<br />




<#----------------------------------- results -------------------------------------------->
<#if (results?size > 0) >  
  
  <table > 
    <thead>
    <tr>
    <#----------------- column descriptions ------------------------------>
      <#list updateparameters.getTableByName(data.table).columns as column>
        <#if column.name != "rowid"  && column.fullname != "muutosloki.id">
          <th <#if column.datatype == INTEGER || column.datatype == DECIMAL>class="{sorter: 'digit'}"</#if>  >${texts[ column.fullname ]!column.fullname}</th>
        </#if>
	  </#list>
    <#------------------------------------------------------------------->
    </tr>
    </thead>
    
    <tbody>
    <#list results as result>
      <tr>
      <form action="${system.baseURL}" method="post">
      <input type="hidden" name="page"   value="${system.page}" />
      <input type="hidden" name="table"  value="${data.table}" />
      
      <#---------------------- attach searchparameters to request ---------------------> 
        <#list searchparameters.getTableByName(data.table).columns as column>
           <@inputHidden "searchparameters" column />
	    </#list>
	  <#---------------------- --------------------------------------- ---------------->
	           
      <#list result.getTableByName(data.table).columns as column>
        <#if column.name == "rowid" || column.fullname == "muutosloki.id"> <@inputHidden "updateparameters" column /> 
        <#else>
            <td>
              <#if column.modifiability == "NO" || column.tablename="muutosloki">
              		<#if column.hasAssosiatedSelection()>
              			${ column.value } - ${ selections[column.assosiatedSelection][ column.value ] } 
              		<#else>
              			${column.value}
              		</#if>  
              		<@inputHidden "updateparameters" column />
              <#else>
                <#if column.inputType == "TEXT">     <@inputText "updateparameters" column true errors /> </#if>
                <#if column.inputType == "TEXTAREA"> <@textarea "updateparameters" column 2 40 errors />   </#if>
                <#if column.inputType == "SELECT">   <@selectionChosen selections "updateparameters" column 1 false true true errors /> </#if>
                <#if column.inputType == "DATE">     <@date "updateparameters" column errors /> </#if> 
                <#if column.inputType == "PASSWORD"> <@inputPassword "updateparameters" column errors /> </#if>
              </#if>
            </td>
        </#if>
	  </#list>
	   
	   <#if data.tablemanagement_permissions !=  TABLEMANAGEMENT_INSERT_ONLY>	
	     <td><input type="submit" name="update" value="${texts.update}" /> </td>
	   </#if>
	   <#if data.tablemanagement_permissions == TABLEMANAGEMENT_UPDATE_INSERT_DELETE> <td><input type="submit" name="delete" value="${texts.delete}" /> </td> </#if>
       
      </form>
      </tr>
      
    </#list>
    </tbody>
  </table>
</#if>

<#------------------------------------ ---------------- -------------------------------------------------->




<#else>

<#------------------------------------      INSERT     --------------------------------------------------->


<h2>${texts[data.table+"_management_add_link"]}</h2>


<form action="${system.baseURL}" method="post">
<input type="hidden" name="page"   value="${system.page}" />
<input type="hidden" name="action" value="insert" />
<input type="hidden" name="table"  value="${data.table}" />

<table>
  <tr>
    <#----------------- column descriptions ------------------------------>
    <#list updateparameters.getTableByName(data.table).columns as column>
      <#if column.fieldType == NORMAL_FIELD || column.fieldType == IMMUTABLE_KEY || column.fieldType == PASSWORD>
        <th>${texts[ column.fullname ]!column.fullname}</th>
      </#if>
	</#list>
	<#------------------------------------------------------------------->
  </tr> 
  <tr>
  <#list updateparameters.getTableByName(data.table).columns as column>
    <#if column.fieldType == NORMAL_FIELD || column.fieldType == IMMUTABLE_KEY || column.fieldType == PASSWORD>
      <td>
        <#if column.inputType == "TEXT">     <@inputText "updateparameters" column true errors /> </#if>
        <#if column.inputType == "TEXTAREA"> <@textarea "updateparameters" column 2 40 errors />   </#if>
        <#if column.inputType == "SELECT">   <@selectionChosen selections "updateparameters" column 1 false true true errors /> </#if>
        <#if column.inputType == "DATE">     <@date "updateparameters" column errors /> </#if> 
        <#if column.inputType == "PASSWORD"> <@inputPassword "updateparameters" column errors /> </#if>
      </td>
    </#if>
  </#list>
  </tr>
</table>
  
  <br />
  
  <input type="submit" value="${texts.insert}" />
</form>

<br /><br />

<form action="${system.baseURL}" method="post">
<input type="hidden" name="page"   value="${system.page}" />
<input type="hidden" name="table"  value="${data.table}" />
<input type="submit" value="${texts.cancel}" />
</form>

<#------------------------------------ ---------------- -------------------------------------------------->
</#if>


<#include "footer.ftl">
