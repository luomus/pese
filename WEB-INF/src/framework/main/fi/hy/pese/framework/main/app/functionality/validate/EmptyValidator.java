package fi.hy.pese.framework.main.app.functionality.validate;

import java.util.Map;

public class EmptyValidator implements _Validator {

	@Override
	public Map<String, String> errors() {
		return null;
	}

	@Override
	public boolean hasErrors() {
		return false;
	}

	@Override
	public void validate() throws Exception {}

	@Override
	public boolean hasWarnings() {
		return false;
	}

	@Override
	public Map<String, String> warnings() {
		return null;
	}

	@Override
	public boolean noErrors() {
		return true;
	}

	@Override
	public boolean noWarnings() {
		return true;
	}

}
