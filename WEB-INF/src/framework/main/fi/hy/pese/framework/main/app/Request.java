package fi.hy.pese.framework.main.app;

import java.io.PrintWriter;
import java.util.List;

import fi.hy.pese.framework.main.db._DAO;
import fi.hy.pese.framework.main.general.data.HistoryAction;
import fi.hy.pese.framework.main.general.data._Data;
import fi.hy.pese.framework.main.general.data._Row;
import fi.hy.pese.framework.main.kirjekyyhky._KirjekyyhkyXMLGenerator;
import fi.luomus.commons.config.Config;
import fi.luomus.commons.http.HttpStatus;
import fi.luomus.commons.kirjekyyhky.KirjekyyhkyAPIClient;
import fi.luomus.commons.pdf.PDFWriter;
import fi.luomus.commons.reporting.ErrorReporter;
import fi.luomus.commons.session.SessionHandler;
import fi.luomus.commons.tipuapi.TipuApiResource;

public class Request<T extends _Row> implements _Request<T> {
	
	private final RequestImpleData<T> req;
	
	/**
	 * A new request containing object
	 * @param data
	 */
	public Request(RequestImpleData<T> data) {
		this.req = data;
	}
	
	/*
	 * (non-Javadoc)
	 * @see fi.hy.pese.framework.main.app._Request#data()
	 */
	@Override
	public _Data<T> data() {
		return req.getData();
	}
	
	/*
	 * (non-Javadoc)
	 * @see fi.hy.pese.framework.main.app._Request#session()
	 */
	@Override
	public SessionHandler session() {
		return req.getSessionHandler();
	}
	
	/*
	 * (non-Javadoc)
	 * @see fi.hy.pese.framework.main.app._Request#dao()
	 */
	@Override
	public _DAO<T> dao() {
		return req.getDao();
	}
	
	/*
	 * (non-Javadoc)
	 * @see fi.hy.pese.framework.main.app._Request#frontpage()
	 */
	@Override
	public String frontpage() {
		return req.getApplication().frontpage();
	}
	
	/*
	 * (non-Javadoc)
	 * @see fi.hy.pese.framework.main.app._Request#functionalityFor(java.lang.String)
	 */
	@Override
	public _Functionality<T> functionalityFor(String page) {
		return req.getApplication().functionalityFor(page, this);
	}
	
	/*
	 * (non-Javadoc)
	 * @see fi.hy.pese.framework.main.app._Request#redirecter()
	 */
	@Override
	public HttpStatus redirecter() {
		if (req.getRedirecter() == null) return new HttpStatus(null);
		return req.getRedirecter();
	}
	
	/*
	 * (non-Javadoc)
	 * @see fi.hy.pese.framework.main.app._Request#config()
	 */
	@Override
	public Config config() {
		if (req.getApplication() == null) return null;
		return req.getApplication().config();
	}
	
	/*
	 * (non-Javadoc)
	 * @see fi.hy.pese.framework.main.app._Request#pdfWriter()
	 */
	@Override
	public PDFWriter pdfWriter() {
		return req.getApplication().pdfWriter();
	}
	
	@Override
	public PDFWriter pdfWriter(String outputFolder) {
		return req.getApplication().pdfWriter(outputFolder);
	}
	
	@Override
	public _KirjekyyhkyXMLGenerator kirjekyyhkyFormDataXMLGenerator() throws Exception {
		return req.getApplication().kirjekyyhkyFormDataXMLGenerator(this);
	}
	
	@Override
	public KirjekyyhkyAPIClient kirjekyyhkyAPI() throws Exception {
		return req.getApplication().kirjekyyhkyAPI();
	}
	
	@Override
	public PrintWriter out() {
		return req.getOut();
	}
	
	@Override
	public void addHistoryAction(HistoryAction action) {
		req.getApplication().addHistoryAction(action);
	}
	
	@Override
	public List<HistoryAction> getHistoryActions() {
		return req.getApplication().getHistoryActions();
	}
	
	@Override
	public TipuApiResource getTipuApiResource(String resourcename) {
		return req.getApplication().getTipuApiResource(resourcename);
	}
	
	@Override
	public String getOriginalRequestURI() {
		return req.getOriginalRequestURI();
	}
	
	@Override
	public ErrorReporter getErrorReporter() {
		return req.getErrorReporter();
	}
	
}
