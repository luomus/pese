package fi.hy.pese.ulko.main.app;

import fi.hy.pese.framework.main.general.data._Row;
import fi.hy.pese.framework.main.general.data._Table;

public interface _UlkoDomainModelRow extends _Row {

	_Table getPyynto();
	_Table getLoyto();
	_Table getRengastus();

}
