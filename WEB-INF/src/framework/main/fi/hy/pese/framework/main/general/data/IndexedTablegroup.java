package fi.hy.pese.framework.main.general.data;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

public class IndexedTablegroup implements _IndexedTablegroup, Serializable {

	private static final long	serialVersionUID	= 5235990675304035569L;

	private final SortedMap<Integer, _Table> map;
	private final DatabaseTableStructure tableStructure;
	private final int staticCount;
	private final _Table basetable;
	private int indexOfNextFreeSlot = 1;

	public IndexedTablegroup(DatabaseTableStructure tableStructure) {
		this(tableStructure, 0);
	}

	public IndexedTablegroup(DatabaseTableStructure tableStructure, int staticCount) {
		this.tableStructure = tableStructure;
		this.map = new TreeMap<>();
		this.staticCount = staticCount;
		this.basetable = new Table(tableStructure);
	}

	@Override
	public Iterator<_Table> iterator() {
		return map.values().iterator();
	}

	@Override
	public _Table get(int i) {
		if (i >= indexOfNextFreeSlot) {
			indexOfNextFreeSlot = i + 1;
		}
		if (!map.containsKey(i)) {
			map.put(i, new Table(tableStructure, i));
		}
		return map.get(i);
	}

	@Override
	public Collection<_Table> values() {
		return map.values();
	}

	@Override
	public void clearAllValues() {
		for (_Table t : this) {
			t.clearAllValues();
		}
	}

	@Override
	public int getStaticCount() {
		return staticCount;
	}

	@Override
	public String getBasetablename() {
		return tableStructure.getName();
	}

	@Override
	public _Table getBasetable() {
		return basetable;
	}

	@Override
	public boolean hasValues() {
		for (_Table t : this) {
			if (t.hasValues()) return true;
		}
		return false;
	}

	@Override
	public _Table getNext() {
		return get(indexOfNextFreeSlot);
	}

	@Override
	public void sortUsing(Comparator<_Table> comparator) {
		List<_Table> allTables = new LinkedList<>(map.values());
		Collections.sort(allTables, comparator);
		map.clear();
		int i = 1;
		for (_Table t : allTables) {
			map.put(i++, t);
		}
	}
}
