package fi.hy.pese.framework.main.app;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import fi.hy.pese.framework.main.app.functionality.ApiServiceFunctionality;
import fi.hy.pese.framework.main.db._DAO;
import fi.hy.pese.framework.main.general.SelectionsLoader;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data._Row;
import fi.hy.pese.framework.main.view.RedirectTo500ExceptionViewer;
import fi.hy.pese.framework.main.view._ExceptionViewer;
import fi.hy.pese.framework.main.view._Viewer;
import fi.luomus.commons.containers.Selection;
import fi.luomus.commons.utils.SingleObjectCache;

public class ApiServiceFactory<T extends _Row> extends BaseFactoryForApplicationSpecificationFactory<T> {
	
	public ApiServiceFactory(String configFileName) throws Exception {
		super(configFileName);
	}
	
	// Can be overriden to use another implementation than FreeMarker
	@Override
	public _Viewer viewer() throws IOException {
		return null;
	}
	
	@Override
	public String frontpage() {
		return Const.LOGIN_PAGE;
	}
	
	@Override
	public _Functionality<T> functionalityFor(String page, _Request<T> request) {
		return new ApiServiceFunctionality<>(request);
	}
	
	@Override
	public _ExceptionViewer exceptionViewer(_Request<T> request) {
		return new RedirectTo500ExceptionViewer(request.redirecter());
	}
	
	@Override
	public void postProsessingHook(_Request<T> request) {
		// lets not load Kirjekyyhky count
	}
	
	@Override
	protected Map<String, Selection> fetchSelections(_DAO<T> dao) throws Exception {
		throw new UnsupportedOperationException();
	}
	
	private final SelectionsLoader<T> selectionsLoader = new SelectionsLoader<>(this);
	
	private final SingleObjectCache<Map<String, Selection>> cachedSelections = new SingleObjectCache<>(selectionsLoader, 15, TimeUnit.MINUTES);
	
	@Override
	public synchronized Map<String, Selection> selections(_DAO<T> dao) throws Exception {
		return cachedSelections.get();
	}
	
}
