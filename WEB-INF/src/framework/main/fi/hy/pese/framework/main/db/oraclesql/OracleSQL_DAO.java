package fi.hy.pese.framework.main.db.oraclesql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import fi.hy.pese.framework.main.db.FirstResultRowToTableHandler;
import fi.hy.pese.framework.main.db.Results;
import fi.hy.pese.framework.main.db.ResultsToListHandler;
import fi.hy.pese.framework.main.db._DAO;
import fi.hy.pese.framework.main.db._ResultsHandler;
import fi.hy.pese.framework.main.general._Logger;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._Data;
import fi.hy.pese.framework.main.general.data._IndexedTablegroup;
import fi.hy.pese.framework.main.general.data._ReferenceKey;
import fi.hy.pese.framework.main.general.data._Row;
import fi.hy.pese.framework.main.general.data._RowFactory;
import fi.hy.pese.framework.main.general.data._Table;
import fi.luomus.commons.containers.Selection;
import fi.luomus.commons.db.connectivity.TransactionConnection;
import fi.luomus.commons.utils.Utils;

/**
 * Implements DAO.
 * This implementation is for Oracle 10.
 * This implementation writes all database alterations to text file when the transaction is committed.
 * @see _DAO
 */
public class OracleSQL_DAO<T extends _Row> implements _DAO<T> {
	
	private final TransactionConnection		con;
	private final _Logger					logger;
	private final String 					joins;
	private final String 					orderby;
	private final _RowFactory<T> 		rowFactory;
	
	/**
	 * @param con
	 * @param structure
	 */
	public OracleSQL_DAO(TransactionConnection con, _RowFactory<T> rowFactory) {
		this(con, rowFactory, null);
	}
	
	public OracleSQL_DAO(TransactionConnection con, _RowFactory<T> rowFactory, _Logger logger) {
		this.con = con;
		this.rowFactory = rowFactory;
		this.logger = logger;
		if (rowFactory != null) {
			this.joins = rowFactory.joins();
			this.orderby = rowFactory.orderby();
		} else {
			this.joins = "JOINS NOT DEFINED";
			this.orderby = "ORDERBY NOT DEFINED";
		}
	}
	
	@Override
	public int executeUpdate(String sql, String... values) throws SQLException {
		PreparedStatement p = null;
		try {
			p = con.prepareStatement(sql);
			int i = 1;
			for (String value : values) {
				p.setString(i++, value);
			}
			int result = p.executeUpdate();
			
			return result;
		} finally {
			if (p != null) p.close();
		}
	}
	
	@Override
	public void executeSearch(String sql, _ResultsHandler resultHandler, String... values) throws SQLException {
		PreparedStatement p = null;
		ResultSet rs = null;
		try {
			p = con.prepareStatement(sql);
			int i = 1;
			for (String value : values) {
				p.setString(i++, value);
			}
			rs = p.executeQuery();
			resultHandler.process(new Results(rs));
		} finally {
			if (rs != null) rs.close();
			if (p != null) p.close();
		}
	}
	
	@Override
	public _Table newTableByName(String name) throws IllegalArgumentException {
		return rowFactory.newRow().getTableByName(name);
	}
	
	@Override
	public T newRow() {
		return rowFactory.newRow();
	}
	
	@Override
	public boolean checkRowExists(_Table table) throws SQLException {
		return Queries.checkRowExists(table, con);
	}
	
	@Override
	public boolean checkRowIDMatchesKeys(_Table table) throws SQLException {
		return Queries.checkRowIdMatchesKeys(table, con);
	}
	
	@Override
	public boolean checkReferenceValueExists(String value, _ReferenceKey referenceKey) throws SQLException {
		return Queries.checkReferenceValueExists(value, referenceKey, con);
	}
	
	@Override
	public boolean checkValuesExists(_Table table) throws SQLException {
		return Queries.checkValuesExists(table, con);
	}
	
	public void close() {
		con.release();
	}
	
	@Override
	public void commit() throws SQLException {
		con.commitTransaction();
		if (logger != null) logger.commitMessages();
	}
	
	@Override
	public void executeDelete(_Table table) throws SQLException, NothingToDeleteException {
		if (logger != null) {
			if (table.getRowidValue().length() > 0) {
				_Table origData = returnTableByRowid(table.getName(), table.getRowidValue());
				logger.logDelete(origData);
			} else {
				logger.logDelete(table);
			}
		}
		Queries.executeDelete(table, con);
	}
	
	@Override
	public void executeDelete(_IndexedTablegroup tables) throws SQLException, NothingToDeleteException {
		for (_Table table : tables) {
			if (table.getRowidValue().length() > 0) {
				executeDelete(table);
			}
		}
	}
	
	@Override
	public void executeInsert(_Table table) throws SQLException {
		setURIIfDoesNotExist(table);
		Queries.executeInsert(table, con);
		if (logger != null) logger.logInsert(table);
	}
	
	private void setURIIfDoesNotExist(_Table table) {
		if (table.hasColumn("uri")) {
			_Column uri = table.get("uri");
			if (!uri.hasValue()) {
				uri.setValue("http://tun.fi/A." + Utils.generateGUID());
			}
		}
	}
	
	@Override
	public void executeSearch(_Table parameters, _ResultsHandler resultHandler) throws SQLException {
		Queries.executeSearch(parameters, resultHandler, con);
	}
	
	@Override
	public void executeSearch(_Table parameters, _ResultsHandler resultHandler, String... orderBy) throws SQLException {
		Queries.executeSearch(parameters, resultHandler, orderBy, con);
	}
	
	@Override
	public void executeSearch(T parameters, _ResultsHandler resultHandler, String... additional) throws SQLException {
		Queries.executeSearch(con, parameters, resultHandler, joins, orderby, additional);
	}
	
	@Override
	public void executeUpdate(_Table newValues) throws SQLException {
		setURIIfDoesNotExist(newValues);
		if (logger != null) {
			_Table oldValues = returnTableByRowid(newValues.getName(), newValues.getRowidValue());
			logger.logUpdate(oldValues, newValues);
		}
		Queries.executeUpdate(newValues, con);
	}
	
	@Override
	public Map<String, Selection> returnCodeSelections(String codeTable, String tableField, String fieldField, String valueField, String descField, String groupField, String[] orderBy) throws SQLException {
		return Queries.returnSelectionsFromCodeTable(con, codeTable, tableField, fieldField, valueField, descField, groupField, orderBy);
	}
	
	@Override
	public Selection returnSelections(String tablename, String valuefield, String descriptionfield,  String[] orderBy) throws SQLException {
		return Queries.returnSelection(con, tablename, valuefield, descriptionfield, orderBy);
	}
	
	@Override
	public long returnSelectionsUpdatedTime() throws SQLException {
		return Queries.returnSelectionsUpdatedTime(con);
	}
	
	@Override
	public void rollback() throws SQLException {
		con.rollbackTransaction();
	}
	
	@Override
	public void setSelectionsUpdateTime() throws SQLException {
		Queries.setSelectionsUpdatedTime(con);
	}
	
	@Override
	public void startTransaction() throws SQLException {
		con.startTransaction();
	}
	
	@Override
	public String returnValueByRowid(String table, String valueColumn, String rowid) throws SQLException {
		return Queries.returnValueByRowid(table, valueColumn, rowid, con);
	}
	
	@Override
	public void addSavedSearchesListingToData(_Data<T> data) throws SQLException {
		_Table savedSearch = rowFactory.newRow().getSavedSearches();
		if (!savedSearch.isInitialized()) return;
		ResultsToListHandler<T> resultHandler = new ResultsToListHandler<>(data, this, Const.SAVED_SEARCHES, savedSearch.getName());
		executeSearch(savedSearch, resultHandler, "last_used DESC, userid");
	}
	
	@Override
	public List<_Table> returnTable(String tablename) throws SQLException {
		List<_Table> results = new LinkedList<>();
		ResultsToListHandler<T> resultHandler = new ResultsToListHandler<>(this, results, rowFactory.newRow().getTableByName(tablename).getName());
		_Table emptyParams = this.newTableByName(tablename);
		this.executeSearch(emptyParams, resultHandler);
		return results;
	}
	
	@Override
	public _Table returnTableByKeys(String tablename, String... keys) throws SQLException {
		if (keys == null || keys.length < 1) {
			throw new SQLException("No keys given");
		}
		for (String key : keys) {
			if (key == null || key.length() < 1) {
				throw new SQLException("Value of the key/one of the keys is not given");
			}
		}
		
		_Table table = this.newTableByName(tablename);
		if (table.hasUniqueNumericIdColumn()) {
			table.getUniqueNumericIdColumn().setValue(keys[0]);
		} else {
			if (!table.hasImmutableKeys()) throw new SQLException("Table " + tablename + " doesn't define any keys.");
			int i = 0;
			for (_Column c : table.getKeys()) {
				c.setValue(keys[i++]);
			}
		}
		Queries.executeExactMatchSearch(table, new FirstResultRowToTableHandler(table, FirstResultRowToTableHandler.CHECK_ONLY_ONE_RESULT), con);
		return table;
	}
	
	@Override
	public _Table returnTableByRowid(String tablename, String rowid) throws SQLException {
		if (rowid.length() < 1) throw new SQLException("Rowid not given for " + tablename);
		_Table table = rowFactory.newRow().getTableByName(tablename);
		table.setRowidValue(rowid);
		executeSearch(table, new FirstResultRowToTableHandler(table, FirstResultRowToTableHandler.CHECK_ONLY_ONE_RESULT));
		return table;
	}
	
	@Override
	public String returnAndSetNextId(_Table table) throws SQLException {
		if (!table.hasUniqueNumericIdColumn()) throw new SQLException("Table " + table.getName() + " doesn't have unique numeric id.");
		return Queries.returnAndSetNextId(table, con);
	}
	
	@Override
	public PreparedStatement prepareStatement(String sql) throws SQLException {
		return con.prepareStatement(sql);
	}
	
	@Override
	public List<_Table> returnTables(_Table searchparams) throws SQLException {
		return returnTables(searchparams, (String[]) null);
	}
	
	@Override
	public List<_Table> returnTables(_Table searchparams, String... orderBy) throws SQLException {
		return returnTables(searchparams, Integer.MAX_VALUE, orderBy);
	}
	
	@Override
	public List<_Table> returnTables(_Table searchparams, int limit, String... orderBy) throws SQLException {
		List<_Table> results = new LinkedList<>();
		ResultsToListHandler<T> handler = new ResultsToListHandler<>(this, results, searchparams.getName()).setLimit(limit);
		executeSearch(searchparams, handler, orderBy);
		return results;
	}
	
	@Override
	public void markForWarehousing(int rowId) throws SQLException {
		_Table transactionEntry = rowFactory.newRow().getTransactionEntry();
		if (!transactionEntry.isInitialized()) return;
		returnAndSetNextId(transactionEntry);
		transactionEntry.get("row_id").setValue(rowId);
		transactionEntry.get("attempted").setValue(0);
		Queries.executeInsert(transactionEntry, con);
	}
	
	@Override
	public void markWarehousingAttempted(_Table transactionEntry) throws SQLException {
		_Column attempted = transactionEntry.get("attempted");
		attempted.setValue(1);
		Queries.executeUpdate(transactionEntry, con);
	}
	
	@Override
	public void markWarehousingSuccess(_Table transactionEntry) throws NothingToDeleteException, SQLException {
		PreparedStatement p = null;
		try {
			p = con.prepareStatement(" DELETE FROM transaction WHERE row_id = ? ");
			p.setInt(1, transactionEntry.get("row_id").getIntegerValue());
			p.executeUpdate();
		} finally {
			Utils.close(p);
		}
	}
	
	@Override
	public void markAllWarehousingUnattempted() throws SQLException {
		PreparedStatement p = null;
		try {
			p = con.prepareStatement(" UPDATE transaction SET attempted = 0 ");
			p.executeUpdate();
		} finally {
			Utils.close(p);
		}
	}
	
}
