package fi.hy.pese.peto.main.functionality;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.db._DAO;
import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.main.general.data._Table;
import fi.hy.pese.framework.main.kirjekyyhky.KirjekyyhkyXMLGeneratorUtility;
import fi.hy.pese.framework.main.kirjekyyhky.KirjekyyhkyXMLGeneratorUtility.StructureDefinition;
import fi.hy.pese.framework.main.kirjekyyhky._KirjekyyhkyXMLGenerator;
import fi.hy.pese.peto.main.app.PetoConst;
import fi.luomus.commons.config.Config;
import fi.luomus.commons.containers.DateValue;
import fi.luomus.commons.containers.Selection;
import fi.luomus.commons.containers.SelectionImple;
import fi.luomus.commons.tipuapi.TipuAPIClient;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.FileUtils;

public class PetoKirjekyyhkyXMLGenerator implements _KirjekyyhkyXMLGenerator {

	private static final String[]	KAYNTI_COLUMNS_TO_EXLUDE	= { "id", "tarkastus" };
	private final _Request<_PesaDomainModelRow> request;
	private final Config config;
	private final _DAO<_PesaDomainModelRow> dao;
	private final KirjekyyhkyXMLGeneratorUtility generator;
	private final List<String> producedDataXMLFiles = new LinkedList<>();

	public PetoKirjekyyhkyXMLGenerator(_Request<_PesaDomainModelRow> request) throws Exception {
		this.request = request;
		this.config = request.config();
		this.dao = request.dao();
		this.generator = initGenerator();
	}

	@Override
	public File writeFormDataXML(Map<String, String> data, String filename) {
		filename += ".xml";
		List<String> owners = new ArrayList<>();

		if (data.containsKey("pesa.seur_tarkastaja")) {
			owners.add(data.get("pesa.seur_tarkastaja"));
			data.put("tarkastus.tarkastaja", data.get("pesa.seur_tarkastaja"));
		}
		for (String lomakeVastaanottaja : getLomakeVastaanottajat(data)) {
			if (!owners.contains(lomakeVastaanottaja)) {
				owners.add(lomakeVastaanottaja);
			}
		}

		String edellinenTarkPvm = data.get("edellinen_tark_pvm");
		String edellinen_tark_laji = data.get("edellinen_tark_laji");
		if (edellinenTarkPvm == null) {
			data.put("edellinen_vuosi", "");
		} else {
			try {
				DateValue date = DateUtils.convertToDateValue(edellinenTarkPvm);
				data.put("edellinen_vuosi", date.getYear());
			} catch (Exception e) {
				data.put("edellinen_vuosi", "");
			}
		}
		if (edellinen_tark_laji == null) {
			data.put("edellinen_laji", "");
		} else {
			data.put("edellinen_laji", edellinen_tark_laji);
		}

		data.remove("tarkastus.pesiva_laji");

		File file = new File(config.kirjekyyhkyFolder(), filename);
		try {
			String xml = generator.generateDataXML(data, owners);
			FileUtils.writeToFile(file, xml);
			producedDataXMLFiles.add(filename);
		} catch (Exception e) {
			e.printStackTrace();
			request.data().addToNoticeTexts("VIRHE: " + e.getMessage() + " " + e.getStackTrace()[0]);
			return null;
		}
		return file;
	}

	private List<String> getLomakeVastaanottajat(Map<String, String> data) {
		String pesaid = data.get("pesa.id");
		_Table searchParams = dao.newRow().getLomakeVastaanottajat();
		searchParams.get("pesa").setValue(pesaid);
		List<String> vastaanottajat = new ArrayList<>();
		try {
			for (_Table t : dao.returnTables(searchParams)) {
				vastaanottajat.add(t.get("vastaanottaja").getValue());
			}
		} catch (SQLException e) {
			throw new RuntimeException("Lomake vastaanottajat pesälle " + pesaid);
		}
		return vastaanottajat;
	}

	@Override
	public List<String> producedFormDataXMLFiles() {
		return producedDataXMLFiles;
	}

	public void writeStructureXML() throws Exception {
		String xml = generator.generateStructureXML();
		File file = new File(config.reportFolder(), "petoseuranta.xml");
		try {
			FileUtils.writeToFile(file, xml, request.config().characterEncoding());
		} catch (Exception e) {
			request.data().addToNoticeTexts("VIRHE: " + e.getMessage() + " " + e.getStackTrace()[0]);
		}
	}

	private KirjekyyhkyXMLGeneratorUtility initGenerator() throws FileNotFoundException, IOException {
		KirjekyyhkyXMLGeneratorUtility generator = new KirjekyyhkyXMLGeneratorUtility();
		StructureDefinition definition = generator.getDefinition();

		definition.typeID = "petoseuranta";
		definition.title = "Petolintujen pesätutkimus";

		definition.menutexts.newForm = "Ilmoita uusi pesä";
		definition.menutexts.printEmptyPdf = "Tulosta tyhjä lomake";
		// definition.menutexts.printInstructions = "Tulosta täyttöohje";

		definition.titleFields.add("tarkastus.vuosi");
		definition.titleFields.add("pesa.kunta");
		definition.titleFields.add("pesa.kyla");
		definition.titleFields.add("pesa.nimi");
		definition.titleFields.add("pesa.id");
		definition.titleFields.add("edellinen_laji");
		definition.titleFields.add("edellinen_vuosi");
		definition.titleFields.add("tarkastus.tarkastaja");

		definition.validation.URI = config.get("ValidatonServiceURL");
		definition.validation.username = config.get("ValidatonServiceUsername");
		definition.validation.password = config.get("ValidatonServicePassword");

		definition.coordinateFields.leveys = "pesa.leveys";
		definition.coordinateFields.pituus = "pesa.pituus";
		definition.coordinateFields.tyyppi = "pesa.koord_ilm_tyyppi";
		definition.coordinateFields.mittaustapa = "pesa.koord_ilm_mittaustapa";
		definition.coordinateFields.setTitleFields("pesa.kyla", "pesa.nimi", "pesa.id");

		_PesaDomainModelRow row = dao.newRow();
		definition.row = row;
		generator.excludeColumn("pesa", "yht_leveys");
		generator.excludeColumn("pesa", "yht_pituus");
		generator.excludeColumn("pesa", "ast_leveys");
		generator.excludeColumn("pesa", "ast_pituus");
		generator.excludeColumn("pesa", "des_leveys");
		generator.excludeColumn("pesa", "des_pituus");
		generator.excludeColumn("pesa", "eur_leveys");
		generator.excludeColumn("pesa", "eur_pituus");
		generator.excludeColumn("tarkastus", "id");
		generator.excludeColumn("tarkastus", "pesa");
		generator.excludeColumn("tarkastus", "ref_vanhaan_dataan");
		generator.excludeColumn("tarkastus", "oli_nipussa");
		generator.excludeColumn("tarkastus", "kommentti_edellinen_laji");
		generator.excludeColumn("tarkastus", "aikuisia_max_lkm");

		for (int i = 1; i<=row.getKaynnit().getStaticCount(); i++) {
			for (String c : KAYNTI_COLUMNS_TO_EXLUDE) {
				generator.excludeColumn("kaynti."+i, c);
			}
		}

		generator.addCustomColumn("pesa.leveys", _Column.INTEGER, 7);
		generator.addCustomColumn("pesa.pituus", _Column.INTEGER, 7);
		generator.addCustomColumn("edellinen_laji", _Column.VARCHAR, 40);
		generator.addCustomColumn("edellinen_vuosi", _Column.INTEGER, 4);

		generator.includeTable("pesa");
		generator.includeTable("tarkastus");
		generator.includeTable("kaynti.1");
		generator.includeTable("kaynti.2");
		generator.includeTable("kaynti.3");
		generator.includeTable("kaynti.4");

		definition.selections = createCopyAndModify(request.data().selections());

		generator.excludeSelection(TipuAPIClient.SPECIES);
		generator.excludeSelection(TipuAPIClient.MUNICIPALITIES);
		generator.excludeSelection(TipuAPIClient.RINGERS);
		generator.excludeSelection(TipuAPIClient.ELY_CENTRES);
		generator.excludeSelection(PetoConst.TAULUT);

		generator.defineTipuApiSelection("pesa", "seur_tarkastaja", TipuAPIClient.RINGERS);
		generator.defineTipuApiSelection("pesa", "kunta", TipuAPIClient.MUNICIPALITIES);
		//generator.defineTipuApiSelection("pesa", "rak_laji", TipuAPIClient.SPECIES);
		// generator.defineTipuApiSelection("tarkastus", "pesiva_laji", TipuAPIClient.SPECIES);
		generator.defineTipuApiSelection("tarkastus", "muu_laji", TipuAPIClient.SPECIES);
		generator.defineTipuApiSelection("tarkastus", "tarkastaja", TipuAPIClient.RINGERS);
		generator.noCodesForSelection("k_e");

		generator.userAsDefaultValue("pesa.seur_tarkastaja");
		generator.userAsDefaultValue("tarkastus.tarkastaja");

		generator.setCustomFieldRequired("pesa.leveys");
		generator.setCustomFieldRequired("pesa.pituus");

		Map<String, String> fieldTitles = new HashMap<>();
		for (Entry<String, String> e : request.data().uiTexts().entrySet()) {
			if (e.getKey().startsWith("tarkastuslomake.")) {
				// if (e.getKey().startsWith("tarkastuslomake.aikuinen.")) continue;
				fieldTitles.put(e.getKey().substring("tarkastuslomake.".length()), e.getValue());
			} else {
				if (!fieldTitles.containsKey(e.getKey())) {
					fieldTitles.put(e.getKey(), e.getValue());
				}
			}
		}
		definition.fieldTitles = fieldTitles;

		definition.layout = FileUtils.readContents(new File(config.templateFolder(), "layout.xml"));

		definition.emptyPDF = new File(config.templateFolder(), "tarkastuslomake_uusi_pesa.pdf");
		//definition.instructionsPDF = new File(config.templateFolder(), "merikotkatutkimus-ohjeet.pdf");
		return generator;
	}

	private Map<String, Selection> createCopyAndModify(Map<String, Selection> originals) {
		Map<String, Selection> selections = new HashMap<>(originals);
		SelectionImple koordIlmTyyppi = new SelectionImple( selections.get("pesa.koord_ilm_tyyppi") );
		koordIlmTyyppi.removeOption("A");
		selections.put("pesa.koord_ilm_tyyppi", koordIlmTyyppi);
		return selections;
	}

}
