package fi.hy.pese.framework.main.db.oraclesql;

import java.sql.SQLException;

public class NothingToDeleteException extends SQLException {

	private static final long	serialVersionUID	= 4355120165584099950L;

}
