package fi.hy.pese.framework.test.app.functionality;

import java.io.File;
import java.io.FileNotFoundException;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fi.hy.pese.framework.main.app.BaseFactoryForApplicationSpecificationFactory;
import fi.hy.pese.framework.main.app.FunctionalityExecutor;
import fi.hy.pese.framework.main.app.Request;
import fi.hy.pese.framework.main.app.RequestImpleData;
import fi.hy.pese.framework.main.app._Functionality;
import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.app.functionality.SearchFunctionality;
import fi.hy.pese.framework.main.app.functionality.validate._Validator;
import fi.hy.pese.framework.main.db._DAO;
import fi.hy.pese.framework.main.db._ResultSet;
import fi.hy.pese.framework.main.db._ResultsHandler;
import fi.hy.pese.framework.main.db.oraclesql.OracleSQL_DAO;
import fi.hy.pese.framework.main.general.ReportWriter;
import fi.hy.pese.framework.main.general.StringWrapper;
import fi.hy.pese.framework.main.general._PDFDataMapGenerator;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data.Data;
import fi.hy.pese.framework.main.general.data.DatabaseTableStructure;
import fi.hy.pese.framework.main.general.data.PesaDomainModelDatabaseStructure;
import fi.hy.pese.framework.main.general.data.PesaDomainModelRowFactory;
import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._Data;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.main.general.data._ResultSorter;
import fi.hy.pese.framework.main.general.data._RowFactory;
import fi.hy.pese.framework.main.general.data._Table;
import fi.hy.pese.framework.main.kirjekyyhky._KirjekyyhkyXMLGenerator;
import fi.hy.pese.framework.test.TestConfig;
import fi.hy.pese.framework.test.db.DAOStub;
import fi.hy.pese.merikotka.main.app.MerikotkaDatabaseStructureCreator;
import fi.hy.pese.peto.main.app.PetoDatabaseStructureCreator;
import fi.luomus.commons.config.Config;
import fi.luomus.commons.containers.Selection;
import fi.luomus.commons.kirjekyyhky.FormData;
import fi.luomus.commons.kirjekyyhky.KirjekyyhkyAPIClient;
import fi.luomus.commons.pdf.PDFWriter;
import fi.luomus.commons.utils.DateUtils;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class SearchFunctionalityAndReportWriterTest {

	public static Test suite() {
		return new TestSuite(SearchFunctionalityAndReportWriterTest.class.getDeclaredClasses());
	}

	public static class WhenUsingSearchFunctionality extends TestCase {

		private static final String	DESCRIPTION_FOR_MY_SEARCH	= "Description for my search";
		private static final String	MY_SEARCH_NAME				= "My Search";
		private static final String	SAVED_SEARCH_ROWID			= "AAA";
		private static final String	NEW_SEARCH_DESC				= "";
		private static final String	NEW_SEARCH					= "New Search";
		private static final String	NEW_SEARCH_ROWID			= "AAB";

		private class SearchFunctionalityTestFactory extends BaseFactoryForApplicationSpecificationFactory<_PesaDomainModelRow> {
			public SearchFunctionalityTestFactory(String configFileName) throws FileNotFoundException {
				super(configFileName);
			}

			@Override
			public _Functionality<_PesaDomainModelRow> functionalityFor(String page, _Request<_PesaDomainModelRow> request) {
				return new SearchFunctionality(request) {

					@Override
					public _PDFDataMapGenerator pdfDataMapGenerator() {
						return new _PDFDataMapGenerator() {

							@Override
							public Map<String, String> tarkastusDataFor(String tarkastusid) throws Exception {
								return new HashMap<>();
							}

							@Override
							public Map<String, String> tarkastusDataFor(String pesaid, String year) throws Exception {
								return new HashMap<>();
							}
						};
					}

					@Override
					protected _ResultSorter<_PesaDomainModelRow> sorter() {
						return null;
					}

					@Override
					protected void applicationPrintReport(String reportName) {}

					@Override
					public String deadNestsQuery() {
						return " dead nests query would go here ";
					}

					@Override
					public String liveNestsQuery() {
						return " live nests query would go here ";
					}

				};
			}

			@Override
			public _DAO<_PesaDomainModelRow> dao(String userId) {
				return new SearchFunctionalityTestFakeDAO();
			}

			@Override
			public Config config() {
				return config;
			}

			@Override
			public PDFWriter pdfWriter() {
				return new PDFWriter() {
					private final List<String>	producedFiles	= new ArrayList<>();
					int							i				= 1;

					@Override
					public File writePdf(String templatename, Map<String, String> data, String filename) {
						if (i == 1)
							filename = "REV_78_PESA_3_2010.pdf";
						else
							filename = "REV_78_PESA_6_2010.pdf";
						producedFiles.add(filename);
						i++;
						return null;
					}

					@Override
					public List<String> producedFiles() {
						return producedFiles;
					}

					@Override
					public List<String> noticeTexts() {
						return null;
					}

					@Override
					public File createCollection(String filename) {
						producedFiles.add(filename + ".pdf");
						return null;
					}
				};
			}

			@Override
			public _KirjekyyhkyXMLGenerator kirjekyyhkyFormDataXMLGenerator(_Request<_PesaDomainModelRow> request) {
				return formXMLgenerator;
			}

			@Override
			public KirjekyyhkyAPIClient kirjekyyhkyAPI() {
				return formXMLsender;
			}

			@Override
			protected Map<String, Selection> fetchSelections(_DAO<_PesaDomainModelRow> dao) throws Exception {
				return null;
			}
		}

		private class SearchFunctionalityTestFakeDAO extends OracleSQL_DAO<_PesaDomainModelRow> {
			public SearchFunctionalityTestFakeDAO() {
				super(null, rowFactory);
			}

			private boolean	newSearchAdded	= false;

			@Override
			public _PesaDomainModelRow newRow() {
				return rowFactory.newRow();
			}

			@Override
			public void addSavedSearchesListingToData(_Data<_PesaDomainModelRow> data) throws SQLException {
				if (newSearchAdded) {
					_Table search = this.newTableByName("saved_searches");
					search.setRowidValue(NEW_SEARCH_ROWID);
					search.get("name").setValue(NEW_SEARCH);
					search.get("description").setValue(NEW_SEARCH_DESC);
					data.addToList(Const.SAVED_SEARCHES, search);
				}
				_Table search = this.newTableByName("saved_searches");
				search.setRowidValue(SAVED_SEARCH_ROWID);
				search.get("name").setValue(MY_SEARCH_NAME);
				search.get("description").setValue(DESCRIPTION_FOR_MY_SEARCH);
				data.addToList(Const.SAVED_SEARCHES, search);
			}

			@Override
			public void executeInsert(_Table table) {
				newSearchAdded = true;
			}

			@Override
			public String returnAndSetNextId(_Table table) throws SQLException {
				return "";
			}

			@Override
			public int executeUpdate(String sql, String... values) throws SQLException {
				return 1;
			}

			@Override
			public void executeSearch(_PesaDomainModelRow parameters, _ResultsHandler resultHandler, String... additional) throws SQLException {
				if (parameters.getPesa().getUniqueNumericIdColumn().hasValue()) {
					resultHandler.process(new _ResultSet() {
						int	i	= 0;
						int	k	= 0;

						@Override
						public boolean next() throws SQLException {
							return ++i <= 2;
						}

						@Override
						public String getString(String columnLabel) throws SQLException {
							return "" + k++;
						}

						@Override
						public String getString(int i) throws SQLException {
							return "" + k++;
						}

						@Override
						public Date getDate(String columnLabel) throws SQLException {
							return null;
						}

						@Override
						public Date getDate(int i) throws SQLException {
							return null;
						}
					});
				} else {
					for (String s : additional) {
						additionalQueries += s;
					}
					_Table pesa = parameters.getPesa();
					_Column yht_leveys = pesa.get("yht_leveys");
					_Column yht_pituus = pesa.get("yht_pituus");
					_Column eur_leveys = pesa.get("eur_leveys");
					_Column eur_pituus = pesa.get("eur_pituus");
					coordinates = yht_leveys.getValue() + yht_pituus.getValue() + eur_leveys.getValue() + eur_pituus.getValue();
				}
			}
		}

		private class FakeKirjekyyhkyFormDataXMLGenerator implements _KirjekyyhkyXMLGenerator {

			List<String> files = new ArrayList<>();

			@Override
			public File writeFormDataXML(Map<String, String> data, String filename) {
				files.add(filename);
				return new File("foo");
			}

			@Override
			public List<String> producedFormDataXMLFiles() {
				return files;
			}

		}

		private class FakeKirjekyyhkyFormDataXMLSender implements KirjekyyhkyAPIClient {

			public int numberOfSendFiles = 0;
			public boolean hasBeenClosed = false;

			@Override
			public void send(File xml, File pdf) throws Exception {
				numberOfSendFiles++;
			}

			@Override
			public void close() {
				hasBeenClosed = true;
			}

			@Override
			public Integer getCount() {
				return null;
			}

			@Override
			public List<FormData> getForms() {
				return null;
			}

			@Override
			public void receive(String id) {
			}

			@Override
			public FormData get(String id) throws Exception {
				return null;
			}

			@Override
			public void sendForCorrections(String id, String message) {
			}

			@Override
			public void sendFormStructure(File xml, File js) throws Exception {
			}

		}

		private _Data<_PesaDomainModelRow>				data;

		private PesaDomainModelDatabaseStructure	structure;
		private _RowFactory<_PesaDomainModelRow> rowFactory;
		private String				additionalQueries;
		private String				coordinates;
		private _Functionality<_PesaDomainModelRow>		searchFunctionality;
		private _KirjekyyhkyXMLGenerator formXMLgenerator;
		private FakeKirjekyyhkyFormDataXMLSender formXMLsender;
		private final Config config = TestConfig.getConfig();

		@Override
		protected void setUp() throws Exception {
			structure = new PesaDomainModelDatabaseStructure();
			rowFactory = new PesaDomainModelRowFactory(structure, "", "");
			DatabaseTableStructure pesa = new DatabaseTableStructure("pesavakio");
			pesa.addColumn("pesaid", _Column.INTEGER, 5);
			pesa.addColumn("pesanimi", _Column.VARCHAR, 20);
			pesa.addColumn("yht_leveys", _Column.INTEGER, 6);
			pesa.addColumn("yht_pituus", _Column.INTEGER, 6);
			pesa.addColumn("eur_leveys", _Column.INTEGER, 6);
			pesa.addColumn("eur_pituus", _Column.INTEGER, 6);
			pesa.addColumn("ast_leveys", _Column.INTEGER, 6);
			pesa.addColumn("ast_pituus", _Column.INTEGER, 6);
			pesa.get("pesaid").setTypeToUniqueNumericIncreasingId();
			structure.setPesa(pesa);

			DatabaseTableStructure searches = new DatabaseTableStructure("saved_searches");
			searches.addColumn("id", _Column.INTEGER, 5);
			searches.addColumn("userid", _Column.VARCHAR, 25);
			searches.addColumn("last_used", _Column.DATE);
			searches.addColumn("name", _Column.VARCHAR, 150);
			searches.addColumn("description", _Column.VARCHAR, 500);
			searches.addColumn("selected_fields", _Column.VARCHAR, 2000);
			searches.addColumn("search_values", _Column.VARCHAR, 4000);
			searches.get("id").setTypeToUniqueNumericIncreasingId();
			structure.setSavedSearches(searches);

			DatabaseTableStructure tarkastus = new DatabaseTableStructure("tarkastus");
			tarkastus.addColumn("tarkid", _Column.INTEGER, 5);
			tarkastus.get("tarkid").setTypeToUniqueNumericIncreasingId();
			structure.setTarkastus(tarkastus);

			data = new Data<>(rowFactory);
			data.setUserId("lokki");
			SearchFunctionalityTestFactory factory = new SearchFunctionalityTestFactory(null);
			_Request<_PesaDomainModelRow> request = new Request<>(new RequestImpleData<_PesaDomainModelRow>().setData(data).setDao(factory.dao("userid")).setApplication(factory));
			data.setPage(Const.SEARCH);

			additionalQueries = "";
			coordinates = "";
			searchFunctionality = factory.functionalityFor(Const.SEARCH, request);
			formXMLgenerator = new FakeKirjekyyhkyFormDataXMLGenerator();
			formXMLsender = new FakeKirjekyyhkyFormDataXMLSender();
		}

		@Override
		public void tearDown() {
			File kokoelmaZip = new File(config.pdfFolder(), "Kokoelma.zip");
			File testPdf = new File(config.pdfFolder(), "test.pdf");
			if (kokoelmaZip.exists()) {
				kokoelmaZip.delete();
			}
			if (testPdf.exists()) {
				testPdf.delete();
			}
		}

		public void test__it_sets_coord_radius_to_empty_string_if_not_given() throws Exception {
			FunctionalityExecutor.execute(searchFunctionality);
			FunctionalityExecutor.execute(searchFunctionality);
			assertEquals("", data.get(Const.COORDINATE_SEARCH_RADIUS));
		}

		public void test__it_sets_coord_radius() throws Exception {
			data.set(Const.COORDINATE_SEARCH_RADIUS, "100");
			FunctionalityExecutor.execute(searchFunctionality);
			assertEquals("100", data.get(Const.COORDINATE_SEARCH_RADIUS));
		}

		public void test__it_sets_nest_condition_to_ALIVE_if_not_given() throws Exception {
			FunctionalityExecutor.execute(searchFunctionality);
			assertEquals(Const.NEST_CONDITION__ALL, data.get(Const.NEST_CONDITION));
		}

		public void test__it_generates_a_list_of_selected_fields_if_given() throws Exception {
			data.set(Const.SELECTED_FIELDS, "pesavakio.pesaid, pesavakio.pesanimi, ");
			FunctionalityExecutor.execute(searchFunctionality);
			List<Object> list = data.getLists().get(Const.SELECTED_FIELDS);
			assertNotNull(list);
			assertEquals(2, list.size());
			for (Object o : list) {
				assertTrue(o instanceof StringWrapper);
			}
			assertEquals("pesavakio.pesaid", list.get(0).toString());
			assertEquals("pesavakio.pesanimi", list.get(1).toString());
		}

		public void test__it_includes_pesaid_to_selected_fields_if_not_given() throws Exception {
			data.set(Const.SELECTED_FIELDS, "pesavakio.pesanimi");
			FunctionalityExecutor.execute(searchFunctionality);
			List<Object> list = data.getLists().get(Const.SELECTED_FIELDS);
			assertNotNull(list);
			assertEquals(2, list.size());
			for (Object o : list) {
				assertTrue(o instanceof StringWrapper);
			}
			assertEquals("pesavakio.pesanimi", list.get(0).toString());
			assertEquals("pesavakio.pesaid", list.get(1).toString());
		}

		public void test__it_doesnt_generate_selected_fields_list_if_the_selected_fields_parameters_not_given() throws Exception {
			FunctionalityExecutor.execute(searchFunctionality);
			assertFalse(data.getLists().containsKey(Const.SELECTED_FIELDS));
		}

		public void test__sets_action_to_printreport_if_printreport_key_exists_in_data() throws Exception {
			data.set(Const.PRINT_TO_TEXTFILE, "foo");
			FunctionalityExecutor.execute(searchFunctionality);
			assertEquals(Const.PRINT_TO_TEXTFILE, data.action());
		}

		public void test___generates_search_values_string_to_data_from_searchparameters__for_save_search_feature() throws Exception {
			data.searchparameters().getPesa().get("pesaid").setValue("5-10");
			data.searchparameters().getPesa().get("pesanimi").setValue("%A%");
			data.set(Const.COORDINATE_SEARCH_RADIUS, "1000");
			data.set(Const.NEST_CONDITION, Const.NEST_CONDITION__ONLY_DESTROYED_NESTS);
			FunctionalityExecutor.execute(searchFunctionality);
			String expected = "pesavakio.pesaid=5-10" + Const.SEPARATOR + "pesavakio.pesanimi=%A%";
			expected += Const.SEPARATOR + Const.COORDINATE_SEARCH_RADIUS + "=1000";
			expected += Const.SEPARATOR + Const.NEST_CONDITION + "=" + Const.NEST_CONDITION__ONLY_DESTROYED_NESTS;
			assertEquals(expected, data.get(Const.SEARCH_VALUES));
		}

		public void test___it_loads_a_list_of_saved_searches() throws Exception {
			FunctionalityExecutor.execute(searchFunctionality);
			assertFalse("Should contain saved searches", data.getLists().get(Const.SAVED_SEARCHES).isEmpty());
			assertTrue("Should contain tables", data.getLists().get(Const.SAVED_SEARCHES).get(0) instanceof _Table);
			_Table search = (_Table) data.getLists().get(Const.SAVED_SEARCHES).get(0);
			assertEquals(SAVED_SEARCH_ROWID, search.getRowidValue());
			assertEquals(MY_SEARCH_NAME, search.get("name").getValue());
			assertEquals(DESCRIPTION_FOR_MY_SEARCH, search.get("description").getValue());
		}

		public void test__it_saves_a_search() throws Exception {
			data.setAction(Const.SAVE_SEARCH);
			data.set(Const.SELECTED_FIELDS, "pesavakio.pesa_id,pesavakio.pesanimi,vuosi.p_kunta_id,");
			data.set(Const.SEARCH_VALUES, "reviiri.reviiri_id=3,tarkastus.tark_vuosi=2005,");
			data.set(Const.SEARCH_NAME, NEW_SEARCH);
			data.set(Const.SEARCH_DESCRIPTION, NEW_SEARCH_DESC);
			FunctionalityExecutor.execute(searchFunctionality);
			assertEquals(2, data.getLists().get(Const.SAVED_SEARCHES).size());
		}

		public void test___when_executing_a_saved_search_it_sets_searchvalues_to_searchparameters_and_data() throws Exception {
			data.set(Const.SEARCH_VALUES, "pesavakio.pesaid=1-50" + Const.SEPARATOR + "pesavakio.pesanimi=%a%" + Const.SEPARATOR + "foo=5" + Const.SEPARATOR + "CoordinateSearchRadius=1000" +
					Const.SEPARATOR);
			data.setAction(Const.EXECUTE_SAVED_SEARH);
			FunctionalityExecutor.execute(searchFunctionality);
			assertEquals("1-50", data.searchparameters().getPesa().get("pesaid").getValue());
			assertEquals("%a%", data.searchparameters().getPesa().get("pesanimi").getValue());
			assertEquals("5", data.get("foo"));
			assertEquals("1000", data.get("CoordinateSearchRadius"));
		}

		public void test__when_printing_pdf_it_calls_pdf_writer___does_not__call__kirjekyyhkyXMLGenerator() throws Exception {
			data.set(Const.YEAR, "2010");
			data.setAction(Const.PRINT_PDF);
			data.searchparameters().getPesa().getUniqueNumericIdColumn().setValue("3|6");
			FunctionalityExecutor.execute(searchFunctionality);
			assertEquals(2, data.producedFiles().size());
			assertEquals("REV_78_PESA_3_2010.pdf", data.producedFiles().get(0));
			assertEquals("REV_78_PESA_6_2010.pdf", data.producedFiles().get(1));
			assertEquals(0, formXMLgenerator.producedFormDataXMLFiles().size());
		}

		public void test__if_collection_filename_given_creates_a_collection_of_all_files__and_a_zip_file() throws Exception {
			data.set(Const.YEAR, "2010");
			data.setAction(Const.PRINT_PDF);
			data.searchparameters().getPesa().getUniqueNumericIdColumn().setValue("3|6");
			data.set(Const.COLLECTION_FILENAME, "Kokoelma");
			FunctionalityExecutor.execute(searchFunctionality);
			assertEquals(4, data.producedFiles().size());
			assertEquals("Kokoelma.zip", data.producedFiles().get(0));
			assertEquals("REV_78_PESA_3_2010.pdf", data.producedFiles().get(1));
			assertEquals("REV_78_PESA_6_2010.pdf", data.producedFiles().get(2));
			assertEquals("Kokoelma.pdf", data.producedFiles().get(3));
			assertTrue(new File(config.pdfFolder(), "Kokoelma.zip").exists());
		}

		public void test__when_printing_pdf_it__creates__kirjekyyhky_form_data_files() throws Exception {
			data.set(Const.YEAR, "2010");
			data.set(Const.GENERATE_KIRJEKYYHKY_FORM_DATA_XML, Const.YES);
			data.setAction(Const.PRINT_PDF);
			data.searchparameters().getPesa().getUniqueNumericIdColumn().setValue("3|6");
			FunctionalityExecutor.execute(searchFunctionality);
			assertEquals(2, data.producedFiles().size());
			assertEquals("REV_78_PESA_3_2010.pdf", data.producedFiles().get(0));
			assertEquals("REV_78_PESA_6_2010.pdf", data.producedFiles().get(1));
			assertEquals(2, formXMLgenerator.producedFormDataXMLFiles().size());
			assertEquals(2, formXMLsender.numberOfSendFiles);
			assertEquals(true, formXMLsender.hasBeenClosed);
		}

		public void test___all_nests() throws Exception {
			data.setAction(Const.SEARCH);
			FunctionalityExecutor.execute(searchFunctionality);
			assertEquals("", additionalQueries);
		}

		public void test___only_live_nests() throws Exception {
			data.setAction(Const.SEARCH);
			data.set(Const.NEST_CONDITION, Const.NEST_CONDITION__ONLY_LIVE_NESTS);
			FunctionalityExecutor.execute(searchFunctionality);
			assertEquals(" live nests query would go here ", additionalQueries);
		}

		public void test___only_destroyed_nests() throws Exception {
			data.setAction(Const.SEARCH);
			data.set(Const.NEST_CONDITION, Const.NEST_CONDITION__ONLY_DESTROYED_NESTS);
			FunctionalityExecutor.execute(searchFunctionality);
			assertEquals(" dead nests query would go here ", additionalQueries);
		}

		public void test___if_coordinate_radius_given__it_must_be_numeric() throws Exception {
			data.setAction(Const.SEARCH);
			data.set(Const.COORDINATE_SEARCH_RADIUS, "foo");
			FunctionalityExecutor.execute(searchFunctionality);
			assertEquals(_Validator.MUST_BE_INTEGER, data.errors().get(Const.COORDINATE_SEARCH_RADIUS));
		}

		public void test___if_coordinate_radius_given__one_pair_of_coordinates_must_be_given() throws Exception {
			data.setAction(Const.SEARCH);
			data.set(Const.COORDINATE_SEARCH_RADIUS, "1000");
			FunctionalityExecutor.execute(searchFunctionality);
			assertEquals(_Validator.COORDINATES_MUST_BE_GIVEN, data.errors().get(Const.COORDINATE_FIELDS));
		}

		public void test___if_coordinate_radius_given__one_pair_of_coordinates_must_be_given2() throws Exception {
			data.setAction(Const.SEARCH);
			data.set(Const.COORDINATE_SEARCH_RADIUS, "1000");
			data.searchparameters().getPesa().get("yht_leveys").setValue("5");
			FunctionalityExecutor.execute(searchFunctionality);
			assertEquals(_Validator.COORDINATES_MUST_BE_GIVEN, data.errors().get(Const.COORDINATE_FIELDS));
		}

		public void test___if_coordinate_radius_given__one_pair_of_coordinates_must_be_given3() throws Exception {
			data.setAction(Const.SEARCH);
			data.set(Const.COORDINATE_SEARCH_RADIUS, "1000");
			data.searchparameters().getPesa().get("yht_leveys").setValue("5");
			data.searchparameters().getPesa().get("yht_pituus").setValue("5");
			FunctionalityExecutor.execute(searchFunctionality);
			assertEquals(null, data.errors().get(Const.COORDINATE_FIELDS));
		}

		public void test___if_coordinate_radius_given__one_pair_of_coordinates_must_be_given4() throws Exception {
			data.setAction(Const.SEARCH);
			data.set(Const.COORDINATE_SEARCH_RADIUS, "1000");
			data.searchparameters().getPesa().get("eur_leveys").setValue("5");
			data.searchparameters().getPesa().get("eur_pituus").setValue("5");
			FunctionalityExecutor.execute(searchFunctionality);
			assertEquals(null, data.errors().get(Const.COORDINATE_FIELDS));
		}

		public void test___if_coordinate_radius_given__only_one_pair_of_coordinates_can_be_given() throws Exception {
			data.setAction(Const.SEARCH);
			data.set(Const.COORDINATE_SEARCH_RADIUS, "1000");
			data.searchparameters().getPesa().get("eur_leveys").setValue("5");
			data.searchparameters().getPesa().get("eur_pituus").setValue("5");
			data.searchparameters().getPesa().get("yht_leveys").setValue("5");
			FunctionalityExecutor.execute(searchFunctionality);
			assertEquals(_Validator.COORDINATES_MUST_BE_GIVEN, data.errors().get(Const.COORDINATE_FIELDS));
		}

		public void test___if_coordinate_radius_given__given_coordinates_must_be_numeric__and_not_for_example_value_ranges_or_list() throws Exception {
			data.setAction(Const.SEARCH);
			data.set(Const.COORDINATE_SEARCH_RADIUS, "1000");
			data.searchparameters().getPesa().get("eur_leveys").setValue("5-10");
			data.searchparameters().getPesa().get("eur_pituus").setValue("5|10");
			FunctionalityExecutor.execute(searchFunctionality);
			assertEquals(_Validator.COORDINATE_MUST_BE_INTEGER_IF_SEARCH_RADIUS_GIVEN, data.errors().get("pesavakio.eur_leveys"));
			assertEquals(_Validator.COORDINATE_MUST_BE_INTEGER_IF_SEARCH_RADIUS_GIVEN, data.errors().get("pesavakio.eur_pituus"));
		}

		public void test___if_coordinate_radius_given__given_coordinates_must_be_numeric__and_not_for_example_value_ranges_or_list2() throws Exception {
			data.setAction(Const.SEARCH);
			data.set(Const.COORDINATE_SEARCH_RADIUS, "1000");
			data.searchparameters().getPesa().get("yht_leveys").setValue("5-10");
			data.searchparameters().getPesa().get("yht_pituus").setValue("5|10");
			FunctionalityExecutor.execute(searchFunctionality);
			assertEquals(_Validator.COORDINATE_MUST_BE_INTEGER_IF_SEARCH_RADIUS_GIVEN, data.errors().get("pesavakio.yht_leveys"));
			assertEquals(_Validator.COORDINATE_MUST_BE_INTEGER_IF_SEARCH_RADIUS_GIVEN, data.errors().get("pesavakio.yht_pituus"));
		}

		public void test___search_by_radius() throws Exception {
			data.setAction(Const.SEARCH);
			data.set(Const.COORDINATE_SEARCH_RADIUS, "1000");
			data.searchparameters().getPesa().get("eur_leveys").setValue("3000");
			data.searchparameters().getPesa().get("eur_pituus").setValue("5000");
			FunctionalityExecutor.execute(searchFunctionality);
			assertEquals("SQRT( POWER(3000 - pesavakio.eur_leveys, 2) + POWER(5000 - pesavakio.eur_pituus, 2) ) <= 1000", additionalQueries);
			assertEquals("Keeps original values", "3000", data.searchparameters().getPesa().get("eur_leveys").getValue());
			assertEquals("Keeps original values", "5000", data.searchparameters().getPesa().get("eur_pituus").getValue());
			assertEquals("But doesn't search with them", "", coordinates);
		}

		public void test___search_by_radius2() throws Exception {
			data.setAction(Const.SEARCH);
			data.set(Const.COORDINATE_SEARCH_RADIUS, "50000");
			data.searchparameters().getPesa().get("yht_leveys").setValue("2000");
			data.searchparameters().getPesa().get("yht_pituus").setValue("7000");
			FunctionalityExecutor.execute(searchFunctionality);
			assertEquals("SQRT( POWER(2000 - pesavakio.yht_leveys, 2) + POWER(7000 - pesavakio.yht_pituus, 2) ) <= 50000", additionalQueries);
			assertEquals("Keeps original values", "2000", data.searchparameters().getPesa().get("yht_leveys").getValue());
			assertEquals("Keeps original values", "7000", data.searchparameters().getPesa().get("yht_pituus").getValue());
			assertEquals("But doesn't search with them", "", coordinates);
		}

	}

	public static class WhenPrintingReports extends TestCase {

		private _Data<_PesaDomainModelRow>	data;
		private Config	testConfig;
		private String	reportRequests;

		private class ReportPrintingTestFactory extends BaseFactoryForApplicationSpecificationFactory<_PesaDomainModelRow> {
			public ReportPrintingTestFactory(String configFileName) throws FileNotFoundException {
				super(configFileName);
			}

			@Override
			public _Functionality<_PesaDomainModelRow> functionalityFor(String page, _Request<_PesaDomainModelRow> request) {
				return new SearchFunctionality(request) {
					@Override
					protected _ResultSorter<_PesaDomainModelRow> sorter() {
						return null;
					}

					@Override
					protected void applicationPrintReport(String reportName) {

						ReportWriter<_PesaDomainModelRow> writer = new ReportWriter<_PesaDomainModelRow>(request, reportName) {

							@Override
							protected boolean produceSpecific() throws Exception {
								if (filenamePrefix.equals("nogood")) {
									request.data().addToNoticeTexts("application writes this error");
									return false;
								}
								reportRequests += filenamePrefix + "|";
								return true;
							}
						};
						writer.produce();

					}

					@Override
					public String deadNestsQuery() {
						// Auto-generated method stub
						return null;
					}

					@Override
					public String liveNestsQuery() {
						// Auto-generated method stub
						return null;
					}
				};
			}

			@Override
			public _DAO<_PesaDomainModelRow> dao(String userId) {
				return new FakeDao(rowFactory);
			}

			@Override
			public Config config() {
				return testConfig;
			}

			@Override
			protected Map<String, Selection> fetchSelections(_DAO<_PesaDomainModelRow> dao) throws Exception {
				return null;
			}
		}

		private static class FakeDao extends DAOStub<_PesaDomainModelRow> {
			private final _RowFactory<_PesaDomainModelRow> f;
			public FakeDao(_RowFactory<_PesaDomainModelRow> f) {
				this.f = f;
			}
			@Override
			public _PesaDomainModelRow newRow() {
				return f.newRow();
			}
		}

		private _Functionality<_PesaDomainModelRow>	searchFunctionality;
		private PesaDomainModelDatabaseStructure structure;
		private _RowFactory<_PesaDomainModelRow> rowFactory;

		@Override
		protected void setUp() throws Exception {
			testConfig = TestConfig.getConfig();
			structure =  MerikotkaDatabaseStructureCreator.hardCoded();
			rowFactory = new PesaDomainModelRowFactory(structure, "", "");
			data = new Data<>(rowFactory);
			ReportPrintingTestFactory f = new ReportPrintingTestFactory(null);
			f.setRowFactory(rowFactory);
			RequestImpleData<_PesaDomainModelRow> reqData = new RequestImpleData<_PesaDomainModelRow>().setData(data).setDao(f.dao("")).setApplication(f);
			_Request<_PesaDomainModelRow> request = new Request<>(reqData);
			data.setPage(Const.REPORTS_PAGE);
			reportRequests = "";
			searchFunctionality = f.functionalityFor(Const.REPORTS_PAGE, request);
		}

		@Override
		protected void tearDown() {
			for (File f : new File(testConfig.reportFolder()).listFiles()) {
				if (f.getAbsolutePath().endsWith(".txt")) {
					f.delete();
				}
			}
		}

		public void test___default_nest_condition_is_live_nests() throws Exception {
			FunctionalityExecutor.execute(searchFunctionality);
			FunctionalityExecutor.execute(searchFunctionality);
			assertEquals(Const.NEST_CONDITION__ONLY_LIVE_NESTS, data.get(Const.NEST_CONDITION));
		}

		public void test___no_validation_errors__if_no_action() throws Exception {
			FunctionalityExecutor.execute(searchFunctionality);
			assertEquals(0, data.errors().size());
		}

		public void test___printing_a_report___validation_error__if_no_report_selected() throws Exception {
			data.setAction(Const.PRINT_REPORT);
			FunctionalityExecutor.execute(searchFunctionality);
			assertEquals(1, data.errors().size());
			assertEquals(_Validator.AT_LEAST_ONE_MUST_BE_SELECTED, data.errors().get(Const.SELECTED_REPORTS));
		}

		public void test__asks_implementing_application_to_produce_the_report() throws Exception {
			data.setAction(Const.PRINT_REPORT);
			data.set(Const.SELECTED_REPORTS, "A");
			FunctionalityExecutor.execute(searchFunctionality);
			assertEquals(0, data.errors().size());
			String timestamp = DateUtils.getCurrentDateTime("yyyy-MM-dd-HH-mm-ss");
			assertEquals("A_" + timestamp + ".txt", data.producedFiles().get(0));
			assertEquals(0, data.noticeTexts().size());
			assertEquals("A|", reportRequests);
		}

		public void test__asks_implementing_application_to_produce_multiple_reports() throws Exception {
			data.setAction(Const.PRINT_REPORT);
			data.set(Const.SELECTED_REPORTS, "uinuvat_unnikot |B| C");
			FunctionalityExecutor.execute(searchFunctionality);
			assertEquals(0, data.errors().size());
			String timestamp = DateUtils.getCurrentDateTime("yyyy-MM-dd-HH-mm-ss");
			assertEquals("uinuvat_unnikot_" + timestamp + ".txt", data.producedFiles().get(0));
			assertEquals("B_" + timestamp + ".txt", data.producedFiles().get(1));
			assertEquals("C_" + timestamp + ".txt", data.producedFiles().get(2));
			assertEquals(0, data.noticeTexts().size());
			assertEquals("uinuvat_unnikot|B|C|", reportRequests);
		}

		public void test___if__implementing_application_returns_false() throws Exception {
			data.setAction(Const.PRINT_REPORT);
			data.set(Const.SELECTED_REPORTS, "nogood");
			FunctionalityExecutor.execute(searchFunctionality);
			assertEquals(0, data.errors().size());
			assertEquals(0, data.producedFiles().size());

			File dir = new File(testConfig.reportFolder());
			for (String s : dir.list()) {
				if (s.startsWith("nogood")) {
					fail("Should not have created the file " + s);
				}
			}

			assertEquals(1, data.noticeTexts().size());
			assertEquals("application writes this error", data.noticeTexts().get(0));
		}

		public void test___if__implementing_application_returns_false__for_one__but_not_for_others() throws Exception {
			data.setAction(Const.PRINT_REPORT);
			data.set(Const.SELECTED_REPORTS, "A|nogood|C|"); // Note the extra |
			FunctionalityExecutor.execute(searchFunctionality);
			assertEquals(0, data.errors().size());
			assertEquals(2, data.producedFiles().size());

			File dir = new File(testConfig.reportFolder());
			for (String s : dir.list()) {
				if (s.startsWith("nogood")) {
					fail("Should not have created the file " + s);
				}
			}

			assertEquals(1, data.noticeTexts().size());
			assertEquals("application writes this error", data.noticeTexts().get(0));
		}

	}


	public static class AddingResultColumnsToDataLists extends TestCase {

		private SearchFunctionality searchFunctionality;
		private _Data<_PesaDomainModelRow> data;
		private final PesaDomainModelDatabaseStructure structure  = PetoDatabaseStructureCreator.hardCoded();
		private final _RowFactory<_PesaDomainModelRow> rowFactory = new PesaDomainModelRowFactory(structure, "", "");
		@Override
		public void setUp() {
			data = new Data<>(rowFactory);
			data.setPage(Const.SEARCH_PAGE);
			_Request<_PesaDomainModelRow> request = new Request<>(new RequestImpleData<_PesaDomainModelRow>().setData(data).setDao(new DAOStub<_PesaDomainModelRow>()));
			searchFunctionality = new SearchFunctionality(request) {
				@Override
				protected _ResultSorter<_PesaDomainModelRow> sorter() {
					return null;
				}
				@Override
				protected void applicationPrintReport(String reportName) {
				}
				@Override
				public String deadNestsQuery() {
					// Auto-generated method stub
					return null;
				}
				@Override
				public String liveNestsQuery() {
					// Auto-generated method stub
					return null;
				}
			};
		}

		public void test___adds_empty_list_if_selected_fields_not_set() throws Exception {
			FunctionalityExecutor.execute(searchFunctionality);
			assertEquals(true, data.getLists().containsKey(Const.RESULT_COLUMNS));
			assertEquals(0, data.getLists().get(Const.RESULT_COLUMNS).size());
		}

		public void test___adds_normal_fields() throws Exception {
			data.addToList(Const.SELECTED_FIELDS, "pesa.id");
			data.addToList(Const.SELECTED_FIELDS, "tarkastus.vuosi");
			FunctionalityExecutor.execute(searchFunctionality);
			assertEquals(true, data.getLists().containsKey(Const.RESULT_COLUMNS));
			List<Object> list = data.getLists().get(Const.RESULT_COLUMNS);
			assertEquals("pesa.id", list.get(0).toString());
			assertEquals("tarkastus.vuosi", list.get(1).toString());
		}
		public void test___adds_normal_and_kaynti_fields() throws Exception {
			data.addToList(Const.SELECTED_FIELDS, "pesa.id");
			data.addToList(Const.SELECTED_FIELDS, "tarkastus.vuosi");
			data.addToList(Const.SELECTED_FIELDS, "kaynti.pvm");
			data.addToList(Const.SELECTED_FIELDS, "kaynti.id");
			FunctionalityExecutor.execute(searchFunctionality);
			List<Object> list = data.getLists().get(Const.RESULT_COLUMNS);
			assertEquals("pesa.id", list.get(0).toString());
			assertEquals("tarkastus.vuosi", list.get(1).toString());
			assertEquals("kaynti.1.pvm", list.get(2).toString());
			assertEquals("kaynti.1.id", list.get(3).toString());
			assertEquals("kaynti.2.pvm", list.get(4).toString());
			assertEquals("kaynti.2.id", list.get(5).toString());
			assertEquals("kaynti.3.pvm", list.get(6).toString());
			assertEquals("kaynti.3.id", list.get(7).toString());
		}

		public void test___only_as_many_kaynti_fields_as_reported_to_have_been_used_in_this_resultset() throws Exception {
			data.addToList(Const.SELECTED_FIELDS, "pesa.id");
			data.addToList(Const.SELECTED_FIELDS, "tarkastus.vuosi");
			data.addToList(Const.SELECTED_FIELDS, "kaynti.pvm");
			data.addToList(Const.SELECTED_FIELDS, "kaynti.id");
			data.getCountOfUsedSlots().setUsedKaynnit(1);
			FunctionalityExecutor.execute(searchFunctionality);
			List<Object> list = data.getLists().get(Const.RESULT_COLUMNS);
			assertEquals("pesa.id", list.get(0).toString());
			assertEquals("tarkastus.vuosi", list.get(1).toString());
			assertEquals("kaynti.1.pvm", list.get(2).toString());
			assertEquals("kaynti.1.id", list.get(3).toString());
			assertEquals(4, list.size());
		}

		public void test___only_as_many_kaynti_fields_as_reported_to_have_been_used_in_this_resultset_2() throws Exception {
			data.addToList(Const.SELECTED_FIELDS, "pesa.id");
			data.addToList(Const.SELECTED_FIELDS, "tarkastus.vuosi");
			data.addToList(Const.SELECTED_FIELDS, "kaynti.pvm");
			data.addToList(Const.SELECTED_FIELDS, "kaynti.id");
			data.getCountOfUsedSlots().setUsedKaynnit(2);
			FunctionalityExecutor.execute(searchFunctionality);
			List<Object> list = data.getLists().get(Const.RESULT_COLUMNS);
			assertEquals(6, list.size());
		}
	}


}
