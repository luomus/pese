package fi.hy.pese.rengastus.main.app;

import fi.hy.pese.framework.main.general.data.Row;
import fi.hy.pese.framework.main.general.data.Table;
import fi.hy.pese.framework.main.general.data._Table;

public class RengastusDomainModelRow extends Row implements _RengastusDomainModelRow {
	
	private static final long	serialVersionUID	= 8637460608877999315L;
	
	private final Table kontrolli;
	
	public RengastusDomainModelRow(RengastusDatabaseStructure rowStructure) {
		this(rowStructure, null);
	}
	
	public RengastusDomainModelRow(RengastusDatabaseStructure rowStructure, String prefix) {
		super(rowStructure, prefix);
		kontrolli = new Table(rowStructure.getKontrolli());
		defineBasetable(kontrolli);
	}
	
	@Override
	public _Table getKontrolli() {
		return kontrolli;
	}
	
}
