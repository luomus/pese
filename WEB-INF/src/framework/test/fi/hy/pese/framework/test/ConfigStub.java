package fi.hy.pese.framework.test;

import java.util.Collection;
import java.util.List;

import fi.luomus.commons.config.Config;
import fi.luomus.commons.db.connectivity.ConnectionDescription;

public class ConfigStub implements Config {
	
	@Override
	public boolean defines(String property) {
		//  Auto-generated method stub
		return false;
	}
	
	@Override
	public String get(String property) throws IllegalArgumentException {
		//  Auto-generated method stub
		return null;
	}
	
	@Override
	public boolean developmentMode() {
		//  Auto-generated method stub
		return false;
	}
	
	@Override
	public boolean stagingMode() {
		//  Auto-generated method stub
		return false;
	}
	
	@Override
	public String characterEncoding() {
		//  Auto-generated method stub
		return null;
	}
	
	@Override
	public String systemId() throws UnsupportedOperationException {
		//  Auto-generated method stub
		return null;
	}
	
	@Override
	public String baseFolder() throws UnsupportedOperationException {
		//  Auto-generated method stub
		return null;
	}
	
	@Override
	public String baseURL() throws UnsupportedOperationException {
		//  Auto-generated method stub
		return null;
	}
	
	@Override
	public String commonURL() throws UnsupportedOperationException {
		//  Auto-generated method stub
		return null;
	}
	
	@Override
	public String templateFolder() throws UnsupportedOperationException {
		//  Auto-generated method stub
		return null;
	}
	
	@Override
	public String languagefileFolder() throws UnsupportedOperationException {
		//  Auto-generated method stub
		return null;
	}
	
	@Override
	public String defaultLanguage() throws UnsupportedOperationException {
		//  Auto-generated method stub
		return null;
	}
	
	@Override
	public ConnectionDescription connectionDescription() throws UnsupportedOperationException {
		//  Auto-generated method stub
		return null;
	}
	
	@Override
	public boolean parameterLogging() {
		//  Auto-generated method stub
		return false;
	}
	
	@Override
	public String logFolder() throws UnsupportedOperationException {
		//  Auto-generated method stub
		return null;
	}
	
	@Override
	public String reportFolder() throws UnsupportedOperationException {
		//  Auto-generated method stub
		return null;
	}
	
	@Override
	public String pdfFolder() throws UnsupportedOperationException {
		//  Auto-generated method stub
		return null;
	}
	
	@Override
	public String kirjekyyhkyFolder() throws UnsupportedOperationException {
		//  Auto-generated method stub
		return null;
	}
	
	@Override
	public Collection<String> supportedLanguages() throws UnsupportedOperationException {
		//  Auto-generated method stub
		return null;
	}
	
	@Override
	public int getInteger(String property) throws IllegalArgumentException, NumberFormatException {
		// Auto-generated method stub
		return 0;
	}
	
	@Override
	public String fontFolder() throws UnsupportedOperationException {
		return null;
	}
	
	@Override
	public ConnectionDescription connectionDescription(String systemName) throws UnsupportedOperationException {
		return null;
	}
	
	@Override
	public String languageFilePrefix() throws UnsupportedOperationException {
		return null;
	}
	
	@Override
	public String staticURL() throws UnsupportedOperationException {
		return null;
	}
	
	@Override
	public List<String> languageFiles() throws UnsupportedOperationException {
		return null;
	}
	
	@Override
	public boolean productionMode() {
		// Auto-generated method stub
		return false;
	}
	
	@Override
	public String dataFolder() throws UnsupportedOperationException {
		// Auto-generated method stub
		return null;
	}
	
}
