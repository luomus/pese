package fi.hy.pese.framework.test;

import java.io.File;
import java.io.FileNotFoundException;

import fi.luomus.commons.config.Config;
import fi.luomus.commons.config.ConfigReader;

public class TestConfig {
	
	private static final String FALLBACK_DEFAULT_TOMCAT_FOLDER = "C:/apache-tomcat";
	
	public static Config getConfig()  {
		try {
			String fullPath = getConfigFile();
			Config config = new ConfigReader(fullPath);
			return config;
		} catch (FileNotFoundException e) {
			throw new RuntimeException(e);
		}
	}
	
	public static String getConfigFile() {
		return getConfigFile("pese_frameworktests.config");
	}
	
	public static String getConfigFile(String configFile) {
		String base = System.getenv("CATALINA_HOME");
		if (base == null) base = FALLBACK_DEFAULT_TOMCAT_FOLDER;
		String fullPath = base + File.separator + "app-conf" + File.separator + configFile;
		System.out.println("Using test config " + fullPath);
		return fullPath;
	}
	
}
