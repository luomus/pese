package fi.hy.pese.framework.test.general;

import java.util.ArrayList;
import java.util.List;

import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data.Column;
import fi.hy.pese.framework.main.general.data.DatabaseColumnStructure;
import fi.hy.pese.framework.main.general.data.DatabaseTableStructure;
import fi.hy.pese.framework.main.general.data.PesaDomainModelDatabaseStructure;
import fi.hy.pese.framework.main.general.data.PesaDomainModelRow;
import fi.hy.pese.framework.main.general.data.Table;
import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.main.general.data._ReferenceKey;
import fi.hy.pese.framework.main.general.data._Table;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class RowdatastructureTest {
	
	public static Test suite() {
		return new TestSuite(RowdatastructureTest.class.getDeclaredClasses());
	}
	
	public static class WhenCreatingTables extends TestCase {
		
		public void test_it_requires_a_valid_databasename_for_the_table() {
			new DatabaseTableStructure("pesavakio");
			try {
				new DatabaseTableStructure("");
				new Table(null);
				fail("Should not accept null or empty strings");
			} catch (Exception e) {
			}
		}
		
		public void test_it_allows_to_set_the_name_of_the_table_and_the_id_field() {
			DatabaseTableStructure t = new DatabaseTableStructure("pesavakio");
			t.addColumn("pesaid", _Column.INTEGER, 5);
			t.get("pesaid").setTypeToUniqueNumericIncreasingId();
			assertEquals("pesavakio", t.getName());
			assertEquals("pesaid", t.getUniqueNumericIdColumn().getName());
		}
		
	}
	
	public static class WhenAddingColumns extends TestCase {
		
		private PesaDomainModelDatabaseStructure	rowStructure;
		
		@Override
		protected void setUp() throws Exception {
			rowStructure = new PesaDomainModelDatabaseStructure();
			rowStructure.setPesa(new DatabaseTableStructure("pesavakio"));
			
		}
		
		public void test_it_adds_a_column() {
			rowStructure.getPesa().addColumn("pesaid", _Column.INTEGER, 5);
			assertNotNull(rowStructure.getPesa().get("pesaid"));
		}
		
		public void test_it_doesnt_accept_invalid_names() {
			try {
				rowStructure.getPesa().addColumn(null, _Column.INTEGER, 5);
				fail("should not accept null");
			} catch (Exception e) {
			}
			try {
				rowStructure.getPesa().addColumn("", _Column.INTEGER, 5);
				fail("should not accept empty string");
			} catch (Exception e) {
			}
			try {
				rowStructure.getPesa().addColumn("pesaid", _Column.INTEGER, 5);
				rowStructure.getPesa().addColumn("pesaid", _Column.INTEGER, 5);
				fail("should not let add column twice");
			} catch (Exception e) {
			}
		}
		
		public void test_the_types_of_the_column() {
			rowStructure.getPesa().addColumn("pesaid", _Column.INTEGER, 5);
			DatabaseColumnStructure c = rowStructure.getPesa().get("pesaid");
			assertEquals(_Column.NORMAL_CHANGEABLE_FIELD, c.getFieldType());
		}
		
		public void test_the_types_of_the_column2() {
			rowStructure.getPesa().addColumn("pesaid", _Column.INTEGER, 5);
			DatabaseColumnStructure c = rowStructure.getPesa().get("pesaid");
			c.setTypeToUniqueNumericIncreasingId();
			assertEquals(_Column.UNIQUE_NUMERIC_INCREASING_ID, c.getFieldType());
		}
		
		public void test_the_types_of_the_column3() {
			rowStructure.getPesa().addColumn("pesaid", _Column.INTEGER, 5);
			DatabaseColumnStructure c = rowStructure.getPesa().get("pesaid");
			c.setTypeToImmutablePartOfAKey();
			assertEquals(_Column.IMMUTABLE_PART_OF_A_KEY, c.getFieldType());
		}
		
		public void test_it_accepts_only_one_size_for_numeric_type() {
			try {
				rowStructure.getPesa().addColumn("pesaid", _Column.INTEGER, 1, 2);
				fail("number datatype has to require one size");
			} catch (Exception e) {
			}
		}
		
		public void test_it_accepts_only_one_size_for_varchar_type() {
			try {
				rowStructure.getPesa().addColumn("pesanimi", _Column.VARCHAR, 1, 2);
				fail("varchar datatype has to require one size");
			} catch (Exception e) {
			}
		}
		
		public void test_it_accepts_only_tywo_sizes_for_decimal_type() {
			try {
				rowStructure.getPesa().addColumn("AAAA", _Column.DECIMAL, 1);
				fail("decimal has to require two sizes");
			} catch (Exception e) {
			}
			
			try {
				rowStructure.getPesa().addColumn("AAAA", _Column.DECIMAL, 1, 3, 4);
				fail("decimal has to require two sizes");
			} catch (Exception e) {
			}
			try {
				rowStructure.getPesa().addColumn("AAAA", _Column.DECIMAL, 1, 2);
			} catch (Exception e) {
				fail("Should accept");
			}
		}
		
		public void test_it_accepts_no_size_for_date() {
			try {
				rowStructure.getPesa().addColumn("AAAA", _Column.DATE);
			} catch (Exception e) {
				fail("Should accept");
			}
		}
		
		public void test_it_throws_exception_if_trying_to_define_idcolumn_directly() {
			rowStructure.getPesa().addColumn("a", _Column.INTEGER, 5);
			try {
				rowStructure.getPesa().setIdColumn("a");
				fail("Should throw exception");
			} catch (IllegalStateException e) {
			}
		}
		
		public void test_it_can_have_only_one_uniqueId() {
			rowStructure.getPesa().addColumn("a", _Column.INTEGER, 5);
			rowStructure.getPesa().addColumn("b", _Column.INTEGER, 5);
			rowStructure.getPesa().get("a").setTypeToUniqueNumericIncreasingId();
			assertEquals(_Column.UNIQUE_NUMERIC_INCREASING_ID, rowStructure.getPesa().get("a").getFieldType());
			try {
				rowStructure.getPesa().get("b").setTypeToUniqueNumericIncreasingId();
				fail("Should throw exception");
			} catch (Exception e) {
			}
			try {
				rowStructure.getPesa().get("a").setTypeToUniqueNumericIncreasingId();
			} catch (Exception e) {
				fail("Should allow to set the same column to unique id twice");
			}
		}
		
		public void test__if_table_has_unique_numeric_id_one_cant_define_other_keys_to_it() {
			rowStructure.getPesa().addColumn("a", _Column.INTEGER, 5);
			rowStructure.getPesa().addColumn("b", _Column.VARCHAR, 20);
			rowStructure.getPesa().get("a").setTypeToUniqueNumericIncreasingId();
			try {
				rowStructure.getPesa().get("b").setTypeToImmutablePartOfAKey();
				fail("Should throw exception");
			} catch (Exception e) {
			}
			
		}
		
		public void test__if_table_has_immutable_keys_defined_one_cant_define_unique_id() {
			rowStructure.getPesa().addColumn("a", _Column.INTEGER, 5);
			rowStructure.getPesa().addColumn("b", _Column.VARCHAR, 20);
			rowStructure.getPesa().get("b").setTypeToImmutablePartOfAKey();
			
			try {
				rowStructure.getPesa().get("a").setTypeToUniqueNumericIncreasingId();
				fail("Should throw exception");
			} catch (Exception e) {
			}
		}
		
		public void test_it_throws_exception_if_trying_to_define_immutable_keys_directly() {
			rowStructure.getPesa().addColumn("a", _Column.INTEGER, 5);
			try {
				rowStructure.getPesa().addToImmutableKeys("a");
				fail("Should throw exception");
			} catch (IllegalStateException e) {
			}
		}
		
		public void test_it_also_removes_columns() {
			rowStructure.getPesa().addColumn("a", _Column.VARCHAR, 10);
			rowStructure.getPesa().addColumn("b", _Column.VARCHAR, 10);
			rowStructure.getPesa().addColumn("c", _Column.VARCHAR, 10);
			rowStructure.getPesa().addColumn("d", _Column.VARCHAR, 10);
			String[] names = {
			                  "rowid", "a", "b", "c", "d"
			};
			int i = 0;
			for (DatabaseColumnStructure c : rowStructure.getPesa().getColumns()) {
				assertEquals(names[i++], c.getName());
			}
			rowStructure.getPesa().removeColumn("a");
			rowStructure.getPesa().removeColumn("c");
			String[] names_after = {
			                        "rowid", "b", "d"
			};
			i = 0;
			for (DatabaseColumnStructure c : rowStructure.getPesa().getColumns()) {
				assertEquals(names_after[i++], c.getName());
			}
		}
		
		public void test__reordering_columns() {
			DatabaseTableStructure pesa = rowStructure.getPesa();
			pesa.addColumn("a", _Column.VARCHAR, 10);
			pesa.addColumn("b", _Column.VARCHAR, 10);
			pesa.addColumn("c", _Column.VARCHAR, 10);
			pesa.addColumn("d", _Column.VARCHAR, 10);
			
			checkOrder(pesa, "rowid a b c d ");
			
			pesa.moveAsFirst(pesa.get("d"));
			checkOrder(pesa, "rowid d a b c ");
			
			pesa.moveAsFirst(pesa.get("a"));
			checkOrder(pesa, "rowid a d b c ");
			
			List<String> order = new ArrayList<>();
			order.add("c");
			order.add("b");
			order.add("a");
			pesa.reorder(order);
			checkOrder(pesa, "rowid c b a d ");
		}
		
		private void checkOrder(DatabaseTableStructure pesa, String expected) {
			String names = "";
			for (DatabaseColumnStructure c : pesa.getColumns()) {
				names += c.getName() + " ";
			}
			assertEquals(expected, names);
		}
		
	}
	
	public static class PoikanenEntries extends TestCase {
		
		private _PesaDomainModelRow	row;
		
		@Override
		protected void setUp() {
			PesaDomainModelDatabaseStructure rowStructure;
			rowStructure = new PesaDomainModelDatabaseStructure();
			DatabaseTableStructure poikanen = new DatabaseTableStructure("poikanen");
			poikanen.addColumn("id", _Column.INTEGER, 5);
			poikanen.get("id").setTypeToUniqueNumericIncreasingId();
			rowStructure.setPoikanen(poikanen);
			
			row = new PesaDomainModelRow(rowStructure);
			row.getColumn("poikanen.1.id").setValue("5");
			row.getColumn("poikanen.2.id").setValue("6");
		}
		
		public void test_adding_poikanen_entries() {
			assertEquals("poikanen", row.getPoikaset().get(1).getName());
			assertEquals("id", row.getPoikaset().get(1).get("id").getName());
			assertEquals("poikanen.1.id", row.getPoikaset().get(1).get("id").getFullname());
			assertEquals("poikanen.2.id", row.getPoikaset().get(2).get("id").getFullname());
			
			assertEquals("5", row.getColumn("poikanen.1.id").getValue());
			assertEquals("6", row.getColumn("poikanen.2.id").getValue());
			
			assertEquals("5", row.getPoikaset().get(1).get("id").getValue());
			assertEquals("6", row.getPoikaset().get(2).get("id").getValue());
		}
		
		public void test_listing_all_tables_includes_poikanen_entries() {
			String values = "";
			for (_Table t : row) {
				for (_Column c : t) {
					values += c.getValue();
				}
			}
			assertEquals("56", values);
		}
		
		public void test_clearing_all_clears_poikanen_values() {
			row.clearAllValues();
			String values = "";
			for (_Table t : row) {
				for (_Column c : t) {
					values += c.getValue();
				}
			}
			assertEquals("", values);
		}
	}
	
	public static class ListingAllColumns extends TestCase {
		
		private PesaDomainModelDatabaseStructure	rowStructure;
		
		public void test_it_lists_columns() {
			rowStructure = new PesaDomainModelDatabaseStructure();
			rowStructure.setPesa(new DatabaseTableStructure("pesavakio"));
			rowStructure.getPesa().addColumn("pesaid", _Column.INTEGER, 5);
			rowStructure.getPesa().addColumn("pesanimi", _Column.VARCHAR, 20);
			String s = "";
			for (DatabaseColumnStructure c : rowStructure.getPesa().getColumns()) {
				s += c.getName();
			}
			assertEquals("rowidpesaidpesanimi", s);
		}
		
	}
	
	public static class SettingAndGettingValues extends TestCase {
		
		private static final String	THIS_VALUE_EXEEDS_20_CHARACTERS	= "THIS VALUE EXEEDS 20 CHARACTERS. THIS VALUE EXEEDS 20 CHARACTERS.";
		private _PesaDomainModelRow				row;
		private _Table				table;
		
		@Override
		protected void setUp() {
			PesaDomainModelDatabaseStructure rowStructure = new PesaDomainModelDatabaseStructure();
			rowStructure.setPesa(new DatabaseTableStructure("pesavakio"));
			rowStructure.getPesa().addColumn("pesaid", _Column.INTEGER, 5);
			rowStructure.getPesa().addColumn("des", _Column.DECIMAL, 5, 2);
			rowStructure.getPesa().addColumn("nimi", _Column.VARCHAR, 25);
			
			row = new PesaDomainModelRow(rowStructure);
			row.getPesa().get("pesaid").setValue("4");
			row.getPesa().get(Const.ROWID).setValue("AAA");
			
			table = new Table(rowStructure.getPesa());
		}
		
		public void test_it_sets_the_value() {
			assertEquals("4", row.getPesa().get("pesaid").getValue());
			assertEquals("AAA", row.getPesa().get(Const.ROWID).getValue());
		}
		
		public void test_toString_returns_value() {
			assertEquals("4", row.getPesa().get("pesaid").toString());
			assertEquals("AAA", row.getPesa().get(Const.ROWID).toString());
		}
		
		public void test_rowid_shortcut_methods() {
			assertEquals("AAA", row.getPesa().getRowidValue());
			row.getPesa().setRowidValue("BBB");
			assertEquals("BBB", row.getPesa().getRowidValue());
		}
		
		public void test_it_returns_front_zero_and_commas_for_decimals() {
			_Column des = table.get("des");
			
			des.setValue("5,5");
			assertEquals("5,50", des.getValue());
			des.setValue("53.5");
			assertEquals("53,50", des.getValue());
			
			des.setValue(".5");
			assertEquals("0,50", des.getValue());
			
			des.setValue(",,..,,");
			assertEquals(",,,,,,", des.getValue());
			
			des.setValue("");
			assertEquals("", des.getValue());
			
			des.setValue("10,7777");
			assertEquals("10,78", des.getValue());
		}
		
		public void test__setting_too_long_value_doesnt_throw_exceptions_or_cut_text_etc_____validation_will_alert__which_is_not_tested_here() {
			table.get("nimi").setValue(THIS_VALUE_EXEEDS_20_CHARACTERS);
			assertEquals(THIS_VALUE_EXEEDS_20_CHARACTERS, table.get("nimi").getValue());
		}
		
		public void test__getting_url_encoded_value() {
			table.get("nimi").setValue("Pesä 1");
			assertEquals("Pes%C3%A4+1", table.get("nimi").getUrlEncodedValue());
		}
		
	}
	
	public static class UseExample extends TestCase {
		
		public void test___use_example() {
			PesaDomainModelDatabaseStructure rowStructure = new PesaDomainModelDatabaseStructure();
			rowStructure.setPesa(new DatabaseTableStructure("pesavakio"));
			rowStructure.getPesa().addColumn("pesaid", _Column.INTEGER, 5);
			rowStructure.getPesa().get("pesaid").setTypeToUniqueNumericIncreasingId();
			
			_PesaDomainModelRow row = new PesaDomainModelRow(rowStructure);
			
			_Table pesa = row.getPesa();
			String kys = "select max(" + pesa.getUniqueNumericIdColumn().getName() + ")+1 from " + pesa.getName();
			String exp = "select max(pesaid)+1 from pesavakio";
			assertEquals(exp, kys);
		}
	}
	
	public static class InputTypes_InputSizes_Modifiability extends TestCase {
		
		private _PesaDomainModelRow	row;
		
		@Override
		protected void setUp() throws Exception {
			PesaDomainModelDatabaseStructure rowStructure = new PesaDomainModelDatabaseStructure();
			DatabaseTableStructure laji = new DatabaseTableStructure("laji");
			laji.addColumn("id", Column.VARCHAR, 2);
			laji.addColumn("nimi", Column.VARCHAR, 20);
			laji.addColumn("kommentti", Column.VARCHAR, 250);
			laji.addColumn("minimipaino", Column.DECIMAL, 3, 2);
			laji.addColumn("numero", Column.INTEGER, 5);
			laji.addColumn("kotikunta", Column.VARCHAR, 6);
			laji.get("kotikunta").setAssosiatedSelection("kunnat");
			laji.get("id").setTypeToImmutablePartOfAKey();
			rowStructure.setKaynti(laji);
			
			DatabaseTableStructure pesa = new DatabaseTableStructure("pesa");
			pesa.addColumn("pesaid", _Column.INTEGER, 5);
			pesa.addColumn("nimi", _Column.VARCHAR, 25);
			pesa.addColumn("kirj_pvm", _Column.DATE);
			pesa.addColumn("muutos_pvm", _Column.DATE);
			pesa.addColumn("loyto_pvm", _Column.DATE);
			pesa.get("pesaid").setTypeToUniqueNumericIncreasingId();
			pesa.get("kirj_pvm").setTypeToDateAddedColumn();
			pesa.get("muutos_pvm").setTypeToDateModifiedColumn();
			rowStructure.setPesa(pesa);
			
			row = new PesaDomainModelRow(rowStructure);
		}
		
		public void test_it_sets_type_to_text_for_short_varchar_fields() throws Exception {
			assertEquals(_Column.INPUT_TYPE_TEXT, row.getKaynti().get("nimi").getInputType());
		}
		
		public void test_it_sets_type_to_textarea_for_long_varchar_fields() throws Exception {
			assertEquals(_Column.INPUT_TYPE_TEXTAREA, row.getKaynti().get("kommentti").getInputType());
		}
		
		public void test_it_sets_type_to_text_for_decimal_fields() throws Exception {
			assertEquals(_Column.INPUT_TYPE_TEXT, row.getKaynti().get("minimipaino").getInputType());
		}
		
		public void test_it_sets_type_to_text_for_number_fields() throws Exception {
			assertEquals(_Column.INPUT_TYPE_TEXT, row.getKaynti().get("numero").getInputType());
		}
		
		public void test_it_sets_type_to_selection_if_assosiatedSelectionSet() throws Exception {
			assertEquals(_Column.INPUT_TYPE_SELECTION, row.getKaynti().get("kotikunta").getInputType());
		}
		
		public void test_it_sets_input_type_for_dates_as_date() throws Exception {
			assertEquals(_Column.INPUT_TYPE_DATE, row.getPesa().get("kirj_pvm").getInputType());
			assertEquals(_Column.INPUT_TYPE_DATE, row.getPesa().get("muutos_pvm").getInputType());
			assertEquals(_Column.INPUT_TYPE_DATE, row.getPesa().get("loyto_pvm").getInputType());
		}
		
		public void test_it_sets_sizes() throws Exception {
			assertEquals(20, row.getKaynti().get("nimi").getInputSize());
			assertEquals(250, row.getKaynti().get("kommentti").getInputSize());
			assertEquals(3 + 1 + 2, row.getKaynti().get("minimipaino").getInputSize());
			assertEquals(1, row.getPesa().get("kirj_pvm").getInputSize());
		}
		
		public void test_it_sets_keys_as_not_modifiable_others_as_modifiable() throws Exception {
			assertEquals(_Column.MODIFIABLE_YES, row.getKaynti().get("kommentti").getModifiability());
			assertEquals(_Column.MODIFIABLE_NO, row.getKaynti().get("id").getModifiability());
			assertEquals(_Column.MODIFIABLE_NO, row.getPesa().get("pesaid").getModifiability());
		}
		
		public void test_it_sets_modified_and_added_dates_as_unmodifiable() throws Exception {
			assertEquals(_Column.MODIFIABLE_NO, row.getPesa().get("kirj_pvm").getModifiability());
			assertEquals(_Column.MODIFIABLE_NO, row.getPesa().get("muutos_pvm").getModifiability());
			assertEquals(_Column.MODIFIABLE_YES, row.getPesa().get("loyto_pvm").getModifiability());
		}
		
		public void test_it_returns_immutable_varhcarkeys_in_uppercase() {
			row.getKaynti().get("id").setValue("aa");
			row.getKaynti().get("nimi").setValue("nnnn");
			assertEquals("AA", row.getKaynti().get("id").getValue());
			assertEquals("nnnn", row.getKaynti().get("nimi").getValue());
		}
		
	}
	
	public static class ToUppercaseColumns extends TestCase {
		
		@Override
		protected void setUp() {
			
		}
		
		public void test_it_returns_column_values_of_columns_that_are_defined_to_be_upperacseolumn_in_uppercase() {
			DatabaseTableStructure tableStructure = new DatabaseTableStructure("table");
			tableStructure.addColumn("column", _Column.VARCHAR, 25);
			tableStructure.get("column").setToUppercaseColumn();
			
			_Table table = new Table(tableStructure);
			table.get("column").setValue("asd");
			assertEquals("ASD", table.get("column").getValue());
		}
		
	}
	
	public static class ContainsValues extends TestCase {
		
		public void test_table_containsvalues() {
			DatabaseTableStructure tableStructure = new DatabaseTableStructure("T");
			tableStructure.addColumn("a", _Column.VARCHAR, 5);
			tableStructure.addColumn("b", _Column.INTEGER, 5);
			tableStructure.addColumn("c", _Column.DECIMAL, 5, 2);
			tableStructure.addColumn("d", _Column.DATE);
			
			_Table table = new Table(tableStructure);
			
			assertFalse("should not have values", table.hasValues());
			table.get("a").setValue("a");
			assertTrue("should have values", table.hasValues());
			table.clearAllValues();
			assertFalse("should not have values", table.hasValues());
			table.get("d").setValue("1.1.2005");
			assertTrue("should have values", table.hasValues());
			table.clearAllValues();
			assertFalse("should not have values", table.hasValues());
			table.setRowidValue("AAA");
			assertTrue("should have values", table.hasValues());
		}
		
	}
	
	public static class ColumnByFullNameFromTables extends TestCase {
		
		public void test_it_gets_column_by_fullname_from_tablestructure() {
			DatabaseTableStructure tableStructure = new DatabaseTableStructure("t");
			tableStructure.addColumn("a", _Column.VARCHAR, 5);
			PesaDomainModelDatabaseStructure structure = new PesaDomainModelDatabaseStructure();
			structure.setPesa(tableStructure);
			structure.getColumn("t.a");
			
			tableStructure = new DatabaseTableStructure("poikanen.1");
			tableStructure.addColumn("a", _Column.VARCHAR, 5);
			structure = new PesaDomainModelDatabaseStructure();
			structure.setPesa(tableStructure);
			structure.getColumn("poikanen.1.a");
		}
		
		public void test_it_gets_column_by_fullname_from_row() {
			DatabaseTableStructure tableStructure = new DatabaseTableStructure("t");
			tableStructure.addColumn("a", _Column.VARCHAR, 5);
			PesaDomainModelDatabaseStructure structure = new PesaDomainModelDatabaseStructure();
			structure.setPesa(tableStructure);
			
			_PesaDomainModelRow row = new PesaDomainModelRow(structure);
			row.getColumn("t.a");
			
			tableStructure = new DatabaseTableStructure("poikanen.1");
			tableStructure.addColumn("a", _Column.VARCHAR, 5);
			structure = new PesaDomainModelDatabaseStructure();
			structure.setPesa(tableStructure);
			
			row = new PesaDomainModelRow(structure);
			row.getColumn("poikanen.1.a");
		}
		
	}
	
	public static class RemovingWhitespaceFromIntegerAndDecimalValues___and_removing_dublicate_whitespaces extends TestCase {
		
		public void test_it_removes_whitespace() {
			DatabaseTableStructure tableStructure = new DatabaseTableStructure("t");
			tableStructure.addColumn("i", _Column.INTEGER, 5);
			tableStructure.addColumn("d", _Column.DECIMAL, 5, 2);
			tableStructure.addColumn("v", _Column.VARCHAR, 20);
			
			_Table table = new Table(tableStructure);
			_Column i = table.get("i");
			_Column d = table.get("d");
			_Column v = table.get("v");
			
			i.setValue("1  ");
			assertEquals("1", i.getValue());
			i.setValue("  1 3434 ad  ");
			assertEquals("13434ad", i.getValue());
			i.setValue("5 - 10");
			assertEquals("5-10", i.getValue());
			i.setValue("-5 - -1");
			assertEquals("-5--1", i.getValue());
			
			d.setValue("1  ");
			assertEquals("1,00", d.getValue());
			d.setValue(" 1, 03");
			assertEquals("1,03", d.getValue());
			d.setValue(" 1, 03 52 2 ");
			assertEquals("1,04", d.getValue());
			
			v.setValue(" asd d ");
			assertEquals("asd d", v.getValue());
			
			v.setValue(" asd        d     ");
			assertEquals("asd d", v.getValue());
		}
		
	}
	
	public static class Test__CopyValues extends TestCase {
		
		private static final String	DATE_MODIFIED	= "date_modified";
		private static final String	DATE_ADDED		= "date_added";
		private static final String	ID				= "id";
		private static final String	VARCHAR_COL		= "varchar_col";
		private static final String	INTEGER_COL		= "integer_col";
		private _Table				original;
		private _Table				copy;
		
		@Override
		protected void setUp() {
			original = makeTable();
			copy = makeTable();
		}
		
		private Table makeTable() {
			DatabaseTableStructure tableStructure = new DatabaseTableStructure("testable");
			tableStructure.addColumn(ID, _Column.INTEGER, 5);
			tableStructure.get(ID).setTypeToUniqueNumericIncreasingId();
			tableStructure.addColumn(DATE_ADDED, _Column.DATE);
			tableStructure.get(DATE_ADDED).setTypeToDateAddedColumn();
			tableStructure.addColumn(DATE_MODIFIED, _Column.DATE);
			tableStructure.get(DATE_MODIFIED).setTypeToDateModifiedColumn();
			tableStructure.addColumn(INTEGER_COL, _Column.INTEGER, 5);
			tableStructure.addColumn(VARCHAR_COL, _Column.VARCHAR, 5);
			return new Table(tableStructure);
		}
		
		public void test__() throws Exception {
			original.get(VARCHAR_COL).setValue("huuhaa");
			original.get(ID).setValue("111");
			original.setRowidValue("rowid");
			copy.setValues(original);
			assertEquals("huuhaa", copy.get(VARCHAR_COL).getValue());
			assertEquals("111", copy.get(ID).getValue());
			assertEquals("", copy.getRowidValue());
		}
	}
	
	public static class Test__Equals_and_HasValues extends TestCase {
		
		private static final String	DATE_MODIFIED	= "date_modified";
		private static final String	DATE_ADDED		= "date_added";
		private static final String	ID				= "id";
		private static final String	VARCHAR_COL		= "varchar_col";
		private static final String	INTEGER_COL		= "integer_col";
		private _Table				table;
		
		@Override
		protected void setUp() {
			table = makeTable();
		}
		
		private Table makeTable() {
			DatabaseTableStructure tableStructure = new DatabaseTableStructure("testable");
			tableStructure.addColumn(ID, _Column.INTEGER, 5);
			tableStructure.get(ID).setTypeToUniqueNumericIncreasingId();
			tableStructure.addColumn(DATE_ADDED, _Column.DATE);
			tableStructure.get(DATE_ADDED).setTypeToDateAddedColumn();
			tableStructure.addColumn(DATE_MODIFIED, _Column.DATE);
			tableStructure.get(DATE_MODIFIED).setTypeToDateModifiedColumn();
			tableStructure.addColumn(INTEGER_COL, _Column.INTEGER, 5);
			tableStructure.addColumn(VARCHAR_COL, _Column.VARCHAR, 5);
			return new Table(tableStructure);
		}
		
		public void test__() throws Exception {
			assertTrue(table.equalsIgnoreMetadata(table));
		}
		
		@SuppressWarnings("unlikely-arg-type")
		public void test__2_1() throws Exception {
			DatabaseTableStructure otherTableStructure = new DatabaseTableStructure("foo");
			otherTableStructure.addColumn(INTEGER_COL, _Column.INTEGER, 5);
			try {
				table.equals(otherTableStructure);
				fail("Should have thrown exception");
			} catch (IllegalArgumentException e) {
			}
		}
		
		public void test__2_2() throws Exception {
			DatabaseTableStructure otherTableStructure = new DatabaseTableStructure("foo");
			otherTableStructure.addColumn(INTEGER_COL, _Column.INTEGER, 5);
			Table otherTable = new Table(otherTableStructure);
			try {
				table.equalsIgnoreMetadata(otherTable);
				fail("Should have thrown exception");
			} catch (IllegalArgumentException e) {
			}
		}
		
		public void test__3() throws Exception {
			Table otherTable = makeTable();
			assertTrue(table.equalsIgnoreMetadata(otherTable));
		}
		
		public void test__4() throws Exception {
			Table otherTable = makeTable();
			table.get(INTEGER_COL).setValue("1");
			otherTable.get(INTEGER_COL).setValue("2");
			assertFalse(table.equalsIgnoreMetadata(otherTable));
		}
		
		public void test__5() throws Exception {
			Table otherTable = makeTable();
			table.get(Const.ROWID).setValue("aaa");
			table.get(INTEGER_COL).setValue("1");
			otherTable.get(Const.ROWID).setValue("aaa");
			otherTable.get(INTEGER_COL).setValue("1");
			assertTrue(table.equalsIgnoreMetadata(otherTable));
		}
		
		public void test__it_ignores_rowid() throws Exception {
			Table otherTable = makeTable();
			table.get(Const.ROWID).setValue("aaa");
			assertTrue(table.equalsIgnoreMetadata(otherTable));
		}
		
		public void test__it_ignores_ids() throws Exception {
			Table otherTable = makeTable();
			table.get(ID).setValue("34343");
			assertTrue(table.equalsIgnoreMetadata(otherTable));
		}
		
		public void test__it_ignores_added_modified__dates() throws Exception {
			Table otherTable = makeTable();
			table.get(DATE_ADDED).setValue("1.1.1");
			table.get(DATE_MODIFIED).setValue("1.2.3");
			assertTrue(table.equalsIgnoreMetadata(otherTable));
		}
		
		public void test___hasValues___() {
			Table t = makeTable();
			assertEquals("Should not have values", false, t.hasValues());
			assertEquals("Should not have values", false, t.hasValuesIgnoreMetadata());
		}
		
		public void test___hasValues___1() throws Exception {
			Table t = makeTable();
			t.setRowidValue("foo");
			t.getUniqueNumericIdColumn().setValue("foo");
			assertEquals("Should have values", true, t.hasValues());
			assertEquals("Should not have values", false, t.hasValuesIgnoreMetadata());
		}
		
		public void test___hasValues___2() throws Exception {
			Table t = makeTable();
			t.setRowidValue("foo");
			t.getUniqueNumericIdColumn().setValue("foo");
			t.get(VARCHAR_COL).setValue("foo");
			t.get(INTEGER_COL).setValue("123123");
			assertEquals("Should have values", true, t.hasValues());
			assertEquals("Should have values", true, t.hasValuesIgnoreMetadata());
		}
		
		public void test___hasValues___3() throws Exception {
			Table t = makeTable();
			t.setRowidValue("foo");
			t.getUniqueNumericIdColumn().setValue("foo");
			t.get(VARCHAR_COL).setValue("foo");
			t.get(INTEGER_COL).setValue("123123");
			assertEquals("Should have values", true, t.hasValues());
			assertEquals("Should have values", true, t.hasValuesIgnoreMetadata(VARCHAR_COL));
			assertEquals("Should not have values", false, t.hasValuesIgnoreMetadata(VARCHAR_COL, INTEGER_COL));
		}
		
		public void test__throws_exception_if_ignored_columns_dont_exists() {
			Table t = makeTable();
			try {
				t.hasValuesIgnoreMetadata("foobar");
				fail("Should have thrown exception");
			} catch (IllegalArgumentException e) {
				// ok
			}
			try {
				t.equalsIgnoreMetadata(makeTable(), "foobar");
				fail("Should have thrown exception");
			} catch (IllegalArgumentException e) {
				// ok
			}
		}
		
	}
	
	public static class WorkingWithReferencekeys extends TestCase {
		
		public void test___() {
			DatabaseTableStructure table1 = new DatabaseTableStructure("t1");
			DatabaseTableStructure table2 = new DatabaseTableStructure("t2");
			
			table1.addColumn("id", _Column.INTEGER, 5).setTypeToUniqueNumericIncreasingId();
			table2.addColumn("id", _Column.INTEGER, 5).setTypeToUniqueNumericIncreasingId();
			DatabaseColumnStructure c = table2.addColumn("refto_t1", _Column.INTEGER, 5).references(table1.getUniqueNumericIdColumn());
			assertEquals("t1.id", c.getReferenceColumn().toString());
			assertEquals("t1", c.getReferenceColumn().table());
			assertEquals("id", c.getReferenceColumn().column());
			
			DatabaseColumnStructure t1Id = table1.getUniqueNumericIdColumn();
			assertEquals(null, t1Id.getReferenceColumn());
			assertEquals(1, t1Id.getReferencingColumns().size());
			_ReferenceKey key = t1Id.getReferencingColumns().iterator().next();
			assertEquals("t2.refto_t1", key.toString());
			assertEquals("t2", key.table());
			assertEquals("refto_t1", key.column());
		}
	}
}
