package fi.hy.pese.framework.main.db;

import java.sql.SQLException;

/**
 * This result handler can be used to check if the search produced any results
 */
public class RowExistsResultHandler implements _ResultsHandler {

	/**
	 * After giving this result handler to a search query, value is true if at least one row existed, false if no matching rows were found
	 */
	public boolean	rowExists;

	@Override
	public void process(_ResultSet rs) throws SQLException {
		rowExists = rs.next();
	}

}
