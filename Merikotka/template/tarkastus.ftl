<#include "macro.ftl">

<#macro separator text >
  <tr><td colspan="2"><b>${ text }</b></td></tr>
</#macro>

<#macro tr_selection selections prefix column size multiple firstEmpty showValue errors >
 <tr class="kentta">
   <td>${texts["tarkastuslomake."+column.fullname]}:</td>
   <td> 
     <@selection selections prefix column size multiple firstEmpty showValue errors />
     <@compare column /> 
   </td>
 </tr>
</#macro>

<#macro tr_selectionDESC desc selections prefix column size multiple firstEmpty showValue errors >
 <tr class="kentta">
   <td>${desc}:</td>
   <td> <@selection selections prefix column size multiple firstEmpty showValue errors /> <@compare column />  </td>
 </tr>
</#macro>

<#macro tr_inputText prefix column sized errors > 
 <tr class="kentta">
   <td>${texts["tarkastuslomake."+column.fullname]}:</td>
   <td> <@inputText prefix column sized errors /> <@compare column /> </td>
 </tr>
</#macro>

<#macro tr_textarea prefix column rows cols errors > 
 <tr class="kentta">
   <td colspan="2"> 
     ${texts["tarkastuslomake."+column.fullname]}:
     <p align="center"><@textarea prefix column rows cols errors /> </p> 
     <p> <@compare column /> </p>
   </td>
 </tr>
</#macro>

<#macro tr_date prefix column errors >
 <tr class="kentta">
   <td>${texts["tarkastuslomake."+column.fullname]}:</td>
   <td class="paivays"> <@date prefix column errors /> <@compare column /> </td>
 </tr>
</#macro>

<#macro tr_radio prefix column value errors >
  <tr class="kentta">
    <td colspan="2" <@checkerror errors column.fullname /> >
      <@radio "updateparameters" column value errors />
      ${texts["tarkastuslomake."+column.fullname+"."+value]}
    </td>
  </tr>
</#macro>

<#macro tr_inputSelectCombo prefix column sized showValue errors > 
 <tr class="kentta">
   <td>${texts["tarkastuslomake."+column.fullname]}:</td>
   <td> <@inputSelectCombo prefix column sized showValue errors /> <@compare column />  </td>
 </tr>
</#macro>

<#macro tr_poikanen_selection selections updateparameters columnName showValue errors second >
 <tr>
   <td class="kuvaus">${texts["tarkastuslomake.poikanen."+columnName]}:</td>
   <td> <@selection selections "updateparameters" updateparameters.getColumn("poikanen.1."+columnName) 1 false true showValue errors /> </td>
   <td> <@selection selections "updateparameters" updateparameters.getColumn("poikanen.2."+columnName) 1 false true showValue errors /> </td>
   <td> <@selection selections "updateparameters" updateparameters.getColumn("poikanen.3."+columnName) 1 false true showValue errors /> </td>
   <td> <@selection selections "updateparameters" updateparameters.getColumn("poikanen.4."+columnName) 1 false true showValue errors /> </td>
   <#if second>
     <td> <@selection selections "updateparameters" updateparameters.getColumn("poikanen.1."+columnName+"_2") 1 false true showValue errors /> </td>
     <td> <@selection selections "updateparameters" updateparameters.getColumn("poikanen.2."+columnName+"_2") 1 false true showValue errors /> </td>
     <td> <@selection selections "updateparameters" updateparameters.getColumn("poikanen.3."+columnName+"_2") 1 false true showValue errors /> </td>
     <td> <@selection selections "updateparameters" updateparameters.getColumn("poikanen.4."+columnName+"_2") 1 false true showValue errors /> </td>
   <#else>
      <td class="tyhja"> </td> 
      <td class="tyhja"> </td> 
      <td class="tyhja"> </td>
      <td class="tyhja"> </td>
   </#if>
 </tr>
</#macro>
 
<#macro tr_poikanen_inputText updateparameters columnName errors second sized> 
 <tr>
   <td class="kuvaus">${texts["tarkastuslomake.poikanen."+columnName]}:</td>
   <td> <@inputText "updateparameters" updateparameters.getColumn("poikanen.1."+columnName) sized errors /> </td>
   <td> <@inputText "updateparameters" updateparameters.getColumn("poikanen.2."+columnName) sized errors /> </td>
   <td> <@inputText "updateparameters" updateparameters.getColumn("poikanen.3."+columnName) sized errors /> </td>
   <td> <@inputText "updateparameters" updateparameters.getColumn("poikanen.4."+columnName) sized errors /> </td>
   <#if second>
     <td> <@inputText "updateparameters" updateparameters.getColumn("poikanen.1."+columnName+"_2") sized errors /> </td>
     <td> <@inputText "updateparameters" updateparameters.getColumn("poikanen.2."+columnName+"_2") sized errors /> </td>
     <td> <@inputText "updateparameters" updateparameters.getColumn("poikanen.3."+columnName+"_2") sized errors /> </td>
     <td> <@inputText "updateparameters" updateparameters.getColumn("poikanen.4."+columnName+"_2") sized errors /> </td>
   <#else>
      <td class="tyhja"> </td> 
      <td class="tyhja"> </td> 
      <td class="tyhja"> </td>
      <td class="tyhja"> </td>
   </#if>
 </tr>
</#macro>
  



<#include "navi.ftl">

<div class="tarkastus"> 

<#if (data.kirjekyyhky!"") == "yes">
  <h1>${texts["tarkastus_title_kirjekyyhky_"+system.action]}</h1>
<#else>
  <h1>${texts["tarkastus_title_"+system.action]}</h1>
</#if>




<table>
  <tr>
	<#if results?has_content>
  		<td>
    		<form action="${system.baseURL}" method="get" name="tarkastusSelector">
       		<input type="hidden" name="page"   value="${system.page}" />
       		<input type="hidden" name="action" value="fill_for_update" />
  			<table>
  			<tr>
      		<td>Aikaisemmat pesintävuodet:</td>
      		<td>
           		<select name="updateparameters.pesatarkastus.p_tarkastus_id">
           		<#list results as i> 
           			<#if i.tarkastus.p_tarkastus_id == updateparameters.tarkastus.p_tarkastus_id>
    	    			<option value="${i.tarkastus.p_tarkastus_id}" selected="selected">${i.tarkastus.tark_vuosi} - ${i.tarkastus.pesimistulos}</option>
	      			<#else>
	        			<option value="${i.tarkastus.p_tarkastus_id}">${i.tarkastus.tark_vuosi} - ${i.tarkastus.pesimistulos}</option>
      				</#if>
		    	</#list>
            	</select>
          	</td>
          	<td><a class="button-link" href="#" onclick="document.forms.tarkastusSelector.submit();">Tarkastele</a></td>
        	</tr>
        	</table>
        	</form>
    	</td>  	
	</#if> 
        
    <#if system.action == "update">
    	<td>
    		<form action="${system.baseURL}" method="post">
        	<input type="hidden" name="page" value="${system.page}" />
        	<input type="hidden" name="action" value="fill_for_insert" />
    		<input type="hidden" name="updateparameters.pesavakio.pesa_id" value="${updateparameters.pesa.pesa_id}" />
    		<table> 
	    	<tr>
        	<td>Uusi tarkastus pesintävuodelle: </td>
       		<td><input type="text" name="tark_vuosi" size="4" maxlength="4" value="${data.tark_vuosi!""}" <@checkerror errors "tark_vuosi" /> /></td> 
 	  		<td><input type="submit" value="Etene" /></td>
    		</tr>
    		</table>
	    	</form>
	    </td>
    </#if>
    
    <#if updateparameters.pesa.pesa_id != "" && !data.kirjekyyhky??>
      <td valign="top">
        <a target="_karttapaikka" href="https://asiointi.maanmittauslaitos.fi/karttapaikka/?share=customMarker&n=${updateparameters.pesa.eur_leveys}&e=${updateparameters.pesa.eur_pituus}&title=${updateparameters.pesa.pesanimi.getUrlEncodedValue()}(${updateparameters.pesa.pesa_id})">Lisää pesä kartalle</a>
      </td>
    </#if>
    
  </tr>
</table>





<br /><br />




<@errorlist errors texts />

<#if lists.ristiriidat??>
  <br />
  <b>Pesät joille on onnistunut tarkastus tälle vuodelle: &nbsp; &nbsp;</b> 
  <#list lists.ristiriidat as pesaid>
    <a style="color:red" href="${system.baseURL}?page=tarkastus&amp;action=fill_for_update&amp;updateparameters.pesavakio.pesa_id=${pesaid}&amp;updateparameters.pesatarkastus.tark_vuosi=${updateparameters.tarkastus.tark_vuosi}">&nbsp;${pesaid}&nbsp;</a>
     &nbsp;&nbsp;
  </#list>
</#if>

<@warninglist warnings texts />
<@statustexts system texts />


<div class="tarkastuslomake">

 <div class="kirjekyyhkyTools">
 <#if lists.log??>
   <table>
     <caption>Päivitysloki</caption>
     <tr>
       <th>Aika</th>
       <th>Käyttäjä</th>
       <th>Toiminto</th>
     </tr>
     <#list lists.log as e>
       <tr>
         <td>${e.time}</td>
         <td> <#if selections['ringers'].containsOption(e.user)>${selections['ringers'][e.user]}<#else>${e.user}</#if></td>
         <td>${e.action}</td>
       </tr>
     </#list>
   </table>
   <br />
 </#if>
 <#if (lists.message)?has_content>
   <table>
     <caption>Vanhat lähetetyt korjausviestit</caption>
     <#list lists.message as message>
       <tr>
         <td>${message}</td>
       </tr>
     </#list>
   </table>
   <br />
 </#if>
 
 <#if (data.kirjekyyhky!"") == "yes">
 <form action="${system.baseURL}" method="post" onsubmit="return false;" name="palautalomake">
   <input type="hidden" name="page" value="kirjekyyhky_management" />
   <input type="hidden" name="action" value="return_for_corrections" />
   <input type="hidden" name="id" value="${data.id!""}" />
   <table>
     <caption>Palauta lomake korjattavaksi</caption>
     <tr>
       <td style="vertical-align: middle;"> Viesti </td>
       <td> <textarea name="return_for_corrections_message" rows="2" cols="50"></textarea> </td>
       <td  style="vertical-align: middle;"> <button onclick="document.palautalomake.submit()">Palauta</button> </td>
     </tr>
   </table>
   </form>
 </#if>
</div>
 

<form action="${system.baseURL}" method="post" onsubmit="return false;" name="tarkastuslomake">
<input type="hidden" name="page" value="${system.page}" />
<input type="hidden" name="action" value="${system.action}" />
<input type="hidden" name="kirjekyyhky" value="${data.kirjekyyhky!""}" />
<input type="hidden" name="id" value="${data.id!""}" />
<input type="hidden" name="bypassed_warnings" id="bypassed_warnings" value="${data.bypassed_warnings!""}" /> 
<@inputHidden "updateparameters" updateparameters.vuosi.rowid />
<@inputHidden "updateparameters" updateparameters.pesa.rowid />
<@inputHidden "updateparameters" updateparameters.pesa.pesa_id />
<@inputHidden "updateparameters" updateparameters.olosuhde.rowid />
<@inputHidden "updateparameters" updateparameters.tarkastus.rowid />
<@inputHidden "updateparameters" updateparameters.tarkastus.p_tarkastus_id />
<@inputHidden "updateparameters" updateparameters.tarkastus.tark_vuosi />
<@inputHidden "updateparameters" updateparameters.getColumn("poikanen.1.rowid") />
<@inputHidden "updateparameters" updateparameters.getColumn("poikanen.2.rowid") />
<@inputHidden "updateparameters" updateparameters.getColumn("poikanen.3.rowid") />
<@inputHidden "updateparameters" updateparameters.getColumn("poikanen.4.rowid") />
<@inputHidden "updateparameters" updateparameters.getColumn("aikuinen.1.rowid") />
<@inputHidden "updateparameters" updateparameters.getColumn("aikuinen.2.rowid") />
<@inputHidden "updateparameters" updateparameters.getColumn("aikuinen.3.rowid") />
 
 

  <table>
    <tr>
      <td valign="middle">  
        <h2>
        <!-- Toiminnon otsikko -->
        <#if system.action == "insert">
          Uusi tarkastus: &nbsp;&nbsp; pesä: ${updateparameters.pesa.pesa_id} &nbsp;&nbsp; pesintävuosi: ${updateparameters.tarkastus.tark_vuosi}
        </#if>
        <#if system.action == "update">
          Tarkastuksen muokkaus: &nbsp;&nbsp; pesä: ${updateparameters.pesa.pesa_id} &nbsp;&nbsp; pesintävuosi: ${updateparameters.tarkastus.tark_vuosi}
        </#if>
        <#if system.action == "insert_first">
          Uusi pesä - pesintävuosi ${updateparameters.tarkastus.tark_vuosi}
        </#if>
        </h2>
      </td>
      <td align="right">
        <#if (data.kirjekyyhky!"") == "yes">
          <input type="submit" value="${texts["tarkastus_submit_kirjekyyhky_"+system.action]}" class="lahetanappi" onclick="document.tarkastuslomake.submit()"/>
        <#else>
  	      <input type="submit" value="${texts["tarkastus_submit_"+system.action]}" class="lahetanappi" onclick="document.tarkastuslomake.submit()"/>
  	    </#if>
      </td>
    </tr>
  </table>
   
 <#if (data.kirjekyyhky!"") == "yes">
  <div class="osiotaulu" style="width: 400px;">
    <div class="otsikko">Uusi reviiri</div>
    <table>
      <tr>
        <td>Oliko reviiri uusi: </td>
        <td> ${data["updateparameters.oliko_reviiri_uusi"]!""} <input type="hidden" name="updateparameters.oliko_reviiri_uusi" value="${data["updateparameters.oliko_reviiri_uusi"]!""}" /></td>
      </tr>
      <tr> 
        <td>(Uuden) reviirin nimi:</td>
        <td> ${data["updateparameters.reviirinimi"]!""} <input type="hidden" name="updateparameters.reviirinimi" value="${data["updateparameters.reviirinimi"]!""}" /></td>
      </tr>
      <tr>
        <td>(Uuden) reviirin pesät:</td>
        <td> ${data["updateparameters.uusi_reviiri_pesat"]!""} <input type="hidden" name="updateparameters.uusi_reviiri_pesat" value="${data["updateparameters.uusi_reviiri_pesat"]!""}" /></td>
      </tr>
    </table>
  </div>
 </#if>
 

                
                
  <div class="osiotaulu">
     <!-- PESäN PERUSTIEDOT JA REVIIRI -->
     <div class="otsikko">Pesän perustiedot</div>
     <table>
       <tr>
         <td>
            
          <table>
            <tr class="kentta">
              <td>Pesän id:</td>
               <td>${ (updateparameters.pesa.pesa_id) }</td>
            </tr>
              
            <@tr_inputText "updateparameters" updateparameters.pesa.pesanimi true errors />
                
           <tr class="kentta">
             <td>Pesän kirjauspvm:</td>
              <td>
                <#if updateparameters.pesa.kirj_pvm.dateValue.yyyy != "" >
                   ${updateparameters.pesa.kirj_pvm.dateValue.dd}.${updateparameters.pesa.kirj_pvm.dateValue.mm}.${updateparameters.pesa.kirj_pvm.dateValue.yyyy}
                </#if>
              </td>
            </tr>
                
            <tr class="kentta">
              <td>Pesän muutospvm:</td>
              <td>
                <#if updateparameters.pesa.muutos_pvm.dateValue.yyyy != "" >
                  ${updateparameters.pesa.muutos_pvm.dateValue.dd}.${updateparameters.pesa.muutos_pvm.dateValue.mm}.${updateparameters.pesa.muutos_pvm.dateValue.yyyy}
                </#if>
              </td>
            </tr>
          </table>
          
        </td>
        <td>
          <table> 
            <@tr_selectionDESC "Pesän kunta vuonna "+updateparameters.tarkastus.tark_vuosi     selections "updateparameters" updateparameters.vuosi.pesan_kunta 1 false true false errors />
            <#if (data.kirjekyyhky!"") == "yes" && updateparameters.vuosi.reviiri_id == "">
            	<@tr_selectionDESC "Pesän reviiri vuonna "+updateparameters.tarkastus.tark_vuosi   selections "updateparameters" comparisonparameters.vuosi.reviiri_id 1 false true false errors />
            	<@tr_selectionDESC "Reviirin kunta vuonna "+updateparameters.tarkastus.tark_vuosi  selections "updateparameters" updateparameters.vuosi.reviirin_kunta 1 false true false errors />
            <#else>
            	<@tr_selectionDESC "Pesän reviiri vuonna "+updateparameters.tarkastus.tark_vuosi   selections "updateparameters" updateparameters.vuosi.reviiri_id 1 false true false errors />
            	<@tr_selectionDESC "Reviirin kunta vuonna "+updateparameters.tarkastus.tark_vuosi  selections "updateparameters" updateparameters.vuosi.reviirin_kunta 1 false true false errors />
            </#if>
          </table>
        </td>
      </tr>
    </table>                               
  </div>
   
	<!-- TARKASTUKSEN TIEDOT -->
	<div class="osiotaulu" style="width: 50%; float: left;">
		<div class="otsikko">TARKASTUKSEN TIEDOT (${updateparameters.tarkastus.tark_vuosi})</div>
			<table>  
                <@tr_date "updateparameters" updateparameters.tarkastus.tark_pvm errors />        
                <@tr_selection selections "updateparameters" updateparameters.tarkastus.tark_pvm_tark 1 false false true errors />
                <@tr_inputText "updateparameters" updateparameters.tarkastus.tark_tunti true errors />
                <@tr_selection selections "updateparameters" updateparameters.tarkastus.tark_tapa 1 false true true errors />
                <@separator "" />
                <@tr_inputSelectCombo "updateparameters" updateparameters.tarkastus.tarkastaja1_id true false errors />
                <@tr_inputSelectCombo "updateparameters" updateparameters.tarkastus.tarkastaja2_id true false errors />
                <@separator "" />
                <@tr_inputSelectCombo "updateparameters" updateparameters.pesa.seur_tarkastaja true false errors />
                <@separator "" />
                                             
                <tr class="kentta">
                  <td>Tarkastuksen kirjauspvm:</td>
                  <td><#if updateparameters.tarkastus.kirj_pvm.dateValue.yyyy != "">${updateparameters.tarkastus.kirj_pvm.dateValue.dd}.${updateparameters.tarkastus.kirj_pvm.dateValue.mm}.${updateparameters.tarkastus.kirj_pvm.dateValue.yyyy}</#if></td>
                </tr>
                
                <tr class="kentta">
                  <td>Tarkastuksen muutospvm:</td>
                  <td><#if updateparameters.tarkastus.muutos_pvm.dateValue.yyyy != "">${updateparameters.tarkastus.muutos_pvm.dateValue.dd}.${updateparameters.tarkastus.muutos_pvm.dateValue.mm}.${updateparameters.tarkastus.muutos_pvm.dateValue.yyyy}</#if></td>
                </tr>
                
			</table>
	</div>
	
	<div class="osiotaulu" style="width: 40%; float: left;">
		<div class="otsikko">MUUT LOMAKKEEN VASTAANOTTAJAT </div>
		<div id="lomake_vastaanottajat">
			<div class="henkilot">
				<#list lists["lomake_vastaanottajat"] as vastaanottaja>
					<input name="lomake_vastaanottaja" value="${vastaanottaja}"/>
				</#list>
			</div>
			<button onclick="addVastaanottaja(); return false;"> + Lisää</button>
		</div>
	</div>
    <div class="clear"></div>
            
  <table>
    <tr>
      <td>
          <div class="osiotaulu">
                <!-- PESäN SIJAINTI -->
                <div class="otsikko">PESÄN SIJAINTI</div>
                
                <table>
                
                <@tr_inputText "updateparameters" updateparameters.pesa.tarkka_sijainti false errors />
                <@tr_inputText "updateparameters" updateparameters.pesa.vanha_pesanro false errors />
                <@tr_selection selections "updateparameters" updateparameters.pesa.helcom 1 false true true errors />
                <@separator "Koordinaatit:" />
                <@tr_selection selections "updateparameters" updateparameters.pesa.koord_mittaus 1 false true true errors />
                <@tr_selection selections "updateparameters" updateparameters.pesa.koord_tark    1 false true true errors />
                <@separator "" />
                <@tr_radio "updateparameters" updateparameters.pesa.koord_tyyppi "Y" errors />
                <@tr_inputText "updateparameters" updateparameters.pesa.yht_leveys true errors />              
                <@tr_inputText "updateparameters" updateparameters.pesa.yht_pituus true errors />
                <@separator "" />
                <tr><td colspan="2"><button id="openLahiymparistonPesatDialogButton" onclick="getLahiymparistonPesat();"> lähiympäristön pesät</button> </td></tr>
                <@separator "" />
                <@tr_radio "updateparameters" updateparameters.pesa.koord_tyyppi "E" errors />
                <@tr_inputText "updateparameters" updateparameters.pesa.eur_leveys true errors />              
                <@tr_inputText "updateparameters" updateparameters.pesa.eur_pituus true errors />
                <@separator "" />
                <@tr_radio "updateparameters" updateparameters.pesa.koord_tyyppi "A" errors />
                <@tr_inputText "updateparameters" updateparameters.pesa.ast_leveys true errors />
                <@tr_inputText "updateparameters" updateparameters.pesa.ast_pituus true errors />
                <@separator "Luonnonpesä:" />
                <@tr_inputText "updateparameters" updateparameters.pesa.loyt_vuosi true errors />
                <@tr_inputText "updateparameters" updateparameters.pesa.rak_vuosi true errors />
                <@tr_selection selections "updateparameters" updateparameters.pesa.rak_vuosi_tark 1 false true true errors />
                <@tr_selection selections "updateparameters" updateparameters.pesa.rak_laji       1 false true true errors />
                <@separator "Tekopesä:" />
                <@tr_inputText "updateparameters" updateparameters.pesa.rak_vuosi_teko true errors />
                <@tr_selection selections "updateparameters" updateparameters.pesa.rak_vuosi_teko_tark 1 false true true errors />
                <@separator "" />
                <@tr_inputText "updateparameters" updateparameters.pesa.tuhoutumisvuosi true errors />
                
                </table>
                
                <div class="otsikko">SUOJELU</div>
                
                <table>

                <@tr_date "updateparameters" updateparameters.olosuhde.palsta_rauh_pvm errors />
                <@separator "Rauhoitustaulu:" />
                <@tr_date "updateparameters" updateparameters.pesa.r_taulu_pvm errors />
                <@tr_selection selections "updateparameters" updateparameters.pesa.r_taulu_kieli 1 false true true errors />
                <@tr_inputText "updateparameters" updateparameters.pesa.r_taulu_nro true errors />
                <@tr_date "updateparameters" updateparameters.pesa.r_taulu_poisto_pvm errors />
                <@separator "" />
                <@tr_selection selections "updateparameters" updateparameters.olosuhde.palsta_rauhoitus 1 false true true errors />
                <@tr_date "updateparameters" updateparameters.olosuhde.rauh_aika_alku errors />
                <@tr_date "updateparameters" updateparameters.olosuhde.rauh_aika_loppu errors />
                <@tr_selection selections "updateparameters" updateparameters.olosuhde.naturassa 1 false true true errors />
                <@tr_textarea "updateparameters" updateparameters.olosuhde.suojelualue 2 39 errors />
                <@tr_selection selections "updateparameters" updateparameters.olosuhde.palsta_omistaja 1 false true true errors />
                <@tr_textarea "updateparameters" updateparameters.olosuhde.omist_kommentti 3 39 errors /> 
                
                </table>               
          </div>
      </td>
      <td>
        <table>
          <tr>
            <td>
              <table>
                <tr>
                  <td>
                      <div class="osiotaulu">
                         <!-- PESäPUU JA PESä -->
                         <div class="otsikko">PESÄPUU JA PESÄ</div>
                            
                         <table>
                         
                         <@tr_date "updateparameters" updateparameters.olosuhde.pesa_mit_pvm errors />
                         <@separator "" />
                         <@tr_selection selections "updateparameters" updateparameters.pesa.puulaji 1 false true true errors />
                         <@tr_selection selections "updateparameters" updateparameters.olosuhde.elavyys 1 false true true errors />
                         <@tr_selection selections "updateparameters" updateparameters.olosuhde.sijainti 1 false true true errors />
                         <@tr_textarea "updateparameters" updateparameters.pesa.kommentti 3 39 errors /> 
                         <@separator "" />
                         <@tr_inputText "updateparameters" updateparameters.olosuhde.korkeus true errors />
                         <@tr_selection selections "updateparameters" updateparameters.olosuhde.korkeus_tark 1 false true true errors />
                         <@separator "Puun tyven mitat 130cm korkeudelta:" />
                         <@tr_inputText "updateparameters" updateparameters.olosuhde.tyviymparys true errors />
                         <@tr_inputText "updateparameters" updateparameters.olosuhde.tyvihalkaisija true errors />
                         <@separator "Puun latvan mitat pesän alta:" />
                         <@tr_inputText "updateparameters" updateparameters.olosuhde.latvaymparys true errors />
                         <@tr_inputText "updateparameters" updateparameters.olosuhde.latvahalkaisija true errors />
                         <@separator "Pesän" />
                         <@tr_inputText "updateparameters" updateparameters.olosuhde.et_maasta true errors />
                         <@tr_inputText "updateparameters" updateparameters.olosuhde.et_latvasta true errors />
                         <@tr_inputText "updateparameters" updateparameters.tarkastus.pesa_korkeus true errors />
                         <@tr_inputText "updateparameters" updateparameters.tarkastus.pesa_halk_max true errors />
                         <@tr_inputText "updateparameters" updateparameters.tarkastus.pesa_halk_min true errors />
                         
                         </table>              
                      </div>
                  </td>
                  <td>
                      <div class="osiotaulu">
                          <!-- PESÄN YMPÄRISTÖ -->
                          <div class="otsikko">PESÄN YMPÄRISTÖ</div>
                          
                          <table>                

                          <@tr_date "updateparameters" updateparameters.olosuhde.ymp_mit_pvm errors />
                          <@separator "" />
                          <@tr_selection selections "updateparameters" updateparameters.olosuhde.saari_tyyppi 1 false true true errors />
                          <@tr_selection selections "updateparameters" updateparameters.olosuhde.autoyhteys 1 false true true errors />
                          <@separator "Etäisyydet (m), 0-1000 m tai >1000 m :" />
                          <@tr_inputText "updateparameters" updateparameters.pesa.et_meri true errors />
                          <@tr_inputText "updateparameters" updateparameters.pesa.et_jarvi true errors />
                          <@tr_inputText "updateparameters" updateparameters.olosuhde.et_avosuo true errors />
                          <@tr_inputText "updateparameters" updateparameters.tarkastus.et_tie true errors />
                          <@tr_inputText "updateparameters" updateparameters.olosuhde.et_ilmajohto true errors />
                          <@tr_inputText "updateparameters" updateparameters.tarkastus.et_as true errors />
                          <@tr_inputText "updateparameters" updateparameters.tarkastus.et_moottorikelkka true errors />
                          <@tr_inputText "updateparameters" updateparameters.tarkastus.et_kalaviljely true errors />
                          <@tr_inputText "updateparameters" updateparameters.olosuhde.et_viljapelto true errors />
                          <@tr_inputText "updateparameters" updateparameters.olosuhde.et_avohakkuu true errors />
                          <@tr_inputText "updateparameters" updateparameters.olosuhde.et_siemenpuusto true errors />
                          <@separator "Asuinrakennuksien määrä (kpl):" />
                          <@tr_inputText "updateparameters" updateparameters.tarkastus.as_lkm_500 true errors />
                          <@tr_inputText "updateparameters" updateparameters.tarkastus.as_lkm_1000 true errors />
                          <@separator "" />         
                          <@tr_selection selections "updateparameters" updateparameters.olosuhde.pesan_nakyvyys 1 false true true errors />
                          <@separator "" /> 
                          <@tr_selection selections "updateparameters" updateparameters.olosuhde.puusto 1 false true true errors />
                          <@tr_selection selections "updateparameters" updateparameters.olosuhde.puusto_kasittely 1 false true true errors />
                          <@tr_selection selections "updateparameters" updateparameters.olosuhde.puusto_ika 1 false true true errors />
                          <@separator "" />
                          <@tr_selection selections "updateparameters" updateparameters.olosuhde.maastotyyppi 1 false true true errors />
              
                          </table>                            
                      </div>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td>
                <div class="osiotaulu"> 
                      <!-- UHKATEKIJäT -->
                      <div class="otsikko">UHKATEKIJÄT</div> <br />
                      <table>
                        <@tr_textarea "updateparameters" updateparameters.pesa.uhat 8 85 errors />  
                      </table>
                </div>
            </td>
          </tr>
        </table> 
      </td>
    </tr>
  </table>
  
  
  <div class="osiotaulu">
    <!-- RELASKOOPPIMITTAUKSET -->
    <div class="otsikko">RELASKOOPPIMITTAUKSET</div>

  <table class="relaskooppimittaukset">
   <tr>
    <th>Relaskooppimittaukset</th>
    
    <td>Mittausten pvm</td> 
    <td colspan="2" class="paivays">
      <@date "updateparameters" updateparameters.olosuhde.relaskoop_pvm errors />
   	</td>
    <td colspan="5"></td>
  </tr>
  
   <tr>
    <th>Pesältä &rarr;</th>
    <td colspan="2"><b>25m pohjoiseen</b></td>
    <td colspan="2"><b>25m itään</b></td>
    <td colspan="2"><b>25m etelään</b></td>
    <td colspan="2"><b>25m länteen</b></td>
   </tr>
   
   <tr>
    <th></th>
    <td>lkm</td>
    <td>keskipituus</td>
    <td>lkm</td>
    <td>keskipituus</td>
    <td>lkm</td>
    <td>keskipituus</td>
    <td>lkm</td>
    <td>keskipituus</td>
   </tr>
   
   <tr>
    <th>Männyt</th>
    <td>  <@inputText "updateparameters" updateparameters.olosuhde.manty_lkm_p true errors />  <@compare updateparameters.olosuhde.manty_lkm_p /> </td>
    <td>  <@inputText "updateparameters" updateparameters.olosuhde.manty_pit_p true errors />  <@compare updateparameters.olosuhde.manty_pit_p /> </td>
    <td>  <@inputText "updateparameters" updateparameters.olosuhde.manty_lkm_i true errors />  <@compare updateparameters.olosuhde.manty_lkm_i /> </td>
    <td>  <@inputText "updateparameters" updateparameters.olosuhde.manty_pit_i true errors />  <@compare updateparameters.olosuhde.manty_pit_i /> </td>
    <td>  <@inputText "updateparameters" updateparameters.olosuhde.manty_lkm_e true errors />  <@compare updateparameters.olosuhde.manty_lkm_e /> </td>
    <td>  <@inputText "updateparameters" updateparameters.olosuhde.manty_pit_e true errors />  <@compare updateparameters.olosuhde.manty_pit_e /> </td>
    <td>  <@inputText "updateparameters" updateparameters.olosuhde.manty_lkm_l true errors />  <@compare updateparameters.olosuhde.manty_lkm_l /> </td>
    <td>  <@inputText "updateparameters" updateparameters.olosuhde.manty_pit_l true errors />  <@compare updateparameters.olosuhde.manty_pit_l /> </td>
  </tr>
   
   <tr>
    <th>Kuuset</th>
    <td>  <@inputText "updateparameters" updateparameters.olosuhde.kuusi_lkm_p true errors /> <@compare updateparameters.olosuhde.kuusi_lkm_p /> </td>
    <td>  <@inputText "updateparameters" updateparameters.olosuhde.kuusi_pit_p true errors /> <@compare updateparameters.olosuhde.kuusi_pit_p /> </td>
    <td>  <@inputText "updateparameters" updateparameters.olosuhde.kuusi_lkm_i true errors /> <@compare updateparameters.olosuhde.kuusi_lkm_i /> </td>
    <td>  <@inputText "updateparameters" updateparameters.olosuhde.kuusi_pit_i true errors /> <@compare updateparameters.olosuhde.kuusi_pit_i /> </td>
    <td>  <@inputText "updateparameters" updateparameters.olosuhde.kuusi_lkm_e true errors /> <@compare updateparameters.olosuhde.kuusi_lkm_e /> </td>
    <td>  <@inputText "updateparameters" updateparameters.olosuhde.kuusi_pit_e true errors /> <@compare updateparameters.olosuhde.kuusi_pit_e /> </td>
    <td>  <@inputText "updateparameters" updateparameters.olosuhde.kuusi_lkm_l true errors /> <@compare updateparameters.olosuhde.kuusi_lkm_l /> </td>
    <td>  <@inputText "updateparameters" updateparameters.olosuhde.kuusi_pit_l true errors /> <@compare updateparameters.olosuhde.kuusi_pit_l /> </td>
   </tr>
   
   <tr>
    <th>Muut</th>
    <td>  <@inputText "updateparameters" updateparameters.olosuhde.muu_lkm_p true errors /> <@compare updateparameters.olosuhde.muu_lkm_p /> </td>
    <td>  <@inputText "updateparameters" updateparameters.olosuhde.muu_pit_p true errors /> <@compare updateparameters.olosuhde.muu_pit_p /> </td>
    <td>  <@inputText "updateparameters" updateparameters.olosuhde.muu_lkm_i true errors /> <@compare updateparameters.olosuhde.muu_lkm_i /> </td>
    <td>  <@inputText "updateparameters" updateparameters.olosuhde.muu_pit_i true errors /> <@compare updateparameters.olosuhde.muu_pit_i /> </td>
    <td>  <@inputText "updateparameters" updateparameters.olosuhde.muu_lkm_e true errors /> <@compare updateparameters.olosuhde.muu_lkm_e /> </td>
    <td>  <@inputText "updateparameters" updateparameters.olosuhde.muu_pit_e true errors /> <@compare updateparameters.olosuhde.muu_pit_e /> </td>
    <td>  <@inputText "updateparameters" updateparameters.olosuhde.muu_lkm_l true errors /> <@compare updateparameters.olosuhde.muu_lkm_l /> </td>
    <td>  <@inputText "updateparameters" updateparameters.olosuhde.muu_pit_l true errors /> <@compare updateparameters.olosuhde.muu_pit_l /> </td>
   </tr>
  </table>
  
  </div>
  
  
  <table>
    <tr>
      <td rowspan="2">
                <!-- PESINTä -->
                <div class="osiotaulu">
                <div class="otsikko">PESINTÄ</div>
                <table>
                <@tr_selection selections "updateparameters" updateparameters.tarkastus.pesimistulos  1 false true true errors />
                <@tr_selection selections "updateparameters" updateparameters.tarkastus.pesimist_tark 1 false true true errors />
                <@separator "" />
                <@tr_selection selections "updateparameters" updateparameters.tarkastus.nahdyt_merkit 1 false true true errors /> 
                <@tr_selection selections "updateparameters" updateparameters.tarkastus.pesa_merkit 1 false true true errors /> 
                <@separator "" />
                <@tr_selection selections "updateparameters" updateparameters.tarkastus.epaonni_syy  1 false true true errors />
                <@tr_selection selections "updateparameters" updateparameters.tarkastus.epaonni_tark 1 false true true errors />
                <@separator "" /> 
                <@tr_selection selections "updateparameters" updateparameters.tarkastus.pesa_kunto 1 false true true errors />
                <@tr_selection selections "updateparameters" updateparameters.tarkastus.pesa_kunto_ihmisen_vaik 1 false true true errors />
                <@tr_selection selections "updateparameters" updateparameters.tarkastus.pesa_kunto_aika 1 false true true errors />
                <@separator "" />
                <@separator "Lukumäärät (kpl) :" />
                <@tr_inputText "updateparameters" updateparameters.tarkastus.munia_lkm true errors />
                <@tr_date "updateparameters" updateparameters.tarkastus.munia_pvm errors />
                <@tr_selection selections "updateparameters" updateparameters.tarkastus.elavia_lkm 1 false true true errors />
                <@tr_inputText "updateparameters" updateparameters.tarkastus.kuoriutumattomia_lkm true errors />
                <@tr_inputText "updateparameters" updateparameters.tarkastus.kuolleita_lkm true errors />
                <@tr_inputText "updateparameters" updateparameters.tarkastus.reng_poik_lkm true errors />
                <@tr_inputText "updateparameters" updateparameters.tarkastus.lentopoik_lkm true errors />
                <@separator "" />
                <@tr_selection selections "updateparameters" updateparameters.tarkastus.muulaji 1 false true true errors /> 
                <@tr_selection selections "updateparameters" updateparameters.tarkastus.muulaji_vastarakki 1 false true true errors />
                <@tr_selection selections "updateparameters" updateparameters.tarkastus.muulaji_rinnakkainen 1 false true true errors />
                <@separator "" /> 
                <@tr_textarea "updateparameters" updateparameters.tarkastus.pesimist_kommentti 3 39 errors /> 

                </table>
                </div>
      </td>
      <td colspan="2">
                <!-- TIEDOT AIKUISISTA -->
                <div class="osiotaulu">
                <div class="otsikko">TIEDOT AIKUISISTA</div>
                
                  ${texts["tarkastuslomake.pesatarkastus.aikuisia_lkm"]}: &nbsp;  
                  <@selection selections "updateparameters" updateparameters.tarkastus.aikuisia_lkm 1 false true true errors />

                      <table>
						<tr>
							<th rowspan="2"> Sukupuoli </th>
							<th colspan="2"> Oikea rengas </th>
							<th colspan="2"> Vasen rengas </th>
						</tr>
						<tr>
							<th> Tunnus </th> <th> Väri </th>
							<th> Tunnus </th> <th> Väri </th>
						</tr>
						<tr>
							<td> <@selection selections "updateparameters" updateparameters.getColumn("aikuinen.1.sukupuoli") 1 false true true errors /> </td>
							<td> <@inputText "updateparameters" updateparameters.getColumn("aikuinen.1.rengas_oikea") true errors /> </td>
							<td> <@selection selections "updateparameters" updateparameters.getColumn("aikuinen.1.varit_oikea") 1 false true true errors /> </td>
							<td> <@inputText "updateparameters" updateparameters.getColumn("aikuinen.1.rengas_vasen") true errors /> </td>
							<td> <@selection selections "updateparameters" updateparameters.getColumn("aikuinen.1.varit_vasen") 1 false true true errors /> </td>
						</tr>
        				<tr>
							<td> <@selection selections "updateparameters" updateparameters.getColumn("aikuinen.2.sukupuoli") 1 false true true errors /> </td>
							<td> <@inputText "updateparameters" updateparameters.getColumn("aikuinen.2.rengas_oikea") true errors /> </td>
							<td> <@selection selections "updateparameters" updateparameters.getColumn("aikuinen.2.varit_oikea") 1 false true true errors /> </td>
							<td> <@inputText "updateparameters" updateparameters.getColumn("aikuinen.2.rengas_vasen") true errors /> </td>
							<td> <@selection selections "updateparameters" updateparameters.getColumn("aikuinen.2.varit_vasen") 1 false true true errors /> </td>
						</tr>
						<tr>
							<td> <@selection selections "updateparameters" updateparameters.getColumn("aikuinen.3.sukupuoli") 1 false true true errors /> </td>
							<td> <@inputText "updateparameters" updateparameters.getColumn("aikuinen.3.rengas_oikea") true errors /> </td>
							<td> <@selection selections "updateparameters" updateparameters.getColumn("aikuinen.3.varit_oikea") 1 false true true errors /> </td>
							<td> <@inputText "updateparameters" updateparameters.getColumn("aikuinen.3.rengas_vasen") true errors /> </td>
							<td> <@selection selections "updateparameters" updateparameters.getColumn("aikuinen.3.varit_vasen") 1 false true true errors /> </td>
						</tr>
					</table>
				
				<table>
				  <tr>
				    <td> ${texts["tarkastuslomake.pesatarkastus.rengas_kommentti"]} </td> 
				    <td> ${texts["tarkastuslomake.pesatarkastus.kuollut_kommentti"]} </td>
				  </tr>
				  <tr>
                    <td> <@textarea "updateparameters" updateparameters.tarkastus.rengas_kommentti  3 39 errors /> </td> 
                    <td> <@textarea "updateparameters" updateparameters.tarkastus.kuollut_kommentti 3 39 errors /> </td>
                  </tr>
                </table> 
              </div>     
      </td>
    </tr>
    <tr>
      <td>
       		    
      </td>
      <td>
                <div class="osiotaulu">
                <div class="otsikko">TYÖPANOS</div>
                <table>
                <@tr_inputText "updateparameters" updateparameters.tarkastus.tyop_tuntia true errors />
                <@tr_inputText "updateparameters" updateparameters.tarkastus.tyop_auto_aj true errors />
                <@tr_inputText "updateparameters" updateparameters.tarkastus.tyop_vene_aj true errors />
                <@tr_textarea "updateparameters" updateparameters.tarkastus.tyop_kommentti 5 39 errors /> 
                </table>
                </div>
      </td>
    </tr>
  </table>
  
  
  
  
  <table>
  <tr>
  <td>
  <div class="osiotaulu">
        <!-- POIKASET -->
        <div class="otsikko">TIEDOT POIKASISTA / MUNISTA</div>
          
          <table class="poikastaulu">
             <tr>
              <td> </td>
              <td colspan="4"><b>Ensimmäinen mittaus</b></td>
              <td colspan="4"><b>Toinen mittaus</b></td>
             </tr>
             
            <tr>
              <td> </td>
              <td colspan="4">
                Pvm:<@date "updateparameters" updateparameters.tarkastus.mittaus_pvm_1 errors /> &nbsp; &nbsp;
                Mittaaja: <@inputSelectCombo "updateparameters" updateparameters.tarkastus.mittaaja_id_1 true false errors />  
              </td>
              <td colspan="4">
                Pvm: <@date "updateparameters" updateparameters.tarkastus.mittaus_pvm_2 errors /> &nbsp; &nbsp;
                Mittaaja: <@inputSelectCombo "updateparameters" updateparameters.tarkastus.mittaaja_id_2 true false errors />
              </td>
            </tr>
            
            <tr>
              <td>Poikanen tai muna &rarr;</td>
              <td>1</td><td>2</td><td>3</td><td>4</td>
              <td>1</td><td>2</td><td>3</td><td>4</td>
            </tr>
            
            <@tr_poikanen_inputText updateparameters "rengas_oikea" errors false true />
            <@tr_poikanen_selection selections updateparameters "varit_oikea" true errors false />
            <@tr_poikanen_inputText updateparameters "rengas_vasen" errors false true />
            <@tr_poikanen_selection selections updateparameters "varit_vasen" true errors false />
            <@tr_poikanen_selection selections updateparameters "sukupuoli" true errors false />
            <@tr_poikanen_inputText updateparameters "siipi_pituus" errors true true />
 <tr>
   <td class="kuvaus">Poikasen ikä (laskennallinen):</td>
   <td> ${updateparameters.getColumn("poikanen.1.poikasen_ika")} </td>
   <td> ${updateparameters.getColumn("poikanen.2.poikasen_ika")} </td>
   <td> ${updateparameters.getColumn("poikanen.3.poikasen_ika")} </td>
   <td> ${updateparameters.getColumn("poikanen.4.poikasen_ika")} </td>
   <td> ${updateparameters.getColumn("poikanen.1.poikasen_ika_2")} </td>
   <td> ${updateparameters.getColumn("poikanen.2.poikasen_ika_2")} </td>
   <td> ${updateparameters.getColumn("poikanen.3.poikasen_ika_2")} </td>
   <td> ${updateparameters.getColumn("poikanen.4.poikasen_ika_2")} </td>
 </tr>
            <@tr_poikanen_selection selections updateparameters "siipi_pituus_m" true errors true />
            <@tr_poikanen_inputText updateparameters "nilkka_max" errors true true />
            <@tr_poikanen_inputText updateparameters "nilkka_min" errors true true />
            <@tr_poikanen_inputText updateparameters "nokka_pituus" errors true true />
            <@tr_poikanen_inputText updateparameters "nokka_tyvi" errors true true />
            <@tr_poikanen_inputText updateparameters "paino" errors true true />
            <@tr_poikanen_selection selections updateparameters "kupu" true errors true />
            <@tr_poikanen_selection selections updateparameters "hoyhennayte" true errors true />
            <@tr_poikanen_selection selections updateparameters "verinayte" true errors true />
            <@tr_poikanen_inputText updateparameters "munan_pituus" errors false true />
            <@tr_poikanen_inputText updateparameters "munan_leveys" errors false true />
            <@tr_poikanen_inputText updateparameters "kommentti" errors true false />
            
          </table>        
          Kuoriutumispäivä: ${updateparameters.tarkastus.kuoriutumispaiva}
  </div>
  </td>
  </tr>
  </table>

  
  <table>
  <tr>
  <td>
  <div class="osiotaulu">
  <!-- NAYTTEET -->

  <div class="otsikko" >NÄYTTEITÄ OTETTU</div>
  
    <table>
      <tr>
      
        <td>Munia (kpl):</td>
        <td><@inputText "updateparameters" updateparameters.tarkastus.nayte_m true errors /></td>
    
        <td>Munansiruja:</td>
        <td><@selection selections "updateparameters" updateparameters.tarkastus.nayte_s 1 false true false errors /></td>
        
        <td>Sulkia, höyheniä:</td>
        <td><@selection selections "updateparameters" updateparameters.tarkastus.nayte_i 1 false true false errors /></td>
        
        <td>Kuolleita poikasia (kpl):</td>
        <td><@inputText "updateparameters" updateparameters.tarkastus.nayte_p true errors /></td>
        
      </tr>
      <tr>
        
        <td>Kuolleita aikuisia (kpl):</td>
        <td><@inputText "updateparameters" updateparameters.tarkastus.nayte_a true errors /></td>
        
        <td>Saalispusseja (kpl):</td>
        <td><@inputText "updateparameters" updateparameters.tarkastus.nayte_r true errors /></td>
        
        <td>Oksennuspalloja:</td>
        <td><@selection selections "updateparameters" updateparameters.tarkastus.nayte_o 1 false true false errors /></td>
        
        <td></td><td></td>
        
      </tr>
    </table>
  </div>
  </td>
  </tr>
  </table>
  
  
  <div class="osiotaulu">
  <!-- KUVAT -->
    <div class="otsikko" >KUVAT VUONNA ${updateparameters.tarkastus.tark_vuosi}</div>
      <table>
        <tr>
          <td>Kuvia linnuista:</td>
	      <td><@selection selections "updateparameters" updateparameters.tarkastus.kuvia_lintu 1 false true false errors /></td>
	      <td>Kuvaaja:</td>
	      <td><@inputSelectCombo "updateparameters" updateparameters.tarkastus.kuvaaja_lintu true false errors /></td>
          
          <td>Kuvia pesästä:</td>
          <td><@selection selections "updateparameters" updateparameters.tarkastus.kuvia_pesa 1 false true false errors /></td>
	      <td>Kuvaaja:</td>
	      <td><@inputSelectCombo "updateparameters" updateparameters.tarkastus.kuvaaja_pesa true false errors /></td>
        </tr>
      </table>
  </div>
    
  
   <#if (data.kirjekyyhky!"") == "yes">
          <input type="submit" value="${texts["tarkastus_submit_kirjekyyhky_"+system.action]}" class="lahetanappi" onclick="document.tarkastuslomake.submit()"/>
     <#else>
  	      <input type="submit" value="${texts["tarkastus_submit_"+system.action]}" class="lahetanappi" onclick="document.tarkastuslomake.submit()"/>
   </#if>
  
<div id="lahiymparistonPesatContainer" class="popupBoxElement hidden">
		<a href="#" id="lahiymparistonPesatCloseButton"> Sulje  <span class="icon">X</span> </a>
		<div id="lahiymparistonPesat"> Lataa... </div>
		
		<table class="infoContainer">
			<tr>
	    		<td colspan="4">
            		Säde:
            		<input type="text" name="CoordinateSearchRadius" value="${data.CoordinateSearchRadius!"500" }" size="7" tabindex="6666" /> m 
            		<button onclick="getLahiymparistonPesat();" tabindex="6667"> Hae </button> 
	         	</td>
	    	</tr>
		</table>
</div>

</form>
 
</div>
</div>



<script type="text/javascript">
	  var form = document.forms['tarkastuslomake'];
	  var nestID = form['updateparameters.pesavakio.pesa_id'];
	  var yhtLeveys = form['updateparameters.pesavakio.yht_leveys'];
	  var yhtPituus = form['updateparameters.pesavakio.yht_pituus'];
	  var eurLeveys = form['updateparameters.pesavakio.eur_leveys'];
	  var eurPituus = form['updateparameters.pesavakio.eur_pituus'];
	  var radius = form['CoordinateSearchRadius'];
	  
	  $('#openLahiymparistonPesatDialogButton').click(function() {
		$('#lahiymparistonPesatContainer').show(300); 
		return false;
	});
	$('#lahiymparistonPesatCloseButton').click(function () {
     	$('#lahiymparistonPesatContainer').hide(300);
     	$('#openLahiymparistonPesatDialogButton').focus(); 
    	return false;
	});
	
	var lomakeVastaanottajat = [<#list selections["ringers"].options as option>"${option.text?replace("\"","")} (${option.value})"<#if option_has_next>,</#if></#list>];
	
	
</script>
 

</body>

</html>
