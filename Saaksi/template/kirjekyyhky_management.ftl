
<#include "header.ftl">

<#-- <div class="kirjekyyhky_management"> -->

<h1>${texts.kirjekyyhky_management_title}</h1>

<h2>${texts.kirjekyyhky_management_incoming}</h2>

<@errorlist errors texts /> 
<@statustexts system texts />

<#if lists.forms??>
  <table  id="resultsTable" class="tablesorter">
    <thead>
    <tr>
      <th>Vuosi</th>
      <th>Kunta</th>
      <th>Kylä</th>
      <th>Reviiri</th>
      <th>Pesä</th>
      <th>Omistajat</th>
      <th>Lähetyspvm</th>
      <th class="{sorter: false}"> &nbsp; </th>
      <th class="{sorter: false}"> &nbsp; </th>
    </tr>
    </thead>
    <tbody>
  		<#list lists.forms as form>
    		<tr>
      			<td> ${form.data['tarkastus.vuosi']!""} </td>
      			<td> <#if form.data['pesa.kunta']?has_content> ${selections['municipalities'][form.data['pesa.kunta']?upper_case]} </#if> </td>
      			<td> ${form.data['pesa.kyla']!""} </td>
      			<td> ${form.data['reviiri.reviirinimi']!""} </td>
      			<td> 
      				${form.data['pesa.pesanimi']!""} 
      			    <#if form.data['pesa.pesaid']?has_content> (${form.data['pesa.pesaid']}) <#else> (uusi pesä) </#if>
				</td>
      			<td> <#list form.owners as owner> ${owner!""} <#if owner_has_next>,</#if> </#list> </td>
      			<td> ${form.lastUpdated} </td>
      		    <td> <a href="${system.baseURL}?page=${system.page}&amp;action=receive&amp;id=${form.id}" class="button-link"> Aloita vastaanotto </a> </td>
      		    <td> Ota vastaan toiseen pesään: 
      		    		<form method="get" action="${system.baseURL}">
      		    			<input type="text" name="nestID" size="5"/>
      		    			<input type="submit" value="Aloita vastaanotto" onclick="return kirjekyyhkyReceiveValidateNestID(this);"/>
      		    			<input type="hidden" name="page" value="${system.page}" />
      		    			<input type="hidden" name="action" value="receive_to_other_nest" />
      		    			<input type="hidden" name="id" value="${form.id}" />
      		    		</form>
      		    </td>
    		</tr>
  		</#list>
    </tbody>
  </table>
<#else>
  Ei mitään vastaanotettavaa.
</#if>


<#-- </div> -->





<#include "footer.ftl">
