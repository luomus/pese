package fi.hy.pese.ulko.main.app;

import fi.hy.pese.framework.main.app.PeSeServlet;
import fi.hy.pese.framework.main.app._ApplicationSpecificationFactory;
import fi.luomus.commons.db.connectivity.PreparedStatementStoringAndClosingTransactionConnection;
import fi.luomus.commons.db.connectivity.TransactionConnection;

public class UlkoServlet extends PeSeServlet<_UlkoDomainModelRow> {

	static final long	serialVersionUID	= 2525L;

	@Override
	protected _ApplicationSpecificationFactory<_UlkoDomainModelRow> createFactory() throws Exception {
		UlkoFactory factory = new UlkoFactory("pese_ulko.config");
		factory.loadUITexts();

		TransactionConnection con = null;
		try {
			con = new PreparedStatementStoringAndClosingTransactionConnection(factory.config().connectionDescription());
			UlkoDatabaseStructure structure = UlkoDatabaseStructureCreator.loadFromDatabase(con);
			factory.setRowFactory(new UlkoDomainModelRowFactory(structure));
		} catch (Exception e) {
			throw new Exception(e);
		} finally {
			if (con != null) con.release();
		}
		return factory;
	}

}
