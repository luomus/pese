package fi.hy.pese.framework.main.db.oraclesql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import fi.hy.pese.framework.main.db.Results;
import fi.hy.pese.framework.main.db._ResultsHandler;
import fi.hy.pese.framework.main.general.Utils;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data.DatabaseTableStructure;
import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.main.general.data._ReferenceKey;
import fi.hy.pese.framework.main.general.data._Row;
import fi.hy.pese.framework.main.general.data._Table;
import fi.hy.pese.rengastus.main.app._RengastusDomainModelRow;
import fi.hy.pese.ulko.main.app._UlkoDomainModelRow;
import fi.luomus.commons.containers.Selection;
import fi.luomus.commons.containers.SelectionImple;
import fi.luomus.commons.containers.SelectionOptionImple;
import fi.luomus.commons.db.connectivity.TransactionConnection;

public class Queries {

	private static final String	RETURN_TABLE_STRUCTURE_SQL__USER	= " SELECT column_name, data_type, data_length, data_precision, data_scale, char_length FROM  user_tab_columns  WHERE table_name = ? ORDER BY  column_id ";

	public static boolean checkRowExists(_Table table, TransactionConnection con) throws SQLException {
		SelectStatement statement = new SelectStatement(con);
		statement.setOnlyExactMatches(true);
		statement.select("1");
		statement.from(table.getName());
		if (hasRowId(table)) {
			statement.where(table.get(Const.ROWID));
		} else if (table.hasUniqueNumericIdColumn()) {
			statement.where(table.getUniqueNumericIdColumn());
		} else {
			if (!table.hasImmutableKeys()) throw new SQLException(table.getName() + " does not define any keys");
			for (_Column c : table) {
				if (isPartOfTheKey(c)) statement.where(c);
			}
		}
		try {
			boolean result = statement.hasResults();
			return result;
		} finally {
			statement.close();
		}
	}

	public static boolean checkRowIdMatchesKeys(_Table table, TransactionConnection con) throws SQLException {
		if (!hasRowId(table)) return false;
		SelectStatement statement = new SelectStatement(con);
		statement.setOnlyExactMatches(true);
		statement.select("1");
		statement.from(table.getName());
		statement.where(table.get(Const.ROWID));
		if (table.hasUniqueNumericIdColumn()) {
			statement.where(table.getUniqueNumericIdColumn());
		} else {
			if (!table.hasImmutableKeys()) throw new SQLException(table.getName() + " does not define any keys");
			for (_Column c : table) {
				if (isPartOfTheKey(c)) statement.where(c);
			}
		}
		try {
			boolean result = statement.hasResults();
			return result;
		} finally {
			statement.close();
		}
	}

	public static boolean checkReferenceValueExists(String value, _ReferenceKey referenceKey, TransactionConnection con) throws SQLException {
		SelectStatement statement = new SelectStatement(con);
		statement.select("1");
		statement.from(referenceKey.table());
		statement.where(referenceKey.column() + " = ? ", value);
		try  {
			boolean result = statement.hasResults();
			return result;
		} finally {
			statement.close();
		}
	}

	public static String returnReferenceValue(String foreignKeyValue, _ReferenceKey referenceKey, TransactionConnection con) throws SQLException {
		SelectStatement statement = new SelectStatement(con);
		statement.select(referenceKey.column());
		statement.from(referenceKey.table());
		statement.where(referenceKey.column() + " = ? ", foreignKeyValue);
		try {
			ResultSet rs = statement.executeQuery();
			int count = 0;
			String value = null;
			while (rs.next()) {
				count++;
				value = rs.getString(1);
			}
			if (count != 1) {
				throw new SQLException("Instead of 1 row foreign key matched " + count + " rows");
			}
			return value;
		} finally {
			statement.close();
		}
	}

	public static boolean checkValuesExists(_Table table, TransactionConnection con) throws SQLException {
		SelectStatement statement = new SelectStatement(con);
		statement.setOnlyExactMatches(true);
		statement.selectAll_From_Where(table);
		try {
			boolean result = statement.hasResults();
			return result;
		} finally {
			statement.close();
		}
	}

	public static void executeDelete(_Table table, TransactionConnection con) throws SQLException, NothingToDeleteException {
		DeleteStatement statement = new DeleteStatement(con);
		try {
			statement.deleteByRowId(table);
			int i = statement.execute();
			if (i == 0) throw new NothingToDeleteException();
			if (i != 1) throw new SQLException(" Delete by table statement deleted " + i + " rows instead of 1");
		} finally {
			statement.close();
		}
	}

	public static void executeInsert(_Table table, TransactionConnection con) throws SQLException {
		if (table.hasUniqueNumericIdColumn()) {
			_Column idColumn = table.getUniqueNumericIdColumn();
			String id = idColumn.getValue();
			if (isNull(id)) { throw new SQLException("No next id set for " + idColumn.getFullname()); }
		}
		table.setRowidValue("");
		if (checkRowExists(table, con)) { throw new SQLException("Row with given IDs already exists. " + Utils.debugS(table)); }
		InsertStatement statement = new InsertStatement(con);
		statement.insert(table);
		try {
			int i = statement.execute();
			if (i != 1) throw new SQLException("Insert table statement added " + i + " rows instead of 1");
			String rowid = returnRowidOfInsertedRow(table, con);
			table.setRowidValue(rowid);
		} finally {
			statement.close();
		}
	}

	public static void executeExactMatchSearch(_Table parameters, _ResultsHandler resultHandler, TransactionConnection con) throws SQLException {
		SelectStatement statement = new SelectStatement(con);
		statement.setOnlyExactMatches(true);
		statement.selectAll_From_Where_OrderBy(parameters);
		ResultSet rs = null;
		try {
			rs = statement.executeQuery();
			resultHandler.process(new Results(rs));
		} finally {
			statement.close();
			if (rs != null) rs.close();
		}
	}

	public static void executeSearch(_Table parameters, _ResultsHandler resultHandler, TransactionConnection con) throws SQLException {
		SelectStatement statement = new SelectStatement(con);
		statement.selectAll_From_Where_OrderBy(parameters);
		ResultSet rs = null;
		try {
			rs = statement.executeQuery();
			resultHandler.process(new Results(rs));
		} finally {
			statement.close();
			if (rs != null) rs.close();
		}
	}

	public static void executeSearch(_Table parameters, _ResultsHandler resultHandler, String[] orderBy, TransactionConnection con) throws SQLException {
		SelectStatement statement = new SelectStatement(con);
		statement.selectAll_From_Where(parameters);
		statement.orderBy(orderBy);
		ResultSet rs = null;
		try {
			rs = statement.executeQuery();
			resultHandler.process(new Results(rs));
		} finally {
			if (rs != null) rs.close();
			statement.close();
		}
	}

	public static void executeSearch(TransactionConnection con, _Row params, _ResultsHandler resultHandler, String joins, String orderBy, String... additional) throws SQLException {
		SelectStatement statement = null;
		if (params instanceof _PesaDomainModelRow) {
			statement = executeSearch_pesaDomainModelRow(con, (_PesaDomainModelRow) params, joins);
		} else if (params instanceof _RengastusDomainModelRow) {
			statement = executeSearch_rengastusDomainModelRow(con, (_RengastusDomainModelRow) params, joins);
		} else if (params instanceof _UlkoDomainModelRow) {
			statement = executeSearch_ulkoDomainModelRow(con, (_UlkoDomainModelRow) params, joins);
		} else {
			throw new UnsupportedOperationException("This DAO does not support " + params.getClass().getName());
		}

		for (String additionalQuery : additional) {
			statement.where(additionalQuery);
		}
		statement.orderBy(orderBy);

		ResultSet rs = null;
		try {
			rs = statement.executeQuery();
			resultHandler.process(new Results(rs));
		} finally {
			if (rs != null) rs.close();
			statement.close();
		}
	}
	
	private static SelectStatement executeSearch_pesaDomainModelRow(TransactionConnection con, _PesaDomainModelRow parameters, String joins) {
		_Table tarkastus = parameters.getTarkastus();
		_Table kaynti = parameters.getKaynti();
		_Table poikanen = parameters.getPoikanen();
		_Table aikuinen = parameters.getAikuinen();
		_Table muna = parameters.getMuna();
		_Table pesa = parameters.getPesa();
		_Table olosuhde = parameters.getOlosuhde();
		_Table vuosi = parameters.getVuosi();
		_Table reviiri = parameters.getReviiri();

		SelectStatement statement = new SelectStatement(con);
		statement.addToWhereClause(joins);
		includeIfInitialized(tarkastus, statement);
		includeIfInitialized(kaynti, statement);
		includeIfInitialized(poikanen, statement);
		includeIfInitialized(aikuinen, statement);
		includeIfInitialized(muna, statement);
		includeIfInitialized(pesa, statement);
		includeIfInitialized(olosuhde, statement);
		includeIfInitialized(vuosi, statement);
		includeIfInitialized(reviiri, statement);
		return statement;
	}

	private static SelectStatement executeSearch_rengastusDomainModelRow(TransactionConnection con, _RengastusDomainModelRow parameters, String joins) {
		SelectStatement statement = new SelectStatement(con);
		statement.addToWhereClause(joins);
		statement.selectAll_From_Where(parameters.getKontrolli());
		return statement;
	}

	private static SelectStatement executeSearch_ulkoDomainModelRow(TransactionConnection con, _UlkoDomainModelRow parameters, String joins) {
		SelectStatement statement = new SelectStatement(con);
		statement.addToWhereClause(joins);
		statement.selectAll_From_Where(parameters.getPyynto());
		statement.selectAll_From_Where(parameters.getLoyto());
		statement.selectAll_From_Where(parameters.getRengastus());
		return statement;
	}

	private static void includeIfInitialized(_Table table, SelectStatement statement) {
		if (table.isInitialized()) {
			statement.selectAll_From_Where(table);
		}
	}

	public static void executeUpdate(_Table table, TransactionConnection con) throws SQLException {
		UpdateStatement statement = new UpdateStatement(con);
		statement.updateByRowId(table);
		try {
			int i = statement.execute();
			if (i != 1) throw new SQLException("Update updated " + i + " rows instead of 1");
		} finally {
			statement.close();
		}
	}

	private static boolean hasRowId(_Table table) {
		return table.getRowidValue().length() > 0;
	}

	private static boolean isDifferent(String text1, String text2) {
		return !text2.equals(text1);
	}

	private static boolean isEmpty(String[] parameters) {
		return parameters == null || parameters.length < 1;
	}

	private static boolean isNull(String value) {
		return value == null || value.length() < 1;
	}

	private static boolean isPartOfTheKey(_Column c) {
		return c.getFieldType() == _Column.IMMUTABLE_PART_OF_A_KEY;
	}

	private static String returnRowidOfInsertedRow(_Table table, TransactionConnection con) throws SQLException {
		SelectStatement statement = new SelectStatement(con);
		statement.setOnlyExactMatches(true);
		statement.select("rowid");
		statement.from(table.getName());
		if (table.hasUniqueNumericIdColumn()) {
			statement.where(table.getUniqueNumericIdColumn());
		} else {
			if (!table.hasImmutableKeys()) throw new SQLException(table.getName() + " does not define any keys");
			for (_Column c : table) {
				if (isPartOfTheKey(c)) {
					statement.where(c);
				}
			}
		}
		ResultSet rs = null;
		try {
			rs = statement.executeQuery();
			rs.next();
			String rowid = rs.getString(1);
			if (rs.next()) throw new SQLException("returnRowidOfInsertedRow produced too many results");
			return rowid;
		} finally {
			if (rs != null) rs.close();
			statement.close();
		}
	}

	/**
	 * Generates Selection of type (value, desc): (K, Kartta), (G, GPS)
	 * @param con
	 * @param tablename
	 * @param valuefield
	 * @param descriptionfield
	 * @param groupField
	 * @param orderBy
	 * @return
	 * @throws SQLException
	 */
	public static SelectionImple returnSelection(TransactionConnection con, String tablename, String valuefield, String descriptionfield, String[] orderBy) throws SQLException {
		SelectStatement statement = new SelectStatement(con);
		statement.setDistinct(true);
		statement.select(valuefield);
		statement.select(descriptionfield);
		statement.from(tablename);
		if (isEmpty(orderBy)) {
			statement.orderBy(valuefield);
		} else {
			statement.orderBy(orderBy);
		}
		ResultSet rs = null;
		try {
			rs = statement.executeQuery();
			SelectionImple selection = new SelectionImple(tablename);
			while (rs.next()) {
				selection.addOption(new SelectionOptionImple(rs.getString(1), rs.getString(2)));
			}
			return selection;
		} finally {
			if (rs != null) rs.close();
			statement.close();
		}
	}

	/**
	 * Returns a map of selections from "code table" ("aputaulu").
	 * @param con
	 * @param codeTable "aputaulu"
	 * @param tableField "taulu"
	 * @param fieldField "kentta" or "attribuutti"
	 * @param valueField "arvo"
	 * @param descField "selite"
	 * @param groupField "ryhmittely"
	 * @param orderBy "jarjestys", "selite", ...
	 * @return
	 * @throws SQLException
	 */
	public static Map<String, Selection> returnSelectionsFromCodeTable(TransactionConnection con, String codeTable, String tableField, String fieldField, String valueField, String descField, String groupField, String[] orderBy) throws SQLException {
		SelectStatement statement = new SelectStatement(con);
		statement.select(tableField, fieldField, valueField, descField, groupField);
		statement.from(codeTable);
		Vector<String> orderByFields = new Vector<>();
		orderByFields.add(tableField);
		orderByFields.add(fieldField);
		for (String s : orderBy) {
			orderByFields.add(s);
		}
		statement.orderBy(orderByFields);

		ResultSet rs = null;
		try {
			rs = statement.executeQuery();
			Map<String, Selection> selections = new HashMap<>();
			SelectionImple selection = null;
			String currentSelection = "";
			String table, field, value, desc, group;
			while (rs.next()) {
				table = rs.getString(1).toLowerCase();
				field = rs.getString(2).toLowerCase();
				value = rs.getString(3).toUpperCase();
				desc = rs.getString(4);
				group = rs.getString(5);
				if (isDifferent(currentSelection, table + field)) {
					selection = new SelectionImple(table + "." + field);
					selections.put(table + "." + field, selection);
					currentSelection = table + field;
				}
				if (selection == null) throw new IllegalStateException("Selection not initialized properly");
				selection.addOption(new SelectionOptionImple(value, desc, group));
			}
			return selections;
		} finally {
			if (rs != null) rs.close();
			statement.close();
		}
	}

	public static long returnSelectionsUpdatedTime(TransactionConnection con) throws SQLException {
		PreparedStatement p = null;
		ResultSet rs = null;
		try {
			p = con.prepareStatement(" SELECT time FROM selections_updated ");
			rs = p.executeQuery();
			long time;
			if (!rs.next())
				time = 0;
			else {
				time = rs.getTimestamp(1).getTime() / 1000;
			}
			return time;
		} finally {
			if (rs != null) rs.close();
			if (p != null) p.close();
		}
	}

	
	public static DatabaseTableStructure returnTableStructure(String tablename, String as, TransactionConnection con) throws SQLException {
		DatabaseTableStructure table = new DatabaseTableStructure(as != null ? as.toLowerCase() : tablename.toLowerCase());
		PreparedStatement statement = con.prepareStatement(RETURN_TABLE_STRUCTURE_SQL__USER);
		try {
			statement.setString(1, tablename.toUpperCase());
			insertTableStructureValues(table, statement);
			if (table.getColumns().size() < 1) {
				throw new SQLException("No table "+tablename.toLowerCase() + " defined in user_tab_columns");
			}
		} finally {
			statement.close();
		}
		return table;
	}

	public static DatabaseTableStructure returnTableStructure(String tablename, TransactionConnection con) throws SQLException {
		return returnTableStructure(tablename, null, con);
	}

	private static void insertTableStructureValues(DatabaseTableStructure table, PreparedStatement p) throws SQLException {
		ResultSet rs = null;
		try {
			rs = p.executeQuery();
			while (rs.next()) {
				String name = rs.getString(1).toLowerCase();
				String type = rs.getString(2);
				// int length = rs.getInt(3);
				int precision = rs.getInt(4);
				int scale = rs.getInt(5);
				int charLength = rs.getInt(6);

				if (type.contains("CHAR"))
					table.addColumn(name, _Column.VARCHAR, charLength);
				else if (type.equals("DATE"))
					table.addColumn(name, _Column.DATE);
				else if (type.equals("NUMBER") && scale == 0)
					table.addColumn(name, _Column.INTEGER, precision);
				else if (type.equals("NUMBER") && scale > 0)
					table.addColumn(name, _Column.DECIMAL, precision, scale);
				else if (type.equals("ROWID")) table.customRowid(name);
				else {
					throw new SQLException("Unknown datatype " + type + " for column " + name + " in table " + table.getName());
				}
			}
		} finally {
			if (rs != null) rs.close();
		}
	}

	public static void setSelectionsUpdatedTime(TransactionConnection con) throws SQLException {
		PreparedStatement p = null;
		try {
			p = con.prepareStatement(" UPDATE selections_updated SET time = SYSDATE ");
			p.execute();
		} finally {
			if (p != null) p.close();
		}
	}

	public static String returnValueByRowid(String table, String valueColumn, String rowid, TransactionConnection con) throws SQLException {
		SelectStatement statement = new SelectStatement(con);
		statement.select(valueColumn);
		statement.from(table);
		statement.where(" rowid = ? ", rowid);
		ResultSet rs = null;
		try {
			rs = statement.executeQuery();
			if (!rs.next()) throw new SQLException("Return Value By RowID did not produce any results.");
			String value = rs.getString(1);
			return value;
		} finally {
			if (rs != null) rs.close();
			statement.close();
		}
	}

	public static String returnAndSetNextId(_Table table, TransactionConnection con) throws SQLException {
		String nextval = "";
		String query = " SELECT " + Const.SEQUENCE_PREFIX + table.getName() + ".NEXTVAL FROM dual ";
		PreparedStatement p = null;
		ResultSet rs = null;
		try {
			p = con.prepareStatement(query);
			rs = p.executeQuery();
			rs.next();
			nextval = rs.getString(1);
			table.getUniqueNumericIdColumn().setValue(nextval);
			return nextval;
		} finally {
			if (rs != null) rs.close();
			if (p != null) p.close();
		}
	}
	

}
