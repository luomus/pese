package fi.hy.pese.framework.main.general.data;

import java.util.Iterator;
import java.util.List;

/**
 * A structure representing one application specific table in the database.
 */
public interface _Table extends Iterable<_Column> {

	String	UNINITIALIZED_TABLE						= "UNINITIALIZED TABLE";

	int		TABLEMANAGEMENT_NO_PERMISSIONS			= 0;
	int		TABLEMANAGEMENT_UPDATE_ONLY				= 1;
	int		TABLEMANAGEMENT_UPDATE_INSERT			= 2;
	int		TABLEMANAGEMENT_UPDATE_INSERT_DELETE	= 3;
	int		TABLEMANAGEMENT_INSERT_ONLY				= 4;

	/**
	 * @return Name of the table in the database, for example "poikanen"
	 */
	String getName();

	/**
	 * @return Fullname of the table, for example "poikanen.1"
	 */
	String getFullname();

	/**
	 * Returns a structure representing one of the columns of this table by it's name
	 * @param column
	 * @return column
	 * @throws IllegalArgumentException if a column matching the name has not been defined
	 */
	_Column get(String column) throws IllegalArgumentException;

	/**
	 * Returns a list of all the columns in this table
	 * @return
	 */
	Iterator<_Column> getColumns();

	/**
	 * Returns the number of columns, including rowid
	 * @return
	 */
	int numberOfColumns();

	/**
	 * Returns the unique numeric id column of this table. Not all tables have unique numeric id column.
	 * @return unique numeric id column
	 * @throws IllegalArgumentException if the table does not have an unique numeric id column
	 */
	_Column getUniqueNumericIdColumn() throws IllegalArgumentException;

	/**
	 * Tells wheter the table has an unique numeric id column
	 * @return true if has, false if not
	 */
	boolean hasUniqueNumericIdColumn();

	/**
	 * Tells if a collection (one or more) columns have been defined to form the key for this table
	 * @return true if key defined, false if not
	 */
	boolean hasImmutableKeys();

	/**
	 * Clears the values of all the columns of this table
	 */
	void clearAllValues();

	/**
	 * Tells if any of the columns (including rowid) of this table have value set, ie. something else than ""
	 * @return true if at least one value, false if all values equal ""
	 */
	boolean hasValues();

	/**
	 * Tells if any of the NORMAL columns (= not rowid, date added/modified, keys,etc) of this table have value set, EXCEPT those that are given as ignore parameters.
	 * @param ignore list of column names (short names, ie   "poikanen_id"  <- CORRECT,   "poikanen.1.poikanen_id" <- WRONG).
	 * @return true if all values (except rowid and those ignored columns) equal ""
	 */
	boolean hasValuesIgnoreMetadata(String... ignoreColumns);

	/**
	 * Returns a value telling what permissions the application has defined for this table, to be used with the Tablemanagement -functionality. <br>
	 * The value is one of: <br>
	 * TABLEMANAGEMENT_NO_PERMISSIONS = 0 <br>
	 * TABLEMANAGEMENT_UPDATE_ONLY = 1 <br>
	 * TABLEMANAGEMENT_UPDATE_INSERT = 2 <br>
	 * TABLEMANAGEMENT_UPDATE_INSERT_DELETE = 3 <br>
	 * <br>
	 * The default is NO_PERMISSIONS.
	 * @return tablemanagement functionality permissions
	 */
	int getTablemanagementPermissions();

	/**
	 * Convenience method, short for <code> column(Const.ROWID).getValue() </code>
	 * @return rowid
	 */
	String getRowidValue();

	/**
	 * Convenience method, short for <code> column(Const.ROWID).setValue() </code>
	 */
	void setRowidValue(String rowid);

	/**
	 * Tells if values of this table equal the values of the other table. Throws an exception if table structures do not match.
	 * Ignores rowid, id and date added/updated -columns, and those columns (defined by their name) that are given as ignoreColumns parameter.
	 * @param otherTable
	 * @param ignoreColumns
	 * @return
	 * @throws IllegalArgumentException
	 */
	boolean equalsIgnoreMetadata(_Table otherTable, String... ignoreColumns) throws IllegalArgumentException;

	/**
	 * Converts values of all columns to HTML entities. For more details see Utils.toHTMLEntities(String)
	 */
	void convertAllValuesToHTMLEntities();

	/**
	 * Tells if the table has been initialized (is in use by the implementing application)
	 * @return true if is initialized, false if not
	 */
	boolean isInitialized();

	/**
	 * Copies values from originTable to this table. EXCEPT for rowid !
	 * @param originTable
	 */
	void setValues(_Table originTable);

	/**
	 * Returns the column of this table that references the table given as parameter (if any)
	 * @param anotherTable
	 * @return
	 * @throws IllegalArgumentException if no reference defined
	 */
	_Column getReferencingColumn(_Table anotherTable) throws IllegalArgumentException;

	/**
	 * If this table is part of IndexedTablegroup, returns the index of the table. Returns 0 othervise.
	 * @return
	 * @see _IndexedTablegroup
	 */
	int getIndex();

	/**
	 * Returns all the keys. If hasUniquenumericKey() returns a list with size = 1, else returns a list of immutableKeys.
	 * @return
	 */
	List<_Column> getKeys();

	boolean hasColumn(String string);

	List<_Column> getColumns(String ... columnNames);

}
