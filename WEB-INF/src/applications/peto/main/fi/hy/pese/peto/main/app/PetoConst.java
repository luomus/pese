package fi.hy.pese.peto.main.app;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import fi.hy.pese.framework.main.app.functionality.validate.PesimistulosValidationDefinition;
import fi.hy.pese.framework.main.app.functionality.validate.PesimistulosValidationDefinition.Havainto;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.main.general.data._Table;
import fi.luomus.commons.utils.CoordinateConverter;
import fi.luomus.commons.utils.Utils;

public class PetoConst {
	
	public static final Collection<String> ALLOWED_NESTING_SPECIES = Utils.collection(
			"ACCGEN", "ACCNIS", "AEGFUN", "AQUCHR", "AQUCLA", "ASIFLA", "ASIOTU", "BUBBUB", "BUTBUT", "BUTLAG", "CIRAER",
			"CIRCYA", "CIRMAC", "CIRPYG", "FALCOL", "FALPER", "FALRUS", "FALSUB", "FALTIN", "GLAPAS", "MILMIG", "NYCSCA",
			"PANHAL", "PERAPI", "STRALU", "STRNEB", "STRURA", "SURULU" );
	
	public static final Collection<String> ALLOWED_RAKENTANUT_LAJI = Utils.collection(
			"ACCGEN", "ACCNIS", "AEGFUN", "AQUCHR", "AQUCLA", "ASIFLA", "ASIOTU", "BUBBUB", "BUTBUT", "BUTLAG", "CIRAER",
			"CIRCYA", "CIRMAC", "CIRPYG", "COLPAL", "CORONE", "CORRAX", "DENMAJ", "DRYMAR", "FALCOL", "FALPER", "FALRUS",
			"FALSUB", "FALTIN", "FORRUF", "GARGLA", "GLAPAS", "HALALB", "MILMIG", "NYCSCA", "PANHAL", "PERAPI", "PERINF",
			"PICPIC", "PICTRI", "SCIVUL", "STRALU", "STRNEB", "STRURA", "STUVUL", "SURULU", "CORONEX");
	
	public static final Collection<String> DEAD_NEST_CODES = Utils.collection("5", "6", "7");
	
	public static final Collection<String>	TARKASTUS_FIELDS_TO_BE_PREFILLED_FOR_INSERTS	= Utils.collection(
			"tarkastaja", "pesa_sijainti", "pesa_korkeus", "pesa_korkeus_tarkkuus", "puu_elavyys", "puu_korkeus", "puu_korkeus_tarkkuus", "puusto", "puusto_kasittelyaste",
			"maasto_1", "maasto_2", "rauhoitustaulu", "pesa_etaisyys_runko", "pesa_etaisyys_runko_tark", "pesiva_laji", "kommentti_edellinen_laji" );
	
	public static final String		TAULUT											= "taulut";
	public static final String		APUTAULU										= "aputaulu";
	public static final String		K_E												= "k_e";
	
	public static final String		JOINS											= joins();
	
	private static String joins() {
		StringBuilder joins = new StringBuilder();
		joins.append("       pesa.id        =  tarkastus.pesa         ");
		joins.append(" AND   tarkastus.id   =  kaynti.tarkastus(+)    ");
		return joins.toString();
	}
	
	public static final String	ORDER_BY	= orderBy();
	
	private static String orderBy() {
		return " pesa.kunta, pesa.kyla, pesa.nimi, pesa.id, tarkastus.vuosi DESC, tarkastus.id, kaynti.pvm ";
	}
	
	
	
	// public static final Collection<String> PESAN_KUNTO__DEAD = Utils.stringCollection("U", "D", "E", "F", "R", "G", "H", "I");
	// public static final Collection<String> PESAN_KUNTO__ALIVE = Utils.stringCollection("P", "O", "K", "T", "A", "J");
	// public static final Collection<String> PESIMISTULOS_SUCCESS = Utils.stringCollection("P", "R", "L");
	// public static final Collection<String> PESIMISTULOS_IN_RISING_SUCCESS_ORDER = Utils.stringCollection("U", "A", "K", "M", "P", "R", "L", "1", "2", "3", "7", "8", "9");
	
	public static final String	INVALID_TARK_VUOSI_TARK_PVM_YYYY	= "INVALID_TARK_VUOSI_TARK_PVM_YYYY";
	public static final String	INVALID_RAK_VUOSI					= "INVALID_RAK_VUOSI";
	public static final String	INVALID_LOYT_VUOSI					= "INVALID_LOYT_VUOSI";
	public static final String  INVALID_RAK_TAI_LOYT_VUOSI = "INVALID_RAK_TAI_LOYT_VUOSI";
	public static final String	PESA_IS_DEAD_CAN_NOT_INSERT			= "PESA_IS_DEAD_CAN_NOT_INSERT";
	public static final String	INVALID_REPORT_YEARS				= "INVALID_REPORT_YEARS";
	public static final String	NOT_INSPECTED_BUT_VALUES_GIVEN		= "NOT_INSPECTED_BUT_VALUES_GIVEN";
	public static final String  NEST_STATE_IS_DEAD_BUT_NOT_MARKED_DEAD  = "NEST_STATE_IS_DEAD_BUT_NOT_MARKED_DEAD";
	public static final String  ALL_KAYNTI_NOT_FROM_SAME_YEAR = "ALL_KAYNTI_NOT_FROM_SAME_YEAR";
	public static final String  INVALID_TARK_VUOSI_KAYNTI_VUOSI = "INVALID_TARK_VUOSI_KAYNTI_VUOSI";
	public static final String  INVALID_KAYNTI_VUOSI_LOYT_VUOSI = "INVALID_KAYNTI_VUOSI_LOYT_VUOSI";
	public static final String  INVALID_KAYNTI_VUOSI_RAK_VUOSI = "INVALID_KAYNTI_VUOSI_RAK_VUOSI";
	public static final String  PESIVA_LAJI_REQUIRED_WITH_THIS_PESIMISTULOS = "PESIVA_LAJI_REQUIRED_WITH_THIS_PESIMISTULOS";
	public static final String  PESIVA_LAJI_MUST_BE_EMPTY_WITH_THIS_PESIMISTULOS = "PESIVA_LAJI_MUST_BE_EMPTY_WITH_THIS_PESIMISTULOS";
	public static final String  PESA_CAN_NO_BE_HIGHER_THAN_TREE_HEIGHT = "PESA_CAN_NO_BE_HIGHER_THAN_TREE_HEIGHT";
	public static final String  PUU_LAJI_CAN_NOT_BE_GIVEN_FOR_THIS_TYPE_OF_NEST = "PUU_LAJI_CAN_NOT_BE_GIVEN_FOR_THIS_TYPE_OF_NEST";
	public static final String  RAK_LAJI_CAN_NOT_BE_GIVEN_FOR_THIS_TYPE_OF_NEST = "RAK_LAJI_CAN_NOT_BE_GIVEN_FOR_THIS_TYPE_OF_NEST";
	public static final String  INSPECTION_FOR_YEAR_ALREADY_EXISTS = "INSPECTION_FOR_YEAR_ALREADY_EXISTS";
	// public static final String REPORT_PESIMISTULOKSET = "report_pesimistulokset";
	// public static final String REPORT_TARKASTAMATTOMAT = "report_tarkastamattomat";
	public static final String MAKSIMISSAAN_KAKSI_MAASTOKOODIA ="maksimissaan_kaksi_maastokoodia";
	public static final String THIS_NEST_STATUS_CODE_CAN_BE_USED_ONLY_FOR_NESTS_THAT_ARE_IN_TREE = "THIS_NEST_STATUS_CODE_CAN_BE_USED_ONLY_FOR_NESTS_THAT_ARE_IN_TREE";
	public static final String THIS_NEST_STATUS_CODE_CAN_BE_USED_ONLY_FOR_NESTS_THAT_ARE_ON_GROUND = "THIS_NEST_STATUS_CODE_CAN_BE_USED_ONLY_FOR_NESTS_THAT_ARE_ON_GROUND";
	public static final String MUST_HAVE_NESTING_SEQUENCE_MINUS_ONE = "MUST_HAVE_NESTING_SEQUENCE_MINUS_ONE";
	public static final String NESTING_SEQUENCE_USED_MORE_THAN_ONCE = "NESTING_SEQUENCE_USED_MORE_THAN_ONCE";
	
	public static final String	REPORT_HAVAINNOIJALISTAUS	= "report_havainnoijalistaus";
	
	public static final String	REPORT_KANSILEHDET	= "report_kansilehdet";
	
	public static final Object	SEURANTARUUTUMANAGEMENT_PAGE	= "seurantaruutumanagement";
	
	public static final Object	COPY_SEURANTARUUDUT	= "copy_seurantaruudut";
	
	public static final String	SUCCESS_COPY_SEURANTARUUDUT	= "success_copy_seurantaruudut";
	
	public static final String	SEURANTARUUDULLA	= "Seurantaruudulla";
	
	public static final String	MAASTOKOODIT_EIVÄT_SAA_OLLA_SAMOJA	= "MAASTOKOODIT_EIVÄT_SAA_OLLA_SAMOJA";
	
	public static final String	MAASTOKOODIA_2_EI_SAA_ANTAA_JOS_KOODIA_1_EI_OLE_ANNETTU	= "MAASTOKOODIA_2_EI_SAA_ANTAA_JOS_KOODIA_1_EI_OLE_ANNETTU";
	
	public static final String	EI_SAA_ANTAA_JOS_PESA_EI_PUUSSA	= "EI_SAA_ANTAA_JOS_PESA_EI_PUUSSA";
	
	public static final String	NESTING_SPECIES_NOT_AMONG_ALLOWED_SPECIES	= "NESTING_SPECIES_NOT_AMONG_ALLOWED_SPECIES";
	
	public static final String	COUNTS	= "COUNTS";
	
	public static final String	LIIAN_ISO_PESYE	= "LIIAN_ISO_PESYE";
	
	public static final String	EI_RAJA_ARVOA	= "EI_RAJA_ARVOA";
	
	public static final String	PESIVAT_LAJIT	= "pesivat_lajit";
	public static final String	RAKENTAVAT_LAJIT	= "rakentavat_lajit";
	
	public static void removeTarkastusValuesButKeepSpecific(_PesaDomainModelRow tarkastus) {
		Map<String, String> values = specificValuesToMap(tarkastus.getTarkastus());
		tarkastus.getTarkastus().clearAllValues();
		tarkastus.getKaynnit().clearAllValues();
		restoreSpecificValues(tarkastus.getTarkastus(), values);
	}
	
	private static void restoreSpecificValues(_Table tarkastus, Map<String, String> values) {
		for (String f : TARKASTUS_FIELDS_TO_BE_PREFILLED_FOR_INSERTS) {
			tarkastus.get(f).setValue(values.get(f));
		}
	}
	
	private static Map<String, String> specificValuesToMap(_Table tarkastus) {
		Map<String, String> values = new HashMap<>();
		for (String f : TARKASTUS_FIELDS_TO_BE_PREFILLED_FOR_INSERTS) {
			values.put(f, tarkastus.get(f).getValue());
		}
		return values;
	}
	
	public static void kirjekyyhkyModelToData(Map<String, String> datamap,  _PesaDomainModelRow kirjekyyhkyparameters) {
		String leveys = datamap.get(Const.UPDATEPARAMETERS + ".pesa.leveys");
		String pituus = datamap.get(Const.UPDATEPARAMETERS + ".pesa.pituus");
		
		String koord_tyyppi = kirjekyyhkyparameters.getPesa().get("koord_ilm_tyyppi").getValue();
		_Column levCol = null;
		_Column pitCol = null;
		if (koord_tyyppi.equals("Y")) {
			levCol = kirjekyyhkyparameters.getPesa().get("yht_leveys");
			pitCol = kirjekyyhkyparameters.getPesa().get("yht_pituus");
		} else if (koord_tyyppi.equals("E")) {
			levCol = kirjekyyhkyparameters.getPesa().get("eur_leveys");
			pitCol = kirjekyyhkyparameters.getPesa().get("eur_pituus");
		} else if (koord_tyyppi.equals("A")) {
			levCol = kirjekyyhkyparameters.getPesa().get("yht_leveys");
			pitCol = kirjekyyhkyparameters.getPesa().get("yht_pituus");
			koord_tyyppi = "Y";
		}
		if (levCol != null && pitCol != null) {
			convertCoordinates(kirjekyyhkyparameters, leveys, pituus, koord_tyyppi);
			levCol.setValue(leveys);
			pitCol.setValue(pituus);
		}
		
		kirjekyyhkyparameters.getTarkastus().get("tarkastettu").setValue("K");
	}
	
	private static void convertCoordinates(_PesaDomainModelRow kirjekyyhkyparameters, String leveys, String pituus, String koord_tyyppi) {
		try {
			CoordinateConverter converter = new CoordinateConverter();
			if (koord_tyyppi.equals("Y")) {
				converter.setYht_leveys(leveys);
				converter.setYht_pituus(pituus);
			} else if (koord_tyyppi.equals("E")) {
				converter.setEur_leveys(leveys);
				converter.setEur_pituus(pituus);
			}
			converter.convert();
			kirjekyyhkyparameters.getPesa().get("yht_leveys").setValue(converter.getYht_leveys().toString());
			kirjekyyhkyparameters.getPesa().get("yht_pituus").setValue(converter.getYht_pituus().toString());
			kirjekyyhkyparameters.getPesa().get("eur_leveys").setValue(converter.getEur_leveys().toString());
			kirjekyyhkyparameters.getPesa().get("eur_pituus").setValue(converter.getEur_pituus().toString());
			kirjekyyhkyparameters.getPesa().get("ast_leveys").setValue(converter.getAst_leveys().toString());
			kirjekyyhkyparameters.getPesa().get("ast_pituus").setValue(converter.getAst_pituus().toString());
		} catch (Exception e) {
			// Something wrong with coordinates... we do nothing, validation will pick this up later.
		}
	}
	
	// public static boolean firstPesimistulosBetterThanSecond(String thisPesimistulos, String bestPesimistulos) {
	// if (bestPesimistulos.equals("")) return true;
	// if (thisPesimistulos.equals("")) return false;
	// if (!PESIMISTULOS_IN_RISING_SUCCESS_ORDER.contains(thisPesimistulos)) throw new IllegalStateException("Pesimistulos code: '"+thisPesimistulos+"' not found");
	// if (!PESIMISTULOS_IN_RISING_SUCCESS_ORDER.contains(bestPesimistulos)) throw new IllegalStateException("Pesimistulos code: '"+bestPesimistulos+"' not found");
	// int thisIndex = 0;
	// int bestIndex = 0;
	// for (String code : PESIMISTULOS_IN_RISING_SUCCESS_ORDER) {
	// if (code.equals(thisPesimistulos)) break;
	// thisIndex++;
	// }
	// for (String code : PESIMISTULOS_IN_RISING_SUCCESS_ORDER) {
	// if (code.equals(bestPesimistulos)) break;
	// bestIndex++;
	// }
	// return thisIndex > bestIndex;
	// }
	
	
	
	private static final PesimistulosValidationDefinition PESIMISTULOS_A = new PesimistulosValidationDefinition();
	private static final PesimistulosValidationDefinition PESIMISTULOS_B = new PesimistulosValidationDefinition().setVaaditut(Havainto.EMOJA);
	private static final PesimistulosValidationDefinition PESIMISTULOS_C = PESIMISTULOS_B;
	private static final PesimistulosValidationDefinition PESIMISTULOS_D = new PesimistulosValidationDefinition().setSallitut(Havainto.EMOJA);
	private static final PesimistulosValidationDefinition PESIMISTULOS_E = PESIMISTULOS_D;
	private static final PesimistulosValidationDefinition PESIMISTULOS_F = PESIMISTULOS_D;
	private static final PesimistulosValidationDefinition PESIMISTULOS_K = PESIMISTULOS_D;
	private static final PesimistulosValidationDefinition PESIMISTULOS_L = new PesimistulosValidationDefinition().setSallitut(Havainto.EMOJA, Havainto.HAUTOVA, Havainto.SIRUJA);
	private static final PesimistulosValidationDefinition PESIMISTULOS_M = new PesimistulosValidationDefinition().setVaaditut(Havainto.SIRUJA, Havainto.MUNIA, Havainto.HAUTOVA, Havainto.EMOJA);
	private static final PesimistulosValidationDefinition PESIMISTULOS_N = PESIMISTULOS_M;
	private static final PesimistulosValidationDefinition PESIMISTULOS_O = PESIMISTULOS_N;
	private static final PesimistulosValidationDefinition PESIMISTULOS_P = new PesimistulosValidationDefinition().setSallitut(Havainto.EMOJA, Havainto.HAUTOVA, Havainto.SIRUJA, Havainto.MUNIA).setVaaditut(Havainto.KUOLLEITA, Havainto.PESAP);
	private static final PesimistulosValidationDefinition PESIMISTULOS_Q = new PesimistulosValidationDefinition().setSallitut(Havainto.EMOJA, Havainto.HAUTOVA, Havainto.SIRUJA, Havainto.MUNIA, Havainto.KUOLLEITA).setVaaditut(Havainto.PESAP);
	private static final PesimistulosValidationDefinition PESIMISTULOS_R = PESIMISTULOS_Q;
	private static final PesimistulosValidationDefinition PESIMISTULOS_T = PESIMISTULOS_P;
	private static final PesimistulosValidationDefinition PESIMISTULOS_V = new PesimistulosValidationDefinition().setSallitut(Havainto.EMOJA, Havainto.HAUTOVA, Havainto.SIRUJA, Havainto.MUNIA, Havainto.KUOLLEITA, Havainto.PESAP).setVaaditut(Havainto.LENTOP);
	private static final PesimistulosValidationDefinition PESIMISTULOS_Y = new PesimistulosValidationDefinition().setVaaditut(Havainto.EMOJA, Havainto.HAUTOVA);
	private static final PesimistulosValidationDefinition PESIMISTULOS_X = new PesimistulosValidationDefinition();
	
	public static Map<String, PesimistulosValidationDefinition> PESIMISTULOS_VALIDATION_DEFINITIONS = initPesimistulosValidationDefinitions();
	
	private static HashMap<String, PesimistulosValidationDefinition> initPesimistulosValidationDefinitions() {
		HashMap<String, PesimistulosValidationDefinition> map = new HashMap<>();
		map.put("A", PESIMISTULOS_A);
		map.put("B", PESIMISTULOS_B);
		map.put("C", PESIMISTULOS_C);
		map.put("D", PESIMISTULOS_D);
		map.put("E", PESIMISTULOS_E);
		map.put("F", PESIMISTULOS_F);
		map.put("K", PESIMISTULOS_K);
		map.put("L", PESIMISTULOS_L);
		map.put("M", PESIMISTULOS_M);
		map.put("N", PESIMISTULOS_N);
		map.put("O", PESIMISTULOS_O);
		map.put("P", PESIMISTULOS_P);
		map.put("Q", PESIMISTULOS_Q);
		map.put("R", PESIMISTULOS_R);
		map.put("T", PESIMISTULOS_T);
		map.put("V", PESIMISTULOS_V);
		map.put("Y", PESIMISTULOS_Y);
		map.put("X", PESIMISTULOS_X);
		return map;
	}
	
	public static final String	PESIMISTULOS_JA_HAVAINTO_RISTIRIIDASSA	= "PESIMISTULOS_JA_HAVAINTO_RISTIRIIDASSA";
	
	public static final String	EI_TARPEEKSI_HAVAINTOJA_PESIMISTULOKSELLE	= "EI_TARPEEKSI_HAVAINTOJA_PESIMISTULOKSELLE_";
	
	public static final String	POIKASTEN_MAARA_ILMOITETTAVA_JOS_POIKASIA_MITATTU	= "POIKASTEN_MAARA_ILMOITETTAVA_JOS_POIKASIA_MITATTU";
	
	public static final String	EI_SAA_ILMOITTAA_KUORIUTUMATTOMIA_TALLA_PESIMISTULOKSELLA	= "EI_SAA_ILMOITTAA_KUORIUTUMATTOMIA_TALLA_PESIMISTULOKSELLA";
	
	public static final String	PESA_ON_ILMOITETTAVA_TUHOUTUNEEKSI_TATA_EPAONNI_KOODIA_KAYTETTAESSA	= "PESA_ON_ILMOITETTAVA_TUHOUTUNEEKSI_TATA_EPAONNI_KOODIA_KAYTETTAESSA";
	
	public static final String	AINAKIN_YKSI_KUNTO_ILMOITETTAVA	= "AINAKIN_YKSI_KUNTO_ILMOITETTAVA";
	
	public static final String	PUULAJI_ILMOITETTAVA_JOS_ELAVYYS_ILMOITETTU	= "PUULAJI_ILMOITETTAVA_JOS_ELAVYYS_ILMOITETTU";
	
	public static final String	MUULAJI_PITAA_OLLA_PESIMISTULOS_X	= "MUULAJI_PITAA_OLLA_PESIMISTULOS_X";
	
}
