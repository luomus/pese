package fi.hy.pese.framework.main.app.functionality;

import java.sql.SQLException;
import java.sql.SQLSyntaxErrorException;
import java.util.Collection;
import java.util.HashSet;

import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.app.functionality.validate.TablemanagementValidator;
import fi.hy.pese.framework.main.app.functionality.validate._Validator;
import fi.hy.pese.framework.main.db.RowsToData_Single_Table_ResultHandler;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.consts.Success;
import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._Row;
import fi.hy.pese.framework.main.general.data._Table;

public class TablemanagementFunctionality<T extends _Row> extends BaseFunctionality<T> {

	private final Collection<String> insertModePages = new HashSet<>();

	public TablemanagementFunctionality(_Request<T> request) {
		super(request);
	}

	public TablemanagementFunctionality(_Request<T> request, String ... insertModePages) {
		super(request);
		for (String page : insertModePages) {
			this.insertModePages.add(page.toLowerCase());
		}
	}

	@Override
	public _Validator validator() {
		return new TablemanagementValidator<>(request);
	}

	@Override
	public void preProsessingHook() throws Exception {
		if (!data.contains(Const.TABLE))  throw new IllegalArgumentException("No table given.");
		String tablename = data.get(Const.TABLE).toLowerCase();

		_Table searchparameters = data.searchparameters().getTableByName(tablename);
		if (searchparameters.getTablemanagementPermissions() == _Table.TABLEMANAGEMENT_NO_PERMISSIONS) {
			throw new UnsupportedOperationException("No permissions to manage table " +tablename);
		}

		data.set(Const.TABLEMANAGEMENT_PERMISSIONS, Integer.toString(searchparameters.getTablemanagementPermissions()));

		if (data.action().equals("")) {
			if (updateParameterGiven()) {
				data.setAction(Const.UPDATE);
			} else if (deleteParameterGiven()) {
				data.setAction(Const.DELETE);
			} else if (insertModePages.contains(tablename)) {
				data.setAction(Const.INSERT_PAGE);
			}
		}

		if (data.action().equals(Const.UPDATE)) {
			checkUpdatePermissions(tablename, searchparameters);
		} else if (data.action().equals(Const.INSERT)) {
			checkInsertPermissions(tablename, searchparameters);
		} else if (data.action().equals(Const.DELETE)) {
			checkDeletePermissions(tablename, searchparameters);
		}

		if (insertModePages.contains(tablename)) {
			data.set("insertmodepage", "yes");
		}
	}

	private boolean deleteParameterGiven() {
		return data.contains(Const.DELETE);
	}

	private boolean updateParameterGiven() {
		return data.contains(Const.UPDATE);
	}

	private void checkDeletePermissions(String tablename, _Table searchparameters) throws Exception {
		if (searchparameters.getTablemanagementPermissions() == _Table.TABLEMANAGEMENT_UPDATE_INSERT_DELETE) return;
		throw new UnsupportedOperationException("No permissions to delete from table " + tablename);
	}

	private void checkInsertPermissions(String tablename, _Table searchparameters) throws Exception {
		if (searchparameters.getTablemanagementPermissions() == _Table.TABLEMANAGEMENT_UPDATE_INSERT_DELETE) return;
		if (searchparameters.getTablemanagementPermissions() == _Table.TABLEMANAGEMENT_UPDATE_INSERT) return;
		if (searchparameters.getTablemanagementPermissions() == _Table.TABLEMANAGEMENT_INSERT_ONLY) return;
		throw new UnsupportedOperationException("No permissions to insert to table " + tablename);
	}

	private void checkUpdatePermissions(String tablename, _Table searchparameters) throws Exception {
		if (searchparameters.getTablemanagementPermissions() == _Table.TABLEMANAGEMENT_UPDATE_INSERT_DELETE) return;
		if (searchparameters.getTablemanagementPermissions() == _Table.TABLEMANAGEMENT_UPDATE_INSERT) return;
		if (searchparameters.getTablemanagementPermissions() == _Table.TABLEMANAGEMENT_UPDATE_ONLY) return;
		throw new UnsupportedOperationException("No permissions to update table " + tablename);
	}

	@Override
	public void afterSuccessfulValidation() throws Exception {
		String action = data.action();
		if (action.equals("") || action.equals("show_search")) return;

		String tablename = data.get(Const.TABLE);
		_Table table = data.updateparameters().getTableByName(tablename);
		_Table searchparamTable = data.searchparameters().getTableByName(tablename);

		if (action.equals(Const.UPDATE)) {
			update(table);
		} else if (action.equals(Const.DELETE)) {
			delete(table);
		} else if (action.equals(Const.INSERT)) {
			insert(table);
			if (!insertModePages.contains(tablename.toLowerCase())) {
				searchparamTable.setRowidValue(table.getRowidValue());
				data.setAction(Const.SEARCH);
			} else {
				data.setAction(Const.INSERT_PAGE);
				data.updateparameters().clearAllValues();
			}
		}

		try {
			dao.setSelectionsUpdateTime();
		} catch (SQLSyntaxErrorException e) { /* some systems don't have/need this feature */}

		if (!data.action().equals(Const.INSERT_PAGE)) {
			dao.executeSearch(searchparamTable, new RowsToData_Single_Table_ResultHandler<>(data, tablename));
		}
	}

	protected void insert(_Table table) throws SQLException {
		if (table.hasUniqueNumericIdColumn()) {
			table.getUniqueNumericIdColumn().setValue(dao.returnAndSetNextId(table));
		}
		for (_Column c : table) {
			if (c.getFieldType() == _Column.USER_ID_COLUMN) {
				c.setValue(data.userName());
				break;
			}
		}
		dao.executeInsert(table);
		data.setSuccessText(Success.INSERT);
	}

	protected void delete(_Table table) throws SQLException {
		request.dao().executeDelete(table);
		data.setSuccessText(Success.DELETE);
	}

	protected void update(_Table table) throws SQLException {
		request.dao().executeUpdate(table);
		data.setSuccessText(Success.UPDATE);
	}

	@Override
	public void afterFailedValidation() throws Exception {
		String tablename = data.get(Const.TABLE);
		if (data.action().equals(Const.UPDATE)) {
			data.newResultRow(data.updateparameters()); // Change faulty updateparameters into a result row for corrections
		} else if (data.action().equals(Const.DELETE)) { // Delete validation failed (unusual...); execute the search again to update the view
			_Table searchparameters = data.searchparameters().getTableByName(tablename);
			dao.executeSearch(searchparameters, new RowsToData_Single_Table_ResultHandler<>(data, tablename));
		}
	}

}
