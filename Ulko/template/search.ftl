<#include "header.ftl">

<h1>${texts.search_title}</h1>

<@statustexts system texts />
<@errorlist errors texts />

<#if system.action == "search">
  <#if results?size == 0> 
    Haku ei tuottanut tuloksia 
  <#else> 
    Löytyi ${results?size} hakutulosta. <br /> <br />
  </#if>
</#if>

<#-- --------------------------------------------------------------------------------------------------------------------------------------------- -->
<#-- -------------------------- RESULTS ---------------------------------------------------------------------------------------------------------- -->
<#assign resultcolumns = [ "pyynto.id", "pyynto.muutos_pvm", "loyto.scheme", "loyto.rengas", "pyynto.loytaja_nimi", "loyto.kunta", "rengastus.euring_paikka", "pyynto.kommentti"  ]>

<#if (results?size > 0)>
 
 
  <table id="resultsTable" class="tablesorter"> 
    <thead> 
      <tr> 
        <#-- ---------- result field descriptions ----------------------- -->
          <#list resultcolumns as colName> <#assign column = searchparameters.getColumn(colName)>
              <th <#if column.datatype == INTEGER || column.datatype == DECIMAL>class="{sorter: 'digit'}"</#if>  >${texts[colName]!colName}</th>
          </#list>
        <#-- ----------------------- ----------------------- ------------ -->
      </tr>
    </thead>
    
    <tbody>
    <#list results as result>  
      <tr class="linkki" onclick="location.href = '${system.baseURL}?id=${result.pyynto.id}'"> <#list resultcolumns as colName> <#assign column = result.getColumn(colName)> <#if column.inputType == "SELECT"> <td>${ column.value } - ${ selections[column.assosiatedSelection][ column.value ] }</td> <#else> <td>${column.value}</td> </#if> </#list></tr> 
    </#list>
    </tbody>
  </table>
  
  <br /><br /><br />
  
</#if>
<#-- -------------------------- result listing ends ---------------------------------------------------------------------------------------------- -->
<#-- --------------------------------------------------------------------------------------------------------------------------------------------- -->

<#list files as file>
  <#if system.action == "print_pdf">
    <span class="file"> <a target="_new" href="${system.baseURL}?page=filemanagement&amp;action=show&amp;folder=pdf&amp;filename=${file}">${file}</a> </span> <br />
  <#else>
    <span class="file"> <a target="_new" href="${system.baseURL}?page=filemanagement&amp;action=show&amp;folder=report&amp;filename=${file}">${file}</a> </span> <br />
  </#if>
</#list>


<#-- --------------------------------------------------------------------------------------------------------------------------------------------- -->
<#-- -------------------------- ADVANCED SEARCH -------------------------------------------------------------------------------------------------- -->



<form action="${system.baseURL}" method="post" name="advancedSearch">
<input type="hidden" name="page"   value="${system.page}" />
<input type="hidden" name="action" value="search" />
<input type="hidden" name="search_type" value="advancedSearch" />
<input type="hidden" name="selectedFields" id="selectedFields" value="" />
<input type="hidden" name="print_to_textfile" value="" id="print_to_textfile_advanced" />

<table>
  <tr>
    
    <td style="vertical-align: top;">
     
      <table id="advanced_search_table" <#if (data.search_type!"") != "advancedSearch"> </#if> >
        <thead>
          <tr>
      		<th>Tapauksen tiedoilla</th>
      		<th>Löydön tiedoilla</th>
      		<th>Rengastuksen tiedoilla</th>
          </tr>
        </thead>
        
        <tbody>
          <tr>
            <td valign="top"> <@searchFields searchparameters.pyynto texts /> </td>
            <td valign="top"> <@searchFields searchparameters.loyto texts /> </td>
            <td valign="top"> <@searchFields searchparameters.rengastus texts /> </td>
          </tr>
        </tbody>
      </table>
      
    </td>   
  </tr>
</table>

<br /><br /><br />

<input type="submit" value="${texts.search}" onclick=" generateAdvancedSearch(); if (confirmSearch(document.advancedSearch, '${texts.search_confirm_search_all}')) document.advancedSearch.submit(); else return false;" />
<input type="submit" value="${texts.clear}"  onclick="location.href = '${system.baseURL}?page=search'; return false; " />   

</form>

<br />


<#-- ------------------------- advanced search ends  --------------------------------------------------------------------------------------------- -->
<#-- --------------------------------------------------------------------------------------------------------------------------------------------- -->



<#include "footer.ftl">

