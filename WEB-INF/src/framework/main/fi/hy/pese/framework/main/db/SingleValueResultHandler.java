package fi.hy.pese.framework.main.db;

import java.sql.SQLException;

public class SingleValueResultHandler implements _ResultsHandler {

	public String value = null;

	@Override
	public void process(_ResultSet rs) throws SQLException {
		if (!rs.next()) throw new SQLException("Value not found");
		value = rs.getString(1);
		if (rs.next()) throw new SQLException("More than one value found");
	}

}
