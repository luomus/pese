
<#include "header.ftl">

<#if (data.kirjekyyhky!"") == "yes">
  <h1>${texts["tarkastus_title_kirjekyyhky_"+system.action+data.mode]}</h1>
<#else>
  <h1>${texts["tarkastus_title_"+system.action+data.mode]}</h1>
</#if>

<#if system.action != "insert_first">
<table>
  <tr>
  
    <#if results?has_content> 
    	<td>
        	<form action="${system.baseURL}" method="post">
        	<input type="hidden" name="page"   value="${system.page}" />
        	<input type="hidden" name="action" value="fill_for_update" />
           	Aikaisemmat pesintävuodet:
           	<select name="updateparameters.tarkastus.id">
              <#list results as i> 
                <#if i.tarkastus.id == updateparameters.tarkastus.id>
		          <option value="${i.tarkastus.id}" selected="selected">${i.tarkastus.vuosi} - ${i.tarkastus.pesiva_laji} </option>
		        <#else>
		          <option value="${i.tarkastus.id}" >${i.tarkastus.vuosi} - ${i.tarkastus.pesiva_laji}</option>
  		        </#if>
		      </#list>
           </select>
           <input type="submit" value="Tarkastele" />
        	</form>
    	</td>
    </#if>
    
    <#if system.action == "update">
    <td>
        <form action="${system.baseURL}" method="post">
        <input type="hidden" name="page" value="${system.page}" />
        <input type="hidden" name="action" value="fill_for_insert" />
        <input type="hidden" name="updateparameters.pesa.id" value="${updateparameters.pesa.id}" />
        
        Uusi tarkastus vuodelle:
        <input type="text" size="4" name="tark_vuosi" value="${data.tark_vuosi!""}" <@checkerror errors "tark_vuosi" /> /> 
        <input type="submit" value="Etene" />

        </form>
    </td>
    </#if>
    
    <#if updateparameters.pesa.id != "" && !data.kirjekyyhky??>
    	<td> 
      		<a target="_karttapaikka" href="https://asiointi.maanmittauslaitos.fi/karttapaikka/?share=customMarker&n=${updateparameters.pesa.eur_leveys}&amp;e=${updateparameters.pesa.eur_pituus}&amp;title=${updateparameters.pesa.nimi.getUrlEncodedValue()}(${updateparameters.pesa.id})">Lisää pesä kartalle</a> 
    	</td>
    </#if>
    
  </tr>
</table>
</#if>  
    
  
<#if lists.log??>
   <table style="width: 50%">
     <caption>Päivitysloki</caption>
     <tr>
       <th>Aika</th>
       <th>Käyttäjä</th>
       <th>Toiminto</th>
     </tr>
     <#list lists.log as e>
       <tr>
         <td>${e.time}</td>
         <td> <#if selections['ringers'].containsOption(e.user)>${selections['ringers'][e.user]}<#else>${e.user}</#if></td>
         <td>${e.action}</td>
       </tr>
     </#list>
   </table>
   <br />
 </#if>
 
 <#if (data.kirjekyyhky!"") == "yes">
 <form action="${system.baseURL}" method="post" onsubmit="return false;" name="palautalomake">
   <input type="hidden" name="page" value="kirjekyyhky_management" />
   <input type="hidden" name="action" value="return_for_corrections" />
   <input type="hidden" name="id" value="${data.id!""}" />
   <table style="width: 50%">
     <caption>Palauta lomake korjattavaksi</caption>
     <tr>
       <td style="vertical-align: middle;"> Viesti </td>
       <td> <textarea name="return_for_corrections_message" rows="2" cols="50"></textarea> </td>
       <td  style="vertical-align: middle;"> <button onclick="document.palautalomake.submit()">Palauta</button> </td>
     </tr>
   </table>
   </form>
 </#if>

<@statustexts system texts />
<@errorlist errors texts />
<@warninglist warnings texts />

<#if lists.ristiriidat??>
  <br />
  <b>Pesät joille on onnistunut tarkastus tälle vuodelle: &nbsp; &nbsp;</b> 
  <#list lists.ristiriidat as pesaid>
    <a style="color:red" href="${system.baseURL}?page=tarkastus&amp;action=fill_for_update&amp;updateparameters.pesa.id=${pesaid}&amp;updateparameters.tarkastus.vuosi=${updateparameters.tarkastus.vuosi}">&nbsp;${pesaid}&nbsp;</a>
     &nbsp;&nbsp;
  </#list>
</#if>

 <table class="noborders sameAsBackground" >
   <tr>
     <td style="width: 800px;">
        <h2>
        <!-- Toiminnon otsikko -->
        <#if system.action == "insert">
          <img src="${system.commonURL}/insert.gif" alt="Lisää" /> Uusi tarkastus: &nbsp;&nbsp; pesä: ${updateparameters.pesa.id} &nbsp;&nbsp; pesintävuosi: ${updateparameters.tarkastus.vuosi}
        </#if>
        <#if system.action == "update">
          <img src="${system.commonURL}/update.gif" alt="Muokkaa" /> Tarkastuksen muokkaus: &nbsp;&nbsp; pesä: ${updateparameters.pesa.id} &nbsp;&nbsp; pesintävuosi: ${updateparameters.tarkastus.vuosi}
        </#if>
        <#if system.action == "insert_first" && data.mode != "yhdyskunta">
          <img src="${system.commonURL}/insert.gif" alt="Uusi" /> Uuden pesän lisäys
        </#if>
        <#if system.action == "insert_first" && data.mode == "yhdyskunta">
          <img src="${system.commonURL}/insert.gif" alt="Uusi" /> Yhdyskunnan pesien ilmoitus
        </#if>
        </h2>
    </td> 
    <td style="vertical-align: middle;">
      <#if (data.kirjekyyhky!"") == "yes">
        <button onclick="document.tarkastusForm.submit()">${texts["tarkastus_submit_kirjekyyhky_"+system.action+data.mode]}</button>
      <#else>
        <button onclick="document.tarkastusForm.submit()">${texts["tarkastus_submit_"+system.action+data.mode]}</button>
      </#if>
    </td>
  </tr>
</table>

<div id="tarkastuslomake">


 
<form action="${system.baseURL}" method="post" onsubmit="return false;" name="tarkastusForm">
<input type="hidden" name="page" value="${system.page}" />
<input type="hidden" name="mode" value="${data.mode}" />
<input type="hidden" name="action" value="${system.action}" />
<input type="hidden" name="kirjekyyhky" value="${data.kirjekyyhky!""}" />
<input type="hidden" name="id" value="${data.id!""}" />
<input type="hidden" name="bypassed_warnings" id="bypassed_warnings" value="${data.bypassed_warnings!""}" /> 
<@inputHidden "updateparameters" updateparameters.pesa.rowid />
<@inputHidden "updateparameters" updateparameters.pesa.id />
<@inputHidden "updateparameters" updateparameters.tarkastus.rowid />
<@inputHidden "updateparameters" updateparameters.tarkastus.id />
<@inputHidden "updateparameters" updateparameters.tarkastus.vanha_kortti />
<@inputHidden "updateparameters" updateparameters.tarkastus.muninta_pvm />
<@inputHidden "updateparameters" updateparameters.tarkastus.taydellinen_munaluku />

<@inputHiddenAll "updateparameters" updateparameters.kaynnit /> 
<@inputHiddenAll "updateparameters" updateparameters.aikuiset /> 
<@inputHiddenAll "updateparameters" updateparameters.poikaset /> 
<@inputHiddenAll "updateparameters" updateparameters.munat /> 


<#if data.mode == "yhdyskunta">
	<#include "tarkastus_yhdyskunta.ftl">
<#else>
	<#include "tarkastus_pesa_vihrea.ftl">
</#if>

     <#if (data.kirjekyyhky!"") == "yes">
        <button onclick="document.tarkastusForm.submit()">${texts["tarkastus_submit_kirjekyyhky_"+system.action+data.mode]}</button>
      <#else>
        <button onclick="document.tarkastusForm.submit()">${texts["tarkastus_submit_"+system.action+data.mode]}</button>
      </#if>


<script type="text/javascript">
	  var form = document.forms['tarkastusForm'];
	  var nestID = form['updateparameters.pesa.id'];
	  var yhtLeveys = form['updateparameters.pesa.yht_leveys'];
	  var yhtPituus = form['updateparameters.pesa.yht_pituus'];
	  var eurLeveys = form['updateparameters.pesa.eur_leveys'];
	  var eurPituus = form['updateparameters.pesa.eur_pituus'];
	  var radius = form['CoordinateSearchRadius'];
</script>
   



<div id="pesanSijaintiValinta" class="hidden popupBoxElement">
<a href="#"> Sulje  <span class="icon">X</span> </a> 
<h3>Pesän sijainti</h3>
  <@selection selections "updateparameters" updateparameters.pesa.pesa_sijainti 2 false true true errors />
</div>

<div id="biotooppiValinta" class="hidden popupBoxElement">
<a href="#"> Sulje  <span class="icon">X</span> </a> 
<h3>Varsinainen pesimäbiotooppi</h3>
  <@selection selections "updateparameters" updateparameters.tarkastus.pesa_biotooppi 30 false true true errors /> 
</div>

<script type="text/javascript">

	$('#updateparameters\\.pesa\\.pesa_sijainti').val( '${updateparameters.pesa.pesa_sijainti}' );
	$('#updateparameters\\.tarkastus\\.pesa_biotooppi').val( '${updateparameters.tarkastus.pesa_biotooppi}' );
	
	$('#pesa_sijainti_muuta_fieldset').click(function() {
		$('#pesanSijaintiValinta').show(300); 
		return false;
	});
	$('#pesanSijaintiValinta').click(function () {
     	$(this).hide(300);
     	$('#pesan_sijainti_content').focus(); 
    	return false;
	});
	$('#updateparameters\\.pesa\\.pesa_sijainti').change(function () {
		$('#pesan_sijainti_content').html( $(this).find(":selected").text() );
		$('#pesan_sijainti_content').focus(); 
	});
	
	$('#biotooppi_muuta_fieldset').click(function() {
		$('#biotooppiValinta').show(300); 
		return false;
	});
	$('#biotooppiValinta').click(function () {
     	$(this).hide(300);
     	$('#biotooppi_content').focus(); 
    	return false;
	});
	$('#updateparameters\\.tarkastus\\.pesa_biotooppi').change(function () {
		$('#biotooppi_content').html( $(this).find(":selected").text() );
		$('#biotooppi_content').focus(); 
	});
	
	<#if (data.kirjekyyhky!"") != "yes">
	var koordMittaustapa = $('#updateparameters\\.pesa\\.koord_mittaustapa');
	if (koordMittaustapa.val() == undefined) {
		koordMittaustapa.val('K');
	}
	var koordMittaustapa = $('#updateparameters\\.pesa\\.koord_tarkkuus');
	if (koordMittaustapa.val() == '') {
		koordMittaustapa.val('1000');
	}
	</#if>
	
</script>


</form>
</div>

<#include "footer.ftl">

