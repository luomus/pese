package fi.hy.pese.saaksi.main.app;

import java.util.HashMap;
import java.util.Map;

import fi.hy.pese.framework.main.general.WarehouseGenerationDefinitionBuilder;
import fi.hy.pese.framework.main.general.WarehouseRowGenerator.NestIsDead;
import fi.hy.pese.framework.main.general.WarehouseRowGenerator.NotInspected;
import fi.hy.pese.framework.main.general.WarehouseRowGenerator.ResolveAtlasCode;
import fi.hy.pese.framework.main.general.WarehouseRowGenerator.ResolveCoordinates;
import fi.hy.pese.framework.main.general.WarehouseRowGenerator.ResolveCounts;
import fi.hy.pese.framework.main.general.WarehouseRowGenerator.ResolveDateAccuracy;
import fi.hy.pese.framework.main.general.WarehouseRowGenerator.ResolveLocality.LocalityKylaNimi;
import fi.hy.pese.framework.main.general.WarehouseRowGenerator.ResolveNestCount;
import fi.hy.pese.framework.main.general.WarehouseRowGenerator.ResolveSpecies;
import fi.hy.pese.framework.main.general.WarehouseRowGenerator.WarehouseGenerationDefinition;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.main.general.data._Table;
import fi.laji.datawarehouse.etl.models.dw.Coordinates;
import fi.laji.datawarehouse.etl.models.dw.Coordinates.Type;
import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException;
import fi.luomus.commons.containers.rdf.Qname;

public class SaaksiWarehouseDefinition {
	
	private static WarehouseGenerationDefinition definition;
	
	public static WarehouseGenerationDefinition instance() {
		if (definition == null) {
			definition = init();
		}
		return definition;
	}
	
	public static WarehouseGenerationDefinition init() {
		return new WarehouseGenerationDefinitionBuilder()
				.sourceId(new Qname("KE.42"))
				.collectionId(new Qname("HR.95"))
				.nestIsDead(new NestIsDead() {
					@Override
					public boolean get(_PesaDomainModelRow row) {
						return "D".equals(row.getColumn("tarkastus.epaonn").getValue());
					}
				})
				.notInspected(new NotInspected() {
					@Override
					public boolean get(_PesaDomainModelRow row) {
						return "J".equals(row.getColumn("tarkastus.vaihtop_laiminl").getValue());
					}
				})
				.yearColumnFullname("tarkastus.vuosi")
				.dateColumnFullname("tarkastus.tark_pvm")
				.siteTypeColumnFullName("olosuhde.pesan_sijainti")
				.municipalityColumnFullName("pesa.kunta")
				.coordinates(new ResolveCoordinates() {
					@Override
					public Coordinates get(_Table pesa) throws NumberFormatException, IllegalArgumentException, DataValidationException {
						Coordinates c = new Coordinates(pesa.get("eur_leveys").getDoubleValue(), pesa.get("eur_pituus").getDoubleValue(), Type.EUREF);
						String koordTark = pesa.get("koord_tyyppi").getValue();
						if ("88".equals(koordTark)) c.setAccuracyInMeters(1);
						else if ("90".equals(koordTark)) c.setAccuracyInMeters(100);
						else if ("70".equals(koordTark)) c.setAccuracyInMeters(1);
						else c.setAccuracyInMeters(1000);
						return c;
					}
				})
				.locality(new LocalityKylaNimi("kyla", "pesanimi"))
				.dateAccuracy(new ResolveDateAccuracy() {
					@Override
					public DateAccuracy get(_Table tarkastus) {
						if (tarkastus.getName().equals("kaynti")) return DateAccuracy.ACCURATE;
						String v = tarkastus.get("tark_pvm_tark").getValue();
						if ("1".equals(v) || v.isEmpty()) return DateAccuracy.ACCURATE;
						if ("2".equals(v)) return DateAccuracy.MONTH;
						return DateAccuracy.YEAR;
					}
				})
				.nestingSpecies(new ResolveSpecies() {
					@Override
					public String get(_PesaDomainModelRow row) {
						String v = row.getTarkastus().get("muulaji").getValue();
						if (v.isEmpty()) return "PANHAL";
						return v;
					}
				})
				.nestCount(new ResolveNestCount() {
					@Override
					public int get(_PesaDomainModelRow row) {
						String kunto = row.getColumn("tarkastus.kunto").getValue();
						String pesimistulos = row.getColumn("tarkastus.pesimistulos").getValue();
						if ("A".equals(pesimistulos) && "TAE".contains(kunto)) return 0;
						return 1;
					}
				})
				.atlasCode(new ResolveAtlasCode() {
					@Override
					public Qname get(_PesaDomainModelRow row) {
						String pesimistulos = row.getColumn("tarkastus.pesimistulos").getValue();
						return ATLAS_CODES.get(pesimistulos);
					}
				})
				.counts(new ResolveCounts() {
					@Override
					public Counts get(_Table kaynti) {
						Counts counts = new Counts();
						counts.adults = counts.value(kaynti.get("aikuisia"));
						counts.liveEggs = counts.value(kaynti.get("munia"), "8", "9");
						counts.livePullus = counts.value(kaynti.get("elavia"), "9");
						counts.deadPullus = counts.value(kaynti.get("kuolleita"), "9");
						counts.liveJuvenile = counts.value(kaynti.get("lentopoik"), "9");
						return counts;
					}
				})
				//.isSuspecious(null)
				//.shouldSecure(null)
				.build();
	}
	
	private static final Map<String, Qname> ATLAS_CODES;
	static {
		ATLAS_CODES = new HashMap<>(); // tarkastus.pesimistulos X=7 B=2 D=62 E=66 F=66 K=66 L=66 M=71 N=82 O=82 P=71 Q=82 R=82 T=71 V=82 Y=66
		ATLAS_CODES.put("X", new Qname("MY.atlasCodeEnum7"));
		ATLAS_CODES.put("B", new Qname("MY.atlasCodeEnum2"));
		ATLAS_CODES.put("D", new Qname("MY.atlasCodeEnum62"));
		ATLAS_CODES.put("E", new Qname("MY.atlasCodeEnum66"));
		ATLAS_CODES.put("F", new Qname("MY.atlasCodeEnum66"));
		ATLAS_CODES.put("K", new Qname("MY.atlasCodeEnum66"));
		ATLAS_CODES.put("L", new Qname("MY.atlasCodeEnum66"));
		ATLAS_CODES.put("M", new Qname("MY.atlasCodeEnum71"));
		ATLAS_CODES.put("N", new Qname("MY.atlasCodeEnum82"));
		ATLAS_CODES.put("O", new Qname("MY.atlasCodeEnum82"));
		ATLAS_CODES.put("P", new Qname("MY.atlasCodeEnum71"));
		ATLAS_CODES.put("Q", new Qname("MY.atlasCodeEnum82"));
		ATLAS_CODES.put("R", new Qname("MY.atlasCodeEnum82"));
		ATLAS_CODES.put("T", new Qname("MY.atlasCodeEnum71"));
		ATLAS_CODES.put("V", new Qname("MY.atlasCodeEnum82"));
		ATLAS_CODES.put("Y", new Qname("MY.atlasCodeEnum66"));
	}
	
}
