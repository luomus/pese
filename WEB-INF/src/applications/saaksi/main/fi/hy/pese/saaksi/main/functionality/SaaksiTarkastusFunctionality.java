package fi.hy.pese.saaksi.main.functionality;

import java.sql.SQLException;

import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.app.functionality.TarkastusFunctionality;
import fi.hy.pese.framework.main.app.functionality.validate._Validator;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data._IndexedTablegroup;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.main.general.data._Table;
import fi.hy.pese.saaksi.main.app.SaaksiConst;
import fi.hy.pese.saaksi.main.functionality.validator.SaaksiTarkastusValidator;
import fi.luomus.commons.tipuapi.TipuAPIClient;
import fi.luomus.commons.utils.CoordinateConverter;

public class SaaksiTarkastusFunctionality extends TarkastusFunctionality {
	
	public SaaksiTarkastusFunctionality(_Request<_PesaDomainModelRow> request) {
		super(request);
	}
	
	@Override
	public _Validator validator() {
		return new SaaksiTarkastusValidator(request);
	}
	
	@Override
	public void applicationSpecificDatamanipulation() throws Exception {
		String action = data.action();
		if (action.equals(Const.FILL_FOR_INSERT)) {
			selectWhatResultToUseForThisTarkYear();
			data.updateparameters().getTarkastus().clearAllValues();
			moveTarkVuosiFromDataToUpdateparameters();
		} else if (action.equals(Const.INSERT_FIRST) || action.equals(Const.INSERT) || action.equals(Const.UPDATE)) {
			preInsertUpdate(action);
		}
	}
	
	private void selectWhatResultToUseForThisTarkYear() throws Exception {
		if (data.results().size() < 1) throw new Exception("No results");
		int tark_vuosi = Integer.valueOf(data.get("tark_vuosi"));
		_PesaDomainModelRow useAsBase = null;
		for (_PesaDomainModelRow row : data.results()) {
			useAsBase = row;
			int row_tark_vuosi = Integer.valueOf(row.getTarkastus().get("tark_pvm").getDateValue().getYear());
			if (tark_vuosi > row_tark_vuosi) break;
		}
		if (useAsBase == null) throw new IllegalStateException();
		if (useAsBase == lastRow(data.results())) {
			useAsBase.getOlosuhde().setRowidValue("");
		}
		data.setUpdateparameters(data.copyOf(useAsBase));
	}
	
	private void preInsertUpdate(String action) throws Exception, SQLException {
		_PesaDomainModelRow params = data.updateparameters();
		convertCoordinates(params.getPesa());
		params.getOlosuhde().get("ilm_pvm").setValue(params.getTarkastus().get("tark_pvm"));
		setIds(action, params);
		String vuosi = params.getTarkastus().get("tark_pvm").getDateValue().getYear();
		params.getTarkastus().get("vuosi").setValue(vuosi);
		params.getVuosi().get("vuosi").setValue(vuosi);
		laaniKunnanPerusteella(params);
		// TODO laske ympäryys/halkaisija jos laskennallinen
		// TODO tark_pvm -> olosuhde ilm pvm
	}
	
	private void laaniKunnanPerusteella(_PesaDomainModelRow params) {
		String kunta = params.getPesa().get("kunta").getValue();
		String laaniNro = request.getTipuApiResource(TipuAPIClient.MUNICIPALITIES).getById(kunta).getNode("old-county").getContents();
		String laani = SaaksiConst.laaniNroToLaanitunnusMapping.get(laaniNro);
		params.getPesa().get("laani").setValue(laani);
	}
	
	private void setIds(String action, _PesaDomainModelRow params) throws SQLException {
		String pesa_id = null;
		String tarkastus_id = null;
		if (action.equals(Const.INSERT_FIRST)) {
			pesa_id = dao.returnAndSetNextId(params.getPesa());
			tarkastus_id = dao.returnAndSetNextId(params.getTarkastus());
		} else if (action.equals(Const.INSERT)) {
			pesa_id = params.getPesa().get("pesaid").getValue();
			tarkastus_id = dao.returnAndSetNextId(params.getTarkastus());
		} else if (action.equals(Const.UPDATE)) {
			pesa_id = params.getPesa().get("pesaid").getValue();
			tarkastus_id = params.getTarkastus().get("tarkastusid").getValue();
		}
		params.getVuosi().get("pesaid").setValue(pesa_id);
		params.getOlosuhde().get("pesaid").setValue(pesa_id);
		params.getTarkastus().get("pesaid").setValue(pesa_id);
		
		tarkIdTo(params.getPoikaset(), tarkastus_id);
		tarkIdTo(params.getKaynnit(), tarkastus_id);
	}
	
	private void tarkIdTo(_IndexedTablegroup map, String tarkastus_id) {
		for (_Table t : map.values()) {
			if (t.hasValues()) {
				try {
					t.get("tarkastusid").setValue(tarkastus_id);
				} catch (IllegalArgumentException e) {
					t.get("tarkastus").setValue(tarkastus_id);
				}
			}
		}
	}
	
	private void convertCoordinates(_Table pesa) throws Exception {
		CoordinateConverter converter = new CoordinateConverter();
		switch (SaaksiConst.KOORD_TYYPPI_MAPPING.get(pesa.get("koord_tyyppi").getValue())) {
			case YHT:
				converter.setYht_leveys(pesa.get("yht_leveys").getValue());
				converter.setYht_pituus(pesa.get("yht_pituus").getValue());
				break;
			case EUR:
				converter.setEur_leveys(pesa.get("eur_leveys").getValue());
				converter.setEur_pituus(pesa.get("eur_pituus").getValue());
				break;
			case AST:
				converter.setAst_leveys(pesa.get("ast_leveys").getValue());
				converter.setAst_pituus(pesa.get("ast_pituus").getValue());
				break;
			default: throw new IllegalArgumentException("Unknown coord type: " + pesa.get("koord_tyyppi").getValue());
		}
		converter.convert();
		pesa.get("yht_leveys").setValue(converter.getYht_leveys().toString());
		pesa.get("yht_pituus").setValue(converter.getYht_pituus().toString());
		pesa.get("eur_leveys").setValue(converter.getEur_leveys().toString());
		pesa.get("eur_pituus").setValue(converter.getEur_pituus().toString());
		pesa.get("ast_leveys").setValue(converter.getAst_leveys().toString());
		pesa.get("ast_pituus").setValue(converter.getAst_pituus().toString());
		pesa.get("des_leveys").setValue(converter.getDes_leveys().toString());
		pesa.get("des_pituus").setValue(converter.getDes_pituus().toString());
	}
	
	private void moveTarkVuosiFromDataToUpdateparameters() {
		data.updateparameters().getVuosi().get("vuosi").setValue(data.get("tark_vuosi"));
		data.updateparameters().getTarkastus().get("tark_pvm").setValue(".."+data.get("tark_vuosi"));
		data.updateparameters().getTarkastus().get("vuosi").setValue(data.get("tark_vuosi"));
	}
	
	@Override
	protected void doInsertFirst() throws Exception {
		_PesaDomainModelRow params = data.updateparameters();
		dao.executeInsert(params.getPesa());
		dao.executeInsert(params.getVuosi());
		fetchIdAndInsert(params.getOlosuhde());
		dao.executeInsert(params.getTarkastus());
		insertPoikaset(params);
		insertKaynnit(params);
	}
	
	@Override
	protected void doInsert() throws Exception {
		_PesaDomainModelRow params = data.updateparameters();
		updateIfChanges(params.getPesa());
		dao.executeInsert(params.getVuosi());
		insertIfChanges(params.getOlosuhde(), "ilm_pvm");
		dao.executeInsert(params.getTarkastus());
		insertPoikaset(params);
		insertKaynnit(params);
	}
	
	@Override
	protected void doUpdate() throws Exception {
		_PesaDomainModelRow params = data.updateparameters();
		updateIfChanges(params.getPesa());
		updateIfChanges(params.getVuosi());
		updateOrInsertOlosuhdeIfChanges(params, "ilm_pvm");
		updateIfChanges(params.getTarkastus());
		updatePoikaset(params);
		updateKaynnit(params);
	}
	
	@Override
	protected boolean prevOlosuhdeIsForThisYear(_Table current_tarkastus, _Table prev_olosuhde) {
		return prev_olosuhde.get("ilm_pvm").getDateValue().getYear().equals(current_tarkastus.get("tark_pvm").getDateValue().getYear());
	}
	
}
