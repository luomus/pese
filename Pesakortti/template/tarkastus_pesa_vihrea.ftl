
<div style="float: left;">


<h3>Perustiedot pesästä</h3>

<table class="osio paaosio">
	<tr>
		<td>
			<fieldset>
			<legend>Kunta</legend>
			<@inputSelectCombo "updateparameters" updateparameters.pesa.kunta true false errors />
			</fieldset>
		</td>
		<td>
			<fieldset>
			<legend>Lähempi paikka</legend>
			<@inputText "updateparameters" updateparameters.pesa.tarkempi_paikka true errors />
			</fieldset>
		</td>
	</tr>
	<tr>
		<td>
			<fieldset>
			<legend>Pesän nimi</legend>
			<@inputText "updateparameters" updateparameters.pesa.nimi true errors />
			</fieldset>
		</td>
		<td>
			<fieldset>
			<legend>Pesän ID</legend>
			<input type="text" readonly="readonly" class="disabled" value="${updateparameters.pesa.id}" />
			</fieldset>
		</td>
	</tr>
	<#if updateparameters.tarkastus.vanha_kortti.hasValue()>
	  <tr>
  		<td style="vertical-align: middle"> &nbsp; &nbsp;  Vuoden ${updateparameters.tarkastus.vuosi} tarkastuksen vanhan kortin numero: </td>
  		<td style="padding-left: 10px;">  <input type="text" readonly="readonly" class="disabled" value="${updateparameters.tarkastus.vanha_kortti}" /></td>
  	  </tr>
	</#if>
</table>



<table class="osio paaosio">
	<tr>
		<td>
			<fieldset>
			<legend>Esipainettu lomake</legend>
			<select name="updateparameters.pesa.lomake" size="1" >
    			<option value="K" <#if updateparameters.pesa.lomake == "K"> selected="selected" </#if> >Kyllä</option>
    			<option value="E" <#if  updateparameters.pesa.lomake == "" || updateparameters.pesa.lomake == "E"> selected="selected" </#if> >Ei</option>
    		</select>
			</fieldset>
		</td>
		<td>
			<fieldset>    
    		<legend>Lomakkeen vastaanottaja</legend>
    		<@inputSelectCombo "updateparameters" updateparameters.pesa.seur_tarkastaja true true errors />
			</fieldset>
		</td>
	</tr>
</table>  	


 
<table class="osio paaosio">
	<caption>Koordinaatit</caption>
	<tr>
  		<td>
  			<table id="koordinaatit">
  				<tr>
    				<td> </td>
    				<td> Pohjois- </td>
    				<td> Itä- </td>
  				</tr>
  				<tr>
    				<td> <@radio "updateparameters" updateparameters.pesa.koord_ilm_tyyppi "Y" errors /> Yhtenäiskoordinaatit </td>
    				<td> <@inputText "updateparameters" updateparameters.pesa.yht_leveys true errors /> </td>
    				<td> <@inputText "updateparameters" updateparameters.pesa.yht_pituus true errors /> </td>
  				</tr>
  				<tr>
    				<td> <@radio "updateparameters" updateparameters.pesa.koord_ilm_tyyppi "E" errors /> EUREF-koordinaatit </td>
    				<td> <@inputText "updateparameters" updateparameters.pesa.eur_leveys true errors /> </td>
    				<td> <@inputText "updateparameters" updateparameters.pesa.eur_pituus true errors /> </td>
  				</tr>
  				<tr>
  				  <td> <@radio "updateparameters" updateparameters.pesa.koord_ilm_tyyppi "NULL" errors /> (tyhjä) </td>
  				</tr>
  			</table>
  		</td>
  		<td>
			<table>
				<tr>
					<td>
						<fieldset>
						<legend>Mittaustapa</legend>
						<@selection selections "updateparameters" updateparameters.pesa.koord_mittaustapa 2 false true false errors /> 
						</fieldset>
					</td>
					<td>
						<fieldset>    
    					<legend>Tarkkuus</legend>
    					<@inputText "updateparameters" updateparameters.pesa.koord_tarkkuus true errors /> m
						</fieldset>
					</td>
					<td>
						<fieldset>    
    					<legend>Luovutus</legend>
    					<@selection selections "updateparameters" updateparameters.pesa.suojelu_kaytto 2 false true false errors />
						</fieldset>
					</td>
				</tr>
			</table>
  		</td>
  	</tr>
</table>


<table id="loytopontto" class="paaosio">
	<tr>
		<td>
			<table class="osio">
				<caption>Uuden pesän löytäminen</caption>
				<tr>
					<td>
						<fieldset>
						<legend>Pesä löytyi</legend> 
						<@selection selections "updateparameters" updateparameters.pesa.loytotapa 2 false false false errors /> 
						</fieldset>
						<fieldset>
						<legend>Rakentanut laji</legend>
						<@inputSelectCombo "updateparameters" updateparameters.pesa.rak_laji true false errors />
						</fieldset>
					</td>
				</tr>
			</table>
		</td>
		<td>
			<table class="osio">
				<caption>Pöntön tiedot</caption>
				<tr>
					<td>
						<fieldset id="pontto">
						<label>Pönttöalueen numero:</label>
						<@inputText "updateparameters" updateparameters.pesa.ponttoalue_nro true errors />  <br />
						<label>Pöntön numero:</label>
						<@inputText "updateparameters" updateparameters.pesa.pontto_nro true errors /> <br />
						<label>Ripustusvuosi:</label>
						<@inputText "updateparameters" updateparameters.tarkastus.pontto_ripustus true errors />
						</fieldset>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<h3> Pesäpaikan ja pesän kuvaus </h3>

<table class="osio paaosio">
	<tr>
		<td>
			<fieldset id="pesa_sijainti_muuta_fieldset" <@checkwarning "pesa.pesa_sijainti" /> >
			<legend <@checkwarning "pesa.pesa_sijainti" /> >Pesäpaikka</legend>
			<a href="#" class="muuta" id="pesan_sijainti_content">
				<#if updateparameters.pesa.pesa_sijainti == ""> 
					(tyhjä) 
				<#else> 
					<#assign column = updateparameters.pesa.pesa_sijainti> 
      	        	${column.value} - ${selections[column.assosiatedSelection][column.value]}    
				</#if>
			 </a> 
			</fieldset>
			
			<fieldset>
			<legend>Ympäröivä maasto</legend>
			<@selection selections "updateparameters" updateparameters.tarkastus.pesa_ymp_biotooppi 1 false true false errors />
			</fieldset>
		</td>
		
		<td>
			<fieldset id="reunavaikutus">
			<legend>Pesän sijainti suhteessa reunaan</legend>
			<label>Pesän ympäristö</label><br />
			<@selection selections "updateparameters" updateparameters.tarkastus.pesa_suhde_reuna 1 false true false errors /><br />
			<label>Etäisyys reunasta</label><br />
			<@selection selections "updateparameters" updateparameters.tarkastus.pesa_etaisyys_reuna 1 false true false errors />
			</fieldset>
		</td>
		<td >
			<fieldset id="biotooppi_muuta_fieldset" <@checkwarning "tarkastus.pesa_biotooppi" /> >
			<legend <@checkwarning "tarkastus.pesa_biotooppi" /> >Varsinainen pesimäbiotooppi</legend>
			<a href="#" class="muuta" id="biotooppi_content">
				<#if updateparameters.tarkastus.pesa_biotooppi == ""> 
					(tyhjä) 
				<#else> 
					<#assign column = updateparameters.tarkastus.pesa_biotooppi> 
      	        	${column.value} - ${selections[column.assosiatedSelection][column.value]}    
				</#if>
			</a> 
			</fieldset>
			<fieldset>
			<legend>Varsinaisen pesimäbiotoopin ala</legend>
			<@selection selections "updateparameters" updateparameters.tarkastus.pesa_biotooppi_ala 1 false true false errors />
			</fieldset>
		</td>
	</tr>
	<tr>
		<td>
			<fieldset>
			<legend>Pesän korkeus maasta/vedestä</legend>
			<@inputText "updateparameters" updateparameters.tarkastus.pesa_korkeus true errors /> m
			</fieldset>
		</td>
		<td>
			<fieldset>
			<legend>Pesäpuun korkeus</legend>
			<@inputText "updateparameters" updateparameters.tarkastus.puu_korkeus true errors /> m
			</fieldset>
		</td>
		<td>
			<fieldset>
			<legend>Pesän etäisyys rungosta</legend>
			<@inputText "updateparameters" updateparameters.tarkastus.pesa_etaisyys_runko true errors /> m
			</fieldset>
		</td>
	</tr>
</table>



<table class="osio paaosio">
	<tr>
		<td>
			<fieldset>
			<legend>Kommentteja pesästä</legend>
			<@textarea "updateparameters" updateparameters.pesa.kommentti 3 80 errors /> 
			</fieldset>
		</td>
	</tr>
</table>


</div>

<div style="white-space: nowrap;">
	<#if results?has_content>
    	<table class="infoContainer">
      	  <caption> Pesän historia </caption>
          <tr>
            <th>Vuosi</th>
            <th>Tarkastaja</th> 
            <th>Pesivä laji</th> 
            <th>Pesimistulos</th>
          </tr>
          <#list results as i>
            <tr>
              <td><a href="${system.baseURL}?page=tarkastus&amp;action=fill_for_update&amp;updateparameters.tarkastus.id=${i.tarkastus.id}"> ${i.tarkastus.vuosi} </a></td>
              <td>${selections[i.tarkastus.tarkastaja.assosiatedSelection][i.tarkastus.tarkastaja.value]}</td>  
              <td>${selections[i.tarkastus.pesiva_laji.assosiatedSelection][i.tarkastus.pesiva_laji.value]}</td> 
              <td>${i.tarkastus.pesimistulos}</td>
            </tr> 
          </#list>
    	</table>
	</#if>
	
	<br />
	
	<div style="display: inline-block;">
		<div id="lahiymparistonPesat"> <table class="infoContainer" style="width: 300px;"><caption>Lähiympäristön&nbsp;pesät</caption><tr class="hidden"><td></td></tr></table> </div>
		
		<table class="infoContainer">
			<tr>
	    		<td colspan="4">
            		Säde:
            		<input type="text" name="CoordinateSearchRadius" value="${data.CoordinateSearchRadius!"500" }" size="7" tabindex="6666" /> m 
            		<button onclick="getLahiymparistonPesat();" tabindex="6667"> Hae </button> 
	         	</td>
	    	</tr>
		</table>
	</div>
	
</div>

<div  style="clear: both;"> </div>



<h3> Pesinnän tiedot vuonna ${updateparameters.tarkastus.vuosi} </h3>

<table class="osio paaosio">
	<tr>
		<td>
			<fieldset>
			<legend>Pesinnän vuosi</legend>
			<@inputText "updateparameters" updateparameters.tarkastus.vuosi true errors /> 
			</fieldset>
		</td>
		<td>
			<fieldset>
			<legend>Havainoijanumero</legend>
			<@inputSelectCombo "updateparameters" updateparameters.tarkastus.tarkastaja true true errors />
			</fieldset>
		</td>
	</tr>
	<tr>
		<td>
			<fieldset>    
    		<legend>Pesivä laji</legend>
			<@inputSelectCombo "updateparameters" updateparameters.tarkastus.pesiva_laji true false errors />			</fieldset>
		</td>
		<td>
			<fieldset>    
    		<legend>Suojaus (tietojen salaus 5v. eteenpäin)</legend>
    		<select name="updateparameters.tarkastus.suojaus" size="1" >
    			<option value="K" <#if updateparameters.tarkastus.suojaus == "K"> selected="selected" </#if> >Kyllä</option>
    			<option value="E" <#if updateparameters.tarkastus.suojaus == "" || updateparameters.tarkastus.suojaus == "E"> selected="selected" </#if> >Ei</option>
    		</select>
			</fieldset>
		</td>
	</tr>
</table>  	

<table class="osio paaosio">
	<caption>Uusintapesintä</caption>
	<tr>
		<td>
			<fieldset>
			<legend>Monesko saman parin pesye</legend>
			<@inputText "updateparameters" updateparameters.tarkastus.uusintapesinta_jarjestysnro true errors /> 
			</fieldset>
		</td>
		<td>
			<fieldset>
			<legend>Yhteinen tunniste</legend>
			<@inputText "updateparameters" updateparameters.tarkastus.uusintapesinta_tunniste true errors /> 
			</fieldset>
		</td>
		<td>
			<fieldset>
			<legend>Uusintapesinnän perustelut</legend>
			<@textarea "updateparameters" updateparameters.tarkastus.uusintapesinta_perustelut 1 45 errors /> 
			</fieldset>
		</td>
	</tr>
</table>

<script type="text/javascript">
      var e = $('#updateparameters\\.tarkastus\\.uusintapesinta_jarjestysnro');
      if (e.val() == '') e.val('0'); 
</script>

<table class="osio paaosio">
	<caption>Yhdyskunta</caption>
	<tr>
		<td>
			<fieldset id="yhdyskunnassa">
			<legend>Onko yhdyskunnassa</legend>
			<@selection selections "updateparameters" updateparameters.tarkastus.yhdyskunnassa 1 false true true errors />
			</fieldset>
		</td>
		<td>
			<fieldset>
			<legend>Yhdyskunnan laji</legend>
			<@inputSelectCombo "updateparameters" updateparameters.tarkastus.yhdyskunta_laji true false errors />
			</fieldset>
		</td>
		<td>
			<fieldset>
			<legend>Yhdyskunnan koko</legend>
			<@inputText "updateparameters" updateparameters.tarkastus.yhdyskunta_koko true errors /> 
			</fieldset>
		</td>
	</tr>
</table>

<table class="osio" id="kayntitaulu">
	<caption>Pesäkäynnit</caption>
	<tr>
		<th rowspan="2"> # </th>
		<th rowspan="2"> Päiväys </th>
		<th rowspan="2"> Kellonaika </th>
		<th colspan="2"> Munien lkm </th>
		<th colspan="2"> Poikasten lkm </th>
		<th rowspan="2"> Pesinnän vaihe </th>
		<th rowspan="2"> Lisätiedot </th>
	</tr>
	<tr>
		<th> Elävät </th>
		<th> Kuolleet </th>
		<th> Elävät </th>
		<th> Kuolleet </th>
	</tr>
	
	<#assign maxKayntiVisible = 0>
	<#list 1..updateparameters.getKaynnit().getStaticCount() as i>
	 	<#assign kaynti = updateparameters.getTableByName("kaynti."+i)>
		<#if kaynti.hasValues()>
			<#assign maxKayntiVisible = i>
		</#if>
	</#list>
	<#if (maxKayntiVisible < 5)> <#assign maxKayntiVisible = 5> </#if>
	
	<#list 1..updateparameters.getKaynnit().getStaticCount() as i>
		<#if (i < maxKayntiVisible+1)>
			<#if i % 2 == 0> <tr class="odd" id="kaynti_${i}"> <#else> <tr id="kaynti_${i}"> </#if>
		<#else>
			<#if i % 2 == 0> <tr class="hidden odd" id="kaynti_${i}"> <#else> <tr class="hidden" id="kaynti_${i}"> </#if>
		</#if>
		
    	<td> ${i} </td>
    	<td> 
                 <@date "updateparameters" updateparameters.getColumn("kaynti."+i+".pvm") errors /> <br />
                 <div style="margin-top: 2px;"> <@selection selections "updateparameters" updateparameters.getColumn("kaynti."+i+".pvm_tarkkuus") 1 false true false errors /> </div>  
        </td>
        <td> 
                 <@inputText "updateparameters" updateparameters.getColumn("kaynti."+i+".klo_h") true errors />
                 . 
                 <@inputText "updateparameters" updateparameters.getColumn("kaynti."+i+".klo_min") true errors />
        </td>
        <td> <@inputText "updateparameters" updateparameters.getColumn("kaynti."+i+".lkm_munat") true errors /> </td>
        <td> <@inputText "updateparameters" updateparameters.getColumn("kaynti."+i+".lkm_munat_kuolleet") true errors /> </td>
        <td> <@inputText "updateparameters" updateparameters.getColumn("kaynti."+i+".lkm_poikaset") true errors /> </td>
        <td> <@inputText "updateparameters" updateparameters.getColumn("kaynti."+i+".lkm_poikaset_kuolleet") true errors /> </td>
        <td> 
        	<input type="text" name="PesinnanVaihe.${i}" value="${data["PesinnanVaihe."+i]!""}" size="3"
        	  <@checkerror errors "kaynti."+i+".pesinnan_vaihe" /> 
        	  <@checkerror errors "kaynti."+i+".ika_munat" />
        	  <@checkerror errors "kaynti."+i+".ika_poikaset" />
        	/>
        	<#if (updateparameters.getColumn("kaynti."+i+".ika_munat").hasValue())>Munan ikä</#if>
        	<#if (updateparameters.getColumn("kaynti."+i+".ika_poikaset").hasValue())>Poikasen ikä</#if>
        </td>
        <td> <@textarea "updateparameters"  updateparameters.getColumn("kaynti."+i+".kommentti") 1 60 errors />  </td>
	    </tr>
	</#list>
		<tr>
			<td colspan="10"> <button id="lisaaKaynti" class="addNewRowButton"> + Lisää käynti </button> </td>
		</tr>
</table>

<script type="text/javascript">
    var maxKayntiVisible = ${maxKayntiVisible};
	$('#lisaaKaynti').click(function () {
     	$('#kaynti_'+ (++maxKayntiVisible) ).css('display','table-row');
     	if (maxKayntiVisible >= ${updateparameters.getKaynnit().getStaticCount()}) {
     		$(this).hide(50);
     	}
	});
</script>


<table class="osio  paaosio">
	<caption>Yhteenveto</caption>
	<tr>
		<td>
			<fieldset>
			<legend>Pesintätulos</legend>
			<@selection selections "updateparameters" updateparameters.tarkastus.pesimistulos 1 false true true errors />
			</fieldset>
		</td>
		<td>
			<fieldset>
			<legend>Pesän kunto</legend>
			<@selection selections "updateparameters" updateparameters.tarkastus.pesa_kunto 1 false true true errors />
			</fieldset>
		</td>
  	</tr>
</table>

<table id="poikasetAikuisetMunat" class="paaosio">
	<tr>
		<td>
			<table class="osio poikAik">
				<caption>Poikaset</caption>
				<tr>
					<th> Rengas </th>
					<th> Sukupuoli </th>
					<th> Ikä </th>
					<th> Kommentti </th>
					<th> Laji	</th>
				</tr>
				
				<#assign maxPoikanenVisible = 0>
				<#list 1..updateparameters.getPoikaset().getStaticCount() as i>
	 				<#assign poikanen = updateparameters.getTableByName("poikanen."+i)>
					<#if poikanen.hasValues()>
						<#assign maxPoikanenVisible = i>
					</#if>
				</#list>
				<#if (maxPoikanenVisible < 3)> <#assign maxPoikanenVisible = 3> </#if>
	
				<#list 1..updateparameters.getPoikaset().getStaticCount() as i>
					<#if (i < maxPoikanenVisible+1)>
						<tr id="poikanen_${i}">
					<#else>
						<tr class="hidden" id="poikanen_${i}">
					</#if>
						  <td> <@inputText "updateparameters" updateparameters.getColumn("poikanen."+i+".rengas") true errors /> </td>
						  <td> <@selection selections "updateparameters" updateparameters.getColumn("poikanen."+i+".sukupuoli") 1 false true true errors /> </td>
						  <td> <@selection selections "updateparameters" updateparameters.getColumn("poikanen."+i+".ika") 1 false true true errors /> </td>
						  <td> <@inputText "updateparameters" updateparameters.getColumn("poikanen."+i+".kommentti") false errors /> </td>
						  <td> <@inputSelectCombo "updateparameters" updateparameters.getColumn("poikanen."+i+".laji") true false errors /> </td>
					  </tr>
				</#list>
				<tr>
					<td colspan="5"> <button id="lisaaPoikanen" class="addNewRowButton"> + Lisää poikanen </button> </td>
				</tr>
			</table>
			
			<script type="text/javascript">
    			var maxPoikanenVisible = ${maxPoikanenVisible};
				$('#lisaaPoikanen').click(function () {
     				$('#poikanen_'+ (++maxPoikanenVisible) ).css('display','table-row');
     				if (maxPoikanenVisible >= ${updateparameters.getPoikaset().getStaticCount()}) {
     					$(this).hide(50);
     				}
				});
			</script>

		</td>
		<td rowspan="2">
			<table class="osio">
				<caption>Munat</caption>
				<tr>
					<th> Pituus (mm)</th>
					<th> Leveys (mm)</th>
				</tr>
				<#list 1..updateparameters.getMunat().getStaticCount() as i>
					<tr>
						<td> <@inputText "updateparameters" updateparameters.getColumn("muna."+i+".pituus") true errors /> </td>
						<td> <@inputText "updateparameters" updateparameters.getColumn("muna."+i+".leveys") true errors /> </td>
					</tr>
				</#list>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table class="osio poikAik">
				<caption>Aikuiset</caption>
				<tr>
					<th> Rengas </th>
					<th> Sukupuoli </th>
					<th> Ikä </th>
					<th> Kommentti </th>
					<th> Laji </th>
				</tr>
				<#list 1..updateparameters.getAikuiset().getStaticCount() as i>
					<tr>
						<td> <@inputText "updateparameters" updateparameters.getColumn("aikuinen."+i+".rengas") true errors /> </td>
						<td> <@selection selections "updateparameters" updateparameters.getColumn("aikuinen."+i+".sukupuoli") 1 false true true errors /> </td>
						<td> <@selection selections "updateparameters" updateparameters.getColumn("aikuinen."+i+".ika") 1 false true true errors /> </td>
						<td> <@inputText "updateparameters" updateparameters.getColumn("aikuinen."+i+".kommentti") false errors /></td>
						<td> <@inputSelectCombo "updateparameters" updateparameters.getColumn("aikuinen."+i+".laji") true false errors /> </td>
					</tr>
				</#list>
			</table>
		</td>
	</tr>
</table>

<table class="osio paaosio">
	<tr>
		<td colspan="2">
			<fieldset>
			<legend>Kommentteja pesintää koskien</legend>
			<@textarea "updateparameters" updateparameters.tarkastus.kommentti 3 80 errors /> 
			</fieldset>
		</td>
	</tr>
	<tr>
		<td>
			<fieldset>
			<legend>Epäilyttävä?</legend>
			<@selection selections "updateparameters" updateparameters.tarkastus.epailyttava 1 false true false errors /> 
			</fieldset>
		</td>
		<td>
			<fieldset>
			<legend>Yhteispesinän ID</legend>
			<@inputText "updateparameters" updateparameters.tarkastus.yhteispesinta false errors />
			</fieldset>
		</td>
	</tr>
</table>

<br />

	<script type="text/javascript">
	  //var e = $('#updateparameters\\.pesa\\.suojelu_kaytto'); 
	  //if (e.val() == undefined) e.val('K');
	  
      var e = $('#updateparameters\\.pesa\\.seur_tarkastaja\\.selector'); 
	  e.change(function() {
	    var e = $('#updateparameters\\.pesa\\.seur_tarkastaja\\.selector');
	    var value = e.val().trim(); 
	    if (value == '') return;
	    var e = $('#updateparameters\\.tarkastus\\.tarkastaja\\.input');
	    if (e.val().trim() != '') return; 
        $('#updateparameters\\.tarkastus\\.tarkastaja\\.selector').val( value );
        $('#updateparameters\\.tarkastus\\.tarkastaja\\.input').val( value );
      });
	  var e = $('#updateparameters\\.pesa\\.seur_tarkastaja\\.input'); 
	  e.blur(function() {
	    var e = $('#updateparameters\\.pesa\\.seur_tarkastaja\\.input');
	    var value = e.val().trim(); 
	    if (value == '') return;
	    var e = $('#updateparameters\\.tarkastus\\.tarkastaja\\.input');
	    if (e.val().trim() != '') return; 
        $('#updateparameters\\.tarkastus\\.tarkastaja\\.selector').val( value );
        $('#updateparameters\\.tarkastus\\.tarkastaja\\.input').val( value );
      });
            
      var e = $('#updateparameters\\.tarkastus\\.yhdyskunnassa');
      e.blur(function() {
        if ($('#updateparameters\\.tarkastus\\.yhdyskunnassa').val() != 'K') return;
        var e = $('#updateparameters\\.tarkastus\\.pesiva_laji\\.selector'); 
	    var target = $('#updateparameters\\.tarkastus\\.yhdyskunta_laji\\.selector');
	    if (target.val().trim() != '') return; 
        target.val( e.val() ).change();
      });
      
      <#list 1..updateparameters.getKaynnit().getStaticCount() as i>
        var e = $('#updateparameters\\.kaynti\\.${i}\\.pvm\\.dd');
        e.blur(function() { 
          if (   $('#updateparameters\\.kaynti\\.${i}\\.pvm\\.dd').val().trim() == ''    ) return;
	      var source = $('#updateparameters\\.tarkastus\\.vuosi');  
	      $('#updateparameters\\.kaynti\\.${i}\\.pvm\\.yyyy').val( source.val() );
          $('#updateparameters\\.kaynti\\.${i}\\.pvm_tarkkuus').val('1');
        });
      </#list>        
      
      <#assign fields = ["kommentti", "ika", "sukupuoli", "rengas"]>
      <#list fields as field>  
      	<#list 1..updateparameters.getAikuiset().getStaticCount() as i>
      		 var e = $('#updateparameters\\.aikuinen\\.${i}\\.${field}');
        	 e.blur(function() { 
          		if ( $('#updateparameters\\.aikuinen\\.${i}\\.${field}').val().trim() == '' ) return;
	      		var source = $('#updateparameters\\.tarkastus\\.pesiva_laji\\.input');
	      		var target = $('#updateparameters\\.aikuinen\\.${i}\\.laji\\.selector');
	      		if (target.val() != '') return;
	      		target.val( source.val() ).change();
        	});
      	</#list>
      </#list>
      <#list fields as field>  
      	<#list 1..updateparameters.getPoikaset().getStaticCount() as i>
      		 var e = $('#updateparameters\\.poikanen\\.${i}\\.${field}');
        	 e.blur(function() { 
          		if ( $('#updateparameters\\.poikanen\\.${i}\\.${field}').val().trim() == '' ) return;
	      		var source = $('#updateparameters\\.tarkastus\\.pesiva_laji\\.input');
	      		var target = $('#updateparameters\\.poikanen\\.${i}\\.laji\\.selector');
	      		if (target.val() != '') return;
	      		target.val( source.val() ).change();
        	});
      	</#list>
      </#list>
            
      
	</script>
	
	 