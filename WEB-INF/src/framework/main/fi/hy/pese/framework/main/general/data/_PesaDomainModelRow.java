package fi.hy.pese.framework.main.general.data;

public interface _PesaDomainModelRow extends _Row {

	_Table getPesa();

	_Table getOlosuhde();

	_Table getTarkastus();

	_Table getKaynti();

	_IndexedTablegroup getKaynnit();

	_Table getVuosi();

	_Table getPoikanen();

	_IndexedTablegroup getPoikaset();

	_Table getAikuinen();

	_IndexedTablegroup getAikuiset();

	_Table getMuna();

	_IndexedTablegroup getMunat();

	_Table getReviiri();


}
