package fi.hy.pese.ulko.main.functionality;

import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._Table;
import fi.luomus.commons.tipuapi.TipuApiResource;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.Utils;
import fi.luomus.commons.xml.Document.Node;

public class Euring2020Util {
	
	private static final Map<String, String> AGE_MAP = initAgeMap();
	private final TipuApiResource species;
	private final TipuApiResource municipalities;

	public Euring2020Util(TipuApiResource species, TipuApiResource municipalities) {
		this.species = species;
		this.municipalities = municipalities;
	}
	
	public String convert(_Table loyto, String diario) throws Exception {
		EuringRow r = new EuringRow();
		r.append(loyto.get("scheme").getValue()); // Ringing Scheme
		r.append(identificatioMethod(loyto)); // Primary Identification Method
		r.append(formatRing(loyto)); // Identification Number (ring)
		r.append(metalRingVerification(loyto)); // Verification of the Metal Ring
		r.append(null); // Metal Ring Information
		r.append(otherMarksInformation(loyto)); // Other Marks Information
		r.append(speciesNumber(loyto.get("laji").getValue())); // Species Mentioned
		r.append(null); // Species Concluded
		r.append(manipulated(loyto)); // Manipulated
		r.append(null); // Moved Before Recovery
		r.append("--"); // Catching Method // TODO?
		r.append("U"); // Catching Lures
		r.append(sexAsReported(loyto)); // Sex Mentioned
		r.append("U"); // Sex Concluded
		r.append(ageAsReported(loyto)); // Age Mentioned
		r.append(ageAsReported(loyto)); // Age Concluded
		r.append("-"); // Status // TODO?
		r.append("--"); // Brood Size
		r.append("--"); // Pullus Age
		r.append("-"); // Accuracy of Pullus Age
		r.append(eventDate(loyto)); // Date
		r.append(eventDateAccuracy(loyto)); // Accuracy of Date
		r.append(time(loyto)); // Time
		r.append(placeCode(loyto)); // Place Code (at time of encounter)
		boolean hasCoordinates = appendCoordinatesAndAccuracy(r, loyto); // Legacy Co-ordinates, Accuracy of Co-ordinates
		if (!hasCoordinates) return null; // skip entire record
		r.append(condition(loyto)); // Condition
		r.append(circumstances(loyto)); // Circumstances
		r.append(circumstancesPresumed(loyto)); // Circumstances Presumed
		r.append("3"); // EURING Code Identifier
		r.append(null); // Derived data - Distance
		r.append(null); // Derieved data - Direction
		r.append(null); // Derieved date - Elapsed time
		r.append(loyto.get("siipi").getValue()); // Wing Length
		r.append(null); // Third Primary
		r.append(null); // State of Wing Point
		r.append(loyto.get("paino").getValue()); // Mass
		r.append(null); // Moult
		r.append("U"); // Plumage Code
		r.append(null); // Hind Claw
		r.append(null); // Bill Length
		r.append(null); // Bill  Method
		r.append(null); // Total Head Length
		r.append(null); // Tarsus
		r.append(null); // Tarsus Method
		r.append(null); // Tail Length
		r.append(null); // Tail Difference
		r.append(loyto.get("rasva").getValue()); // Fat Score
		r.append("P"); // Fat Method
		r.append(null); // Pectoral Muscle
		r.append(null); // Brood Patch
		r.append(null); // Primary Score
		r.append(null); // Primary Moult
		r.append(null); // Old Greater Coverts
		r.append(null); // Alula
		r.append(null); // Carpal Covert
		r.append(null); // Sex Determination Method
		r.append(localeString(loyto)); // Place Name
		r.append(notes(loyto)); // Remarks
		r.append("U"+diario); // Reference
		r.append(lat(loyto)); // Latitude
		r.append(lon(loyto)); // Longitude
		r.append(placeCode(loyto)); // Current Place Code
		r.append(null); // More Other Marks
		return r.toString();
	}

	private String notes(_Table loyto) {
		return loyto.get("kommentti").getValue().replace("\n", "").replace("\r", "");
	}

	private String localeString(_Table loyto) {
		String municipalityCode = loyto.get("kunta").getValue();
		String municipalityName = "";
		String locality = loyto.get("tarkempi_paikka").getValue();
		if (given(municipalityCode)) {
			municipalityName = municipalities.getById(municipalityCode).getNode("name").getContents();
		}
		if (given(municipalityName) && given(locality)) {
			return municipalityName + ", " + locality;
		}
		if (given(municipalityName)) {
			return municipalityName;
		}
		return locality;
	}

	private String circumstancesPresumed(_Table loyto) {
		String c = loyto.get("loytotapa_tarkkuus").getValue();
		if ("K".equals(c)) return "0";
		return "1";
	}

	private String circumstances(_Table loyto) {
		String c = loyto.get("loytotapa").getValue();
		if (!given(c)) return "--";
		if ("c".equals("-")) return "--";
		if (notInteger(c)) {
			return "20";
		}
		return c;
	}

	private String condition(_Table loyto) {
		String c = loyto.get("linnun_tila").getValue();
		if (!given(c)) return "0";
		return c;
	}

	private boolean notInteger(String coordinateAccuracy) {
		try {
			Integer.valueOf(coordinateAccuracy);
			return false;
		} catch (Exception e) {
			return true;
		}
	}

	private boolean appendCoordinatesAndAccuracy(EuringRow r, _Table loyto) {
		if (hasCoordinates(loyto)) {
			r.append("...............");
			r.append(coordinateAccuracy(loyto));
			return true;
		}
		return false;
	}

	private boolean hasCoordinates(_Table loyto) {
		return loyto.get("wgs84_pituus").hasValue() && loyto.get("wgs84_leveys").hasValue();
	}

	private String lat(_Table loyto) {
		return coord(loyto.get("wgs84_leveys"));
	}

	private String lon(_Table loyto) {
		return coord(loyto.get("wgs84_pituus"));
	}

	private String coord(_Column c) {
		return ""+Utils.round(c.getDoubleValue(), 4);
	}

	private String coordinateAccuracy(_Table loyto) {
		String coordinateAccuracy = loyto.get("paikka_tarkkuus").getValue();
		if (!given(coordinateAccuracy)) return "0";
		if ("0".equals(coordinateAccuracy)) return "0";
		if ("14".contains(coordinateAccuracy)) return "2";
		if ("25".contains(coordinateAccuracy)) return "5";
		if ("36".contains(coordinateAccuracy)) return "7";
		return "9";
	}

	private String placeCode(_Table loyto) {
		String code = loyto.get("euring_paikka").getValue();
		if (!given(code)) return "SF--";
		return code;
	}

	private String time(_Table loyto) {
		String hour = loyto.get("klo").getValue();
		if (!given(hour) || hour.equals("-1")) hour = "--";
		return addFrontZeros(hour, 2) + "--";
	}

	private String eventDateAccuracy(_Table loyto) {
		String eventDateAccuracy = loyto.get("aika_tarkkuus").getValue();
		if (!given(eventDateAccuracy)) return "0";
		if (eventDateAccuracy.equals("-")) return "9";
		return eventDateAccuracy;
	}

	private String eventDate(_Table loyto) throws ParseException {
		String date = loyto.get("pvm").getValue();
		if (!given(date)) return null;
		return DateUtils.format(DateUtils.convertToDate(date), "ddMMyyyy");
	}

	private static String addFrontZeros(String s, int desiredLength) {
		while (s.length() < desiredLength) {
			s = "0" + s;
		}
		return s;
	}

	private String ageAsReported(_Table loyto) {
		String age = loyto.get("ika").getValue();
		if (!given(age)) return "0";
		if (ageIsPull(loyto)) return "1";
		String v = AGE_MAP.get(age);
		if (given(v)) return v;
		return "Z";
	}

	private String sexAsReported(_Table loyto) {
		String sex = loyto.get("sukupuoli").getValue();
		if (sex.equals("K")) return "M";
		if (sex.equals("N")) return "F";
		return "U";
	}


	private boolean ageIsPull(_Table loyto) {
		return loyto.get("ika").getValue().startsWith("P");
	}

	private boolean given(String s) {
		return s != null && s.length() > 0;
	}

	private String manipulated(_Table loyto) {
		String val = loyto.get("linnun_tila").getValue();
		if ("4".equals(val)) return "P";
		if ("5".equals(val)) return "P";
		if ("6".equals(val)) return "C";
		if ("7".equals(val)) return "H";
		if ("8".equals(val)) return "H";
		return "U"; // U Uncoded or unknown if manipulated or not.
	}

	private String speciesNumber(String speciesCode) throws Exception {
		if (!given(speciesCode)) return "00000";
		Node info = species.getById(speciesCode);
		if (info == null) throw new UnsupportedOperationException("Uknown species " + speciesCode);
		int number = Integer.valueOf(info.getAttribute("euring-code"));
		return addFrontZeros(Integer.toString(number), 5);
	}

	private String otherMarksInformation(_Table loyto) {
		String val = loyto.get("loytotapa").getValue();
		if ("A".equals(val)) return "MM";
		if ("B".equals(val)) return "MM";
		if ("K".equals(val)) return "CC";
		if ("L".equals(val)) return "DC";
		if ("M".equals(val)) return "OM";
		return null;
	}

	private String metalRingVerification(_Table loyto) {
		if (!loyto.get("rengas").hasValue()) return "0";
		String val = loyto.get("rengas_varmennettu").getValue();
		if (!given(val)) return "0";
		if ("K".equals(val)) return "1";
		return "0";
	}

	private String formatRing(_Table loyto) {
		String originalRing = loyto.get("rengas").getValue();
		if (!given(originalRing)) return null;
		originalRing = Utils.removeWhitespace(originalRing);
		StringBuilder b = new StringBuilder();
		boolean dotsAdded = false;
		for (char c : originalRing.toCharArray()) {
			if ((!Character.isAlphabetic(c) && c != '+') && !dotsAdded) {
				addDots(10 - originalRing.length(), b);
				dotsAdded = true;
			}
			b.append(c);
		}
		return b.toString();
	}

	private static void addDots(int count, StringBuilder b) {
		for (int i = 0; i<count; i++) {
			b.append(".");
		}
	}

	private String identificatioMethod(_Table loyto) {
		String val = loyto.get("loytotapa").getValue();
		if (!given(val)) return null;
		if (val.equals("81")) return "B0";
		if (val.equals("82")) return "C0";
		if (val.equals("83")) return "D0";
		if (val.equals("84")) return "R0";
		if (val.equals("85")) return "E0";
		if (val.equals("86")) return "F0";
		return "A0";
	}

	private static class EuringRow {
		private static final String SEPARATOR_CHAR = "|";
		private final StringBuilder b = new StringBuilder();
		public EuringRow append(String s) {
			if (s != null && s.length() > 0) {
				b.append(s);
			}
			b.append(SEPARATOR_CHAR);
			return this;
		}
		@Override
		public String toString() {
			Utils.removeLastChar(b);
			return b.toString();
		}
	}

	private static Map<String, String> initAgeMap() {
		Map<String, String> map = new HashMap<>();
		map.put("FL", "2");
		map.put("1", "3");
		map.put("+1", "4");
		map.put("2", "5");
		map.put("+2", "6");
		map.put("3", "7");
		map.put("+3", "8");
		map.put("4", "9");
		map.put("+4", "A");
		map.put("5", "B");
		map.put("+5", "C");
		map.put("6", "D");
		map.put("+6", "E");
		map.put("7", "F");
		map.put("+7", "G");
		map.put("8", "H");
		map.put("+8", "J");
		map.put("9", "K");
		map.put("+9", "L");
		map.put("10", "M");
		map.put("+10", "N");
		map.put("11", "O");
		map.put("+11", "P");
		map.put("12", "Q");
		map.put("+12", "R");
		map.put("13", "S");
		map.put("+13", "T");
		map.put("14", "U");
		map.put("+14", "V");
		map.put("15", "W");
		map.put("+15", "X");
		map.put("16", "Y");
		return map;
	}
	
}
