package fi.hy.pese.framework.test.db;

import java.io.File;
import java.io.FileNotFoundException;
import java.sql.Date;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fi.hy.pese.framework.main.app.BaseFactoryForApplicationSpecificationFactory;
import fi.hy.pese.framework.main.app.Request;
import fi.hy.pese.framework.main.app.RequestImpleData;
import fi.hy.pese.framework.main.app._ApplicationSpecificationFactory;
import fi.hy.pese.framework.main.app._Functionality;
import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.db.ResultsToReportHandler;
import fi.hy.pese.framework.main.db._DAO;
import fi.hy.pese.framework.main.db._ResultSet;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data.Data;
import fi.hy.pese.framework.main.general.data.DatabaseColumnStructure;
import fi.hy.pese.framework.main.general.data.PesaDomainModelDatabaseStructure;
import fi.hy.pese.framework.main.general.data.PesaDomainModelRow;
import fi.hy.pese.framework.main.general.data.PesaDomainModelRowFactory;
import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.test.TestConfig;
import fi.hy.pese.merikotka.main.app.MerikotkaDatabaseStructureCreator;
import fi.luomus.commons.config.Config;
import fi.luomus.commons.containers.Selection;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.FileUtils;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class ResultsToReportTest {
	
	public static Test suite() {
		return new TestSuite(ResultsToReportTest.class.getDeclaredClasses());
	}
	
	public static class WhenWritingResultsToTextfile extends TestCase {
		
		private static final int		TARKASTUS_ROWID_INDEX	= 1;
		private ResultsToReportHandler	handler;
		private _Request<_PesaDomainModelRow>				request;
		private Data<_PesaDomainModelRow>					data;
		private static final Config	config = TestConfig.getConfig();
		private PesaDomainModelDatabaseStructure	structure;
		
		private Map<String, String> uiTexts() {
			Map<String, String> uiTexts = new HashMap<>();
			uiTexts.put("short.pesavakio.pesa_id", "pesa_id");
			uiTexts.put("short.pesavakio.pesanimi", "nimi");
			uiTexts.put("short.pesatarkastus.tark_vuosi", "tark_vuosi");
			uiTexts.put("short.poikanen.1.poikanen_id", "P1.id");
			uiTexts.put("short.poikanen.2.poikanen_id", "P2.id");
			uiTexts.put("short.poikanen.3.poikanen_id", "P3.id");
			uiTexts.put("short.poikanen.1.rengas_oikea", "P1.reng_oik");
			uiTexts.put("short.poikanen.2.rengas_oikea", "P2.reng_oik");
			uiTexts.put("short.poikanen.3.rengas_oikea", "P3.reng_oik");
			uiTexts.put("short.aikuinen.1.aikuinen_id", "A1.id");
			uiTexts.put("short.aikuinen.2.aikuinen_id", "A2.id");
			uiTexts.put("short.aikuinen.3.aikuinen_id", "A3.id");
			uiTexts.put(Const.RUN_DATE_TEXT, "AJETTU");
			uiTexts.put(Const.RUN_PARAMETERS_TEXT, "PARAMETREILLA");
			return uiTexts;
		}
		
		private class FakeDAO extends DAOStub<_PesaDomainModelRow> {
			@Override
			public _PesaDomainModelRow newRow() {
				return new PesaDomainModelRow(structure);
			}
		}
		
		private class FactoryForResultsToReporTest extends BaseFactoryForApplicationSpecificationFactory<_PesaDomainModelRow> {
			public FactoryForResultsToReporTest(String configFileName) throws FileNotFoundException {
				super(configFileName);
			}
			
			@Override
			public Config config() {
				return config;
			}
			
			@Override
			public _Functionality<_PesaDomainModelRow> functionalityFor(String page, _Request<_PesaDomainModelRow> request) throws UnsupportedOperationException {
				return null;
			}
			
			@Override
			protected Map<String, Selection> fetchSelections(_DAO<_PesaDomainModelRow> dao) throws Exception {
				return null;
			}
		}
		
		@Override
		protected void setUp() throws Exception {
			structure = MerikotkaDatabaseStructureCreator.hardCoded();
			data = new Data<>(new PesaDomainModelRowFactory(structure, "", ""));
			data.setUITexts(uiTexts());
			_ApplicationSpecificationFactory<_PesaDomainModelRow> factory = new FactoryForResultsToReporTest(null);
			request = new Request<>(new RequestImpleData<_PesaDomainModelRow>().setData(data).setDao(new FakeDAO()).setApplication(factory));
			handler = new ResultsToReportHandler(request);
			deleteAllReports();
		}
		
		@Override
		protected void tearDown() {
			deleteAllReports();
		}
		
		private void deleteAllReports() {
			File dir = new File(config.reportFolder());
			for (File f : dir.listFiles()) {
				if (f.getName().endsWith(".txt")) {
					f.delete();
				}
			}
		}
		
		public void test__if_no_results__doesnt__create_a_file() throws Exception {
			data.addToList(Const.SELECTED_FIELDS, " pesavakio.pesa_id");
			handler.process(new _ResultSet() {
				@Override
				public boolean next() throws SQLException {
					return false;
				}
				
				@Override
				public String getString(String columnLabel) throws SQLException {
					return null;
				}
				
				@Override
				public String getString(int i) throws SQLException {
					return null;
				}
				
				@Override
				public Date getDate(String columnLabel) throws SQLException {
					return null;
				}
				
				@Override
				public Date getDate(int i) throws SQLException {
					return null;
				}
			});
			assertEquals("[]", data.noticeTexts().toString());
			assertEquals(0, data.producedFiles().size());
			assertEquals(0, fileCount());
		}
		
		private int fileCount() {
			File dir = new File(config.reportFolder());
			int count = 0;
			for (String filename : dir.list()) {
				if (filename.endsWith(".txt")) count++;
			}
			return count;
		}
		
		public void test__if_no_selected_fields__does_not_create_a_file() throws Exception {
			handler.process(new _ResultSet() {
				int	row	= 1;
				
				@Override
				public boolean next() throws SQLException {
					return (row++ == 5);
				}
				
				@Override
				public String getString(String columnLabel) throws SQLException {
					return null;
				}
				
				@Override
				public String getString(int i) throws SQLException {
					return null;
				}
				
				@Override
				public Date getDate(String columnLabel) throws SQLException {
					return null;
				}
				
				@Override
				public Date getDate(int i) throws SQLException {
					return null;
				}
			});
			assertEquals(0, data.producedFiles().size());
			assertEquals(0, fileCount());
		}
		
		public void test___it_writes_the_report____incl_poikaset() throws Exception {
			data.addToList(Const.SELECTED_FIELDS, "pesavakio.pesa_id");
			data.addToList(Const.SELECTED_FIELDS, "pesatarkastus.tark_vuosi");
			data.addToList(Const.SELECTED_FIELDS, "pesatarkastus.pesimistulos");
			data.addToList(Const.SELECTED_FIELDS, "poikanen.poikanen_id");
			data.addToList(Const.SELECTED_FIELDS, "poikanen.rengas_oikea");
			data.addToList(Const.SELECTED_FIELDS, "aikuinen.aikuinen_id");
			data.searchparameters().getColumn("pesavakio.pesanimi").setValue("search parameter");
			
			handler.process(new _ResultSet() {
				int	row	= 0;
				
				@Override
				public boolean next() throws SQLException {
					return (++row <= 3);
				}
				
				@Override
				public Date getDate(int i) throws SQLException {
					return null;
				}
				
				@Override
				public Date getDate(String columnLabel) throws SQLException {
					return null;
				}
				
				@Override
				public String getString(int i) throws SQLException {
					if (i == TARKASTUS_ROWID_INDEX) {
						if (row == 1 || row == 2) {
							return "AAA";
						}
						return "AAB";
					}
					int columnIndex = 1;
					for (DatabaseColumnStructure c : structure.getTarkastus().getColumns()) {
						if (columnIndex == i) {
							if (row == 3) {
								if (c.getName().equals("pesimistulos")) return "A";
							}
							if (c.getName().equals("tark_vuosi")) return "2008";
							if (c.getName().equals("pesimistulos")) return "R";
							return c.getName();
						}
						columnIndex++;
					}
					for (DatabaseColumnStructure c : structure.getPoikanen().getColumns()) {
						if (columnIndex == i) {
							if (row == 1) {
								if (isRowid(c)) return "poik_1_rowid";
								if (c.getName().equals("poikanen_id")) return "36";
								if (c.getName().equals("rengas_oikea")) return "E3521";
							}
							if (row == 2) {
								if (isRowid(c)) return "poik_2_rowid";
								if (c.getName().equals("poikanen_id")) return "37";
								if (c.getName().equals("rengas_oikea")) return "E3462";
							}
							if (row == 3) { return ""; }
							return c.getName();
						}
						columnIndex++;
					}
					for (@SuppressWarnings("unused") DatabaseColumnStructure c : structure.getAikuinen().getColumns()) {
						if (columnIndex == i) {
							if (row == 1) return "XYZ";
							return "";
						}
						columnIndex++;
					}
					for (DatabaseColumnStructure c : structure.getPesa().getColumns()) {
						if (columnIndex == i) {
							if (c.getName().equals("pesa_id")) return "1";
							return c.getName();
						}
						columnIndex++;
					}
					for (DatabaseColumnStructure c : structure.getOlosuhde().getColumns()) {
						if (columnIndex == i) { return c.getName(); }
						columnIndex++;
					}
					for (DatabaseColumnStructure c : structure.getVuosi().getColumns()) {
						if (columnIndex == i) { return c.getName(); }
						columnIndex++;
					}
					for (DatabaseColumnStructure c : structure.getReviiri().getColumns()) {
						if (columnIndex == i) { return c.getName(); }
						columnIndex++;
					}
					throw new SQLException("end reached");
				}
				
				private boolean isRowid(DatabaseColumnStructure c) {
					return c.getFieldType() == _Column.ROWID;
				}
				
				@Override
				public String getString(String columnLabel) throws SQLException {
					return null;
				}
			});
			
			assertEquals(1, data.producedFiles().size());
			File file = new File(config.reportFolder() + File.separator + data.producedFiles().get(0));
			assertEquals(true, file.exists());
			
			List<String> lines = FileUtils.readLines(file);
			// Utils.debug(lines);
			assertEquals("AJETTU: ||||PARAMETREILLA: nimi: search parameter; ", lines.get(0));
			assertEquals(currentDate(), lines.get(1));
			assertEquals("pesa_id|tark_vuosi|pesatarkastus.pesimistulos|P1.id|P1.reng_oik|P2.id|P2.reng_oik|P3.id|P3.reng_oik|poikanen.4.poikanen_id|poikanen.4.rengas_oikea|A1.id|A2.id|A3.id|", lines.get(3));
			assertEquals("", lines.get(4));
			assertEquals("1|2008|R|36|E3521|37|E3462|||||XYZ|||", lines.get(5));
			assertEquals("1|2008|A||||||||||||", lines.get(6));
			assertEquals("", lines.get(7));
		}
		
		private String currentDate() {
			String date = DateUtils.getCurrentDateTime("dd.MM.yyyy");
			return date;
		}
		
		public void test___it_doesnt_write_dublicate_lines() throws Exception {
			data.addToList(Const.SELECTED_FIELDS, "pesavakio.pesa_id");
			data.addToList(Const.SELECTED_FIELDS, "pesavakio.pesanimi");
			
			data.searchparameters().getColumn("pesavakio.pesa_id").setValue("40|50");
			
			handler.process(new _ResultSet() {
				int	row	= 0;
				
				@Override
				public boolean next() throws SQLException {
					return (++row <= 5);
				}
				
				@Override
				public Date getDate(int i) throws SQLException {
					return null;
				}
				
				@Override
				public Date getDate(String columnLabel) throws SQLException {
					return null;
				}
				
				@Override
				public String getString(int i) throws SQLException {
					if (i == TARKASTUS_ROWID_INDEX) {
						if (row == 1 || row == 2) { return "ROWID_FOR_PESA_40_TARK_2008"; }
						if (row == 3) { return "ROWID_FOR_PESA_40_TARK_2007"; }
						if (row == 4) { return "ROWID_FOR_PESA_50_TARK_2008"; }
						if (row == 5) { return "ROWID_FOR_PESA_50_TARK_2007"; }
					}
					int columnIndex = 1;
					for (DatabaseColumnStructure c : structure.getTarkastus().getColumns()) {
						if (columnIndex == i) { return c.getName(); }
						columnIndex++;
					}
					for (DatabaseColumnStructure c : structure.getPoikanen().getColumns()) {
						if (columnIndex == i) { return c.getName(); }
						columnIndex++;
					}
					for (DatabaseColumnStructure c : structure.getAikuinen().getColumns()) {
						if (columnIndex == i) { return c.getName(); }
						columnIndex++;
					}
					for (DatabaseColumnStructure c : structure.getPesa().getColumns()) {
						if (columnIndex == i) {
							if (c.getName().equals("pesa_id")) {
								if (row == 1 || row == 2 || row == 3) { return "40"; }
								if (row == 4 || row == 5) { return "50"; }
							}
							if (c.getName().equals("pesanimi")) {
								if (row == 1 || row == 2 || row == 3) { return "Pesän 40 nimi"; }
								if (row == 4 || row == 5) { return "Pesän 50 nimi"; }
							}
							return c.getName();
						}
						columnIndex++;
					}
					for (DatabaseColumnStructure c : structure.getOlosuhde().getColumns()) {
						if (columnIndex == i) { return c.getName(); }
						columnIndex++;
					}
					for (DatabaseColumnStructure c : structure.getVuosi().getColumns()) {
						if (columnIndex == i) { return c.getName(); }
						columnIndex++;
					}
					for (DatabaseColumnStructure c : structure.getReviiri().getColumns()) {
						if (columnIndex == i) { return c.getName(); }
						columnIndex++;
					}
					throw new SQLException("end reached");
				}
				
				@Override
				public String getString(String columnLabel) throws SQLException {
					return null;
				}
			});
			
			assertEquals(1, data.producedFiles().size());
			File file = new File(config.reportFolder() + File.separator + data.producedFiles().get(0));
			assertEquals(true, file.exists());
			
			List<String> lines = FileUtils.readLines(file);
			// Utils.debug(lines);
			assertEquals("AJETTU: ||||PARAMETREILLA: pesa_id: 40,50; ", lines.get(0));
			assertEquals(currentDate(), lines.get(1));
			assertEquals("", lines.get(2));
			assertEquals("pesa_id|nimi|", lines.get(3));
			assertEquals("", lines.get(4));
			assertEquals("40|Pesän 40 nimi|", lines.get(5));
			assertEquals("50|Pesän 50 nimi|", lines.get(6));
			assertEquals("", lines.get(7));
		}
		
	}
}
