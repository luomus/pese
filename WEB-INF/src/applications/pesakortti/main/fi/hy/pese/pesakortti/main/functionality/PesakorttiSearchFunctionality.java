package fi.hy.pese.pesakortti.main.functionality;


import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.app.functionality.SearchFunctionality;
import fi.hy.pese.framework.main.general._PDFDataMapGenerator;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.main.general.data._ResultSorter;
import fi.hy.pese.framework.main.general.data._Table;

public class PesakorttiSearchFunctionality extends SearchFunctionality {

	public PesakorttiSearchFunctionality(_Request<_PesaDomainModelRow> request) {
		super(request);
	}

	private PesakorttiDataToMapForPDFPrintingGenerator dataToMapForPDFPrintingGenerator = null;
	//private final Collection<String>	deadNests	= null;

	@Override
	public String nestingOutput(_Table tarkastus) {
		return ""; // TODO  kokonaistuotto
	}

	@Override
	public _ResultSorter<_PesaDomainModelRow> sorter() {
		return null; // No need to sort afterwards; can be sorted using sql
	}

	@Override
	public _PDFDataMapGenerator pdfDataMapGenerator() {
		if (dataToMapForPDFPrintingGenerator == null) {
			dataToMapForPDFPrintingGenerator = new PesakorttiDataToMapForPDFPrintingGenerator(dao, request);
		}
		return dataToMapForPDFPrintingGenerator;
	}

	@Override
	protected void applicationPrintReport(String reportName) {
		// ReportWriter writer = null;
		// if (reportName.equals(MerikotkaConst.REPORT_PESIMISTULOKSET)) {
		// writer = new MerikotkaReport_pesimistulostenYhteenvetolista(request, reportName);
		// }
		// if (reportName.equals(MerikotkaConst.REPORT_REVIIRIT)) {
		// writer = new MerikotkaReport_reviirilista(request, reportName);
		// }
		// if (reportName.equals(MerikotkaConst.REPORT_TARKASTAMATTOMAT)) {
		// writer = new MerikotkaReport_tarkastamattomat(request, reportName, this);
		// }
		// if (writer != null) {
		// writer.produce();
		// } else {
		// data.addToNoticeTexts(reportName + " not implemented");
		// }
		data.addToNoticeTexts(reportName + " not implemented");
	}

	@Override
	public String deadNestsQuery() {
		return " 1 = 1 "; 		// TODO
	}

	@Override
	public String liveNestsQuery() {
		return " 1 = 1 "; 		// TODO
	}

}
