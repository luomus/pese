package fi.hy.pese.peto.main.functionality;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.app.functionality.NestSorter;
import fi.hy.pese.framework.main.app.functionality.SearchFunctionality;
import fi.hy.pese.framework.main.general.ReportWriter;
import fi.hy.pese.framework.main.general._PDFDataMapGenerator;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.main.general.data._ResultSorter;
import fi.hy.pese.framework.main.general.data._Table;
import fi.hy.pese.peto.main.app.PetoConst;
import fi.hy.pese.peto.main.functionality.reports.PetoReport_havainnoijalistaus;
import fi.hy.pese.peto.main.functionality.reports.PetoReport_kansilehdet;
import fi.luomus.commons.tipuapi.TipuAPIClient;
import fi.luomus.commons.utils.Utils;
import fi.luomus.commons.xml.Document.Node;

public class PetoSearchFunctionality extends SearchFunctionality {

	private static final String	SEURANTARUUDULLA_WHERE_CLAUSE	= "" +
			" ( SELECT  count(1)                                                                    " +
			"   FROM    seurantaruutu s                                                             " +
			"   WHERE   s.vuosi = tarkastus.vuosi                                                   " +
			"   AND     pesa.yht_leveys between s.kulma_yht_leveys and s.kulma_yht_leveys + s.koko  " +
			"   AND     pesa.yht_pituus between s.kulma_yht_pituus and s.kulma_yht_pituus + s.koko  " +
			" ) > 0                                                                                 ";

	public PetoSearchFunctionality(_Request<_PesaDomainModelRow> request) {
		super(request);
	}

	private PetoDataToMapForPDFPrintingGenerator dataToMapForPDFPrintingGenerator = null;

	@Override
	public String nestingOutput(_Table tarkastus) {
		return ""; // TODO  kokonaistuotto
	}

	@Override
	public _ResultSorter<_PesaDomainModelRow> sorter() {
		return null; // No need to sort afterwards; can be sorted using sql
	}

	@Override
	protected _ResultSorter<_PesaDomainModelRow> pdfPrintingSorter() {
		return new NestSorter(comparator());
	}

	public static Comparator<_PesaDomainModelRow> comparator() {
		return new Comparator<_PesaDomainModelRow>() {
			@Override
			public int compare(_PesaDomainModelRow first, _PesaDomainModelRow second) {
				int result = first.getColumn("pesa.seur_tarkastaja").getValue().compareTo(second.getColumn("pesa.seur_tarkastaja").getValue());
				if (result != 0) return result;
				result = first.getColumn("tarkastus.pesiva_laji").getValue().compareTo(second.getColumn("tarkastus.pesiva_laji").getValue());
				if (result != 0) return result;
				result = first.getColumn("pesa.kunta").getValue().compareTo(second.getColumn("pesa.kunta").getValue());
				if (result != 0) return result;
				result = first.getColumn("pesa.kyla").getValue().compareTo(second.getColumn("pesa.kyla").getValue());
				if (result != 0) return result;
				result = first.getColumn("pesa.id").getValue().compareTo(second.getColumn("pesa.id").getValue());
				return result;
			}
		};
	}

	@Override
	public _PDFDataMapGenerator pdfDataMapGenerator() {
		if (dataToMapForPDFPrintingGenerator == null) {
			dataToMapForPDFPrintingGenerator = new PetoDataToMapForPDFPrintingGenerator(dao, request);
		}
		return dataToMapForPDFPrintingGenerator;
	}

	private static final String	DEAD_NESTS_SQL	= deadNestSql();
	private static final String	LIVE_NESTS_SQL	= liveNestSql();

	private static String deadNestSql() {
		StringBuilder sql = new StringBuilder();
		sql.append(" pesa.id IN (SELECT pesa FROM tarkastus, kaynti WHERE kaynti.tarkastus = tarkastus.id AND kaynti.pesa_kunto IN ( ");
		Utils.toCommaSeperatedStatement(sql, PetoConst.DEAD_NEST_CODES , true);
		sql.append(" ) ) " );
		return sql.toString();
	}
	private static String liveNestSql() {
		StringBuilder sql = new StringBuilder();
		sql.append(" pesa.id NOT IN (SELECT pesa FROM tarkastus, kaynti WHERE kaynti.tarkastus = tarkastus.id AND kaynti.pesa_kunto IN ( ");
		Utils.toCommaSeperatedStatement(sql, PetoConst.DEAD_NEST_CODES , true);
		sql.append(" ) ) " );
		return sql.toString();
	}

	@Override
	public String deadNestsQuery()  {
		return DEAD_NESTS_SQL;
	}

	@Override
	public String liveNestsQuery() {
		return LIVE_NESTS_SQL;
	}

	@Override
	protected void applicationPrintReport(String reportName) {
		ReportWriter<_PesaDomainModelRow> writer = null;
		if (reportName.equals(PetoConst.REPORT_HAVAINNOIJALISTAUS)) {
			writer = new PetoReport_havainnoijalistaus(request, reportName);
		} else if (reportName.equals(PetoConst.REPORT_KANSILEHDET)) {
			writer = new PetoReport_kansilehdet(request, reportName);
		}
		if (writer != null) {
			writer.produce();
		} else {
			data.addToNoticeTexts(reportName + " not implemented");
		}
	}

	@Override
	public void preProsessingHook() throws Exception {
		super.preProsessingHook();
		if (!data.contains(PetoConst.SEURANTARUUDULLA)) {
			data.set(PetoConst.SEURANTARUUDULLA, "0");
		}
	}

	@Override
	protected void additionalSearchConditionsHook(List<String> additionalQueries) {
		if ("1".equals(data.get(PetoConst.SEURANTARUUDULLA))) {
			additionalQueries.add(SEURANTARUUDULLA_WHERE_CLAUSE);
		}
		if (data.contains(Const.YMPARISTOKESKUS)) {
			addYmparistokeskusSearchQuery(additionalQueries);
		}
	}

	private void addYmparistokeskusSearchQuery(List<String> additionalQueries) {
		String ympkesk = data.get(Const.YMPARISTOKESKUS);
		List<String> ympkeskKunnat = new LinkedList<>();
		for (Node n : request.getTipuApiResource(TipuAPIClient.MUNICIPALITIES).getAll()) {
			if (n.getNode("ely-centre").getContents().equals(ympkesk)) {
				ympkeskKunnat.add(n.getNode("id").getContents());
			}
		}
		StringBuilder statement = new StringBuilder();
		statement.append(data.searchparameters().getPesa().get("kunta").getFullname());
		statement.append(" IN ( ");
		Utils.toCommaSeperatedStatement(statement, ympkeskKunnat, true);
		statement.append(" ) ");
		additionalQueries.add(statement.toString());
	}
}
