package fi.hy.pese.framework.main.general.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import fi.hy.pese.framework.main.general.Utils;

public class Row implements _Row, Serializable {
	
	private static final long	serialVersionUID	= -1543697651485577731L;
	
	private final Table					aputaulu;
	private final Table					logTable;
	private final Table					savedSearches;
	private final Table					lomakeVastaanottajat;
	private final Table					transactionEntry;
	private final Map<String, _Table>	basetables;
	private final Map<String, _IndexedTablegroup> basetableToIndexedTablegroupLink;
	private final String				prefix;
	private final Map<String, String>	customValues = new HashMap<>();
	
	public Row(DatabaseStructure rowStructure) {
		this(rowStructure, null);
	}
	
	public Row(DatabaseStructure rowStructure, String prefix) {
		this.prefix = prefix;
		this.basetables = new LinkedHashMap<>(25);
		this.basetableToIndexedTablegroupLink = new LinkedHashMap<>();
		
		constructBasetables(rowStructure);
		
		aputaulu = new Table(rowStructure.getAputaulu());
		logTable = new Table(rowStructure.getLogTable());
		transactionEntry = new Table(rowStructure.getTransactionEntry());
		savedSearches = new Table(rowStructure.getSavedSearches());
		lomakeVastaanottajat = new Table(rowStructure.getLomakeVastaanottajat());
		
		defineBasetable(aputaulu);
		defineBasetable(logTable);
		defineBasetable(transactionEntry);
		defineBasetable(savedSearches);
		defineBasetable(lomakeVastaanottajat);
	}
	
	protected void constructBasetables(DatabaseStructure rowStructure) {
		for (DatabaseTableStructure tableStructure : rowStructure.getAllTables()) {
			if (basetables.containsKey(tableStructure.getName())) continue;
			defineBasetable(new Table(tableStructure));
		}
	}
	
	protected void addGroupLink(_Table t, IndexedTablegroup group) {
		basetableToIndexedTablegroupLink.put(t.getName().toLowerCase(), group);
	}
	
	@Override
	public String getPrefix() {
		return this.prefix;
	}
	
	protected void defineBasetable(Table t) {
		basetables.put(t.getFullname().toLowerCase(), t);
	}
	
	@Override
	public Table getAputaulu() {
		return aputaulu;
	}
	
	@Override
	public _Table getLogTable() {
		return logTable;
	}
	
	@Override
	public _Table getTransactionEntry() {
		return transactionEntry;
	}
	
	@Override
	public _Table getSavedSearches() {
		return savedSearches;
	}
	
	@Override
	public _Table getTableByName(String name) throws IllegalArgumentException {
		if (name == null) throw new IllegalArgumentException("Given name was null");
		_Table t = basetables.get(name.toLowerCase());
		if (t != null) return t;
		if (name.contains(".")) {
			String[] arr = Utils.splitColumnFullname(name);
			String tableName = arr[0];
			String index = arr[1];
			_IndexedTablegroup group = basetableToIndexedTablegroupLink.get(tableName.toLowerCase());
			if (group != null) {
				t = group.get(Integer.valueOf(index));
				if (t != null) return t;
			}
		}
		throw new IllegalArgumentException("Table " + name + " not found");
	}
	
	@Override
	public void clearAllValues() {
		for (_Table table : this) {
			if (table == null) continue;
			table.clearAllValues();
		}
	}
	
	@Override
	public _Column getColumn(String columnFullname) {
		String[] arr = Utils.splitColumnFullname(columnFullname);
		String tableName = arr[0];
		String columnName = arr[1];
		return getTableByName(tableName).get(columnName);
	}
	
	@Override
	public boolean containsColumn(String columnFullname) {
		try {
			getColumn(columnFullname);
		} catch (Exception e) {
			return false;
		}
		return true;
	}
	
	@Override
	public boolean containsTable(String tableFullname) {
		try {
			getTableByName(tableFullname);
		} catch (Exception e) {
			return false;
		}
		return true;
	}
	
	@Override
	public Iterator<_Table> iterator() {
		return getAllTables().iterator();
	}
	
	private Collection<_Table> getAllTables() {
		Collection<_Table> tables = new ArrayList<>();
		tables.addAll(basetables.values());
		for (_IndexedTablegroup group : basetableToIndexedTablegroupLink.values()) {
			tables.addAll(group.values());
		}
		return tables;
	}
	
	@Override
	public Map<String, _IndexedTablegroup> getGroups() {
		return basetableToIndexedTablegroupLink;
	}
	
	@Override
	public boolean hasValues() {
		for (_Table t : this) {
			if (t.hasValues()) return true;
		}
		return false;
	}
	
	@Override
	public void setCustomValue(String key, String value) {
		customValues.put(key, value);
	}
	
	@Override
	public String getCustomValue(String key) {
		if (!customValues.containsKey(key)) return "";
		return customValues.get(key);
	}
	
	@Override
	public _Table getLomakeVastaanottajat() {
		return lomakeVastaanottajat;
	}
	
}
