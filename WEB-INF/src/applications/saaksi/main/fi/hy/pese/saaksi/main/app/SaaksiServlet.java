package fi.hy.pese.saaksi.main.app;

import fi.hy.pese.framework.main.app.PeSeServlet;
import fi.hy.pese.framework.main.app._ApplicationSpecificationFactory;
import fi.hy.pese.framework.main.general.data.PesaDomainModelDatabaseStructure;
import fi.hy.pese.framework.main.general.data.PesaDomainModelRowFactory;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.luomus.commons.db.connectivity.PreparedStatementStoringAndClosingTransactionConnection;
import fi.luomus.commons.db.connectivity.TransactionConnection;

public class SaaksiServlet extends PeSeServlet<_PesaDomainModelRow> {

	private static final long	serialVersionUID	= -7185896305661963676L;

	@Override
	protected _ApplicationSpecificationFactory<_PesaDomainModelRow> createFactory() throws Exception {
		SaaksiFactory factory = new SaaksiFactory("pese_saaksi.config");
		factory.loadUITexts();

		TransactionConnection con = new PreparedStatementStoringAndClosingTransactionConnection(factory.config().connectionDescription());
		try {
			PesaDomainModelDatabaseStructure structure = SaaksiDatabaseStructureCreator.loadFromDatabase(con);
			factory.setRowFactory(new PesaDomainModelRowFactory(structure, SaaksiConst.JOINS, SaaksiConst.ORDER_BY));
		} catch (Exception e) {
			con.release();
			throw new Exception(e);
		}
		con.release();
		return factory;
	}

}
