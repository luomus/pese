package fi.hy.pese.framework.main.app.functionality;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.main.general.data._ResultSorter;

public class NestSorter implements _ResultSorter<_PesaDomainModelRow> {

	private final Map<String, List<_PesaDomainModelRow>>	inspectionsByNest;
	private final Comparator<_PesaDomainModelRow> comparator;

	public NestSorter(Comparator<_PesaDomainModelRow> comparator) {
		this.inspectionsByNest = new LinkedHashMap<>();
		this.comparator = comparator;
	}

	/**
	 * Sorts results using the given comparator.
	 * The results are initially ordered by pesa_id, tark_vuosi
	 * @param results
	 * @return sorter results
	 */
	@Override
	public List<_PesaDomainModelRow> sort(List<_PesaDomainModelRow> results) {
		List<_PesaDomainModelRow> resultList = new LinkedList<>();
		for (_PesaDomainModelRow nest : nestsInRightOrder(results)) {
			addAllInspectionsOfTheNestToResultList(resultList, nest);
		}
		return resultList;
	}

	private void addAllInspectionsOfTheNestToResultList(List<_PesaDomainModelRow> resultsInNewOrder, _PesaDomainModelRow nest) {
		for (_PesaDomainModelRow inspection : inspectionsByNest.get(nest.getPesa().getRowidValue())) {
			resultsInNewOrder.add(inspection);
		}
	}

	/**
	 * This goes through the original result list, orders the nests and then returns the order of the nests.
	 * Note: this also adds all inspections to inspectionsByNest -map/list. A little confusing....
	 * @param results
	 * @return
	 */
	private List<_PesaDomainModelRow> nestsInRightOrder(List<_PesaDomainModelRow> results) {
		List<_PesaDomainModelRow> sortCriteria = new ArrayList<>(results.size());
		List<_PesaDomainModelRow> inspectionsOfThisNest = new ArrayList<>(50);
		String prev_nest = "initialvalue";
		for (_PesaDomainModelRow inspection : results) {
			String this_nest = inspection.getPesa().getRowidValue();
			if (!prev_nest.equals(this_nest)) {
				sortCriteria.add(inspection);
				inspectionsOfThisNest = new ArrayList<>(50);
				inspectionsByNest.put(this_nest, inspectionsOfThisNest);
				prev_nest = this_nest;
			}
			inspectionsOfThisNest.add(inspection);
		}
		Collections.sort(sortCriteria, comparator);
		return sortCriteria;
	}



}
