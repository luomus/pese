package fi.hy.pese.framework.main.db;

import java.sql.SQLException;

public class Results implements _ResultSet {

	private final java.sql.ResultSet	rs;

	public Results(java.sql.ResultSet rs) {
		this.rs = rs;
	}

	@Override
	public boolean next() throws SQLException {
		return rs.next();
	}

	@Override
	public String getString(int i) throws SQLException {
		return rs.getString(i);
	}

	@Override
	public String getString(String columnLabel) throws SQLException {
		return rs.getString(columnLabel);
	}

	@Override
	public java.sql.Date getDate(int i) throws SQLException {
		return rs.getDate(i);
	}

	@Override
	public java.sql.Date getDate(String columnLabel) throws SQLException {
		return rs.getDate(columnLabel);
	}
}
