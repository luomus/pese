<#--
<@checkerror errors field.fullname />
<@errorlist errors texts />
<@statustexts system texts />

<@selection selections "updateparameters" updateparameters. 0|1|2 multiple? firstEmpty? showValue? errors />
<@inputText "updateparameters" updateparameters. sized? errors />
<@textarea "updateparameters" updateparameters. rowsN colsN errors />  
<@date "updateparameters" updateparameters. errors />
<@inputHidden "updateparameters" updateparameters. /> 
<@radio "updateparameters" updateparameters. "A" errors />
<@inputSelectCombo "updateparameters" updateparameters. sized? showValue? errors />
-->

<#assign ROW_ID = 0>
<#assign UNIQUE_KEY = 1>
<#assign IMMUTABLE_KEY = 2>
<#assign NORMAL_FIELD = 3>
<#assign DATE_ADDED = 4>
<#assign DATE_MODIFIED = 5>
<#assign PASSWORD = 6>
<#assign USER_ID = 7>

<#assign TABLEMANAGEMENT_NO_PERMISSIONS = "0">
<#assign TABLEMANAGEMENT_UPDATE_ONLY = "1">
<#assign TABLEMANAGEMENT_UPDATE_INSERT = "2">
<#assign TABLEMANAGEMENT_UPDATE_INSERT_DELETE = "3">
<#assign TABLEMANAGEMENT_INSERT_ONLY = "4">
	
<#assign  VARCHAR = 1>
<#assign  INTEGER = 2>
<#assign  DECIMAL = 3>
<#assign  DATE = 4>


<#macro checkerror errors columnName >  
	<#if errors[columnName]??> 
		class="errorfield"
	</#if>
	<@checkwarning columnName /> 
</#macro>

<#macro checkwarning columnName >  
	<#if warnings[columnName]??> class="warningfield"</#if> 
</#macro>

<#macro warninglist warnings texts>
  <#if (warnings?size > 0) >
    <table class="warning" >
      <#assign errorfields = warnings?keys>
      <#list errorfields as errorfield> 
        <tr id="ackWarning_${errorfield_index}" > 
          <td> ${texts[errorfield]!errorfield} : </td> 
          <td> 
          		${texts[warnings[errorfield]]!warnings[errorfield]} <button class="ackWarning">Kuittaa</button>
          		<script type="text/javascript">
          			$('#ackWarning_${errorfield_index}').click(function() {
						$(this).hide(300); 
						var e = $('#bypassed_warnings'); 
						e.val( e.val() + '|' + '${errorfield}');
						return false;
					});
          		</script>
          </td>  
        </tr> 
      </#list>
    </table>
    <br />
    <br />
  </#if>
</#macro>

<#macro errorlist errors texts>
  <#if (errors?size > 0) >
    <table class="error" >
      <#assign errorfields = errors?keys>
      <#list errorfields as errorfield> <tr> <td> ${texts[errorfield]!errorfield} : </td> <td> ${texts[errors[errorfield]]!errors[errorfield]} </td> </tr> </#list>
    </table>
    <br />
    <br />
  </#if>
</#macro>

<#macro statustexts systemvalues texts>
  <#if systemvalues.success != ""> <p class="success" >${texts[systemvalues.success]!systemvalues.success} </p> </#if>
  <#if systemvalues.failure != ""> <p class="error"   >${texts[systemvalues.failure]!systemvalues.failure} </p> </#if>
  <#list system.notices as notice> <p class="notice"  >${texts[notice]!notice} </p> </#list>
</#macro>

<#macro selection selections prefix column size multiple firstEmpty showValue errors >
  <#assign thisSelections = selections[column.assosiatedSelection].options> 
  <#assign thisSize = size> <#if size == 2><#assign thisSize = selections[column.assosiatedSelection].size><#if firstEmpty><#assign thisSize = thisSize +1></#if></#if>  <#if multiple><#if thisSize == 1><#assign thisSize=0></#if></#if>
  <#assign prevGroup = "">
  <#assign groupOpen = false>
  <select name="${prefix +"." + column.fullname}" id="${prefix +"." + column.fullname}" <@checkerror errors column.fullname /> <#if multiple>multiple="multiple"</#if> <#if (thisSize > 0)>size="${thisSize}"</#if>  > 
    <#if firstEmpty><option value=""></option></#if>
    <#list thisSelections as option> 
    	<#if option.group != prevGroup> <#if groupOpen> </optgroup> </#if> <#if option.group != ""><optgroup label="${option.group}"> <#assign groupOpen = true> <#else> <#assign groupOpen = false> </#if> <#assign prevGroup = option.group> </#if> 
    	<#assign thisSelected = (column.value == option.value)> <#if thisSelected == false> <#list column.valueList as value> <#if value == option.value> <#assign thisSelected = true> <#break> </#if> </#list> </#if> <option value="${option.value}" <#if thisSelected> selected="selected" </#if> ><#if showValue>${option.value} - </#if>${option.text}</option> 
    </#list>
    <#if groupOpen> </optgroup> </#if>
  </select>
</#macro>

<#macro selectionChosen selections prefix column size multiple firstEmpty showValue errors >
  <#assign thisSelections = selections[column.assosiatedSelection].options> 
  <#assign thisSize = size> <#if size == 2><#assign thisSize = selections[column.assosiatedSelection].size><#if firstEmpty><#assign thisSize = thisSize +1></#if></#if>  <#if multiple><#if thisSize == 1><#assign thisSize=0></#if></#if>
  <#assign prevGroup = "">
  <#assign groupOpen = false>
  <select name="${prefix +"." + column.fullname}" id="${prefix +"." + column.fullname}" <@checkerror errors column.fullname /> <#if multiple>multiple="multiple"</#if> <#if (thisSize > 0)>size="${thisSize}"</#if>  > 
    <#if firstEmpty><option value="">-</option></#if>
    <#list thisSelections as option> 
    	<#if option.group != prevGroup> <#if groupOpen> </optgroup> </#if> <#if option.group != ""><optgroup label="${option.group}"> <#assign groupOpen = true> <#else> <#assign groupOpen = false> </#if> <#assign prevGroup = option.group> </#if> 
    	<#assign thisSelected = (column.value == option.value)> <#if thisSelected == false> <#list column.valueList as value> <#if value == option.value> <#assign thisSelected = true> <#break> </#if> </#list> </#if> <option value="${option.value}" <#if thisSelected> selected="selected" </#if> ><#if showValue>${option.value} - </#if>${option.text}</option> 
    </#list>
    <#if groupOpen> </optgroup> </#if>
  </select>
  <script type="text/javascript">$('#${(prefix +"." + column.fullname)?replace(".","\\\\.")}').addClass('chosen').attr('data-placeholder','Valitse...');</script>
</#macro>

<#macro inputSelectCombo prefix column sized showValue errors >
  <#assign inputId = prefix +"."+ column.fullname +".input">
  <#assign selectorId = prefix +"."+ column.fullname +".selector">
  <#assign thisSelections = selections["municipalities"].options>
  <input type="text" name="${ prefix +"."+ column.fullname }" id="${inputId}" <#if (prefix == "updateparameters")> maxlength="${column.inputSize}" </#if> <#if sized>size="${column.inputSize}"</#if> value="${column.value}" <@checkerror errors column.fullname />  onkeyup="syncSelectElementWithInputElement('${inputId}', '${selectorId}');"  />
  <select name="${selectorId}" id="${selectorId}" <@checkerror errors column.fullname /> onchange="syncInputElementWithSelectElement('${inputId}', '${selectorId}');" >
        <option value=""></option>
        <#list thisSelections as option> <option value="${option.value}" <#if column.value == option.value> selected="selected" </#if> >${option.text} <#if showValue>(${option.value})</#if> </option> </#list>
    </select>
   <script type="text/javascript">
   		$('#${selectorId?replace(".","\\\\.")}').addClass('chosen').attr('data-placeholder','Kirjoita kenttään tai valitse...');
   		$('#${inputId?replace(".","\\\\.")}').on('keyup change', function(){ 
   			$(this).removeClass('errorfield'); 
   			var e = $('#${selectorId?replace(".","\\\\.")}');
   			e.val('');
   			e.trigger("liszt:updated"); 
   		});
   </script>
</#macro>

<#macro selectionWithNull selections prefix column size multiple firstEmpty showValue errors >
  <#assign thisSelections = selections[column.assosiatedSelection].options> 
  <select class="chosen" name="${prefix +"." + column.fullname}" id="${prefix +"." + column.fullname}" <@checkerror errors column.fullname /> <#if multiple>multiple="multiple"</#if> > 
    <#if firstEmpty><option value=""></option></#if>
    <#assign nullSelected = false>
    <#list column.valueList as value> <#if value == "NULL"> <#assign nullSelected = true> <#break> </#if> </#list>
    <option value="NULL" <#if nullSelected> selected="selected" </#if> > NULL - Tyhjä </option>
    <#list thisSelections as option> <#assign thisSelected = (column.value == option.value)> <#if thisSelected == false> <#list column.valueList as value> <#if value == option.value> <#assign thisSelected = true> <#break> </#if> </#list> </#if> <option value="${option.value}" <#if thisSelected> selected="selected" </#if> > <#if showValue>${option.value} - </#if> ${option.text}</option> </#list>
  </select>
  <script type="text/javascript">$('#${(prefix +"." + column.fullname)?replace(".","\\\\.")}').addClass('chosen').attr('data-placeholder','Valitse...');</script>
</#macro>

<#macro fieldSelection table name texts >
  <select class="chosen fieldSelection" data-placeholder="Valitse kentät" id="fieldSelection_${name}" multiple="multiple" onchange="generateAdvancedSearch('${name}');">
    <#list table.columns as column> <#if column.fieldType != ROW_ID && column.fieldType != DATE_ADDED &&  column.fieldType != DATE_MODIFIED> <option value="${column.fullname}" <#if lists.selectedFields??><@checkIfSelected resultcolumns column/></#if> >${ texts[column.fullname]!column.name }</option> </#if> </#list>
  </select> 
</#macro>

<#macro searchFields table texts >
    <#list table.columns as column> <#assign name  = "searchparameters."+column.fullname> <#if column.fieldType != ROW_ID && column.fieldType != DATE_ADDED &&  column.fieldType != DATE_MODIFIED>
        <div id="advanced_search_field_${column.fullname}"  > ${ texts[ column.fullname ]!column.name }:<br />
          <#if column.inputType == "TEXT">      <@inputText "searchparameters" column false errors /> </#if>
          <#if column.inputType == "TEXTAREA">  <@inputText "searchparameters" column false errors /> </#if>
          <#if column.inputType == "SELECT">    <@selectionWithNull selections "searchparameters" column 0 true true true errors /> </#if>
          <#if column.inputType == "DATE">      <@date "searchparameters" column errors /> </#if>
          <br /> </div> </#if> 
    </#list>
</#macro>


<#macro inputText prefix column sized errors > <input type="text" name="${ prefix +"."+ column.fullname }" <#if prefix == "updateparameters"> id="${prefix +"." + column.fullname}" </#if> value="${column.value}" <#if (prefix == "updateparameters")> maxlength="${column.inputSize}" </#if>  <#if sized>size="${column.inputSize}"</#if> <@checkerror errors column.fullname /> /> </#macro>

<#macro inputHidden prefix column > <input type="hidden" name="${ prefix +"."+ column.fullname }" <#if prefix == "updateparameters"> id="${prefix +"." + column.fullname}" </#if> value="${ column.value }" /> </#macro>

<#macro textarea prefix column rows cols errors > <textarea name="${ prefix +"."+ column.fullname }" <#if prefix == "updateparameters"> id="${prefix +"." + column.fullname}" </#if> rows="${rows}" cols="${cols}" <@checkerror errors column.fullname /> >${column.value}</textarea>  </#macro>

<#macro date prefix column errors > <span style="white-space:nowrap;"> <input type="text" name="${ prefix +"."+ column.fullname }.dd" <#if prefix == "updateparameters"> id="${ prefix +"."+ column.fullname }.dd" </#if> size="2" value ="${column.dateValue.dd}"   <@checkerror errors column.fullname />  />.<input type="text" name="${ prefix +"."+ column.fullname }.mm"  <#if prefix == "updateparameters"> id="${ prefix +"."+ column.fullname }.mm" </#if> size="2" value ="${column.dateValue.mm}"   <@checkerror errors column.fullname />  />.<input type="text" name="${ prefix +"."+ column.fullname }.yyyy" <#if prefix == "updateparameters"> id="${ prefix +"."+ column.fullname }.yyyy" </#if> size="4" value ="${column.dateValue.yyyy}" <@checkerror errors column.fullname />  /> </span> </#macro>


<#macro coordinateSelection errors searchName>
  <#assign inputId = "CoordinateSearchRadius.input."+searchName>
  <#assign selectorId = "CoordinateSearchRadius.selector."+searchName>
  <input type="text" name="CoordinateSearchRadius" id="${inputId}" size="7" value="${(data.CoordinateSearchRadius)!""}" <@checkerror errors "CoordinateSearchRadius" />  /> m 
  <select name="${selectorId}" id="${selectorId}" <@checkerror errors "CoordinateSearchRadius" /> onchange="syncInputElementWithSelectElement('${inputId}', '${selectorId}');">
        <option value=""></option>
              <option value="200"    <#if (data.CoordinateSearchRadius == "200") >    selected="selected" </#if> >200 m</option>
              <option value="1000"   <#if (data.CoordinateSearchRadius == "1000") >   selected="selected" </#if> >1000 m</option>
              <option value="10000"  <#if (data.CoordinateSearchRadius == "10000") >  selected="selected" </#if> >10 km</option>
              <option value="50000"  <#if (data.CoordinateSearchRadius == "50000") >  selected="selected" </#if> >50 km</option>
              <option value="100000" <#if (data.CoordinateSearchRadius == "100000") > selected="selected" </#if> >100 km</option>
    </select>
</#macro>


