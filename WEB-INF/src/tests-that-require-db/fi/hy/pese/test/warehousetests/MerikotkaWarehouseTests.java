package fi.hy.pese.test.warehousetests;

import java.io.File;
import java.net.URL;

import fi.hy.pese.framework.main.db._DAO;
import fi.hy.pese.framework.main.general.WarehouseRowGenerator;
import fi.hy.pese.framework.main.general.WarehouseRowGenerator.WarehouseGenerationDefinition;
import fi.hy.pese.framework.main.general.data.PesaDomainModelDatabaseStructure;
import fi.hy.pese.framework.main.general.data.PesaDomainModelRowFactory;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.test.TestConfig;
import fi.hy.pese.framework.test.db.DAOStub;
import fi.hy.pese.merikotka.main.app.MerikotkaDatabaseStructureCreator;
import fi.hy.pese.merikotka.main.app.MerikotkaFactory;
import fi.hy.pese.merikotka.main.app.MerikotkaWarehouseDefinition;
import fi.laji.datawarehouse.etl.models.dw.DwRoot;
import fi.laji.datawarehouse.etl.models.dw.Gathering;
import fi.luomus.commons.utils.FileUtils;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class MerikotkaWarehouseTests {

	public static Test suite() {
		return new TestSuite(MerikotkaWarehouseTests.class.getDeclaredClasses());
	}

	public static class WhenGeneratingWarehouseDocument extends TestCase {

		public class FakeDAO<T extends _PesaDomainModelRow> extends DAOStub<T> {
			public FakeDAO() {}

		}

		private MerikotkaFactory application;
		private _DAO<_PesaDomainModelRow> dao;
		private WarehouseRowGenerator generator;
		private _PesaDomainModelRow row;

		@Override
		protected void setUp() throws Exception {
			application = new MerikotkaFactory(TestConfig.getConfigFile("pese_merikotka.config"));

			PesaDomainModelDatabaseStructure structure = MerikotkaDatabaseStructureCreator.hardCoded();
			application.setRowFactory(new PesaDomainModelRowFactory(structure, null, null));
			
			application.loadUITexts();
			dao = application.dao(MerikotkaWarehouseTests.class.getSimpleName());
			WarehouseGenerationDefinition definition = MerikotkaWarehouseDefinition.instance();
			generator = new WarehouseRowGenerator(definition, dao, application);
			row = dao.newRow();
		}

		@Override
		protected void tearDown() throws Exception {
			if (application != null) if (dao != null) application.release(dao);
		}

		public void test__delete() throws Exception {
			DwRoot root = generator.deletedEntry(123);
			assertRoot("merikotka_1_delete.json", root);
		}

		public void test__not_inspected() throws Exception {
			row.getPesa().getUniqueNumericIdColumn().setValue(1);
			row.getTarkastus().getUniqueNumericIdColumn().setValue(100);
			row.getTarkastus().get("tark_vuosi").setValue(2020);
			row.getColumn("pesatarkastus.tark_tapa").setValue("8");
			DwRoot root = generator.generate(row);
			assertRoot("merikotka_2_not_inspected.json", root);
		}

		public void test__common() throws Exception {
			commonTestData();
			DwRoot root = generator.generate(row);
			assertRoot("merikotka_3_common.json", root);
		}

		private void commonTestData() {
			row.getPesa().getUniqueNumericIdColumn().setValue(1);
			row.getTarkastus().getUniqueNumericIdColumn().setValue(100);
			row.getColumn("vuosi.pesan_kunta").setValue("SIIPYY");

			row.getTarkastus().get("tark_pvm").setValue("03.06.2004");
			row.getTarkastus().get("tark_pvm_tark").setValue("1");
			row.getTarkastus().get("tark_vuosi").setValue(2004);
			row.getColumn("pesatarkastus.tark_tapa").setValue("1");
			row.getTarkastus().get("tarkastaja1_id").setValue(846);
			row.getTarkastus().get("tarkastaja2_id").setValue(1100);

			row.getPesa().get("tarkka_sijainti").setValue("Siipyy");
			row.getPesa().get("koord_mittaus").setValue("G");
			row.getPesa().get("koord_tark").setValue("0");
			row.getPesa().get("eur_leveys").setValue(6891245);
			row.getPesa().get("eur_pituus").setValue(201234);
			
			row.getOlosuhde().get("palsta_omistaja").setValue("P");
			row.getOlosuhde().get("omist_kommentti").setValue("Should not show");

			row.getOlosuhde().get("sijainti").setValue("O");
			row.getPesa().get("uhat").setValue("Should not show");
			
			row.getTarkastus().get("pesimistulos").setValue("R");
			row.getTarkastus().get("nahdyt_merkit").setValue("R");
			row.getTarkastus().get("pesa_kunto").setValue("P");

			row.getTarkastus().get("elavia_lkm").setValue("2");
			
			row.getTarkastus().get("mittaaja_id_1").setValue("100");
			row.getColumn("poikanen.1.rengas_oikea").setValue("A123");
			row.getColumn("poikanen.1.siipi_pituus").setValue("310");
			row.getPoikaset().get(1).getUniqueNumericIdColumn().setValue(789);
		}

		public void test__muulaji() throws Exception {
			commonTestData();
			row.getTarkastus().get("muulaji").setValue("BUCCLA");
			row.getTarkastus().get("muulaji_vastarakki").setValue("K");
			row.getTarkastus().get("muulaji_rinnakkainen").setValue("MOTCIT");
			DwRoot root = generator.generate(row);
			assertRoot("merikotka_6_other_species.json", root);
		}

		public void test__date_accuracy() throws Exception {
			commonTestData();
			row.getTarkastus().get("tark_pvm_tark").setValue("2");
			DwRoot root = generator.generate(row);
			for (Gathering g : root.getPublicDocument().getGatherings()) {
				assertEquals("2004-06-01 - 2004-06-30", g.getDisplayDateTime());
			}

			row.getTarkastus().get("tark_pvm_tark").setValue("3");
			root = generator.generate(row);
			for (Gathering g : root.getPublicDocument().getGatherings()) {
				assertEquals("2004-01-01 - 2004-12-31", g.getDisplayDateTime());
			}

			row.getTarkastus().get("tark_pvm_tark").setValue("4");
			root = generator.generate(row);
			for (Gathering g : root.getPublicDocument().getGatherings()) {
				assertEquals("2004-01-01 - 2004-12-31", g.getDisplayDateTime());
			}
		}

		public void test__dead_nest() throws Exception {
			commonTestData();
			row.getColumn("pesatarkastus.pesa_kunto").setValue("D");
			DwRoot root = generator.generate(row);
			assertRoot("merikotka_7_dead_nest.json", root);
		}
		
		public void test__coordinate_uncertainty() throws Exception {
			commonTestData();
			row.getColumn("pesavakio.koord_tark").setValue("S");
			DwRoot root = generator.generate(row);
			assertEquals("6891245.0 : 6891246.0 : 201234.0 : 201235.0 : EUREF : 1000", root.getPublicDocument().getGatherings().get(0).getCoordinates().toString());
		}

		private void assertRoot(String filename, DwRoot root) {
			URL url = this.getClass().getResource(filename);
			File file = new File(url.getFile());
			try {
				String data = FileUtils.readContents(file);
				assertEquals(data, root.toJSON().beautify());
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}

	}

}
