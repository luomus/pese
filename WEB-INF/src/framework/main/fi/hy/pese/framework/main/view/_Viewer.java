package fi.hy.pese.framework.main.view;

import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.general.data._Row;

/**
 * A viewer which the control uses at the end of request processing to show the results of functionality actions
 */
public interface _Viewer {

	/**
	 * Show view for request
	 * @param request
	 * @throws Exception
	 */
	void viewFor(_Request<? extends _Row> request) throws Exception;

}
