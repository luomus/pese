package fi.hy.pese.merikotka.main.app;

import java.util.Map;

import fi.hy.pese.framework.main.app.BaseFactoryForApplicationSpecificationFactory;
import fi.hy.pese.framework.main.app._Functionality;
import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.app.functionality.CoordinateRadiusSearcherFunctionality;
import fi.hy.pese.framework.main.app.functionality.FilemanagementFunctionality;
import fi.hy.pese.framework.main.app.functionality.FilemanagementFunctionality.FileListGeneratorImple;
import fi.hy.pese.framework.main.app.functionality.LintuvaaraLoginFunctionality;
import fi.hy.pese.framework.main.app.functionality.LoginFunctionalityWithSingleUser;
import fi.hy.pese.framework.main.app.functionality.LogoutFunctionality;
import fi.hy.pese.framework.main.app.functionality.PesintamanagementFunctionality;
import fi.hy.pese.framework.main.db._DAO;
import fi.hy.pese.framework.main.general.WarehouseRowGenerator;
import fi.hy.pese.framework.main.general._WarehouseRowGenerator;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data.ParameterMap;
import fi.hy.pese.framework.main.general.data._Data;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.main.kirjekyyhky._KirjekyyhkyXMLGenerator;
import fi.hy.pese.framework.test.app.functionality.EmptyFunctionality;
import fi.hy.pese.merikotka.main.functionality.MerikotkaKirjekyyhkyManagementFunctionality;
import fi.hy.pese.merikotka.main.functionality.MerikotkaKirjekyyhkyXMLGenerator;
import fi.hy.pese.merikotka.main.functionality.MerikotkaSearchFunctionality;
import fi.hy.pese.merikotka.main.functionality.MerikotkaTableManagementFunctionality;
import fi.hy.pese.merikotka.main.functionality.MerikotkaTarkastusFunctionality;
import fi.hy.pese.merikotka.main.functionality.MerikotkaUusipesaFunctionality;
import fi.luomus.commons.config.Config;
import fi.luomus.commons.containers.Selection;
import fi.luomus.commons.containers.SelectionImple;
import fi.luomus.commons.containers.SelectionOptionImple;
import fi.luomus.commons.tipuapi.TipuAPIClient;
import fi.luomus.commons.utils.AESDecryptor;

public class MerikotkaFactory extends BaseFactoryForApplicationSpecificationFactory<_PesaDomainModelRow> {
	
	public MerikotkaFactory(String configFileName) throws Exception {
		super(configFileName);
	}
	
	@Override
	public String frontpage() {
		return Const.SEARCH_PAGE; // Warning: frontpage must be defined bellow (functionalityFor()) or an infinite loop will happen
	}
	
	private AESDecryptor	decryptor	= null;
	
	private AESDecryptor getAESDecryptor() {
		if (decryptor == null) {
			try {
				decryptor = new AESDecryptor(config().get(Config.LINTUVAARA_PUBLIC_RSA_KEY));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return decryptor;
	}
	
	@Override
	public _Functionality<_PesaDomainModelRow> functionalityFor(String page, _Request<_PesaDomainModelRow> request) {
		if (page.equals(Const.LOGIN_PAGE)) {
			if (config().developmentMode()) {
				return new LoginFunctionalityWithSingleUser<>(request, "z", "z");
			}
			return new LintuvaaraLoginFunctionality<>(request, getAESDecryptor(), getErrorReporter());
		}
		else if (page.equals(Const.TABLEMANAGEMENT_PAGE))
			return new MerikotkaTableManagementFunctionality(request);
		else if (page.equals(Const.SEARCH_PAGE))
			return new MerikotkaSearchFunctionality(request);
		else if (page.equals(Const.TARKASTUS_PAGE))
			return new MerikotkaTarkastusFunctionality(request);
		else if (page.equals(Const.INSERT_FIRST_PAGE))
			return new MerikotkaUusipesaFunctionality(request);
		else if (page.equals(Const.REPORTS_PAGE))
			return new MerikotkaSearchFunctionality(request);
		else if (page.equals(Const.FILEMANAGEMENT_PAGE))
			return new FilemanagementFunctionality<>(request, new FileListGeneratorImple(request.config().pdfFolder(), request.config().reportFolder()));
		else if (page.equals(Const.KIRJEKYYHKY_MANAGEMENT_PAGE))
			return new MerikotkaKirjekyyhkyManagementFunctionality(request);
		else if (page.equals("aputiedot_menu")) {
			return new EmptyFunctionality<>();
		} else if (page.equals(Const.PESINTAMANAGEMENT_PAGE)) {
			return new PesintamanagementFunctionality(request, "tark_vuosi");
		}	else if (page.equals(Const.COORDINATE_RADIUS_SEARCH_PAGE)) {
			return new CoordinateRadiusSearcherFunctionality(request, new MerikotkaSearchFunctionality(request));
		}
		else if (page.equals(Const.LOGOUT_PAGE))
			return new LogoutFunctionality<>(request);
		else if (page.equals("luo")) {
			try {
				MerikotkaKirjekyyhkyXMLGenerator generator = new MerikotkaKirjekyyhkyXMLGenerator(request);
				generator.writeStructureXML();
			} catch (Exception e) {
				e.printStackTrace();
			}
			request.data().setPage(Const.FILEMANAGEMENT_PAGE);
			return functionalityFor(Const.FILEMANAGEMENT_PAGE, request);
		}
		//		else if (page.equals("temp"))
		//			return new TempBatchProsessingFunctionality(request);
		else {
			request.data().setPage(frontpage());
			return functionalityFor(frontpage(), request);
		}
	}
	
	@Override
	protected Map<String, Selection> fetchSelections(_DAO<_PesaDomainModelRow> dao) throws Exception {
		Map<String, Selection> selections = dao.returnCodeSelections(MerikotkaConst.APUTAULU, "taulu", "attribuutti", "arvo", "selite", "ryhmittely", new String[] {"jarjestys", "arvo"});
		selections.put(MerikotkaConst.REVIIRIT, dao.returnSelections("reviiri", "reviiri_id", "reviirinimi", new String[] { "reviirinimi", "reviiri_id"}));
		selections.put(MerikotkaConst.TAULUT, dao.returnSelections("aputaulu", "taulu", "taulu", null));
		selections.put(MerikotkaConst.SUURALUEET, dao.returnSelections("suuralue", "id", "nimi", new String[] {"nimi"}));
		TipuAPIClient tipuAPI = null;
		try {
			tipuAPI = this.tipuAPI();
			selections.put(TipuAPIClient.RINGERS, tipuAPI.getAsSelection(TipuAPIClient.RINGERS, 2));
			selections.put(TipuAPIClient.MUNICIPALITIES, tipuAPI.getAsSelection(TipuAPIClient.MUNICIPALITIES, 1));
			selections.put(TipuAPIClient.SPECIES, tipuAPI.getAsSelection(TipuAPIClient.SPECIES, 1));
			selections.put(TipuAPIClient.ELY_CENTRES, tipuAPI.getAsSelection(TipuAPIClient.ELY_CENTRES, 1));
		} finally {
			if (tipuAPI != null) tipuAPI.close();
		}
		
		SelectionImple k_e = new SelectionImple("k_e");
		k_e.addOption(new SelectionOptionImple(MerikotkaConst.YES, "Kyllä"));
		k_e.addOption(new SelectionOptionImple(MerikotkaConst.NO, "Ei"));
		selections.put(MerikotkaConst.YES_NO_SELECTION, k_e);
		
		return selections;
	}
	
	@Override
	public _KirjekyyhkyXMLGenerator kirjekyyhkyFormDataXMLGenerator(_Request<_PesaDomainModelRow> request) throws Exception {
		return new MerikotkaKirjekyyhkyXMLGenerator(request);
	}
	
	@Override
	public _Data<_PesaDomainModelRow> initData(String language, ParameterMap params) throws Exception {
		_Data<_PesaDomainModelRow> data = super.initData(language, params);
		data.set(Const.LINTUVAARA_URL, getLintuvaaraURL());
		return data;
	}

	@Override
	public _WarehouseRowGenerator getWarehouseRowGenerator(_DAO<_PesaDomainModelRow> dao) {
		return new WarehouseRowGenerator(MerikotkaWarehouseDefinition.instance(), dao, this);
	}

}
