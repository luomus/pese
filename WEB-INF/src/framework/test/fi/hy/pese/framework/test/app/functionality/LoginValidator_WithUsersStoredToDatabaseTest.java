package fi.hy.pese.framework.test.app.functionality;

import fi.hy.pese.framework.main.app.Request;
import fi.hy.pese.framework.main.app.RequestImpleData;
import fi.hy.pese.framework.main.app.functionality.validate.LoginValidatorWithUsersStoredToDatabase;
import fi.hy.pese.framework.main.app.functionality.validate._Validator;
import fi.hy.pese.framework.main.db.oraclesql.OracleSQL_DAO;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data.Data;
import fi.hy.pese.framework.main.general.data.DatabaseTableStructure;
import fi.hy.pese.framework.main.general.data.PesaDomainModelDatabaseStructure;
import fi.hy.pese.framework.main.general.data.Table;
import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._Data;
import fi.hy.pese.framework.main.general.data._Row;
import fi.hy.pese.framework.main.general.data._Table;
import fi.hy.pese.framework.test.RowFactory;
import fi.luomus.commons.utils.Utils;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class LoginValidator_WithUsersStoredToDatabaseTest {

	public static Test suite() {
		return new TestSuite(LoginValidator_WithUsersStoredToDatabaseTest.class.getDeclaredClasses());
	}

	public static class WhenLoggingIn extends TestCase {

		private class FakeDAO extends OracleSQL_DAO<_Row> {

			public FakeDAO() {
				super(null, null);
			}

			@Override
			public boolean checkValuesExists(_Table table) {
				try {
					if (table.get("username").getValue().equals("LOKKI") && // keys are toUppercase
							table.get("password").getValue().equals(Utils.encrypt("lokki2"))) return true;
					return false;
				} catch (Exception e) {
					fail();
				}
				return false;
			}
			@Override
			public _Table newTableByName(String name) throws IllegalArgumentException {
				_Table table = new Table(structure.getTableStructureByName(name));
				return table;
			}
		}

		private _Validator	validator;
		private _Data<_Row>		data;
		private PesaDomainModelDatabaseStructure structure;

		@Override
		protected void setUp() {
			structure = new PesaDomainModelDatabaseStructure();
			DatabaseTableStructure userLoginInfo = new DatabaseTableStructure("kayttaja");
			userLoginInfo.addColumn("username", _Column.VARCHAR, 25);
			userLoginInfo.addColumn("password", _Column.VARCHAR, 256);
			userLoginInfo.get("username").setTypeToImmutablePartOfAKey();
			userLoginInfo.get("password").setTypeToPasswordColumn();
			structure.defineCustomTable(userLoginInfo);
			data = new Data<>(new RowFactory());
			validator = new LoginValidatorWithUsersStoredToDatabase<>(new Request<>(new RequestImpleData<>().setData(data).setDao(new FakeDAO())));
		}

		public void test___no_errors_if_no_action() throws Exception {
			validator.validate();
			assertFalse(validator.hasErrors());
		}

		public void test___fails_empty_password() throws Exception {
			data.setAction(Const.CHECK_LOGIN);
			validator.validate();
			assertTrue("login should fail", validator.hasErrors());
			assertEquals(_Validator.LOGIN_INCORRECT, validator.errors().get(Const.LOGIN_FIELDS));
		}

		public void test___passes_correct_password() throws Exception {
			data.set(Const.LOGIN_USERNAME, "lokki");
			data.set(Const.LOGIN_PASSWORD, "lokki2");
			data.setAction(Const.CHECK_LOGIN);
			validator.validate();
			assertFalse("login should succeed", validator.hasErrors());
		}

	}

}
