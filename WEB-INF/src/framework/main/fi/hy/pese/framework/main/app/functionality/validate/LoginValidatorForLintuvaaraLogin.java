package fi.hy.pese.framework.main.app.functionality.validate;

import java.util.HashMap;
import java.util.Map;

import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.app.functionality.LintuvaaraLoginFunctionality;
import fi.hy.pese.framework.main.general.data._Row;
import fi.luomus.commons.reporting.ErrorReporter;
import fi.luomus.commons.utils.AESDecryptor;
import fi.luomus.commons.utils.AESDecryptor.AESDecryptionException;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.xml.Document;
import fi.luomus.commons.xml.Document.Node;
import fi.luomus.commons.xml.XMLReader;

public class LoginValidatorForLintuvaaraLogin<T extends _Row> extends Validator<T> {

	private static final String		ADMIN						= "admin";
	private static final String		USER_TYPE					= "user_type";
	private static final String		NAME						= "name";
	private static final String		LOGIN_ID					= "login_id";
	private static final String		AUTH_FOR					= "auth_for";
	private static final String		EXPIRES_AT					= "expires_at";
	public static final String		USERMODEL_ATTRIBUTE_PREFIX	= "usermodel_";
	private final ErrorReporter	errorReporter;
	private final String			systemID;
	private final AESDecryptor		decryptor;

	public LoginValidatorForLintuvaaraLogin(AESDecryptor decryptor, ErrorReporter errorReporter, _Request<T> request) {
		super(request);
		this.errorReporter = errorReporter;
		this.systemID = request.config().systemId();
		this.decryptor = decryptor;
	}

	/**
	 * Decrypts usermodel, checks it is for the right system and has not expired.
	 * Usermodel example:
	 * <login type="maallikko">
	 * <login_id>petrus.repo@iki.fi</login_id>
	 * <expires_at>1236252609</expires_at>
	 * <auth_for>Peto</auth_for>
	 * </login>
	 */
	@Override
	public void validate() throws Exception {
		String key = data.get(LintuvaaraLoginFunctionality.KEY);
		String iv = data.get(LintuvaaraLoginFunctionality.IV);
		String model = data.get(LintuvaaraLoginFunctionality.DATA);

		if (isNull(key) || isNull(iv) || isNull(model)) {
			failLogin();
			return;
		}

		try {
			model = decryptor.decrypt(key, iv, model);
		} catch (AESDecryptionException dex) {
			failLogin();
			reportAttempt(dex, key, iv, model);
			return;
		}
		
		Map<String, String> usermodel = null;
		try {
			usermodel = parseUserModel(model);
		} catch (Exception e) {
			failLogin();
			reportModelError(model, e);
			return;
		}

		if (invalidSystemID(usermodel)) {
			failLogin();
			reportAuthForError(model);
			return;
		}

		if (!request.config().developmentMode()) {
			if (hasExpired(usermodel)) {
				failLogin();
				reportExpired(model);
				return;
			}
		}

		if (notAdminUser(usermodel)) {
			failLogin();
			reportNotAdminAttempt(model);
		}

		storeUsermodelToSession(usermodel);
	}

	private void reportNotAdminAttempt(String model) {
		StringBuilder error = errorHeader();
		error.append("Usermodel is not for an admin user \n");
		error.append(model).append("\n");
		errorReporter.report(error.toString());
	}

	private boolean notAdminUser(Map<String, String> usermodel) {
		return !usermodel.get(USER_TYPE).equals(ADMIN);
	}

	private void storeUsermodelToSession(Map<String, String> usermodel) {
		usermodel.remove(EXPIRES_AT);
		usermodel.remove(AUTH_FOR);
		for (Map.Entry<String, String> e : usermodel.entrySet()) {
			request.session().put(USERMODEL_ATTRIBUTE_PREFIX + e.getKey(), e.getValue());
			if (e.getKey().equals(LOGIN_ID)) request.session().setUserId(e.getValue());
			if (e.getKey().equals(NAME)) request.session().setUserName(e.getValue());
			if (e.getKey().equals(USER_TYPE)) request.session().setUserType(e.getValue());
		}
	}

	private void reportExpired(String model) {
		StringBuilder error = errorHeader();
		error.append("Usermodel has expired \n");
		error.append(model).append("\n");
		errorReporter.report(error.toString());
	}

	private boolean hasExpired(Map<String, String> usermodel) {
		long current_time = DateUtils.getCurrentEpoch();
		long expires = new Integer(usermodel.get(EXPIRES_AT));
		return (current_time > expires);
	}

	private boolean invalidSystemID(Map<String, String> usermodel) {
		return !systemID.equalsIgnoreCase(usermodel.get(AUTH_FOR));
	}

	private void reportAuthForError(String model) {
		StringBuilder error = errorHeader();
		error.append("Usermodel was not intended for this application \n");
		error.append(model).append("\n");
		errorReporter.report(error.toString());
	}

	private void reportModelError(String model, Exception e) {
		StringBuilder error = errorHeader();
		error.append(e.getMessage()).append(" \n");
		error.append(model).append("\n");
		errorReporter.report(error.toString());
	}

	private StringBuilder errorHeader() {
		StringBuilder error = new StringBuilder("Lintuvaara Login Failed for application ");
		error.append(systemID).append(" : \n");
		return error;
	}

	private void reportAttempt(Exception e, String key, String iv, String model) {
		StringBuilder error = errorHeader();
		error.append(e.getMessage()).append(" \n");
		error.append("key:  ").append(key).append(" \n");
		error.append("iv:   ").append(iv).append(" \n");
		error.append("data: ").append(model).append(" \n");
		errorReporter.report(error.toString());
	}

	private void failLogin() {
		error("lintuvaara login", "login has failed");
	}

	/**
	 * Usermodel example:
	 * <login type="maallikko">
	 * <login_id>petrus.repo@iki.fi</login_id>
	 * <expires_at>1236252609</expires_at>
	 * <auth_for>Peto</auth_for>
	 * </login>
	 */
	private Map<String, String> parseUserModel(String xml) throws Exception {
		Map<String, String> usermodel = new HashMap<>();
		XMLReader reader = new XMLReader();
		Document document = reader.parse(xml);
		Node rootNode = document.getRootNode();
		String userType = rootNode.getAttribute("type");
		usermodel.put(USER_TYPE, userType);
		for (Node node : rootNode.getChildNodes()) {
			usermodel.put(node.getName(), node.getContents());
		}
		return usermodel;
	}

}
