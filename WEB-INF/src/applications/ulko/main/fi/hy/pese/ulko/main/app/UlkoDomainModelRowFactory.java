package fi.hy.pese.ulko.main.app;

import fi.hy.pese.framework.main.general.data._RowFactory;

public class UlkoDomainModelRowFactory implements _RowFactory<_UlkoDomainModelRow> {

	private final UlkoDatabaseStructure	rowStructure;

	public UlkoDomainModelRowFactory(UlkoDatabaseStructure rowStructure) {
		this.rowStructure = rowStructure;
	}

	@Override
	public String joins() {
		return UlkoConst.JOINS;
	}

	@Override
	public String orderby() {
		return UlkoConst.ORDER_BY;
	}

	@Override
	public _UlkoDomainModelRow newRow() {
		return new UlkoDomainModelRow(rowStructure);
	}

	@Override
	public _UlkoDomainModelRow newRow(String prefix) {
		return new UlkoDomainModelRow(rowStructure, prefix);
	}

}
