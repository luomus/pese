package fi.hy.pese.framework.test.app.functionality;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import fi.hy.pese.framework.main.app.Request;
import fi.hy.pese.framework.main.app.RequestImpleData;
import fi.hy.pese.framework.main.app.functionality.validate.Validator;
import fi.hy.pese.framework.main.app.functionality.validate._Validator;
import fi.hy.pese.framework.main.general.data.Data;
import fi.hy.pese.framework.main.general.data.DatabaseTableStructure;
import fi.hy.pese.framework.main.general.data.PesaDomainModelDatabaseStructure;
import fi.hy.pese.framework.main.general.data.PesaDomainModelRowFactory;
import fi.hy.pese.framework.main.general.data.Table;
import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._Data;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.main.general.data._Row;
import fi.hy.pese.framework.main.general.data._Table;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class ValidatorTest {
	
	public static Test suite() {
		return new TestSuite(ValidatorTest.class.getDeclaredClasses());
	}
	
	public static class CheckAsUpdateparametersTest extends TestCase {
		
		private static final String	DATE_FIELD		= "date_field";
		private static final String	DECIMAL_FIELD	= "decimal_field";
		private static final String	VARCHAR_FIELD	= "varchar_field";
		private static final String	INTEGER_FIELD	= "integer_field";
		private _Data<_PesaDomainModelRow>				data;
		private _Validator			validator;
		
		private class TestValidator extends Validator<_PesaDomainModelRow> {
			public TestValidator(_Data<_PesaDomainModelRow> data) {
				super(new Request<>(new RequestImpleData<_PesaDomainModelRow>().setData(data)));
			}
			
			@Override
			public void validate() throws Exception {
				checkAsUpdateparameters(data.updateparameters());
			}
		}
		
		@Override
		protected void setUp() throws Exception {
			PesaDomainModelDatabaseStructure rowStructuretemplate = new PesaDomainModelDatabaseStructure();
			DatabaseTableStructure laji = new DatabaseTableStructure("laji");
			laji.addColumn(INTEGER_FIELD, _Column.INTEGER, 5);
			laji.addColumn(VARCHAR_FIELD, _Column.VARCHAR, 10);
			laji.addColumn(DECIMAL_FIELD, _Column.DECIMAL, 5, 2);
			laji.addColumn(DATE_FIELD, _Column.DATE);
			laji.get(VARCHAR_FIELD).setToUppercaseColumn();
			laji.get(INTEGER_FIELD).setValueLimits(0, 200);
			rowStructuretemplate.setKaynti(laji);
			data = new Data<>(new PesaDomainModelRowFactory(rowStructuretemplate, "", ""));
			validator = new TestValidator(data);
		}
		
		
		public void test___minmax__validation__1() throws Exception {
			_Column id = data.updateparameters().getKaynti().get(INTEGER_FIELD);
			id.setValue("-600");
			validator.validate();
			assertEquals(_Validator.TOO_SMALL, error(id));
		}
		
		public void ttest___minmax__validation__2() throws Exception {
			_Column id = data.updateparameters().getKaynti().get(INTEGER_FIELD);
			id.setValue("123456");
			validator.validate();
			assertEquals(_Validator.TOO_LARGE, error(id));
		}
		
		
		private String error(_Column c) {
			return validator.errors().get(c.getFullname());
		}
		
		public void test__numeric___it_passes_a_valid_numeric_value() throws Exception {
			data.updateparameters().getKaynti().get(INTEGER_FIELD).setValue("123");
			validator.validate();
			assertFalse("should not have errors", validator.hasErrors());
		}
		
		public void test__numeric___it_passes_empty_numeric_value() throws Exception {
			data.updateparameters().getKaynti().get(INTEGER_FIELD).setValue("");
			validator.validate();
			assertFalse("should not have errors", validator.hasErrors());
		}
		
		public void test__numeric___it_fails_non_numeric_value() throws Exception {
			_Column id = data.updateparameters().getKaynti().get(INTEGER_FIELD);
			id.setValue("a");
			validator.validate();
			assertEquals(_Validator.MUST_BE_INTEGER, error(id));
		}
		
		public void test__numeric___it_fails_non_numeric_value2() throws Exception {
			_Column id = data.updateparameters().getKaynti().get(INTEGER_FIELD);
			id.setValue("%");
			validator.validate();
			assertEquals(_Validator.MUST_BE_INTEGER, error(id));
		}
		
		public void test__numeric___it_fails_if_given_decimal_value() throws Exception {
			_Column id = data.updateparameters().getKaynti().get(INTEGER_FIELD);
			id.setValue("5,2");
			validator.validate();
			assertEquals(_Validator.MUST_BE_INTEGER, error(id));
		}
		
		public void test__numeric___it_fails_if_given_decimal_value2() throws Exception {
			_Column id = data.updateparameters().getKaynti().get(INTEGER_FIELD);
			id.setValue("5.2");
			validator.validate();
			assertEquals(_Validator.MUST_BE_INTEGER, error(id));
		}
		
		//		public void test__numeric___it_fails_if_non_zero_value_starts_with_zero() throws Exception {
		//			_Column id = data.updateparameters().getKaynti().get(INTEGER_FIELD);
		//			id.setValue("02");
		//			validator.validate();
		//			assertEquals(_Validator.INVALID_VALUE, error(id));
		//		}
		
		public void test__numeric___it_takes_zero() throws Exception {
			_Column id = data.updateparameters().getKaynti().get(INTEGER_FIELD);
			id.setValue("0");
			validator.validate();
			assertFalse("should not have errors", validator.hasErrors());
		}
		
		/*
		 * laji.addColumn(DECIMAL_FIELD, _Column.DECIMAL, 5, 2);
		 * number(5,2) --->
		 * 1 : 1,00
		 * 1,2 : 1,20
		 * 1,2222 : 1,22
		 * 333,22 : 333,22
		 * 4444 : too large ! 4+2 == 6 > 5
		 * 4444,22: too large
		 * 1,333 : too large 3 > 2
		 * ...
		 */
		public void test__decimal___it_passes_a_valid_value() throws Exception {
			data.updateparameters().getKaynti().get(DECIMAL_FIELD).setValue("257,52");
			validator.validate();
			assertFalse("should not have errors", validator.hasErrors());
		}
		
		public void test__decimal___it_passes_a_valid_value2() throws Exception {
			data.updateparameters().getKaynti().get(DECIMAL_FIELD).setValue("257.52");
			validator.validate();
			assertFalse("should not have errors", validator.hasErrors());
		}
		
		public void test__decimal___it_passes_a_valid_value3() throws Exception {
			data.updateparameters().getKaynti().get(DECIMAL_FIELD).setValue("257");
			validator.validate();
			assertFalse("should not have errors", validator.hasErrors());
		}
		
		public void test__decimal___it_passes_empty_value() throws Exception {
			data.updateparameters().getKaynti().get(DECIMAL_FIELD).setValue("");
			validator.validate();
			assertFalse("should not have errors", validator.hasErrors());
		}
		
		public void test__decimal___it_doesnt_fail_too_large_scale_because_it_will_be_automaticly_rounded() throws Exception {
			_Column decimalField = data.updateparameters().getKaynti().get(DECIMAL_FIELD);
			decimalField.setValue("1.111");
			validator.validate();
			assertEquals(null, error(decimalField));
			assertEquals("1,11", decimalField.getValue());
		}
		
		public void test__decimal___it_fails_too_long_values2() throws Exception {
			_Column decimalField = data.updateparameters().getKaynti().get(DECIMAL_FIELD);
			decimalField.setValue("1234");
			validator.validate();
			assertEquals(_Validator.TOO_LONG, error(decimalField));
		}
		
		public void test__decimal___it_fails_too_long_values5() throws Exception {
			_Column decimalField = data.updateparameters().getKaynti().get(DECIMAL_FIELD);
			decimalField.setValue("1234,2");
			validator.validate();
			assertEquals(_Validator.TOO_LONG, error(decimalField));
		}
		
		public void test__decimal___it_fails_invalid_values() throws Exception {
			_Column decimalField = data.updateparameters().getKaynti().get(DECIMAL_FIELD);
			decimalField.setValue("a");
			validator.validate();
			assertEquals(_Validator.MUST_BE_DECIMAL, error(decimalField));
		}
		
		public void test__decimal___it_fails_invalid_values2() throws Exception {
			_Column decimalField = data.updateparameters().getKaynti().get(DECIMAL_FIELD);
			decimalField.setValue("2,a");
			validator.validate();
			assertEquals(_Validator.MUST_BE_DECIMAL, error(decimalField));
		}
		
		public void test__decimal___it_fails_invalid_values3() throws Exception {
			_Column decimalField = data.updateparameters().getKaynti().get(DECIMAL_FIELD);
			decimalField.setValue("2..2");
			validator.validate();
			assertEquals(_Validator.MUST_BE_DECIMAL, error(decimalField));
		}
		
		public void test__decimal___it_fails_invalid_values4() throws Exception {
			_Column decimalField = data.updateparameters().getKaynti().get(DECIMAL_FIELD);
			decimalField.setValue(",2");
			assertEquals("0,20", decimalField.getValue());
			validator.validate();
			assertEquals(null, error(decimalField));
		}
		
		public void test__decimal___it_fails_invalid_values5() throws Exception {
			_Column decimalField = data.updateparameters().getKaynti().get(DECIMAL_FIELD);
			decimalField.setValue("2,");
			assertEquals("2,00", decimalField.getValue());
			validator.validate();
			assertEquals(null, error(decimalField));
		}
		
		public void test__varchar___it_passes_a_valid_value() throws Exception {
			data.updateparameters().getKaynti().get(VARCHAR_FIELD).setValue("Asda");
			validator.validate();
			assertFalse("should not have errors", validator.hasErrors());
		}
		
		public void test__varchar___it_passes_empty_value() throws Exception {
			data.updateparameters().getKaynti().get(VARCHAR_FIELD).setValue("");
			validator.validate();
			assertFalse("should not have errors", validator.hasErrors());
		}
		
		public void test__varchar___it_fails_invalid_values() throws Exception {
			_Column f = data.updateparameters().getKaynti().get(VARCHAR_FIELD);
			f.setValue("LONGER THAN MAX VALUE");
			validator.validate();
			assertEquals(_Validator.TOO_LONG, error(f));
		}
		
		public void test__touppercasecolumn___it_fails_special_chars() throws Exception {
			_Column f = data.updateparameters().getKaynti().get(VARCHAR_FIELD);
			f.setValue("!");
			validator.validate();
			assertEquals(_Validator.CAN_NOT_CONTAIN_SPECIAL_CHARS, error(f));
		}
		
		public void test__touppercasecolumn___it_doesnt_fail__this() throws Exception {
			_Column f = data.updateparameters().getKaynti().get(VARCHAR_FIELD);
			f.setValue("åöäÖÅÄ 2");
			validator.validate();
			assertEquals(null, error(f));
		}
		
		public void test__date___it_passes_a_valid_value() throws Exception {
			data.updateparameters().getKaynti().get(DATE_FIELD).setValue("1.1.2009");
			validator.validate();
			assertFalse("should not have errors", validator.hasErrors());
		}
		
		public void test__date___it_passes_a_valid_value2() throws Exception {
			data.updateparameters().getKaynti().get(DATE_FIELD).setValue("30.12.1960");
			validator.validate();
			assertFalse("should not have errors", validator.hasErrors());
		}
		
		public void test__date___it_passes_empty_value() throws Exception {
			data.updateparameters().getKaynti().get(DATE_FIELD).setValue("");
			validator.validate();
			assertFalse("should not have errors", validator.hasErrors());
		}
		
		//		public void test__date___it_passes_null_value() throws Exception {
		//			data.updateparameters().getKaynti().get(DATE_FIELD).setValue(null);
		//			validator.validate();
		//			assertFalse("should not have errors", validator.hasErrors());
		//		}
		
		public void test__date___it_fails_invalid_values() throws Exception {
			_Column date = data.updateparameters().getKaynti().get(DATE_FIELD);
			date.setValue("a");
			validator.validate();
			assertEquals(_Validator.INVALID_DATE, error(date));
		}
		
		public void test__date___it_fails_invalid_values2() throws Exception {
			_Column date = data.updateparameters().getKaynti().get(DATE_FIELD);
			date.setValue(".12.2000");
			validator.validate();
			assertEquals(_Validator.INVALID_DATE, error(date));
		}
		
		public void test__date___it_fails_invalid_values3() throws Exception {
			_Column date = data.updateparameters().getKaynti().get(DATE_FIELD);
			date.setValue("1.12.");
			validator.validate();
			assertEquals(_Validator.INVALID_DATE, error(date));
		}
		
		public void test__date___it_fails_invalid_values4() throws Exception {
			_Column date = data.updateparameters().getKaynti().get(DATE_FIELD);
			date.setValue("1.4.2oo5");
			validator.validate();
			assertEquals(_Validator.INVALID_DATE, error(date));
		}
		
		public void test__date___it_fails_invalid_values5() throws Exception {
			_Column date = data.updateparameters().getKaynti().get(DATE_FIELD);
			date.setValue("0.1.2005");
			validator.validate();
			assertEquals(_Validator.INVALID_DATE, error(date));
		}
		
		public void test__date___it_fails_invalid_values6() throws Exception {
			_Column date = data.updateparameters().getKaynti().get(DATE_FIELD);
			date.setValue("32.1.2005");
			validator.validate();
			assertEquals(_Validator.INVALID_DATE, error(date));
		}
		
		public void test__date___it_fails_invalid_values7() throws Exception {
			_Column date = data.updateparameters().getKaynti().get(DATE_FIELD);
			date.setValue("1.0.2005");
			validator.validate();
			assertEquals(_Validator.INVALID_DATE, error(date));
		}
		
		public void test__date___it_fails_invalid_values8() throws Exception {
			_Column date = data.updateparameters().getKaynti().get(DATE_FIELD);
			date.setValue("1.13.2005");
			validator.validate();
			assertEquals(_Validator.INVALID_DATE, error(date));
		}
		
		public void test__date___it_fails_invalid_values9() throws Exception {
			_Column date = data.updateparameters().getKaynti().get(DATE_FIELD);
			date.setValue("1.1.200");
			validator.validate();
			assertEquals(_Validator.INVALID_DATE, error(date));
		}
		
		public void test__date__it_fails_dates_that_are_in_the_future() throws Exception {
			_Column date = data.updateparameters().getKaynti().get(DATE_FIELD);
			date.setValue("1.1.2049");
			validator.validate();
			assertEquals(_Validator.CAN_NOT_BE_IN_THE_FUTURE, error(date));
		}
		
		public void test__date__it_fails_dates_that_are_in_the_future2() throws Exception {
			_Column date = data.updateparameters().getKaynti().get(DATE_FIELD);
			date.setValue(fi.luomus.commons.utils.DateUtils.getCurrentDateTime("dd.MM.yyyy"));
			validator.validate();
			assertEquals(null, error(date));
		}
		
		public void test__date__it_fails_dates_that_are_in_the_future3() throws Exception {
			_Column date = data.updateparameters().getKaynti().get(DATE_FIELD);
			Calendar k = Calendar.getInstance();
			k.add(Calendar.DATE, 1);
			SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
			date.setValue(sdf.format(k.getTime()));
			validator.validate();
			assertEquals(_Validator.CAN_NOT_BE_IN_THE_FUTURE, error(date));
		}
		
	}
	
	public static class MakeSureCheckAsUpdateparametersValidatesPoikanenFields extends TestCase {
		
		private _Data<_PesaDomainModelRow>		data;
		private _Validator	validator;
		
		private class TestValidator extends Validator<_PesaDomainModelRow> {
			public TestValidator(_Data<_PesaDomainModelRow> data) {
				super(new Request<>(new RequestImpleData<_PesaDomainModelRow>().setData(data)));
			}
			
			@Override
			public void validate() {
				checkAsUpdateparameters(data.updateparameters());
			}
		}
		
		@Override
		protected void setUp() throws Exception {
			PesaDomainModelDatabaseStructure rowStructuretemplate = new PesaDomainModelDatabaseStructure();
			DatabaseTableStructure poikanen = new DatabaseTableStructure("poikanen");
			poikanen.addColumn("poikanen_id", _Column.INTEGER, 5);
			poikanen.addColumn("rengas", _Column.VARCHAR, 8);
			rowStructuretemplate.setPoikanen(poikanen);
			rowStructuretemplate.setPoikasetCount(3);
			data = new Data<>(new PesaDomainModelRowFactory(rowStructuretemplate, "", ""));
			validator = new TestValidator(data);
		}
		
		public void test__it_validates_poikanen_fields() throws Exception {
			data.updateparameters().getPoikanen().get("poikanen_id").setValue("a");
			validator.validate();
			assertEquals(_Validator.MUST_BE_INTEGER, validator.errors().get("poikanen.poikanen_id"));
		}
		
		public void test__it_validates_poikane123_fields() throws Exception {
			data.updateparameters().getPoikaset().get(1).get("poikanen_id").setValue("a");
			data.updateparameters().getPoikaset().get(3).get("poikanen_id").setValue("a");
			validator.validate();
			assertEquals(_Validator.MUST_BE_INTEGER, validator.errors().get("poikanen.1.poikanen_id"));
			assertEquals(_Validator.MUST_BE_INTEGER, validator.errors().get("poikanen.3.poikanen_id"));
		}
		
	}
	
	public static class SearchparameterValidation__Integer extends TestCase {
		
		private _Data<_PesaDomainModelRow>		data;
		private _Validator	validator;
		
		private class TestValidator extends Validator<_PesaDomainModelRow> {
			public TestValidator(_Data<_PesaDomainModelRow> data) {
				super(new Request<>(new RequestImpleData<_PesaDomainModelRow>().setData(data)));
			}
			
			@Override
			public void validate() {
				checkAsSearchparameters(data.searchparameters());
			}
		}
		
		@Override
		protected void setUp() throws Exception {
			PesaDomainModelDatabaseStructure template = new PesaDomainModelDatabaseStructure();
			template.setPesa(new DatabaseTableStructure("pesa"));
			template.getPesa().addColumn("pesaid", _Column.INTEGER, 5);
			template.getPesa().get("pesaid").setValueLimits(0, 10);
			data = new Data<>(new PesaDomainModelRowFactory(template, "", ""));
			validator = new TestValidator(data);
		}
		
		public void test___minmax__validation___minmax_not_checked_for_searchparams() throws Exception {
			_Column id = data.searchparameters().getColumn("pesa.pesaid");
			id.setValue("-600");
			validator.validate();
			assertFalse("should not have errors", validator.hasErrors());
			
		}
		
		public void test__integer__() throws Exception {
			data.searchparameters().getColumn("pesa.pesaid").setValue("5");
			validator.validate();
			assertEquals(null, validator.errors().get("pesa.pesaid"));
		}
		
		public void test__integer__negative() throws Exception {
			data.searchparameters().getColumn("pesa.pesaid").setValue("-5");
			validator.validate();
			assertEquals(null, validator.errors().get("pesa.pesaid"));
		}
		
		public void test__integer__non_numeric() throws Exception {
			data.searchparameters().getColumn("pesa.pesaid").setValue("a");
			validator.validate();
			assertEquals(_Validator.MUST_BE_INTEGER, validator.errors().get("pesa.pesaid"));
		}
		
		public void test__integer__non_numeric2() throws Exception {
			data.searchparameters().getColumn("pesa.pesaid").setValue("-a");
			validator.validate();
			assertEquals(_Validator.MUST_BE_INTEGER, validator.errors().get("pesa.pesaid"));
		}
		
		public void test__integer__valid_range() throws Exception {
			data.searchparameters().getColumn("pesa.pesaid").setValue("0-5");
			validator.validate();
			assertEquals(null, validator.errors().get("pesa.pesaid"));
		}
		
		public void test__integer__valid_range2() throws Exception {
			data.searchparameters().getColumn("pesa.pesaid").setValue("0-0");
			validator.validate();
			assertEquals(null, validator.errors().get("pesa.pesaid"));
		}
		
		public void test__integer__valid_range3() throws Exception {
			data.searchparameters().getColumn("pesa.pesaid").setValue("-1--1");
			validator.validate();
			assertEquals(null, validator.errors().get("pesa.pesaid"));
		}
		
		public void test__integer__valid_range4() throws Exception {
			data.searchparameters().getColumn("pesa.pesaid").setValue("-1-5");
			validator.validate();
			assertEquals(null, validator.errors().get("pesa.pesaid"));
		}
		
		public void test__integer__valid_range5() throws Exception {
			data.searchparameters().getColumn("pesa.pesaid").setValue("-5--1");
			validator.validate();
			assertEquals(null, validator.errors().get("pesa.pesaid"));
		}
		
		public void test__integer__invalid_range() throws Exception {
			data.searchparameters().getColumn("pesa.pesaid").setValue("5-0");
			validator.validate();
			assertEquals(_Validator.INVALID_RANGE, validator.errors().get("pesa.pesaid"));
		}
		
		public void test__integer__invalid_range2() throws Exception {
			data.searchparameters().getColumn("pesa.pesaid").setValue("-");
			validator.validate();
			assertEquals(_Validator.MUST_BE_INTEGER, validator.errors().get("pesa.pesaid"));
		}
		
		public void test__integer__invalid_range3() throws Exception {
			data.searchparameters().getColumn("pesa.pesaid").setValue("5-");
			validator.validate();
			assertEquals(_Validator.INVALID_RANGE, validator.errors().get("pesa.pesaid"));
		}
		
		public void test__integer__invalid_range4() throws Exception {
			data.searchparameters().getColumn("pesa.pesaid").setValue("5-a");
			validator.validate();
			assertEquals(_Validator.MUST_BE_INTEGER, validator.errors().get("pesa.pesaid"));
		}
		
		public void test__integer__invalid_range5() throws Exception {
			data.searchparameters().getColumn("pesa.pesaid").setValue("-5--8");
			validator.validate();
			assertEquals(_Validator.INVALID_RANGE, validator.errors().get("pesa.pesaid"));
		}
		
		public void test__integer__invalid_range6() throws Exception {
			data.searchparameters().getColumn("pesa.pesaid").setValue("5--8");
			validator.validate();
			assertEquals(_Validator.INVALID_RANGE, validator.errors().get("pesa.pesaid"));
		}
		
		public void test__integer__invalid_range7() throws Exception {
			data.searchparameters().getColumn("pesa.pesaid").setValue("--5");
			validator.validate();
			assertEquals(_Validator.MUST_BE_INTEGER, validator.errors().get("pesa.pesaid"));
		}
		
		public void test__integer__invalid_range8() throws Exception {
			data.searchparameters().getColumn("pesa.pesaid").setValue("-5--5-");
			validator.validate();
			assertEquals(_Validator.INVALID_RANGE, validator.errors().get("pesa.pesaid"));
		}
		
		public void test__integer__invalid_range9() throws Exception {
			data.searchparameters().getColumn("pesa.pesaid").setValue("--");
			validator.validate();
			assertEquals(_Validator.MUST_BE_INTEGER, validator.errors().get("pesa.pesaid"));
		}
		
		public void test__integer__invalid_range10() throws Exception {
			data.searchparameters().getColumn("pesa.pesaid").setValue("---");
			validator.validate();
			assertEquals(_Validator.MUST_BE_INTEGER, validator.errors().get("pesa.pesaid"));
		}
		
		public void test__integer__valid_range11() throws Exception {
			data.searchparameters().getColumn("pesa.pesaid").setValue("0 - 5");
			validator.validate();
			assertEquals(null, validator.errors().get("pesa.pesaid"));
		}
		
		public void test__integer__valid_range12() throws Exception {
			data.searchparameters().getColumn("pesa.pesaid").setValue("  0- 0");
			validator.validate();
			assertEquals(null, validator.errors().get("pesa.pesaid"));
		}
		
		public void test__integer__valid_range13() throws Exception {
			data.searchparameters().getColumn("pesa.pesaid").setValue("- 1 - -1");
			validator.validate();
			assertEquals(null, validator.errors().get("pesa.pesaid"));
		}
		
		public void test__integer__valid_range14() throws Exception {
			data.searchparameters().getColumn("pesa.pesaid").setValue("-1 - 5");
			validator.validate();
			assertEquals(null, validator.errors().get("pesa.pesaid"));
		}
		
		public void test__integer__valid_range15() throws Exception {
			data.searchparameters().getColumn("pesa.pesaid").setValue(" -5-- 1");
			validator.validate();
			assertEquals(null, validator.errors().get("pesa.pesaid"));
		}
		
	}
	
	public static class SearchparameterValidation__Decimal extends TestCase {
		
		private _Data<_PesaDomainModelRow>		data;
		private _Validator	validator;
		
		private class TestValidator extends Validator<_PesaDomainModelRow> {
			public TestValidator(_Data<_PesaDomainModelRow> data) {
				super(new Request<>(new RequestImpleData<_PesaDomainModelRow>().setData(data)));
			}
			
			@Override
			public void validate() {
				checkAsSearchparameters(data.searchparameters());
			}
		}
		
		@Override
		protected void setUp() throws Exception {
			PesaDomainModelDatabaseStructure template = new PesaDomainModelDatabaseStructure();
			template.setPesa(new DatabaseTableStructure("pesa"));
			template.getPesa().addColumn("pesaid", _Column.DECIMAL, 5, 2);
			data = new Data<>(new PesaDomainModelRowFactory(template, "", ""));
			validator = new TestValidator(data);
		}
		
		public void test__decimal__() throws Exception {
			data.searchparameters().getColumn("pesa.pesaid").setValue("5,6");
			validator.validate();
			assertEquals(null, validator.errors().get("pesa.pesaid"));
		}
		
		public void test__decimal__negative() throws Exception {
			data.searchparameters().getColumn("pesa.pesaid").setValue("-5,44");
			validator.validate();
			assertEquals(null, validator.errors().get("pesa.pesaid"));
		}
		
		public void test__decimal__non_numeric() throws Exception {
			data.searchparameters().getColumn("pesa.pesaid").setValue("a");
			validator.validate();
			assertEquals(_Validator.MUST_BE_DECIMAL, validator.errors().get("pesa.pesaid"));
		}
		
		public void test__decimal__non_numeric2() throws Exception {
			data.searchparameters().getColumn("pesa.pesaid").setValue("-a.6");
			validator.validate();
			assertEquals(_Validator.MUST_BE_DECIMAL, validator.errors().get("pesa.pesaid"));
		}
		
		public void test__decimal__valid_range() throws Exception {
			data.searchparameters().getColumn("pesa.pesaid").setValue("0,0-5,6");
			validator.validate();
			assertEquals(null, validator.errors().get("pesa.pesaid"));
		}
		
		public void test__decimal__valid_range2() throws Exception {
			data.searchparameters().getColumn("pesa.pesaid").setValue("0-0");
			validator.validate();
			assertEquals(null, validator.errors().get("pesa.pesaid"));
		}
		
		public void test__decimal__valid_range3() throws Exception {
			data.searchparameters().getColumn("pesa.pesaid").setValue("-1,0--1,0");
			validator.validate();
			assertEquals(null, validator.errors().get("pesa.pesaid"));
		}
		
		public void test__decimal__valid_range4() throws Exception {
			data.searchparameters().getColumn("pesa.pesaid").setValue("-531,43-5");
			validator.validate();
			assertEquals(null, validator.errors().get("pesa.pesaid"));
		}
		
		public void test__decimal__valid_range5() throws Exception {
			data.searchparameters().getColumn("pesa.pesaid").setValue("-5345,7--601.4");
			validator.validate();
			assertEquals(null, validator.errors().get("pesa.pesaid"));
		}
		
		public void test__decimal__invalid_range() throws Exception {
			data.searchparameters().getColumn("pesa.pesaid").setValue("5,6-0,6");
			validator.validate();
			assertEquals(_Validator.INVALID_RANGE, validator.errors().get("pesa.pesaid"));
		}
		
		public void test__decimal__invalid_range2() throws Exception {
			data.searchparameters().getColumn("pesa.pesaid").setValue("-");
			validator.validate();
			assertEquals(_Validator.MUST_BE_DECIMAL, validator.errors().get("pesa.pesaid"));
		}
		
		public void test__decimal__invalid_range3() throws Exception {
			data.searchparameters().getColumn("pesa.pesaid").setValue("5,64-");
			validator.validate();
			assertEquals(_Validator.INVALID_RANGE, validator.errors().get("pesa.pesaid"));
		}
		
		public void test__decimal__invalid_range4() throws Exception {
			data.searchparameters().getColumn("pesa.pesaid").setValue("5,4-a");
			validator.validate();
			assertEquals(_Validator.MUST_BE_DECIMAL, validator.errors().get("pesa.pesaid"));
		}
		
		public void test__decimal__invalid_range5() throws Exception {
			data.searchparameters().getColumn("pesa.pesaid").setValue("-5,44--8.4");
			validator.validate();
			assertEquals(_Validator.INVALID_RANGE, validator.errors().get("pesa.pesaid"));
		}
		
		public void test__decimal__invalid_range6() throws Exception {
			data.searchparameters().getColumn("pesa.pesaid").setValue("5,5--8,5");
			validator.validate();
			assertEquals(_Validator.INVALID_RANGE, validator.errors().get("pesa.pesaid"));
		}
		
		public void test__decimal__invalid_range7() throws Exception {
			data.searchparameters().getColumn("pesa.pesaid").setValue("--5.00");
			validator.validate();
			assertEquals(_Validator.MUST_BE_DECIMAL, validator.errors().get("pesa.pesaid"));
		}
		
		public void test__decimal__invalid_range8() throws Exception {
			data.searchparameters().getColumn("pesa.pesaid").setValue("-5.1--5-");
			validator.validate();
			assertEquals(_Validator.INVALID_RANGE, validator.errors().get("pesa.pesaid"));
		}
		
		public void test__decimal__invalid_range9() throws Exception {
			data.searchparameters().getColumn("pesa.pesaid").setValue("--");
			validator.validate();
			assertEquals(_Validator.MUST_BE_DECIMAL, validator.errors().get("pesa.pesaid"));
		}
		
		public void test__decimal__invalid_range10() throws Exception {
			data.searchparameters().getColumn("pesa.pesaid").setValue("---");
			validator.validate();
			assertEquals(_Validator.MUST_BE_DECIMAL, validator.errors().get("pesa.pesaid"));
		}
		
		public void test__decimal__invalid_range11() throws Exception {
			data.searchparameters().getColumn("pesa.pesaid").setValue(",-,-");
			validator.validate();
			assertEquals(_Validator.INVALID_RANGE, validator.errors().get("pesa.pesaid"));
		}
		
		public void test__decimal__invalid_range12() throws Exception {
			data.searchparameters().getColumn("pesa.pesaid").setValue("--,-4,");
			validator.validate();
			assertEquals(_Validator.MUST_BE_DECIMAL, validator.errors().get("pesa.pesaid"));
		}
	}
	
	public static class SearchparameterValidation__ValueList extends TestCase {
		
		private _Data<_PesaDomainModelRow>		data;
		private _Validator	validator;
		
		private class TestValidator extends Validator<_PesaDomainModelRow> {
			public TestValidator(_Data<_PesaDomainModelRow> data) {
				super(new Request<>(new RequestImpleData<_PesaDomainModelRow>().setData(data)));
			}
			
			@Override
			public void validate() {
				checkAsSearchparameters(data.searchparameters());
			}
		}
		
		@Override
		protected void setUp() throws Exception {
			PesaDomainModelDatabaseStructure template = new PesaDomainModelDatabaseStructure();
			template.setPesa(new DatabaseTableStructure("pesa"));
			template.getPesa().addColumn("pesaid", _Column.INTEGER, 5);
			template.getPesa().addColumn("des_leveys", _Column.DECIMAL, 5, 2);
			template.getPesa().addColumn("pesanimi", _Column.VARCHAR, 5);
			data = new Data<>(new PesaDomainModelRowFactory(template, "", ""));
			validator = new TestValidator(data);
		}
		
		public void test__doesnt_accept_equals_sign() throws Exception {
			data.searchparameters().getColumn("pesa.pesanimi").setValue("a=5");
			validator.validate();
			assertEquals(_Validator.CAN_NOT_CONTAIN_EQUALS_SIGN, validator.errors().get("pesa.pesanimi"));
		}
		
		public void test__valid() throws Exception {
			data.searchparameters().getColumn("pesa.pesaid").setValue("1");
			validator.validate();
			assertEquals(null, validator.errors().get("pesa.pesaid"));
		}
		
		public void test__valid_list() throws Exception {
			data.searchparameters().getColumn("pesa.pesaid").setValue("1|2|6");
			validator.validate();
			assertEquals(null, validator.errors().get("pesa.pesaid"));
		}
		
		public void test__invalid_list() throws Exception {
			data.searchparameters().getColumn("pesa.pesaid").setValue("1|2|");
			validator.validate();
			assertEquals(_Validator.INVALID_VALUE_LIST, validator.errors().get("pesa.pesaid"));
		}
		
		public void test__invalid_list2() throws Exception {
			data.searchparameters().getColumn("pesa.pesaid").setValue("|1|2");
			validator.validate();
			assertEquals(_Validator.INVALID_VALUE_LIST, validator.errors().get("pesa.pesaid"));
		}
		
		public void test__invalid_list3() throws Exception {
			data.searchparameters().getColumn("pesa.pesaid").setValue("|1|2|");
			validator.validate();
			assertEquals(_Validator.INVALID_VALUE_LIST, validator.errors().get("pesa.pesaid"));
		}
		
		public void test__invalid_list4() throws Exception {
			data.searchparameters().getColumn("pesa.pesaid").setValue("1||2");
			validator.validate();
			assertEquals(_Validator.INVALID_VALUE_LIST, validator.errors().get("pesa.pesaid"));
		}
		
		public void test__invalid_list5() throws Exception {
			data.searchparameters().getColumn("pesa.pesaid").setValue("1|||2");
			validator.validate();
			assertEquals(_Validator.INVALID_VALUE_LIST, validator.errors().get("pesa.pesaid"));
		}
		
		public void test__validates_types__integer() throws Exception {
			data.searchparameters().getColumn("pesa.pesaid").setValue("1|a|2");
			validator.validate();
			assertEquals(_Validator.MUST_BE_INTEGER, validator.errors().get("pesa.pesaid"));
		}
		
		public void test__validates_types__decimal_ok() throws Exception {
			data.searchparameters().getColumn("pesa.des_leveys").setValue("13,2|5.0|2.63");
			validator.validate();
			assertEquals(null, validator.errors().get("pesa.des_leveys"));
		}
		
		public void test__validates_types__decimal() throws Exception {
			data.searchparameters().getColumn("pesa.des_leveys").setValue("13,2|a|2.63");
			validator.validate();
			assertEquals(_Validator.MUST_BE_DECIMAL, validator.errors().get("pesa.des_leveys"));
		}
		
		public void test__validates_types__varchar_ok() throws Exception {
			data.searchparameters().getColumn("pesa.pesanimi").setValue("1|a|2asd---------------!");
			validator.validate();
			assertEquals(null, validator.errors().get("pesa.pesanimi"));
		}
		
		public void test___not_like__search() throws Exception {
			data.searchparameters().getColumn("pesa.pesanimi").setValue("!A");
			data.searchparameters().getColumn("pesa.pesaid").setValue("!5");
			data.searchparameters().getColumn("pesa.des_leveys").setValue("!1,5");
			validator.validate();
			assertEquals(null, validator.errors().get("pesa.pesanimi"));
			assertEquals(null, validator.errors().get("pesa.pesaid"));
			assertEquals(null, validator.errors().get("pesa.des_leveys"));
		}
		
	}
	
	public static class SearchparameterValidation__Date extends TestCase {
		
		private _Data<_PesaDomainModelRow>		data;
		private _Validator	validator;
		
		private class TestValidator extends Validator<_PesaDomainModelRow> {
			public TestValidator(_Data<_PesaDomainModelRow> data) {
				super(new Request<>(new RequestImpleData<_PesaDomainModelRow>().setData(data)));
			}
			
			@Override
			public void validate() {
				checkAsSearchparameters(data.searchparameters());
			}
		}
		
		@Override
		protected void setUp() throws Exception {
			PesaDomainModelDatabaseStructure template = new PesaDomainModelDatabaseStructure();
			template.setPesa(new DatabaseTableStructure("pesa"));
			template.getPesa().addColumn("loytopvm", _Column.DATE);
			data = new Data<>(new PesaDomainModelRowFactory(template, "", ""));
			validator = new TestValidator(data);
		}
		
		public void test__valid() throws Exception {
			data.searchparameters().getColumn("pesa.loytopvm").setValue("1.1.2005");
			validator.validate();
			assertEquals(null, validator.errors().get("pesa.loytopvm"));
		}
		
		public void test__valid2() throws Exception {
			data.searchparameters().getColumn("pesa.loytopvm").setValue("..2005");
			validator.validate();
			assertEquals(null, validator.errors().get("pesa.loytopvm"));
		}
		
		public void test__valid3() throws Exception {
			data.searchparameters().getColumn("pesa.loytopvm").setValue("..2005-2007");
			validator.validate();
			assertEquals(null, validator.errors().get("pesa.loytopvm"));
		}
		
		public void test__valid4() throws Exception {
			data.searchparameters().getColumn("pesa.loytopvm").setValue(".. 2005  - 2007");
			validator.validate();
			assertEquals(null, validator.errors().get("pesa.loytopvm"));
		}
		
		public void test__valid5() throws Exception {
			data.searchparameters().getColumn("pesa.loytopvm").setValue(".8. 2005");
			validator.validate();
			assertEquals(null, validator.errors().get("pesa.loytopvm"));
		}
		
		public void test__invalid() throws Exception {
			data.searchparameters().getColumn("pesa.loytopvm").setValue("1.4.2005-2007");
			validator.validate();
			assertEquals(_Validator.INVALID_DATE_RANGE, validator.errors().get("pesa.loytopvm"));
		}
		
		public void test__invalid1() throws Exception {
			data.searchparameters().getColumn("pesa.loytopvm").setValue("..2008-2007");
			validator.validate();
			assertEquals(_Validator.INVALID_DATE_RANGE, validator.errors().get("pesa.loytopvm"));
		}
		
		public void test__invalid2() throws Exception {
			data.searchparameters().getColumn("pesa.loytopvm").setValue("1.4.");
			validator.validate();
			assertEquals(_Validator.INVALID_DATE, validator.errors().get("pesa.loytopvm"));
		}
		
		public void test__invalid3() throws Exception {
			data.searchparameters().getColumn("pesa.loytopvm").setValue("a");
			validator.validate();
			assertEquals(_Validator.INVALID_DATE, validator.errors().get("pesa.loytopvm"));
		}
		
		public void test__invalid5() throws Exception {
			data.searchparameters().getColumn("pesa.loytopvm").setValue("1..2007");
			validator.validate();
			assertEquals(_Validator.INVALID_DATE, validator.errors().get("pesa.loytopvm"));
		}
		
		public void test__invalid6() throws Exception {
			data.searchparameters().getColumn("pesa.loytopvm").setValue("1..2007-2009");
			validator.validate();
			assertEquals(_Validator.INVALID_DATE, validator.errors().get("pesa.loytopvm"));
		}
		
		public void test__invalid7() throws Exception {
			data.searchparameters().getColumn("pesa.loytopvm").setValue("..2000|2008|2010");
			validator.validate();
			assertEquals(_Validator.CAN_NOT_CONTAIN_SEPARATOR, validator.errors().get("pesa.loytopvm"));
		}
		
		public void test__invalid8() throws Exception {
			data.searchparameters().getColumn("pesa.loytopvm").setValue("1-5.8.2000");
			validator.validate();
			assertEquals(_Validator.INVALID_DATE, validator.errors().get("pesa.loytopvm"));
		}
		
		public void test__invalid9() throws Exception {
			data.searchparameters().getColumn("pesa.loytopvm").setValue(".8-9.2000");
			validator.validate();
			assertEquals(_Validator.INVALID_DATE, validator.errors().get("pesa.loytopvm"));
		}
		
		public void test__invalid10() throws Exception {
			data.searchparameters().getColumn("pesa.loytopvm").setValue("1-5.8-10.2000-2005");
			validator.validate();
			assertEquals(_Validator.INVALID_DATE, validator.errors().get("pesa.loytopvm"));
		}
		
		public void test__invalid11() throws Exception {
			data.searchparameters().getColumn("pesa.loytopvm").setValue(".8a. 2005");
			validator.validate();
			assertEquals(_Validator.INVALID_DATE, validator.errors().get("pesa.loytopvm"));
		}
		
		public void test__invalid12() throws Exception {
			data.searchparameters().getColumn("pesa.loytopvm").setValue(".8. 2o05");
			validator.validate();
			assertEquals(_Validator.INVALID_DATE, validator.errors().get("pesa.loytopvm"));
		}
		
		public void test__invalid13() throws Exception {
			data.searchparameters().getColumn("pesa.loytopvm").setValue("..-200-2005");
			validator.validate();
			assertEquals(_Validator.INVALID_DATE_RANGE, validator.errors().get("pesa.loytopvm"));
		}
		
		public void test__invalid14() throws Exception {
			data.searchparameters().getColumn("pesa.loytopvm").setValue("..-2005");
			validator.validate();
			assertEquals(_Validator.INVALID_DATE_RANGE, validator.errors().get("pesa.loytopvm"));
		}
		
		public void test__invalid15() throws Exception {
			data.searchparameters().getColumn("pesa.loytopvm").setValue("..A-2005");
			validator.validate();
			assertEquals(_Validator.INVALID_DATE, validator.errors().get("pesa.loytopvm"));
		}
		
		public void test__valid_empty() throws Exception {
			data.searchparameters().getColumn("pesa.loytopvm").setValue("");
			validator.validate();
			assertEquals(null, validator.errors().get("pesa.loytopvm"));
		}
		
		public void test__valid_empty2() throws Exception {
			data.searchparameters().getColumn("pesa.loytopvm").setValue("..");
			validator.validate();
			assertEquals(null, validator.errors().get("pesa.loytopvm"));
		}
	}
	
	public static class ValidationInternals extends TestCase {
		
		private final _Table mytable = new Table(new DatabaseTableStructure("mytable"), 1);
		private final _Table otherNonIndexedTable = new Table(new DatabaseTableStructure("othertable"));
		
		private TestValidator validator;
		
		@Override
		public void setUp() {
			validator = new TestValidator();
		}
		
		@SuppressWarnings("deprecation")
		private class TestValidator extends Validator<_Row> {
			@Override
			public void validate() throws Exception {}
			@Override
			public boolean hasErrors(_Table table) {
				return super.hasErrors(table);
			}
			public void setError(String name) {
				super.error(name, "error");
			}
		}
		
		public void test__table_hasErrors_check_util_function() {
			assertFalse(validator.hasErrors(mytable));
		}
		
		public void test__table_hasErrors_check_util_function_2() {
			validator.setError("mytable.1.jotain");
			assertTrue(validator.hasErrors(mytable));
		}
		
		public void test__table_hasErrors_check_util_function_3() {
			validator.setError("mytable.2.jotain");
			assertFalse(validator.hasErrors(mytable));
		}
		
		public void test__table_hasErrors_check_util_function_4() {
			validator.setError("mytable");
			assertFalse(validator.hasErrors(mytable));
		}
		
		public void test__table_hasErrors_check_util_function_5() {
			validator.setError("othertable.1.jotain");
			assertFalse(validator.hasErrors(otherNonIndexedTable));
		}
		
		public void test__table_hasErrors_check_util_function_6() {
			validator.setError("othertable.jotein");
			assertTrue(validator.hasErrors(otherNonIndexedTable));
		}
		
	}
	
}