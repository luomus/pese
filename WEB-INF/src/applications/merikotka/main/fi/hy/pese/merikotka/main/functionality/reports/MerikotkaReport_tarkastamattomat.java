package fi.hy.pese.merikotka.main.functionality.reports;

import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.app.functionality.SearchFunctionality;
import fi.hy.pese.framework.main.db._ResultSet;
import fi.hy.pese.framework.main.db._ResultsHandler;
import fi.hy.pese.framework.main.general.ReportWriter;
import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.merikotka.main.app.MerikotkaConst;
import fi.luomus.commons.containers.Selection;
import fi.luomus.commons.tipuapi.TipuAPIClient;
import fi.luomus.commons.xml.Document.Node;

/**
 * Haetaan kaikki pesät jotka vastaa hakuparametreja (poislukien tarkastusvuodet)
 * Jokaiselle hakuvuodelle
 * -- tee lista yllälevista pesistä jotka ei ole tarkastettu ko. vuonna
 * Tulosta tulokset suuralueiden ja vuosien mukaan erotellen
 * Esim: suuralue A: vuosi 2008: vuosi 2007: vuosi 2005:
 * suuralue E: vuosi 2008: vuosi 2007: vuosi 2005:
 * jossa listataan tarkastamattomista pesistä pesän nykyinen
 * pesa|pesanimi|reviiri_id|reviirinimi|kunta_tunnus|ymp_keskus|suur_alue|viimeisin tark_vuosi
 * sekä lopuksi yhteenveto tarkastamatta kpl|yht kpl|% tarkastamatta|
 */
public class MerikotkaReport_tarkastamattomat extends ReportWriter<_PesaDomainModelRow> {

	private final SearchFunctionality	searchFunctionality;

	public MerikotkaReport_tarkastamattomat(_Request<_PesaDomainModelRow> request, String filenamePrefix, SearchFunctionality searchFunctionality) {
		super(request, filenamePrefix);
		this.searchFunctionality = searchFunctionality;
	}

	List<Integer>						years;
	Set<String>							allNests;
	Map<Integer, Set<String>>			inspectedNestsByYear;
	Map<String, Map<String, String>>	nestInfo;

	@Override
	public boolean produceSpecific() throws Exception {
		years = yearsToPrint(request.data().searchparameters().getTarkastus().get("tark_vuosi"));
		if (years.isEmpty()) {
			request.data().errors().put(filenamePrefix, MerikotkaConst.INVALID_REPORT_YEARS);
			return false;
		}

		NestListGeneratingResultHandler handler = allNestsMatchingSearchparameters();
		allNests = handler.nests();
		nestInfo = handler.nestInfo();
		inspectedNestsByYear = inspectedNestsByYear();

		headerLine();
		newLine();
		newLine();

		for (Integer year : years) {
			Set<String> notInspected = notInspectedNestsForYear(year, allNests, inspectedNestsByYear);

			write(year + ": ");
			newLine();
			newLine();

			if (!notInspected.isEmpty()) {
				writeColumnDescriptions();
				newLine();
				for (String pesa_id : notInspected) {
					Map<String, String> info = nestInfo.get(pesa_id);
					writePesaRow(pesa_id, info);
					newLine();
				}
			}

			newLine();
			summary(allNests, notInspected);
			newLine();
			newLine();
		}
		return true;
	}

	private void summary(Set<String> allNests, Set<String> notInspected) {
		write("kpl|yht kpl|%|");
		newLine();
		write(Integer.toString(notInspected.size()));
		separator();
		write(Integer.toString(allNests.size()));
		separator();
		write(Double.toString(Double.valueOf(notInspected.size()) / Double.valueOf(allNests.size()) * 100));
	}

	private void writePesaRow(String pesa_id, Map<String, String> info) {
		write(pesa_id);
		separator();
		write(info.get("pesanimi"));
		separator();
		write(info.get("reviiri_id"));
		separator();
		write(info.get("reviirinimi"));
		separator();
		write(info.get("kunta_tunnus"));
		separator();
		write(info.get("ymp_keskus"));
		separator();
		write(info.get("suur_alue"));
		separator();
		write(info.get("tark_vuosi"));
		separator();
	}

	private void writeColumnDescriptions() {
		write(shortNameFromUiTexts("pesavakio.pesa_id"));
		separator();
		write(shortNameFromUiTexts("pesavakio.pesanimi"));
		separator();
		write(shortNameFromUiTexts("reviiri.reviiri_id"));
		separator();
		write(shortNameFromUiTexts("reviiri.reviirinimi"));
		separator();
		write(shortNameFromUiTexts("kunta.kunta_tunnus"));
		separator();
		write(shortNameFromUiTexts("kunta.ymp_keskus"));
		separator();
		write(shortNameFromUiTexts("kunta.suur_alue"));
		separator();
		write("viimeisin tarkastusvuosi");
		separator();
	}

	private Set<String> notInspectedNestsForYear(Integer year, Set<String> allNests, Map<Integer, Set<String>> inspectedNestsByYear) {
		Set<String> copyOfAllNests = new LinkedHashSet<>(allNests);
		if (inspectedNestsByYear.containsKey(year)) {
			Set<String> inspectedNests = inspectedNestsByYear.get(year);
			copyOfAllNests.removeAll(inspectedNests);
		}
		return copyOfAllNests;
	}

	private Map<Integer, Set<String>> inspectedNestsByYear() throws SQLException {
		String tarkastuksetQuery = "select pesa_id, tark_vuosi from pesatarkastus order by tark_vuosi, pesa_id";
		NestListGeneratingResultHandler handler = new NestListGeneratingResultHandler();
		handler.setNoInfos(true);
		request.dao().executeSearch(tarkastuksetQuery, handler);
		return handler.nestsByYear();
	}

	private NestListGeneratingResultHandler allNestsMatchingSearchparameters() throws Exception {
		_Column tark_vuosi = request.data().searchparameters().getTarkastus().get("tark_vuosi");
		String yearParameter = clearAndReturnValue(tark_vuosi);
		NestListGeneratingResultHandler handler = new NestListGeneratingResultHandler();
		searchFunctionality.search(handler);
		tark_vuosi.setValue(yearParameter);
		return handler;
	}

	private String clearAndReturnValue(_Column tark_vuosi) {
		String yearParameter = tark_vuosi.getValue();
		tark_vuosi.setValue("");
		return yearParameter;
	}

	private class NestListGeneratingResultHandler implements _ResultsHandler {
		private final Set<String>						nests		= new LinkedHashSet<>();
		private final Map<Integer, Set<String>>			nestByYear	= new LinkedHashMap<>();
		private final Map<String, Map<String, String>>	nestInfo	= new LinkedHashMap<>();
		private boolean									noInfos		= false;

		public void setNoInfos(boolean value) {
			noInfos = value;
		}

		@Override
		public void process(_ResultSet rs) throws SQLException {
			while (rs.next()) {
				String pesa_id = rs.getString("pesa_id");
				Integer year = Integer.valueOf(rs.getString("tark_vuosi"));

				nests.add(pesa_id);

				if (!nestByYear.containsKey(year)) {
					nestByYear.put(year, new LinkedHashSet<String>());
				}
				nestByYear.get(year).add(pesa_id);

				if (noInfos) continue;
				if (!nestInfo.containsKey(pesa_id)) {
					Map<String, String> info = new LinkedHashMap<>();
					info.put("pesa_id", pesa_id);
					info.put("pesanimi", rs.getString("pesanimi"));
					info.put("reviiri_id", rs.getString("reviiri_id"));
					info.put("reviirinimi", rs.getString("reviirinimi"));
					String kuntaLyh = rs.getString("reviirin_kunta");
					Node kuntaNode = null;
					try {
						kuntaNode = request.getTipuApiResource(TipuAPIClient.MUNICIPALITIES).getById(kuntaLyh);
					} catch (Exception e) {
						throw new RuntimeException("Kunta " +kuntaLyh+ " not found as Tipu-API resource.");
					}
					Selection ympkeskukset = request.data().selections().get(TipuAPIClient.ELY_CENTRES);
					Selection suuralueet = request.data().selections().get(MerikotkaConst.SUURALUEET);
					info.put("kunta_tunnus", kuntaNode.getNode("name").getContents());
					info.put("ymp_keskus", ympkeskukset.get(kuntaNode.getNode("ely-centre").getContents()));
					info.put("suur_alue", suuralueet.get(kuntaNode.getNode("merikotka-suuralue").getContents()));
					info.put("tark_vuosi", rs.getString("tark_vuosi"));
					nestInfo.put(pesa_id, info);
				}
			}
		}

		public Set<String> nests() {
			return nests;
		}

		public Map<Integer, Set<String>> nestsByYear() {
			return nestByYear;
		}

		public Map<String, Map<String, String>> nestInfo() {
			return nestInfo;
		}

	}

}
