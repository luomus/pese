package fi.hy.pese.framework.main.app;

import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.Collection;

import fi.hy.pese.framework.main.db._DAO;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data.ParameterMap;
import fi.hy.pese.framework.main.general.data._Data;
import fi.hy.pese.framework.main.general.data._Row;
import fi.hy.pese.framework.main.view._Viewer;
import fi.luomus.commons.config.Config;
import fi.luomus.commons.http.HttpStatus;
import fi.luomus.commons.session.SessionHandler;
import fi.luomus.commons.utils.DateUtils;

/**
 * Controller of the PeSe framework application. Processes a request by the server.
 * Inits data, selections, localization texts, etc, and calls the functionalities
 * as defined in the Factory that is given by the Servlet.
 */
public class Control<T extends _Row> {

	private final _ApplicationSpecificationFactory<T>	application;
	private final Config							config;
	private final _Viewer							viewer;
	private final Collection<String>				supportedLanguages;
	private final String							startedAtTimestamp;

	public Control(_ApplicationSpecificationFactory<T> applicationSpecificationFactory) throws Exception {
		this.application = applicationSpecificationFactory;
		this.config = applicationSpecificationFactory.config();
		this.viewer = applicationSpecificationFactory.viewer();
		this.supportedLanguages = config.supportedLanguages();
		startedAtTimestamp = currentTimestamp();
	}

	/**
	 * Processes a request
	 * @param params
	 * @param session
	 * @param out
	 * @param redirecter
	 * @param originalRequestURI
	 */
	public void process(ParameterMap params, SessionHandler session, PrintWriter out, HttpStatus redirecter, String originalRequestURI) {
		_DAO<T> dao = null;
		_Request<T> request = null;
		_Data<T> data = null;
		try {
			String language = getAndSetLanguage(params, session);
			data = initializeData(language, session, params);
			String page = data.page();

			if (!session.isAuthenticatedFor(config.systemId())) {
				page = Const.LOGIN_PAGE;
			} else {
				if (lintuvaaraLoginParamsGiven(data)) {
					redirecter.redirectTo(config.baseURL());
				}
				if (isNull(page) || page.equals(Const.LOGIN_PAGE)) {
					page = application.frontpage();
				}
			}
			data.setPage(page);
			data.setLanguage(language);

			dao = application.dao(session.userId());
			dao.startTransaction();

			data.setSelections(application.selections(dao));
			String flash = session.getFlash();
			if (flash == null) flash = "";
			data.setSuccessText(flash); // TODO erityyppisiä flash messageja..

			request = initRequest(session, out, redirecter, dao, data, originalRequestURI);
			_Functionality<T> functionality = application.functionalityFor(page, request);
			FunctionalityExecutor.execute(functionality);

			application.postProsessingHook(request);

			dao.commit();

			if (hasNotBeenRedirected(redirecter) && !data.page().equals(Const.OUTPUT_ALREADY_PRINTED_TO_REQUEST_OUT_PAGE)) {
				data.convertValuesToHTMLEntities();
				viewer.viewFor(request);
			}

		} catch (Throwable condition) {
			if (dao != null) try {
				dao.rollback();
			} catch (SQLException sqle) {
				application.reportException("Trying to do rollback", sqle);
			}

			if (hasNotBeenRedirected(redirecter)) {
				application.exceptionViewer(request).view(condition);
			}
			application.reportException(data == null ? "null data" : data.getInputParametersForDebugging(), condition);

		} finally {

			if (dao != null) application.release(dao);

		}
	}
	
	private boolean lintuvaaraLoginParamsGiven(_Data<T> data) {
		return data.contains("key") && data.contains("iv") && data.contains("data");
	}
	
	private _Request<T> initRequest(SessionHandler session, PrintWriter out, HttpStatus redirecter, _DAO<T> dao, _Data<T> data, String originalRequestURI) {
		RequestImpleData<T> requestData = new RequestImpleData<>();
		requestData.setData(data);
		requestData.setDao(dao);
		requestData.setSessionHandler(session);
		requestData.setRedirecter(redirecter);
		requestData.setApplication(application);
		requestData.setOut(out);
		requestData.setOriginalRequestURI(originalRequestURI);
		return new Request<>(requestData);
	}

	private boolean hasNotBeenRedirected(HttpStatus redirecter) {
		return !redirecter.isHttpStatusCodeSet();
	}

	private boolean isNull(String value) {
		return value == null || value.length() < 1;
	}

	private String getAndSetLanguage(ParameterMap params, SessionHandler session) {
		String language = params.getAndRemove(Const.LANGUAGE);
		if (notDefined(language)) {
			language = session.get(Const.LANGUAGE);
			if (notDefined(language)) language = config.defaultLanguage();
		} else {
			if (!supportedLanguages.contains(language)) language = config.defaultLanguage();
			session.put(Const.LANGUAGE, language);
		}
		return language;
	}

	private boolean notDefined(String language) {
		return language == null || language.equals("");
	}

	private _Data<T> initializeData(String language, SessionHandler session, ParameterMap params) throws Exception {
		_Data<T> data = application.initData(language, params);
		data.setUserId(session.userId());
		data.setUserName(session.userName());
		data.setUserType(session.userType());
		if (config.developmentMode()) {
			data.setLastUpdateTimestamp(currentTimestamp());
		} else {
			data.setLastUpdateTimestamp(startedAtTimestamp);
		}
		return data;
	}
	
	private String currentTimestamp() {
		return Long.toString(DateUtils.getCurrentEpoch());
	}

}
