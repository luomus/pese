package fi.hy.pese.ulko.main.db;

import java.sql.SQLException;

import fi.hy.pese.framework.main.db.BaseResultsHandler;
import fi.hy.pese.framework.main.db._ResultSet;
import fi.hy.pese.framework.main.general.data._Data;
import fi.hy.pese.ulko.main.app._UlkoDomainModelRow;

public class UlkoRowsToDataResultsHandler extends BaseResultsHandler {
	
	private final _Data<_UlkoDomainModelRow> data;
	
	public UlkoRowsToDataResultsHandler(_Data<_UlkoDomainModelRow> data) {
		this.data = data;
	}
	
	@Override
	public void process(_ResultSet rs) throws SQLException {
		while (rs.next()) {
			_UlkoDomainModelRow row = data.newResultRow();
			int i = 1;
			i = setResultsToTable(rs, row.getPyynto(), i);
			i = setResultsToTable(rs, row.getLoyto(), i);
			i = setResultsToTable(rs, row.getRengastus(), i);
		}
	}
	
}
