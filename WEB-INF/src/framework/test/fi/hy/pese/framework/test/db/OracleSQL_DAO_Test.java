package fi.hy.pese.framework.test.db;

import fi.hy.pese.framework.main.db._DAO;
import fi.hy.pese.framework.main.db.oraclesql.OracleSQL_DAO;
import fi.hy.pese.framework.main.general._Logger;
import fi.hy.pese.framework.main.general.data.DatabaseTableStructure;
import fi.hy.pese.framework.main.general.data.PesaDomainModelDatabaseStructure;
import fi.hy.pese.framework.main.general.data.PesaDomainModelRowFactory;
import fi.hy.pese.framework.main.general.data.Table;
import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.main.general.data._Table;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class OracleSQL_DAO_Test {

	public static Test suite() {
		return new TestSuite(OracleSQL_DAO_Test.class.getDeclaredClasses());
	}

	public static class WritingDataAlterationsToLog extends TestCase {

		//
		// NOTE: The actual functionality is tested in test.general.LogTest
		// Here we only test that oracle_sql_dao uses the logger
		//

		private class FakeLogger implements _Logger {
			private String	messages	= "";

			@Override
			public void log(String message) {}

			@Override
			public void logDelete(_Table table) {
				messages = "delete logged";
			}

			@Override
			public void logInsert(_Table table) {
				messages = "insert logged";
			}

			@Override
			public void logUpdate(_Table oldValues, _Table newValues) {
				messages = "update logged";
			}

			@Override
			public void commitMessages() {
				loggedMessages = messages;
			}
		}

		private String				loggedMessages;
		private _DAO<_PesaDomainModelRow>				dao;
		private _Table				table;
		private _Logger				logger;
		private PesaDomainModelDatabaseStructure	dbStructure;

		@Override
		protected void setUp() throws Exception {
			dbStructure = new PesaDomainModelDatabaseStructure();
			loggedMessages = "";
			logger = new FakeLogger();
			dao = new OracleSQL_DAO<>(new ConnectionStub(), new PesaDomainModelRowFactory(dbStructure, "", ""), logger);
			DatabaseTableStructure structure = new DatabaseTableStructure("test_table");
			structure.addColumn("test_column_1", _Column.VARCHAR, 20);
			structure.get("test_column_1").setTypeToImmutablePartOfAKey();
			dbStructure.setKaynti(structure);
			table = new Table(structure);
			dao.startTransaction();
		}

		@Override
		protected void tearDown() {

		}

		public void test__writing_update_log() throws Exception {
			table.setRowidValue("AAA");
			dao.executeUpdate(table);
			dao.commit();
			assertEquals("update logged", loggedMessages);
		}

		public void test__writing_delete_log() throws Exception {
			dao.executeDelete(table);
			dao.commit();
			assertEquals("delete logged", loggedMessages);
		}

		public void test__does_not_log_before_commit_called____update() throws Exception {
			table.setRowidValue("AAA");
			dao.executeUpdate(table);
			assertEquals("", loggedMessages);
		}

		public void test__does_not_log_before_commit_called____delete() throws Exception {
			dao.executeDelete(table);
			assertEquals("", loggedMessages);
		}

	}
}
