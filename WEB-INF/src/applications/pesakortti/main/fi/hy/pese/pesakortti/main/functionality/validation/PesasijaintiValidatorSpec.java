package fi.hy.pese.pesakortti.main.functionality.validation;

import fi.hy.pese.framework.main.app.functionality.validate.NumericRange;

public class PesasijaintiValidatorSpec {

	/**
	 * meters - decimal
	 */
	public NumericRange<Double> allowedNestHeightRange;

	/**
	 * meters - integer
	 */
	public NumericRange<Integer> allowedTreeHeightRange;

	/**
	 * meters - decimal
	 */
	public NumericRange<Double> allowedNestDistanceFromTreeRange;

	/**
	 * Is the nest in a tree?
	 */
	public boolean inTree = false;

	/**
	 * Tree height warning threshold - integer
	 * Null by default
	 */
	public NumericRange<Integer>	noWarningsTreeHeightRange = null;

	/**
	 * Nest height warning threshold - decimal
	 */
	public NumericRange<Double>	noWarningsNestHeightRange = null;

	/**
	 * Nest distance from tree warning threshold - decimal
	 */
	public NumericRange<Double>	noWarningsNestDistanceFromTreeRange;

}
