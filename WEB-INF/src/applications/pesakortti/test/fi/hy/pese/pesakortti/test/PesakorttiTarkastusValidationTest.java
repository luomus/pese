package fi.hy.pese.pesakortti.test;


import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import fi.hy.pese.framework.main.app.Request;
import fi.hy.pese.framework.main.app.RequestImpleData;
import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.app.functionality.validate._Validator;
import fi.hy.pese.framework.main.db._DAO;
import fi.hy.pese.framework.main.general.PesimisenBiologisetRajatUtil.Rajat;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data.Data;
import fi.hy.pese.framework.main.general.data.PesaDomainModelDatabaseStructure;
import fi.hy.pese.framework.main.general.data.PesaDomainModelRowFactory;
import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._Data;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.main.general.data._ReferenceKey;
import fi.hy.pese.framework.main.general.data._RowFactory;
import fi.hy.pese.framework.main.general.data._Table;
import fi.hy.pese.framework.test.TestConfig;
import fi.hy.pese.framework.test.db.DAOStub;
import fi.hy.pese.pesakortti.main.app.PesakorttiConst;
import fi.hy.pese.pesakortti.main.app.PesakorttiDatabaseStructureCreator;
import fi.hy.pese.pesakortti.main.app.PesakorttiFactory;
import fi.hy.pese.pesakortti.main.functionality.validation.PesakorttiTarkastusValidator;
import fi.hy.pese.pesakortti.main.functionality.validation.PesasijaintiValidatorSpec;
import fi.luomus.commons.containers.Selection;
import fi.luomus.commons.containers.SelectionImple;
import fi.luomus.commons.containers.SelectionOptionImple;
import fi.luomus.commons.tipuapi.TipuAPIClient;
import fi.luomus.commons.tipuapi.TipuApiResource;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.xml.Document.Node;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class PesakorttiTarkastusValidationTest {
	
	public static Test suite() {
		return new TestSuite(PesakorttiTarkastusValidationTest.class.getDeclaredClasses());
	}
	
	public static class WhenSendingTheForm extends TestCase {
		
		private static final String	NESTBOX	= "18";
		private static final String	KELO	= "26";
		private static final String	SPRUCE	= "13";
		private static final String	FLOATING_ON_WATER	= "1";
		
		public class FakeDAO extends DAOStub<_PesaDomainModelRow> {
			
			private final _RowFactory<_PesaDomainModelRow> rowFactory;
			
			public FakeDAO(_RowFactory<_PesaDomainModelRow> rowFactory) {
				this.rowFactory = rowFactory;
			}
			
			@Override
			public _PesaDomainModelRow newRow() {
				_PesaDomainModelRow row = rowFactory.newRow();
				return row;
			}
			
			@Override
			public _Table returnTableByKeys(String table, String ... keys) {
				if (table.equals("ponttoalue") && keys[0].equals("1")) {
					_Table ponttoalue = dao.newRow().getTableByName("ponttoalue");
					ponttoalue.get("kunta").setValue("HELSIN");
					return ponttoalue;
				}
				throw new UnsupportedOperationException("not implemented by this test");
			}
			
			@Override
			public boolean checkReferenceValueExists(String value, _ReferenceKey referenceKey) throws SQLException {
				return true;
			}
			
			@Override
			public boolean checkValuesExists(_Table table) throws SQLException {
				if (table.getName().equals("tarkastus")) {
					if (matches(table)) {
						return true;
					}
					return false;
				}
				throw new UnsupportedOperationException("Not implemented");
			}
			
			private boolean matches(_Table tarkastus) {
				return tarkastus.get("vuosi").getValue().equals("2010") &&
						tarkastus.get("pesiva_laji").getValue().equals("FICHYP") &&
						tarkastus.get("uusintapesinta_jarjestysnro").getValue().equals("1") &&
						tarkastus.get("uusintapesinta_tunniste").getValue().equalsIgnoreCase("valkopunarenkainen pari");
			}
		}
		
		private _Data<_PesaDomainModelRow>				data;
		private PesakorttiTarkastusValidator			validator;
		private _PesaDomainModelRow				params;
		private _DAO<_PesaDomainModelRow>				dao;
		private _Request<_PesaDomainModelRow>			request;
		private final static TipuApiResource kunnat = initMunicipalities();
		private final FakePesakorttiFactory factory = initFactory();
		private final Map<String,Selection> selections = setSelections();
		private static _RowFactory<_PesaDomainModelRow> rowFactory;
		
		private static class FakePesakorttiFactory extends PesakorttiFactory {
			public FakePesakorttiFactory(String configFile) throws FileNotFoundException {
				super(configFile);
			}
			@Override
			public Map<String, Selection> fetchSelections(_DAO<_PesaDomainModelRow> dao)  {
				return null;
			}
			@Override
			public TipuApiResource getTipuApiResource(String resourcename)  {
				if (resourcename.equals(TipuAPIClient.MUNICIPALITIES)) {
					return kunnat;
				}
				throw new IllegalArgumentException("Not implemeted for resource " + resourcename);
			}
		}
		
		private static FakePesakorttiFactory initFactory() {
			FakePesakorttiFactory factory = null;
			try {
				factory = new FakePesakorttiFactory(TestConfig.getConfigFile("pese_pesakortti.config"));
			} catch (Exception e) {
				e.printStackTrace();
				fail(e.toString());
				return factory;
			}
			
			PesaDomainModelDatabaseStructure structure = PesakorttiDatabaseStructureCreator.hardCoded();
			rowFactory = new PesaDomainModelRowFactory(structure, "", "");
			factory.setRowFactory(rowFactory);
			
			return factory;
		}
		
		private static Map<String, Selection> setSelections() {
			Map<String, Selection> selections = new HashMap<>();
			
			SelectionImple kunnat = new SelectionImple(TipuAPIClient.MUNICIPALITIES);
			kunnat.addOption(new SelectionOptionImple("HELSIN", "Helsinki"));
			kunnat.addOption(new SelectionOptionImple("VANTAA", "Vantaa"));
			selections.put(kunnat.getName(), kunnat);
			
			SelectionImple koordtyyppi = new SelectionImple("pesa.koord_ilm_tyyppi");
			koordtyyppi.addOption(new SelectionOptionImple("E", "Euref"));
			koordtyyppi.addOption(new SelectionOptionImple("Y", "Yhtenäis"));
			koordtyyppi.addOption(new SelectionOptionImple("A", "Aste"));
			selections.put(koordtyyppi.getName(), koordtyyppi);
			
			SelectionImple mittaust = new SelectionImple("pesa.koord_mittaustapa");
			mittaust.addOption(new SelectionOptionImple("K", "Kartta"));
			mittaust.addOption(new SelectionOptionImple("G", "GPS"));
			mittaust.addOption(new SelectionOptionImple("X", "Muu"));
			selections.put(mittaust.getName(), mittaust);
			
			SelectionImple pesa_sij = new SelectionImple("pesa.pesa_sijainti");
			for (Map.Entry<String, PesasijaintiValidatorSpec> e : PesakorttiConst.PESAN_SIJAINTI_SPECS.entrySet()) {
				if (e.getKey() == null || e.getKey().equals("")) continue;
				pesa_sij.addOption(new SelectionOptionImple(e.getKey(), e.getKey()));
			}
			selections.put(pesa_sij.getName(), pesa_sij);
			
			SelectionImple tarkastajat = new SelectionImple(TipuAPIClient.RINGERS);
			tarkastajat.addOption(new SelectionOptionImple("333", "Minä"));
			selections.put(tarkastajat.getName(), tarkastajat);
			
			SelectionImple lajit = new SelectionImple(TipuAPIClient.SPECIES);
			lajit.addOption(new SelectionOptionImple("FICHYP", "FICHYP"));
			lajit.addOption(new SelectionOptionImple("PANHAL", "PANHAL"));
			selections.put(lajit.getName(), lajit);
			
			SelectionImple biot = new SelectionImple("tarkastus.pesa_biotooppi");
			biot.addOption(new SelectionOptionImple("20", "hakkuu"));
			biot.addOption(new SelectionOptionImple("40", "tunturi"));
			biot.addOption(new SelectionOptionImple("41", "tunturi"));
			biot.addOption(new SelectionOptionImple("42", "tunturi"));
			biot.addOption(new SelectionOptionImple("43", "tunturi"));
			selections.put(biot.getName(), biot);
			
			SelectionImple biot_ala = new SelectionImple("tarkastus.pesa_biotooppi_ala");
			biot_ala.addOption(new SelectionOptionImple("2", "10 aaria"));
			selections.put(biot_ala.getName(), biot_ala);
			
			SelectionImple k_e = new SelectionImple(PesakorttiConst.K_E);
			k_e.addOption(new SelectionOptionImple("K", "kyllä"));
			k_e.addOption(new SelectionOptionImple("E", "ei"));
			selections.put(k_e.getName(), k_e);
			
			SelectionImple pvmtark = new SelectionImple("kaynti.pvm_tarkkuus");
			pvmtark.addOption(new SelectionOptionImple("1", "tarkka"));
			selections.put(pvmtark.getName(), pvmtark);
			
			SelectionImple yhdyskunnassa = new SelectionImple("tarkastus.yhdyskunnassa");
			yhdyskunnassa.addOption(new SelectionOptionImple("0", "ei"));
			yhdyskunnassa.addOption(new SelectionOptionImple("1", "joo, eri laji"));
			yhdyskunnassa.addOption(new SelectionOptionImple("2", "joo, sama laji"));
			yhdyskunnassa.addOption(new SelectionOptionImple("3", "joo, sama laji, tässä monen pesän tiedot"));
			selections.put(yhdyskunnassa.getName(), yhdyskunnassa);
			
			SelectionImple pesinnan_vaihe = new SelectionImple("kaynti.pesinnan_vaihe");
			pesinnan_vaihe.addOption(new SelectionOptionImple("R9", "Asumaton"));
			pesinnan_vaihe.addOption(new SelectionOptionImple("R1", "rak"));
			pesinnan_vaihe.addOption(new SelectionOptionImple("E1", "emo"));
			pesinnan_vaihe.addOption(new SelectionOptionImple("M0", "muna - ei saailmoittaa"));
			pesinnan_vaihe.addOption(new SelectionOptionImple("M1", "muna"));
			pesinnan_vaihe.addOption(new SelectionOptionImple("P1", "poik"));
			pesinnan_vaihe.addOption(new SelectionOptionImple("T1", "tuho"));
			pesinnan_vaihe.addOption(new SelectionOptionImple("J1", "jälkit"));
			pesinnan_vaihe.addOption(new SelectionOptionImple("P8", "poik"));
			pesinnan_vaihe.addOption(new SelectionOptionImple("P6", "poik"));
			selections.put(pesinnan_vaihe.getName(), pesinnan_vaihe);
			
			SelectionImple sukupuoli = new SelectionImple("lintu.sukupuoli");
			sukupuoli.addOption(new SelectionOptionImple("K", "koiras"));
			selections.put(sukupuoli.getName(), sukupuoli);
			
			return selections;
		}
		
		private Map<Integer, _Table> ponttoalueet;
		
		private Map<Integer, _Table> initPonttoalueet() {
			Map<Integer, _Table> ponttoalueet = new HashMap<>();
			_Table ponttoalue = dao.newRow().getTableByName("ponttoalue");
			ponttoalue.getUniqueNumericIdColumn().setValue("1");
			ponttoalue.get("kunta").setValue("HELSIN");
			ponttoalueet.put(1, ponttoalue);
			return ponttoalueet;
		}
		
		private static TipuApiResource initMunicipalities() {
			TipuApiResource kunnatResource = new TipuApiResource();
			Node helsinki = new Node("municipality");
			helsinki.addChildNode("id").setContents("HELSIN");
			helsinki.addAttribute("centerpoint-lon", "25.0").addAttribute("centerpoint-lat", "60.0").addAttribute("radius", "30");
			Node vantaa = new Node("municipality");
			vantaa.addChildNode("id").setContents("VANTAA");
			kunnatResource.add(helsinki);
			kunnatResource.add(vantaa);
			return kunnatResource;
		}
		
		
		@Override
		protected void setUp() throws Exception {
			data = new Data<>(rowFactory);
			data.setSelections(selections);
			dao = new FakeDAO(rowFactory);
			RequestImpleData<_PesaDomainModelRow> reqData = new RequestImpleData<>();
			reqData.setApplication(factory);
			reqData.setDao(dao);
			reqData.setData(data);
			request = new Request<>(reqData);
			ponttoalueet = initPonttoalueet();
			validator = new PesakorttiTarkastusValidator(request, ponttoalueet, PESYEKOOT);
			params = data.updateparameters();
			data.setPage(Const.TARKASTUS_PAGE);
			data.setAction(Const.INSERT_FIRST);
		}
		
		@Override
		protected void tearDown() throws Exception {}
		
		public void test__() throws Exception {
			validator.validate();
		}
		
		public void test__calling_type_validations() throws Exception {
			_Column pesaid = params.getPesa().getUniqueNumericIdColumn();
			_Column kunta = params.getPesa().get("kunta");
			pesaid.setValue("asda");
			kunta.setValue("toooooo looooong");
			validator.validate();
			assertEquals(_Validator.MUST_BE_INTEGER, errorFor(pesaid));
			assertEquals(_Validator.TOO_LONG, errorFor(kunta));
		}
		
		private String errorFor(_Column pesaid) {
			return validator.errors().get(pesaid.getFullname());
		}
		
		public void test__kunta() throws Exception {
			_Column kunta = params.getPesa().get("kunta");
			kunta.setValue("");
			validator.validate();
			assertEquals(_Validator.CAN_NOT_BE_NULL, errorFor(kunta));
		}
		
		public void test__kunta_2() throws Exception {
			_Column kunta = params.getPesa().get("kunta");
			kunta.setValue("FOOBAR");
			validator.validate();
			assertEquals(_Validator.ASSOSIATED_SELECTION_DOES_NOT_CONTAIN_THE_VALUE, errorFor(kunta));
		}
		
		public void test__coordinates_given__are_valid() throws Exception {
			column("pesa.koord_ilm_tyyppi").setValue("F");
			validator.validate();
			assertEquals(_Validator.ASSOSIATED_SELECTION_DOES_NOT_CONTAIN_THE_VALUE, error("pesa.koord_ilm_tyyppi"));
		}
		
		private String error(String columnFullname) {
			return validator.errors().get(columnFullname);
		}
		
		private String warning(String columnFullname) {
			return validator.warnings().get(columnFullname);
		}
		
		private _Column column(String fullname) {
			return params.getColumn(fullname);
		}
		
		public void test__coordinates_given__are_valid3() throws Exception {
			column("pesa.koord_ilm_tyyppi").setValue("Y");
			column("pesa.yht_leveys").setValue("asdsa");
			column("pesa.yht_pituus").setValue("36");
			validator.validate();
			assertEquals("Y", column("pesa.koord_ilm_tyyppi").getValue());
			assertEquals(null, error("pesa.koord_ilm_tyyppi"));
			assertEquals(_Validator.MUST_BE_INTEGER, error("pesa.yht_leveys"));
			assertEquals(_Validator.INVALID_VALUE, error("pesa.yht_pituus"));
		}
		
		public void test__coordinates_given__are_valid4() throws Exception {
			column("pesa.koord_ilm_tyyppi").setValue("E");
			column("pesa.eur_leveys").setValue("asdsa");
			column("pesa.eur_pituus").setValue("750000");
			validator.validate();
			assertEquals(null, error("pesa.koord_ilm_tyyppi"));
			assertEquals(_Validator.MUST_BE_INTEGER, error("pesa.eur_leveys"));
			assertEquals(_Validator.INVALID_VALUE, error("pesa.eur_pituus"));
		}
		
		public void test__coordinates_are_inside_selected_kunta() throws Exception {
			column("pesa.koord_ilm_tyyppi").setValue("Y");
			column("pesa.yht_leveys").setValue("6678730");
			column("pesa.yht_pituus").setValue("3387147");
			column("pesa.kunta").setValue("HELSIN");
			validator.validate();
			assertEquals(null, error("pesa.ast_leveys"));
			assertEquals(null, error("pesa.ast_pituus"));
			assertEquals(null, error("pesa.koord_ilm_tyyppi"));
			assertEquals(null, error("pesa.kunta"));
		}
		
		public void test__coordinates_are_inside_selected_kunta2() throws Exception {
			column("pesa.koord_ilm_tyyppi").setValue("Y");
			column("pesa.yht_leveys").setValue("6678730");
			column("pesa.yht_pituus").setValue("3387147");
			column("pesa.kunta").setValue("sdsadas");
			validator.validate();
			assertEquals(null, error("pesa.ast_leveys"));
			assertEquals(null, error("pesa.ast_pituus"));
			assertEquals(null, error("pesa.koord_ilm_tyyppi"));
			assertEquals(_Validator.ASSOSIATED_SELECTION_DOES_NOT_CONTAIN_THE_VALUE, error("pesa.kunta"));
		}
		
		public void test__coordinates_are_inside_selected_kunta3() throws Exception {
			column("pesa.koord_ilm_tyyppi").setValue("Y");
			column("pesa.yht_leveys").setValue("6708000");
			column("pesa.yht_pituus").setValue("3388000");
			column("pesa.kunta").setValue("HELSIN");
			validator.validate();
			assertEquals(null, error("pesa.ast_leveys"));
			assertEquals(_Validator.COORDINATES_NOT_INSIDE_REGION, error("pesa.yht_pituus"));
			assertEquals(_Validator.COORDINATES_NOT_INSIDE_REGION, error("pesa.yht_leveys"));
			assertEquals(null, error("pesa.kunta"));
		}
		
		public void test___empty_coords_allowed_but_then__none_of_the_value_can_be_given() throws Exception {
			column("pesa.koord_ilm_tyyppi").setValue("");
			column("pesa.yht_leveys").setValue("");
			column("pesa.yht_pituus").setValue("");
			column("pesa.eur_leveys").setValue("");
			column("pesa.eur_pituus").setValue("");
			column("pesa.ast_leveys").setValue("");
			column("pesa.ast_pituus").setValue("");
			column("pesa.koord_tarkkuus").setValue("");
			column("pesa.koord_mittaustapa").setValue("");
			
			validator.validate();
			
			assertEquals(null, error("pesa.koord_ilm_tyyppi"));
			assertEquals(null, error("pesa.yht_leveys"));
			assertEquals(null, error("pesa.yht_pituus"));
			assertEquals(null, error("pesa.eur_leveys"));
			assertEquals(null, error("pesa.eur_pituus"));
			assertEquals(null, error("pesa.ast_leveys"));
			assertEquals(null, error("pesa.ast_pituus"));
			assertEquals(null, error("pesa.koord_tarkkuus"));
			assertEquals(null, error("pesa.koord_mittaustapa"));
		}
		
		public void test___empty_coords_allowed_but_then__none_of_the_value_can_be_given______if_ilm_tyyppi_is_only_one__it_is_cleared() throws Exception {
			// because radio button value can not be emtied
			column("pesa.koord_ilm_tyyppi").setValue("E");
			column("pesa.yht_leveys").setValue("");
			column("pesa.yht_pituus").setValue("");
			column("pesa.eur_leveys").setValue("");
			column("pesa.eur_pituus").setValue("");
			column("pesa.ast_leveys").setValue("");
			column("pesa.ast_pituus").setValue("");
			column("pesa.koord_tarkkuus").setValue("");
			column("pesa.koord_mittaustapa").setValue("");
			
			validator.validate();
			
			assertEquals(null, error("pesa.koord_ilm_tyyppi"));
			assertEquals(null, error("pesa.yht_leveys"));
			assertEquals(null, error("pesa.yht_pituus"));
			assertEquals(null, error("pesa.eur_leveys"));
			assertEquals(null, error("pesa.eur_pituus"));
			assertEquals(null, error("pesa.ast_leveys"));
			assertEquals(null, error("pesa.ast_pituus"));
			assertEquals(null, error("pesa.koord_tarkkuus"));
			assertEquals(null, error("pesa.koord_mittaustapa"));
			assertEquals("", column("pesa.koord_ilm_tyyppi").getValue());
		}
		
		public void test___empty_coords_allowed_but_then__none_of_the_value_can_be_given_2() throws Exception {
			column("pesa.koord_ilm_tyyppi").setValue("");
			column("pesa.yht_leveys").setValue("6666666");
			column("pesa.yht_pituus").setValue("3333333");
			column("pesa.eur_leveys").setValue("");
			column("pesa.eur_pituus").setValue("");
			column("pesa.ast_leveys").setValue("");
			column("pesa.ast_pituus").setValue("");
			column("pesa.koord_tarkkuus").setValue("");
			column("pesa.koord_mittaustapa").setValue("");
			
			validator.validate();
			
			assertEquals(_Validator.CAN_NOT_BE_NULL, error("pesa.koord_ilm_tyyppi"));
			assertEquals(null, error("pesa.yht_leveys"));
			assertEquals(null, error("pesa.yht_pituus"));
			assertEquals(null, error("pesa.eur_leveys"));
			assertEquals(null, error("pesa.eur_pituus"));
			assertEquals(null, error("pesa.ast_leveys"));
			assertEquals(null, error("pesa.ast_pituus"));
			assertEquals(_Validator.CAN_NOT_BE_NULL, error("pesa.koord_tarkkuus"));
			assertEquals(_Validator.ARE_YOU_SURE_VALUE_IS_NULL, warning("pesa.koord_mittaustapa"));
		}
		
		public void test___empty_coords_allowed_but_then__none_of_the_value_can_be_given_3() throws Exception {
			column("pesa.koord_ilm_tyyppi").setValue("E");
			column("pesa.yht_leveys").setValue("6666666");
			column("pesa.yht_pituus").setValue("3333333");
			column("pesa.eur_leveys").setValue("");
			column("pesa.eur_pituus").setValue("");
			column("pesa.ast_leveys").setValue("");
			column("pesa.ast_pituus").setValue("");
			column("pesa.koord_tarkkuus").setValue("1");
			column("pesa.koord_mittaustapa").setValue("");
			
			validator.validate();
			
			assertEquals(null, error("pesa.koord_ilm_tyyppi"));
			assertEquals(null, error("pesa.yht_leveys"));
			assertEquals(null, error("pesa.yht_pituus"));
			assertEquals(_Validator.CAN_NOT_BE_NULL, error("pesa.eur_leveys"));
			assertEquals(_Validator.CAN_NOT_BE_NULL, error("pesa.eur_pituus"));
			assertEquals(null, error("pesa.ast_leveys"));
			assertEquals(null, error("pesa.ast_pituus"));
			assertEquals(null, error("pesa.koord_tarkkuus"));
			assertEquals(_Validator.ARE_YOU_SURE_VALUE_IS_NULL, warning("pesa.koord_mittaustapa"));
		}
		
		public void test___empty_coords_allowed_but_then__none_of_the_value_can_be_given_4() throws Exception {
			column("pesa.koord_ilm_tyyppi").setValue("E");
			column("pesa.yht_leveys").setValue("6666666");
			column("pesa.yht_pituus").setValue("3333333");
			column("pesa.eur_leveys").setValue("");
			column("pesa.eur_pituus").setValue("");
			column("pesa.ast_leveys").setValue("23232");
			column("pesa.ast_pituus").setValue("22222");
			column("pesa.koord_tarkkuus").setValue("1");
			column("pesa.koord_mittaustapa").setValue("");
			
			validator.validate();
			
			assertEquals(null, error("pesa.koord_ilm_tyyppi"));
			assertEquals(null, error("pesa.yht_leveys"));
			assertEquals(null, error("pesa.yht_pituus"));
			assertEquals(_Validator.CAN_NOT_BE_NULL, error("pesa.eur_leveys"));
			assertEquals(_Validator.CAN_NOT_BE_NULL, error("pesa.eur_pituus"));
			assertEquals(null, error("pesa.ast_leveys"));
			assertEquals(null, error("pesa.ast_pituus"));
			assertEquals(null, error("pesa.koord_tarkkuus"));
			assertEquals(_Validator.ARE_YOU_SURE_VALUE_IS_NULL, warning("pesa.koord_mittaustapa"));
		}
		
		public void test__kirjekyyhky_coordinate_validation_1() throws Exception {
			column("pesa.koord_ilm_tyyppi").setValue("X");
			
			validator.setKirjekyyhkyValidation(true);
			validator.validate();
			
			assertEquals(_Validator.ASSOSIATED_SELECTION_DOES_NOT_CONTAIN_THE_VALUE, error("pesa.koord_ilm_tyyppi"));
		}
		
		public void test__kirjekyyhky_coordinate_validation_2() throws Exception {
			validator.setKirjekyyhkyValidation(true);
			validator.validate();
			
			assertEquals(_Validator.CAN_NOT_BE_NULL, error("pesa.koord_ilm_tyyppi"));
		}
		
		public void test__kirjekyyhky_coordinate_validation_3() throws Exception {
			column("pesa.koord_ilm_tyyppi").setValue("E");
			column("pesa.yht_leveys").setValue("6666666");
			column("pesa.yht_pituus").setValue("3333333");
			column("pesa.eur_leveys").setValue("");
			column("pesa.eur_pituus").setValue("");
			column("pesa.ast_leveys").setValue("23232");
			column("pesa.ast_pituus").setValue("22222");
			column("pesa.koord_tarkkuus").setValue("1");
			column("pesa.koord_mittaustapa").setValue("");
			
			validator.setKirjekyyhkyValidation(true);
			validator.validate();
			
			assertEquals(null, error("pesa.koord_ilm_tyyppi"));
			assertEquals(null, error("pesa.yht_leveys"));
			assertEquals(null, error("pesa.yht_pituus"));
			assertEquals(_Validator.CAN_NOT_BE_NULL, error("pesa.pituus"));
			assertEquals(_Validator.CAN_NOT_BE_NULL, error("pesa.leveys"));
			assertEquals(null, error("pesa.ast_leveys"));
			assertEquals(null, error("pesa.ast_pituus"));
			assertEquals(null, error("pesa.koord_tarkkuus"));
			assertEquals(null, warning("pesa.koord_mittaustapa"));
			assertEquals(null, error("pesa.koord_mittaustapa"));
		}
		
		public void test__kirjekyyhky_coordinate_validation_4() throws Exception {
			column("pesa.koord_ilm_tyyppi").setValue("Y");
			column("pesa.yht_leveys").setValue("6666666");
			column("pesa.yht_pituus").setValue("3333333");
			column("pesa.kunta").setValue("HELSIN");
			
			validator.setKirjekyyhkyValidation(true);
			validator.validate();
			
			assertEquals(null, error("pesa.koord_ilm_tyyppi"));
			assertEquals(null, error("pesa.yht_leveys"));
			assertEquals(null, error("pesa.yht_pituus"));
			assertEquals(_Validator.COORDINATES_NOT_INSIDE_REGION, error("pesa.pituus"));
			assertEquals(_Validator.COORDINATES_NOT_INSIDE_REGION, error("pesa.leveys"));
			assertEquals(null, error("pesa.ast_leveys"));
			assertEquals(null, error("pesa.ast_pituus"));
			assertEquals(null, error("pesa.koord_tarkkuus"));
			assertEquals(null, warning("pesa.koord_mittaustapa"));
			assertEquals(null, error("pesa.koord_mittaustapa"));
		}
		
		public void test___empty_coords_allowed_but_then__none_of_the_value_can_be_given_5() throws Exception {
			column("pesa.koord_ilm_tyyppi").setValue("E");
			column("pesa.yht_leveys").setValue("");
			column("pesa.yht_pituus").setValue("");
			column("pesa.eur_leveys").setValue("6666666");
			column("pesa.eur_pituus").setValue("333333");
			column("pesa.ast_leveys").setValue("23232");
			column("pesa.ast_pituus").setValue("22222");
			column("pesa.koord_tarkkuus").setValue("1");
			column("pesa.koord_mittaustapa").setValue("K");
			
			validator.validate();
			
			assertEquals(null, error("pesa.koord_ilm_tyyppi"));
			assertEquals(null, error("pesa.yht_leveys"));
			assertEquals(null, error("pesa.yht_pituus"));
			assertEquals(null, error("pesa.eur_leveys"));
			assertEquals(null, error("pesa.eur_pituus"));
			assertEquals(null, error("pesa.ast_leveys"));
			assertEquals(null, error("pesa.ast_pituus"));
			assertEquals(null, error("pesa.koord_tarkkuus"));
			assertEquals(null, error("pesa.koord_mittaustapa"));
		}
		
		public void test___koord_tarkkuus() throws Exception {
			column("pesa.koord_tarkkuus").setValue("12000");
			validator.validate();
			assertEquals(null, error("pesa.koord_tarkkuus"));
		}
		
		public void test___koord_tarkkuus_2() throws Exception {
			column("pesa.koord_tarkkuus").setValue("-1");
			validator.validate();
			assertEquals(_Validator.INVALID_VALUE, error("pesa.koord_tarkkuus"));
		}
		
		public void test___koord_tarkkuus_3() throws Exception {
			column("pesa.koord_tarkkuus").setValue(PesakorttiConst.MAX_KOORD_TARKKUUS);
			validator.validate();
			assertEquals(null, error("pesa.koord_tarkkuus"));
		}
		
		public void test___koord_tarkkuus_4() throws Exception {
			column("pesa.koord_tarkkuus").setValue(PesakorttiConst.MAX_KOORD_TARKKUUS+1);
			validator.validate();
			assertEquals(_Validator.INVALID_VALUE, error("pesa.koord_tarkkuus"));
		}
		
		public void test___koord_tarkkuus_5() throws Exception {
			column("pesa.koord_tarkkuus").setValue("asdasd");
			validator.validate();
			assertEquals(_Validator.MUST_BE_INTEGER, error("pesa.koord_tarkkuus"));
		}
		
		public void test___koord_mittaustapa() throws Exception {
			column("pesa.koord_tarkkuus").setValue(1);
			column("pesa.koord_mittaustapa").setValue("X");
			validator.validate();
			assertEquals(null, error("pesa.koord_tarkkuus"));
			assertEquals(null, error("pesa.koord_mittaustapa"));
			assertEquals(_Validator.ARE_YOU_SURE, warning("pesa.koord_mittaustapa"));
		}
		
		public void test___koord_mittaustapa_2() throws Exception {
			column("pesa.koord_tarkkuus").setValue(23000);
			column("pesa.koord_mittaustapa").setValue("X");
			validator.validate();
			assertEquals(null, error("pesa.koord_tarkkuus"));
			assertEquals(null, error("pesa.koord_mittaustapa"));
			assertEquals(null, warning("pesa.koord_mittaustapa"));
		}
		
		public void test___koord_mittaustapa_3() throws Exception {
			column("pesa.koord_tarkkuus").setValue(23000);
			column("pesa.koord_mittaustapa").setValue("G");
			validator.validate();
			assertEquals(null, error("pesa.koord_tarkkuus"));
			assertEquals(null, error("pesa.koord_mittaustapa"));
			assertEquals(_Validator.ARE_YOU_SURE, warning("pesa.koord_mittaustapa"));
		}
		
		public void test___koord_mittaustapa_____bypassing_warning() throws Exception {
			column("pesa.koord_tarkkuus").setValue(23000);
			column("pesa.koord_mittaustapa").setValue("G");
			data.bypassWarning(column("pesa.koord_mittaustapa"));
			validator.validate();
			assertEquals(null, error("pesa.koord_tarkkuus"));
			assertEquals(null, error("pesa.koord_mittaustapa"));
			assertEquals(null, warning("pesa.koord_mittaustapa"));
		}
		
		public void test___koord_mittaustapa__warnings__for_kartta() throws Exception {
			column("pesa.koord_tarkkuus").setValue(1);
			column("pesa.koord_mittaustapa").setValue("K");
			validator.validate();
			assertEquals(null, error("pesa.koord_tarkkuus"));
			assertEquals(null, error("pesa.koord_mittaustapa"));
			assertEquals(_Validator.ARE_YOU_SURE, warning("pesa.koord_mittaustapa"));
		}
		
		public void test___koord_mittaustapa__warnings__for_kartta2() throws Exception {
			column("pesa.koord_tarkkuus").setValue(100);
			column("pesa.koord_mittaustapa").setValue("K");
			validator.validate();
			assertEquals(null, error("pesa.koord_tarkkuus"));
			assertEquals(null, error("pesa.koord_mittaustapa"));
			assertEquals(null, warning("pesa.koord_mittaustapa"));
		}
		
		public void test___nest_site__vs__tree_info__nest_height() throws Exception {
			column("pesa.pesa_sijainti").setValue(SPRUCE);
			column("tarkastus.pesa_korkeus").setValue("5,5"); // m
			column("tarkastus.puu_korkeus").setValue("10"); // m
			column("tarkastus.pesa_etaisyys_runko").setValue("0.30"); // m
			validator.validate();
			assertEquals(null, error("pesa.pesa_sijainti"));
			assertEquals(null, error("tarkastus.pesa_korkeus"));
			assertEquals(null, error("tarkastus.puu_korkeus"));
			assertEquals(null, error("tarkastus.pesa_etaisyys_runko"));
		}
		
		public void test___nest_site__vs__tree_info__nest_height_2() throws Exception {
			column("pesa.pesa_sijainti").setValue(FLOATING_ON_WATER);
			column("tarkastus.pesa_korkeus").setValue("5,5"); // m
			column("tarkastus.puu_korkeus").setValue("10"); // m
			column("tarkastus.pesa_etaisyys_runko").setValue("30"); // cm
			validator.validate();
			assertEquals(PesakorttiConst.NEST_TOO_HIGH_FOR_THIS_TYPE_OF_NEST, error("pesa.pesa_sijainti"));
			assertEquals(PesakorttiConst.NEST_TOO_HIGH_FOR_THIS_TYPE_OF_NEST, error("tarkastus.pesa_korkeus"));
			assertEquals(PesakorttiConst.TREE_INFO_CAN_NOT_BE_GIVEN_FOR_THIS_TYPE_OF_NEST, error("tarkastus.puu_korkeus"));
			assertEquals(PesakorttiConst.TREE_INFO_CAN_NOT_BE_GIVEN_FOR_THIS_TYPE_OF_NEST, error("tarkastus.pesa_etaisyys_runko"));
		}
		
		public void test___nest_site__vs__tree_info__nest_height_3() throws Exception {
			column("pesa.pesa_sijainti").setValue(FLOATING_ON_WATER);
			column("tarkastus.pesa_korkeus").setValue("0,5"); // m
			column("tarkastus.puu_korkeus").setValue(""); // m
			column("tarkastus.pesa_etaisyys_runko").setValue(""); // cm
			validator.validate();
			assertEquals(null, error("pesa.pesa_sijainti"));
			assertEquals(null, error("tarkastus.pesa_korkeus"));
			assertEquals(null, error("tarkastus.puu_korkeus"));
			assertEquals(null, error("tarkastus.pesa_etaisyys_runko"));
		}
		
		public void test___nest_site__vs__tree_info__nest_height_4() throws Exception {
			column("pesa.pesa_sijainti").setValue(KELO);
			column("tarkastus.pesa_korkeus").setValue("15,5"); // m
			column("tarkastus.puu_korkeus").setValue("25"); // m
			column("tarkastus.pesa_etaisyys_runko").setValue("2,00"); // m
			validator.validate();
			assertEquals(null, error("pesa.pesa_sijainti"));
			assertEquals(null, error("tarkastus.pesa_korkeus"));
			assertEquals(null, error("tarkastus.puu_korkeus"));
			assertEquals(null, error("tarkastus.pesa_etaisyys_runko"));
		}
		
		public void test___nest_site__vs__tree_info__nest_height_5() throws Exception {
			column("pesa.pesa_sijainti").setValue(KELO);
			column("tarkastus.pesa_korkeus").setValue("55,5"); // m
			column("tarkastus.puu_korkeus").setValue("60"); // m
			column("tarkastus.pesa_etaisyys_runko").setValue("5,10"); // m
			validator.validate();
			assertEquals(PesakorttiConst.NEST_TOO_HIGH_FOR_THIS_TYPE_OF_NEST, error("pesa.pesa_sijainti"));
			assertEquals(PesakorttiConst.NEST_TOO_HIGH_FOR_THIS_TYPE_OF_NEST, error("tarkastus.pesa_korkeus"));
			assertEquals(_Validator.TOO_LARGE, error("tarkastus.puu_korkeus"));
			assertEquals(null, error("tarkastus.pesa_etaisyys_runko"));
			assertEquals(_Validator.ARE_YOU_SURE, warning("tarkastus.pesa_etaisyys_runko"));
		}
		
		public void test___nest_site__vs__tree_info__nest_height_6() throws Exception {
			column("pesa.pesa_sijainti").setValue(KELO);
			column("tarkastus.pesa_korkeus").setValue("-6"); // m
			column("tarkastus.puu_korkeus").setValue("60"); // m
			column("tarkastus.pesa_etaisyys_runko").setValue(""); // m
			validator.validate();
			assertEquals(PesakorttiConst.TREE_TOO_HIGH_FOR_THIS_TYPE_OF_NEST, error("pesa.pesa_sijainti"));
			assertEquals(_Validator.TOO_SMALL, error("tarkastus.pesa_korkeus"));
			assertEquals(PesakorttiConst.TREE_TOO_HIGH_FOR_THIS_TYPE_OF_NEST, error("tarkastus.puu_korkeus"));
			assertEquals(null, error("tarkastus.pesa_etaisyys_runko"));
		}
		
		public void test___nest_site__vs__tree_info__nest_height_7() throws Exception {
			column("pesa.pesa_sijainti").setValue(KELO);
			column("tarkastus.pesa_korkeus").setValue("1,7"); // m
			column("tarkastus.puu_korkeus").setValue("1"); // m
			validator.validate();
			assertEquals(null, error("tarkastus.pesa_korkeus"));
			assertEquals(null, error("tarkastus.puu_korkeus"));
		}
		
		public void test___nest_site__vs__tree_info__warning_threshold_() throws Exception {
			column("pesa.pesa_sijainti").setValue(SPRUCE);
			column("tarkastus.puu_korkeus").setValue("50"); // m
			validator.validate();
			assertEquals(PesakorttiConst.TREE_TOO_HIGH_FOR_THIS_TYPE_OF_NEST, error("tarkastus.puu_korkeus"));
			assertEquals(null, warning("tarkastus.puu_korkeus"));
		}
		
		public void test___nest_site__vs__tree_info__warning_threshold_2() throws Exception {
			column("pesa.pesa_sijainti").setValue(SPRUCE);
			column("tarkastus.puu_korkeus").setValue("37"); // m
			validator.validate();
			assertEquals(null, error("tarkastus.puu_korkeus"));
			assertEquals(_Validator.ARE_YOU_SURE, warning("tarkastus.puu_korkeus"));
		}
		
		public void test___ponttoalue__() throws Exception {
			column("pesa.ponttoalue_nro").setValue("");
			column("pesa.pontto_nro").setValue("");
			validator.validate();
			assertEquals(null, error("pesa.ponttoalue_nro"));
			assertEquals(null, error("pesa.pontto_nro"));
		}
		
		public void test___ponttoalue__if_alue_given_pontto_must_be_given() throws Exception {
			column("pesa.ponttoalue_nro").setValue("1");
			column("pesa.pontto_nro").setValue("");
			validator.validate();
			assertEquals(PesakorttiConst.PONTTOALUE_PONTTO_MUST_BOTH_BE_GIVEN_OR_NEITHER, error("pesa.ponttoalue_nro"));
			assertEquals(PesakorttiConst.PONTTOALUE_PONTTO_MUST_BOTH_BE_GIVEN_OR_NEITHER, error("pesa.pontto_nro"));
		}
		
		public void test___ponttoalue__if_alue_given_pontto_must_be_given_2() throws Exception {
			column("pesa.ponttoalue_nro").setValue("");
			column("pesa.pontto_nro").setValue("1");
			validator.validate();
			assertEquals(PesakorttiConst.PONTTOALUE_PONTTO_MUST_BOTH_BE_GIVEN_OR_NEITHER, error("pesa.ponttoalue_nro"));
			assertEquals(PesakorttiConst.PONTTOALUE_PONTTO_MUST_BOTH_BE_GIVEN_OR_NEITHER, error("pesa.pontto_nro"));
		}
		
		public void test___ponttoalue__if_alue_given_pontto_must_be_given_3() throws Exception {
			column("pesa.ponttoalue_nro").setValue("1");
			column("pesa.pontto_nro").setValue("1");
			column("pesa.pesa_sijainti").setValue(NESTBOX);
			validator.validate();
			assertEquals(null, error("pesa.ponttoalue_nro"));
			assertEquals(null, error("pesa.pontto_nro"));
		}
		
		public void test__ponttoalue_must_exists() throws Exception {
			column("pesa.ponttoalue_nro").setValue("999");
			column("pesa.pontto_nro").setValue("1");
			column("pesa.pesa_sijainti").setValue(NESTBOX);
			validator.validate();
			assertEquals(_Validator.DOES_NOT_EXIST, error("pesa.ponttoalue_nro"));
			assertEquals(null, error("pesa.pontto_nro"));
		}
		
		public void test__ponttoalue_kunta_and_pesa_kunta__should_match() throws Exception {
			column("pesa.kunta").setValue("HELSIN");
			column("pesa.ponttoalue_nro").setValue("1");
			column("pesa.pontto_nro").setValue("1");
			column("pesa.pesa_sijainti").setValue(NESTBOX);
			validator.validate();
			assertEquals(null, error("pesa.kunta"));
			assertEquals(null, warning("pesa.kunta"));
		}
		
		public void test__ponttoalue_kunta_and_pesa_kunta__should_match_2() throws Exception {
			column("pesa.kunta").setValue("VANTAA");
			column("pesa.ponttoalue_nro").setValue("1");
			column("pesa.pontto_nro").setValue("1");
			column("pesa.pesa_sijainti").setValue(NESTBOX);
			validator.validate();
			assertEquals(null, error("pesa.kunta"));
			assertEquals(PesakorttiConst.PONTTOALUE_KUNTA_DOES_NOT_MATCH_WITH_PESA_KUNTA, warning("pesa.kunta"));
		}
		
		public void test___tark_v____valid() throws Exception {
			column("tarkastus.vuosi").setValue(DateUtils.getCurrentYear());
			validator.validate();
			assertEquals(null, error("tarkastus.vuosi"));
			assertEquals(null, warning("tarkastus.vuosi"));
		}
		
		public void test___tark_v____can_not_be_in_future() throws Exception {
			column("tarkastus.vuosi").setValue(DateUtils.getCurrentYear()+1);
			validator.validate();
			assertEquals(_Validator.CAN_NOT_BE_IN_THE_FUTURE, error("tarkastus.vuosi"));
			assertEquals(null, warning("tarkastus.vuosi"));
		}
		
		public void test___tark_v____historic() throws Exception {
			column("tarkastus.vuosi").setValue(DateUtils.getCurrentYear() - _Validator.YEAR_WARNING_THRESHOLD);
			validator.validate();
			assertEquals(null, error("tarkastus.vuosi"));
			assertEquals(_Validator.ARE_YOU_SURE, warning("tarkastus.vuosi"));
		}
		
		public void test___tark_v____historic_2() throws Exception {
			column("tarkastus.vuosi").setValue(DateUtils.getCurrentYear() - 5);
			validator.validate();
			assertEquals(null, error("tarkastus.vuosi"));
			assertEquals(null, warning("tarkastus.vuosi"));
		}
		
		public void test___havainnoija_required() throws Exception {
			column("tarkastus.tarkastaja").setValue("");
			validator.validate();
			assertEquals(null, error("tarkastus.tarkastaja"));
			assertEquals(_Validator.ARE_YOU_SURE_VALUE_IS_NULL, warning("tarkastus.tarkastaja"));
		}
		
		public void test___havainnoija_required_2() throws Exception {
			column("tarkastus.tarkastaja").setValue("333");
			validator.validate();
			assertEquals(null, error("tarkastus.tarkastaja"));
			assertEquals(null, warning("tarkastus.tarkastaja"));
		}
		
		public void test___pesiva_laji__can_not_be__given__if() throws Exception {
			column("tarkastus.vuosi").setValue("1990");
			column("tarkastus.pesiva_laji").setValue("FICHYP");
			column("kaynti.1.pesinnan_vaihe").setValue("R9");
			column("kaynti.2.pesinnan_vaihe").setValue("R9");
			validator.validate();
			assertEquals(PesakorttiConst.PESIVA_LAJI_MUST_BE_EMPTY_WITH_THIS_NESTING_STAGE, error("tarkastus.pesiva_laji"));
			assertEquals(null, warning("tarkastus.pesiva_laji"));
		}
		
		public void test___pesiva_laji__must_be__given__otherwise() throws Exception {
			column("tarkastus.vuosi").setValue("1990");
			column("tarkastus.pesiva_laji").setValue("FICHYP");
			column("kaynti.1.pesinnan_vaihe").setValue("R9");
			column("kaynti.2.pesinnan_vaihe").setValue("M1");
			validator.validate();
			assertEquals(null, error("tarkastus.pesiva_laji"));
			assertEquals(null, warning("tarkastus.pesiva_laji"));
		}
		
		public void test___pesiva_laji__must_be__given__otherwise_2() throws Exception {
			column("tarkastus.vuosi").setValue("1990");
			column("tarkastus.pesiva_laji").setValue("");
			column("kaynti.1.pesinnan_vaihe").setValue("R9");
			column("kaynti.2.pesinnan_vaihe").setValue("M1");
			validator.validate();
			assertEquals(PesakorttiConst.PESIVA_LAJI_REQUIRED_WITH_THIS_NESTING_STAGE, error("tarkastus.pesiva_laji"));
			assertEquals(null, warning("tarkastus.pesiva_laji"));
		}
		
		public void test___pesinnan_jarjestysnro___1() throws Exception {
			column("tarkastus.vuosi").setValue("2010");
			column("tarkastus.pesiva_laji").setValue("FICHYP");
			column("tarkastus.uusintapesinta_jarjestysnro").setValue("0");
			column("tarkastus.uusintapesinta_tunniste").setValue("");
			validator.validate();
			assertEquals(null, error("tarkastus.uusintapesinta_jarjestysnro"));
			assertEquals(null, error("tarkastus.uusintapesinta_tunniste"));
		}
		
		public void test___pesinnan_jarjestysnro___2() throws Exception {
			column("tarkastus.vuosi").setValue("2010");
			column("tarkastus.pesiva_laji").setValue("FICHYP");
			column("tarkastus.uusintapesinta_jarjestysnro").setValue("1");
			column("tarkastus.uusintapesinta_tunniste").setValue("");
			validator.validate();
			assertEquals(null, error("tarkastus.uusintapesinta_jarjestysnro"));
			assertEquals(_Validator.CAN_NOT_BE_NULL, error("tarkastus.uusintapesinta_tunniste"));
		}
		
		public void test___pesinnan_jarjestysnro___3() throws Exception {
			column("tarkastus.vuosi").setValue("2010");
			column("tarkastus.pesiva_laji").setValue("FICHYP");
			column("tarkastus.uusintapesinta_jarjestysnro").setValue("1");
			column("tarkastus.uusintapesinta_tunniste").setValue("valkopunarenkainen pari");
			validator.validate();
			assertEquals(PesakorttiConst.NESTING_SEQUENCE_USED_MORE_THAN_ONCE, error("tarkastus.uusintapesinta_jarjestysnro"));
			assertEquals(PesakorttiConst.NESTING_SEQUENCE_USED_MORE_THAN_ONCE, error("tarkastus.uusintapesinta_tunniste"));
		}
		
		public void test___pesinnan_jarjestysnro___3_2() throws Exception { // TODO enemmän testejä tästä (kts. peto vastaavat)
			params.getTarkastus().getUniqueNumericIdColumn().setValue("2321");
			params.getPesa().getUniqueNumericIdColumn().setValue("2321");
			data.setAction(Const.UPDATE);
			
			column("tarkastus.vuosi").setValue("2010");
			column("tarkastus.pesiva_laji").setValue("FICHYP");
			column("tarkastus.uusintapesinta_jarjestysnro").setValue("1");
			column("tarkastus.uusintapesinta_tunniste").setValue("valkopunarenkainen pari");
			
			validator.validate();
			
			assertEquals(null, error("tarkastus.uusintapesinta_jarjestysnro"));
			assertEquals(null, error("tarkastus.uusintapesinta_tunniste"));
		}
		
		public void test___pesinnan_jarjestysnro___4() throws Exception {
			column("tarkastus.vuosi").setValue("2010");
			column("tarkastus.pesiva_laji").setValue("FICHYP");
			column("tarkastus.uusintapesinta_jarjestysnro").setValue("2");
			column("tarkastus.uusintapesinta_tunniste").setValue("valkopunarenkainen pari");
			validator.validate();
			assertEquals(null, error("tarkastus.uusintapesinta_jarjestysnro"));
			assertEquals(null, error("tarkastus.uusintapesinta_tunniste"));
		}
		
		public void test___pesinnan_jarjestysnro___5() throws Exception {
			column("tarkastus.vuosi").setValue("2010");
			column("tarkastus.pesiva_laji").setValue("FICHYP");
			column("tarkastus.uusintapesinta_jarjestysnro").setValue("2");
			column("tarkastus.uusintapesinta_tunniste").setValue("valkopnarenkainen pari typo");
			column("kaynti.1.pesinnan_vaihe").setValue("P1");
			validator.validate();
			assertEquals(PesakorttiConst.MUST_HAVE_NESTING_SEQUENCE_MINUS_ONE, error("tarkastus.uusintapesinta_jarjestysnro"));
			assertEquals(PesakorttiConst.MUST_HAVE_NESTING_SEQUENCE_MINUS_ONE, error("tarkastus.uusintapesinta_tunniste"));
		}
		
		public void test___pesinnan_jarjestysnro___6() throws Exception {
			column("tarkastus.vuosi").setValue("2010");
			column("tarkastus.pesiva_laji").setValue("FICHYP");
			column("tarkastus.uusintapesinta_jarjestysnro").setValue("1");
			column("tarkastus.uusintapesinta_tunniste").setValue("valkopunarenkainen pari");
			assertEquals("VALKOPUNARENKAINEN PARI", column("tarkastus.uusintapesinta_tunniste").getValue());
			column("kaynti.1.pesinnan_vaihe").setValue("P1");
			validator.validate();
			assertEquals(PesakorttiConst.NESTING_SEQUENCE_USED_MORE_THAN_ONCE, error("tarkastus.uusintapesinta_jarjestysnro"));
			assertEquals(PesakorttiConst.NESTING_SEQUENCE_USED_MORE_THAN_ONCE, error("tarkastus.uusintapesinta_tunniste"));
		}
		
		public void test___tree_measurements_can_be_given_only_for_nests_that_are_in_tree() throws Exception {
			column("pesa.pesa_sijainti").setValue("1");
			column("tarkastus.puu_korkeus").setValue("10");
			column("tarkastus.pesa_etaisyys_runko").setValue("10");
			column("tarkastus.pesa_korkeus").setValue("0,4");
			validator.validate();
			assertEquals(PesakorttiConst.TREE_INFO_CAN_NOT_BE_GIVEN_FOR_THIS_TYPE_OF_NEST, error("tarkastus.puu_korkeus"));
			assertEquals(PesakorttiConst.TREE_INFO_CAN_NOT_BE_GIVEN_FOR_THIS_TYPE_OF_NEST, error("tarkastus.pesa_etaisyys_runko"));
			assertEquals(null, error("tarkastus.pesa_korkeus"));
		}
		
		public void test___nest_can_t__be_higher_than_the_tree() throws Exception {
			column("pesa.pesa_sijainti").setValue("13");
			column("tarkastus.puu_korkeus").setValue("10");
			column("tarkastus.pesa_korkeus").setValue("15");
			validator.validate();
			assertEquals(PesakorttiConst.PESA_CAN_NO_BE_HIGHER_THAN_TREE_HEIGHT, error("tarkastus.puu_korkeus"));
			assertEquals(PesakorttiConst.PESA_CAN_NO_BE_HIGHER_THAN_TREE_HEIGHT, error("tarkastus.pesa_korkeus"));
		}
		
		public void test___nest_can_t__be_higher_than_the_tree_2() throws Exception {
			column("pesa.pesa_sijainti").setValue("13");
			column("tarkastus.puu_korkeus").setValue("10");
			column("tarkastus.pesa_korkeus").setValue("10");
			validator.validate();
			assertEquals(null, error("tarkastus.puu_korkeus"));
			assertEquals(null, error("tarkastus.pesa_korkeus"));
		}
		
		public void test___biotope_area__can_t__be_given__if___biotope__not_given() throws Exception {
			column("tarkastus.pesa_biotooppi").setValue("20");
			column("tarkastus.pesa_biotooppi_ala").setValue("2");
			validator.validate();
			assertEquals(null, error("tarkastus.pesa_biotooppi"));
			assertEquals(null, error("tarkastus.pesa_biotooppi_ala"));
		}
		
		public void test___biotope_area__can_t__be_given__if___biotope__not_given_2() throws Exception {
			column("tarkastus.pesa_biotooppi").setValue("");
			column("tarkastus.pesa_biotooppi_ala").setValue("2");
			validator.validate();
			assertEquals(null, error("tarkastus.pesa_biotooppi"));
			assertEquals(null, error("tarkastus.pesa_biotooppi_ala"));
			assertEquals(PesakorttiConst.BIOTOPE_AREA_CAN_T_BE_GIVEN_IF_BIOTOPE_NOT_GIVEN, warning("tarkastus.pesa_biotooppi_ala"));
		}
		
		public void test__warning_for_high__colony_size() throws Exception {
			column("tarkastus.yhdyskunnassa").setValue("K");
			column("tarkastus.yhdyskunta_koko").setValue("200");
			validator.validate();
			assertEquals(null, error("tarkastus.yhdyskunta_koko"));
			assertEquals(null, warning("tarkastus.yhdyskunta_koko"));
		}
		
		public void test__warning_for_high__colony_size_2() throws Exception {
			column("tarkastus.yhdyskunnassa").setValue("K");
			column("tarkastus.yhdyskunta_koko").setValue("1001");
			validator.validate();
			assertEquals(null, error("tarkastus.yhdyskunta_koko"));
			assertEquals(_Validator.ARE_YOU_SURE, warning("tarkastus.yhdyskunta_koko"));
		}
		
		public void test__colony_info_can_be_given_only_if_nest_is_in_colony() throws Exception {
			column("tarkastus.yhdyskunnassa").setValue("");
			column("tarkastus.yhdyskunta_laji").setValue("FICHYP");
			column("tarkastus.yhdyskunta_koko").setValue("5");
			validator.validate();
			assertEquals(PesakorttiConst.COLONY_INFO_CAN_NOT_BE_GIVEN_IF_NOT_REPORTED_TO_BE_IN_COLONY, error("tarkastus.yhdyskunta_laji"));
			assertEquals(PesakorttiConst.COLONY_INFO_CAN_NOT_BE_GIVEN_IF_NOT_REPORTED_TO_BE_IN_COLONY, error("tarkastus.yhdyskunta_koko"));
		}
		
		public void test__colony_info_can_be_given_only_if_nest_is_in_colony_2() throws Exception {
			column("tarkastus.yhdyskunnassa").setValue("E");
			column("tarkastus.yhdyskunta_laji").setValue("FICHYP");
			column("tarkastus.yhdyskunta_koko").setValue("5");
			validator.validate();
			assertEquals(PesakorttiConst.COLONY_INFO_CAN_NOT_BE_GIVEN_IF_NOT_REPORTED_TO_BE_IN_COLONY, error("tarkastus.yhdyskunta_laji"));
			assertEquals(PesakorttiConst.COLONY_INFO_CAN_NOT_BE_GIVEN_IF_NOT_REPORTED_TO_BE_IN_COLONY, error("tarkastus.yhdyskunta_koko"));
		}
		
		private static Map<String, Rajat> pesyekoot() {
			Map<String, Rajat> pesyekoot = new HashMap<>();
			pesyekoot.put("FICHYP", new Rajat("FICHYP", 8, 1.0));
			pesyekoot.put("PANHAL", new Rajat("PANHAL", 4, 2.5));
			return pesyekoot;
		}
		private static final Map<String, Rajat> PESYEKOOT = pesyekoot();
		
		public void test__maksimipesimiskoko() throws Exception {
			column("tarkastus.pesiva_laji").setValue("FICHYP");
			column("kaynti.1.lkm_munat").setValue("30");
			column("kaynti.1.pesinnan_vaihe").setValue("P1");
			validator.validate();
			assertEquals(PesakorttiConst.MAX_PESYEKOKO_YLITTYNYT, warning(PesakorttiConst.COUNTS));
		}
		
		public void test__maksimipesimiskoko_2() throws Exception {
			column("tarkastus.pesiva_laji").setValue("FICHYP");
			column("kaynti.1.lkm_munat").setValue("8");
			column("kaynti.1.pesinnan_vaihe").setValue("P1");
			validator.validate();
			assertEquals(null, warning(PesakorttiConst.COUNTS));
		}
		
		public void test__maksimipesimiskoko_3() throws Exception {
			column("tarkastus.pesiva_laji").setValue("FICHYP");
			column("kaynti.1.lkm_munat").setValue("4");
			column("kaynti.1.pesinnan_vaihe").setValue("P1");
			validator.validate();
			assertEquals(null, warning(PesakorttiConst.COUNTS));
		}
		
		public void test__maksimi_munintanopeus_1() throws Exception {
			column("tarkastus.pesiva_laji").setValue("FICHYP"); // 1.0 munaa päivässä max
			
			column("kaynti.1.lkm_munat").setValue("0");
			column("kaynti.1.pvm").setValue("1.1.2000");
			
			column("kaynti.2.lkm_munat").setValue("1");
			column("kaynti.2.pvm").setValue("1.1.2000");
			
			column("kaynti.3.lkm_munat").setValue("3");
			column("kaynti.3.pvm").setValue("1.1.2000");
			
			column("kaynti.4.lkm_munat").setValue("4");
			column("kaynti.4.pvm").setValue("5.1.2000");
			
			column("kaynti.5.lkm_munat").setValue("7");
			column("kaynti.5.pvm").setValue("10.1.2000");
			
			column("kaynti.6.lkm_munat").setValue("14");
			column("kaynti.6.pvm").setValue("15.1.2000");
			
			validator.validate();
			assertEquals(null, warning("kaynti.1.lkm_munat"));
			assertEquals(null, warning("kaynti.2.lkm_munat"));
			assertEquals(PesakorttiConst.MAX_PESINTANOPEUS_YLITTYNYT, warning("kaynti.3.lkm_munat"));
			assertEquals(null, warning("kaynti.4.lkm_munat"));
			assertEquals(null, warning("kaynti.5.lkm_munat"));
			assertEquals(PesakorttiConst.MAX_PESINTANOPEUS_YLITTYNYT, warning("kaynti.6.lkm_munat"));
		}
		
		public void test__maksimi_munintanopeus_2() throws Exception {
			column("tarkastus.pesiva_laji").setValue("FICHYP");
			
			column("kaynti.1.lkm_munat").setValue("2");
			column("kaynti.1.pvm").setValue("30.12.2000");
			
			column("kaynti.2.lkm_munat").setValue("3");
			column("kaynti.2.pvm").setValue("1.1.2001");
			
			validator.validate();
			assertEquals(null, warning("kaynti.1.lkm_munat"));
			assertEquals(null, warning("kaynti.2.lkm_munat"));
		}
		
		public void test__maksimi_munintanopeus_3() throws Exception {
			column("tarkastus.pesiva_laji").setValue("PANHAL"); // 2.5 munintaa päivässa max
			
			column("kaynti.1.lkm_munat").setValue("1");
			column("kaynti.1.pvm").setValue("1.1.2000");
			
			column("kaynti.2.lkm_munat").setValue("2");
			column("kaynti.2.pvm").setValue("2.1.2000");
			
			column("kaynti.3.lkm_munat").setValue("3");
			column("kaynti.3.pvm").setValue("5.1.2000");
			
			validator.validate();
			
			assertEquals(null, warning("kaynti.1.lkm_munat"));
			assertEquals(PesakorttiConst.MAX_PESINTANOPEUS_YLITTYNYT, warning("kaynti.2.lkm_munat"));
			assertEquals(null, warning("kaynti.3.lkm_munat"));
		}
		
		public void test__maksimi_munintanopeus_5() throws Exception {
			column("tarkastus.pesiva_laji").setValue("PANHAL"); // 2.5 munintaa päivässa max
			
			column("kaynti.1.lkm_munat").setValue("1");
			column("kaynti.1.pvm").setValue("1.1.2000");
			
			column("kaynti.2.lkm_munat").setValue("4");
			column("kaynti.2.pvm").setValue("9.1.2000");
			
			validator.validate();
			
			assertEquals(null, warning("kaynti.1.lkm_munat"));
			assertEquals(null, warning("kaynti.2.lkm_munat"));
		}
		
		public void test__maksimi_munintanopeus_6() throws Exception {
			column("tarkastus.pesiva_laji").setValue("PANHAL"); // 2.5 munintaa päivässa max
			
			column("kaynti.1.lkm_munat").setValue("1");
			column("kaynti.1.pvm").setValue("1.1.2000");
			
			column("kaynti.2.lkm_munat").setValue("4");
			column("kaynti.2.pvm").setValue("8.1.2000");
			
			validator.validate();
			
			assertEquals(null, warning("kaynti.1.lkm_munat"));
			assertEquals(PesakorttiConst.MAX_PESINTANOPEUS_YLITTYNYT, warning("kaynti.2.lkm_munat"));
		}
		
		public void test__maksimi_munintanopeus_7() throws Exception {
			column("tarkastus.pesiva_laji").setValue("FICHYP"); // 1.0 munintaa päivässa max
			
			column("kaynti.1.pvm").setValue("1.1.2000");
			column("kaynti.1.lkm_munat").setValue("1");
			
			column("kaynti.2.pvm").setValue("2.1.2000");
			
			column("kaynti.3.pvm").setValue("3.1.2000");
			
			column("kaynti.4.pvm").setValue("4.1.2000");
			column("kaynti.4.lkm_poikaset").setValue("4");
			
			validator.validate();
			
			assertEquals(null, warning("kaynti.1.lkm_munat"));
			assertEquals(null, warning("kaynti.2.lkm_munat"));
			assertEquals(null, warning("kaynti.3.lkm_munat"));
			assertEquals(null, warning("kaynti.4.lkm_munat"));
			
		}
		
		public void test__if_kayntipvm_given_tarkkuus_must_be_given() throws Exception {
			column("tarkastus.vuosi").setValue("2010");
			column("kaynti.1.pvm").setValue("1.1.2010");
			validator.validate();
			assertEquals(_Validator.CAN_NOT_BE_NULL, error("kaynti.1.pvm_tarkkuus"));
		}
		
		public void test__if_kayntipvm_given_tarkkuus_must_be_given_2() throws Exception {
			column("kaynti.1.pvm").setValue("1.1.2010");
			column("kaynti.1.pvm_tarkkuus").setValue("1");
			validator.validate();
			assertEquals(null, error("kaynti.1.pvm"));
			assertEquals(null, error("kaynti.1.pvm_tarkkuus"));
		}
		
		public void test__if_kaynti__values_given__pesinnan_vaihe_and_date_are_required() throws Exception {
			column("tarkastus.vuosi").setValue("2010");
			column("kaynti.1.lkm_munat").setValue("1");
			validator.validate();
			assertEquals(_Validator.CAN_NOT_BE_NULL, error("kaynti.1.pvm"));
			assertEquals(_Validator.CAN_NOT_BE_NULL, error("kaynti.1.pesinnan_vaihe"));
		}
		
		public void test__if_kaynti__values_given__pesinnan_vaihe_and_date_are_required_2() throws Exception {
			column("kaynti.1.tarkastus").setValue("1234234");
			validator.validate();
			assertEquals(null, error("kaynti.1.pvm"));
			assertEquals(null, error("kaynti.1.pesinnan_vaihe"));
		}
		
		public void test__uusintapesinta_jrno_is_required() throws Exception {
			column("tarkastus.uusintapesinta_jarjestysnro").setValue("");
			validator.validate();
			assertEquals(_Validator.CAN_NOT_BE_NULL, error("tarkastus.uusintapesinta_jarjestysnro"));
		}
		
		public void test__uusintapesinta_jrno_is_required_2() throws Exception {
			column("tarkastus.uusintapesinta_jarjestysnro").setValue("0");
			validator.validate();
			assertEquals(null, error("tarkastus.uusintapesinta_jarjestysnro"));
		}
		
		public void test____tunturit_1() throws Exception {
			column("tarkastus.pesa_biotooppi").setValue("43");
			column("pesa.yht_leveys").setValue("6700000");
			column("pesa.koord_ilm_tyyppi").setValue("Y");
			validator.validate();
			assertEquals(null, error("tarkastus.pesa_biotooppi"));
			assertEquals(PesakorttiConst.TUNTURI_BIOTOOPPI_LIIAN_ETELASSA, warning("tarkastus.pesa_biotooppi"));
		}
		
		public void test____tunturit_2() throws Exception {
			column("tarkastus.pesa_biotooppi").setValue("43");
			column("pesa.yht_leveys").setValue("7300000");
			column("pesa.koord_ilm_tyyppi").setValue("Y");
			validator.validate();
			assertEquals(null, error("tarkastus.pesa_biotooppi"));
			assertEquals(null, warning("tarkastus.pesa_biotooppi"));
		}
		
		public void test____tunturit_3() throws Exception {
			column("tarkastus.pesa_biotooppi").setValue("40");
			column("pesa.yht_leveys").setValue("6900000");
			column("pesa.koord_ilm_tyyppi").setValue("Y");
			validator.validate();
			assertEquals(null, error("tarkastus.pesa_biotooppi"));
			assertEquals(PesakorttiConst.TUNTURI_BIOTOOPPI_LIIAN_ETELASSA, warning("tarkastus.pesa_biotooppi"));
		}
		
		public void test____tunturit_4() throws Exception {
			column("tarkastus.pesa_biotooppi").setValue("40");
			column("pesa.ast_leveys").setValue("659000");
			column("pesa.koord_ilm_tyyppi").setValue("A");
			validator.validate();
			assertEquals(null, error("tarkastus.pesa_biotooppi"));
			assertEquals(null, warning("tarkastus.pesa_biotooppi"));
		}
		
		public void test____tunturit_5() throws Exception {
			column("tarkastus.pesa_biotooppi").setValue("40");
			column("pesa.ast_leveys").setValue("654000");
			column("pesa.koord_ilm_tyyppi").setValue("A");
			validator.validate();
			assertEquals(null, error("tarkastus.pesa_biotooppi"));
			assertEquals(PesakorttiConst.TUNTURI_BIOTOOPPI_LIIAN_ETELASSA, warning("tarkastus.pesa_biotooppi"));
		}
		
		
		public void test____pesinta_vuosi___kaynti_vuosi() throws Exception {
			column("tarkastus.vuosi").setValue("2000");
			column("kaynti.1.pvm").setValue("..2000");
			column("kaynti.2.pvm").setValue("1.1.2000");
			column("kaynti.3.pvm").setValue("1.1.2001");
			column("kaynti.4.pvm").setValue("1.1.1999");
			column("kaynti.5.pvm").setValue("1.1.1998");
			validator.validate();
			assertEquals(null, error("tarkastus.vuosi"));
			assertEquals(_Validator.INVALID_DATE, error("kaynti.1.pvm"));
			assertEquals(null, error("kaynti.2.pvm"));
			assertEquals(PesakorttiConst.ALL_KAYNTI_MUST_BE_FROM_TARK_VUOSI_OR_ONE_YEAR_BEFORE, error("kaynti.3.pvm"));
			assertEquals(PesakorttiConst.INVALID_KAYNTI_ORDER, error("kaynti.4.pvm"));
			assertEquals(PesakorttiConst.ALL_KAYNTI_MUST_BE_FROM_TARK_VUOSI_OR_ONE_YEAR_BEFORE, error("kaynti.5.pvm"));
		}
		
		public void test____kaynti_pvm_jarjestys() throws Exception {
			column("tarkastus.vuosi").setValue("2000");
			column("kaynti.1.pvm").setValue("5.1.2000");
			column("kaynti.2.pvm").setValue("5.1.2000");
			column("kaynti.3.pvm").setValue("6.1.2000");
			column("kaynti.4.pvm").setValue("5.1.2000");
			column("kaynti.5.pvm").setValue("6.1.2000");
			column("kaynti.6.pvm").setValue("7.1.2000");
			column("kaynti.7.pvm").setValue("6.1.2000");
			validator.validate();
			assertEquals(null, error("tarkastus.vuosi"));
			assertEquals(null, error("kaynti.1.pvm"));
			assertEquals(null, error("kaynti.2.pvm"));
			assertEquals(null, error("kaynti.3.pvm"));
			assertEquals(PesakorttiConst.INVALID_KAYNTI_ORDER, error("kaynti.4.pvm"));
			assertEquals(null, error("kaynti.5.pvm"));
			assertEquals(null, error("kaynti.6.pvm"));
			assertEquals(PesakorttiConst.INVALID_KAYNTI_ORDER, error("kaynti.7.pvm"));
		}
		
		public void test____kaynti_pvm_jarjestys_with_hours() throws Exception {
			column("tarkastus.vuosi").setValue("2000");
			
			column("kaynti.1.pvm").setValue("1.1.2000");
			column("kaynti.2.pvm").setValue("1.1.2000");
			column("kaynti.3.pvm").setValue("1.1.2000");
			
			column("kaynti.1.klo_h").setValue("1");
			column("kaynti.2.klo_h").setValue("2");
			column("kaynti.3.klo_h").setValue("3");
			
			validator.validate();
			assertEquals(null, error("tarkastus.vuosi"));
			assertEquals(null, error("kaynti.1.pvm"));
			assertEquals(null, error("kaynti.2.pvm"));
			assertEquals(null, error("kaynti.3.pvm"));
		}
		
		public void test____kaynti_pvm_jarjestys_with_hours_2() throws Exception {
			column("tarkastus.vuosi").setValue("2000");
			
			column("kaynti.1.pvm").setValue("1.1.2000");
			column("kaynti.2.pvm").setValue("1.1.2000");
			column("kaynti.3.pvm").setValue("1.1.2000");
			
			column("kaynti.1.klo_h").setValue("3");
			column("kaynti.2.klo_h").setValue("1");
			column("kaynti.3.klo_h").setValue("2");
			
			validator.validate();
			assertEquals(null, error("tarkastus.vuosi"));
			assertEquals(null, error("kaynti.1.pvm"));
			assertEquals(PesakorttiConst.INVALID_KAYNTI_ORDER, error("kaynti.2.pvm"));
			assertEquals(PesakorttiConst.INVALID_KAYNTI_ORDER, error("kaynti.3.pvm"));
		}
		
		public void test____kaynti_pvm_jarjestys_with_hours_3() throws Exception {
			column("tarkastus.vuosi").setValue("2000");
			
			column("kaynti.1.pvm").setValue("1.1.2000");
			column("kaynti.2.pvm").setValue("1.1.2000");
			column("kaynti.3.pvm").setValue("1.1.2000");
			
			column("kaynti.1.klo_h").setValue("2");
			column("kaynti.2.klo_h").setValue("2");
			column("kaynti.3.klo_h").setValue("1");
			
			validator.validate();
			assertEquals(null, error("tarkastus.vuosi"));
			assertEquals(null, error("kaynti.1.pvm"));
			assertEquals(null, error("kaynti.2.pvm"));
			assertEquals(PesakorttiConst.INVALID_KAYNTI_ORDER, error("kaynti.3.pvm"));
		}
		
		public void test____kaynti_pvm_jarjestys_with_hours_and_minutes() throws Exception {
			column("tarkastus.vuosi").setValue("2000");
			
			column("kaynti.1.pvm").setValue("1.1.2000");
			column("kaynti.2.pvm").setValue("1.1.2000");
			column("kaynti.3.pvm").setValue("1.1.2000");
			
			column("kaynti.1.klo_h").setValue("1");
			column("kaynti.1.klo_min").setValue("00");
			
			column("kaynti.2.klo_h").setValue("1");
			column("kaynti.2.klo_min").setValue("30");
			
			column("kaynti.3.klo_h").setValue("1");
			column("kaynti.3.klo_min").setValue("50");
			
			validator.validate();
			assertEquals(null, error("tarkastus.vuosi"));
			assertEquals(null, error("kaynti.1.pvm"));
			assertEquals(null, error("kaynti.2.pvm"));
			assertEquals(null, error("kaynti.3.pvm"));
		}
		
		public void test____kaynti_pvm_jarjestys_with_hours_and_minutes_2() throws Exception {
			column("tarkastus.vuosi").setValue("2000");
			
			column("kaynti.1.pvm").setValue("1.1.2000");
			column("kaynti.2.pvm").setValue("1.1.2000");
			column("kaynti.3.pvm").setValue("1.1.2000");
			
			column("kaynti.1.klo_h").setValue("1");
			column("kaynti.1.klo_min").setValue("00");
			
			column("kaynti.2.klo_h").setValue("1");
			column("kaynti.2.klo_min").setValue("30");
			
			column("kaynti.3.klo_h").setValue("1");
			column("kaynti.3.klo_min").setValue("10");
			
			validator.validate();
			assertEquals(null, error("tarkastus.vuosi"));
			assertEquals(null, error("kaynti.1.pvm"));
			assertEquals(null, error("kaynti.2.pvm"));
			assertEquals(PesakorttiConst.INVALID_KAYNTI_ORDER, error("kaynti.3.pvm"));
		}
		
		public void test___pesinnan_vaihe_assosiacted_selection() throws Exception {
			column("kaynti.1.pesinnan_vaihe").setValue("");
			column("kaynti.2.pesinnan_vaihe").setValue("R9");
			column("kaynti.3.pesinnan_vaihe").setValue("xx");
			validator.validate();
			assertEquals(null, error("kaynti.1.pesinnan_vaihe"));
			assertEquals(null, error("kaynti.2.pesinnan_vaihe"));
			assertEquals(_Validator.ASSOSIATED_SELECTION_DOES_NOT_CONTAIN_THE_VALUE, error("kaynti.3.pesinnan_vaihe"));
		}
		
		public void test___validating_hours_and_minutes() throws Exception {
			column("tarkastus.vuosi").setValue("2000");
			column("kaynti.1.klo_h").setValue("5");
			column("kaynti.1.klo_min").setValue("");
			validator.validate();
			assertEquals(null, error("kaynti.1.klo_h"));
			assertEquals(null, error("kaynti.1.klo_min"));
		}
		
		public void test___validating_hours_and_minutes_2() throws Exception {
			column("tarkastus.vuosi").setValue("2000");
			column("kaynti.1.klo_h").setValue("");
			column("kaynti.1.klo_min").setValue("5");
			validator.validate();
			assertEquals(_Validator.CAN_NOT_BE_NULL, error("kaynti.1.klo_h"));
			assertEquals(null, error("kaynti.1.klo_min"));
		}
		
		public void test___validating_hours_and_minutes_3() throws Exception {
			column("tarkastus.vuosi").setValue("2000");
			column("kaynti.1.klo_h").setValue("0");
			column("kaynti.1.klo_min").setValue("0");
			validator.validate();
			assertEquals(null, error("kaynti.1.klo_h"));
			assertEquals(null, error("kaynti.1.klo_min"));
		}
		
		public void test___validating_hours_and_minutes_4() throws Exception {
			column("tarkastus.vuosi").setValue("2000");
			column("kaynti.1.klo_h").setValue("-1");
			column("kaynti.1.klo_min").setValue("-1");
			validator.validate();
			assertEquals(_Validator.TOO_SMALL, error("kaynti.1.klo_h"));
			assertEquals(_Validator.TOO_SMALL, error("kaynti.1.klo_min"));
		}
		
		public void test___validating_hours_and_minutes_5() throws Exception {
			column("tarkastus.vuosi").setValue("2000");
			column("kaynti.1.klo_h").setValue("23");
			column("kaynti.1.klo_min").setValue("59");
			validator.validate();
			assertEquals(null, error("kaynti.1.klo_h"));
			assertEquals(null, error("kaynti.1.klo_min"));
		}
		
		public void test___validating_hours_and_minutes_6() throws Exception {
			column("tarkastus.vuosi").setValue("2000");
			column("kaynti.1.klo_h").setValue("24");
			column("kaynti.1.klo_min").setValue("60");
			validator.validate();
			assertEquals(_Validator.TOO_LARGE, error("kaynti.1.klo_h"));
			assertEquals(_Validator.TOO_LARGE, error("kaynti.1.klo_min"));
		}
		
		public void test___validating__eggs__youngs__sequence_1() throws Exception {
			column("tarkastus.vuosi").setValue("2011");
			column("kaynti.1.pvm").setValue("1.1.2011");
			column("kaynti.2.pvm").setValue("2.1.2011");
			
			column("kaynti.1.lkm_munat").setValue("4");
			column("kaynti.2.lkm_poikaset").setValue("4");
			
			validator.validate();
			assertEquals(null, warning(PesakorttiConst.COUNTS));
		}
		
		//		public void test___validating__eggs__youngs__sequence_2() throws Exception {
		//			column("tarkastus.vuosi").setValue("2011");
		//			column("kaynti.1.pvm").setValue("1.1.2011");
		//			column("kaynti.2.pvm").setValue("2.1.2011");
		//
		//			column("kaynti.1.lkm_poikaset").setValue("4");
		//			column("kaynti.2.lkm_munat").setValue("4");
		//
		//			validator.validate();
		//			assertEquals(PesakorttiConst.EPATAVALLINEN_POIKASTEN_MUNIEN_LKM_VAIHTELU, warning(PesakorttiConst.COUNTS));
		//		}
		
		public void test___validating__eggs__youngs__sequence_3() throws Exception {
			column("tarkastus.vuosi").setValue("2011");
			column("kaynti.1.pvm").setValue("1.1.2011");
			column("kaynti.2.pvm").setValue("2.1.2011");
			
			column("kaynti.1.lkm_munat").setValue("4");
			column("kaynti.1.lkm_poikaset").setValue("4");
			column("kaynti.2.lkm_poikaset").setValue("4");
			
			validator.validate();
			assertEquals(null, warning(PesakorttiConst.COUNTS));
		}
		
		//		public void test___validating__eggs__youngs__sequence_4() throws Exception {
		//			column("tarkastus.vuosi").setValue("2011");
		//			column("kaynti.1.pvm").setValue("1.1.2011");
		//			column("kaynti.2.pvm").setValue("2.1.2011");
		//
		//			column("kaynti.1.lkm_munat").setValue("4");
		//			column("kaynti.1.lkm_poikaset").setValue("4");
		//			column("kaynti.2.lkm_munat").setValue("4");
		//
		//			validator.validate();
		//			assertEquals(PesakorttiConst.EPATAVALLINEN_POIKASTEN_MUNIEN_LKM_VAIHTELU, warning(PesakorttiConst.COUNTS));
		//		}
		
		public void test__nesting_advances_to_wrong_direction() throws Exception {
			column("tarkastus.vuosi").setValue("2011");
			column("kaynti.1.pvm").setValue("1.1.2011");
			column("kaynti.2.pvm").setValue("2.1.2011");
			column("kaynti.3.pvm").setValue("3.1.2011");
			column("kaynti.4.pvm").setValue("4.1.2011");
			column("kaynti.5.pvm").setValue("5.1.2011");
			column("kaynti.6.pvm").setValue("6.1.2011");
			
			column("kaynti.1.pesinnan_vaihe").setValue("R1");
			column("kaynti.2.pesinnan_vaihe").setValue("E1");
			column("kaynti.3.pesinnan_vaihe").setValue("M1");
			column("kaynti.4.pesinnan_vaihe").setValue("P1");
			column("kaynti.5.pesinnan_vaihe").setValue("T1");
			column("kaynti.6.pesinnan_vaihe").setValue("J1");
			
			validator.validate();
			assertEquals(null, warning("kaynti.1.pesinnan_vaihe"));
			assertEquals(null, warning("kaynti.2.pesinnan_vaihe"));
			assertEquals(null, warning("kaynti.3.pesinnan_vaihe"));
			assertEquals(null, warning("kaynti.4.pesinnan_vaihe"));
			assertEquals(null, warning("kaynti.5.pesinnan_vaihe"));
			assertEquals(null, warning("kaynti.6.pesinnan_vaihe"));
		}
		
		public void test__nesting_advances_to_wrong_direction_2() throws Exception {
			column("tarkastus.vuosi").setValue("2011");
			column("kaynti.1.pvm").setValue("1.1.2011");
			column("kaynti.2.pvm").setValue("2.1.2011");
			column("kaynti.3.pvm").setValue("3.1.2011");
			column("kaynti.4.pvm").setValue("4.1.2011");
			column("kaynti.5.pvm").setValue("5.1.2011");
			column("kaynti.6.pvm").setValue("6.1.2011");
			
			column("kaynti.1.pesinnan_vaihe").setValue("R1");
			column("kaynti.2.pesinnan_vaihe").setValue("E1");
			column("kaynti.3.pesinnan_vaihe").setValue("M1");
			column("kaynti.4.pesinnan_vaihe").setValue("E1");
			column("kaynti.5.pesinnan_vaihe").setValue("P1");
			column("kaynti.6.pesinnan_vaihe").setValue("R1");
			
			validator.validate();
			assertEquals(null,warning("kaynti.1.pesinnan_vaihe"));
			assertEquals(null, warning("kaynti.2.pesinnan_vaihe"));
			assertEquals(null, warning("kaynti.3.pesinnan_vaihe"));
			assertEquals(null, warning("kaynti.4.pesinnan_vaihe")); // emovaihe saa esiintyä missä välissä vain
			assertEquals(null, warning("kaynti.5.pesinnan_vaihe"));
			assertEquals(PesakorttiConst.PESINTA_ETENEE_VAARAAN_SUUNTAAN, warning("kaynti.6.pesinnan_vaihe"));
		}
		
		public void test__nesting_advances_to_wrong_direction_3() throws Exception {
			column("tarkastus.vuosi").setValue("2011");
			column("kaynti.1.pvm").setValue("1.1.2011");
			column("kaynti.2.pvm").setValue("2.1.2011");
			column("kaynti.3.pvm").setValue("3.1.2011");
			column("kaynti.4.pvm").setValue("4.1.2011");
			column("kaynti.5.pvm").setValue("5.1.2011");
			column("kaynti.6.pvm").setValue("6.1.2011");
			
			column("kaynti.1.pesinnan_vaihe").setValue("M1");
			column("kaynti.2.ika_munat").setValue("5");
			column("kaynti.3.pesinnan_vaihe").setValue("M1");
			column("kaynti.4.ika_poikaset").setValue("10");
			column("kaynti.5.pesinnan_vaihe").setValue("M1");
			
			validator.validate();
			assertEquals(null, warning("kaynti.1.pesinnan_vaihe"));
			assertEquals(null, warning("kaynti.2.pesinnan_vaihe"));
			assertEquals(null, warning("kaynti.3.pesinnan_vaihe"));
			assertEquals(null, warning("kaynti.4.pesinnan_vaihe"));
			assertEquals(PesakorttiConst.PESINTA_ETENEE_VAARAAN_SUUNTAAN, warning("kaynti.5.pesinnan_vaihe"));
		}
		
		public void test__nesting_advances__p8_and_p9_can_follow__() throws Exception {
			column("tarkastus.vuosi").setValue("2011");
			column("kaynti.1.pvm").setValue("1.1.2011");
			column("kaynti.2.pvm").setValue("2.1.2011");
			column("kaynti.3.pvm").setValue("3.1.2011");
			column("kaynti.4.pvm").setValue("4.1.2011");
			column("kaynti.5.pvm").setValue("5.1.2011");
			column("kaynti.6.pvm").setValue("6.1.2011");
			
			column("kaynti.1.pesinnan_vaihe").setValue("P1");
			column("kaynti.2.ika_poikaset").setValue("10");
			column("kaynti.3.pesinnan_vaihe").setValue("J1");
			column("kaynti.4.pesinnan_vaihe").setValue("P8");
			column("kaynti.5.pesinnan_vaihe").setValue("P6");
			column("kaynti.6.pesinnan_vaihe").setValue("J1");
			
			validator.validate();
			
			assertEquals(null, warning("kaynti.1.pesinnan_vaihe"));
			assertEquals(null, warning("kaynti.2.pesinnan_vaihe"));
			assertEquals(null, warning("kaynti.3.pesinnan_vaihe"));
			assertEquals(null, warning("kaynti.4.pesinnan_vaihe"));
			assertEquals(PesakorttiConst.PESINTA_ETENEE_VAARAAN_SUUNTAAN, warning("kaynti.5.pesinnan_vaihe"));
			assertEquals(null, warning("kaynti.6.pesinnan_vaihe"));
		}
		
		public void test__counts__rakennusvaihe() throws Exception {
			column("tarkastus.vuosi").setValue("2011");
			column("kaynti.1.pvm").setValue("1.1.2011");
			column("kaynti.1.pesinnan_vaihe").setValue("R9");
			column("kaynti.1.lkm_munat").setValue("-5");
			column("kaynti.1.lkm_munat_kuolleet").setValue("4");
			column("kaynti.1.lkm_poikaset").setValue("0");
			column("kaynti.1.lkm_poikaset_kuolleet").setValue("4");
			
			validator.validate();
			
			assertEquals(_Validator.MUST_BE_POSITIVE_NUMBER, error("kaynti.1.lkm_munat"));
			assertEquals(_Validator.MUST_BE_NULL_OR_ZERO, error("kaynti.1.lkm_munat_kuolleet"));
			assertEquals(null, error("kaynti.1.lkm_poikaset"));
			assertEquals(_Validator.MUST_BE_NULL_OR_ZERO, error("kaynti.1.lkm_poikaset_kuolleet"));
		}
		
		public void test__counts__m0_ei_saa_olla_lukuja() throws Exception {
			column("tarkastus.vuosi").setValue("2011");
			column("kaynti.1.pvm").setValue("1.1.2011");
			column("kaynti.1.pesinnan_vaihe").setValue("M0");
			column("kaynti.1.lkm_munat").setValue("5");
			
			validator.validate();
			assertEquals(_Validator.MUST_BE_NULL_OR_ZERO, error("kaynti.1.lkm_munat"));
		}
		
		public void test__counts__emovaihe() throws Exception {
			column("tarkastus.vuosi").setValue("2011");
			column("kaynti.1.pvm").setValue("1.1.2011");
			column("kaynti.1.pesinnan_vaihe").setValue("E1");
			column("kaynti.1.lkm_munat").setValue("0");
			column("kaynti.1.lkm_munat_kuolleet").setValue("4");
			column("kaynti.1.lkm_poikaset").setValue("0");
			column("kaynti.1.lkm_poikaset_kuolleet").setValue("3");
			
			validator.validate();
			
			assertEquals(null, error("kaynti.1.lkm_munat"));
			assertEquals(_Validator.MUST_BE_NULL_OR_ZERO, error("kaynti.1.lkm_munat_kuolleet"));
			assertEquals(null, error("kaynti.1.lkm_poikaset"));
			assertEquals(_Validator.MUST_BE_NULL_OR_ZERO, error("kaynti.1.lkm_poikaset_kuolleet"));
		}
		
		public void test__counts__munavaihe() throws Exception {
			column("tarkastus.vuosi").setValue("2011");
			column("kaynti.1.pvm").setValue("1.1.2011");
			column("kaynti.1.pesinnan_vaihe").setValue("M1");
			column("kaynti.1.lkm_munat").setValue("3");
			column("kaynti.1.lkm_munat_kuolleet").setValue("3");
			column("kaynti.1.lkm_poikaset").setValue("2");
			column("kaynti.1.lkm_poikaset_kuolleet").setValue("2");
			
			validator.validate();
			
			assertEquals(null, error("kaynti.1.lkm_munat"));
			assertEquals(null, error("kaynti.1.lkm_munat_kuolleet"));
			assertEquals(_Validator.MUST_BE_NULL_OR_ZERO, error("kaynti.1.lkm_poikaset"));
			assertEquals(_Validator.MUST_BE_NULL_OR_ZERO, error("kaynti.1.lkm_poikaset_kuolleet"));
		}
		
		public void test__counts__poikasvaihe() throws Exception {
			column("tarkastus.vuosi").setValue("2011");
			column("kaynti.1.pvm").setValue("1.1.2011");
			column("kaynti.1.ika_poikaset").setValue("10");
			column("kaynti.1.lkm_munat").setValue("3");
			column("kaynti.1.lkm_munat_kuolleet").setValue("3");
			column("kaynti.1.lkm_poikaset").setValue("2");
			column("kaynti.1.lkm_poikaset_kuolleet").setValue("2");
			
			validator.validate();
			
			assertEquals(null, error("kaynti.1.lkm_munat"));
			assertEquals(null, error("kaynti.1.lkm_munat_kuolleet"));
			assertEquals(null, error("kaynti.1.lkm_poikaset"));
			assertEquals(null, error("kaynti.1.lkm_poikaset_kuolleet"));
		}
		
		public void test__does_not_require_species_for_poikanen() throws Exception {
			column("poikanen.1.sukupuoli").setValue("K");
			validator.validate();
			assertNull("Should not have errors", error("poikanen.1.laji"));
		}
		
		public void test__does_not_require_species_for_poikanen_for_kirjekyyhky() throws Exception {
			column("poikanen.1.sukupuoli").setValue("K");
			validator.setKirjekyyhkyValidation(true);
			validator.validate();
			assertNull("Should not have errors", error("poikanen.1.laji"));
		}
		
		// TODO munien/poikasten ikä: jos kasvaa enemmän kuin on ollut eroa käyntien välillä
		// TODO: pesän kunto (pönttö uusittu) vs. pöntön ripustusvuosi
		
	}
	
}
