package fi.hy.pese.merikotka.main.functionality.validation;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.app.functionality.validate.TarkastusValidator;
import fi.hy.pese.framework.main.app.functionality.validate._Validator;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.main.general.data._Row;
import fi.hy.pese.framework.main.general.data._Table;
import fi.hy.pese.merikotka.main.app.MerikotkaConst;
import fi.hy.pese.merikotka.main.db.MerikotkaDAO;
import fi.luomus.commons.utils.CoordinateConverter;
import fi.luomus.commons.utils.CoordinateConverter.CoordinateConversionException;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.Utils;

public class MerikotkaTarkastusValidator extends TarkastusValidator {

	private static final Collection<String>	HAS_NESTLINGS	= Utils.collection("Q", "R", "T", "V");
	private static final Collection<String>	COUNT_GIVEN	= Utils.collection("1", "2", "3", "4", "7", "8", "9");
	private static final String	EI_TARKASTETTU	= "8";
	private static final String	EI_LOYDETTY	= "7";
	private static final String	MERIKOTKA = "HALALB";
	private static final int RADIUS_TO_CHECK_FOR_EXISTING_NESTS_IN_METERS	= 200;
	private final boolean kirjekyyhkyValidations;

	public MerikotkaTarkastusValidator(_Request<_PesaDomainModelRow> request) {
		super(request);
		kirjekyyhkyValidations = false;
	}
	
	public MerikotkaTarkastusValidator(_Request<_PesaDomainModelRow> request, boolean kirjekyyhkyValidationserviceValidationsOnly) {
		super(request);
		this.kirjekyyhkyValidations = kirjekyyhkyValidationserviceValidationsOnly;
	}

	@Override
	public void validate() throws Exception {
		if (kirjekyyhkyValidations) {
			validateSendingTheForm();
			copyAndRemoveError("pesavakio.yht_leveys", "koord_leveys");
			copyAndRemoveError("pesavakio.yht_pituus", "koord_pituus");
			copyAndRemoveError("pesavakio.eur_leveys", "koord_leveys");
			copyAndRemoveError("pesavakio.eur_pituus", "koord_pituus");
			copyAndRemoveError("pesavakio.ast_leveys", "koord_leveys");
			copyAndRemoveError("pesavakio.ast_pituus", "koord_pituus");
		} else {
			super.validate();
			if (hasErrors()) return;
			String action = data.action();
			if (action.equals(Const.FILL_FOR_INSERT)) {
				checkTarkastusVuosiValid_NoTarkastusForTheYear("tark_vuosi", data.get("tark_vuosi"));
				if (hasErrors()) return;
				checkNestIsNotDead();
			} else if (action.equals(Const.UPDATE) || action.equals(Const.INSERT) || action.equals(Const.INSERT_FIRST)) {
				validateSendingTheForm();
			}
		}
	}

	private void checkNestIsNotDead() throws SQLException {
		String pesa_id = data.updateparameters().getPesa().getUniqueNumericIdColumn().getValue();
		if (isDead(pesa_id)) {
			Integer yearNestMarkedDead = MerikotkaDAO.yearNestHasDied(pesa_id, dao);
			Integer entryYear = Integer.valueOf(data.get("tark_vuosi"));
			if (entryYear > yearNestMarkedDead) {
				error("tark_vuosi", MerikotkaConst.PESA_IS_DEAD_CAN_NOT_INSERT);
			}
		}
	}

	private boolean isDead(String pesa_id) throws SQLException {
		Collection<String> deadNests = MerikotkaDAO.deadNests(dao);
		return deadNests.contains(pesa_id);
	}

	private void checkTarkastusVuosiValid_NoTarkastusForTheYear(String tarkVuosiFieldName, String tark_vuosi) throws SQLException {
		checkValidYear(tarkVuosiFieldName, tark_vuosi);
		if (noErrors(tarkVuosiFieldName) && noErrors("pesavakio.pesa_id") && doingInsert()) {
			String pesa_id = data.updateparameters().getColumn("pesavakio.pesa_id").getValue();
			boolean tarkastusAlreadyExists = MerikotkaDAO.checkTarkastusForTheYearExists(pesa_id, tark_vuosi, dao);
			if (tarkastusAlreadyExists) {
				error(tarkVuosiFieldName, CAN_NOT_INSERT_ALREADY_EXISTS);
			}
		}
	}

	private boolean doingInsert() {
		return (data.action().equals(Const.INSERT) || data.action().equals(Const.FILL_FOR_INSERT));
	}

	private void validateSendingTheForm() throws Exception {
		_PesaDomainModelRow params = data.updateparameters();
		String action = data.action();

		if (!action.equals(Const.INSERT_FIRST)) {
			if (!kirjekyyhkyValidations) {
				checkNotNull(params.getColumn("pesavakio.pesa_id"));
			}
		}
		if (action.equals(Const.UPDATE)) {
			checkNotNull(params.getColumn("pesatarkastus.p_tarkastus_id"));
		}

		checkAsUpdateparameters(params);
		rauh_aika_loppu_canBeInTheFuture();

		if (kirjekyyhkyValidations) {
			checkNotNull(params.getColumn("pesatarkastus.tark_vuosi"));
			checkValidYear(params.getColumn("pesatarkastus.tark_vuosi"));
		} else {
			checkTarkastusVuosiValid_NoTarkastusForTheYear("pesatarkastus.tark_vuosi", params.getColumn("pesatarkastus.tark_vuosi").getValue());
		}

		checkReferenceValuesOfNonNullColumns(params, dao);

		if (!kirjekyyhkyValidations) {
			checkNotNull(params.getColumn("vuosi.reviirin_kunta"));
			checkNotNull(params.getColumn("vuosi.reviiri_id"));
			checkNotNull(params.getColumn("pesavakio.helcom"));
		}
		checkNotNull(params.getColumn("vuosi.pesan_kunta"));
		checkNotNull(params.getColumn("pesavakio.pesanimi"));
		checkNotNull(params.getColumn("pesavakio.koord_mittaus"));
		checkNotNull(params.getColumn("pesavakio.koord_tyyppi"));
		checkNotNull(params.getColumn("pesavakio.koord_tark"));
		checkNotNull(params.getColumn("pesamuuttuva.pesa_mit_pvm"));
		checkNotNull(params.getColumn("pesamuuttuva.ymp_mit_pvm"));
		checkNotNull(params.getColumn("pesatarkastus.tark_pvm"));
		checkNotNull(params.getColumn("pesatarkastus.tark_pvm_tark"));
		checkNotNull(params.getColumn("pesatarkastus.tarkastaja1_id"));
		checkNotNull(params.getColumn("pesatarkastus.pesimistulos"));
		checkNotNull(params.getColumn("pesatarkastus.pesimist_tark"));
		checkNotNull(params.getColumn("pesatarkastus.nahdyt_merkit"));
		checkNotNull(params.getColumn("pesatarkastus.tark_tapa"));
		checkNotNull(params.getColumn("pesatarkastus.pesa_kunto"));
		//		if (!kirjekyyhkyValidations) {
		//			checkNotNull(params.getColumn("pesavakio.seur_tarkastaja"));
		//		}

		if (!kirjekyyhkyValidations) {
			warnIfReviirinimiReceivedFromKirjekyyhkyDifferentThanCurrent(params);
		}

		if (firstMeasurementsExists(params)) {
			checkNotNull(params.getColumn("pesatarkastus.mittaus_pvm_1"));
			checkNotNull(params.getColumn("pesatarkastus.mittaaja_id_1"));
		} else {
			checkIsNull(params.getColumn("pesatarkastus.mittaus_pvm_1"));
			checkIsNull(params.getColumn("pesatarkastus.mittaaja_id_1"));
		}
		Set<String> secondMeasurementsForPoikanen = secondMeasurements(params);
		_Column mittaus2Pvm = params.getColumn("pesatarkastus.mittaus_pvm_2");
		_Column mittaaja2 = params.getColumn("pesatarkastus.mittaaja_id_2");
		if (exists(secondMeasurementsForPoikanen)) {
			checkNotNull(mittaus2Pvm);
			checkNotNull(mittaaja2);
			if (notNullNoErrors(mittaus2Pvm)) {
				String mittaus2Vuosi = mittaus2Pvm.getDateValue().getYear();
				if (!params.getColumn("pesatarkastus.tark_vuosi").getValue().equals(mittaus2Vuosi)) {
					error(mittaus2Pvm, MerikotkaConst.TOISEN_MITTAUKSEN_VUOSI_OLTAVA_TARKASTUSVUOSI);
				}
			}
		} else {
			checkIsNull(mittaus2Pvm);
			checkIsNull(mittaaja2);
		}

		if (notNullNoErrors(params.getColumn("pesatarkastus.epaonni_syy"))) {
			checkNotNull(params.getColumn("pesatarkastus.epaonni_tark"));
		}

		if (notNullNoErrors(params.getColumn("pesatarkastus.tark_vuosi")) && notNullNoErrors(params.getColumn("pesatarkastus.tark_pvm"))) {
			checkTark_vuosiTheSameOrOneYearBeforeTarkPvm(params);
		}

		if (notNullNoErrors(params.getColumn("pesavakio.koord_tyyppi"))) {
			checkCoordinates(params);
		}

		_Column rak_vuosi = params.getColumn("pesavakio.rak_vuosi");
		_Column rak_vuosi_teko = params.getColumn("pesavakio.rak_vuosi_teko");
		_Column loyt_vuosi = params.getColumn("pesavakio.loyt_vuosi");
		_Column tuhoutusmisvuosi = params.getColumn("pesavakio.tuhoutumisvuosi");
		_Column tarkastusvuosi = params.getColumn("pesatarkastus.tark_vuosi");

		checkYearIfNotNull(rak_vuosi);
		checkYearIfNotNull(rak_vuosi_teko);
		checkYearIfNotNull(loyt_vuosi);
		checkYearIfNotNull(tuhoutusmisvuosi);

		if (notNullNoErrors(rak_vuosi)) {
			checkNotNull(params.getColumn("pesavakio.rak_vuosi_tark"));
		}

		if (notNullNoErrors(rak_vuosi_teko)) {
			checkNotNull(params.getColumn("pesavakio.rak_vuosi_teko_tark"));
		}

		checkRakVuosiInRelationToOtherYears(rak_vuosi, loyt_vuosi, tuhoutusmisvuosi);
		checkRakVuosiInRelationToOtherYears(rak_vuosi_teko, loyt_vuosi, tuhoutusmisvuosi);

		if (!kirjekyyhkyValidations) {
			checkLoytvuosiVsTuhoutumisvuosi(loyt_vuosi, tuhoutusmisvuosi);
		}

		_Column rauh_aika_alku = params.getColumn("pesamuuttuva.rauh_aika_alku");
		_Column rauh_aika_loppu = params.getColumn("pesamuuttuva.rauh_aika_loppu");
		if (notNullNoErrors(rauh_aika_alku) && notNullNoErrors(rauh_aika_loppu)) {
			if (date(rauh_aika_alku).after(date(rauh_aika_loppu))) {
				error(rauh_aika_alku, MerikotkaConst.INVALID_RAUH_AIKA_ALKU_LOPPU);
				error(rauh_aika_loppu, MerikotkaConst.INVALID_RAUH_AIKA_ALKU_LOPPU);
			}
		}

		_Column r_taulu_kiinitys = params.getColumn("pesavakio.r_taulu_pvm");
		_Column r_taulu_poisto = params.getColumn("pesavakio.r_taulu_poisto_pvm");
		if (notNullNoErrors(r_taulu_kiinitys) && notNullNoErrors(r_taulu_poisto)) {
			if (date(r_taulu_kiinitys).after(date(r_taulu_poisto))) {
				error(r_taulu_poisto, MerikotkaConst.INVALID_R_TAULU_POISTO_KIINNITYS);
			}
		}

		if (!kirjekyyhkyValidations) {
			checkPesakuntoVsTuhoutumisvuosi(params, tuhoutusmisvuosi, tarkastusvuosi);
		}

		_Column korkeus = params.getColumn("pesamuuttuva.korkeus");
		_Column korkeus_tark = params.getColumn("pesamuuttuva.korkeus_tark");
		if (isNull(korkeus)) {
			if (!isNull(korkeus_tark)) {
				error(korkeus_tark, MerikotkaConst.INVALID_KORK_TARK);
			}
		} else {
			if (isNull(korkeus_tark)) {
				error(korkeus_tark, MerikotkaConst.INVALID_KORK_TARK);
			}
		}

		_Column halk_max = params.getColumn("pesatarkastus.pesa_halk_max");
		_Column halk_min = params.getColumn("pesatarkastus.pesa_halk_min");
		if (notNullNoErrors(halk_max) && notNullNoErrors(halk_min)) {
			if (intValue(halk_max) < intValue(halk_min)) {
				error(halk_max, INVALID_VALUE);
				error(halk_min, INVALID_VALUE);
			}
		}

		_Column pesimistulos = params.getColumn("pesatarkastus.pesimistulos");
		_Column elavia_lkm = params.getColumn("pesatarkastus.elavia_lkm");
		_Column reng_poik_lkm = params.getColumn("pesatarkastus.reng_poik_lkm");
		_Column lentopoik_lkm = params.getColumn("pesatarkastus.lentopoik_lkm");
		if (MerikotkaConst.PESIMISTULOS_POIKANEN_NOT_ALLOWED.contains(pesimistulos.getValue())) {
			if (poikanenValuesGiven(params)) {
				error(pesimistulos, MerikotkaConst.INVALID_PESIMISTULOS_POIKASET);
			}
			checkIsNullOrZero(elavia_lkm, MerikotkaConst.INVALID_PESIMISTULOS_POIKASET);
			checkIsNullOrZero(reng_poik_lkm, MerikotkaConst.INVALID_PESIMISTULOS_POIKASET);
			checkIsNullOrZero(lentopoik_lkm, MerikotkaConst.INVALID_PESIMISTULOS_POIKASET);
		}
		_Column lopMunaMaara = params.getColumn("pesatarkastus.munia_lkm");
		_Column kuoriutumattomat = params.getColumn("pesatarkastus.kuoriutumattomia_lkm");
		if (MerikotkaConst.PESIMISTULOS_MUNA_NOT_ALLOWED.contains(pesimistulos.getValue())) {
			if (munaValuesGiven(params)) {
				error(pesimistulos, MerikotkaConst.INVALID_PESIMISTULOS_MUNAT);
			}
			checkIsNullOrZero(lopMunaMaara, MerikotkaConst.INVALID_PESIMISTULOS_MUNAT);
			checkIsNullOrZero(kuoriutumattomat, MerikotkaConst.INVALID_PESIMISTULOS_MUNAT);
		}

		if (MerikotkaConst.PESIMISTULOS_SUCCESS.contains(pesimistulos.getValue()) && !kirjekyyhkyValidations) {
			String reviiri_id = params.getColumn("vuosi.reviiri_id").getValue();
			String vuosi = params.getColumn("pesatarkastus.tark_vuosi").getValue();
			String pesa_id = params.getColumn("pesavakio.pesa_id").getValue();
			Collection<String> conflicts = MerikotkaDAO.findOutIfThereAreOtherSuccessfulNestingsForReviiri(reviiri_id, vuosi, pesa_id, dao);
			if (!conflicts.isEmpty()) {
				error(pesimistulos, MerikotkaConst.REVIIRI_ALREADY_HAS_SUCCESSFUL_NESTING);
				for (String conflict : conflicts) {
					data.addToList(Const.RISTIRIIDAT, conflict);
				}
			}
		}

		_Column nahdyt_merkit = params.getColumn("pesatarkastus.nahdyt_merkit");
		if (notNullNoErrors(nahdyt_merkit) && notNullNoErrors(pesimistulos)) {
			checkPesimistulosMatchesNahdytMerkit(pesimistulos, nahdyt_merkit, "A", MerikotkaConst.PESIMISTULOS_A__ALLOWED_NAHNDYT_MERKIT);
			checkPesimistulosMatchesNahdytMerkit(pesimistulos, nahdyt_merkit, "K", MerikotkaConst.PESIMISTULOS_K__ALLOWED_NAHNDYT_MERKIT);
			checkPesimistulosMatchesNahdytMerkit(pesimistulos, nahdyt_merkit, "J", MerikotkaConst.PESIMISTULOS_J__ALLOWED_NAHNDYT_MERKIT);
			checkPesimistulosMatchesNahdytMerkit(pesimistulos, nahdyt_merkit, "M", MerikotkaConst.PESIMISTULOS_M__ALLOWED_NAHNDYT_MERKIT);
			checkPesimistulosMatchesNahdytMerkit(pesimistulos, nahdyt_merkit, "P", MerikotkaConst.PESIMISTULOS_P__ALLOWED_NAHNDYT_MERKIT);
			checkPesimistulosMatchesNahdytMerkit(pesimistulos, nahdyt_merkit, "R", MerikotkaConst.PESIMISTULOS_R__ALLOWED_NAHNDYT_MERKIT);
			checkPesimistulosMatchesNahdytMerkit(pesimistulos, nahdyt_merkit, "L", MerikotkaConst.PESIMISTULOS_L__ALLOWED_NAHNDYT_MERKIT);
			checkPesimistulosMatchesNahdytMerkit(pesimistulos, nahdyt_merkit, "U", MerikotkaConst.PESIMISTULOS_U__ALLOWED_NAHNDYT_MERKIT);
		}
		if (notNullNoErrors(nahdyt_merkit)) {
			if (valueIsOneOf(nahdyt_merkit.getValue(), HAS_NESTLINGS)) {
				_Column elavia = params.getColumn("pesatarkastus.elavia_lkm");
				_Column kuolleita = params.getColumn("pesatarkastus.kuolleita_lkm");
				if (!valueIsOneOf(elavia.getValue(), COUNT_GIVEN) && !valueIsOneOf(kuolleita.getValue(), COUNT_GIVEN)) {
					error(elavia, MerikotkaConst.INVALID_NAHDYT_MERKIT_POIKASCOUNT);
					error(kuolleita, MerikotkaConst.INVALID_NAHDYT_MERKIT_POIKASCOUNT);
				}
			}
		}

		_Column muulaji = params.getColumn("pesatarkastus.muulaji");
		_Column muulaji_rinnakkainen = params.getColumn("pesatarkastus.muulaji_rinnakkainen");
		if (notNullNoErrors(muulaji)) {
			if (muulaji.getValue().equals(MERIKOTKA)) {
				error(muulaji, MerikotkaConst.MUULAJI_CAN_NOT_BE_HALALB);
			}
			if (MerikotkaConst.PESIMISTULOS_SUCCESS.contains(pesimistulos.getValue())) {
				error(muulaji, MerikotkaConst.INVALID_MUULAJI_PESIMISTULOS);
			}
		}
		if (notNullNoErrors(muulaji_rinnakkainen)) {
			if (muulaji_rinnakkainen.getValue().equals(MERIKOTKA) || muulaji_rinnakkainen.getValue().startsWith(("MOT"))) {
				error(muulaji_rinnakkainen, MerikotkaConst.MUULAJI_RINNAKKAINEN);
			}
		}

		_Column tark_tapa = params.getColumn("pesatarkastus.tark_tapa");
		_Column epaonn = params.getColumn("pesatarkastus.epaonni_syy");
		if (tark_tapa.getValue().equals(EI_TARKASTETTU) || tark_tapa.getValue().equals(EI_LOYDETTY)) {
			if (!isNull(epaonn) || !isNullOrZero(elavia_lkm) || !isNullOrZero(reng_poik_lkm) || !isNullOrZero(lentopoik_lkm)) {
				error(tark_tapa, MerikotkaConst.INVALID_TARK_TAPA);
			}
		}

		for (_Table poikanen : params.getPoikaset().values()) {
			checkSiipiPituus_mittausmenetelma(poikanen);
			checknillkaMinMax(poikanen);
			checkRenkaat(poikanen);
		}

		_Column tark_tunti = params.getColumn("pesatarkastus.tark_tunti");
		if (notNullNoErrors(tark_tunti)) {
			checkHour(tark_tunti);
		}

		if (notNullNoErrors(params.getColumn("pesatarkastus.kuvaaja_pesa"))) {
			if (!params.getColumn("pesatarkastus.kuvia_pesa").getValue().equals(MerikotkaConst.YES)) {
				error(params.getColumn("pesatarkastus.kuvia_pesa"), MerikotkaConst.SHOULD_NOT_GIVE_KUVAAJA_UNLESS_VALUE_IS_YES);
			}
		}

		if (notNullNoErrors(params.getColumn("pesatarkastus.kuvaaja_lintu"))) {
			if (!params.getColumn("pesatarkastus.kuvia_lintu").getValue().equals(MerikotkaConst.YES)) {
				error(params.getColumn("pesatarkastus.kuvaaja_lintu"), MerikotkaConst.SHOULD_NOT_GIVE_KUVAAJA_UNLESS_VALUE_IS_YES);
			}
		}

		if (kirjekyyhkyValidations) {
			for (String field : MerikotkaConst.ETAISYYS_FIELDS) {
				_Column c = params.getColumn(field);
				if (hasErrors(c)) {
					String value = Utils.removeWhitespace(c.getValue());
					if (value.equalsIgnoreCase("&gt;1000") || value.equalsIgnoreCase(">1000") ) {
						removeError(c);
					}
				}
			}
		}

		checkAikuinenCountVsAikunenInfo(params);

		if (params.getColumn("pesatarkastus.pesa_kunto").getValue().equals("K")) { // Pesä pudonnut
			checkIsNull(params.getColumn("pesamuuttuva.et_maasta"), "PESA_PUDONNUT_EI_SAA_ILMOITTAA_MITTAA");
			checkIsNull(params.getColumn("pesamuuttuva.et_latvasta"), "PESA_PUDONNUT_EI_SAA_ILMOITTAA_MITTAA");
			checkIsNull(params.getColumn("pesatarkastus.pesa_korkeus"), "PESA_PUDONNUT_EI_SAA_ILMOITTAA_MITTAA");
			checkIsNull(params.getColumn("pesatarkastus.pesa_halk_max"), "PESA_PUDONNUT_EI_SAA_ILMOITTAA_MITTAA");
			checkIsNull(params.getColumn("pesatarkastus.pesa_halk_min"), "PESA_PUDONNUT_EI_SAA_ILMOITTAA_MITTAA");
		}
		if (noErrors()) {
			CoordinateConverter converter = convertThisNestsCoordinates(params);
			if (addingNewNest(params)) {
				warnIfExistingNestsNearby(converter);
			}
			if (!kirjekyyhkyValidations) {
				warnIfReviiriNestTooFarAwayFromEachOther(params, converter);
			}
		}
	}

	private void warnIfReviirinimiReceivedFromKirjekyyhkyDifferentThanCurrent(_PesaDomainModelRow params) {
		_Column reviiri = request.data().updateparameters().getVuosi().get("reviiri_id");
		if (request.data().contains("updateparameters.reviirinimi") && notNullNoErrors(reviiri)) {
			String ilmoitettuReviirinimi = request.data().get("updateparameters.reviirinimi");
			String nykyinenReviiriID = reviiri.getValue();
			String nykyinenReviirinimi = null;
			try {
				nykyinenReviirinimi = dao.returnTableByKeys("reviiri", nykyinenReviiriID).get("reviirinimi").getValue();
			} catch (SQLException e) {
				error(reviiri, INVALID_VALUE);
			}
			if (!ilmoitettuReviirinimi.equalsIgnoreCase(nykyinenReviirinimi)) {
				warning(params.getColumn("vuosi.reviiri_id"), MerikotkaConst.ILMOITETTU_REVIIRIN_NIMI_JA_REVIIRI_EIVAT_TASMAA);
			}
		}
	}
	
	private void warnIfReviiriNestTooFarAwayFromEachOther(_PesaDomainModelRow params, CoordinateConverter converter) throws Exception {
		_Column reviiri = params.getVuosi().get("reviiri_id");
		Collection<String> reviirinPesat = getReviiringPesat(reviiri);
		long min_lev = converter.getYht_leveys();
		long max_lev = min_lev;
		long min_pit = converter.getYht_pituus();
		long max_pit = min_pit;

		for (String pesaid : reviirinPesat) {
			_Table pesa = dao.returnTableByKeys("pesavakio", pesaid.toString());
			long pesa_lev = pesa.get("yht_leveys").getIntegerValue();
			long pesa_pit = pesa.get("yht_pituus").getIntegerValue();
			min_lev = Math.min(min_lev, pesa_lev);
			max_lev = Math.max(max_lev, pesa_lev);
			min_pit = Math.min(min_pit, pesa_pit);
			max_pit = Math.max(max_pit, pesa_pit);
		}
		if (max_lev - min_lev > 4000 || max_pit - min_pit > 4000) {
			warning(reviiri, "PESAT_YLI_4KM_TOISISTAAN");
		}
	}
	
	private CoordinateConverter convertThisNestsCoordinates(_PesaDomainModelRow params) throws CoordinateConversionException {
		String koord_tyyppi = params.getColumn("pesavakio.koord_tyyppi").getValue();
		CoordinateConverter converter = new CoordinateConverter();
		if (koord_tyyppi.equals("Y")) {
			converter.setYht_leveys(params.getColumn("pesavakio.yht_leveys").getValue());
			converter.setYht_pituus(params.getColumn("pesavakio.yht_pituus").getValue());
		} else if (koord_tyyppi.equals("E")) {
			converter.setEur_leveys(params.getColumn("pesavakio.eur_leveys").getValue());
			converter.setEur_pituus(params.getColumn("pesavakio.eur_pituus").getValue());
		} else if (koord_tyyppi.equals("A")) {
			converter.setAst_leveys(params.getColumn("pesavakio.ast_leveys").getValue());
			converter.setAst_pituus(params.getColumn("pesavakio.ast_pituus").getValue());
		} else {
			throw new IllegalStateException("Invalid coordinate type: "+koord_tyyppi+". Validations do not let us get this far.");
		}
		converter.convert();
		return converter;
	}
	
	private Collection<String> getReviiringPesat(_Column reviiri) throws SQLException {
		_Table searchParams = dao.newRow().getVuosi();
		searchParams.get("reviiri_id").setValue(reviiri);
		Collection<String> pesat = new HashSet<>();
		for (_Table t : dao.returnTables(searchParams)) {
			pesat.add(t.get("pesa_id").getValue());
		}
		return pesat;
	}

	private void warnIfExistingNestsNearby(CoordinateConverter converter) throws SQLException {
		PreparedStatement p = null;
		ResultSet rs = null;
		try {
			p = dao.prepareStatement(EXISTING_NESTS_CLOSE_BY_SQL);
			p.setLong(1, converter.getYht_leveys());
			p.setLong(2, converter.getYht_pituus());
			rs = p.executeQuery();
			if (rs.next()) {
				warning(MerikotkaConst.PESAN_SIJAINTI, MerikotkaConst.TOINEN_PESA_LAHELLA);
			}
		} finally {
			Utils.close(p, rs);
		}
	}

	private static final String EXISTING_NESTS_CLOSE_BY_SQL = sql();
	private static String sql() {
		StringBuilder statement = new StringBuilder();
		statement.append("SELECT 1 FROM pesavakio WHERE ");
		statement.append("SQRT( ");
		statement.append("POWER( ? - yht_leveys, 2) + ");
		statement.append("POWER( ? - yht_pituus, 2) ");
		statement.append(") <= ").append(RADIUS_TO_CHECK_FOR_EXISTING_NESTS_IN_METERS);
		return statement.toString();
	}

	private boolean addingNewNest(_PesaDomainModelRow params) {
		return isNull(params.getColumn("pesavakio.pesa_id"));
	}

	private void checkAikuinenCountVsAikunenInfo(_PesaDomainModelRow params) {
		int monenkoAikuisenTietojaIlmoitettu = 0;
		for (_Table aikuinen : params.getAikuiset()) {
			if (aikuinen.hasValuesIgnoreMetadata("aikuinen_id", "tarkastus_id")) {
				monenkoAikuisenTietojaIlmoitettu++;
			}
		}
		_Column aikLkm = params.getTarkastus().get("aikuisia_lkm");
		if (monenkoAikuisenTietojaIlmoitettu > 0) {
			checkIsNotNullOrZero(aikLkm);
			if (notNullNoErrors(aikLkm)) {
				if (aikLkm.getIntegerValue() < monenkoAikuisenTietojaIlmoitettu) {
					error(aikLkm, MerikotkaConst.AIK_LKM_AIKUINEN_INFO_RISTIRIITA);
				}
			}
		}
	}

	private void checkRenkaat(_Table lintu) {
		_Column rengas_oikea = lintu.get("rengas_oikea");
		_Column rengas_vasen = lintu.get("rengas_vasen");
		checkRengas(rengas_oikea, 4, 6);
		checkRengas(rengas_vasen, 4, 4);
	}

	private void checkRengas(_Column rengas, int min, int max) {
		if (!rengas.hasValue()) return;
		String value = rengas.getValue();
		if (value.contains("-")) {
			error(rengas, MerikotkaConst.RENKAASSA_EI_SAA_OLLA_VALIMERKKEJA);
			return;
		}
		value = Utils.removeWhitespace(value);
		if (value.length() < min) {
			error(rengas, TOO_SHORT);
		} else if (value.length() > max) {
			warning(rengas, TOO_LONG);
		}
		if (!isAlphabet(value.charAt(0))) {
			error(rengas, MerikotkaConst.RENKAAN_TAYTYY_ALKAA_KIRJAIMELLA);
		}

	}

	private void checkPesakuntoVsTuhoutumisvuosi(_Row params, _Column tuhoutusmisvuosi, _Column tarkastusvuosi) {
		_Column pesa_kunto = params.getColumn("pesatarkastus.pesa_kunto");
		if (hasErrors(pesa_kunto)) return;
		if (MerikotkaConst.PESAN_KUNTO__DEAD.contains(pesa_kunto.getValue())) {
			if (isNull(tuhoutusmisvuosi)) {
				error(tuhoutusmisvuosi, MerikotkaConst.INVALID_TUHOUTUMISVUOSI_PESAN_KUNTO);
				error(pesa_kunto, MerikotkaConst.INVALID_TUHOUTUMISVUOSI_PESAN_KUNTO);
			}
		}

		if (MerikotkaConst.PESAN_KUNTO__ALIVE.contains(pesa_kunto.getValue())) {
			if (notNullNoErrors(tuhoutusmisvuosi) && notNullNoErrors(tarkastusvuosi)) {
				if (intValue(tuhoutusmisvuosi) <= intValue(tarkastusvuosi)) {
					error(tuhoutusmisvuosi, MerikotkaConst.INVALID_TUHOUTUMISVUOSI_PESAN_KUNTO);
					error(pesa_kunto, MerikotkaConst.INVALID_TUHOUTUMISVUOSI_PESAN_KUNTO);
				}
			}
		}
	}


	private void checkLoytvuosiVsTuhoutumisvuosi(_Column loyt_vuosi, _Column tuhoutusmisvuosi) {
		if (notNullNoErrors(loyt_vuosi)) {
			if (notNullNoErrors(tuhoutusmisvuosi)) {
				if (intValue(loyt_vuosi) > intValue(tuhoutusmisvuosi)) {
					error(loyt_vuosi, MerikotkaConst.INVALID_LOYT_VUOSI);
					error(tuhoutusmisvuosi, MerikotkaConst.INVALID_LOYT_VUOSI);
				}
			}
		}
	}

	private void checkRakVuosiInRelationToOtherYears(_Column rak_vuosi, _Column loyt_vuosi, _Column tuhoutusmisvuosi) {
		if (notNullNoErrors(rak_vuosi)) {
			if (notNullNoErrors(loyt_vuosi)) {
				if (intValue(rak_vuosi) > intValue(loyt_vuosi)) {
					error(rak_vuosi, MerikotkaConst.INVALID_RAK_VUOSI);
					error(loyt_vuosi, MerikotkaConst.INVALID_RAK_VUOSI);
				}
			}
			if (!kirjekyyhkyValidations) {
				if (notNullNoErrors(tuhoutusmisvuosi)) {
					if (intValue(rak_vuosi) > intValue(tuhoutusmisvuosi)) {
						error(rak_vuosi, MerikotkaConst.INVALID_RAK_VUOSI);
						error(tuhoutusmisvuosi, MerikotkaConst.INVALID_RAK_VUOSI);
					}
				}
			}
		}
	}

	private void rauh_aika_loppu_canBeInTheFuture() {
		if (_Validator.CAN_NOT_BE_IN_THE_FUTURE.equals(errors().get("pesamuuttuva.rauh_aika_loppu"))) {
			errors().remove("pesamuuttuva.rauh_aika_loppu");
		}
	}

	private void checkSiipiPituus_mittausmenetelma(_Table poikanen) {
		_Column siipi_pituus = poikanen.get("siipi_pituus");
		_Column siipi_pituus_m = poikanen.get("siipi_pituus_m");
		_Column siipi_pituus_2 = poikanen.get("siipi_pituus_2");
		_Column siipi_pituus_m_2 = poikanen.get("siipi_pituus_m_2");
		if (!isNull(siipi_pituus) && isNull(siipi_pituus_m)) {
			error(siipi_pituus_m, _Validator.CAN_NOT_BE_NULL);
		}
		if (!isNull(siipi_pituus_2) && isNull(siipi_pituus_m_2)) {
			error(siipi_pituus_m_2, _Validator.CAN_NOT_BE_NULL);
		}
		if (isNull(siipi_pituus) && !isNull(siipi_pituus_m)) {
			error(siipi_pituus, _Validator.CAN_NOT_BE_NULL);
		}
		if (isNull(siipi_pituus_2) && !isNull(siipi_pituus_m_2)) {
			error(siipi_pituus_2, _Validator.CAN_NOT_BE_NULL);
		}
	}

	private void checknillkaMinMax(_Table poikanen) {
		_Column nilkka_min = poikanen.get("nilkka_min");
		_Column nilkka_max = poikanen.get("nilkka_max");
		_Column nilkka_min_2 = poikanen.get("nilkka_min_2");
		_Column nilkka_max_2 = poikanen.get("nilkka_max_2");
		checkNilkka(nilkka_min, nilkka_max);
		checkNilkka(nilkka_min_2, nilkka_max_2);
	}

	private void checkNilkka(_Column nilkka_min, _Column nilkka_max) {
		if (notNullNoErrors(nilkka_min) && notNullNoErrors(nilkka_max)) {
			if (!firstIsLargerThanSecond(nilkka_max, nilkka_min)) {
				error(nilkka_min, MerikotkaConst.NILKKA_MIN_MUST_BE_SMALLER_THAN_NILKKA_MAX);
				error(nilkka_max, MerikotkaConst.NILKKA_MIN_MUST_BE_SMALLER_THAN_NILKKA_MAX);
			}
		}
	}

	private void checkPesimistulosMatchesNahdytMerkit(_Column pesimistulos, _Column nahdyt_merkit, String pesimistulosCode, Collection<String> allowedMerkitWithPesimistulosCode) {
		if (pesimistulos.getValue().equals(pesimistulosCode)) {
			if (!allowedMerkitWithPesimistulosCode.contains(nahdyt_merkit.getValue())) {
				error(nahdyt_merkit, MerikotkaConst.INVALID_PESIMISTULOS_NAHDYT_MERKIT);
				error(pesimistulos, MerikotkaConst.INVALID_PESIMISTULOS_NAHDYT_MERKIT);
			}
		}
	}

	private boolean poikanenValuesGiven(_PesaDomainModelRow params) {
		for (_Table poikanen : params.getPoikaset().values()) {
			if (poikanen.hasValuesIgnoreMetadata("poikasen_ika", "munan_pituus", "munan_leveys", "kommentti")) {
				return true;
			}
		}
		return false;
	}

	private boolean munaValuesGiven(_PesaDomainModelRow params) {
		for (_Table poikanen : params.getPoikaset().values()) {
			if (poikanen.get("munan_pituus").hasValue() || poikanen.get("munan_leveys").hasValue()) {
				return true;
			}
		}
		return false;
	}

	private Date date(_Column c) throws ParseException {
		return DateUtils.convertToDate(c.getValue());
	}

	private Integer intValue(_Column c) {
		return Integer.valueOf(c.getValue());
	}

	private void checkYearIfNotNull(_Column rak_vuosi) {
		if (notNullNoErrors(rak_vuosi)) checkValidYear(rak_vuosi);
	}

	private void checkTark_vuosiTheSameOrOneYearBeforeTarkPvm(_Row params) {
		int pesintavuosi = Integer.valueOf(params.getColumn("pesatarkastus.tark_vuosi").getValue());
		int tark_pvm_yyyy = Integer.valueOf(params.getColumn("pesatarkastus.tark_pvm").getDateValue().getYear());
		if (pesintavuosi < tark_pvm_yyyy - 1 || pesintavuosi > tark_pvm_yyyy) {
			error("pesatarkastus.tark_pvm", MerikotkaConst.INVALID_TARK_VUOSI_TARK_PVM_YYYY);
		}
	}

	private boolean exists(Set<String> set) {
		return !set.isEmpty();
	}

	private boolean firstMeasurementsExists(_PesaDomainModelRow params) {
		for (_Table poikanen : params.getPoikaset().values()) {
			if (poikanen.hasValuesIgnoreMetadata("poikanen_id", "tarkastus_id")) return true;
		}
		return false;
	}

	private Set<String> secondMeasurements(_PesaDomainModelRow params) {
		Set<String> secondMeasurementsExistForPoikanen = new HashSet<>();
		for (_Table poikanen : params.getPoikaset().values()) {
			for (_Column c : poikanen) {
				if (c.getName().endsWith("_2") && !isNull(c.getValue())) {
					secondMeasurementsExistForPoikanen.add(c.getTablename());
					break;
				}
			}
		}
		return secondMeasurementsExistForPoikanen;
	}

	private void checkCoordinates(_Row params) throws Exception {
		String koord_tyyppi = params.getColumn("pesavakio.koord_tyyppi").getValue();
		_Column leveys = null, pituus = null;
		int leveys_min, leveys_max, pituus_min, pituus_max;
		CoordinateConverter converter = new CoordinateConverter();
		boolean okToCheckInsideReagion = false;

		if (koord_tyyppi.equals("Y")) {
			leveys = params.getColumn("pesavakio.yht_leveys");
			pituus = params.getColumn("pesavakio.yht_pituus");
			leveys_min = _Validator.YHT_LEVEYS_MIN;
			leveys_max = _Validator.YHT_LEVEYS_MAX;
			pituus_min = _Validator.YHT_PITUUS_MIN;
			pituus_max = _Validator.YHT_PITUUS_MAX;
			checkCoordinateRanges(leveys, pituus, leveys_min, leveys_max, pituus_min, pituus_max);
			if (noErrors(leveys) && noErrors(pituus)) {
				converter.setYht_leveys(leveys.getValue());
				converter.setYht_pituus(pituus.getValue());
				okToCheckInsideReagion = true;
			}
		} else if (koord_tyyppi.equals("E")) {
			leveys = params.getColumn("pesavakio.eur_leveys");
			pituus = params.getColumn("pesavakio.eur_pituus");
			leveys_min = _Validator.EUR_LEVEYS_MIN;
			leveys_max = _Validator.EUR_LEVEYS_MAX;
			pituus_min = _Validator.EUR_PITUUS_MIN;
			pituus_max = _Validator.EUR_PITUUS_MAX;
			checkCoordinateRanges(leveys, pituus, leveys_min, leveys_max, pituus_min, pituus_max);
			if (noErrors(leveys) && noErrors(pituus)) {
				converter.setEur_leveys(leveys.getValue());
				converter.setEur_pituus(pituus.getValue());
				okToCheckInsideReagion = true;
			}
		} else if (koord_tyyppi.equals("A")) {
			leveys = params.getColumn("pesavakio.ast_leveys");
			pituus = params.getColumn("pesavakio.ast_pituus");
			leveys_min = _Validator.AST_LEVEYS_MIN;
			leveys_max = _Validator.AST_LEVEYS_MAX;
			pituus_min = _Validator.AST_PITUUS_MIN;
			pituus_max = _Validator.AST_PITUUS_MAX;
			checkCoordinateRanges(leveys, pituus, leveys_min, leveys_max, pituus_min, pituus_max);
			if (noErrors(leveys) && noErrors(pituus)) {
				converter.setAst_leveys(leveys.getValue());
				converter.setAst_pituus(pituus.getValue());
				okToCheckInsideReagion = true;
			}
		} else {
			error("pesavakio.koord_tyyppi", _Validator.INVALID_VALUE);
		}

		if (okToCheckInsideReagion && noErrors("vuosi.pesan_kunta")) {
			checkInsideReagion(params.getColumn("vuosi.pesan_kunta").getValue(), leveys, pituus, converter);
		}
	}

}
