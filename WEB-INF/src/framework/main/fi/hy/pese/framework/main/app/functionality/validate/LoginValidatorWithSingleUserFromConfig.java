package fi.hy.pese.framework.main.app.functionality.validate;

import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data._Row;

public class LoginValidatorWithSingleUserFromConfig<T extends _Row> extends Validator<T> {

	private final String	correct_username;
	private final String	correct_password;

	public LoginValidatorWithSingleUserFromConfig(_Request<T> request, String correct_username, String correct_password) {
		super(request);
		this.correct_username = correct_username;
		this.correct_password = correct_password;
	}

	@Override
	public void validate() {
		if (data.action().equals(Const.CHECK_LOGIN)) {
			String username = data.get(Const.LOGIN_USERNAME);
			String password = data.get(Const.LOGIN_PASSWORD);

			if (!correct_username.equals(username) || !correct_password.equals(password)) {
				error(Const.LOGIN_FIELDS, LOGIN_INCORRECT);
			}
		}
	}

}
