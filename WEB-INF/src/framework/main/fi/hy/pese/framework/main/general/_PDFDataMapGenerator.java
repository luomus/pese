package fi.hy.pese.framework.main.general;

import java.util.Map;

public interface _PDFDataMapGenerator {

	Map<String, String> tarkastusDataFor(String pesaid, String year) throws Exception;

	Map<String, String> tarkastusDataFor(String tarkastusid) throws Exception;

}
