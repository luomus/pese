package fi.hy.pese.framework.test.kirjekyyhky;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import fi.hy.pese.framework.main.general.data.PesaDomainModelDatabaseStructure;
import fi.hy.pese.framework.main.general.data.Row;
import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.kirjekyyhky.KirjekyyhkyXMLGeneratorUtility;
import fi.hy.pese.framework.main.kirjekyyhky.KirjekyyhkyXMLGeneratorUtility.StructureDefinition;
import fi.hy.pese.framework.test.TestConfig;
import fi.hy.pese.merikotka.main.app.MerikotkaDatabaseStructureCreator;
import fi.luomus.commons.config.Config;
import fi.luomus.commons.containers.Selection;
import fi.luomus.commons.containers.SelectionImple;
import fi.luomus.commons.containers.SelectionOptionImple;
import fi.luomus.commons.utils.FileUtils;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class KirjekyyhkyXMLGeneratorTests {

	public static Test suite() {
		return new TestSuite(KirjekyyhkyXMLGeneratorTests.class.getDeclaredClasses());
	}

	public static class WhenGeneratingStructureXML extends TestCase {

		private KirjekyyhkyXMLGeneratorUtility generator;
		private StructureDefinition definition;
		private final PesaDomainModelDatabaseStructure	structure = MerikotkaDatabaseStructureCreator.hardCoded();
		private final Config config = TestConfig.getConfig();
		private final File folder = new File(config.baseFolder() + "kirjekyyhky-xml-tests/");

		@Override
		protected void setUp() throws Exception {
			generator = new KirjekyyhkyXMLGeneratorUtility();
			definition = generator.getDefinition();
		}

		public void test___creating_root_element() throws Exception {
			definition.typeID = "merikotkalomake";
			String expected = FileUtils.readContents((new File(folder, "test1.xml")));
			assertEquals(expected.trim(), generator.generateStructureXML().trim());
		}

		public void test___basic_elements() throws Exception {
			definition.typeID = "merikotkalomake";
			definition.title = "Merikotkatutkimus";
			definition.titleFields.add("joku.field");
			definition.titleFields.add("toinen.field");
			definition.validation.URI = "http://localhost/jotain";
			definition.validation.username = "minuntunnus";
			definition.validation.password = "salainensana";

			String expected = FileUtils.readContents((new File(folder, "test2.xml")));
			assertEquals(expected.trim(), generator.generateStructureXML().trim());
		}

		public void test___adding_field_list__and___layout__and__selections() throws Exception {
			definition.typeID = "merikotkalomake";
			definition.title = "Merikotkatutkimus";
			definition.titleFields.add("joku.field");
			definition.titleFields.add("toinen.field");
			definition.validation.URI = "http://localhost/jotain";
			definition.validation.username = "minuntunnus";
			definition.validation.password = "salainensana";

			definition.row = new Row(structure);
			generator.excludeColumn("vuosi", "reviirin_kunta");
			generator.addCustomColumn("vuosi.custom-column", _Column.INTEGER, 5);
			generator.addCustomColumn("custom-field", _Column.VARCHAR, 10);
			generator.addCustomColumn("custom-field-with-selection", _Column.VARCHAR, "LAJIT");
			generator.addCustomColumn("tipu-api-selection", _Column.INTEGER, 6, "TARKASTAJAT");
			generator.includeTable("vuosi");

			Map<String, Selection> selections = new LinkedHashMap<>();

			SelectionImple selection = new SelectionImple("LAJIT");
			selection.addOption(new SelectionOptionImple("PANHAL", "Kalasääksi"));
			selection.addOption(new SelectionOptionImple("HALALB", "Merikotka"));
			selections.put(selection.getName(), selection);

			selection = new SelectionImple("LISTA");
			selection.addOption(new SelectionOptionImple("A", "Kuvaus A"));
			selection.addOption(new SelectionOptionImple("B", "Kuvaus 'B'"));
			selection.addOption(new SelectionOptionImple("C > D", "Cee > Dee"));
			selections.put(selection.getName(), selection);

			selection = new SelectionImple("OptionGrouppeja");
			selection.addOption(new SelectionOptionImple("1", "Paras", "Positiiviset"));
			selection.addOption(new SelectionOptionImple("2", "Parempi", "Positiiviset"));
			selection.addOption(new SelectionOptionImple("3", "Hyvä", "Positiiviset"));
			selection.addOption(new SelectionOptionImple("4", "Huono", "Negatiiviset"));
			selection.addOption(new SelectionOptionImple("5", "Huonompi", "Negatiiviset"));
			selection.addOption(new SelectionOptionImple("6", "Huonoin", "Negatiiviset"));
			selections.put(selection.getName(), selection);

			selection = new SelectionImple("foobar");
			selection.addOption(new SelectionOptionImple("foo", "bar"));
			selections.put(selection.getName(), selection);

			definition.selections  = selections;

			generator.noCodesForSelection("LAJIT");
			generator.excludeSelection("foobar");
			generator.defineTipuApiSelection("tipu-api-selection", "ringers");

			Map<String, String> fieldTitles = new HashMap<>();
			fieldTitles.put("vuosi.vuosi", "Vuosi on 'hieno' & kaunis ja \"mukava\"");
			fieldTitles.put("custom-field", "Tämän kuvaus <tähän>");
			definition.fieldTitles = fieldTitles;

			definition.layout = "" +
					"<section id=\"vuosi\" title=\"Pesintävuosi\"> \n" +
					"<field name=\"pesatarkastus.tark_vuosi\" /> \n" +
					"</section> \n";

			String expected = FileUtils.readContents((new File(folder, "test3.xml")));
			assertEquals(expected.trim(), generator.generateStructureXML().trim());
		}

		public void test___generating__form_data_xml() throws Exception {
			definition.typeID = "merikotkalomake";
			definition.row = new Row(structure);
			generator.excludeColumn("vuosi", "r_kunta_id");
			generator.addCustomColumn("vuosi.custom-column", _Column.INTEGER, 5);
			generator.addCustomColumn("custom-field", _Column.VARCHAR, 10);
			generator.addCustomColumn("custom-field-with-selection", _Column.VARCHAR, "LAJIT");

			generator.includeTable("vuosi");

			Map<String, String> data = new HashMap<>();
			data.put("custom-field", "Custom Value");
			data.put("vuosi.custom-column", "Vuosi Custom Value");
			data.put("vuosi.vuosi", "value");

			List<String> owners = new ArrayList<>();
			owners.add("846");
			owners.add("3333");

			String xml = generator.generateDataXML(data, owners);
			String expected = FileUtils.readContents((new File(folder, "test4.xml")));
			assertEquals(expected.trim(), xml.trim());
		}
	}

}
