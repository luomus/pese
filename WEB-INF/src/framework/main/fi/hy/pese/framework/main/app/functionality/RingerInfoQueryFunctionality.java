package fi.hy.pese.framework.main.app.functionality;

import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data._Row;
import fi.luomus.commons.tipuapi.TipuAPIClient;
import fi.luomus.commons.tipuapi.TipuApiResource;
import fi.luomus.commons.xml.Document.Node;
import fi.luomus.commons.xml.XMLWriter;

public class RingerInfoQueryFunctionality<T extends _Row> extends BaseFunctionality<T> {
	
	public RingerInfoQueryFunctionality(_Request<T> request) {
		super(request);
	}
	
	@Override
	public void afterSuccessfulValidation() throws Exception {
		String renro = data.get("renro");
		TipuApiResource ringers = request.getTipuApiResource(TipuAPIClient.RINGERS);
		Node ringerInfo = ringers.getById(renro);
		if (ringerInfo ==  null) {
			request.redirecter().status404();
			return;
		}
		String xml = new XMLWriter(ringerInfo).generateXML();
		data.setXMLOutput(xml);
		data.setPage(Const.XML_OUTPUT_PAGE);
	}
	
}
