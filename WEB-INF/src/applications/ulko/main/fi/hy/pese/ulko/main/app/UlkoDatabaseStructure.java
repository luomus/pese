package fi.hy.pese.ulko.main.app;

import fi.hy.pese.framework.main.general.data.DatabaseStructure;
import fi.hy.pese.framework.main.general.data.DatabaseTableStructure;
import fi.hy.pese.framework.main.general.data._Table;

public class UlkoDatabaseStructure extends DatabaseStructure {
	
	private DatabaseTableStructure pyynto;
	private DatabaseTableStructure loyto;
	private DatabaseTableStructure rengastus;
	
	public UlkoDatabaseStructure() {
		super();
		setPyynto(new DatabaseTableStructure(_Table.UNINITIALIZED_TABLE));
		setLoyto(new DatabaseTableStructure(_Table.UNINITIALIZED_TABLE));
		setRengastus(new DatabaseTableStructure(_Table.UNINITIALIZED_TABLE));
	}
	
	public void setPyynto(DatabaseTableStructure pyynto) {
		this.pyynto = pyynto;
	}
	
	public DatabaseTableStructure getPyynto() {
		return pyynto;
	}
	
	public void setLoyto(DatabaseTableStructure tapaaminen) {
		this.loyto = tapaaminen;
	}
	
	public DatabaseTableStructure getLoyto() {
		return loyto;
	}
	
	public void setRengastus(DatabaseTableStructure rengastus) {
		this.rengastus = rengastus;
	}
	
	public DatabaseTableStructure getRengastus() {
		return rengastus;
	}
	
}
