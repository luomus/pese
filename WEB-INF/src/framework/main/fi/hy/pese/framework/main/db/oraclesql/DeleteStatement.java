package fi.hy.pese.framework.main.db.oraclesql;

import java.util.LinkedList;
import java.util.List;

import fi.hy.pese.framework.main.general.data._Table;
import fi.luomus.commons.db.connectivity.TransactionConnection;

public class DeleteStatement extends Statement {

	private String				table	= "";
	private final List<String>	where;

	public DeleteStatement(TransactionConnection con) {
		super(con);
		where = new LinkedList<>();
	}

	@Override
	protected String constructQuery() {
		StringBuilder query = new StringBuilder(" DELETE FROM ");
		query.append(table);
		query.append(" WHERE ");
		toAndSeperatedStatement(query, where);
		return query.toString();
	}

	public void deleteByRowId(_Table table) {
		this.table = table.getName();
		where.add(" rowid = ? ");
		values.add(table.getRowidValue());
	}

}
