package fi.hy.pese.ulko.main.functionality;

import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.ulko.main.app._UlkoDomainModelRow;
import fi.luomus.commons.tipuapi.TipuAPIClient;
import fi.luomus.commons.xml.Document.Node;

public class MunicipalityToEuringPlaceQuery extends LoytotiedotQuery {

	public MunicipalityToEuringPlaceQuery(_Request<_UlkoDomainModelRow> request) {
		super(request);
	}

	@Override
	protected void process() {
		String kunta = data.get("municipalityID").toUpperCase();
		set("loyto.euring_paikka", resolve(kunta, request));
	}

	public static String resolve(String municipalityID, _Request<_UlkoDomainModelRow> request) {
		Node municipality = request.getTipuApiResource(TipuAPIClient.MUNICIPALITIES).getById(municipalityID);
		if (municipality != null) {
			Node euringPlace = getEuringPlace(municipality, request.getTipuApiResource(TipuAPIClient.EURING_PROVINCES));
			if (euringPlace != null) {
				return euringPlace.getNode("id").getContents();
			}
		}
		return "SF--";
	}

}


