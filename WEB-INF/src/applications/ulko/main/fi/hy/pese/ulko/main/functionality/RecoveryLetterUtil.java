package fi.hy.pese.ulko.main.functionality;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._Table;
import fi.hy.pese.ulko.main.app._UlkoDomainModelRow;
import fi.luomus.commons.containers.Selection;
import fi.luomus.commons.pdf.PDFWriter;
import fi.luomus.commons.tipuapi.TipuAPIClient;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.xml.Document.Node;

public class RecoveryLetterUtil {
	
	protected static final String	LA	= "LA";
	private static final String	EN	= "EN";
	protected final _Table pyynto;
	protected final _Table loyto;
	protected final _Request<_UlkoDomainModelRow> request;
	protected final String diario;
	protected final Map<String, String> dataMap;
	protected final PDFWriter writer;
	protected final Euring2020Util euring2020Util;
	
	public RecoveryLetterUtil(_Table pyynto, _Table loyto, _Request<_UlkoDomainModelRow> request) {
		this.pyynto = pyynto;
		this.loyto = loyto;
		this.request = request;
		diario = pyynto.getUniqueNumericIdColumn().getValue();
		dataMap = new HashMap<>();
		toMap(pyynto);
		toMap(loyto);
		dataMap.put("diario", "U" + diario);
		dataMap.put("paivays", DateUtils.getCurrentDateTime("dd.MM.yyyy"));
		this.writer = request.pdfWriter();
		this.euring2020Util = new Euring2020Util(request.getTipuApiResource(TipuAPIClient.SPECIES), request.getTipuApiResource(TipuAPIClient.MUNICIPALITIES));
	}
	
	protected void toMap(_Table table) {
		for (_Column c : table) {
			if (c.hasValue()) {
				dataMap.put(c.getName(), c.getValue());
				dataMap.put(c.getFullname(), c.getValue());
			}
		}
		addRasvaIndex("rasva");
		addRasvaIndex(table.getName()+".rasva");
	}
	
	private void addRasvaIndex(String fieldname) {
		if (dataMap.containsKey(fieldname)) {
			dataMap.put(fieldname, dataMap.get(fieldname) + "/8");
		}
	}
	
	public File write(boolean kansilehti) throws Exception {
		dataMap.put("vastaanottaja", vastaanottaja());
		dataMap.put("renkaat", renkaat(loyto, EN));
		dataMap.put("aika", aika(loyto, EN));
		dataMap.put("paikka", paikka(loyto));
		dataMap.put("koordinaatit", koordinaatit(loyto, EN));
		
		dataMap.put("koodit_selite", kooditSelite(loyto));
		
		dataMap.put("laji", laji(loyto, LA));
		
		dataMap.put("sukupuoli", getValueDescription(loyto.get("sukupuoli"), EN));
		dataMap.put("ika", ika(loyto, EN));
		
		String loytaja = henkilo("loytaja", ", ", true).trim();
		loytaja = loytaja.substring(0, loytaja.length()-1);
		String ilmoittaja = henkilo("ilmoittaja", ", ", true).trim();
		if (ilmoittaja.length() > 0) {
			ilmoittaja = ilmoittaja.substring(0, ilmoittaja.length()-1);
			loytaja += "\n" + ilmoittaja;
		}
		dataMap.put("loytaja", loytaja);
		dataMap.put("kommentti", loyto.get("kommentti").getValue());
		
		dataMap.put("euring_code", euring2020Util.convert(loyto, diario));
		
		File kirje = writer.writePdf("letter_to_foreign_place", dataMap, diario);
		if (kansilehti) {
			dataMap.put("kansilehti", dataMap.get("vastaanottaja"));
			writer.writePdf("kansilehti", dataMap, diario+"_kansilehti");
			return writer.createCollection(diario+"_kansilehdella");
		}
		request.data().addToNoticeTexts(writer.noticeTexts());
		return kirje;
	}
	
	protected String ika(_Table table, String lang) {
		_Column pvm = table.get("pvm");
		String ika = table.get("ika").getValue();
		String desc = getValueDescription(table.get("ika"), lang);
		ika = ika.replace("+", "");
		if (isNumber(ika) && pvm.hasValue()) {
			int beforeValue = Integer.valueOf(ika) - 1;
			int year = (Integer.valueOf(pvm.getDateValue().getYear()) - beforeValue);
			if (lang.equals(EN)) {
				desc += " " + year;
			} else {
				if (desc.contains("vuonna")) {
					desc = desc.subSequence(0, desc.indexOf("vuonna") + "vuonna".length()) + " " + year + " " + desc.substring(desc.indexOf("vuonna") + "vuonna".length());
				} else {
					desc = desc.subSequence(0, desc.indexOf("vuotta") + "vuotta".length()) + " " + year + " " + desc.substring(desc.indexOf("vuotta") + "vuotta".length());
				}
				desc = desc.replace("  ", " ");
			}
		}
		return desc;
	}
	
	private boolean isNumber(String value) {
		try {
			Integer.valueOf(value);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}
	
	/**
	 * Henkilön tiedot katenoituna yhteen käyttäen parametrina annettua separaattoria
	 * @param tyyppi "loytaja" tai "ilmoittaja"
	 * @param separator
	 * @return
	 */
	protected String henkilo(String tyyppi, String separator, boolean emailPuh) {
		StringBuilder loytaja = new StringBuilder();
		loytaja.append(v(pyynto.get(tyyppi + "_nimi"), separator));
		loytaja.append(v(pyynto.get(tyyppi + "_osoite"), separator));
		loytaja.append(v(pyynto.get(tyyppi + "_postinumero"), separator).replace("\n", " "));
		loytaja.append(v(pyynto.get(tyyppi + "_postitoimipaikka"), separator));
		if (emailPuh) {
			loytaja.append(v(pyynto.get(tyyppi + "_email"), separator).toLowerCase());
			loytaja.append(v(pyynto.get(tyyppi + "_puhelin"), separator));
		}
		return loytaja.toString();
	}
	
	private String v(_Column c, String separator) {
		String value = c.getValue().toUpperCase().trim();
		if (value.length() > 0) {
			value += separator;
		}
		return value;
	}
	
	protected String getValueDescription(_Column c, String lang) {
		return request.data().selections().get(c.getAssosiatedSelection()).get(c.getValue(), lang);
	}
	
	protected String laji(_Table table, String lang) {
		Node speciesInfo = request.getTipuApiResource(TipuAPIClient.SPECIES).getById(table.get("laji").getValue());
		if (speciesInfo == null) return "";
		for (Node name : speciesInfo.getChildNodes("name")) {
			if (name.getAttribute("lang").equals(lang)) {
				return name.getContents();
			}
		}
		throw new IllegalStateException("Ei nimeä lajille " + loyto.get("laji").getValue() + " kielellä " +lang + ".");
	}
	
	private String kooditSelite(_Table loyto) {
		_Column linnun_tila = loyto.get("linnun_tila");
		_Column loytotapa = loyto.get("loytotapa");
		_Column loytotapa_tarkkuus = loyto.get("loytotapa_tarkkuus");
		StringBuilder kooditSelite = new StringBuilder();
		if (linnun_tila.hasValue()) {
			kooditSelite.append(getValueDescription(linnun_tila, EN)).append(" (").append(linnun_tila.getValue()).append(")\n");
		}
		if (loytotapa.hasValue()) {
			kooditSelite.append(getValueDescription(loytotapa, EN)).append(" (").append(loytotapa.getValue()).append(")");
			if ("K".equals(loytotapa_tarkkuus.getValue())) {
				kooditSelite.append(" (accurate)");
			}
			if ("E".equals(loytotapa_tarkkuus.getValue())) {
				kooditSelite.append(" (presumed)");
			}
		}
		return kooditSelite.toString();
	}
	
	protected String koordinaatit(_Table table, String lang) {
		StringBuilder koordinaatit = new StringBuilder();
		
		if (table.get("wgs84_ast_leveys").hasValue() && table.get("wgs84_ast_pituus").hasValue()) {
			String leveys = toDegMinSecString(table.get("wgs84_ast_leveys").getValue(), "N", "S");
			String pituus = toDegMinSecString(table.get("wgs84_ast_pituus").getValue(), "E", "W");
			koordinaatit.append(leveys).append("   ").append(pituus);
		}
		
		if (table.get("wgs84_leveys").hasValue() && table.get("wgs84_pituus").hasValue()) {
			double leveys = table.get("wgs84_leveys").getDoubleValue();
			double pituus = table.get("wgs84_pituus").getDoubleValue();
			leveys = (double) Math.round(leveys * 100) / 100;
			pituus = (double) Math.round(pituus * 100) / 100;
			koordinaatit.append("         ").append(leveys).append(" N   ").append(pituus).append(" E");
		}
		
		_Column paikka_tarkkuus = table.get("paikka_tarkkuus");
		if (paikka_tarkkuus.hasValue() && !paikka_tarkkuus.getValue().equals("0")) {
			koordinaatit.append("    " + getValueDescription(paikka_tarkkuus, lang));
		}
		return koordinaatit.toString();
	}
	
	private String toDegMinSecString(String value, String positivePostfix, String negativePostfix) {
		String postfix = positivePostfix;
		if (value.startsWith("-")) {
			value = value.substring(1);
			postfix = negativePostfix;
		}
		while (value.length() < 6) {
			value = "0" + value;
		}
		return value.substring(0, 2) + "°" + value.substring(2, 4) + "'" + value.substring(4) + "\"" + " " + postfix;
	}
	
	protected String paikka(_Table table) {
		StringBuilder paikka = new StringBuilder();
		_Column tarkempiPaikka = table.get("tarkempi_paikka");
		_Column kunta = table.get("kunta");
		_Column euringPaikka = table.get("euring_paikka");
		
		if (tarkempiPaikka.hasValue()) {
			paikka.append(tarkempiPaikka.getValue()).append(", ");
		}
		
		Selection selection = request.data().selections().get(TipuAPIClient.MUNICIPALITIES);
		if (selection.containsOption(kunta.getValue())) {
			paikka.append(selection.get(kunta.getValue())).append(", ");
		} else {
			paikka.append(kunta.getValue()).append(", ");
		}
		
		paikka.append(request.data().selections().get(TipuAPIClient.EURING_PROVINCES).get(euringPaikka.getValue())).append("     ");
		paikka.append(euringPaikka.getValue());
		return paikka.toString().toUpperCase();
	}
	
	protected String aika(_Table table, String lang) {
		StringBuilder aika = new StringBuilder();
		if (!table.get("pvm").hasValue()) return "";
		
		aika.append(table.get("pvm").getValue());
		
		if (table.get("klo").hasValue()) {
			String klo = table.get("klo").getValue();
			if (klo.length() == 1) {
				klo = "0" + klo;
			}
			aika.append("  /  ").append(klo).append(" h");
		}
		aika.append("    " + getValueDescription(table.get("aika_tarkkuus"), lang));
		return aika.toString();
	}
	
	protected String vastaanottaja() {
		Node scheme = request.getTipuApiResource(TipuAPIClient.SCHEMES).getById(loyto.get("scheme").getValue());
		if (scheme == null) return "";
		StringBuilder vastaanottaja = new StringBuilder().append(scheme.getNode("name").getContents()).append("\n");
		for (Node addressLine : scheme.getNode("address").getChildNodes()) {
			if (addressLine.getContents().length() > 0) {
				vastaanottaja.append(addressLine.getContents()).append("\n");
			}
		}
		return vastaanottaja.toString().toUpperCase();
	}
	
	protected  String renkaat(_Table table, String lang) {
		StringBuilder renkaat = new StringBuilder();
		renkaat.append(table.get("rengas").getValue()).append(" ").append(table.get("rengas_selite").getValue());
		String varmennettu = table.get("rengas_varmennettu").getValue();
		if (lang.equals(EN)) {
			if (varmennettu.equals("K")) {
				renkaat.append(" / Verified");
			} else if (varmennettu.equals("E")){
				renkaat.append(" / Not verified");
			}
		}
		return renkaat.toString().trim();
	}
	
}
