package fi.hy.pese.framework.main.general.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import fi.hy.pese.framework.main.general.consts.Const;
import fi.luomus.commons.utils.Utils;

public class Table implements _Table, Serializable {
	
	private static final long	serialVersionUID	= -7012595048538370277L;
	private final Map<String, String>		valueMap = new HashMap<>();
	private final DatabaseTableStructure	tableStructure;
	private boolean							convertedToHTMLEntities	= false;
	private final String tableFullname;
	private int index = 0;
	
	public Table(DatabaseTableStructure tableStructure) {
		this.tableStructure = tableStructure;
		this.tableFullname = tableStructure.getName();
	}
	
	public Table(DatabaseTableStructure tableStructure, int i) {
		this.tableStructure = tableStructure;
		this.tableFullname = tableStructure.getName() + "." + i;
		this.index = i;
	}
	
	@Override
	public String getName() {
		return tableStructure.getName();
	}
	
	@Override
	public _Column getUniqueNumericIdColumn() throws IllegalArgumentException {
		DatabaseColumnStructure idColumn = tableStructure.getUniqueNumericIdColumn();
		return new Column(this, idColumn, valueMap);
	}
	
	@Override
	public boolean hasUniqueNumericIdColumn() {
		return tableStructure.hasUniqueNumericIdColumn();
	}
	
	@Override
	public boolean hasImmutableKeys() {
		return tableStructure.hasImmutableKeys();
	}
	
	@Override
	public Column get(String name) throws IllegalArgumentException {
		DatabaseColumnStructure structure = tableStructure.get(name);
		return new Column(this, structure, valueMap);
	}
	
	private class ColumnIterator implements Iterator<_Column> {
		
		private final Iterator<DatabaseColumnStructure>	iterator;
		private final Map<String, String>				valueMap;
		private final Table table;
		
		public ColumnIterator(Table table, DatabaseTableStructure tableStructure, Map<String, String> valueMap) {
			this.iterator = tableStructure.getColumns().iterator();
			this.valueMap = valueMap;
			this.table = table;
		}
		
		@Override
		public boolean hasNext() {
			return iterator.hasNext();
		}
		
		@Override
		public _Column next() {
			return new Column(table, iterator.next(), valueMap);
		}
		
		@Override
		public void remove() {
			throw new UnsupportedOperationException();
		}
		
	}
	
	@Override
	public Iterator<_Column> getColumns() {
		return new ColumnIterator(this, tableStructure, valueMap);
	}
	
	@Override
	public void clearAllValues() {
		valueMap.clear();
	}
	
	/**
	 * Convenience method, short for column(Const.ROWID).getValue()
	 */
	@Override
	public String getRowidValue() {
		return get(Const.ROWID).getValue();
	}
	
	/**
	 * Convenience method, short for column(Const.ROWID).setValue()
	 */
	@Override
	public void setRowidValue(String rowid) {
		get(Const.ROWID).setValue(rowid);
	}
	
	@Override
	public String getFullname() {
		return tableFullname;
	}
	
	@Override
	public boolean hasValues() {
		for (String s : valueMap.values()) {
			if (s.length() > 0) return true;
		}
		return false;
	}
	
	@Override
	public boolean hasValuesIgnoreMetadata(String... ignoreColumns) {
		Collection<String> columnsToIgnore = new HashSet<>();
		for (String s : ignoreColumns) {
			checkIfColumnExists(s);
			columnsToIgnore.add(s);
		}
		for (_Column c : this) {
			if (c.getFieldType() != _Column.NORMAL_CHANGEABLE_FIELD) continue;
			if (columnsToIgnore.contains(c.getName())) continue;
			if (c.hasValue()) return true;
		}
		return false;
	}
	
	/**
	 * Throws IllegalArgumentException if column does not exists
	 * @param columnName
	 */
	private void checkIfColumnExists(String columnName) {
		this.get(columnName);
	}
	
	@Override
	public boolean equalsIgnoreMetadata(_Table otherTable, String... ignoreColumns) throws IllegalArgumentException {
		if (otherTable == null) throw new IllegalArgumentException("Table compared to is null");
		Collection<String> columnsToIgnore = new HashSet<>();
		for (String s : ignoreColumns) {
			checkIfColumnExists(s);
			columnsToIgnore.add(s);
		}
		for (_Column myColumn : this) {
			if (myColumn.getFieldType() != _Column.NORMAL_CHANGEABLE_FIELD) continue;
			if (columnsToIgnore.contains(myColumn.getName())) continue;
			_Column othersColumn = otherTable.get(myColumn.getName());
			if (!myColumn.getValue().equals(othersColumn.getValue())) return false;
		}
		return true;
	}
	
	@Override
	public boolean equals(Object o) {
		throw new IllegalArgumentException("Table can not be compared to anything else than another Table");
	}

	@Override
	public int hashCode() {
		throw new IllegalArgumentException("Table can not be compared to anything else than another Table");
	}

	@Override
	public int getTablemanagementPermissions() {
		return tableStructure.getTablemanagementPermissions();
	}
	
	@Override
	public void convertAllValuesToHTMLEntities() {
		if (convertedToHTMLEntities) return;
		for (Map.Entry<String, String> e : valueMap.entrySet()) {
			String value = e.getValue();
			if (value.length() < 1) continue;
			value = Utils.toHTMLEntities(value);
			valueMap.put(e.getKey(), value);
		}
		convertedToHTMLEntities = true;
	}
	
	@Override
	public Iterator<_Column> iterator() {
		return getColumns();
	}
	
	@Override
	public int numberOfColumns() {
		return tableStructure.numberOfColumns();
	}
	
	@Override
	public boolean isInitialized() {
		return tableStructure.isInitialized();
	}
	
	@Override
	public void setValues(_Table originTable) {
		for (_Column originColumn : originTable) {
			if (originColumn.getFieldType() == _Column.ROWID) continue;
			this.get(originColumn.getName()).setValue(originColumn.getValue());
		}
	}
	
	
	@Override
	public _Column getReferencingColumn(_Table anotherTable) throws IllegalArgumentException {
		for (_ReferenceKey key : anotherTable.getUniqueNumericIdColumn().getReferencingColumns()) {
			if (key.table().equals(this.getName())) {
				return this.get(key.column());
			}
		}
		throw new IllegalArgumentException("No foreign key found");
	}
	
	@Override
	public String toString() {
		StringBuilder b = new StringBuilder(this.getFullname()).append(": ");
		for (_Column c : this) {
			b.append(c.getName()).append("=").append(c.getValue()).append(" ");
		}
		return b.toString();
	}
	
	@Override
	public int getIndex() {
		return this.index;
	}
	
	@Override
	public List<_Column> getKeys() {
		if (hasUniqueNumericIdColumn()) {
			return Utils.list(getUniqueNumericIdColumn());
		}
		if (!hasImmutableKeys()) {
			throw new IllegalStateException("Table " + getName() + " doesn't define any keys.");
		}
		List<_Column> list = new ArrayList<>();
		for (String colName : tableStructure.getImmutableKeys()) {
			list.add(this.get(colName));
		}
		return list;
	}
	
	@Override
	public boolean hasColumn(String name) {
		return tableStructure.containsColumn(name);
	}
	
	@Override
	public List<_Column> getColumns(String ... columnNames) {
		List<_Column> list = new ArrayList<>();
		for (String columnName : columnNames) {
			list.add(get(columnName));
		}
		return list;
	}
	
}
