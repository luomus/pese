<!-- lahiympariston pesat --> 

<#assign ROWS_PER_PAGE = 15>

<#if ( errors?size > 0) >
<#-- 
      <tr>
        <td>
          	<#assign errorfields = errors?keys>
          	<#list errorfields as errorfield> <span class="error"> ${texts[errorfield]!errorfield}: ${texts[errors[errorfield]]!errors[errorfield]} </span> <br /> </#list>
        </td>
      </tr>
-->      
<#else>
	  
	<#assign page = 0>
	  
	<#list results as r>
		<#if ((r_index) % ROWS_PER_PAGE) == 0>
	    	<#assign page = page + 1>
	        <table id="lahiymparistonPesat_${page}" class="infoContainer <#if page != 1>hidden</#if>">
	        <caption> Lähiympäristön&nbsp;pesät </caption>
            <tbody>
	    	<tr>
	    		<th>Pesä</th>
        		<th>Yht.koord.</th>
        		<th>Tyyppi</th>
        		<th>Pesä kork.</th>
        		<th>Puu kork.</th>
        		<th>Pöntön nro</th>
        		<th> &nbsp; </th>
	   		</tr>
	    </#if>
	    
	    <#if data.nestid == r.pesa.id> <tr class="highlightedRow"> <#else> <tr> </#if>
          <td> 
            <#if data.nestid != r.pesa.id> 
            	<a href="${system.baseURL}?page=tarkastus&amp;action=fill_for_update&amp;updateparameters.pesa.id=${r.pesa.id}">
            		<#if r.pesa.nimi != ""> ${r.pesa.nimi} (${r.pesa.id}) <#else> ${r.pesa.id} </#if>
            	</a>
            <#else>
            	<#if r.pesa.nimi != ""> ${r.pesa.nimi} (${r.pesa.id}) <#else> ${r.pesa.id} </#if>
            </#if>
          </td>
          <td>${r.pesa.yht_leveys} <br />  ${r.pesa.yht_pituus}</td>
          <td> ${ selections["pesa.pesa_sijainti"][r.pesa.pesa_sijainti] }</td>
          <td>${r.tarkastus.pesa_korkeus}</td>
          <td>${r.tarkastus.puu_korkeus}</td>
          <td>${r.pesa.pontto_nro}</td>
          <td><a target="_karttapaikka" href="https://asiointi.maanmittauslaitos.fi/karttapaikka/?share=customMarker&n=${r.pesa.eur_leveys}&amp;e=${r.pesa.eur_pituus}&amp;title=${r.pesa.nimi.getUrlEncodedValue()}(${r.pesa.id})">Kartalle</a></td>
        </tr>

	     <#if (r_index+1) % ROWS_PER_PAGE == 0 || !r_has_next>
   				<tr>
          			<th colspan="7" style="text-align: left;"> Löytyi yhteensä ${results?size} kappaletta. &nbsp; &nbsp;
            		<#if page != 1> 
              			<a href="javascript:void(0)" onclick="showPrevInfocontainerPage(${page})">&laquo; Edelliset</a> 
            		</#if>
            		<#if r_has_next>
            			<a href="javascript:void(0)" onclick="showNextInfocontainerPage(${page})">Seuraavat &raquo;</a>
            		</#if>
            		</th>
        		</tr>
       		</tbody>
			</table>
		</#if>
		
      </#list>
				

	   
</#if>
