package fi.hy.pese.rengastus.main.app;

import fi.hy.pese.framework.main.general.data._RowFactory;

public class RengastusDomainModelRowFactory implements _RowFactory<_RengastusDomainModelRow> {

	private final RengastusDatabaseStructure	rowStructure;

	public RengastusDomainModelRowFactory(RengastusDatabaseStructure rowStructure) {
		this.rowStructure = rowStructure;
	}

	@Override
	public String joins() {
		return " 1 = 1 ";
	}

	@Override
	public String orderby() {
		return "pvm DESC, nimirengas";
	}

	@Override
	public _RengastusDomainModelRow newRow() {
		return new RengastusDomainModelRow(rowStructure);
	}

	@Override
	public _RengastusDomainModelRow newRow(String prefix) {
		return new RengastusDomainModelRow(rowStructure, prefix);
	}

}
