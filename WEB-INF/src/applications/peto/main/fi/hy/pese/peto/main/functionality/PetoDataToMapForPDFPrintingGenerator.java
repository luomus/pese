package fi.hy.pese.peto.main.functionality;

import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.db.RowsToListResultHandler;
import fi.hy.pese.framework.main.db._DAO;
import fi.hy.pese.framework.main.general._PDFDataMapGenerator;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.main.general.data._Table;
import fi.hy.pese.peto.main.app.PetoConst;
import fi.luomus.commons.containers.Selection;
import fi.luomus.commons.pdf.PDFWriter;
import fi.luomus.commons.tipuapi.TipuAPIClient;
import fi.luomus.commons.tipuapi.TipuApiResource;
import fi.luomus.commons.xml.Document.Node;

public class PetoDataToMapForPDFPrintingGenerator implements _PDFDataMapGenerator {

	private static final String	TARKASTETTU_YES	= "K";
	private final _DAO<_PesaDomainModelRow>					dao;
	private final Map<String, Selection>	selections;
	private final TipuApiResource ringers;

	public PetoDataToMapForPDFPrintingGenerator(_DAO<_PesaDomainModelRow> dao, _Request<_PesaDomainModelRow> request) {
		this.dao = dao;
		this.selections = request.data().selections();
		this.ringers = request.getTipuApiResource(TipuAPIClient.RINGERS);
	}

	/**
	 * Tämä palauttaa tietyn tarkastuksen datat PDF:lle tulostettavaksi. Tämä ei palauta vain esitäytetyksi tarkoitettuja
	 * kenttiä vaan myös varsinaiset tarkastustiedot. Käytetään kun tuotetaan PDF:iä tarkastajan ihailtavaksi, esim tulospalvelussa.
	 * @param tarkastusid
	 * @return
	 * @throws Exception
	 */
	@Override
	public Map<String, String> tarkastusDataFor(String tarkastusid) throws Exception {
		Map<String, String> map = new HashMap<>();

		_PesaDomainModelRow tarkastus = getTarkastus(tarkastusid);
		String pesaid = tarkastus.getPesa().getUniqueNumericIdColumn().getValue();
		String vuosi = tarkastus.getTarkastus().get("vuosi").getValue();

		_PesaDomainModelRow prev_tarkastus = previousTarkastusFor(pesaid, vuosi);
		if (prev_tarkastus != null) {
			prevTarkInfoToMap(map, prev_tarkastus);
		}

		everythingFromRowToMap(map, tarkastus);
		coordinatesToMap(map, tarkastus);
		topBarValuesToMap(pesaid, map, tarkastus);
		rakLaji(map, tarkastus);
		seuraavaTarkastaja(map, tarkastus.getPesa());

		tarkVuosiToWhereItIsNeeded(vuosi, map);

		StringBuilder filename = new StringBuilder("TARKASTAJA_");
		filename.append(map.get("pesa.seur_tarkastaja")).append("_");
		filename.append(tarkastus.getColumn("pesa.kunta"));
		filename.append("_PESA_").append(pesaid).append("_").append(vuosi).append("_KAIKKI_TIEDOT");
		map.put(Const.FILENAME, filename.toString());

		return map;
	}

	private _PesaDomainModelRow getTarkastus(String tarkastusid) throws SQLException {
		_PesaDomainModelRow searchParams = dao.newRow();
		searchParams.getTarkastus().getUniqueNumericIdColumn().setValue(tarkastusid);
		List<_PesaDomainModelRow> results = new LinkedList<>();
		dao.executeSearch(searchParams, new RowsToListResultHandler(dao, results));
		_PesaDomainModelRow result = results.get(0);
		return result;
	}

	private _PesaDomainModelRow previousTarkastusFor(String pesaid, String beforeYear) throws SQLException {
		int lastAllowedYear = Integer.valueOf(beforeYear) - 1;
		_PesaDomainModelRow searchParams = dao.newRow();
		searchParams.getPesa().getUniqueNumericIdColumn().setValue(pesaid);
		searchParams.getTarkastus().get("tarkastettu").setValue(TARKASTETTU_YES);
		searchParams.getTarkastus().get("vuosi").setValue("1900-" + lastAllowedYear);
		List<_PesaDomainModelRow> results = new LinkedList<>();
		dao.executeSearch(searchParams, new RowsToListResultHandler(dao, results));
		if (results.size() < 1) return null;
		_PesaDomainModelRow result = results.get(0);
		return result;
	}

	/**
	 * This function is called to build the application specific data for PDF printing (esitäytetty tarkastuslomake).
	 * Basicly it takes values from database and puts them to a map. The map is later given to pdfWriter, which will
	 * look for values from the map that match the names of the fields on the pdf.
	 * For example forthere are several places where the year of inspection is printed out, so those
	 * are filled to the map to keys "ylapalkki_1_vuosi", "ylapalkki_1_vuosi"... and so on.
	 */
	@Override
	public Map<String, String> tarkastusDataFor(String pesaid, String year) throws Exception {
		Map<String, String> map = new HashMap<>();

		_PesaDomainModelRow prev_tarkastus = previousTarkastusFor(pesaid);
		prevTarkInfoToMap(map, prev_tarkastus);

		PetoConst.removeTarkastusValuesButKeepSpecific(prev_tarkastus);

		everythingFromRowToMap(map, prev_tarkastus);
		coordinatesToMap(map, prev_tarkastus);
		topBarValuesToMap(pesaid, map, prev_tarkastus);
		rakLaji(map, prev_tarkastus);
		seuraavaTarkastaja(map, prev_tarkastus.getPesa());

		tarkVuosiToWhereItIsNeeded(year, map);

		generateFilenameToMap(pesaid, year, map, prev_tarkastus);

		return map;
	}

	private void rakLaji(Map<String, String> map, _PesaDomainModelRow prev_tarkastus) {
		_Column rak_laji = prev_tarkastus.getPesa().get("rak_laji");
		if (rak_laji.hasValue()) {
			map.put("pesa.rak_laji", rak_laji.getValue());
		}
	}

	private void topBarValuesToMap(String pesaid, Map<String, String> map, _PesaDomainModelRow prev_tarkastus) {
		_Table pesa = prev_tarkastus.getPesa();
		String kulyh = pesa.get("kunta").getValue();
		String kuntanimi = selections.get(TipuAPIClient.MUNICIPALITIES).get(kulyh);
		map.put("kunta2", kuntanimi);
		map.put("ylapalkki_1_kunta", kuntanimi);
		map.put("ylapalkki_1_kyla", pesa.get("kyla").getValue());
		map.put("ylapalkki_1_pesanimi", pesa.get("nimi").getValue());
		map.put("ylapalkki_1_pesaid", pesaid);
		map.put("ylapalkki_2_pesaid", pesaid);
	}

	private Selection pesimistulokset = null;
	private Selection pesan_kunnot = null;
	private Selection lajit = null;

	private Selection pesimistuloksetSelection() {
		if (pesimistulokset == null) {
			pesimistulokset = selections.get(dao.newRow().getTarkastus().get("pesimistulos").getAssosiatedSelection());
		}
		return pesimistulokset;
	}

	private Selection pesanKunnatSelection() {
		if (pesan_kunnot == null) {
			pesan_kunnot = selections.get(dao.newRow().getKaynti().get("pesa_kunto").getAssosiatedSelection());
		}
		return pesan_kunnot;
	}

	@SuppressWarnings("unused")
	private Selection lajitSelection() {
		if (lajit == null) {
			lajit = selections.get(dao.newRow().getPesa().get("rak_laji").getAssosiatedSelection());
		}
		return lajit;
	}

	private void coordinatesToMap(Map<String, String> map, _PesaDomainModelRow result) {
		_Table pesa = result.getPesa();
		String koord_tyyppi = pesa.get("koord_ilm_tyyppi").getValue();
		String leveys = "";
		String pituus = "";
		if (koord_tyyppi.equals("Y")) {
			map.put("pesa.koord_ilm_tyyppi_Y", PDFWriter.SELECTED_CHECKBOX);
			leveys = pesa.get("yht_leveys").getValue();
			pituus = pesa.get("yht_pituus").getValue();
		} else if (koord_tyyppi.equals("E")) {
			map.put("pesa.koord_ilm_tyyppi_E", PDFWriter.SELECTED_CHECKBOX);
			leveys = pesa.get("eur_leveys").getValue();
			pituus = pesa.get("eur_pituus").getValue();
		} else if (koord_tyyppi.equals("A")) {
			leveys = pesa.get("yht_leveys").getValue();
			pituus = pesa.get("yht_pituus").getValue();
		}
		map.put("pesa.leveys", leveys);
		map.put("pesa.pituus", pituus);
	}

	private String buildPesanKuntoText(String kunto) {
		return pesanKunnatSelection().get(kunto);
	}

	private String buildLajiText(String laji) {
		//		StringBuilder text = new StringBuilder();
		//		text.append(laji).append(" - ").append(lajitSelection().get(laji));
		//		return text.toString();
		return laji;
	}

	private void everythingFromRowToMap(Map<String, String> map, _PesaDomainModelRow result) {
		tableToMap(map, result.getPesa());
		tableToMap(map, result.getTarkastus());
		for (_Table kaynti : result.getKaynnit()) {
			tableToMap(map, kaynti);
		}
	}

	private void tableToMap(Map<String, String> map, _Table table) {
		for (_Column c : table) {
			if (c.hasAssosiatedSelection()) {
				map.put(c.getFullname() + "_" + c.getValue(), PDFWriter.SELECTED_CHECKBOX);
				map.put(c.getAssosiatedSelection() + "_" + c.getValue().toUpperCase(), PDFWriter.SELECTED_CHECKBOX);
			}
			map.put(c.getFullname(), c.getValue());

		}
	}

	private void generateFilenameToMap(String pesaid, String year, Map<String, String> map, _PesaDomainModelRow result) {
		StringBuilder filename = new StringBuilder("TARKASTAJA_");
		filename.append(map.get("pesa.seur_tarkastaja")).append("_");
		filename.append(result.getColumn("pesa.kunta"));
		filename.append("_PESA_").append(pesaid).append("_").append(year);
		map.put(Const.FILENAME, filename.toString());
	}

	private void prevTarkInfoToMap(Map<String, String> map, _PesaDomainModelRow prev_tarkastus_row) {
		_Table tarkastus = prev_tarkastus_row.getTarkastus();
		Collection<_Table> kaynnit = prev_tarkastus_row.getKaynnit().values();
		if (!kaynnit.isEmpty()) {
			tarkPvmKunto(map, kaynnit);
		}
		pesimistulos(map, tarkastus);
		edellinenTarkastaja(map, tarkastus);
		pesivalaji(map, tarkastus);
	}

	private void edellinenTarkastaja(Map<String, String> map, _Table tarkastus) {
		String nro = tarkastus.get("tarkastaja").getValue();
		Node ringer = ringers.getById(nro);
		if (ringer == null) {
			map.put("edellinen_tark_tarkastaja", " Tuntematon ");
			return;
		}
		String name = ringer.getNode("firstname").getContents() + " " + ringer.getNode("lastname").getContents();
		map.put("edellinen_tark_tarkastaja", name + " (" + nro + ")");
	}

	private void seuraavaTarkastaja(Map<String, String> map, _Table pesa) {
		String nro = pesa.get("seur_tarkastaja").getValue();
		Node ringer = ringers.getById(nro);
		String name = ringer.getNode("firstname").getContents() + " " + ringer.getNode("lastname").getContents();
		String address = "";
		for (Node n : ringer.getNode("address")) {
			address += n.getContents() + " ";
		}
		map.put("tarkastaja_nro", nro);
		map.put("tarkastaja_nimi", name);
		map.put("tarkastaja_osoite", address);
		map.put("tarkastaja_puhelin", ringer.getNode("email").getContents());
		map.put("tarkastaja_email", ringer.getNode("mobile-phone").getContents());
	}

	private void pesimistulos(Map<String, String> map, _Table tarkastus) {
		_Column pesimistulos = tarkastus.get("pesimistulos");
		String prev_pesimistulos = pesimistulos.getValue();
		prev_pesimistulos = pesimistuloksetSelection().get(prev_pesimistulos);
		map.put("edellinen_tark_pesimistulos", prev_pesimistulos);
	}

	private void pesivalaji(Map<String, String> map, _Table tarkastus) {
		String laji = tarkastus.get("pesiva_laji").getValue();
		laji = buildLajiText(laji);
		map.put("edellinen_tark_laji", laji);
	}

	private void tarkPvmKunto(Map<String, String> map, Collection<_Table> kaynnit) {
		String tark_pvm = "";
		String pesa_kunto = "";
		for (_Table kaynti : kaynnit) {
			if (kaynti.get("pvm").hasValue()) {
				tark_pvm = kaynti.get("pvm").getValue();
			}
			if (kaynti.get("pesa_kunto").hasValue()) {
				pesa_kunto = kaynti.get("pesa_kunto").getValue();
			}
		}
		pesa_kunto = buildPesanKuntoText(pesa_kunto);
		map.put("edellinen_tark_pvm", tark_pvm);
		map.put("edellinen_tark_pesa_kunto", pesa_kunto);
	}

	private _PesaDomainModelRow previousTarkastusFor(String pesaid) throws SQLException {
		_PesaDomainModelRow searchParams = dao.newRow();
		searchParams.getPesa().getUniqueNumericIdColumn().setValue(pesaid);
		searchParams.getTarkastus().get("tarkastettu").setValue(TARKASTETTU_YES);
		List<_PesaDomainModelRow> results = new LinkedList<>();
		dao.executeSearch(searchParams, new RowsToListResultHandler(dao, results));
		_PesaDomainModelRow result = results.get(0);
		return result;
	}

	private void tarkVuosiToWhereItIsNeeded(String year, Map<String, String> map) {
		map.put("vuosi_1", year);
		map.put("vuosi_2", year);
		map.put("vuosi_3", year);
		map.put("tarkastus.vuosi", year);
	}

}
