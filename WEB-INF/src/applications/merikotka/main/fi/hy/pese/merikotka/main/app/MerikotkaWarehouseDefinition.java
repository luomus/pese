package fi.hy.pese.merikotka.main.app;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import fi.hy.pese.framework.main.general.WarehouseGenerationDefinitionBuilder;
import fi.hy.pese.framework.main.general.WarehouseRowGenerator.NestIsDead;
import fi.hy.pese.framework.main.general.WarehouseRowGenerator.NotInspected;
import fi.hy.pese.framework.main.general.WarehouseRowGenerator.ResolveAdditionalNestingSpecies;
import fi.hy.pese.framework.main.general.WarehouseRowGenerator.ResolveAtlasCode;
import fi.hy.pese.framework.main.general.WarehouseRowGenerator.ResolveCoordinates;
import fi.hy.pese.framework.main.general.WarehouseRowGenerator.ResolveDateAccuracy;
import fi.hy.pese.framework.main.general.WarehouseRowGenerator.ResolveLocality.LocalityKylaNimi;
import fi.hy.pese.framework.main.general.WarehouseRowGenerator.ResolveNestCount;
import fi.hy.pese.framework.main.general.WarehouseRowGenerator.ResolveSpecies;
import fi.hy.pese.framework.main.general.WarehouseRowGenerator.WarehouseGenerationDefinition;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.main.general.data._Table;
import fi.laji.datawarehouse.etl.models.dw.Coordinates;
import fi.laji.datawarehouse.etl.models.dw.Coordinates.Type;
import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException;
import fi.luomus.commons.containers.rdf.Qname;

public class MerikotkaWarehouseDefinition {

	private static WarehouseGenerationDefinition definition;

	public static WarehouseGenerationDefinition instance() {
		if (definition == null) {
			definition = init();
		}
		return definition;
	}

	public static WarehouseGenerationDefinition init() {
		return new WarehouseGenerationDefinitionBuilder()
				.sourceId(new Qname("KE.41"))
				.collectionId(new Qname("HR.50"))
				.nestIsDead(new NestIsDead() {
					@Override
					public boolean get(_PesaDomainModelRow row) {
						return MerikotkaConst.PESAN_KUNTO__DEAD.contains(row.getColumn("pesatarkastus.pesa_kunto").getValue());
					}
				})
				.notInspected(new NotInspected() {
					@Override
					public boolean get(_PesaDomainModelRow row) {
						return "8".equals(row.getColumn("pesatarkastus.tark_tapa").getValue());
					}
				})
				.yearColumnFullname("pesatarkastus.tark_vuosi")
				.dateColumnFullname("pesatarkastus.tark_pvm")
				.siteTypeColumnFullName("pesamuuttuva.sijainti")
				.municipalityColumnFullName("vuosi.pesan_kunta")
				.coordinates(new ResolveCoordinates() {
					@Override
					public Coordinates get(_Table pesa) throws NumberFormatException, IllegalArgumentException, DataValidationException {
						Coordinates c = new Coordinates(pesa.get("eur_leveys").getDoubleValue(), pesa.get("eur_pituus").getDoubleValue(), Type.EUREF);
						String koordTark = pesa.get("koord_tark").getValue();
						if ("0".equals(koordTark)) c.setAccuracyInMeters(10);
						else if ("1".equals(koordTark)) c.setAccuracyInMeters(100);
						else if ("2".equals(koordTark)) c.setAccuracyInMeters(200);
						else if ("3".equals(koordTark)) c.setAccuracyInMeters(300);
						else if ("4".equals(koordTark)) c.setAccuracyInMeters(400);
						else if ("S".equals(koordTark)) c.setAccuracyInMeters(1000);
						else c.setAccuracyInMeters(5000);
						return c;
					}
				})
				.locality(new LocalityKylaNimi("tarkka_sijainti", "pesanimi"))
				.dateAccuracy(new ResolveDateAccuracy() {
					@Override
					public DateAccuracy get(_Table tarkastus) {
						String v = tarkastus.get("tark_pvm_tark").getValue();
						if ("1".equals(v) || v.isEmpty()) return DateAccuracy.ACCURATE;
						if ("2".equals(v)) return DateAccuracy.MONTH;
						return DateAccuracy.YEAR;
					}
				})
				.nestingSpecies(new ResolveSpecies() {
					@Override
					public String get(_PesaDomainModelRow row) {
						String v = row.getTarkastus().get("muulaji").getValue();
						if (v.isEmpty()) return "HALALB";
						return v;
					}
				})
				.nestCount(new ResolveNestCount() {
					@Override
					public int get(_PesaDomainModelRow row) {
						String kunto = row.getColumn("pesatarkastus.pesa_kunto").getValue();
						String merkit = row.getColumn("pesatarkastus.nahdyt_merkit").getValue();
						if ("A".equals(merkit) && "KYUD".contains(kunto)) return 0;
						return 1;
					}
				})
				.atlasCode(new ResolveAtlasCode() {
					@Override
					public Qname get(_PesaDomainModelRow row) {
						String merkit = row.getColumn("pesatarkastus.nahdyt_merkit").getValue();
						return ATLAS_CODES.get(merkit);
					}
				})
				.additionalNestingSpecies(new ResolveAdditionalNestingSpecies() {
					@Override
					public Set<String> get(_Table tarkastus) {
						Set<String> species = new HashSet<>();
						if ("K".equals(tarkastus.get("muulaji_vastarakki").getValue())) species.add("MOTALB");
						String v = tarkastus.get("muulaji_rinnakkainen").getValue();
						if (!v.isEmpty()) species.add(v);
						return species;
					}
				})
				//.counts(null)
				//.isSuspecious(null)
				//.shouldSecure(null)
				.build();
	}

	private static final Map<String, Qname> ATLAS_CODES;
	static {
		ATLAS_CODES = new HashMap<>();
		ATLAS_CODES.put("D", new Qname("MY.atlasCodeEnum66"));
		ATLAS_CODES.put("E", new Qname("MY.atlasCodeEnum66"));
		ATLAS_CODES.put("F", new Qname("MY.atlasCodeEnum63"));
		ATLAS_CODES.put("H", new Qname("MY.atlasCodeEnum66"));
		ATLAS_CODES.put("J", new Qname("MY.atlasCodeEnum66"));
		ATLAS_CODES.put("K", new Qname("MY.atlasCodeEnum66"));
		ATLAS_CODES.put("L", new Qname("MY.atlasCodeEnum66"));
		ATLAS_CODES.put("M", new Qname("MY.atlasCodeEnum71"));
		ATLAS_CODES.put("N", new Qname("MY.atlasCodeEnum71"));
		ATLAS_CODES.put("P", new Qname("MY.atlasCodeEnum71"));
		ATLAS_CODES.put("Q", new Qname("MY.atlasCodeEnum82"));
		ATLAS_CODES.put("R", new Qname("MY.atlasCodeEnum82"));
		ATLAS_CODES.put("T", new Qname("MY.atlasCodeEnum71"));
		ATLAS_CODES.put("V", new Qname("MY.atlasCodeEnum82"));
		ATLAS_CODES.put("Y", new Qname("MY.atlasCodeEnum66"));
	}

}
