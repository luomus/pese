-- peto

update tarkastus set kommentti_edellinen_laji = coalesce(
(select pesiva_laji
from tarkastus edellinen
where edellinen.pesa = tarkastus.pesa
and edellinen.pesiva_laji is not null
and edellinen.vuosi < tarkastus.vuosi 
order by vuosi desc
fetch first 1 row only), kommentti_edellinen_laji)
;
