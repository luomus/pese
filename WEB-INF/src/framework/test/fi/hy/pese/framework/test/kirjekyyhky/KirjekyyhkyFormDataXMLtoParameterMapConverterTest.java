package fi.hy.pese.framework.test.kirjekyyhky;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;

import fi.hy.pese.framework.main.general.data.Data;
import fi.hy.pese.framework.main.general.data.DatabaseTableStructure;
import fi.hy.pese.framework.main.general.data.ParameterMap;
import fi.hy.pese.framework.main.general.data.PesaDomainModelDatabaseStructure;
import fi.hy.pese.framework.main.general.data.PesaDomainModelRowFactory;
import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.main.general.data._Table;
import fi.hy.pese.framework.test.TestConfig;
import fi.luomus.commons.config.Config;
import fi.luomus.commons.kirjekyyhky.XmlToFormDataUtil;
import fi.luomus.commons.kirjekyyhky.XmlToFormDataUtil.MalformedFormDataException;
import fi.luomus.commons.utils.FileUtils;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class KirjekyyhkyFormDataXMLtoParameterMapConverterTest {

	public static Test suite() {
		return new TestSuite(KirjekyyhkyFormDataXMLtoParameterMapConverterTest.class.getDeclaredClasses());
	}

	public static class WhenCreatingAndUsingTheSessionHandler extends TestCase {

		private final Config config = TestConfig.getConfig();
		private final File folder = new File(config.baseFolder() + "kirjekyyhky-xml-tests/");

		@Override
		protected void setUp() throws Exception {

		}

		//		<data>
		//		<pesa.id>200</pesa.id>
		//		<pesa.nimi>Ku. Ingersholm 4, E om dalgång &gt; &lt; &amp; &quot; &apos;</pesa.nimi>
		//		<pesa.loyto_pvm>03.2.2010</pesa.loyto_pvm>
		//		<pesa.korkeus>5,22</pesa.korkeus>
		//		<pesa.leveys>15.0502</pesa.leveys>
		//		<pesa.kyla>  </pesa.kyla>
		//		</data>

		public void test_() throws Exception {
			String xml = FileUtils.readContents(new File(folder, "data1.xml"));
			Map<String, String> params = XmlToFormDataUtil.readFormData(xml, "mysystemid").getData();
			assertEquals(5, params.size());
			assertEquals("200", params.get("pesa.id"));
			assertEquals("Ku. Ingersholm 4, E om dalgång > < & \" '", params.get("pesa.nimi"));
			assertEquals("03.2.2010", params.get("pesa.loyto_pvm"));
			assertEquals("5,22", params.get("pesa.korkeus"));
			assertEquals("15.0502", params.get("pesa.leveys"));
		}

		public void test__2() throws Exception {
			String xml = FileUtils.readContents(new File(folder, "data1.xml"));

			Map<String, String> params = new HashMap<>();
			for (Map.Entry<String, String> e : XmlToFormDataUtil.readFormData(xml, "mysystemid").getData().entrySet()) {
				params.put("updateparameters." + e.getKey(), e.getValue());
			}

			ParameterMap parameterMap = new ParameterMap(params);

			PesaDomainModelDatabaseStructure structure = new PesaDomainModelDatabaseStructure();
			DatabaseTableStructure pesaStructure = new DatabaseTableStructure("pesa");
			pesaStructure.addColumn("id", _Column.INTEGER, 5);
			pesaStructure.addColumn("nimi", _Column.VARCHAR, 50);
			pesaStructure.addColumn("loyto_pvm", _Column.DATE);
			pesaStructure.addColumn("korkeus", _Column.DECIMAL, 5, 3);
			pesaStructure.addColumn("leveys", _Column.DECIMAL, 5, 3);
			pesaStructure.addColumn("kyla", _Column.VARCHAR, 20);
			structure.setPesa(pesaStructure);

			Data<_PesaDomainModelRow> data = new Data<>(new PesaDomainModelRowFactory(structure, "", ""));

			parameterMap.convertTo(data);

			_Table pesa = data.updateparameters().getPesa();
			assertEquals("200", pesa.get("id").getValue());
			assertEquals("Ku. Ingersholm 4, E om dalgång > < & \" '",  pesa.get("nimi").getValue());
			assertEquals("03.2.2010",  pesa.get("loyto_pvm").getValue());
			assertEquals("5,220",  pesa.get("korkeus").getValue());
			assertEquals("15,050",  pesa.get("leveys").getValue());
			assertEquals("",  pesa.get("kyla").getValue());
		}

		public void test__3() throws Exception {
			PrintStream defaultSystemErr = System.err;
			System.setErr(new PrintStream(new OutputStream() {
				@Override
				public void write(int arg0) throws IOException {
				}
			}));

			try {
				String xml = FileUtils.readContents(new File(folder, "data2.xml"));
				XmlToFormDataUtil.readFormData(xml, "mysystemid");
				fail("Should throw MalformedFormDataException");
			} catch (MalformedFormDataException e) {
				String expected = "" +
						"Form data was malformed: <?xml version='1.0' encoding='utf-8'?>\n" +
						"<form-data type=\"merikotkalomake\" prefilled=\"yes\" id=\"1\">\n" +
						"\n" +
						"	<data>\n" +
						"		<pesa.id> <> </pesa.id>\n" +
						"	</data>\n" +
						"	\n" +
						"</form-data>\n" +
						"";
				assertEquals(expected, e.getMessage());
			} finally {
				System.setErr(defaultSystemErr);
			}

		}


	}


}
