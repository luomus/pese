package fi.hy.pese.framework.main.db;

import java.sql.SQLException;

public interface _ResultSet {

	boolean next() throws SQLException;

	String getString(int i) throws SQLException;

	String getString(String columnLabel) throws SQLException;

	java.sql.Date getDate(int i) throws SQLException;

	java.sql.Date getDate(String columnLabel) throws SQLException;

}
