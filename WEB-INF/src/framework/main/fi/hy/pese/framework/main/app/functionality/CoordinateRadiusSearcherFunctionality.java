package fi.hy.pese.framework.main.app.functionality;

import java.util.LinkedList;
import java.util.List;

import fi.hy.pese.framework.main.app.FunctionalityExecutor;
import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.app.functionality.validate.SearchFunctionalityValidator;
import fi.hy.pese.framework.main.app.functionality.validate._Validator;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;

public class CoordinateRadiusSearcherFunctionality extends BaseFunctionality<_PesaDomainModelRow> {
	
	private final SearchFunctionality searchFunctionality;

	public CoordinateRadiusSearcherFunctionality(_Request<_PesaDomainModelRow> request, SearchFunctionality searchFunctionality) {
		super(request);
		this.searchFunctionality = searchFunctionality;
	}

	@Override
	public void preProsessingHook() throws Exception {
		_Column yhtLeveys = data.searchparameters().getPesa().get("yht_leveys");
		_Column yhtPituus = data.searchparameters().getPesa().get("yht_pituus");
		_Column eurLeveys = data.searchparameters().getPesa().get("eur_leveys");
		_Column eurPituus = data.searchparameters().getPesa().get("eur_pituus");

		if (data.contains("yht_leveys") && data.contains("yht_pituus")) {
			yhtLeveys.setValue(data.get("yht_leveys"));
			yhtPituus.setValue(data.get("yht_pituus"));
		} else {
			eurLeveys.setValue(data.get("eur_leveys"));
			eurPituus.setValue(data.get("eur_pituus"));
		}

		if (!data.contains("nestid")) {
			data.set("nestid", "");
		}
	}
	
	@Override
	public _Validator validator() {
		return new SearchFunctionalityValidator<>(request);
	}

	@Override
	public void afterSuccessfulValidation() throws Exception {
		data.setAction(Const.SEARCH);
		FunctionalityExecutor.execute(searchFunctionality);
		List<_PesaDomainModelRow> results = new LinkedList<>();
		String prevPesaId = "";
		for (_PesaDomainModelRow row : data.results()) {
			String pesaId = row.getPesa().getUniqueNumericIdColumn().getValue();
			if (!prevPesaId.equals(pesaId)) {
				results.add(row);
				prevPesaId = pesaId;
			}
		}
		data.clearResults();
		data.setResults(results);
	}

	@Override
	public void afterFailedValidation() throws Exception {
	}
	
	@Override
	public void postProsessingHook() throws Exception {
		data.setPage(Const.COORDINATE_RADIUS_SEARCH_PAGE);
	}

}
