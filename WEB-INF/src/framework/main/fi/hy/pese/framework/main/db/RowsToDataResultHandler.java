package fi.hy.pese.framework.main.db;

import fi.hy.pese.framework.main.general.data._Data;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;

public class RowsToDataResultHandler extends RowCombiningResultHandler {

	public static final int	UNLIMITED	= -1;
	private final _Data<_PesaDomainModelRow>			data;
	private final int			maxCount;
	private int					count		= 0;

	public RowsToDataResultHandler(_Data<_PesaDomainModelRow> data, _DAO<_PesaDomainModelRow> dao) {
		this(data, dao, UNLIMITED);
	}

	/**
	 * Creates a new result handler which combines "inspection rows" (PeSe domain model row) from resultset rows.
	 * @param data
	 * @param dao
	 * @param maxCount Maximum number of rows to put into data. -1 = unlimited
	 */
	public RowsToDataResultHandler(_Data<_PesaDomainModelRow> data, _DAO<_PesaDomainModelRow> dao, int maxCount) {
		super(dao);
		this.data = data;
		this.maxCount = maxCount;
	}

	@Override
	protected boolean handle(_PesaDomainModelRow row) {
		data.newResultRow(row);
		if (maxCount == UNLIMITED) return true;
		if (++count > maxCount - 1) {
			data.addToNoticeTexts("max_count_reached");
			return false;
		}
		return true;
	}

}
