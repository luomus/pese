function showNextInfocontainerPage(i) {
    $("#lahiymparistonPesat_" + i).hide();
    $("#lahiymparistonPesat_" + (i+1)).show();
}

function showPrevInfocontainerPage(i) {
    $("#lahiymparistonPesat_" + i).hide();
    $("#lahiymparistonPesat_" + (i-1)).show();
}

function getLahiymparistonPesat() {
	url = '?page=coordinate_radius_search';
	url += '&nestid=' + nestID.value;
	if (yhtLeveys != undefined) url += '&yht_leveys=' + yhtLeveys.value;
	if (yhtPituus != undefined) url += '&yht_pituus=' + yhtPituus.value;
	if (eurLeveys != undefined) url += '&eur_leveys=' + eurLeveys.value;
	if (eurPituus != undefined) url += '&eur_pituus=' + eurPituus.value;
	url += '&CoordinateSearchRadius=' + radius.value;
	$.get( url, function(data) {
		if (data.startsWith("<!-- lahiympariston pesat -->")) {
			document.getElementById('lahiymparistonPesat').innerHTML = data;
			//$("#lahiymparistonPesat").html(data);
		} else {
			// login page or something else received as a result....
		}
	}
	);
}


String.prototype.startsWith = function(str){
	return (this.match("^"+str)==str);
}

String.prototype.trim = function () {
    return this.replace(/^\s*/, "").replace(/\s*$/, "");
}

//Function selects the value given as a parameter from a HTML-select element  
//identified by the given id.
//Returns true if value found from selector, false if not.
function selectValue(value, selectorElementId) { 
	var selector = document.getElementById(selectorElementId);
	for (var i = 0; i<selector.options.length; i++) {
		if (selector.options[i].value == value) {
			selector.options[i].selected = true;
			return true;
		}
	}
	return false;
}


//Function sets the value of the HTML-input element identified by the given id.

function setValue(value, inputElementId) { 
	document.getElementById(inputElementId).value = value;
}


//Called when the user selects a value from HTML-select element. Sets
//the value of the corresponding HTML-input element.

function syncInputElementWithSelectElement(inputElementId, selectorElementId) { 
	var selector = document.getElementById(selectorElementId);
	var value = selector.options[selector.selectedIndex].value;
	setValue(value, inputElementId);
	document.getElementById(inputElementId).className = '';
}


//Called when the user enters data to HTML-input element. Sets
//the value of the corresponding HTML-select element.
//If the entered value does not match an option in the select element,
//sets the class of the selector to "errorfield".

function syncSelectElementWithInputElement(inputElementId, selectorElementId) {
	var value = document.getElementById(inputElementId).value.toUpperCase();
	document.getElementById(inputElementId).value = value;
	if ( selectValue(value, selectorElementId) ) {
		document.getElementById(inputElementId).className = '';
	} else {
		document.getElementById(inputElementId).className = 'errorfield';
	}
}




var visibilityStates = new Array();

function updateVisibilities() {
	for (var id in visibilityStates) {
		var row_number = 1;
		var e;
		while ( (e = document.getElementById('result_row_'+id+'_'+row_number) ) != undefined) {
			if (visibilityStates[id] == 'show') {
				e.className = 'more_results';
			} else {
				e.className = 'hidden';
			}
			row_number++;
		}
	}
}

function changeVisibility(id, moreText, lessText) {
	var change_visibility_link_element = document.getElementById('visibility_link_'+id);
	if (visibilityStates[id] == undefined || visibilityStates[id] == 'hide') {
		visibilityStates[id] = 'show';
		change_visibility_link_element.innerHTML = lessText;
	} else {
		visibilityStates[id] = 'hide';
		change_visibility_link_element.innerHTML = moreText;
	}
	updateVisibilities();
}

var currently_showing_all = false;

function changeVisibilityOfAll(ids, showAllText, hideAllText, moreText, lessText) {
	var ids = ids.split(',');
	var prev = '';
	var change_visibility_link_element;

	for (var id in ids) {
		id = ids[id];
		if (id == prev) continue;
		if (id == '') continue;
		change_visibility_link_element = document.getElementById('visibility_link_'+id);
		if (currently_showing_all) {
			visibilityStates[id] = 'hide';
			change_visibility_link_element.innerHTML = moreText;
		} else {
			visibilityStates[id] = 'show';
			change_visibility_link_element.innerHTML = lessText;
		}

		prev = id;
	}

	var a = document.getElementById('showAll');
	if (currently_showing_all) 
		a.innerHTML = showAllText; 
	else 
		a.innerHTML = hideAllText;
	currently_showing_all = !currently_showing_all;
	updateVisibilities();
}


//---------------------------------------------------------------------
//Function asks a confirmation if sending a search with no parameters
//---------------------------------------------------------------------
function confirmSearch(form, confirmationText) {
	for (var element_index=0; element_index < form.elements.length; element_index++) {
		var element = form.elements[element_index];
		if (element.name == 'CoordinateSearchRadius') continue;
		if (element.name == 'NestCondition') continue;
		if (element.name == 'yyyy') continue;
		if (element.type == 'text') { 
			if (element.value != '' && element.value != ' ') { return true; } 
		}
		else if (element.type == 'select-one' || element.type == 'select-multiple') {
			if (element.selectedIndex > 0) { return true; } 
		} 
	}
	return (confirm(confirmationText));
}



//-----------------------------------------------------------------------
//Used by application defined generateAdvancedSearch(). Makes one table's selected fields visible, 
//or hides them if the field is not selected. 
//-----------------------------------------------------------------------
function generateAdvancedSearch(table) {
  generateAdvancedSearchInputs(table);
  var selectedFields = '';
  $(".fieldSelection option:selected").each(function () {
		selectedFields += $(this).val() + ',';
	});
  document.getElementById('advanced_search_table').className = '';
  document.getElementById('selectedFields').value = selectedFields; 
}

function generateAdvancedSearchInputs(table) {
	$("#fieldSelection_"+table+" option:selected").each(function () {
		var escapedName = $(this).val().replace('.', '\\.');
		$("#advanced_search_field_"+escapedName).removeClass('hidden');
	});
	$("#fieldSelection_"+table+" option:not(:selected)").each(function () {
		var escapedName = $(this).val().replace('.', '\\.');
		var searchparamFieldContainer = $("#advanced_search_field_"+escapedName);
		searchparamFieldContainer.addClass('hidden');
		searchparamFieldContainer.find("input").val('');
		searchparamFieldContainer.find("select").val('');
		searchparamFieldContainer.find("select").trigger("liszt:updated");
	});
}



//--------------------------------------------------------------------------------
//The net. Adding and removing ids to the net.   
//var net[id] = undefined or 0 if id not in the net or <id> if id is in the net
//--------------------------------------------------------------------------------
var net = new Array();

function toNet(id) { 
	if (net[id] == undefined) net[id] = id;
	else if (net[id] == 0)    net[id] = id; 
	else net[id] = 0;
}

function netIsEmpty() {
	for (var i in net) {
		var value = net[i];
		if (value != 0) return false;
	}
	return true;
}

var selectAllState = true;

//------------------------------
//Selects all ids to net
//------------------------------
var NEST_ENTRY = 'nest_entry_';

function selectAll() {
	if (selectAllState) net = new Array();
	var elements = document.getElementsByTagName('input')
	for (var element_index=0; element_index < elements.length; element_index++) {
		var element = elements[element_index];
		if (element.name.slice(0, NEST_ENTRY.length) == NEST_ENTRY) { 
			element.checked = selectAllState;
			var id = element.name.slice(NEST_ENTRY.length);
			toNet(id);
		}
	}
	selectAllState = !selectAllState;
}

//--------------------------------------------------------------------------------------------------------
//The contents of the net separated with '|'-char as value of the given element
//--------------------------------------------------------------------------------------------------------
function getIdsFromNetTo(element) {
	var searchparam = '';
	for (var i in net) {
		var id = net[i];
		if (id != 0)
			searchparam += id + '|';
	}
	if (searchparam == '') { searchparam == '9999999' } 
	else { searchparam = searchparam.slice(0, -1) }

	element.value = searchparam;
}      

function kirjekyyhkyReceiveValidateNestID(element) {
	var input = $(element).parent().find('input').filter(':first');
	if (input.val().trim() == '') {
		alert("Täytä kohdepesän ID");
		return false;
	}
	return true;
}

// Lomakkeen vastaanottajat
function addVastaanottaja() {
	var henkilot = $("#lomake_vastaanottajat").find(".henkilot").first();
	var copy = henkilot.find("input").first().clone();
	copy.val("");
	copy.hide();
	copy.autocomplete({ source: lomakeVastaanottajat, minLength: 3 });
	henkilot.append(copy);
	copy.fadeIn('slow');
}
$(function() {
    $("#lomake_vastaanottajat").each(function() { 
    	$(this).find("input").autocomplete({ source: lomakeVastaanottajat, minLength: 3 });
    });
});


