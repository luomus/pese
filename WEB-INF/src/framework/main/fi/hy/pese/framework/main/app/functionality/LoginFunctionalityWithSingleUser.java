package fi.hy.pese.framework.main.app.functionality;

import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.app.functionality.validate.LoginValidatorWithSingleUserFromConfig;
import fi.hy.pese.framework.main.app.functionality.validate._Validator;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data._Row;

public class LoginFunctionalityWithSingleUser<T extends _Row> extends AbstractLoginFunctionality<T> {

	private final String	username;
	private final String	password;

	public LoginFunctionalityWithSingleUser(_Request<T> request) {
		super(request);
		this.username = request.config().get(Const.LOGIN_USERNAME);
		this.password = request.config().get(Const.LOGIN_PASSWORD);
	}

	public LoginFunctionalityWithSingleUser(_Request<T> request, String username, String password) {
		super(request);
		this.username = username;
		this.password = password;
	}

	@Override
	public _Validator validator() {
		return new LoginValidatorWithSingleUserFromConfig<>(request, username, password);
	}

}
