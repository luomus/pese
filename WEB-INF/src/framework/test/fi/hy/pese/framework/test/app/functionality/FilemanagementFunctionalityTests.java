package fi.hy.pese.framework.test.app.functionality;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import fi.hy.pese.framework.main.app.BaseFactoryForApplicationSpecificationFactory;
import fi.hy.pese.framework.main.app.FunctionalityExecutor;
import fi.hy.pese.framework.main.app.Request;
import fi.hy.pese.framework.main.app.RequestImpleData;
import fi.hy.pese.framework.main.app._Functionality;
import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.app.functionality.FilemanagementFunctionality;
import fi.hy.pese.framework.main.app.functionality.FilemanagementFunctionality.File;
import fi.hy.pese.framework.main.app.functionality.FilemanagementFunctionality._FileListGenerator;
import fi.hy.pese.framework.main.db._DAO;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data.Data;
import fi.hy.pese.framework.main.general.data._Data;
import fi.hy.pese.framework.main.general.data._Row;
import fi.hy.pese.framework.test.RowFactory;
import fi.hy.pese.framework.test.TestConfig;
import fi.hy.pese.framework.test.db.DAOStub;
import fi.luomus.commons.config.Config;
import fi.luomus.commons.containers.Selection;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class FilemanagementFunctionalityTests {

	public static Test suite() {
		return new TestSuite(FilemanagementFunctionalityTests.class.getDeclaredClasses());
	}

	public static class WhenUsingFilemanagementFunctionality extends TestCase {

		private class FilemanagementFunctionalityTestFactory extends BaseFactoryForApplicationSpecificationFactory<_Row> {
			public FilemanagementFunctionalityTestFactory() throws FileNotFoundException {
				super();
			}

			@Override
			public _Functionality<_Row> functionalityFor(String page, _Request<_Row> request) {
				return null;
			}

			@Override
			public _DAO<_Row> dao(String userId) {
				return new DAOStub<>();
			}

			@Override
			public Config config() {
				try {
					return TestConfig.getConfig();
				} catch (Exception e) {
					fail();
					e.printStackTrace();
				}
				return null;
			}

			@Override
			protected Map<String, Selection> fetchSelections(_DAO<_Row> dao) throws Exception {
				return null;
			}
		}

		private static class FakeFileList implements _FileListGenerator {
			@Override
			public List<File> pdfFiles() {
				List<File> producedPdfFiles = new ArrayList<>();
				producedPdfFiles.add(new File("1.pdf", "2009-05-05 11:52:20"));
				producedPdfFiles.add(new File("2.pdf", "2049-05-05 11:52:20"));
				return producedPdfFiles;
			}

			@Override
			public List<File> reportFiles() {
				List<File> producedReportFiles= new ArrayList<>();
				producedReportFiles.add(new File("1.txt", "2049-05-05 11:52:20"));
				return producedReportFiles;
			}
		}

		private _Data<_Row>		data;
		private _Request<_Row>	request;
		private _Functionality<_Row> filemanagementFunctionality;
		private FakeHttpRedirected fakeRedirecter;

		@Override
		protected void setUp() throws Exception {
			fakeRedirecter = new FakeHttpRedirected();
			data = new Data<>(new RowFactory());
			FilemanagementFunctionalityTestFactory factory = new FilemanagementFunctionalityTestFactory();
			RequestImpleData<_Row> reqData = new RequestImpleData<>();
			reqData.setData(data).setDao(factory.dao("userid")).setRedirecter(fakeRedirecter).setApplication(factory);
			request = new Request<>(reqData);
			data.setPage(Const.FILEMANAGEMENT_PAGE);
		}

		public void test____if_no__produced_reports_or_pdf_files() throws Exception {
			filemanagementFunctionality = new FilemanagementFunctionality<>(request, new _FileListGenerator() {
				@Override
				public List<File> reportFiles() {
					return new LinkedList<>();
				}
				@Override
				public List<File> pdfFiles() {
					return new LinkedList<>();
				}
			});
			FunctionalityExecutor.execute(filemanagementFunctionality);
			assertFalse(data.getLists().containsKey(FilemanagementFunctionality.PRODUCED_PDF_FILES));
			assertFalse(data.getLists().containsKey(FilemanagementFunctionality.PRODUCED_REPORT_FILES));
		}

		public void test____it_puts_to_data_list_of_produced_reports_and_pdf_files() throws Exception {
			filemanagementFunctionality = new FilemanagementFunctionality<>(request, new FakeFileList());
			FunctionalityExecutor.execute(filemanagementFunctionality);
			assertEquals(2, data.getLists().get(FilemanagementFunctionality.PRODUCED_PDF_FILES).size());
			assertEquals(1, data.getLists().get(FilemanagementFunctionality.PRODUCED_REPORT_FILES).size());
			Object o = data.getLists().get(FilemanagementFunctionality.PRODUCED_PDF_FILES).get(0);
			assertTrue(o instanceof File);
			File f = (File) o;
			assertEquals("1.pdf", f.getName());
			assertEquals("2009-05-05 11:52:20", f.getDate());
		}

		public void test___when_asking_to_show_a_file__doesnt_do_lists() throws Exception {
			filemanagementFunctionality = new FilemanagementFunctionality<>(request, new FakeFileList());
			data.setAction(Const.SHOW);
			data.set(FilemanagementFunctionality.FOLDER, FilemanagementFunctionality.PDF_FOLDER);
			data.set(FilemanagementFunctionality.FILENAME, "1.pdf");
			FunctionalityExecutor.execute(filemanagementFunctionality);
			assertFalse(data.getLists().containsKey(FilemanagementFunctionality.PRODUCED_PDF_FILES));
			assertFalse(data.getLists().containsKey(FilemanagementFunctionality.PRODUCED_REPORT_FILES));
		}

		public void test___when_asking_to_show_a_file__if_no_params_given__validator_gives_errors___generates_lists() throws Exception {
			filemanagementFunctionality = new FilemanagementFunctionality<>(request, new FakeFileList());
			data.setAction(Const.SHOW);
			FunctionalityExecutor.execute(filemanagementFunctionality);
			assertEquals(2, data.getLists().get(FilemanagementFunctionality.PRODUCED_PDF_FILES).size());
			assertEquals(1, data.getLists().get(FilemanagementFunctionality.PRODUCED_REPORT_FILES).size());
		}

		public void test___when_asking_to_show_a_file() throws Exception {
			filemanagementFunctionality = new FilemanagementFunctionality<>(request, new FakeFileList());
			data.setAction(Const.SHOW);
			data.set(FilemanagementFunctionality.FOLDER, FilemanagementFunctionality.PDF_FOLDER);
			data.set(FilemanagementFunctionality.FILENAME, "1.pdf");
			FunctionalityExecutor.execute(filemanagementFunctionality);
			String uri = fakeRedirecter.redirectedTo;
			String expected = request.config().baseURL()+"/File/1.pdf?token=";
			assertTrue("Redirect uri=" +uri+" should start with " + expected, uri.startsWith(expected));
		}

	}

}
