package fi.hy.pese.framework.main.general.data;

import java.util.HashMap;
import java.util.Map;

import fi.hy.pese.framework.main.general.consts.Const;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.Utils;

public class ParameterMap {

	private final Map<String, String>	params;

	public Map<String, String> remainingParams() {
		return params;
	}

	public ParameterMap(Map<String, String> params) {
		this.params = params;
	}

	public boolean containsKey(String key) {
		return params.containsKey(key);
	}

	public String getAndRemove(String key) {
		String value = params.remove(key);
		if (value == null) {
			value = "";
		}
		return value;
	}

	public _Row convertTabledata(_Row row) {
		fromParamsToRows(row, null);
		return row;
	}

	public void convertTo(_Data<? extends _Row> data) {
		pageFromParams(data);
		actionFromParamsToData(data);
		tablerelatedDataFromParasToData(data);
		bypassedWarningsToData(data);
		restOfTheDataFromParamsToData(data);
	}

	private void bypassedWarningsToData(_Data<? extends _Row> data) {
		if (params.containsKey(Const.BYPASSED_WARNINGS)) {
			String bypassedWarnings = params.get(Const.BYPASSED_WARNINGS);
			for (String field : Utils.valuelist(bypassedWarnings)) {
				field = field.trim();
				if (field.length() > 0) {
					data.bypassWarning(field);
				}
			}
		}
	}

	private void restOfTheDataFromParamsToData(_Data<? extends _Row> data) {
		for (Map.Entry<String, String> e : params.entrySet()) {
			data.set(e.getKey(), e.getValue());
		}
	}

	private void tablerelatedDataFromParasToData(_Data<? extends _Row> data) {
		fromParamsToRows(data.searchparameters(), data.updateparameters());
	}

	private void fromParamsToRows(_Row r1, _Row r2) {
		Map<String, String> copy = new HashMap<>(params);
		for (Map.Entry<String, String> e : copy.entrySet()) {
			if (e.getKey().startsWith(r1.getPrefix())) {
				fromEntryToTables(e.getKey(), r1);
			} else if (r2 != null && e.getKey().startsWith(r2.getPrefix())) {
				fromEntryToTables(e.getKey(), r2);
			}
		}
	}

	private void fromEntryToTables(String field, _Row row) {
		if (!field.contains(".")) return;

		String columnFullname = field.substring(row.getPrefix().length() + 1 );
		if (!columnFullname.contains(".")) return;

		if (columnFullname.endsWith("."+Const.YEAR) || columnFullname.endsWith("."+Const.MONTH) || columnFullname.endsWith("."+Const.DAY)) {
			columnFullname = columnFullname.substring(0, columnFullname.lastIndexOf('.'));
		}

		String tableFullname = columnFullname.substring(0, columnFullname.lastIndexOf('.'));
		if (!row.containsTable(tableFullname)) return;

		_Table t = row.getTableByName(tableFullname);
		for (_Column c : t) {
			valueFromParamsToColumn(c, row.getPrefix());
		}
	}

	private void valueFromParamsToColumn(_Column column, String prefix) {
		if (column.getDatatype() == _Column.DATE) {
			String year = dateFieldFromParams(column, prefix, Const.YEAR);
			String month = dateFieldFromParams(column, prefix, Const.MONTH);
			String day = dateFieldFromParams(column, prefix, Const.DAY);
			if (atLeastOneOfDateFieldsSet(year, month, day)) {
				column.setValue(DateUtils.catenateDateString(day, month, year));
				return;
			}
		}
		String value = getAndRemove(prefix + "." + column.getFullname());
		if (value.length() > 0) {
			column.setValue(value);
		}
	}

	private boolean atLeastOneOfDateFieldsSet(String year, String month, String day) {
		return !year.equals("") || !month.equals("") || !day.equals("");
	}

	private String dateFieldFromParams(_Column column, String prefix, String postfix) {
		String value = getAndRemove(prefix + "." + column.getFullname() + "." + postfix);
		if (value == null) value = "";
		return value;
	}

	private void actionFromParamsToData(_Data<? extends _Row> data) {
		if (params.containsKey(Const.ACTION)) {
			data.setAction(getAndRemove(Const.ACTION));
		} else {
			data.setAction("");
		}
	}

	private void pageFromParams(_Data<? extends _Row> data) {
		String page = null;
		if (params.containsKey(Const.PAGE)) {
			page = getAndRemove(Const.PAGE);
		}
		data.setPage(page);
	}
}
