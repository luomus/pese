package fi.hy.pese.framework.main.general.data;

import java.util.List;
import java.util.Map;
import java.util.Set;

import fi.luomus.commons.containers.Selection;
import fi.luomus.commons.languagesupport.LocalizedTextsContainer;

/**
 * Request data, passed on to functinality and finally to view
 */
public interface _Data<T extends _Row> {

	String getInputParametersForDebugging();

	boolean hasWarnings(_Column c);

	boolean hasErrors(_Column c);

	/**
	 * Does misc data contain given key
	 * @param key
	 * @return true if contains, false if not
	 */
	boolean contains(String key);

	/**
	 * Misc data, "data.table", "data.login_passwod" ...
	 */
	String get(String key);

	/**
	 * Get all misc data; "data.table", "data.login_password" ...
	 * @return
	 */
	Map<String, String> getAll();

	/**
	 * Set misc data; "data.table", "data.login_password" ...
	 * @param key
	 * @param value
	 */
	void set(String key, String value);

	/**
	 * Add to misc lists; "edellisetTarkastukset", ...
	 * @param list
	 * @param values
	 */
	void addToList(String list, Object o);

	/**
	 * Misct lists; "edellisetTarkastukset", ...
	 * @return
	 */
	Map<String, List<Object>> getLists();

	/**
	 * Table related searchparameters received from UI
	 * @return
	 */
	T searchparameters();

	/**
	 * Table related update/etc parameters received from UI
	 * @return
	 */
	T updateparameters();

	/**
	 * Table related comparison parameters: comparing old updateparams and new kirjekyyhkyparams
	 * @return
	 */
	T comparisonparameters();

	/**
	 * Sets updateparameters
	 * @param row
	 */
	void setUpdateparameters(T row);

	/**
	 * Sets comparisonparameters
	 * @param row
	 */
	void setComparisonparameters(T row);

	/**
	 * Domainmodel related list of results
	 * @return
	 */
	List<T> results();

	/**
	 * Sets results
	 * @param results
	 */
	void setResults(List<T> results);

	/**
	 * Adds a new resultrow to results() and returns the row
	 * @return
	 */
	T newResultRow();

	/**
	 * Adds a new resultrow to results() by using an existing row
	 * @return
	 */
	void newResultRow(T row);

	/**
	 * Clears all rows from data.results()
	 */
	void clearResults();

	/**
	 * List of selections; "selections.kunta.luonnont_maakunta", "selections.pesavakio.koord_mittaustapa", ...
	 * @return
	 */
	Map<String, Selection> selections();

	/**
	 * Set a list of selections "selections.kunta.luonnont_maakunta", "selections.pesavakio.koord_mittaustapa", ...
	 * @param selections
	 */
	void setSelections(Map<String, Selection> selections);

	/**
	 * Errors produced by validation
	 * @return
	 */
	Map<String, String> errors();

	/**
	 * Warnings produced by validation, except those that have been bypassed
	 * @return
	 */
	Map<String, String> warnings();

	/**
	 * Set errors produced by validation to data
	 * @param errors
	 */
	void setErrors(Map<String, String> errors);

	/**
	 * Sets warnings produced by validation to data
	 * @param warnings
	 */
	void setWarnings(Map<String, String> warnings);

	/**
	 * Base url of the system; "https://rengastus.fi/Merikotka"
	 * @return
	 */
	String baseURL();

	/**
	 * Location of common components; "https://rengastus.fi/"
	 * @return
	 */
	String commonURL();

	/**
	 * Page request
	 * @return
	 */
	String page();

	/**
	 * Set page request
	 * @param text
	 */
	void setPage(String text);

	/**
	 * Action request
	 * @return
	 */
	String action();

	/**
	 * Set action request
	 * @param action
	 */
	void setAction(String action);

	/**
	 * Success text; "success_update",...
	 * @return
	 */
	String successText();

	/**
	 * Set success text; "success_update",...
	 * @param text
	 */
	void setSuccessText(String text);

	/**
	 * Failure text; "pdf_printing_failed_out_of_space", ...
	 * @return
	 */
	String failureText();

	/**
	 * Set failure text; "pdf_printing_failed_out_of_space", ...
	 * @param text
	 */
	void setFailureText(String text);

	/**
	 * Notice texts; "Nests of Reviiri 1 split between 2 different inspectors", ..
	 * @return
	 */
	List<String> noticeTexts();

	/**
	 * Adds a list of texts to notice texts
	 * @param text
	 */
	void addToNoticeTexts(List<String> texts);

	/**
	 * Adds a text to notice texts
	 * @param text
	 */
	void addToNoticeTexts(String text);

	/**
	 * List of produced files / filenames / paths
	 * @return
	 */
	List<String> producedFiles();

	/**
	 * Set list of produced files / filenames / paths
	 * @param files
	 */
	void addToProducedFiles(List<String> files);

	/**
	 * Adds a filename / path / etc to the list of produced files
	 * @param filename
	 */
	void addToProducedFiles(String filename);

	/**
	 * Localized UI texts
	 * @see LocalizedTextsContainer
	 * @return
	 */
	Map<String, String> uiTexts();

	/**
	 * Request language
	 * @return
	 */
	String language();

	/**
	 * Set request language
	 * @param language
	 */
	void setLanguage(String language);

	/**
	 * User id of the user making the request
	 * @return
	 */
	String userId();

	/**
	 * Set request user id
	 * @param userId
	 */
	void setUserId(String userId);

	/**
	 * Name of the user making the request
	 * @return
	 */
	String userName();

	/**
	 * Sets request user name
	 * @param userName
	 */
	void setUserName(String userName);

	/**
	 * User type of the user making the request
	 * @return
	 */
	String userType();

	/**
	 * Set request user type
	 * @param userType
	 */
	void setUserType(String userType);

	/**
	 * Converts all values to use HTML entities, namely & < > ' and "
	 */
	void convertValuesToHTMLEntities();

	/**
	 * Returns the current year
	 * @return
	 */
	String currentYear();

	/**
	 * The timestamp when content such as CSS and javascript files were last updated
	 * @return
	 */
	String lastUpdateTimestamp();

	/**
	 * Sets the timestamp when content such as CSS and javascript files were last updated
	 * @param timestamp
	 */
	void setLastUpdateTimestamp(String timestamp);

	/**
	 * XML output, for example Kirjekyyhky Validation Service output. This will not be converted to HTML entities.
	 * @return
	 */
	String getXMLOutput();

	/**
	 * Set XML output, for example Kirjekyyhky Validation Service output. This will not be converted to HTML entities.
	 * @param xml
	 */
	void setXMLOutput(String xml);

	/**
	 * Returns a new row
	 * @return
	 */
	T newRow();

	/**
	 * Returns a new row with given prefix
	 * @param prefix
	 * @return
	 */
	T newRow(String prefix);

	/**
	 * Returns a copy of a row
	 * @param row
	 * @return
	 */
	T copyOf(T row);

	/**
	 * Is system in devel. mode?
	 * @return
	 */
	boolean developmentMode();

	/**
	 * Is system in staging mode?
	 * @return
	 */
	boolean stagingMode();

	/**
	 * Ignore this warning (user has promted value to be ok)
	 * @param column
	 */
	void bypassWarning(_Column column);

	/**
	 * Ignore this warning (user has promted value to be ok)
	 * @param fieldname
	 */
	void bypassWarning(String fieldname);

	/**
	 * Ignored warning messages
	 * @return
	 */
	Set<String> bypassedWarnings();

	/**
	 * Gets counts of how many results of each result type (kaynnit,poikaset,aikuiset,munat..) appear in the results
	 * @return
	 */
	UsedSlots getCountOfUsedSlots();

	/**
	 * Tells if there are any validation errors. (Or in other words returns !errors().isEmpty() ).
	 * Note: even if there are no errors there can be warnings.
	 * @return true if there are errors, false if there are not.
	 */
	boolean hasErrors();

	/**
	 * Tells if there are any validation warnings. (Or in other words returns !warnings().isEmpty() ).
	 * Note: even if there are no warnings there can be errors.
	 * @return true if there are warnings, false if there are not.
	 */
	boolean hasWarnings();

}
