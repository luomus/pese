package fi.hy.pese.framework.main.general;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import fi.luomus.commons.config.Config;
import fi.luomus.commons.utils.FileUtils;

public class PesimisenBiologisetRajatUtil {

	public static class Rajat {
		public final String laji;
		public final int maxPesyekoko;
		public final double maxMunintaVali;
		public Rajat(String laji, int maxPesyekoko, double maxMunintaNopeus) {
			this.laji = laji;
			this.maxPesyekoko = maxPesyekoko;
			this.maxMunintaVali = maxMunintaNopeus;
		}
	}

	private static Map<String, Rajat> instance = null;

	public static Map<String, Rajat> getInstance(Config config) throws FileNotFoundException, IOException {
		if (instance != null) return instance;

		instance = new HashMap<>();
		File file = new File(new File(config.templateFolder()), "pesimisen_raja-arvoja.txt");
		for (String line : FileUtils.readLines(file)) {
			if (line.trim().length() < 1) continue;
			String[] data = line.split(":");
			String laji = data[0].trim().toUpperCase();
			Rajat rajat = new Rajat(laji, Integer.valueOf(data[1].trim()), Double.valueOf(data[2].trim()));
			instance.put(laji, rajat);
		}
		return instance;
	}
}
