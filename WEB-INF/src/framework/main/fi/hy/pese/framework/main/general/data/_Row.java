package fi.hy.pese.framework.main.general.data;

import java.util.Map;


/**
 * An application specific structure of all the database tables. Their names, columns, keys, etc.
 */
public interface _Row extends Iterable<_Table> {

	String getPrefix();

	_Table getAputaulu();

	_Table getLogTable();

	_Table getLomakeVastaanottajat();

	_Table getSavedSearches();

	_Table getTransactionEntry();

	/**
	 * Returns a table structure that matches the given name
	 * @param table
	 * @return a table
	 * @throws IllegalArgumentException if no table matching the name has been defined
	 */
	_Table getTableByName(String table) throws IllegalArgumentException;

	/**
	 * Clears the values of all the columns of all the tables
	 */
	void clearAllValues();

	/**
	 * Return a column by full name
	 * @param columnFullname for example "poikanen.1.rengas"
	 * @return
	 */
	_Column getColumn(String columnFullname);

	/**
	 * Tells if row has a column matching fullname
	 * @param columnFullname for example "poikanen.1.rengas"
	 * @return true if column exists, false if not
	 */
	boolean containsColumn(String columnFullname);

	/**
	 * Returns those groups which can have N Tables defined by index, for example "poikanen.1" and "kaynti.12"
	 * @return
	 */
	Map<String, _IndexedTablegroup> getGroups();

	/**
	 * Tells if row has a table macthinc ghe full name
	 * @param tableFullname for example "tarkastus" or "poikanen.5"
	 * @return true if table is defined, false if not
	 */
	boolean containsTable(String tableFullname);

	/**
	 * Returns true if any of the tables have values, false otherwise.
	 * @return
	 */
	boolean hasValues();

	void setCustomValue(String key, String value);

	String getCustomValue(String key);

}
