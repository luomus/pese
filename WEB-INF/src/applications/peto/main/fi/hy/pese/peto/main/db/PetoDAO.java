package fi.hy.pese.peto.main.db;

import java.sql.SQLException;
import java.util.Collection;
import java.util.HashSet;

import fi.hy.pese.framework.main.db.CountResultsResultHandler;
import fi.hy.pese.framework.main.db._DAO;
import fi.hy.pese.framework.main.db._ResultSet;
import fi.hy.pese.framework.main.db._ResultsHandler;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.main.general.data._Table;
import fi.hy.pese.peto.main.app.PetoConst;
import fi.luomus.commons.utils.Utils;

public class PetoDAO {

	private static final String	DEAD_NESTS_SQL	= deadNestSql();
	private static final String	WHEN_NEST_DIED_SQL	= whennestDiedSql();
	private static final String PESINNAN_TUNNISTE_SQL = pesinnanTunnisteSQL();

	private static String deadNestSql() {
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT pesa FROM tarkastus, kaynti WHERE kaynti.tarkastus = tarkastus.id AND kaynti.pesa_kunto IN ( ");
		Utils.toCommaSeperatedStatement(sql, PetoConst.DEAD_NEST_CODES , true);
		sql.append(" ) " );
		return sql.toString();
	}

	private static String whennestDiedSql() {
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT min(t.vuosi) FROM tarkastus t, kaynti k WHERE t.id = k.tarkastus AND t.pesa = ? AND k.pesa_kunto IN ( ");
		Utils.toCommaSeperatedStatement(sql, PetoConst.DEAD_NEST_CODES , true);
		sql.append(" ) " );
		return sql.toString();
	}

	// Täytyy olla yksi (ja täsmälleen yksi) pesintä samalle vuodelle samalta lajilta samalla tunnisteella n-1 järjestysnumerolla
	private static String pesinnanTunnisteSQL() {
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT 1 FROM tarkastus WHERE pesinnan_yhdistava_tunniste = ? AND pesinnan_jarjestysnumero = ? AND pesiva_laji = ? AND vuosi = ? ");
		return sql.toString();
	}

	public static int countOfPesinnanTunniste(String tunniste, Integer jarjestysnumero, String pesivaLaji, String vuosi, _DAO<_PesaDomainModelRow> dao) throws SQLException {
		CountResultsResultHandler resultHandler = new CountResultsResultHandler();
		dao.executeSearch(PESINNAN_TUNNISTE_SQL, resultHandler, tunniste, Integer.toString(jarjestysnumero), pesivaLaji, vuosi);
		return resultHandler.numberOfRows;
	}

	public static Collection<String> deadNests(_DAO<_PesaDomainModelRow> dao) throws SQLException {
		DeadNestsQueryResultHandler resultHandler = new DeadNestsQueryResultHandler();
		dao.executeSearch(DEAD_NESTS_SQL, resultHandler);
		return resultHandler.deadNests();
	}

	private static class DeadNestsQueryResultHandler implements _ResultsHandler {
		private final Collection<String>	deadNests	= new HashSet<>();

		@Override
		public void process(_ResultSet rs) throws SQLException {
			while (rs.next()) {
				deadNests.add(rs.getString(1));
			}
		}

		public Collection<String> deadNests() {
			return deadNests;
		}
	}

	/**
	 * Checks if pesa has tarkastus for a given year for the given laji
	 * @param pesa_id
	 * @param tark_vuosi
	 * @param laji
	 * @return true if tarkastus exists, false if not
	 * @throws SQLException
	 */
	public static boolean checkTarkastusForTheYearExists(String pesa_id, String tark_vuosi, String laji, _DAO<_PesaDomainModelRow> dao) throws SQLException {
		if (laji == null || laji.length() < 1) return false;
		_Table tarkastus = dao.newTableByName("tarkastus");
		tarkastus.get("pesa").setValue(pesa_id);
		tarkastus.get("vuosi").setValue(tark_vuosi);
		tarkastus.get("pesiva_laji").setValue(laji);
		return dao.checkValuesExists(tarkastus);
	}

	/**
	 * Returns the year nest has been marked dead, or null if it is still alive
	 * @param pesa_id
	 * @param dao
	 * @return year nest has been marked dead or null if still alive
	 * @throws SQLException
	 */
	public static Integer yearNestHasDied(String pesa_id, _DAO<_PesaDomainModelRow> dao) throws SQLException {
		YearNestHasDiedResultHandler resultHandler = new YearNestHasDiedResultHandler();
		dao.executeSearch(WHEN_NEST_DIED_SQL, resultHandler, pesa_id);
		return resultHandler.yearNestHasDied;
	}

	private static class YearNestHasDiedResultHandler implements _ResultsHandler {

		public Integer yearNestHasDied = null;

		@Override
		public void process(_ResultSet rs) throws SQLException {
			if (!rs.next()) {
				yearNestHasDied = null;
				return;
			}
			String year = rs.getString(1);
			if (year == null) {
				yearNestHasDied = null;
				return;
			}
			yearNestHasDied = Integer.valueOf(year);
		}

	}
}
