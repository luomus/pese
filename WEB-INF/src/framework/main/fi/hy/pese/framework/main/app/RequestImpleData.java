package fi.hy.pese.framework.main.app;

import java.io.PrintWriter;

import fi.hy.pese.framework.main.db._DAO;
import fi.hy.pese.framework.main.general.data._Data;
import fi.hy.pese.framework.main.general.data._Row;
import fi.luomus.commons.http.HttpStatus;
import fi.luomus.commons.reporting.ErrorReporter;
import fi.luomus.commons.session.SessionHandler;

public class RequestImpleData<T extends _Row> {
	
	private _Data<T>							data;
	private SessionHandler						sessionHandler;
	private _DAO<T>								dao;
	private HttpStatus							redirecter;
	private _ApplicationSpecificationFactory<T>	application;
	private PrintWriter							out;
	private String								originalRequestURI;
	
	public _Data<T> getData() {
		return data;
	}
	public RequestImpleData<T> setData(_Data<T> data) {
		this.data = data;
		return this;
	}
	public SessionHandler getSessionHandler() {
		return sessionHandler;
	}
	public RequestImpleData<T> setSessionHandler(SessionHandler sessionHandler) {
		this.sessionHandler = sessionHandler;
		return this;
	}
	public _DAO<T> getDao() {
		return dao;
	}
	public RequestImpleData<T> setDao(_DAO<T> dao) {
		this.dao = dao;
		return this;
	}
	public HttpStatus getRedirecter() {
		return redirecter;
	}
	public RequestImpleData<T> setRedirecter(HttpStatus redirecter) {
		this.redirecter = redirecter;
		return this;
	}
	public _ApplicationSpecificationFactory<T> getApplication() {
		return application;
	}
	public RequestImpleData<T> setApplication(_ApplicationSpecificationFactory<T> application) {
		this.application = application;
		return this;
	}
	public PrintWriter getOut() {
		return out;
	}
	public RequestImpleData<T> setOut(PrintWriter out) {
		this.out = out;
		return this;
	}
	public RequestImpleData<T> setOriginalRequestURI(String originalRequestURI) {
		this.originalRequestURI = originalRequestURI;
		return this;
	}
	public String getOriginalRequestURI() {
		return this.originalRequestURI;
	}
	public ErrorReporter getErrorReporter() {
		return application.getErrorReporter();
	}
	
}