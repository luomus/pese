package fi.hy.pese.framework.test.view;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.view.FreemarkerTemplateViewer;
import fi.hy.pese.framework.test.TestConfig;
import fi.luomus.commons.config.Config;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class FreemarkerTemplateViewerTest {

	public static Test suite() {
		return new TestSuite(FreemarkerTemplateViewerTest.class.getDeclaredClasses());
	}

	public static class WhenAFreemarkerWriterIsCreated extends TestCase {

		private static final String	FOLDER	= "freemarkerwritertestfolder";
		private final File			f		= new File(FOLDER);

		@Override
		protected void setUp() throws Exception {
			f.mkdir();
		}

		@Override
		protected void tearDown() throws Exception {
			f.delete();
		}

		public void test_it_does_not_accepts_a_non_existing_folder_as_the_templatefolder() {
			try {
				new FreemarkerTemplateViewer("");
				fail("Didn't fail");
			} catch (IOException e) {
			}
			try {
				new FreemarkerTemplateViewer("doesnotexist");
				fail("Didn't fail");
			} catch (IOException e) {
			}
		}

		public void test_it_accepts_an_existing_folder_as_the_templatefolder() {
			try {
				new FreemarkerTemplateViewer(FOLDER);
			} catch (IOException e) {
				fail();
			}
		}

		public void test_it_accepts_an_existing_folder_as_the_templatefolder_given_as_absolute_path() throws Exception {
			try {
				new FreemarkerTemplateViewer(FOLDER);
			} catch (FileNotFoundException e) {
				fail();
			}
		}

	}

	public static class OpeningWriterWithParametersFromTestConfig extends TestCase {

		public void test_thatTestEnviromentSetUp() throws Exception {
			Config c = TestConfig.getConfig();
			new FreemarkerTemplateViewer(c.templateFolder());
		}
	}

	public static class WhenAFreemarkerWriterIsCalled extends TestCase {

		private FreemarkerTemplateViewer	viewer;
		private Map<Object, Object>			rootmodel;
		private Map<Object, Object>			dataleaf;
		private Map<Object, Object>			listsleaf;
		private PrintWriter					pwriter;
		private ByteArrayOutputStream		out;

		@Override
		protected void setUp() throws Exception {
			Config c = TestConfig.getConfig();
			viewer = new FreemarkerTemplateViewer(c.templateFolder());
			rootmodel = new HashMap<>();
			dataleaf = new HashMap<>();
			listsleaf = new HashMap<>();

			rootmodel.put(Const.DATA, dataleaf);
			rootmodel.put(Const.LISTS, listsleaf);

			out = new ByteArrayOutputStream();
			pwriter = new PrintWriter(out);
		}

		public void test_it_opens_the_correct_template() throws Exception {
			viewer.viewPage("test1", rootmodel, pwriter);
			assertEquals("test", out.toString());
		}

		public void test_it_reads_from_datamodel() throws Exception {
			dataleaf.put("testkey", "testvalue");
			viewer.viewPage("test2", rootmodel, pwriter);
			assertEquals("test:testvalue", out.toString());
		}

		public void test_it_reads_arrays() throws Exception {
			rootmodel.put("array", new String[] {
			                                     "a", "b", "c"
			});
			viewer.viewPage("test3", rootmodel, pwriter);
			assertEquals("a:b:c", out.toString());
		}

		public void test_it_reads_lists() throws Exception {
			ArrayList<String> list = new ArrayList<>();
			list.add("a");
			list.add("b");
			list.add("c");
			listsleaf.put("abc", list);
			viewer.viewPage("test4", rootmodel, pwriter);
			assertEquals("a:b:c", out.toString());
		}

	}

}
