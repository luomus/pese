package fi.hy.pese.framework.main.general.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fi.hy.pese.framework.main.general.consts.Const;
import fi.luomus.commons.containers.Selection;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.Utils;

public class Data<T extends _Row> implements _Data<T> {

	private final _RowFactory<T>			rowFactory;
	private final Map<String, String>		dataLeaf;
	private final Map<String, List<Object>>	listsLeaf;
	private List<T>							results;
	private final T						    searchparameters;
	private T						    	updateparameters;
	private T						    	comparisonparameters;
	private Map<String, Selection>			selections;
	private Map<String, String>				errors;
	private Map<String, String>				warnings;
	private final HashSet<String>			bypassedWarnings;
	private String							baseURL;
	private String							commonURL;
	private String							page;
	private String							action;
	private String							success;
	private String							failure;
	private List<String>					notes;
	private final List<String>				producedFiles;
	private Map<String, String>				uiTexts;
	private String							language;
	private String							userId;
	private String							userName;
	private String							userType;
	private String							lastUpdateTimestamp;
	private String							xmlOutput;
	private boolean							develMode = true;
	private boolean							stagingMode = false;
	private UsedSlots						countsOfUsedSlots;

	public Data(_RowFactory<T> rowFactory) {
		this.rowFactory = rowFactory;
		dataLeaf = new HashMap<>();
		listsLeaf = new HashMap<>();
		searchparameters = rowFactory.newRow(Const.SEARCHPARAMETERS);
		updateparameters = rowFactory.newRow(Const.UPDATEPARAMETERS);
		comparisonparameters = rowFactory.newRow(Const.COMPARISONPARAMETERS);
		results = new LinkedList<>();
		selections = new HashMap<>(10);
		errors = new LinkedHashMap<>(10);
		warnings = new LinkedHashMap<>(5);
		bypassedWarnings = new HashSet<>(5);

		baseURL = "";
		commonURL = "";
		page = "";
		action = "";
		success = "";
		failure = "";
		notes = new ArrayList<>();
		producedFiles = new LinkedList<>();
		uiTexts = new HashMap<>();
		language = "";
	}

	// Misc text fields ("data.table", "data.login_password" ,...)
	@Override
	public String get(String key) {
		String value = dataLeaf.get(key);
		if (value == null) return "";
		return value;
	}

	@Override
	public Map<String, String> getAll() {
		return dataLeaf;
	}

	@Override
	public void set(String key, String value) {
		if (Const.ACTION.equals(key)) throw new Error("Remember to set action with setAction() -method"); // Just a reminder to the coder...
		dataLeaf.put(key, value);
	}

	@Override
	public boolean contains(String key) {
		if (!dataLeaf.containsKey(key)) return false;
		String value = get(key);
		if (value == null || value.length() < 1) return false;
		return true;
	}

	// Misc lists ("edellisetPesat", "pesanTarkastukset", ..)
	@Override
	public void addToList(String list, Object o) {
		if (!listsLeaf.containsKey(list)) listsLeaf.put(list, new LinkedList<>());
		listsLeaf.get(list).add(o);
	}

	@Override
	public Map<String, List<Object>> getLists() {
		return listsLeaf;
	}

	// Table related search parameters received from user
	@Override
	public T searchparameters() {
		return searchparameters;
	}

	// Table related parameters received from user
	@Override
	public T updateparameters() {
		return updateparameters;
	}

	@Override
	public T comparisonparameters() {
		return comparisonparameters;
	}

	@Override
	public void setUpdateparameters(T row) {
		this.updateparameters = row;
	}

	@Override
	public void setComparisonparameters(T row) {
		this.comparisonparameters = row;
	}

	// Table related results of searches
	@Override
	public List<T> results() {
		return results;
	}

	// Adds a new row to resultlist and returns the _Table element of that row
	@Override
	public T newResultRow() {
		T newRow = newRow();
		results.add(newRow);
		return newRow;
	}

	// Adds the given row as a resultrow
	@Override
	public void newResultRow(T row) {
		results.add(row);
	}

	@Override
	public void setResults(List<T> results) {
		if (results == null) {
			this.results = new LinkedList<>();
		} else {
			this.results = results;
		}
	}

	@Override
	public void clearResults() {
		results.clear();
		// results = new LinkedList<T>();
	}

	@Override
	public T newRow() {
		return rowFactory.newRow();
	}

	@Override
	public T newRow(String prefix) {
		return rowFactory.newRow(prefix);
	}

	@Override
	public T copyOf(T originalRow) {
		T copyOfRow = newRow();
		for (_Table originalTable : originalRow) {
			if (originalTable.hasValues()) {
				_Table copyOfTable = copyOfRow.getTableByName(originalTable.getFullname());
				for (_Column originalColumn : originalTable) {
					_Column copyOfColumn = copyOfTable.get(originalColumn.getName());
					copyOfColumn.setValue(originalColumn.getValue());
				}
			}
		}
		return copyOfRow;
	}

	// Data needed to produce selections
	@Override
	public Map<String, Selection> selections() {
		return selections;
	}

	@Override
	public void setSelections(Map<String, Selection> selections) {
		this.selections = selections;
	}

	// Validation errors
	@Override
	public Map<String, String> errors() {
		return errors;
	}

	@Override
	public void setErrors(Map<String, String> errors) {
		this.errors = errors;
	}

	@Override
	public String baseURL() {
		return baseURL;
	}

	public void setBaseURL(String url) {
		baseURL = url;
	}

	@Override
	public String commonURL() {
		return commonURL;
	}

	public void setCommonURL(String url) {
		commonURL = url;
	}

	// Page
	@Override
	public String page() {
		return page;
	}

	@Override
	public void setPage(String page) {
		this.page = page;
	}

	// Action
	@Override
	public String action() {
		return action;
	}

	@Override
	public void setAction(String action) {
		if (action == null) this.action = "";
		else this.action = action;
	}

	// Success, failure and notice texts
	@Override
	public String successText() {
		return success;
	}

	@Override
	public void setSuccessText(String text) {
		success = text;
	}

	@Override
	public String failureText() {
		return failure;
	}

	@Override
	public void setFailureText(String text) {
		failure = text;
	}

	@Override
	public List<String> noticeTexts() {
		return notes;
	}

	@Override
	public void addToNoticeTexts(List<String> texts) {
		if (texts != null) notes.addAll(texts);
	}

	@Override
	public void addToNoticeTexts(String text) {
		notes.add(text);
	}

	// Produced reports, pdf files, etc
	@Override
	public List<String> producedFiles() {
		return producedFiles;
	}

	@Override
	public void addToProducedFiles(List<String> files) {
		if (files != null) {
			producedFiles.addAll(files);
		}
	}

	@Override
	public void addToProducedFiles(String filename) {
		producedFiles.add(filename);
	}

	// UITexts
	@Override
	public Map<String, String> uiTexts() {
		return uiTexts;
	}

	public void setUITexts(Map<String, String> uiTexts) {
		this.uiTexts = uiTexts;
	}

	// Language
	@Override
	public String language() {
		return language;
	}

	@Override
	public void setLanguage(String language) {
		this.language = language;
	}

	// User id/name/type
	@Override
	public String userId() {
		return this.userId;
	}

	@Override
	public void setUserId(String userId) {
		this.userId = userId;
	}

	@Override
	public String userName() {
		return this.userName;
	}

	@Override
	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Override
	public String userType() {
		return this.userType;
	}

	@Override
	public void setUserType(String userType) {
		this.userType = userType;
	}

	@Override
	public void convertValuesToHTMLEntities() {

		convertAllTablevaluesToHTMLEntities(searchparameters);
		convertAllTablevaluesToHTMLEntities(updateparameters);
		for (T r : results) {
			convertAllTablevaluesToHTMLEntities(r);
		}

		this.success = convert(this.success);
		this.failure = convert(this.failure);
		this.notes = convert(this.notes);

		int i = 0;
		for (String f : this.producedFiles) {
			producedFiles.set(i++, convert(f));
		}

		convert(this.uiTexts);

		convert(this.getAll());

		for (List<Object> list : this.getLists().values()) {
			i = 0;
			for (Object o : list) {
				if (o instanceof String) {
					String s = (String) o;
					list.set(i++, convert(s));
				} else if (o instanceof Map<?, ?>) {
					// .... T O D O
				} else if (o instanceof _Row) {
					convertAllTablevaluesToHTMLEntities((_Row) o);
				} else if (o instanceof _Table) {
					((_Table) o).convertAllValuesToHTMLEntities();
				}
			}
		}

	}

	private List<String> convert(List<String> list) {
		List<String> newList = new ArrayList<>();
		for (String s : list) {
			newList.add(convert(s));
		}
		return newList;
	}

	private void convert(Map<String, String> map) {
		for (Map.Entry<String, String> e : map.entrySet()) {
			e.setValue(convert(e.getValue()));
		}
	}

	private String convert(String s) {
		return Utils.toHTMLEntities(s);
	}

	private void convertAllTablevaluesToHTMLEntities(_Row row) {
		if (row == null) return;
		for (_Table t : row) {
			t.convertAllValuesToHTMLEntities();
		}
	}

	@Override
	public String currentYear() {
		return DateUtils.getCurrentDateTime("yyyy");
	}

	@Override
	public String lastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}

	@Override
	public void setLastUpdateTimestamp(String timestamp) {
		this.lastUpdateTimestamp = timestamp;
	}

	@Override
	public String getXMLOutput() {
		return this.xmlOutput;
	}

	@Override
	public void setXMLOutput(String xml) {
		this.xmlOutput = xml;
	}

	@Override
	public boolean hasErrors(_Column c) {
		return this.errors().containsKey(c.getFullname());
	}

	@Override
	public boolean developmentMode() {
		return develMode;
	}

	public void setDevelopment(boolean mode) {
		this.develMode = mode;
	}

	@Override
	public boolean stagingMode() {
		return stagingMode;
	}

	public void setStaging(boolean mode) {
		this.stagingMode = mode;
	}

	@Override
	public void bypassWarning(_Column column) {
		bypassWarning(column.getFullname());
	}

	@Override
	public void bypassWarning(String fieldname) {
		bypassedWarnings.add(fieldname);
	}

	@Override
	public boolean hasWarnings(_Column c) {
		return this.warnings().containsKey(c.getFullname());
	}

	@Override
	public Map<String, String> warnings() {
		return this.warnings;
	}

	@Override
	public void setWarnings(Map<String, String> warnings) {
		this.warnings = warnings;
	}

	@Override
	public Set<String> bypassedWarnings() {
		return bypassedWarnings;
	}

	@Override
	public UsedSlots getCountOfUsedSlots() {
		if (countsOfUsedSlots == null) {
			countsOfUsedSlots = new UsedSlots();
		}
		return countsOfUsedSlots;
	}

	@Override
	public boolean hasErrors() {
		return !this.errors().isEmpty();
	}

	@Override
	public boolean hasWarnings() {
		return !this.warnings.isEmpty();
	}

	@Override
	public String getInputParametersForDebugging() {
		StringBuilder debug = new StringBuilder();
		debug.append("userid: ").append(userId);
		debug.append(" page: ").append(page);
		debug.append(" action: ").append(action);
		debug.append(" data: ").append(dataLeaf.toString());
		debug.append(" searchparameters: ").append(fi.hy.pese.framework.main.general.Utils.debugS(searchparameters));
		debug.append(" updateparameters: ").append(fi.hy.pese.framework.main.general.Utils.debugS(updateparameters));
		return debug.toString();
	}

}
