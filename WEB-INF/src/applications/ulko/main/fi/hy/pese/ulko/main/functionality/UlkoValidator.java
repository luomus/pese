package fi.hy.pese.ulko.main.functionality;

import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.app.functionality.validate.Validator;
import fi.hy.pese.framework.main.app.functionality.validate._Validator;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._Table;
import fi.hy.pese.ulko.main.app._UlkoDomainModelRow;
import fi.luomus.commons.containers.Selection;
import fi.luomus.commons.tipuapi.TipuAPIClient;
import fi.luomus.commons.tipuapi.TipuAPIClientImple;
import fi.luomus.commons.utils.CoordinatesInsideMunicipalityUtil;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.URIBuilder;
import fi.luomus.commons.xml.Document.Node;

public class UlkoValidator extends Validator<_UlkoDomainModelRow>{
	
	private final _Table pyynto;
	private final _Table loyto;
	private final _Table rengastus;
	
	public UlkoValidator(_Request<_UlkoDomainModelRow> request) {
		super(request);
		this.pyynto = data.updateparameters().getPyynto();
		this.loyto = data.updateparameters().getLoyto();
		this.rengastus = data.updateparameters().getRengastus();
	}
	
	@Override
	public void validate() throws Exception {
		
		if (data.action().equals("laheta_rengastustietopyynto")) {
			_Column scheme = loyto.get("scheme");
			checkNotNull(scheme);
		}
		
		checkAsUpdateparameters(data.updateparameters());
		checkReferenceValuesExists(data.updateparameters(), dao);
		
		checkInteger(pyynto.get("loytaja_postinumero"));
		validateEmail(pyynto.get("loytaja_email"));
		validateEmail(pyynto.get("ilmoittaja_email"));
		validateEmail(pyynto.get("cc_rengastustietopyynto_email"));
		
		checkInteger(loyto.get("wgs84_ast_leveys"));
		checkInteger(loyto.get("wgs84_ast_pituus"));
		checkInteger(rengastus.get("wgs84_ast_leveys"));
		checkInteger(rengastus.get("wgs84_ast_pituus"));
		
		_Column loytoPvm = loyto.get("pvm");
		_Column rengPvm = rengastus.get("pvm");
		if (notNullNoErrors(loytoPvm) && notNullNoErrors(rengPvm)) {
			if (DateUtils.getEpoch(loytoPvm.getDateValue()) < DateUtils.getEpoch(rengPvm.getDateValue())) {
				error(loytoPvm, "Ei voi olla ennen rengastuspäivämäärää");
			}
		}
		
		checkNotNull(pyynto.get("loytaja_nimi"));
		checkNotNull(loyto.get("rengas"));
		checkNotNull(loyto.get("pvm"));
		checkNotNull(loyto.get("kunta"));
		checkNotNull(loyto.get("euring_paikka"));
		
		if (rengastusTiedotGiven()) {
			checkNotNull(rengastus.get("koord_ilm_tyyppi"));
			checkNotNull(rengastus.get("pvm"));
			checkNotNull(rengastus.get("scheme"));
			checkNotNull(rengastus.get("kunta"));
			checkNotNull(rengastus.get("euring_paikka"));
		}
		
		_Column rengKoordIlmTyyppi = rengastus.get("koord_ilm_tyyppi");
		if (notNullNoErrors(rengKoordIlmTyyppi)) {
			if (!rengKoordIlmTyyppi.getValue().startsWith("W")) {
				error(rengKoordIlmTyyppi, "ULKOMAISELLE_KAY_VAIN_WGS84");
			}
		}
		
		validateCoordinates(rengastus, false);
		
		_Column euringPaikka = loyto.get("euring_paikka");
		boolean suomesta = euringPaikka.getValue().startsWith("SF");
		if (suomesta) {
			boolean koordinaatitOk = validateCoordinates(loyto, true);
			validoiSuomalainenKunta(koordinaatitOk);
		} else {
			if (!loyto.get("koord_ilm_tyyppi").getValue().startsWith("W")) {
				error(loyto.get("koord_ilm_tyyppi"), "JOS_ULKOMAILTA_ANNETTAVA_WGS84_KOORDINAATIT");
			} else if (onSuomalainenKunta(loyto.get("kunta").getValue())) {
				error(euringPaikka, "SUOMALAINEN_KUNTA_VALITTU");
			} else {
				validateCoordinates(loyto, false);
			}
		}
		
		if (euringPaikka.getValue().equals("SF--")) {
			error(euringPaikka, "ANNETTAVA_TARKKA_EURING_PAIKKA");
		}
		
		if (!data.action().equals(Const.UPDATE)) {
			warnings.clear();
		}
	}
	
	private void validoiSuomalainenKunta(boolean koordinaatitOk) throws Exception {
		_Column kunta = loyto.get("kunta");
		if (!onSuomalainenKunta(kunta.getValue())) {
			error(kunta, "EI_SUOMALAINEN_KUNTA");
		}
		if (koordinaatitOk && notNullNoErrors(kunta)) {
			checkInsideKunta(loyto);
		}
	}
	
	private boolean onSuomalainenKunta(String kunta) {
		Selection selection = data.selections().get(TipuAPIClient.MUNICIPALITIES);
		return selection.containsOption(kunta); 
	}
	
	private boolean validateCoordinates(_Table table, boolean checkRange) {
		_Column koordIlmTyyppi = table.get("koord_ilm_tyyppi");
		
		for (_Column c : table) {
			if (c.getName().endsWith("_pituus") || c.getName().endsWith("_leveys")) {
				if (c.hasValue() && !koordIlmTyyppi.hasValue()) {
					error(koordIlmTyyppi, "KOORD_TYYPPI_ILMOITETTAVA_JOS_KOORDINAATTEJA_ANNETTU");
					return false;
				}
			}
		}
		
		if (hasErrors(koordIlmTyyppi) || !koordIlmTyyppi.hasValue()) return false; 
		
		_Column leveys = null, pituus = null;
		double leveys_min, leveys_max, pituus_min, pituus_max;
		
		if (koordIlmTyyppi.getValue().equals("Y")) {
			leveys = table.get("yht_leveys");
			pituus = table.get("yht_pituus");
			leveys_min = _Validator.YHT_LEVEYS_MIN;
			leveys_max = _Validator.YHT_LEVEYS_MAX;
			pituus_min = _Validator.YHT_PITUUS_MIN;
			pituus_max = _Validator.YHT_PITUUS_MAX;
			
		} else if (koordIlmTyyppi.getValue().equals("E")) {
			leveys = table.get("eur_leveys");
			pituus = table.get("eur_pituus");
			leveys_min = _Validator.EUR_LEVEYS_MIN;
			leveys_max = _Validator.EUR_LEVEYS_MAX;
			pituus_min = _Validator.EUR_PITUUS_MIN;
			pituus_max = _Validator.EUR_PITUUS_MAX;
		} else if (koordIlmTyyppi.getValue().equals("W")) {
			leveys = table.get("wgs84_leveys");
			pituus = table.get("wgs84_pituus");
			leveys_min = _Validator.WGS84_LEVEYS_MIN;
			leveys_max = _Validator.WGS84_LEVEYS_MAX;
			pituus_min = _Validator.WGS84_PITUUS_MIN;
			pituus_max = _Validator.WGS84_PITUUS_MAX;
		} else if (koordIlmTyyppi.getValue().equals("WA")) {
			leveys = table.get("wgs84_ast_leveys");
			pituus = table.get("wgs84_ast_pituus");
			leveys_min = _Validator.WGS84_AST_LEVEYS_MIN;
			leveys_max = _Validator.WGS84_AST_LEVEYS_MAX;
			pituus_min = _Validator.WGS84_AST_PITUUS_MIN;
			pituus_max = _Validator.WGS84_AST_PITUUS_MAX;
		} else {
			error(table.get("koord_ilm_tyyppi"), _Validator.INVALID_VALUE);
			return false;
		}
		checkNotNull(leveys);
		checkNotNull(pituus);
		if (checkRange) {
			checkCoordinateRanges(leveys, pituus, leveys_min, leveys_max, pituus_min, pituus_max);
		}
		if (noErrors()) {
			tryToConvert(table, leveys, pituus);
		}
		return noErrors(leveys) && noErrors(pituus);
	}

	private void tryToConvert(_Table table, _Column leveys, _Column pituus) {
		try {
			_Table copy = dao.newTableByName(table.getName());
			copy.setValues(table);
			UlkoFunctionality.convertCoordinates(copy, request.config());
		} catch (Exception e) {
			error(pituus, _Validator.COORDINATE_ERROR);
			error(leveys, _Validator.COORDINATE_ERROR);
		}
	}
	
	protected void checkInsideKunta(_Table table) throws Exception {
		Node kunta = request.getTipuApiResource(TipuAPIClient.MUNICIPALITIES).getById(table.get("kunta").getValue());
		Double kuntaDesPit = Double.valueOf(kunta.getAttribute("centerpoint-lon"));
		Double kuntaDesLev = Double.valueOf(kunta.getAttribute("centerpoint-lat"));
		Double kuntaSade = Double.valueOf(kunta.getAttribute("radius"));
		
		_Table tableCopy = dao.newRow().getLoyto();
		tableCopy.setValues(table);
		UlkoFunctionality.convertCoordinates(tableCopy, request.config());
		
		URIBuilder uri = new URIBuilder("coordinate-conversion-service")
		.addParameter("lat", tableCopy.get("yht_leveys").getValue())
		.addParameter("lon", tableCopy.get("yht_pituus").getValue())
		.addParameter("type", "uniform");
		
		TipuAPIClient api = null;
		try  {
			api = new TipuAPIClientImple(request.config());
			Node response = api.getAsDocument(uri.toString()).getRootNode();
			//			<conversion-response>
			//				<trs-tm35fin lat="600439" lon="240020"/>
			//				<uniform lat="600760" lon="3239996"/>
			//				<degrees lat="52542" lon="243917"/>
			//				<decimal lat="5.4283150278548975" lon="24.654636659865304"/>
			//				<wgs84 lat="54.127791120052" lon="23.019345684237"/>
			//			</conversion-response>
			Double loytoLev = Double.valueOf(response.getNode("decimal").getAttribute("lat"));
			Double loytoPit = Double.valueOf(response.getNode("decimal").getAttribute("lon"));
			if (!CoordinatesInsideMunicipalityUtil.insideMunicipality(kuntaDesLev, kuntaDesPit, kuntaSade, loytoLev, loytoPit)) {
				error(table.get("kunta"), COORDINATES_NOT_INSIDE_REGION);
			}
		} finally {
			if (api != null) api.close();
		}
	}
	
	private boolean rengastusTiedotGiven() {
		return rengastus.get("rengas").hasValue();
	}
	
}
