package fi.hy.pese.peto.main.functionality;

import java.util.Map;

import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.app.functionality.KirjekyyhkyManagementFunctionality;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.main.general.data._Table;
import fi.hy.pese.peto.main.app.PetoConst;

public class PetoKirjekyyhkyManagementFunctionality extends KirjekyyhkyManagementFunctionality {
	
	public PetoKirjekyyhkyManagementFunctionality(_Request<_PesaDomainModelRow> request) {
		super(request);
	}
	
	@Override
	protected void applicationSpecificConversions(Map<String, String> formData, _PesaDomainModelRow kirjekyyhkyparameters) {
		PetoConst.kirjekyyhkyModelToData(formData, kirjekyyhkyparameters);
		data.set("tark_vuosi", kirjekyyhkyparameters.getTarkastus().get("vuosi").getValue());
	}
	
	@Override
	protected void applicationSpecificReturnedEmptyProcedure(_PesaDomainModelRow kirjekyyhkyParameters) {
		_Table tark = kirjekyyhkyParameters.getTarkastus();
		tark.get("tarkastettu").setValue("E");
	}
	
	@Override
	protected void applicationSpecificComparisonDataOperations(_PesaDomainModelRow kirjekyyhkyParameters, _PesaDomainModelRow comparisonParameters) {
		kirjekyyhkyParameters.getTarkastus().get("kommentti_edellinen_laji").setValue(comparisonParameters.getTarkastus().get("kommentti_edellinen_laji").getValue());
	}
	
}
