package fi.hy.pese.pesakortti.main.functionality;

import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.db.RowsToListResultHandler;
import fi.hy.pese.framework.main.db._DAO;
import fi.hy.pese.framework.main.general._PDFDataMapGenerator;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.main.general.data._Table;
import fi.luomus.commons.containers.Selection;
import fi.luomus.commons.pdf.PDFWriter;
import fi.luomus.commons.tipuapi.TipuAPIClient;
import fi.luomus.commons.tipuapi.TipuApiResource;
import fi.luomus.commons.utils.Utils;
import fi.luomus.commons.xml.Document.Node;

public class PesakorttiDataToMapForPDFPrintingGenerator implements _PDFDataMapGenerator {
	
	private final _DAO<_PesaDomainModelRow>						dao;
	private final Map<String, Selection>	selections;
	private final TipuApiResource ringers;
	private static final Collection<Character> DIGITS = Utils.collection('0', '1', '2', '3', '4', '5', '6', '7', '8', '9');
	
	public PesakorttiDataToMapForPDFPrintingGenerator(_DAO<_PesaDomainModelRow> dao, _Request<_PesaDomainModelRow> request) {
		this.dao = dao;
		this.selections = request.data().selections();
		this.ringers = request.getTipuApiResource(TipuAPIClient.RINGERS);
	}
	
	/**
	 * This function is called to build the application specific data for PDF printing (esitäytetty tarkastuslomake).
	 * Basicly it takes values from database and puts them to a map. The map is later given to pdfWriter, which will
	 * look for values from the map that match the names of the fields on the pdf.
	 * For example forthere are several places where the year of inspection is printed out, so those
	 * are filled to the map to keys "ylapalkki_1_vuosi", "ylapalkki_1_vuosi"... and so on.
	 */
	@Override
	public Map<String, String> tarkastusDataFor(String pesaid, String year) throws Exception {
		Map<String, String> map = new HashMap<>();
		
		_PesaDomainModelRow row = previousTarkastusFor(pesaid);
		
		map.put("vuosi_1", year);
		map.put("vuosi_2", year);
		
		everythingFromRowToMap(map, row);
		coordinatesToMap(map, row);
		rakLaji(map, row);
		pesaEtaisyysReuna(map, row);
		
		// TODO kirjekyyhkyyn pitää lähettää lyhenne
		//map.put("pesa.kunta", selections.get(PesakorttiConst.KUNNAT).get(row.getColumn("pesa.kunta").getValue()));
		map.put("pesa.kunta", row.getColumn("pesa.kunta").getValue());
		
		seur_tarkastaja(map, row);
		edellinenTarkastus(map, row);
		
		generateFilenameToMap(pesaid, year, map, row);
		return map;
	}
	
	private void pesaEtaisyysReuna(Map<String, String> map, _PesaDomainModelRow row) {
		_Column column = row.getTarkastus().get("pesa_etaisyys_reuna");
		String etReuna = column.getValue();
		if (etReuna.equals("0") || etReuna.equals("1") || etReuna.equals("2")) {
			map.remove(column.getFullname());
			map.put(column.getFullname() + "_" + column.getValue(), PDFWriter.SELECTED_CHECKBOX);
		} else if (column.hasValue()) {
			String desc = selections.get("tarkastus.pesa_etaisyys_reuna").get(etReuna);
			String onlyDigits = "";
			for (char c : desc.toCharArray()) {
				if (DIGITS.contains(c)) {
					onlyDigits += c;
				}
			}
			map.put(column.getFullname(), onlyDigits);
		}
	}
	
	private void edellinenTarkastus(Map<String, String> map, _PesaDomainModelRow row) {
		_Table tarkastus = row.getTarkastus();
		Collection<_Table> kaynnit = row.getKaynnit().values();
		edellinenTarkastaja(map, row);
		pvm(map, kaynnit);
		pesinytLaji(map, tarkastus);
		pesimistulos(map, tarkastus);
		pesanKunto(map, tarkastus);
	}
	
	private void edellinenTarkastaja(Map<String, String> map, _PesaDomainModelRow row) {
		_Column tarkastaja = row.getColumn("tarkastus.tarkastaja");
		if (!tarkastaja.hasValue()) return;
		String nro = tarkastaja.getValue();
		Node ringer = ringers.getById(nro);
		String name = ringer.getNode("firstname").getContents() + " " + ringer.getNode("lastname").getContents();
		map.put("edellinen_tark_tarkastaja", name + " (" + nro + ")");
	}
	
	private void pvm(Map<String, String> map, Collection<_Table> kaynnit) {
		String maxPvm = "";
		for (_Table kaynti : kaynnit) {
			_Column pvm = kaynti.get("pvm");
			if (pvm.hasValue()) {
				maxPvm = pvm.getValue();
			}
		}
		map.put("edellinen_tark_pvm", maxPvm);
	}
	
	
	private void pesimistulos(Map<String, String> map, _Table tarkastus) {
		_Column pesimistulos = tarkastus.get("pesimistulos");
		String prev_pesimistulos = pesimistulos.getValue();
		prev_pesimistulos = pesimistuloksetSelection().get(prev_pesimistulos);
		map.put("edellinen_tark_pesimistulos", prev_pesimistulos);
	}
	
	private void pesanKunto(Map<String, String> map, _Table tarkastus) {
		_Column kunto = tarkastus.get("pesa_kunto");
		String prev_kunto = kunto.getValue();
		prev_kunto = pesanKunnotSelection().get(prev_kunto);
		map.put("edellinen_tark_pesa_kunto", prev_kunto);
	}
	
	private void pesinytLaji(Map<String, String> map, _Table tarkastus) {
		_Column laji = tarkastus.get("pesiva_laji");
		if (laji.hasValue()) {
			map.put("edellinen_tark_laji", buildLajiText(laji.getValue()));
		}
	}
	
	private void seur_tarkastaja(Map<String, String> map, _PesaDomainModelRow row) {
		_Column tarkastaja = row.getColumn("pesa.seur_tarkastaja");
		if (!tarkastaja.hasValue()) return;
		String nro = tarkastaja.getValue();
		Node ringer = ringers.getById(nro);
		String name = ringer.getNode("firstname").getContents() + " " + ringer.getNode("lastname").getContents();
		String address = "";
		for (Node n : ringer.getNode("address")) {
			address += n.getContents() + " ";
		}
		map.put("tarkastaja_nro", nro);
		map.put("tarkastaja_nimi", name);
		map.put("tarkastaja_osoite", address);
		map.put("tarkastaja_puhelin", ringer.getNode("email").getContents());
		map.put("tarkastaja_email", ringer.getNode("mobile-phone").getContents());
	}
	
	private void rakLaji(Map<String, String> map, _PesaDomainModelRow prev_tarkastus) {
		_Column rak_laji = prev_tarkastus.getPesa().get("rak_laji");
		if (rak_laji.hasValue()) {
			map.put("pesa.rak_laji", buildLajiText(rak_laji.getValue()));
		}
	}
	
	
	private void coordinatesToMap(Map<String, String> map, _PesaDomainModelRow result) {
		_Table pesa = result.getPesa();
		String koord_tyyppi = pesa.get("koord_ilm_tyyppi").getValue();
		String leveys = "";
		String pituus = "";
		if (koord_tyyppi.equals("Y")) {
			map.put("pesa.koord_ilm_tyyppi_Y", PDFWriter.SELECTED_CHECKBOX);
			leveys = pesa.get("yht_leveys").getValue();
			pituus = pesa.get("yht_pituus").getValue();
		} else if (koord_tyyppi.equals("E")) {
			map.put("pesa.koord_ilm_tyyppi_E", PDFWriter.SELECTED_CHECKBOX);
			leveys = pesa.get("eur_leveys").getValue();
			pituus = pesa.get("eur_pituus").getValue();
		} else if (koord_tyyppi.equals("A")) {
			leveys = pesa.get("yht_leveys").getValue();
			pituus = pesa.get("yht_pituus").getValue();
		}
		map.put("pesa.leveys", leveys);
		map.put("pesa.pituus", pituus);
	}
	
	private String buildLajiText(String laji) {
		StringBuilder text = new StringBuilder();
		text.append(lajitSelection().get(laji));
		text.append("  (").append(laji).append(")");
		return text.toString();
	}
	
	private void everythingFromRowToMap(Map<String, String> map, _PesaDomainModelRow result) {
		tableToMap(map, result.getPesa());
		tableToMap(map, result.getTarkastus());
	}
	
	private void tableToMap(Map<String, String> map, _Table table) {
		for (_Column c : table) {
			if (c.hasAssosiatedSelection()) {
				map.put(c.getFullname() + "_" + c.getValue(), PDFWriter.SELECTED_CHECKBOX);
				map.put(c.getAssosiatedSelection() + "_" + c.getValue().toUpperCase(), PDFWriter.SELECTED_CHECKBOX);
			}
			map.put(c.getFullname(), c.getValue());
			
		}
	}
	
	private void generateFilenameToMap(String pesaid, String year, Map<String, String> map, _PesaDomainModelRow result) {
		StringBuilder filename = new StringBuilder("TARKASTAJA_");
		filename.append(map.get("pesa.seur_tarkastaja")).append("_");
		filename.append(result.getColumn("pesa.kunta").getValue());
		filename.append("_PESA_").append(pesaid).append("_").append(year);
		map.put(Const.FILENAME, filename.toString());
	}
	
	private _PesaDomainModelRow previousTarkastusFor(String pesaid) throws SQLException {
		_PesaDomainModelRow searchParams = dao.newRow();
		searchParams.getPesa().getUniqueNumericIdColumn().setValue(pesaid);
		//searchParams.getTarkastus().get("tarkastettu").setValue(TARKASTETTU_YES);
		List<_PesaDomainModelRow> results = new LinkedList<>();
		dao.executeSearch(searchParams, new RowsToListResultHandler(dao, results));
		_PesaDomainModelRow result = results.get(0);
		return result;
	}
	
	private Selection pesimistulokset = null;
	private Selection pesimistuloksetSelection() {
		if (pesimistulokset == null) {
			pesimistulokset = selections.get(dao.newRow().getTarkastus().get("pesimistulos").getAssosiatedSelection());
		}
		return pesimistulokset;
	}
	
	private Selection pesanKunnot = null;
	private Selection pesanKunnotSelection() {
		if (pesanKunnot == null) {
			pesanKunnot = selections.get(dao.newRow().getTarkastus().get("pesa_kunto").getAssosiatedSelection());
		}
		return pesanKunnot;
	}
	
	private Selection lajit = null;
	private Selection lajitSelection() {
		if (lajit == null) {
			lajit = selections.get(dao.newRow().getPesa().get("rak_laji").getAssosiatedSelection());
		}
		return lajit;
	}
	
	@Override
	public Map<String, String> tarkastusDataFor(String tarkastusid) throws Exception {
		throw new UnsupportedOperationException();
	}
	
}
