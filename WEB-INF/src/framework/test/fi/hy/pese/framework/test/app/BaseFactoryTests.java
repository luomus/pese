package fi.hy.pese.framework.test.app;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.ObjectInputStream;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import fi.hy.pese.framework.main.app.BaseFactoryForApplicationSpecificationFactory;
import fi.hy.pese.framework.main.app._Functionality;
import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.db._DAO;
import fi.hy.pese.framework.main.db.oraclesql.OracleSQL_DAO;
import fi.hy.pese.framework.main.general.consts.Success;
import fi.hy.pese.framework.main.general.data.DatabaseTableStructure;
import fi.hy.pese.framework.main.general.data.HistoryAction;
import fi.hy.pese.framework.main.general.data.PesaDomainModelDatabaseStructure;
import fi.hy.pese.framework.main.general.data.PesaDomainModelRowFactory;
import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.test.TestConfig;
import fi.luomus.commons.config.Config;
import fi.luomus.commons.config.ConfigReader;
import fi.luomus.commons.containers.Selection;
import fi.luomus.commons.containers.SelectionImple;
import fi.luomus.commons.containers.SelectionOptionImple;
import fi.luomus.commons.languagesupport.LocalizedTextsContainer;
import fi.luomus.commons.utils.DateUtils;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class BaseFactoryTests {

	public static Test suite() {
		return new TestSuite(BaseFactoryTests.class.getDeclaredClasses());
	}

	public static class Tests extends TestCase {

		private static PesaDomainModelDatabaseStructure hardCoded() {
			PesaDomainModelDatabaseStructure template = new PesaDomainModelDatabaseStructure();

			DatabaseTableStructure pesa = new DatabaseTableStructure("pesa");
			pesa.addColumn("id", _Column.INTEGER, 1).setTypeToUniqueNumericIncreasingId();
			template.setPesa(pesa);

			return template;

		}

		private static final String	OPTION_VALUE	= "FOO";
		private static final String	SELECTION_NAME	= "foo";

		private class BaseFactoryExt extends BaseFactoryForApplicationSpecificationFactory<_PesaDomainModelRow> {

			public BaseFactoryExt(String configFileName) throws FileNotFoundException {
				super(configFileName);
			}

			private int	i	= 0;

			@Override
			protected Map<String, Selection> fetchSelections(_DAO<_PesaDomainModelRow> dao) throws SQLException {
				Map<String, Selection> selections = new HashMap<>();
				SelectionImple selection = new SelectionImple(SELECTION_NAME);
				selection.addOption(new SelectionOptionImple(SELECTION_NAME, "" + i++));
				selections.put(SELECTION_NAME, selection);
				return selections;
			}

			@Override
			public _Functionality<_PesaDomainModelRow> functionalityFor(String page, _Request<_PesaDomainModelRow> request) throws UnsupportedOperationException {
				return null;
			}
		}

		private class FakeDAO extends OracleSQL_DAO<_PesaDomainModelRow> {
			public FakeDAO() {
				super(null, new PesaDomainModelRowFactory(structure, "", ""));
			}
			private long	time;
			public void setUpdatedTime(long time) {
				this.time = time;
			}

			@Override
			public long returnSelectionsUpdatedTime() throws SQLException {
				return this.time;
			}

		}

		private BaseFactoryExt	fact;
		private FakeDAO			dao;
		private final PesaDomainModelDatabaseStructure structure = hardCoded();
		private final Config config = init();
		private String configPath;

		private Config init() {
			Config testConfig = TestConfig.getConfig();
			String basefactorytestsConfigPath =  testConfig.baseFolder() + "AbstractFactoryTest.config";
			configPath = basefactorytestsConfigPath;
			try {
				return new ConfigReader(basefactorytestsConfigPath);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
				return null;
			}
		}

		@Override
		protected void setUp() throws Exception {
			fact = new BaseFactoryExt(configPath);
			dao = new FakeDAO();
		}

		@Override
		protected void tearDown() throws Exception {
			File file = historyActionsFile();
			if (file.exists()) {
				file.delete();
			}
		}

		public void test_it_loads_config() throws Exception {
			fact.setRowFactory(new PesaDomainModelRowFactory(structure, "", ""));
			Config c = fact.config();
			assertEquals("uname", c.get(Config.DB_USERNAME));
		}

		public void test_it_reads_uitexts_indicated_in_the_config() throws Exception {
			fact = new BaseFactoryExt(TestConfig.getConfigFile());
			fact.loadUITexts();
			LocalizedTextsContainer uiTexts = fact.uiTexts();
			assertEquals("Testi", uiTexts.getText("text.test", "FI"));
		}

		public void test_it_uses_previously_loaded_selection_after_setting_it() throws Exception {
			Map<String, Selection> selections = fact.selections(dao);
			assertEquals("0", selections.get(SELECTION_NAME).get(OPTION_VALUE));
			selections = fact.selections(dao);
			assertEquals("0", selections.get(SELECTION_NAME).get(OPTION_VALUE));
		}

		public void test_it_reloads_selection_if_timestamp_from_dao_is_different_than_the_previous_time() throws Exception {
			Map<String, Selection> selections = fact.selections(dao);
			assertEquals("0", selections.get(SELECTION_NAME).get(OPTION_VALUE));

			dao.setUpdatedTime(DateUtils.getCurrentEpoch() + 5);
			selections = fact.selections(dao);
			assertEquals("1", selections.get(SELECTION_NAME).get(OPTION_VALUE));

			dao.setUpdatedTime(DateUtils.getCurrentEpoch() + 50);
			selections = fact.selections(dao);
			assertEquals("2", selections.get(SELECTION_NAME).get(OPTION_VALUE));
			selections = fact.selections(dao);
			assertEquals("2", selections.get(SELECTION_NAME).get(OPTION_VALUE));
		}

		public void test___using__and__saving__and_loading__historyactions() throws Exception {

			_PesaDomainModelRow row = dao.newRow();
			row.getPesa().getUniqueNumericIdColumn().setValue("tämän id");
			assertEquals("tämänid", row.getPesa().getUniqueNumericIdColumn().getValue());
			fact.addHistoryAction(new HistoryAction("tämän id", row, Success.TARKASTUS_INSERT, "E O piiraiNe < e  \" öäöä ÄÖÄ ååå ÅÅ "));
			assertEquals(1, fact.getHistoryActions().size());

			fact.saveHistoryActions();

			File file = historyActionsFile();
			assertTrue(file.exists());

			List<HistoryAction> historyActions = new LinkedList<>();
			FileInputStream in = null;
			ObjectInputStream oin = null;
			try {
				in = new FileInputStream(file);
				oin = new ObjectInputStream(in);
				Object o = oin.readObject();
				if (o instanceof List<?>) {
					List<?> list = (List<?> ) o;
					Iterator<?> iterator = list.iterator();
					while (iterator.hasNext()) {
						o = iterator.next();
						if (o instanceof HistoryAction) {
							historyActions.add((HistoryAction) o);
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
				fail(e.getMessage());
			} finally {
				if (in != null) in.close();
				if (oin != null) oin.close();
			}
			assertEquals(1, historyActions.size());
			assertEquals(Success.TARKASTUS_INSERT, historyActions.get(0).getAction());
			assertEquals("E O piiraiNe < e  \" öäöä ÄÖÄ ååå ÅÅ ", historyActions.get(0).getUser());
			assertEquals("tämän id", historyActions.get(0).getId());
		}

		private File historyActionsFile() {
			return new File(config.baseFolder(), config.systemId() + "_history-actions.bin");
		}

	}

}
