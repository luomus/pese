package fi.hy.pese.framework.main.view;

import java.io.PrintWriter;

public class FunnyOwlExceptionViewer implements _ExceptionViewer {

	private final PrintWriter out;

	public FunnyOwlExceptionViewer(PrintWriter out) {
		this.out = out;
	}

	@Override
	public void view(Throwable condition) {
		out.print("<img src=\"Ohhoh.jpg\" alt=\"Jokin meni pieleen\">");
		out.print("<br /><pre>");
		condition.printStackTrace(out);
		out.print("</pre>");
		condition.printStackTrace();
	}

}
