package fi.hy.pese.merikotka.main.app;

import fi.hy.pese.framework.main.app.KirjekyyhkyValidatorServlet;
import fi.hy.pese.framework.main.app._ApplicationSpecificationFactory;
import fi.hy.pese.framework.main.general.data.PesaDomainModelDatabaseStructure;
import fi.hy.pese.framework.main.general.data.PesaDomainModelRowFactory;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.luomus.commons.db.connectivity.PreparedStatementStoringAndClosingTransactionConnection;
import fi.luomus.commons.db.connectivity.TransactionConnection;

public class MerikotkaKirjekyyhkyValidationServlet extends KirjekyyhkyValidatorServlet<_PesaDomainModelRow> {

	private static final long	serialVersionUID	= 1805476044253318666L;

	@Override
	protected _ApplicationSpecificationFactory<_PesaDomainModelRow> createFactory() throws Exception {
		MerikotkaKirjekyyhkyValidationServiceFactory factory = new MerikotkaKirjekyyhkyValidationServiceFactory("pese_merikotka.config");
		factory.loadUITexts();

		TransactionConnection con = new PreparedStatementStoringAndClosingTransactionConnection(factory.config().connectionDescription());
		try {
			PesaDomainModelDatabaseStructure structure = MerikotkaDatabaseStructureCreator.loadFromDatabase(con);
			factory.setRowFactory(new PesaDomainModelRowFactory(structure, MerikotkaConst.JOINS, MerikotkaConst.ORDER_BY));
		} catch (Exception e) {
			con.release();
			throw new Exception(e);
		}

		con.release();
		return factory;
	}

}
