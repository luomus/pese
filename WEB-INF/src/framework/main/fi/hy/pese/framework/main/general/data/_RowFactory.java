package fi.hy.pese.framework.main.general.data;

public interface _RowFactory<T extends _Row> {

	T newRow();

	T newRow(String prefix);

	String joins();

	String orderby();

}
