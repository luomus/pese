package fi.hy.pese.framework.main.app.functionality.validate;

import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data._Row;
import fi.hy.pese.framework.main.general.data._Table;

public class TablemanagementValidator<T extends _Row> extends Validator<T> {

	public TablemanagementValidator(_Request<T> request) {
		super(request);
	}

	@Override
	public void validate() throws Exception {

		if (data.action().equals("")) return;

		String tablename = data.get(Const.TABLE);
		if (isNull(tablename)) {
			error(SYSTEM_ERROR, SYSTEM_ERROR);
			return;
		}

		_Table updateparameters = data.updateparameters().getTableByName(tablename);

		if (data.action().equals(Const.SEARCH)) {
			_Table searchparameters = data.searchparameters().getTableByName(tablename);
			checkAsSearchparameters(searchparameters);
		} else if (data.action().equals(Const.UPDATE)) {
			checkRowIdIsNotNull(updateparameters);
			if (hasErrors()) return;
			checkRowIdMatchesKeys(updateparameters, dao);
			if (hasErrors()) return;
			checkAsUpdateparameters(updateparameters);
			checkRequiredNormalValuesAreGiven(updateparameters);
			checkReferenceValuesExists(updateparameters, dao);
		} else if (data.action().equals(Const.INSERT)) {
			checkRowidIsNull(updateparameters);
			if (hasErrors()) return;
			checkUniqueNumericIdNotGiven(updateparameters);
			if (hasErrors()) return;
			checkAsUpdateparameters(updateparameters);
			checkRequiredNormalValuesAreGiven(updateparameters);
			if (updateparameters.hasImmutableKeys()) {
				checkImmutableKeyCombinationDoesntAlreadyExist(updateparameters, dao);
			}
			checkReferenceValuesExists(updateparameters, dao);
		} else if (data.action().equals(Const.DELETE)) {
			checkRowIdIsNotNull(updateparameters);
			if (hasErrors()) return;
			checkRowIdExists(updateparameters, dao);
			checkNoReferencesToThisRowAndRowCanBeDeleted(updateparameters, dao);
		}
	}

}
