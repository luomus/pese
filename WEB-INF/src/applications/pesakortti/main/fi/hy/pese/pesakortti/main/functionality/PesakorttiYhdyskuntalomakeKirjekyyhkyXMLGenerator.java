package fi.hy.pese.pesakortti.main.functionality;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.db._DAO;
import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.main.kirjekyyhky.KirjekyyhkyXMLGeneratorUtility;
import fi.hy.pese.framework.main.kirjekyyhky.KirjekyyhkyXMLGeneratorUtility.StructureDefinition;
import fi.hy.pese.framework.main.kirjekyyhky._KirjekyyhkyXMLGenerator;
import fi.hy.pese.pesakortti.main.app.PesakorttiConst;
import fi.luomus.commons.config.Config;
import fi.luomus.commons.tipuapi.TipuAPIClient;
import fi.luomus.commons.utils.FileUtils;

public class PesakorttiYhdyskuntalomakeKirjekyyhkyXMLGenerator implements _KirjekyyhkyXMLGenerator {

	private static final String[]	COLUMNS_TO_EXLUDE	= { "id", "tarkastus" };
	private final _Request<_PesaDomainModelRow> request;
	private final Config config;
	private final _DAO<_PesaDomainModelRow> dao;
	private final KirjekyyhkyXMLGeneratorUtility generator;
	private final List<String> producedDataXMLFiles = new LinkedList<>();

	public PesakorttiYhdyskuntalomakeKirjekyyhkyXMLGenerator(_Request<_PesaDomainModelRow> request) throws Exception {
		this.request = request;
		this.config = request.config();
		this.dao = request.dao();
		this.generator = initGenerator();
	}

	@Override
	public File writeFormDataXML(Map<String, String> data, String filename) {
		throw new UnsupportedOperationException();
	}

	@Override
	public List<String> producedFormDataXMLFiles() {
		return producedDataXMLFiles;
	}

	public void writeStructureXML() throws Exception {
		String xml = generator.generateStructureXML();
		File file = new File(config.reportFolder(), "pesakortti-yhdyskunta.xml");
		try {
			FileUtils.writeToFile(file, xml, request.config().characterEncoding());
		} catch (Exception e) {
			request.data().addToNoticeTexts("VIRHE: " + e.getMessage() + " " + e.getStackTrace()[0]);
		}
	}

	private KirjekyyhkyXMLGeneratorUtility initGenerator() throws FileNotFoundException, IOException {
		KirjekyyhkyXMLGeneratorUtility generator = new KirjekyyhkyXMLGeneratorUtility();
		StructureDefinition definition = generator.getDefinition();

		definition.typeID = "pesakortti-yhdyskunta";
		definition.title = "Pesäkorttien yhdyskuntalomake";

		definition.menutexts.newForm = "Ilmoita uusi yhdyskunta";
		definition.menutexts.printEmptyPdf = "Tulosta tyhjä lomake";
		// definition.menutexts.printInstructions = "Tulosta täyttöohje";

		definition.titleFields.add("kaynti.1.pvm");
		definition.titleFields.add("pesa.kunta");
		definition.titleFields.add("pesa.tarkempi_paikka");
		definition.titleFields.add("tarkastus.pesiva_laji");
		definition.titleFields.add("tarkastus.tarkastaja");

		definition.validation.URI = config.get("ValidatonServiceURL_Yhdyskunta");
		definition.validation.username = config.get("ValidatonServiceUsername");
		definition.validation.password = config.get("ValidatonServicePassword");

		definition.coordinateFields.leveys = "pesa.leveys";
		definition.coordinateFields.pituus = "pesa.pituus";
		definition.coordinateFields.tyyppi = "pesa.koord_ilm_tyyppi";
		definition.coordinateFields.mittaustapa = "pesa.koord_mittaustapa";
		definition.coordinateFields.setTitleFields("pesa.tarkempi_paikka");

		_PesaDomainModelRow row = dao.newRow();
		definition.row = row;
		generator.excludeColumn("pesa", "seur_tarkastaja");
		generator.excludeColumn("pesa", "rak_laji");
		generator.excludeColumn("pesa", "yhdyskunta_laji");
		generator.excludeColumn("pesa", "yht_leveys");
		generator.excludeColumn("pesa", "yht_pituus");
		generator.excludeColumn("pesa", "ast_leveys");
		generator.excludeColumn("pesa", "ast_pituus");
		generator.excludeColumn("pesa", "des_leveys");
		generator.excludeColumn("pesa", "des_pituus");
		generator.excludeColumn("pesa", "eur_leveys");
		generator.excludeColumn("pesa", "eur_pituus");
		generator.excludeColumn("tarkastus", "id");
		generator.excludeColumn("tarkastus", "pesa");
		generator.excludeColumn("tarkastus", "vanha_kortti");
		generator.excludeColumn("tarkastus", "muninta_pvm");
		generator.excludeColumn("tarkastus", "taydellinen_munaluku");
		generator.excludeColumn("tarkastus", "yhdyskunnassa_vanha");
		generator.excludeColumn("tarkastus", "yhteispesinta");
		generator.excludeColumn("tarkastus", "epailyttava");
		generator.excludeColumn("tarkastus", "vanha_kortti");

		for (int i = 1; i<=50; i++) { // TODO nostettava yhdyskuntien lkm; 250?
			for (String c : COLUMNS_TO_EXLUDE) {
				generator.excludeColumn("kaynti."+i, c);
			}
			if (i != 1) {
				generator.excludeColumn("kaynti."+i, "pvm");
				generator.excludeColumn("kaynti."+i, "pvm_tarkkuus");
				generator.excludeColumn("kaynti."+i, "klo_h");
			}
			generator.excludeColumn("kaynti."+i, "klo_min");
			generator.excludeColumn("kaynti."+i, "ika_munat");
			generator.excludeColumn("kaynti."+i, "ika_poikaset");
			generator.addCustomColumn("kaynti."+i+".ika", _Column.INTEGER, 2);
			generator.includeTable("kaynti."+i);
		}

		generator.excludeColumn("pesa", "koord_tarkkuus");
		generator.addCustomColumn("pesa.leveys", _Column.INTEGER, 7);
		generator.addCustomColumn("pesa.pituus", _Column.INTEGER, 7);
		generator.addCustomColumn("pesa.koord_tarkkuus", _Column.VARCHAR, 7, "pesa.koord_tarkkuus");

		generator.setCustomFieldRequired("pesa.leveys");
		generator.setCustomFieldRequired("pesa.pituus");

		generator.includeTable("pesa");
		generator.includeTable("tarkastus");

		definition.selections = request.data().selections();

		generator.excludeSelection(TipuAPIClient.SPECIES);
		generator.excludeSelection(TipuAPIClient.MUNICIPALITIES);
		generator.excludeSelection(TipuAPIClient.RINGERS);
		generator.excludeSelection(TipuAPIClient.ELY_CENTRES);
		generator.excludeSelection(PesakorttiConst.TAULUT);

		generator.defineTipuApiSelection("pesa", "kunta", TipuAPIClient.MUNICIPALITIES);
		generator.defineTipuApiSelection("tarkastus", "pesiva_laji", TipuAPIClient.SPECIES);
		generator.defineTipuApiSelection("tarkastus", "tarkastaja", TipuAPIClient.RINGERS);
		generator.noCodesForSelection("k_e");

		generator.setRequired("tarkastus", "pesiva_laji");

		generator.userAsDefaultValue("tarkastus.tarkastaja");

		Map<String, String> fieldTitles = new HashMap<>();
		for (Entry<String, String> e : request.data().uiTexts().entrySet()) {
			if (e.getKey().startsWith("tarkastuslomake-yhdyskunta.")) {
				// if (e.getKey().startsWith("tarkastuslomake.aikuinen.")) continue;
				fieldTitles.put(e.getKey().substring("tarkastuslomake-yhdyskunta.".length()), e.getValue());
			} else {
				if (!fieldTitles.containsKey(e.getKey())) {
					fieldTitles.put(e.getKey(), e.getValue());
				}
			}
		}
		definition.fieldTitles = fieldTitles;

		definition.layout = FileUtils.readContents(new File(config.templateFolder(), "layout-yhdyskunta.xml"));

		definition.emptyPDF = new File(config.templateFolder(), "yhdyskuntalomake.pdf");
		return generator;
	}

}
