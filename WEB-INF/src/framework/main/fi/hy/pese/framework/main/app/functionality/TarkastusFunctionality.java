package fi.hy.pese.framework.main.app.functionality;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import fi.hy.pese.framework.main.app.FunctionalityExecutor;
import fi.hy.pese.framework.main.app._Functionality;
import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.app.functionality.validate.TarkastusValidator;
import fi.hy.pese.framework.main.app.functionality.validate._Validator;
import fi.hy.pese.framework.main.db.RowsToDataResultHandler;
import fi.hy.pese.framework.main.db.oraclesql.NothingToDeleteException;
import fi.hy.pese.framework.main.general.Utils;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.consts.Kirjekyyhky;
import fi.hy.pese.framework.main.general.consts.Success;
import fi.hy.pese.framework.main.general.data.HistoryAction;
import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.main.general.data._Table;
import fi.luomus.commons.kirjekyyhky.KirjekyyhkyAPIClient;
import fi.luomus.commons.tipuapi.TipuAPIClient;

public abstract class TarkastusFunctionality extends BaseFunctionality<_PesaDomainModelRow> {
	
	private static final String	LOMAKE_VASTAANOTTAJAT	= "lomake_vastaanottajat";
	
	public TarkastusFunctionality(_Request<_PesaDomainModelRow> request) {
		super(request);
	}
	
	public abstract void applicationSpecificDatamanipulation() throws Exception;
	
	@Override
	public _Validator validator() throws Exception {
		return new TarkastusValidator(request);
	}
	
	@Override
	public void preProsessingHook() throws Exception {
		lomakeVastaanottajatToDataListsFromParams();
	}
	
	private void lomakeVastaanottajatToDataListsFromParams() {
		if (data.getLists().containsKey(LOMAKE_VASTAANOTTAJAT)) return;
		for (String vastaanottaja : data.get("lomake_vastaanottaja").split("\\|")) {
			data.addToList(LOMAKE_VASTAANOTTAJAT, vastaanottaja);
		}
	}
	
	@Override
	public void afterFailedValidation() throws Exception {
		if (data.action().equals(Const.FILL_FOR_INSERT) || data.action().equals(Const.FILL_FOR_UPDATE)) {
			_Functionality<_PesaDomainModelRow> functionality = request.functionalityFor(Const.SEARCH_PAGE);
			data.setPage(Const.SEARCH_PAGE);
			_Column pesaid = data.updateparameters().getPesa().getUniqueNumericIdColumn();
			if (pesaid.hasValue()) {
				data.setAction(Const.SEARCH);
				data.searchparameters().getPesa().getUniqueNumericIdColumn().setValue(pesaid);
			} else {
				data.setAction("");
			}
			if (receivingFromKirjekyyhky()) {
				data.setAction(Const.INSERT);
			}
			FunctionalityExecutor.execute(functionality);
		} else if (!data.action().equals(Const.INSERT_FIRST)){
			fetchAllInspectionsByPesaIdOrTarkastusId();
		}
	}
	
	@Override
	public void afterSuccessfulValidation() throws Exception {
		if (actionNotSet()) {
			data.setAction(Const.FILL_FOR_INSERT_FIRST);
		}
		
		if (data.action().equals(Const.FILL_FOR_INSERT) || data.action().equals(Const.FILL_FOR_UPDATE)) {
			fetchAllInspectionsByPesaIdOrTarkastusId();
		}
		
		applicationSpecificDatamanipulation(); // For example prefills the correct row as updateparameters for FILL_FOR_INSERT
		
		if (data.action().equals(Const.FILL_FOR_UPDATE)) {
			String tarkastusid = data.updateparameters().getTarkastus().getUniqueNumericIdColumn().getValue();
			if (given(tarkastusid)) {
				data.setUpdateparameters(data.copyOf(selectCorrectInspectionForUpdate(tarkastusid)));
			} else {
				data.setUpdateparameters(data.copyOf(data.results().get(0)));
			}
			lomakeVastaanottajatToDataListsFromDb();
		} else if (data.action().equals(Const.FILL_FOR_INSERT)) {
			data.updateparameters().getPoikaset().clearAllValues();
			data.updateparameters().getAikuiset().clearAllValues();
			data.updateparameters().getKaynnit().clearAllValues();
			lomakeVastaanottajatToDataListsFromDb();
		}
		
		if (data.action().equals(Const.INSERT_FIRST)) {
			doInsertFirst();
			persistLomakeVastaanottajat();
			markForWarehousing();
			data.setSuccessText(Success.TARKASTUS_INSERT_FIRST);
			markReceivedToKirjekyyhkyIfReceivingFormFromKirjekyyhky();
			addActionToActionHistory();
			reloadForUpdate();
		} else if (data.action().equals(Const.INSERT)) {
			doInsert();
			persistLomakeVastaanottajat();
			markForWarehousing();
			data.setSuccessText(Success.TARKASTUS_INSERT);
			markReceivedToKirjekyyhkyIfReceivingFormFromKirjekyyhky();
			addActionToActionHistory();
			reloadForUpdate();
		} else if (data.action().equals(Const.UPDATE)) {
			doUpdate();
			persistLomakeVastaanottajat();
			markForWarehousing();
			data.setSuccessText(Success.TARKASTUS_UPDATE);
			addActionToActionHistory();
			reloadForUpdate();
		} else {
			setNextAction();
		}
	}
	
	private void markForWarehousing() throws Exception {
		dao.markForWarehousing(data.updateparameters().getTarkastus().getUniqueNumericIdColumn().getIntegerValue());
	}
	
	private void lomakeVastaanottajatToDataListsFromDb() throws SQLException {
		if (!dao.newRow().getLomakeVastaanottajat().isInitialized()) return;
		
		if (!data.getLists().containsKey(LOMAKE_VASTAANOTTAJAT)) {
			data.getLists().put(LOMAKE_VASTAANOTTAJAT, new ArrayList<>());
		}
		List<Object> dataListVastaanottajat = data.getLists().get(LOMAKE_VASTAANOTTAJAT);
		
		String pesaid = data.updateparameters().getPesa().getUniqueNumericIdColumn().getValue();
		for (Object dbVastaanottaja : getLomakeVastaanottajatFromDb(pesaid)) {
			if (!dataListVastaanottajat.contains(dbVastaanottaja)) {
				dataListVastaanottajat.add(dbVastaanottaja);
			}
		}
		data.addToList(LOMAKE_VASTAANOTTAJAT, "");
	}
	
	private List<Object> getLomakeVastaanottajatFromDb(String pesaid) throws SQLException {
		List<Object> vastaanottajat = new ArrayList<>();
		List<_Table> tables = getLomakeVastaanottajatAsTableListFromDb(pesaid);
		for (_Table t : tables) {
			String ringerNumber = t.get("vastaanottaja").getValue();
			String ringerName = data.selections().get(TipuAPIClient.RINGERS).get(ringerNumber);
			String combined = ringerName + " (" + ringerNumber + ")";
			vastaanottajat.add(combined);
		}
		return vastaanottajat;
	}
	
	private List<_Table> getLomakeVastaanottajatAsTableListFromDb(String pesaid) throws SQLException {
		_Table searchParam = dao.newRow().getLomakeVastaanottajat();
		searchParam.get("pesa").setValue(pesaid);
		return  dao.returnTables(searchParam, "vastaanottaja");
	}
	
	private void persistLomakeVastaanottajat() throws SQLException {
		if (!dao.newRow().getLomakeVastaanottajat().isInitialized()) return;
		
		String pesaid = data.updateparameters().getPesa().getUniqueNumericIdColumn().getValue();
		List<_Table> dbVastaanottajat = getLomakeVastaanottajatAsTableListFromDb(pesaid);
		Set<Integer> formVastaanottajat = parseVastaanottajatFromFormData();
		
		deleteRemoved(dbVastaanottajat, formVastaanottajat);
		insertAdded(pesaid, formVastaanottajat);
	}
	
	private void insertAdded(String pesaid, Set<Integer> formVastaanottajat) throws SQLException {
		for (int vastaanottaja : formVastaanottajat) {
			_Table t = dao.newRow().getLomakeVastaanottajat();
			t.get("pesa").setValue(pesaid);
			t.get("vastaanottaja").setValue(vastaanottaja);
			dao.executeInsert(t);
		}
	}
	
	private void deleteRemoved(List<_Table> dbVastaanottajat, Set<Integer> formVastaanottajat) throws SQLException, NothingToDeleteException {
		for (_Table t : dbVastaanottajat) {
			int vastaanottaja = t.get("vastaanottaja").getIntegerValue();
			if (formVastaanottajat.contains(vastaanottaja)) {
				noActionNeeded(formVastaanottajat, vastaanottaja);
			} else {
				dao.executeDelete(t);
			}
		}
	}
	
	private void noActionNeeded(Set<Integer> formVastaanottajat, int vastaanottaja) {
		formVastaanottajat.remove(vastaanottaja);
	}
	
	private Set<Integer> parseVastaanottajatFromFormData() {
		Set<Integer> set = new HashSet<>();
		for (Object o : data.getLists().get(LOMAKE_VASTAANOTTAJAT)) {
			String vastaanottaja = o.toString();
			if (vastaanottaja.length() == 0) continue;
			int rengastajaNro = Utils.onlyNumbers(vastaanottaja);
			set.add(rengastajaNro);
		}
		return set;
	}
	
	protected void markReceivedToKirjekyyhkyIfReceivingFormFromKirjekyyhky() throws Exception {
		if (receivingFromKirjekyyhky()) {
			KirjekyyhkyAPIClient api = null;
			try {
				api = request.kirjekyyhkyAPI();
				api.receive(data.get(Kirjekyyhky.ID));
			} finally {
				if (api != null) api.close();
			}
			data.set(Kirjekyyhky.KIRJEKYYHKY, null);
		}
	}
	
	private boolean receivingFromKirjekyyhky() {
		return Const.YES.equals(data.get(Kirjekyyhky.KIRJEKYYHKY));
	}
	
	private void reloadForUpdate() throws Exception {
		data.setAction(Const.FILL_FOR_UPDATE);
		String pesaid = data.updateparameters().getPesa().getUniqueNumericIdColumn().getValue();
		String tarkastusid = data.updateparameters().getTarkastus().getUniqueNumericIdColumn().getValue();
		data.results().clear();
		data.updateparameters().clearAllValues();
		data.updateparameters().getPesa().getUniqueNumericIdColumn().setValue(pesaid);
		data.updateparameters().getTarkastus().getUniqueNumericIdColumn().setValue(tarkastusid);
		_Functionality<_PesaDomainModelRow> functionality = request.functionalityFor(Const.TARKASTUS_PAGE);
		FunctionalityExecutor.execute(functionality);
	}
	
	private void setNextAction() {
		String action = data.action();
		if (action.equals(Const.FILL_FOR_INSERT))
			data.setAction(Const.INSERT);
		else if (action.equals(Const.FILL_FOR_INSERT_FIRST))
			data.setAction(Const.INSERT_FIRST);
		else
			data.setAction(Const.UPDATE);
	}
	
	private boolean actionNotSet() {
		return data.action().length() < 1;
	}
	
	private _PesaDomainModelRow selectCorrectInspectionForUpdate(String tarkastusid) {
		for (_PesaDomainModelRow row : data.results()) {
			if (row.getTarkastus().getUniqueNumericIdColumn().getValue().equals(tarkastusid)) {
				return row;
			}
		}
		throw new IllegalStateException("No tarkastus found for this id!");
	}
	
	private void addActionToActionHistory() {
		request.addHistoryAction(new HistoryAction(
				data.updateparameters().getPesa().getUniqueNumericIdColumn().getValue(),
				data.copyOf(data.updateparameters()),
				data.successText(),
				data.userName()));
	}
	
	//	private boolean pesaIdOk() {
	//		return !data.hasErrors(data.updateparameters().getPesa().getUniqueNumericIdColumn());
	//	}
	//
	//	private void executeSearchFunctionalityWithThisPesaIdAsParams() throws Exception {
	//		data.setPage(Const.SEARCH_PAGE);
	//		data.setAction(Const.SEARCH);
	//		String pesaid = data.updateparameters().getPesa().getUniqueNumericIdColumn().getValue();
	//		String tarkastusid = data.updateparameters().getTarkastus().getUniqueNumericIdColumn().getValue();
	//		data.searchparameters().getPesa().getUniqueNumericIdColumn().setValue(pesaid);
	//		data.searchparameters().getTarkastus().getUniqueNumericIdColumn().setValue(tarkastusid);
	//		_Functionality searchFunctionality = request.functionalityFor(Const.SEARCH_PAGE);
	//		FunctionalityExecutor.execute(searchFunctionality);
	//	}
	
	
	
	private void fetchAllInspectionsByPesaIdOrTarkastusId() throws SQLException {
		String pesaid = data.updateparameters().getPesa().getUniqueNumericIdColumn().getValue();
		String tarkastusid = data.updateparameters().getTarkastus().getUniqueNumericIdColumn().getValue();
		
		if (!given(pesaid) && given(tarkastusid)) {
			_Table tarkastus = dao.returnTableByKeys(data.searchparameters().getTarkastus().getName(), tarkastusid);
			_Column tarkastusColumnThatReferencesPesa = tarkastus.getReferencingColumn(data.updateparameters().getPesa());
			pesaid = tarkastusColumnThatReferencesPesa.getValue();
			if (!given(pesaid)) {
				throw new IllegalStateException("No pesaid found for this tarkastusid!");
			}
		} else if (!given(pesaid)) {
			throw new IllegalArgumentException("No pesaid or tarkastusid given! Validator should make sure this does not happen.");
		}
		_PesaDomainModelRow params = data.newRow();
		params.getPesa().getUniqueNumericIdColumn().setValue(pesaid);
		dao.executeSearch(params, new RowsToDataResultHandler(data, dao));
		if (data.results().size() < 1) {
			throw new IllegalStateException("Parameters did not produce results!");
		}
	}
	
	protected boolean given(String value) {
		return value.length() > 0;
	}
	
	// Applications must make these do something, prefearebly using the protected methods provided bellow
	protected abstract void doInsertFirst() throws Exception;
	
	protected abstract void doInsert() throws Exception;
	
	protected abstract void doUpdate() throws Exception;
	
	protected void fetchIdAndInsert(_Table table) throws SQLException {
		dao.returnAndSetNextId(table);
		dao.executeInsert(table);
	}
	
	protected void insertKaynnit(_PesaDomainModelRow params) throws SQLException {
		for (_Table kaynti : params.getKaynnit().values()) {
			if (kaynti.hasValues()) {
				fetchIdAndInsert(kaynti);
			}
		}
	}
	
	protected void insertPoikaset(_PesaDomainModelRow params) throws SQLException {
		for (_Table poikanen : params.getPoikaset().values()) {
			if (poikanen.hasValues()) {
				fetchIdAndInsert(poikanen);
			}
		}
	}
	
	protected void insertAikuiset(_PesaDomainModelRow params) throws SQLException {
		for (_Table aikuinen : params.getAikuiset().values()) {
			if (aikuinen.hasValues()) {
				fetchIdAndInsert(aikuinen);
			}
		}
	}
	
	protected void updateIfChanges(_Table table, String... ignoredColumns) throws SQLException {
		if (hasChanges(table, ignoredColumns)) {
			dao.executeUpdate(table);
		}
	}
	
	protected void insertIfChanges(_Table table, String... ignoredColumns) throws SQLException {
		if (hasChanges(table, ignoredColumns)) {
			fetchIdAndInsert(table);
		}
	}
	
	protected boolean hasChanges(_Table table, String... ignoredColumns) throws SQLException {
		if (newRow(table)) return true;
		_Table existing_data = dao.returnTableByRowid(table.getName(), table.getRowidValue());
		return !table.equalsIgnoreMetadata(existing_data, ignoredColumns);
	}
	
	private boolean newRow(_Table table) {
		return !table.get(Const.ROWID).hasValue();
	}
	
	protected void updateOrInsertOlosuhdeIfChanges(_PesaDomainModelRow params, String... ignoredColumns) throws SQLException {
		_Table current_olosuhde = params.getOlosuhde();
		_Table current_tarkastus = params.getTarkastus();
		_Table prev_olosuhde = dao.returnTableByRowid(current_olosuhde.getName(), current_olosuhde.getRowidValue());
		if (!current_olosuhde.equalsIgnoreMetadata(prev_olosuhde, ignoredColumns)) {
			if (prevOlosuhdeIsForThisYear(current_tarkastus, prev_olosuhde)) {
				dao.executeUpdate(params.getOlosuhde());
			} else {
				fetchIdAndInsert(current_olosuhde);
			}
		}
	}
	
	/**
	 * Function to find out if there is olosuhde row for the year in question.
	 * The "year in question" might for example be "tarkastus.tark_vuosi", and the year of the previous olosuhderow "olosuhde.ilm_vuosi"
	 * @param params
	 * @param prev_olosuhde
	 * @return true if row exists for the year in question, false if not
	 */
	protected abstract boolean prevOlosuhdeIsForThisYear(_Table current_tarkastus, _Table prev_olosuhde);
	
	protected void updateKaynnit(_PesaDomainModelRow params) throws SQLException {
		for (_Table kaynti : params.getKaynnit().values()) {
			if (!kaynti.hasValues()) continue;
			if (newRow(kaynti)) {
				fetchIdAndInsert(kaynti);
			} else {
				updateIfChanges(kaynti);
			}
		}
	}
	
	protected void updatePoikaset(_PesaDomainModelRow params) throws SQLException {
		for (_Table poikanen : params.getPoikaset().values()) {
			if (!poikanen.hasValues()) continue;
			if (newRow(poikanen)) {
				fetchIdAndInsert(poikanen);
			} else {
				updateIfChanges(poikanen);
			}
		}
	}
	
	protected void updateAikuiset(_PesaDomainModelRow params) throws SQLException {
		for (_Table aikuinen : params.getAikuiset().values()) {
			if (!aikuinen.hasValues()) continue;
			if (newRow(aikuinen)) {
				fetchIdAndInsert(aikuinen);
			} else {
				updateIfChanges(aikuinen);
			}
		}
	}
	
	protected void updateMunat(_PesaDomainModelRow params) throws SQLException {
		for (_Table muna : params.getMunat().values()) {
			if (!muna.hasValues()) continue;
			if (newRow(muna)) {
				fetchIdAndInsert(muna);
			} else {
				updateIfChanges(muna);
			}
		}
	}
	
	protected _PesaDomainModelRow lastRow(List<_PesaDomainModelRow> results) {
		return results.get(results.size() - 1);
	}
	
}
