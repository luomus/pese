package fi.hy.pese.ulko.main.app;

public class UlkoConst {

	public static final String		JOINS											= joins();

	private static String joins() {
		StringBuilder joins = new StringBuilder();
		joins.append("       pyynto.loyto     =  loyto.id             ");
		joins.append(" AND   pyynto.rengastus =  rengastus.id(+)      ");
		return joins.toString();
	}

	public static final String	ORDER_BY	= " loyto.muutos_pvm DESC, loyto.scheme ";

}
