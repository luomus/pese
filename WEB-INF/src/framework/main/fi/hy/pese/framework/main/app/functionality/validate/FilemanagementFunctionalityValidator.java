package fi.hy.pese.framework.main.app.functionality.validate;

import java.util.Set;

import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.app.functionality.FilemanagementFunctionality;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data._Row;
import fi.luomus.commons.utils.Utils;

public class FilemanagementFunctionalityValidator<T extends _Row> extends Validator<T> {

	private static final Set<String >ACCEPTED_FILETYPES = Utils.set(".pdf", ".txt", ".zip", ".xml");

	public FilemanagementFunctionalityValidator(_Request<T> request) {
		super(request);
	}

	@Override
	public void validate() throws Exception {
		if (data.action().equals(Const.SHOW)) {

			String folderType = data.get(FilemanagementFunctionality.FOLDER);
			String filename = data.get(FilemanagementFunctionality.FILENAME);

			if (isNull(folderType) || isNull(filename)) {
				error(_Validator.SYSTEM_ERROR, _Validator.SYSTEM_ERROR);
			}
			if (hasErrors()) return;

			filename = filename.toLowerCase();
			try {
				if (ACCEPTED_FILETYPES.contains(filename.substring(filename.length() - 4))) {
					// ok
				} else {
					error(_Validator.SYSTEM_ERROR, _Validator.SYSTEM_ERROR);
				}
			}
			catch (Exception e) {
				error(_Validator.SYSTEM_ERROR, _Validator.SYSTEM_ERROR);
			}
		}

	}

}
