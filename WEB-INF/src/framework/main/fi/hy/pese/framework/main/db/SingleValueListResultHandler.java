package fi.hy.pese.framework.main.db;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

public class SingleValueListResultHandler implements _ResultsHandler {

	public List<String> results;

	@Override
	public void process(_ResultSet rs) throws SQLException {
		results = new LinkedList<>();
		while (rs.next()) {
			results.add(rs.getString(1));
		}
	}

}
