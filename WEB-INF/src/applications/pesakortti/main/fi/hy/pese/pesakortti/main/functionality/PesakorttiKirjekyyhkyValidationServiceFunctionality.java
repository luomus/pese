package fi.hy.pese.pesakortti.main.functionality;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.app.functionality.KirjekyyhkyValidationServiceFunctionality;
import fi.hy.pese.framework.main.app.functionality.validate._Validator;
import fi.hy.pese.framework.main.general.PesimisenBiologisetRajatUtil;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.main.general.data._Table;
import fi.hy.pese.pesakortti.main.app.PesakorttiConst;
import fi.hy.pese.pesakortti.main.functionality.validation.PesakorttiTarkastusValidator;

public class PesakorttiKirjekyyhkyValidationServiceFunctionality extends KirjekyyhkyValidationServiceFunctionality<_PesaDomainModelRow> {


	private static final boolean	KIRJEKYYHKY_VALIDATIONS_ONLY	= true;

	public PesakorttiKirjekyyhkyValidationServiceFunctionality(_Request<_PesaDomainModelRow> request) {
		super(request);
	}

	@Override
	public _Validator validator() throws Exception {
		try {
			List<_Table> results = dao.returnTable("ponttoalue");
			Map<Integer, _Table> ponttoalueet = new HashMap<>();
			for (_Table t : results) {
				ponttoalueet.put(t.getUniqueNumericIdColumn().getIntegerValue(), t);
			}
			return new PesakorttiTarkastusValidator(request, KIRJEKYYHKY_VALIDATIONS_ONLY, ponttoalueet, PesimisenBiologisetRajatUtil.getInstance(config));
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void preProsessingHook() throws Exception {
		super.preProsessingHook();
		PesakorttiConst.kirjekyyhkyModelToData(data.getAll(), data.updateparameters());
	}



}
