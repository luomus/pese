package fi.hy.pese.framework.main.app.functionality;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.app.functionality.validate.PesintamanagementValidator;
import fi.hy.pese.framework.main.app.functionality.validate._Validator;
import fi.hy.pese.framework.main.db.CountCombinedRowsResultHandler;
import fi.hy.pese.framework.main.db.RowsToDataResultHandler;
import fi.hy.pese.framework.main.db.oraclesql.NothingToDeleteException;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.consts.Success;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.main.general.data._Table;
import fi.luomus.commons.utils.Utils;

public class PesintamanagementFunctionality extends BaseFunctionality<_PesaDomainModelRow> {

	private final String vuosiColumnName;

	public PesintamanagementFunctionality(_Request<_PesaDomainModelRow> request) {
		this(request, "vuosi");
	}

	public PesintamanagementFunctionality(_Request<_PesaDomainModelRow> request, String vuosiColumnName) {
		super(request);
		this.vuosiColumnName = vuosiColumnName;
	}

	@Override
	public void preProsessingHook() throws Exception {}

	@Override
	public _Validator validator() {
		return new PesintamanagementValidator(request);
	}

	@Override
	public void afterSuccessfulValidation() throws Exception {

		if (data.action().equals("")) return;

		getRowsByParams();

		if (data.action().equals(Const.DELETE_NEST)) {
			deleteNestAndAllTarkastus();
		}
		if (data.action().equals(Const.DELETE_TARKASTUKSET)) {
			deleteTarkastukset();
		}
		if (data.action().equals(Const.MOVE_TARKASTUKSET) || data.action().equals(Const.MOVE_TARKASTUKSET_TO_NEW)) {
			moveTarkastukset();
		}
	}

	private void moveTarkastukset() throws SQLException {
		String targetNestId = data.get(Const.MOVE_TO_NEST_ID);
		if (data.action().equals(Const.MOVE_TARKASTUKSET_TO_NEW)) {
			_Table newNest = data.newRow().getPesa();
			newNest.setValues(data.results().get(0).getPesa());
			targetNestId = dao.returnAndSetNextId(newNest);
			dao.executeInsert(newNest);
			data.set(Const.NEW_NEST_ID, targetNestId);
			data.set(Const.MOVE_TO_NEST_ID, targetNestId);
		}

		for (_PesaDomainModelRow r : data.results()) {
			//r.getPesa().getUniqueNumericIdColumn().getReferencingColumns()...
			r.getTarkastus().get("pesa").setValue(targetNestId); // TODO miten saadaan yleiskäyttöisesti pesä:n id -columnin nimi?  "references...."
			dao.executeUpdate(r.getTarkastus());
			addLogEntry(r, data.action());
		}

		if (data.results().size() == 1) {
			data.setSuccessText(Success.TARKASTUS_MOVE);
		} else {
			data.setSuccessText(Success.TARKASTUS_MOVE_MULTIPLE);
		}

		if (countOfRowsAfterMoves() == 0) {
			deleteNestThatWasLeftEmpty();
		}

		data.setAction("");
		data.updateparameters().clearAllValues();
	}

	private void deleteNestThatWasLeftEmpty() throws SQLException {
		_Table pesa = data.results().get(0).getPesa();
		deleteLomakeVastaanottajat(pesa);
		dao.executeDelete(pesa);
		addLogEntry(data.results().get(0), Const.DELETE_NEST);
		if (data.results().size() == 1) {
			data.setSuccessText(Success.TARKASTUS_MOVE_NEST_DELETE);
		} else {
			data.setSuccessText(Success.TARKASTUS_MOVE_MULTIPLE_NEST_DELETE);
		}
	}

	private int countOfRowsAfterMoves() throws SQLException {
		CountCombinedRowsResultHandler resultsHandler = new CountCombinedRowsResultHandler(dao);
		_PesaDomainModelRow parameters = data.newRow();
		parameters.getPesa().getUniqueNumericIdColumn().setValue(data.updateparameters().getPesa().getUniqueNumericIdColumn().getValue());
		dao.executeSearch(parameters, resultsHandler);
		return resultsHandler.numberOfRows;
	}

	private void deleteTarkastukset() throws Exception {
		for (_PesaDomainModelRow r : data.results()) {
			deleteTarkastus(r, false);
			addLogEntry(r, Const.DELETE_TARKASTUKSET);
		}
		if (data.results().size() == 1) {
			data.setSuccessText(Success.TARKASTUS_DELETE);
		} else {
			data.setSuccessText(Success.TARKASTUS_DELETE_MULTIPLE);
		}
		executeSelectNestPage();
	}

	private void executeSelectNestPage() throws Exception {
		data.setAction(Const.SELECT_NEST);
		data.clearResults();
		String pesaId = data.updateparameters().getPesa().getUniqueNumericIdColumn().getValue();
		data.updateparameters().clearAllValues();
		data.updateparameters().getPesa().getUniqueNumericIdColumn().setValue(pesaId);
		afterSuccessfulValidation();
	}

	private void deleteNestAndAllTarkastus() throws SQLException {
		_Table pesa = data.results().get(0).getPesa();
		for (_PesaDomainModelRow r : data.results()) {
			deleteTarkastus(r, true);
		}
		deleteLomakeVastaanottajat(pesa);
		dao.executeDelete(pesa);
		addLogEntry(data.results().get(0), Const.DELETE_NEST);

		data.setSuccessText(Success.PESA_DELETE);
		data.setAction("");
		data.updateparameters().clearAllValues();
	}

	private void deleteLomakeVastaanottajat(_Table pesa) throws SQLException {
		if (!data.results().get(0).getLomakeVastaanottajat().isInitialized()) return;
		PreparedStatement p = null;
		try {
			p = dao.prepareStatement(" DELETE FROM lomake_vastaanottaja WHERE pesa = ? ");
			p.setString(1, pesa.getUniqueNumericIdColumn().getValue());
			p.execute();
		} finally {
			Utils.close(p);
		}
	}

	private void addLogEntry(_PesaDomainModelRow row, String action) throws SQLException {
		_Table logEntry = dao.newTableByName("muutosloki");
		if (!logEntry.isInitialized()) return;

		dao.returnAndSetNextId(logEntry);
		logEntry.get("pesa").setValue(row.getPesa().getUniqueNumericIdColumn().getValue());
		logEntry.get("kayttaja").setValue(data.userName());

		String logEntryText = data.uiTexts().get(action);
		if (logEntryText.contains("YYYY")) {
			logEntryText = logEntryText.replace("YYYY", row.getTarkastus().get(vuosiColumnName).getValue());
		}
		if (logEntryText.contains("NEW_ID")) {
			logEntryText = logEntryText.replace("NEW_ID", data.get(Const.MOVE_TO_NEST_ID));
		}
		logEntry.get("kirjaus").setValue(logEntryText);
		dao.executeInsert(logEntry);
	}

	private void deleteTarkastus(_PesaDomainModelRow r, boolean deleteAll) throws SQLException {
		if (r.getKaynti().isInitialized()) {
			for (_Table kaynti : r.getKaynnit()) {
				deleteIfExists(kaynti);
			}
		}
		if (r.getPoikanen().isInitialized()) {
			for (_Table poikanen : r.getPoikaset()) {
				deleteIfExists(poikanen);
			}
		}
		if (r.getAikuinen().isInitialized()) {
			for (_Table aikuinen : r.getAikuiset()) {
				deleteIfExists(aikuinen);
			}
		}
		if (r.getVuosi().isInitialized()) {
			deleteIfExists(r.getVuosi());
		}
		if (r.getMuna().isInitialized()) {
			for (_Table muna : r.getMunat()) {
				deleteIfExists(muna);
			}
		}
		if (deleteAll) {
			if (r.getOlosuhde().isInitialized()) {
				deleteIfExists(r.getOlosuhde());
			}
		}
		dao.executeDelete(r.getTarkastus());
	}

	private void deleteIfExists(_Table table) throws SQLException, NothingToDeleteException {
		try {
			if (table.hasValues()) dao.executeDelete(table);
		} catch (NothingToDeleteException e) { }
	}

	private void getRowsByParams() throws SQLException, Exception {
		dao.executeSearch(data.updateparameters(), new RowsToDataResultHandler(data, dao));
		if (data.results().size() < 1) {
			throw new Exception("No results. Validator should make sure this does not happen.");
		}
	}

	@Override
	public void afterFailedValidation() throws Exception {
		String action = data.action();
		if (action.equals(Const.DELETE_TARKASTUKSET) || action.equals(Const.MOVE_TARKASTUKSET) || action.equals(Const.MOVE_TARKASTUKSET_TO_NEW)) {
			executeSelectNestPage();
			return;
		}
		data.setAction("");
	}

	@Override
	public void postProsessingHook() throws Exception {}

}
