package fi.hy.pese.framework.main.app;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data.ParameterMap;
import fi.hy.pese.framework.main.general.data._Row;
import fi.luomus.commons.http.HttpStatus;
import fi.luomus.commons.utils.Utils;

/**
 * The abstract base servlet for PeSe Framework Application's Kirjekyyhky Validation service.
 * The applications need to implement the factory() -method. The factory contains all the
 * application specific configuration and data templates, as defined in the _Factory interface.
 * The applications may also override other features of this class, or define their very own Servlet from scratch.
 * @see _ApplicationSpecificationFactory
 */
public abstract class ApiServlet<T extends _Row> extends KirjekyyhkyValidatorServlet<T> {
	
	private static final long	serialVersionUID	= 4176399088595838206L;
	
	private static final Collection<String> STANDARD_PARAMETERS = Utils.collection("value", "field", "resultfields", "format", "startAt", "limit", "resulttype");
	
	@Override
	protected abstract _ApplicationSpecificationFactory<T> createFactory() throws Exception;
	
	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		process(req, res);
	}
	
	@Override
	public void process(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		req.setCharacterEncoding("UTF-8");
		
		String format = req.getParameter("format");
		if (format == null) format = "xml";
		format = format.toLowerCase();
		
		if (format.equals("csv")) {
			res.setContentType("text/csv; charset=utf-8");
		} else if (format.equals("json")) {
			res.setContentType("application/json; charset=utf-8");
		} else {
			res.setContentType("application/xml; charset=utf-8");
		}
		
		PrintWriter out = res.getWriter();
		
		Map<String, String> params = new HashMap<>();
		
		@SuppressWarnings("rawtypes")
		Enumeration e = req.getParameterNames();
		while (e.hasMoreElements()) {
			String key = (String) e.nextElement();
			String[] values = req.getParameterValues(key);
			if (STANDARD_PARAMETERS.contains(key)) {
				params.put(key, values[0]);
			} else {
				key = Const.SEARCHPARAMETERS + "." + key;
				for (String value : values) {
					if (params.containsKey(key)) {
						params.put(key, params.get(key) + "|" + value);
					} else {
						params.put(key, value);
					}
				}
			}
		}
		
		String requestURI = Utils.getRequestURIAndQueryString(req);
		
		control.process(new ParameterMap(params), fakeSessionHandler, out, new HttpStatus(res), requestURI);
	}
	
}
