package fi.hy.pese.framework.main.db.oraclesql;

import java.util.LinkedList;
import java.util.List;

import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._Table;
import fi.luomus.commons.db.connectivity.TransactionConnection;
import fi.luomus.commons.utils.DateUtils;

public class InsertStatement extends Statement {

	private String				table	= "";
	private final List<String>	columnList;
	private final List<String>	valueStatementList;

	public InsertStatement(TransactionConnection con) {
		super(con);
		columnList = new LinkedList<>();
		valueStatementList = new LinkedList<>();
	}

	@Override
	protected String constructQuery() {
		StringBuilder query = new StringBuilder(" INSERT INTO ");
		query.append(table).append(" ( ");
		toCommaSeperatedStatement(query, columnList);
		query.append(" ) VALUES ( ");
		toCommaSeperatedStatement(query, valueStatementList);
		query.append(" ) ");
		return query.toString();
	}

	public void insert(_Table table) {
		this.table = table.getName();
		for (_Column c : table) {
			if (isRowIdColumn(c)) continue;
			columnList.add(c.getName());
			if (isDateAddedColumn(c) || isDateModifiedColumn(c) || c.getValue().equals(Const.SYSDATE)) {
				valueStatementList.add(" SYSDATE ");
			} else {
				valueStatementList.add(" ? ");
				if (c.hasValue() && c.getDatatype() == _Column.DECIMAL) {
					values.add(c.getDoubleValue());
				} else if (c.hasValue() && c.getDatatype() == _Column.DATE) {
					values.add(DateUtils.convertToDate(c.getDateValue()));
				} else {
					values.add(c.getValue());
				}
			}
		}
	}

}
