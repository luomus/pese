package fi.hy.pese.pesakortti.main.functionality;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import fi.hy.pese.framework.main.app.FunctionalityExecutor;
import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.app.functionality.validate._Validator;
import fi.hy.pese.framework.main.general.PesimisenBiologisetRajatUtil;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.main.general.data._Table;
import fi.hy.pese.pesakortti.main.functionality.validation.PesakorttiYhdyskuntaTarkastusValidator;

public class PesakorttiYhdyskuntaTarkastusFunctionality extends PesakorttiTarkastusFunctionality {
	
	public PesakorttiYhdyskuntaTarkastusFunctionality(_Request<_PesaDomainModelRow> request) {
		super(request);
	}
	
	@Override
	public void preProsessingHook() throws Exception {
		super.preProsessingHook();
		_Table pesa = data.updateparameters().getPesa();
		_Table tarkastus = data.updateparameters().getTarkastus();
		pesa.get("lomake").setValue("E");
		tarkastus.get("yhdyskunta_laji").setValue(tarkastus.get("pesiva_laji"));
		tarkastus.get("yhdyskunnassa").setValue("K");
		tarkastus.get("uusintapesinta_jarjestysnro").setValue(0);
		
		_Table kaynti_1 = data.updateparameters().getKaynnit().get(1);
		String pvm = kaynti_1.get("pvm").getValue();
		String pvm_tark = kaynti_1.get("pvm_tarkkuus").getValue();
		String h = kaynti_1.get("klo_h").getValue();
		String min = kaynti_1.get("klo_min").getValue();
		for (_Table kaynti : data.updateparameters().getKaynnit()) {
			if (!kaynti.hasValues()) continue;
			kaynti.get("pvm").setValue(pvm);
			kaynti.get("pvm_tarkkuus").setValue(pvm_tark);
			kaynti.get("klo_h").setValue(h);
			kaynti.get("klo_min").setValue(min);
		}
	}
	
	@Override
	protected boolean pesinnanVaiheToSeparatedColumnsFromData(boolean notUsed, _Table kaynti, int i) {
		String value = data.get("PesinnanVaihe."+i).trim();
		if (value.length() < 1) return notUsed;
		try {
			Integer ika = Integer.valueOf(value);
			if (kaynti.get("lkm_poikaset").hasValue() || kaynti.get("lkm_poikaset_kuolleet").hasValue()) {
				kaynti.get("ika_poikaset").setValue(ika);
				return true;
			}
			kaynti.get("ika_munat").setValue(ika);
			return false;
		} catch (NumberFormatException e) {
			kaynti.get("pesinnan_vaihe").setValue(value);
		}
		return notUsed;
	}
	
	@Override
	public void postProsessingHook() throws Exception {
		data.setAction(Const.INSERT_FIRST);
	}
	
	@Override
	public _Validator validator() {
		try {
			List<_Table> results = dao.returnTable("ponttoalue");
			Map<Integer, _Table> ponttoalueet = new HashMap<>();
			for (_Table t : results) {
				ponttoalueet.put(t.getUniqueNumericIdColumn().getIntegerValue(), t);
			}
			return new PesakorttiYhdyskuntaTarkastusValidator(request, ponttoalueet, PesimisenBiologisetRajatUtil.getInstance(config));
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public void afterFailedValidation() {}
	
	@Override
	public void applicationSpecificDatamanipulation() throws Exception {
	}
	
	@Override
	public void afterSuccessfulValidation() throws Exception {
		if (!data.action().equals(Const.INSERT_FIRST)) return;
		convertCoordinates();
		_PesaDomainModelRow params = data.updateparameters();
		_Table pesa = params.getPesa();
		_Table tarkastus = params.getTarkastus();
		
		String firstId = null;
		String lastId = null;
		for (_Table pesaRivi : params.getKaynnit()) {
			String pesaid = dao.returnAndSetNextId(pesa);
			String tarkastusid = dao.returnAndSetNextId(tarkastus);
			tarkastus.get("pesa").setValue(pesaid);
			pesaRivi.get("tarkastus").setValue(tarkastusid);
			dao.returnAndSetNextId(pesaRivi);
			
			dao.executeInsert(pesa);
			dao.executeInsert(tarkastus);
			dao.executeInsert(pesaRivi);
			
			if (firstId == null) firstId = pesaid;
			lastId = pesaid;
		}
		markReceivedToKirjekyyhkyIfReceivingFormFromKirjekyyhky();
		data.setSuccessText("Yhdyskunnan pesät lisätty onnistuneesti. Pesien ID väliksi tuli: " + firstId + " - " + lastId);
		data.setAction("");
		data.clearResults();
		data.updateparameters().clearAllValues();
		clearPesinnanVaiheFields();
		FunctionalityExecutor.execute(request.functionalityFor(data.page()));
	}
	
	private void clearPesinnanVaiheFields() {
		Iterator<Map.Entry<String, String>> i = data.getAll().entrySet().iterator();
		while (i.hasNext()) {
			Map.Entry<String, String> e = i.next();
			if (e.getKey().startsWith("PesinnanVaihe")) {
				i.remove();
			}
		}
	}
	
}
