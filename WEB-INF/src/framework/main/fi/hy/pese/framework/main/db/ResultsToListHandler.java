package fi.hy.pese.framework.main.db;

import java.sql.SQLException;
import java.util.List;

import fi.hy.pese.framework.main.general.data._Data;
import fi.hy.pese.framework.main.general.data._Row;
import fi.hy.pese.framework.main.general.data._Table;

/**
 * Adds results from ResultSet to data lists -leaf, or adds the results to a given List<_Table>
 */
public class ResultsToListHandler<T extends _Row> extends BaseResultsHandler {

	private final _Data<T>				data;
	private final _DAO<T>				dao;
	private final String				listname;
	private final List<_Table>			resultList;
	private final String				tablename;
	private Integer 					limit;

	/**
	 * Adds results to data lists using the given table structure
	 * @param data
	 * @param rowFactory
	 * @param listname
	 */
	public ResultsToListHandler(_Data<T> data, _DAO<T> dao, String listname, String tablename) {
		this.data = data;
		this.dao = dao;
		this.listname = listname;
		this.resultList = null;
		this.tablename = tablename;
	}

	/**
	 * Adds results to the given list using the given table structure
	 * @param tableStructure
	 * @param resultList
	 */
	public ResultsToListHandler(_DAO<T> dao, List<_Table> resultList, String tablename) {
		this.dao = dao;
		this.data = null;
		this.listname = null;
		this.resultList = resultList;
		this.tablename = tablename;
	}

	@Override
	public void process(_ResultSet rs) throws SQLException {
		if (data != null) {
			resultsToDataLists(rs);
		} else {
			resultsToGivenList(rs);
		}
	}

	private void resultsToDataLists(_ResultSet rs) throws SQLException {
		while (rs.next() && limitNotMet()) {
			_Table resultTable = dao.newTableByName(tablename);
			setResultsToTable(rs, resultTable);
			data.addToList(listname, resultTable);
		}
	}

	private void resultsToGivenList(_ResultSet rs) throws SQLException {
		while (rs.next() && limitNotMet()) {
			_Table resultTable = dao.newTableByName(tablename);
			setResultsToTable(rs, resultTable);
			resultList.add(resultTable);
		}
	}
	
	private boolean limitNotMet() {
		if (limit == null) return true;
		int rowCount = data != null ? data.getLists().get(listname).size() : resultList.size();
		return rowCount < limit;
	}
	
	public ResultsToListHandler<T> setLimit(int limit) {
		this.limit = limit;
		return this;
	}

}
