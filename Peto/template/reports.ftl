
<#include "header.ftl">


<h1>${texts.report_title}</h1>
<br />
<@statustexts system texts />
<@errorlist errors texts />

<#if system.action == "print_report">
  <#assign folder = "report">
<#else>
  <#assign folder = "pdf">
</#if>

<#list files as file>
    <span class="file"> <a target="_new" href="${system.baseURL}?page=filemanagement&amp;action=show&amp;folder=${folder}&amp;filename=${file}">${file}</a> </span> <br />
</#list>

<br />
<br />

<form action="${system.baseURL}" method="post" name="search"  >
<input type="hidden" name="page"   value="${system.page}" />
<input type="hidden" name="action" value="print_report" />


<table>
  <tr>
    <td>${texts.search_quick_year}: </td>
    <td> <@inputText "searchparameters" searchparameters.tarkastus.vuosi false errors /> </td>
    <td> 
      Seuraava tarkastaja: <@inputText "searchparameters" searchparameters.pesa.seur_tarkastaja false errors />
    </td> 
    <td> 
      Lomake: <@selection selections "searchparameters" searchparameters.pesa.lomake 1 false true false errors />
    </td>
    <td colspan="2">
      <select name="NestCondition" size="3">
        <option value="0" <#if (data.NestCondition == "0") > selected="selected" </#if> >Pesimiskelpoiset</option>
        <option value="1" <#if (data.NestCondition == "1") > selected="selected" </#if> >Pesimiskelvottomat</option>
        <option value="2" <#if (data.NestCondition == "2") > selected="selected" </#if> >Kaikki</option>
      </select> 
    </td>
  </tr>
  <tr>
    <td>${texts.search_quick_pesa}: </td>
    <td>${texts.search_quick_id}:         <@inputText "searchparameters" searchparameters.pesa.id true errors  /> </td>
    <td> Kylä: <@inputText "searchparameters" searchparameters.pesa.kyla false errors  /> </td>
    <td colspan="2">${texts.search_quick_pesa_kunta}: <@selection selections "searchparameters" searchparameters.pesa.kunta 1 false true false errors /> </td>
  </tr>
  <tr>
    <td rowspan="2">${texts.search_quick_coordinates}: </td>
    <td>${texts.search_quick_yht}: </td>
	<td>${texts.search_quick_lat}: <@inputText "searchparameters" searchparameters.pesa.yht_leveys false errors /> </td>
	<td>${texts.search_quick_lon}: <@inputText "searchparameters" searchparameters.pesa.yht_pituus false errors /> </td>
	
	<td rowspan="2">${texts.search_quick_radius}:
	  <@coordinateSelection errors "quick" />
    </td>
  </tr>
  <tr> 
    <td>${texts.search_quick_eur}: </td>
	<td>${texts.search_quick_lat}: <@inputText "searchparameters" searchparameters.pesa.eur_leveys false errors /> </td>
	<td>${texts.search_quick_lon}: <@inputText "searchparameters" searchparameters.pesa.eur_pituus false errors /> </td>
  </tr>
</table>


<br /><br />


<table>
<tr>
<td style="vertical-align: top;">

<table>
  <caption>Raportin tulostus</caption>
  <tr>
    <td style="vertical-align: top;">
      <select name="report" size="4" multiple="multiple" <@checkerror errors "report" />>
        <option value="report_havainnoijalistaus"> Havainnoijalistaus </option>
        <option value="report_kansilehdet"> Kansilehdet </option>
      </select>
      <br />
      <br />
      <br />      
      <br />      
      <input type="submit" value="Luo raportti" />
    </td>
  </tr>
</table>

</td>
<td style="vertical-align: top;">

<table>
  <caption>Esitäytettyjen lomakkeiden tulostus</caption>
  <tr>
    <td> Vuodelle: </td>
    <td> <input type="text" size="4" name="yyyy" value="${ (data.yyyy)!(system.currentYear) }" /> </td>
  </tr>
  <tr>
    <td> Kokoelman tiedostonimi: </td>
    <td> <input type="text" size="20" name="collection_filename" value="" />.pdf  (kokoelmaa ei luoda, jos tiedostonimeä ei anneta) </td>
  </tr>
  <tr>
    <td> Lähetetäänkö Kirjekyyhkyyn: </td>
    <td>
      <select name="GenerateKirjekyyhkyFormDataXML" size="2">
        <option value="yes" <#if (data.GenerateKirjekyyhkyFormDataXML!"") == "yes"> selected="selected" </#if> > Kyllä </option>
        <option value="no"  <#if (data.GenerateKirjekyyhkyFormDataXML!"") != "yes"> selected="selected" </#if> > Ei </option>
      </select> 
    </td>
  </tr>
  <tr>
    <td colspan="2"> <br />  <input type="submit" name="print_pdf" value="Luo/lähetä esipainetut lomakkeet" /> </td>
  </tr>
</table>
<#--
<hr />

<table>
  <caption>Tarkastuslomakkeiden tulostus (kaikki tiedot)</caption>
  <tr>
    <td> Kokoelman tiedostonimi: </td>
    <td> <input type="text" size="20" name="collection_filename" value="" />.pdf  (kokoelmaa ei luoda, jos tiedostonimeä ei anneta) </td>
  </tr>
  <tr>
    <td colspan="2"> <br />  <input type="submit" name="print_pdf_full" value="Tee lomakkeet" /> </td>
  </tr>
</table>
-->
</td>
</tr>
</table>
  
</form>


<#----------------- clear button ------------------------------>
  <form action="${system.baseURL}" method="get" name="clear">
  <input type="hidden" name="page"  value="${system.page}" />
  </form>
<#----------------- ------------ ------------------------------>




<#include "footer.ftl">
