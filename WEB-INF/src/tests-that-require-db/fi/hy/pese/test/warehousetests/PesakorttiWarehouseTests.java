package fi.hy.pese.test.warehousetests;

import java.io.File;
import java.net.URL;

import fi.hy.pese.framework.main.db._DAO;
import fi.hy.pese.framework.main.general.WarehouseRowGenerator;
import fi.hy.pese.framework.main.general.WarehouseRowGenerator.WarehouseGenerationDefinition;
import fi.hy.pese.framework.main.general.data.PesaDomainModelDatabaseStructure;
import fi.hy.pese.framework.main.general.data.PesaDomainModelRowFactory;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.test.TestConfig;
import fi.hy.pese.framework.test.db.DAOStub;
import fi.hy.pese.pesakortti.main.app.PesakorttiDatabaseStructureCreator;
import fi.hy.pese.pesakortti.main.app.PesakorttiFactory;
import fi.hy.pese.pesakortti.main.app.PesakorttiWarehouseDefinition;
import fi.laji.datawarehouse.etl.models.dw.DwRoot;
import fi.luomus.commons.utils.FileUtils;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class PesakorttiWarehouseTests {
	
	public static Test suite() {
		return new TestSuite(PesakorttiWarehouseTests.class.getDeclaredClasses());
	}
	
	public static class WhenGeneratingWarehouseDocument extends TestCase {
		
		public class FakeDAO<T extends _PesaDomainModelRow> extends DAOStub<T> {
			public FakeDAO() {}
			
		}
		
		private PesakorttiFactory application;
		private _DAO<_PesaDomainModelRow> dao;
		private WarehouseRowGenerator generator;
		private _PesaDomainModelRow row;
		
		@Override
		protected void setUp() throws Exception {
			application = new PesakorttiFactory(TestConfig.getConfigFile("pese_pesakortti.config"));
			
			PesaDomainModelDatabaseStructure structure = PesakorttiDatabaseStructureCreator.hardCoded();
			application.setRowFactory(new PesaDomainModelRowFactory(structure, null, null));
			application.loadUITexts();
			dao = application.dao(PesakorttiWarehouseTests.class.getSimpleName());
			WarehouseGenerationDefinition definition = PesakorttiWarehouseDefinition.instance();
			generator = new WarehouseRowGenerator(definition, dao, application);
			row = dao.newRow();
		}
		
		@Override
		protected void tearDown() throws Exception {
			if (application != null) if (dao != null) application.release(dao);
		}
		
		public void test__delete() throws Exception {
			DwRoot root = generator.deletedEntry(123);
			assertRoot("pesakortti_1_delete.json", root);
		}
		
		public void test__common() throws Exception {
			commonTestData();
			DwRoot root = generator.generate(row);
			assertRoot("pesakortti_3_common.json", root);
		}
		
		private void commonTestData() {
			row.getPesa().getUniqueNumericIdColumn().setValue(1);
			row.getTarkastus().getUniqueNumericIdColumn().setValue(100);
			row.getTarkastus().get("vuosi").setValue(2023);
			
			row.getPesa().get("kunta").setValue("ALAJÄR");
			row.getPesa().get("tarkempi_paikka").setValue("Horkekauppa");
			row.getPesa().get("eur_leveys").setValue(6987321);
			row.getPesa().get("eur_pituus").setValue(349012);
			row.getPesa().get("koord_mittaustapa").setValue("G");
			row.getPesa().get("koord_tarkkuus").setValue("10");
			row.getPesa().get("rak_laji").setValue("PUFPUF");
			row.getPesa().get("pesa_sijainti").setValue("18");
			
			row.getTarkastus().get("tarkastaja").setValue(846);
			row.getTarkastus().get("pesiva_laji").setValue("STUVUL");
			row.getTarkastus().get("suojaus").setValue("E");

			row.getKaynnit().get(1).getUniqueNumericIdColumn().setValue(12345);
			row.getKaynnit().get(1).get("pvm").setValue("27.05.2023");
			row.getKaynnit().get(1).get("pvm_tarkkuus").setValue("1");
			row.getKaynnit().get(1).get("lkm_poikaset").setValue("4");
			row.getKaynnit().get(1).get("pesinnan_vaihe").setValue("P5");

			row.getTarkastus().get("pesimistulos").setValue("2");
			
			row.getTarkastus().get("pesa_kunto").setValue("0");
			
			row.getColumn("poikanen.1.rengas").setValue("A 1234");
			row.getColumn("poikanen.1.ika").setValue("PP");
			row.getColumn("poikanen.1.laji").setValue("STUVUL");
		}

		public void test__multi_kaynti() throws Exception {
			multiKayntiData();
			DwRoot root = generator.generate(row);
			assertRoot("pesakortti_4_multi_kaynti.json", root);
		}

		private void multiKayntiData() {
			commonTestData();
			
			row.getKaynnit().get(1).clearAllValues();
			row.getKaynnit().get(1).getUniqueNumericIdColumn().setValue(123451);
			row.getKaynnit().get(1).get("pvm").setValue("12.5.2023");
			row.getKaynnit().get(1).get("pvm_tarkkuus").setValue("1");
			row.getKaynnit().get(1).get("lkm_munat").setValue("5");
			row.getKaynnit().get(1).get("pesinnan_vaihe").setValue("M1");
			
			row.getKaynnit().get(2).getUniqueNumericIdColumn().setValue(123452);
			row.getKaynnit().get(2).get("pvm").setValue("21.5.2023");
			row.getKaynnit().get(2).get("pvm_tarkkuus").setValue("1");
			row.getKaynnit().get(2).get("lkm_munat").setValue("6");
			row.getKaynnit().get(2).get("lkm_munat_kuolleet").setValue("1");
			row.getKaynnit().get(2).get("pesinnan_vaihe").setValue("M5");

			row.getKaynnit().get(3).getUniqueNumericIdColumn().setValue(123453);
			row.getKaynnit().get(3).get("pvm").setValue("4.6.2023");
			row.getKaynnit().get(3).get("pvm_tarkkuus").setValue("1");
			row.getKaynnit().get(3).get("lkm_poikaset").setValue("6");
			row.getKaynnit().get(3).get("ika_poikaset").setValue("7");
			
			row.getKaynnit().get(4).getUniqueNumericIdColumn().setValue(123454);
			row.getKaynnit().get(4).get("pvm").setValue("9.6.2023");
			row.getKaynnit().get(4).get("pvm_tarkkuus").setValue("1");
			row.getKaynnit().get(4).get("lkm_poikaset").setValue("6");
			row.getKaynnit().get(4).get("pesinnan_vaihe").setValue("P5");
			
			row.getKaynnit().get(5).getUniqueNumericIdColumn().setValue(123455);
			row.getKaynnit().get(5).get("pvm").setValue("20.6.2023");
			row.getKaynnit().get(5).get("pvm_tarkkuus").setValue("1");
			row.getKaynnit().get(5).get("pesinnan_vaihe").setValue("J2");
			
			row.getTarkastus().get("pesimistulos").setValue("4");
		}

		public void test__forbidden_use() throws Exception {
			commonTestData();
			row.getColumn("pesa.suojelu_kaytto").setValue("E");
			DwRoot root = generator.generate(row);
			assertRoot("pesakortti_5_forbidden_use.json", root);
		}
		
		public void test__forbidden_use_2() throws Exception {
			commonTestData();
			row.getColumn("tarkastus.suojaus").setValue("K");
			DwRoot root = generator.generate(row);
			assertRoot("pesakortti_5_2_forbidden_use.json", root); // this test starts to fail year 2028 .. increase year in test data then or something; hopefully this system is not around at that time... EP
		}
		
		public void test__muulaji() throws Exception {
			commonTestData();
			row.getTarkastus().get("yhdyskunta_laji").setValue("RIPRIP");
			DwRoot root = generator.generate(row);
			assertRoot("pesakortti_6_other_species.json", root);
		}
		
		public void test__date_accuracy() throws Exception {
			multiKayntiData();
			row.getKaynnit().get(1).get("pvm_tarkkuus").setValue("4");
			row.getKaynnit().get(2).get("pvm_tarkkuus").setValue("3");
			DwRoot root = generator.generate(row);

			assertEquals("2023-06-04 - 2023-06-20", root.getPublicDocument().getGatherings().get(0).getDisplayDateTime());
			assertEquals("2023-01-01 - 2023-12-31", root.getPublicDocument().getGatherings().get(1).getDisplayDateTime());
			assertEquals("2023-06-04", root.getPublicDocument().getGatherings().get(2).getDisplayDateTime());
			assertEquals("2023-06-09", root.getPublicDocument().getGatherings().get(3).getDisplayDateTime());
			assertEquals("2023-06-20", root.getPublicDocument().getGatherings().get(4).getDisplayDateTime());
		}

		public void test__coordinate_uncertainty() throws Exception {
			commonTestData();
			
			row.getPesa().get("koord_mittaustapa").setValue("K");
			row.getPesa().get("koord_tarkkuus").setValue("12345");
			DwRoot root = generator.generate(row);
			assertEquals("6987321.0 : 6987322.0 : 349012.0 : 349013.0 : EUREF : 12345", root.getPublicDocument().getGatherings().get(0).getCoordinates().toString());
		}

		public void test__no_counts() throws Exception {
			commonTestData();
			row.getKaynnit().get(1).get("lkm_poikaset").setValue("");
			row.getKaynnit().get(1).get("pesinnan_vaihe").setValue("M9"); // Emo hautoi, ei ajettu pesästä
			DwRoot root = generator.generate(row);
			assertRoot("pesakortti_8_units_from_pesinnan_vaihe.json", root);
		}

		private void assertRoot(String filename, DwRoot root) {
			URL url = this.getClass().getResource(filename);
			File file = new File(url.getFile());
			try {
				String data = FileUtils.readContents(file);
				assertEquals(data, root.toJSON().beautify());
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
		
	}
	
}
