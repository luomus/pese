package fi.hy.pese.pesakortti.test;


import java.util.HashMap;
import java.util.Map;

import fi.hy.pese.framework.main.general.PesimisenBiologisetRajatUtil;
import fi.hy.pese.framework.main.general.PesimisenBiologisetRajatUtil.Rajat;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data.PesaDomainModelRow;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.test.TestConfig;
import fi.hy.pese.pesakortti.main.app.PesakorttiConst;
import fi.hy.pese.pesakortti.main.app.PesakorttiDatabaseStructureCreator;
import fi.hy.pese.pesakortti.main.app.PesakorttiFactory;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class PesakorttiConstTests {

	public static Test suite() {
		return new TestSuite(PesakorttiConstTests.class.getDeclaredClasses());
	}

	public static class Testssss extends TestCase {

		public void test__maksimipesimiskoko() throws Exception {
			PesakorttiFactory factory = new PesakorttiFactory(TestConfig.getConfigFile("pese_pesakortti.config"));
			Map<String, Rajat> maxPesyekoot = PesimisenBiologisetRajatUtil.getInstance(factory.config());
			assertEquals(6, maxPesyekoot.get("HIPICT").maxPesyekoko);
			assertEquals(1.0, maxPesyekoot.get("HIPICT").maxMunintaVali);
		}

		public void test__kirjekyyhkyConversion() {
			Map<String, String> datamap = new HashMap<>();
			datamap.put(Const.UPDATEPARAMETERS + ".pesa.leveys", "6666666");

			_PesaDomainModelRow kirjekyyhkyparameters = new PesaDomainModelRow(PesakorttiDatabaseStructureCreator.hardCoded());
			kirjekyyhkyparameters.getPesa().get("koord_ilm_tyyppi").setValue("Y");

			PesakorttiConst.kirjekyyhkyModelToData(datamap, kirjekyyhkyparameters);
			assertEquals("6666666", kirjekyyhkyparameters.getColumn("pesa.yht_leveys").getValue());
		}

		public void test__kirjekyyhkyConversion_2() {
			Map<String, String> datamap = new HashMap<>();
			datamap.put("kaynti.1.ika", "5");

			_PesaDomainModelRow kirjekyyhkyparameters = new PesaDomainModelRow(PesakorttiDatabaseStructureCreator.hardCoded());

			PesakorttiConst.kirjekyyhkyModelToData(datamap, kirjekyyhkyparameters);

			assertEquals("", kirjekyyhkyparameters.getColumn("kaynti.1.ika_munat").getValue());
			assertEquals("", kirjekyyhkyparameters.getColumn("kaynti.1.ika_poikaset").getValue());
		}

		public void test__kirjekyyhkyConversion_3() {
			Map<String, String> datamap = new HashMap<>();
			datamap.put(Const.UPDATEPARAMETERS+".kaynti.1.ika", "5");

			_PesaDomainModelRow kirjekyyhkyparameters = new PesaDomainModelRow(PesakorttiDatabaseStructureCreator.hardCoded());
			kirjekyyhkyparameters.getColumn("kaynti.1.lkm_munat").setValue("2");

			PesakorttiConst.kirjekyyhkyModelToData(datamap, kirjekyyhkyparameters);

			assertEquals("", kirjekyyhkyparameters.getColumn("kaynti.1.ika_poikaset").getValue());
			assertEquals("5", kirjekyyhkyparameters.getColumn("kaynti.1.ika_munat").getValue());
		}

		public void test__kirjekyyhkyConversion_4() {
			Map<String, String> datamap = new HashMap<>();
			datamap.put(Const.UPDATEPARAMETERS+".kaynti.1.ika", "5");

			_PesaDomainModelRow kirjekyyhkyparameters = new PesaDomainModelRow(PesakorttiDatabaseStructureCreator.hardCoded());
			kirjekyyhkyparameters.getColumn("kaynti.1.lkm_munat").setValue("2");
			kirjekyyhkyparameters.getColumn("kaynti.1.lkm_poikaset").setValue("2");

			PesakorttiConst.kirjekyyhkyModelToData(datamap, kirjekyyhkyparameters);

			assertEquals("5", kirjekyyhkyparameters.getColumn("kaynti.1.ika_poikaset").getValue());
			assertEquals("", kirjekyyhkyparameters.getColumn("kaynti.1.ika_munat").getValue());
		}

	}

}
