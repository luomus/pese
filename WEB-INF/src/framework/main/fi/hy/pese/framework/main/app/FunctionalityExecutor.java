package fi.hy.pese.framework.main.app;

import fi.hy.pese.framework.main.app.functionality.validate._Validator;
import fi.hy.pese.framework.main.general.data._Row;

/**
 * Executes a Functionality.
 * The execution order is as follows: <br>
 * - prepPosessingHook() <br>
 * - validate() <br>
 * --- afterSuccessfulValidation() <br>
 * --- afterFailedValidation() <br>
 * - postProcessingHook() <br>
 * <br>
 * <br>
 * The request is given to the functionality it is executing as a paramter, so that the
 * functionality can access the ressources, and so that the functionality can use the
 * request to execute other functionality.
 */
public class FunctionalityExecutor {
	
	/**
	 * Executes a functionality using the given request
	 * @param functionality
	 * @param request
	 * @throws Exception if the functionality throws an exception
	 */
	public static void execute(_Functionality<? extends _Row> functionality) throws Exception {
		functionality.preProsessingHook();
		
		if (hasBeenRedirected(functionality)) return;
		
		_Validator validator = functionality.validator();
		validator.validate();
		
		if (validator.hasErrors() || validator.hasWarnings()) {
			functionality.functionalityRequest().data().setErrors(validator.errors());
			functionality.functionalityRequest().data().setWarnings(validator.warnings());
			functionality.afterFailedValidation();
		} else {
			functionality.afterSuccessfulValidation();
		}
		
		if (hasBeenRedirected(functionality)) return;
		
		functionality.postProsessingHook();
	}
	
	private static boolean hasBeenRedirected(_Functionality<? extends _Row> functionality) {
		return functionality.functionalityRequest().redirecter().isHttpStatusCodeSet();
	}
	
}
