package fi.hy.pese.framework.main.general.data;


public class PesaDomainModelDatabaseStructure extends DatabaseStructure {

	private DatabaseTableStructure						pesa;
	private DatabaseTableStructure						olosuhde;
	private DatabaseTableStructure						tarkastus;
	private DatabaseTableStructure						kaynti;
	private DatabaseTableStructure						vuosi;
	private DatabaseTableStructure						reviiri;
	private DatabaseTableStructure						poikanen;
	private DatabaseTableStructure						aikuinen;
	private DatabaseTableStructure						muna;

	public PesaDomainModelDatabaseStructure() {
		super();
		pesa = new DatabaseTableStructure(_Table.UNINITIALIZED_TABLE);
		olosuhde = new DatabaseTableStructure(_Table.UNINITIALIZED_TABLE);
		tarkastus = new DatabaseTableStructure(_Table.UNINITIALIZED_TABLE);
		kaynti = new DatabaseTableStructure(_Table.UNINITIALIZED_TABLE);
		vuosi = new DatabaseTableStructure(_Table.UNINITIALIZED_TABLE);
		reviiri = new DatabaseTableStructure(_Table.UNINITIALIZED_TABLE);
		poikanen = new DatabaseTableStructure(_Table.UNINITIALIZED_TABLE);
		aikuinen = new DatabaseTableStructure(_Table.UNINITIALIZED_TABLE);
		muna = new DatabaseTableStructure(_Table.UNINITIALIZED_TABLE);
	}

	public void setPesa(DatabaseTableStructure t) {
		pesa = t;
		toTableStructuremap(t);
	}

	public DatabaseTableStructure getPesa() {
		return pesa;
	}

	public void setOlosuhde(DatabaseTableStructure t) {
		olosuhde = t;
		toTableStructuremap(t);
	}

	public DatabaseTableStructure getOlosuhde() {
		return olosuhde;
	}

	public void setTarkastus(DatabaseTableStructure t) {
		tarkastus = t;
		toTableStructuremap(t);
	}

	public DatabaseTableStructure getTarkastus() {
		return tarkastus;
	}

	public void setKaynti(DatabaseTableStructure t) {
		kaynti = t;
		toTableStructuremap(t);
	}

	public DatabaseTableStructure getKaynti() {
		return kaynti;
	}

	public void setVuosi(DatabaseTableStructure t) {
		vuosi = t;
		toTableStructuremap(t);
	}

	public DatabaseTableStructure getVuosi() {
		return vuosi;
	}

	public void setReviiri(DatabaseTableStructure t) {
		reviiri = t;
		toTableStructuremap(t);
	}

	public DatabaseTableStructure getReviiri() {
		return reviiri;
	}

	public void setPoikanen(DatabaseTableStructure t) {
		poikanen = t;
		toTableStructuremap(t);
	}

	public DatabaseTableStructure getPoikanen() {
		return poikanen;
	}

	public void setAikuinen(DatabaseTableStructure t) {
		aikuinen = t;
		toTableStructuremap(t);
	}

	public DatabaseTableStructure getAikuinen() {
		return aikuinen;
	}

	public void setMuna(DatabaseTableStructure t) {
		muna = t;
		toTableStructuremap(t);
	}

	public DatabaseTableStructure getMuna() {
		return muna;
	}

	private int kaynnitCount = 0;
	private int	poikasetCount = 0;
	private int	aikuisetCount = 0;
	private int	munatCount = 0;

	public int getKaynnitCount() {
		return kaynnitCount;
	}

	public int getPoikasetCount() {
		return poikasetCount;
	}

	public int getAikuisetCount() {
		return aikuisetCount;
	}

	public int getMunatCount() {
		return munatCount;
	}

	public void setKaynnitCount(int i) {
		kaynnitCount = i;
	}

	public void setPoikasetCount(int i) {
		poikasetCount = i;
	}

	public void setAikuisetCount(int i) {
		aikuisetCount = i;
	}

	public void setMunatCount(int i) {
		munatCount = i;
	}

}
