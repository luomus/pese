package fi.hy.pese.framework.test;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import fi.hy.pese.framework.main.app._ApplicationSpecificationFactory;
import fi.hy.pese.framework.main.app._Functionality;
import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.db._DAO;
import fi.hy.pese.framework.main.general._WarehouseRowGenerator;
import fi.hy.pese.framework.main.general.data.HistoryAction;
import fi.hy.pese.framework.main.general.data.ParameterMap;
import fi.hy.pese.framework.main.general.data._Data;
import fi.hy.pese.framework.main.general.data._Row;
import fi.hy.pese.framework.main.kirjekyyhky._KirjekyyhkyXMLGenerator;
import fi.hy.pese.framework.main.view._ExceptionViewer;
import fi.hy.pese.framework.main.view._Viewer;
import fi.luomus.commons.config.Config;
import fi.luomus.commons.containers.Selection;
import fi.luomus.commons.kirjekyyhky.KirjekyyhkyAPIClient;
import fi.luomus.commons.languagesupport.LocalizedTextsContainer;
import fi.luomus.commons.pdf.PDFWriter;
import fi.luomus.commons.reporting.ErrorReporter;
import fi.luomus.commons.tipuapi.TipuAPIClient;
import fi.luomus.commons.tipuapi.TipuApiResource;

public class FactoryStub<T extends _Row> implements _ApplicationSpecificationFactory<T> {

	@Override
	public _Data<T> initData(String language, ParameterMap params) throws Exception {
		return null;
	}

	@Override
	public Config config() {
		//  Auto-generated method stub
		return new ConfigStub();
	}

	@Override
	public _DAO<T> dao(String userId) throws SQLException {
		//  Auto-generated method stub
		return null;
	}

	@Override
	public void release(_DAO<T> dao) {
		//  Auto-generated method stub

	}

	@Override
	public LocalizedTextsContainer uiTexts() {
		//  Auto-generated method stub
		return null;
	}

	@Override
	public _Functionality<T> functionalityFor(String page, _Request<T> request) throws UnsupportedOperationException {
		//  Auto-generated method stub
		return null;
	}

	@Override
	public _ExceptionViewer exceptionViewer(_Request<T> request) {
		//  Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, Selection> selections(_DAO<T> dao) throws Exception {
		//  Auto-generated method stub
		return null;
	}

	@Override
	public TipuApiResource getTipuApiResource(String resourcename) {
		//  Auto-generated method stub
		return null;
	}

	@Override
	public _Viewer viewer() throws IOException {
		//  Auto-generated method stub
		return null;
	}

	@Override
	public void reloadUITexts() throws Exception {
		//  Auto-generated method stub

	}

	@Override
	public String frontpage() {
		//  Auto-generated method stub
		return null;
	}

	@Override
	public PDFWriter pdfWriter() {
		//  Auto-generated method stub
		return null;
	}

	@Override
	public PDFWriter pdfWriter(String outputFolder) {
		//  Auto-generated method stub
		return null;
	}

	@Override
	public _KirjekyyhkyXMLGenerator kirjekyyhkyFormDataXMLGenerator(_Request<T> request) throws Exception {
		//  Auto-generated method stub
		return null;
	}

	@Override
	public KirjekyyhkyAPIClient kirjekyyhkyAPI() throws Exception {
		//  Auto-generated method stub
		return null;
	}

	@Override
	public TipuAPIClient tipuAPI() throws Exception {
		//  Auto-generated method stub
		return null;
	}

	@Override
	public boolean usingKirjekyyhky() {
		//  Auto-generated method stub
		return false;
	}

	@Override
	public boolean usingTipuApi() {
		//  Auto-generated method stub
		return false;
	}

	@Override
	public void loadKirjekyyhkyCount(_Request<T> request) throws Exception {
		//  Auto-generated method stub

	}

	@Override
	public void addHistoryAction(HistoryAction action) {
		//  Auto-generated method stub

	}

	@Override
	public List<HistoryAction> getHistoryActions() {
		//  Auto-generated method stub
		return null;
	}

	@Override
	public void saveHistoryActions() {
		//  Auto-generated method stub

	}

	@Override
	public void loadHistoryActions() {
		//  Auto-generated method stub

	}

	@Override
	public void postProsessingHook(_Request<T> request) {
		// Auto-generated method stub

	}

	@Override
	public void reportException(String originalRequestURI, Throwable condition) {

	}

	@Override
	public ErrorReporter getErrorReporter() {
		return null;
	}

	@Override
	public boolean isWarehouseSource() {
		// Auto-generated method stub
		return false;
	}

	@Override
	public _WarehouseRowGenerator getWarehouseRowGenerator(_DAO<T> dao) {
		// Auto-generated method stub
		return null;
	}

}
