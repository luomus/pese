package fi.hy.pese.framework.main.general.data;

public class UsedSlots {

	private int usedKaynnit = Integer.MAX_VALUE;
	private int usedPoikaset = Integer.MAX_VALUE;
	private int usedAikuiset = Integer.MAX_VALUE;
	private int usedMunat = Integer.MAX_VALUE;

	public UsedSlots setUsedKaynnit(int count) {
		usedKaynnit = count;
		return this;
	}

	public UsedSlots setUsedPoikaset(int count) {
		usedPoikaset = count;
		return this;
	}

	public UsedSlots setUsedAikuiset(int count) {
		usedAikuiset = count;
		return this;
	}

	public UsedSlots setUsedMunat(int count) {
		usedMunat = count;
		return this;
	}

	public int getUsedKaynnit() {
		return usedKaynnit;
	}

	public int getUsedPoikaset() {
		return usedPoikaset;
	}

	public int getUsedAikuiset() {
		return usedAikuiset;
	}

	public int getUsedMunat() {
		return usedMunat;
	}

}
