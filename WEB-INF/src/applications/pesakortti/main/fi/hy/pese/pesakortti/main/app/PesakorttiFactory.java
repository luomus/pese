package fi.hy.pese.pesakortti.main.app;

import java.io.FileNotFoundException;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import fi.hy.pese.framework.main.app.BaseFactoryForApplicationSpecificationFactory;
import fi.hy.pese.framework.main.app._Functionality;
import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.app.functionality.CoordinateRadiusSearcherFunctionality;
import fi.hy.pese.framework.main.app.functionality.FilemanagementFunctionality;
import fi.hy.pese.framework.main.app.functionality.FilemanagementFunctionality.FileListGeneratorImple;
import fi.hy.pese.framework.main.app.functionality.LintuvaaraLoginFunctionality;
import fi.hy.pese.framework.main.app.functionality.LoginFunctionalityWithSingleUser;
import fi.hy.pese.framework.main.app.functionality.LogoutFunctionality;
import fi.hy.pese.framework.main.app.functionality.PesintamanagementFunctionality;
import fi.hy.pese.framework.main.app.functionality.TablemanagementFunctionality;
import fi.hy.pese.framework.main.db._DAO;
import fi.hy.pese.framework.main.general.WarehouseRowGenerator;
import fi.hy.pese.framework.main.general._WarehouseRowGenerator;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.consts.Kirjekyyhky;
import fi.hy.pese.framework.main.general.data.Data;
import fi.hy.pese.framework.main.general.data.ParameterMap;
import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._Data;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.pesakortti.main.functionality.PesakorttiKirjekyyhkyManagementFunctionality;
import fi.hy.pese.pesakortti.main.functionality.PesakorttiKirjekyyhkyXMLGenerator;
import fi.hy.pese.pesakortti.main.functionality.PesakorttiSearchFunctionality;
import fi.hy.pese.pesakortti.main.functionality.PesakorttiTarkastusFunctionality;
import fi.hy.pese.pesakortti.main.functionality.PesakorttiYhdyskuntaTarkastusFunctionality;
import fi.hy.pese.pesakortti.main.functionality.PesakorttiYhdyskuntalomakeKirjekyyhkyXMLGenerator;
import fi.luomus.commons.config.Config;
import fi.luomus.commons.containers.Selection;
import fi.luomus.commons.containers.SelectionImple;
import fi.luomus.commons.containers.SelectionOptionImple;
import fi.luomus.commons.kirjekyyhky.KirjekyyhkyAPIClient;
import fi.luomus.commons.kirjekyyhky.KirjekyyhkyAPIClientImple;
import fi.luomus.commons.languagesupport.LanguageFileReader;
import fi.luomus.commons.languagesupport.LocalizedTextsContainer;
import fi.luomus.commons.languagesupport.LocalizedTextsContainerImple;
import fi.luomus.commons.tipuapi.TipuAPIClient;
import fi.luomus.commons.utils.AESDecryptor;
import fi.luomus.commons.utils.SingleObjectCache;
import fi.luomus.commons.xml.Document;
import fi.luomus.commons.xml.Document.Node;

public class PesakorttiFactory extends BaseFactoryForApplicationSpecificationFactory<_PesaDomainModelRow> {
	
	private static final String[]	APUTAULU_ORDER_BY	= new String[] {"jarjestys", "selite"};
	private final SingleObjectCache<String> kirjekyyhkyCountCache;
	
	public PesakorttiFactory(String configFileName) throws FileNotFoundException {
		super(configFileName);
		
		kirjekyyhkyCountCache = new SingleObjectCache<>(new SingleObjectCache.CacheLoader<String>() {
			@Override
			public String load() {
				KirjekyyhkyAPIClient normalFormAPI = null;
				KirjekyyhkyAPIClient yhdyskuntaFormAPI = null;
				try {
					normalFormAPI = kirjekyyhkyAPI();
					yhdyskuntaFormAPI = kirjekyyhkyYhdyskuntaAPI();
					int count = normalFormAPI.getCount() + yhdyskuntaFormAPI.getCount();
					return Integer.toString(count);
				} catch (Exception e) {
					throw new RuntimeException(e);
				} finally {
					if (normalFormAPI != null) normalFormAPI.close();
					if (yhdyskuntaFormAPI != null) yhdyskuntaFormAPI.close();
				}
			}
		}, SECONDS_BETWEEN_KK_COUNT_LOADS, TimeUnit.SECONDS);
	}
	
	private KirjekyyhkyAPIClient kirjekyyhkyYhdyskuntaAPI() throws Exception {
		return new KirjekyyhkyAPIClientImple(
				config().get(Config.KIRJEKYYHKY_API_URI),
				config().get(Config.KIRJEKYYHKY_API_USERNAME),
				config().get(Config.KIRJEKYYHKY_API_PASSWORD),
				"pesakortti-yhdyskunta");
	}
	
	@Override
	protected String getLintuvaaraURL() {
		return config().get(Config.LINTUVAARA_URL);
	}
	
	LocalizedTextsContainerImple normalTexts = null;
	LocalizedTextsContainerImple yhdyskuntaTexts = null;
	
	@Override
	public _Data<_PesaDomainModelRow> initData(String language, ParameterMap params) throws Exception {
		Data<_PesaDomainModelRow> data = new Data<>(rowFactory);
		data.set(Const.LINTUVAARA_URL, getLintuvaaraURL());
		params.convertTo(data);
		data.setBaseURL(config().baseURL());
		data.setCommonURL(config().commonURL());
		reloadUITexts();
		if (data.get("mode").equals("yhdyskunta")) {
			data.setUITexts(yhdyskuntaTexts.getAllTexts(language));
		} else {
			data.setUITexts(normalTexts.getAllTexts(language));
		}
		data.setDevelopment(config().developmentMode());
		data.setStaging(config().stagingMode());
		return data;
	}
	
	@Override
	public void loadUITexts() throws Exception {
		String folder = config().baseFolder() + config().get(Config.LANGUAGE_FILE_FOLDER);
		String prefix = config().get(Config.LANGUAGE_FILE_PREFIX);
		String[] languages = config().get(Config.SUPPORTED_LANGUAGES).split(",");
		LanguageFileReader r = new LanguageFileReader(folder, prefix, languages);
		LocalizedTextsContainer temp = r.readUITexts();
		
		_PesaDomainModelRow row = this.rowFactory.newRow();
		
		this.yhdyskuntaTexts = new LocalizedTextsContainerImple(languages);
		this.normalTexts = new LocalizedTextsContainerImple(languages);
		
		for (String language : languages) {
			Map<String, String> texts = temp.getAllTexts(language);
			handleYhdyskuntaTexts(row, texts);
			for (Map.Entry<String, String> e : texts.entrySet()) {
				this.yhdyskuntaTexts.setText(e.getKey(), e.getValue(), language);
			}
			
			texts = temp.getAllTexts(language);
			handleNormalTexts(row, texts);
			for (Map.Entry<String, String> e : texts.entrySet()) {
				this.normalTexts.setText(e.getKey(), e.getValue(), language);
			}
		}
		
	}
	
	public static void handleNormalTexts(_PesaDomainModelRow row, Map<String, String> texts) {
		addTextsForIndexedTablegroup(row.getAikuiset(), texts);
		addTextsForIndexedTablegroup(row.getPoikaset(), texts);
		addTextsForIndexedTablegroup(row.getMunat(), texts);
		addTextsForIndexedTablegroup(row.getKaynnit(), texts);
	}
	
	private void handleYhdyskuntaTexts(_PesaDomainModelRow row, Map<String, String> texts) {
		addTextsForIndexedTablegroup(row.getAikuiset(), texts);
		addTextsForIndexedTablegroup(row.getPoikaset(), texts);
		addTextsForIndexedTablegroup(row.getMunat(), texts);
		for (_Column c : row.getKaynti()) {
			String text = texts.get("iter.yhdyskunta."+c.getFullname());
			if (text == null) continue;
			for (int i = 1; i <= 250; i++) {
				String iText = text.replace("<X>", Integer.toString(i));
				texts.put("kaynti."+i+"."+c.getName(), iText);
			}
		}
	}
	
	@Override
	public LocalizedTextsContainer uiTexts() {
		return normalTexts;
	}
	
	@Override
	public String frontpage() {
		return Const.SEARCH_PAGE; // Warning: frontpage must be defined bellow (functionalityFor()) or an infinite loop will happen
	}
	
	private AESDecryptor	decryptor	= null;
	
	private AESDecryptor getAESDecryptor() {
		if (decryptor == null) {
			try {
				decryptor = new AESDecryptor(config().get(Config.LINTUVAARA_PUBLIC_RSA_KEY));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return decryptor;
	}
	
	@Override
	public _Functionality<_PesaDomainModelRow> functionalityFor(String page, _Request<_PesaDomainModelRow> request) {
		if (page.equals(Const.LOGIN_PAGE)) {
			if (config().developmentMode()) {
				return new LoginFunctionalityWithSingleUser<>(request, "z", "z");
			}
			return new LintuvaaraLoginFunctionality<>(request, getAESDecryptor(), getErrorReporter());
		} else if (page.equals(Const.TABLEMANAGEMENT_PAGE)) {
			return new TablemanagementFunctionality<>(request, "havainnoijalomake", "erikoistutkimus");
		}
		else if (page.equals(Const.SEARCH_PAGE)) {
			return new PesakorttiSearchFunctionality(request);
		}
		else if (page.equals(Const.TARKASTUS_PAGE)) {
			if (request.data().get("mode").equals(PesakorttiConst.YHDYSKUNTA_PAGE)) {
				return new PesakorttiYhdyskuntaTarkastusFunctionality(request);
			}
			return new PesakorttiTarkastusFunctionality(request);
		} else if (page.equals(Const.FILEMANAGEMENT_PAGE)) {
			return new FilemanagementFunctionality<>(request, new FileListGeneratorImple(request.config().pdfFolder(), request.config().reportFolder()));
		} else if (page.equals(Const.REPORTS_PAGE))
			return new PesakorttiSearchFunctionality(request);
		else if (page.equals(Const.LOGOUT_PAGE)) {
			return new LogoutFunctionality<>(request);
		} else if (page.equals(Const.PESINTAMANAGEMENT_PAGE)) {
			return new PesintamanagementFunctionality(request);
		} else if (page.equals(Const.COORDINATE_RADIUS_SEARCH_PAGE)) {
			return new CoordinateRadiusSearcherFunctionality(request, new PesakorttiSearchFunctionality(request));
		} else if (page.equals(Const.KIRJEKYYHKY_MANAGEMENT_PAGE)) {
			return new PesakorttiKirjekyyhkyManagementFunctionality(request);
		} else if (page.equals("luo")) {
			try {
				PesakorttiKirjekyyhkyXMLGenerator generator = new PesakorttiKirjekyyhkyXMLGenerator(request);
				generator.writeStructureXML();
			} catch (Exception e) { e.printStackTrace(); }
			request.data().setPage(Const.FILEMANAGEMENT_PAGE);
			return functionalityFor(Const.FILEMANAGEMENT_PAGE, request);
		}
		else if (page.equals("luo-yhdyskunta")) {
			try {
				PesakorttiYhdyskuntalomakeKirjekyyhkyXMLGenerator generator = new PesakorttiYhdyskuntalomakeKirjekyyhkyXMLGenerator(request);
				generator.writeStructureXML();
			} catch (Exception e) { e.printStackTrace(); }
			request.data().setPage(Const.FILEMANAGEMENT_PAGE);
			return functionalityFor(Const.FILEMANAGEMENT_PAGE, request);
		}
		else {
			request.data().setPage(frontpage());
			return functionalityFor(frontpage(), request);
		}
	}
	
	@Override
	public Map<String, Selection> fetchSelections(_DAO<_PesaDomainModelRow> dao) throws Exception {
		Map<String, Selection> selections = dao.returnCodeSelections("aputaulu", "taulu", "kentta", "arvo", "selite", "ryhmittely", APUTAULU_ORDER_BY);
		selections.put(PesakorttiConst.TAULUT, dao.returnSelections("aputaulu", "taulu", "taulu", new String[] {"taulu"}));
		
		TipuAPIClient tipuAPI = null;
		try {
			tipuAPI = this.tipuAPI();
			selections.put(TipuAPIClient.RINGERS, tipuAPI.getAsSelection(TipuAPIClient.RINGERS, 2));
			selections.put(TipuAPIClient.MUNICIPALITIES, tipuAPI.getAsSelection(TipuAPIClient.MUNICIPALITIES, 1));
			selections.put(TipuAPIClient.ELY_CENTRES, tipuAPI.getAsSelection(TipuAPIClient.ELY_CENTRES, 1));
			selections.put(TipuAPIClient.SPECIES, tipuAPI.getAsSelection(TipuAPIClient.SPECIES, 1));
			
			Document ageCodesAsDocument = tipuAPI.getAsDocument(TipuAPIClient.CODES, "11");
			SelectionImple poikastenKoodit = new SelectionImple(PesakorttiConst.IKAKOODIT_POIKANEN);
			SelectionImple aikuistenKoodit = new SelectionImple(PesakorttiConst.IKAKOODIT_AIKUINEN);
			for (Node ressource : ageCodesAsDocument.getRootNode().getChildNodes()) {
				String code = ressource.getNode("code").getContents();
				String desc = ressource.getNode("desc").getContents();
				String metadata = ressource.getNode("metadata").getContents();
				if (metadata.contains("AIKUINEN")) {
					aikuistenKoodit.addOption(new SelectionOptionImple(code, desc));
				} else if (metadata.contains("POIKANEN")) {
					poikastenKoodit.addOption(new SelectionOptionImple(code, desc));
				}
			}
			selections.put(PesakorttiConst.IKAKOODIT_AIKUINEN, aikuistenKoodit);
			selections.put(PesakorttiConst.IKAKOODIT_POIKANEN, poikastenKoodit);
			
		} finally {
			if (tipuAPI != null) tipuAPI.close();
		}
		
		SelectionImple k_e = new SelectionImple("k_e");
		k_e.addOption(new SelectionOptionImple("K", "Kyllä"));
		k_e.addOption(new SelectionOptionImple("E", "Ei"));
		selections.put(PesakorttiConst.K_E, k_e);
		
		return selections;
	}
	
	@Override
	public void loadKirjekyyhkyCount(_Request<_PesaDomainModelRow> request) {
		try {
			if (request.data().page().equals(Const.TARKASTUS_PAGE) && request.data().successText().length() > 1) {
				request.data().set(Kirjekyyhky.KIRJEKYYHKY_COUNT, kirjekyyhkyCountCache.getForceReload());
			} else if (request.data().page().equals(Const.KIRJEKYYHKY_MANAGEMENT_PAGE) && request.data().action().equals(Kirjekyyhky.RETURN_FOR_CORRECTIONS)) {
				request.data().set(Kirjekyyhky.KIRJEKYYHKY_COUNT, kirjekyyhkyCountCache.getForceReload());
			}
			else{
				request.data().set(Kirjekyyhky.KIRJEKYYHKY_COUNT, kirjekyyhkyCountCache.get());
			}
		} catch (Exception e) {
			request.data().set(Kirjekyyhky.KIRJEKYYHKY_COUNT, "ERR");
		}
	}
	
	@Override
	public _WarehouseRowGenerator getWarehouseRowGenerator(_DAO<_PesaDomainModelRow> dao) {
		return new WarehouseRowGenerator(PesakorttiWarehouseDefinition.instance(), dao, this);
	}

}
