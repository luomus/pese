package fi.hy.pese.framework.main.general.data;

import java.util.List;
import java.util.Map;
import java.util.Set;

import fi.luomus.commons.containers.DateValue;
import fi.luomus.commons.containers.Selection;

/**
 * A structure representing one column in database table
 */
public interface _Column {

	int		ROWID							= 0;
	int		UNIQUE_NUMERIC_INCREASING_ID	= 1;
	int		IMMUTABLE_PART_OF_A_KEY			= 2;
	int		NORMAL_CHANGEABLE_FIELD			= 3;
	int		DATE_ADDED_COLUMN				= 4;
	int		DATE_MODIFIED_COLUMN			= 5;
	int		PASSWORD_COLUMN					= 6;
	int		USER_ID_COLUMN 					= 7;

	int		VARCHAR							= 1;
	int		INTEGER							= 2;
	int		DECIMAL							= 3;
	int		DATE							= 4;

	String	INPUT_TYPE_TEXT					= "TEXT";
	String	INPUT_TYPE_TEXTAREA				= "TEXTAREA";
	String	INPUT_TYPE_SELECTION			= "SELECT";
	String	INPUT_TYPE_DATE					= "DATE";
	String	INPUT_TYPE_ROWID				= "ROWID";
	String	INPUT_TYPE_PASSWORD				= "PASSWORD";

	String	MODIFIABLE_YES					= "YES";
	String	MODIFIABLE_NO					= "NO";

	/**
	 * Returns the database name of the column, for example: poikanen_id
	 * @return
	 */
	String getName();

	/**
	 * Returns the database name of the column with the name of the table, for example: poikanen.2.poikanen_id
	 * @return
	 */
	String getFullname();

	/**
	 * Name of parent table for example: poikanen
	 * @return
	 */
	String getDatabaseTablename();

	/**
	 * Full name of the parent table for example: poikanen.1
	 * @return
	 */
	String getTablename();

	/**
	 * Sets the value of this column
	 * @param value
	 */
	void setValue(String value);

	/**
	 * Sets the value of another column as the value of this column
	 * @param column
	 */
	void setValue(_Column column);

	/**
	 * Sets the value of this column
	 * @param value
	 */
	void setValue(Integer value);

	void setValue(Double value);

	/**
	 * Returns the value of the column. A leading zero is added to decimals if needed,
	 * decimal pointers are changed from point to comma, and keys are returned toUpperCase()
	 * @return value
	 */
	String getValue();

	/**
	 * Returns a list of values (separated by '|' character). A helper method.
	 * Returns an empty list if no values, or a list with size 1 if there is value, but no separators.
	 */
	List<String> getValueList();

	/**
	 * Returns the value of the column as a double.
	 * @return
	 * @throws NumberFormatException
	 */
	Double getDoubleValue() throws NumberFormatException;

	/**
	 * Returns the value of the column as an integer
	 * @return
	 * @throws NumberFormatException
	 */
	Integer getIntegerValue() throws NumberFormatException;

	/**
	 * Retuns value of this column parsed to date container object.
	 * @return
	 * @throws UnsupportedOperationException
	 */
	DateValue getDateValue() throws UnsupportedOperationException;

	/**
	 * Returns the value of this column "url encoded".
	 * @see java.net.URLEncoder.encode()
	 * @return
	 */
	String getUrlEncodedValue();

	/**
	 * Returns the type of the column. It is one of <br>
	 * ROWID = 0 <br>
	 * UNIQUE_NUMERIC_INCREASING_ID = 1 <br>
	 * IMMUTABLE_PART_OF_A_KEY = 2 <br>
	 * NORMAL_CHANGEABLE_FIELD = 3 <br>
	 * DATE_ADDED_COLUMN = 4 <br>
	 * DATE_MODIFIED_COLUMN = 5 <br>
	 * PASSWORD_COLUMN = 6 <br>
	 * USER_ID_COLUMN = 7 <br>
	 * <br>
	 * Not to be confused with getDatatype()
	 * @return column type
	 */
	int getFieldType();

	/**
	 * Returns the datatype of the column. It is one of <br>
	 * VARCHAR = 1 <br>
	 * INTEGER = 2 <br>
	 * DECIMAL = 3 <br>
	 * DATE = 4 <br>
	 * @return data type
	 */
	int getDatatype();

	/**
	 * Returns the maximium size of the column value. This is undefiend for dates. For decimal columns this returns the "scale" (max number of integers + decimals).
	 * @return max size of the column value
	 */
	int getSize();

	/**
	 * For decimal numbers, this returns the "precision"; maximium number of decimals that this column can contain
	 * @return precision
	 * @throws UnsupportedOperationException if the column is not defined to be DECIMAL datatype
	 */
	int getDecimalSize() throws UnsupportedOperationException;

	/**
	 * For the view, especially Tablemanagement -functionality. Tells if this column can be modified.
	 * By default the column can be modified if it is a NORMAL_CHANGEABLE_FIELD
	 * @return
	 */
	String getModifiability();

	/**
	 * For the view. Returns the type of HTML -element(s) that should be used. This is one of "TEXT", "TEXTAREA", "SELECT", "DATE", "ROWID".
	 * For example short INTEGER and VARCHAR columns are of type "TEXT" and long VARCHAR colums of type "TEXTAREA". If a selection has been defined for the column, the
	 * input type will be "SELECT".
	 * @return
	 */
	String getInputType();

	/**
	 * For the view. Returns the maximium number of characters that this column can store.
	 * @return
	 */
	int getInputSize();

	/**
	 * Returns the name of the selection that has been assosiated with this column. By default returns an empty string.
	 * The value can for example be "kunta" or "pesavakio.koord_mittaustapa".
	 * @return
	 */
	String getAssosiatedSelection();

	/**
	 * @return true if column is uppercase column, false if not
	 */
	boolean isUppercaseColumn();

	/**
	 * @return true if column value is required, false if it can be empty
	 */
	boolean isRequired();

	/**
	 * Returns the column that this column references (foreign key). Returns null if no reference.
	 * @return referenced column or null
	 */
	_ReferenceKey getReferenceColumn();

	/**
	 * Returns those columns that reference this column. Returns empty set if no references to this column.
	 * @return
	 */
	Set<_ReferenceKey> getReferencingColumns();

	/**
	 * Tells if the column has a value set, ie. something else than ""
	 * @return true if value set, false if not
	 */
	boolean hasValue();

	/**
	 * Tells if there is a selection assosiated with this column
	 * @return true if there is, false if not
	 */
	boolean hasAssosiatedSelection();

	/**
	 * Is this column rowID column?
	 * @return true if it is, false if not
	 */
	boolean isRowIdColumn();

	/**
	 * Returns minimum allowed value for this column (inclusive). +/- infinity by default.
	 * @return
	 * @throws UnsupportedOperationException if not of numeric type
	 */
	double getMinValue() throws UnsupportedOperationException;

	/**
	 * Returns maximum allowed value for this column (inclusive). +/- infinity by default.
	 * @return
	 * @throws UnsupportedOperationException if not of numeric type
	 */
	double getMaxValue() throws UnsupportedOperationException;

	/**
	 * Clears the value
	 */
	void clearValue();

	/**
	 * Get description of this column's value from selections
	 * @param selections
	 * @return
	 */
	String getSelectionDesc(Map<String, Selection> selections);

	boolean kirjekyyhkyRequired();

	/**
	 * Catenates the value to end of value. Will add a single space between original content and new content.
	 * @param value
	 */
	void appendValue(String value);

}
