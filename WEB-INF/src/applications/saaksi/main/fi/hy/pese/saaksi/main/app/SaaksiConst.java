package fi.hy.pese.saaksi.main.app;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import fi.hy.pese.framework.main.app.functionality.validate.PesimistulosValidationDefinition;
import fi.hy.pese.framework.main.app.functionality.validate.PesimistulosValidationDefinition.Havainto;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.luomus.commons.containers.Mapping;
import fi.luomus.commons.utils.CoordinateConverter;
import fi.luomus.commons.utils.Utils;

public class SaaksiConst {

	public static final Mapping<String> laaniNroToLaanitunnusMapping = initLaaniNroToLaanitunnusMapping();

	private static Mapping<String> initLaaniNroToLaanitunnusMapping() {
		Mapping<String> mapping = new Mapping<>();
		mapping.map("1", "N"); //          Uudenmaan lääni
		mapping.map("2", "T"); //          Turun ja Porin lääni
		mapping.map("3", "A"); //          Ahvenanmaa
		mapping.map("4", "H"); //          Hämeen lääni
		mapping.map("5", "Y"); //          Kymen lääni
		mapping.map("6", "M"); //          Mikkelin lääni
		mapping.map("7", "S"); //          Kuopion lääni
		mapping.map("8", "P"); //          Pohjois-Karjalan lääni
		mapping.map("9", "V"); //          Vaasan lääni
		mapping.map("10", "E"); //         Keski-Suomen lääni
		mapping.map("11", "O"); //         Oulun lääni
		mapping.map("12", "L"); //         Lapin lääni
		return mapping;
	}

	public static final String		TAULUT											= "taulut";
	public static final String		APUTAULU										= "aputaulu";
	public static final String		K_E												= "k_e";
	public static final String	REVIIRIT	= "reviirit";
	public static final String		JOINS											= joins();

	private static String joins() {
		StringBuilder joins = new StringBuilder();
		joins.append("       tarkastus.pesaid         =  pesa.pesaid                         ");
		joins.append(" AND   tarkastus.pesaid         =  olosuhde.pesaid                     ");
		joins.append(" AND   tarkastus.pesaid         =  vuosi.pesaid                        ");
		joins.append(" AND   tarkastus.vuosi          =  vuosi.vuosi                         ");
		joins.append(" AND   vuosi.reviiriid          =  reviiri.reviiriid                   ");
		joins.append(" AND   tarkastus.tarkastusid    =  poikanen.tarkastusid(+)             ");
		joins.append(" AND   tarkastus.tarkastusid    =  kaynti.tarkastus(+)                 ");

		joins.append(" AND   olosuhde.ilm_pvm =                                              ");
		joins.append("           (SELECT MAX(ilm_pvm)                                        ");
		joins.append("            FROM   olosuhde o2                                         ");
		joins.append("            WHERE  o2.pesaid = tarkastus.pesaid                        ");
		joins.append("            AND    ilm_pvm <= tarkastus.tark_pvm  )                    ");

		return joins.toString();
	}

	public static final String	ORDER_BY	= orderBy();

	private static String orderBy() {
		return " pesa.pesaid , tarkastus.tark_pvm DESC , poikanen.poikanenid ";
	}

	public static final Collection<String>	ONNISTUMISKOODIT =  Utils.collection("V", "T", "R", "Q", "P", "O", "M", "N", "L", "K");
	public static final Collection<String>	PESIMISTULOS_POIKANEN_NOT_ALLOWED	= Utils.collection("A", "B", "C", "D", "E", "F", "K", "L", "M", "N");
	public static final Collection<String>	PESIMISTULOS_MUNA_NOT_ALLOWED	= Utils.collection("A", "B", "C", "D", "E", "F");

	public static enum KoordTyyppi { YHT, EUR, AST }
	public static final Map<String, KoordTyyppi> KOORD_TYYPPI_MAPPING = initKoordTyyppiMapping();

	private static Map<String, KoordTyyppi> initKoordTyyppiMapping() {
		Map<String, KoordTyyppi> map = new HashMap<>();
		map.put("88", KoordTyyppi.YHT);
		map.put("89", KoordTyyppi.YHT);
		map.put("90", KoordTyyppi.YHT);
		map.put("91", KoordTyyppi.YHT);
		map.put("70", KoordTyyppi.EUR);
		map.put("61", KoordTyyppi.AST);
		map.put("65", KoordTyyppi.AST);
		return map;
	}

	public static void kirjekyyhkyModelToData(Map<String, String> datamap, _PesaDomainModelRow kirjekyyhkyparameters) {
		String leveys = datamap.get(Const.UPDATEPARAMETERS + ".koord_leveys");
		String pituus = datamap.get(Const.UPDATEPARAMETERS + ".koord_pituus");
		String koord_tyyppi = kirjekyyhkyparameters.getPesa().get("koord_tyyppi").getValue();
		String tietokantaanMenevaTyyppi = "";
		_Column levCol = null;
		_Column pitCol = null;

		if (koord_tyyppi.equals("Y")) {
			levCol = kirjekyyhkyparameters.getPesa().get("yht_leveys");
			pitCol = kirjekyyhkyparameters.getPesa().get("yht_pituus");
			tietokantaanMenevaTyyppi = "88";
		} else if (koord_tyyppi.equals("E")) {
			levCol = kirjekyyhkyparameters.getPesa().get("eur_leveys");
			pitCol = kirjekyyhkyparameters.getPesa().get("eur_pituus");
			tietokantaanMenevaTyyppi = "70";
		}

		if (levCol != null && pitCol != null) {
			convertCoordinates(kirjekyyhkyparameters, KOORD_TYYPPI_MAPPING.get(tietokantaanMenevaTyyppi));
			levCol.setValue(leveys);
			pitCol.setValue(pituus);
		}

		kirjekyyhkyparameters.getPesa().get("koord_tyyppi").setValue(tietokantaanMenevaTyyppi);
	}

	private static void convertCoordinates(_PesaDomainModelRow kirjekyyhkyparameters, KoordTyyppi tyyppi) {
		try {
			CoordinateConverter converter = new CoordinateConverter();
			switch (tyyppi) {
				case YHT:
					converter.setYht_leveys(kirjekyyhkyparameters.getPesa().get("yht_leveys").getValue());
					converter.setYht_pituus(kirjekyyhkyparameters.getPesa().get("yht_pituus").getValue());
					break;
				case EUR:
					converter.setEur_leveys(kirjekyyhkyparameters.getPesa().get("eur_leveys").getValue());
					converter.setEur_pituus(kirjekyyhkyparameters.getPesa().get("eur_pituus").getValue());
					break;
				case AST:
					converter.setAst_leveys(kirjekyyhkyparameters.getPesa().get("ast_leveys").getValue());
					converter.setAst_pituus(kirjekyyhkyparameters.getPesa().get("ast_pituus").getValue());
					break;
				default: throw new IllegalArgumentException("Unknown coord type: " + tyyppi);
			}
			converter.convert();
			kirjekyyhkyparameters.getPesa().get("yht_leveys").setValue(converter.getYht_leveys().toString());
			kirjekyyhkyparameters.getPesa().get("yht_pituus").setValue(converter.getYht_pituus().toString());
			kirjekyyhkyparameters.getPesa().get("eur_leveys").setValue(converter.getEur_leveys().toString());
			kirjekyyhkyparameters.getPesa().get("eur_pituus").setValue(converter.getEur_pituus().toString());
			kirjekyyhkyparameters.getPesa().get("ast_leveys").setValue(converter.getAst_leveys().toString());
			kirjekyyhkyparameters.getPesa().get("ast_pituus").setValue(converter.getAst_pituus().toString());
		} catch (Exception e) {
			// Something wrong with coordinates... we do nothing, validation will pick this up later.
		}
	}

	private static final PesimistulosValidationDefinition PESIMISTULOS_A = new PesimistulosValidationDefinition();
	private static final PesimistulosValidationDefinition PESIMISTULOS_B = new PesimistulosValidationDefinition().setSallitut(Havainto.EMOJA);
	private static final PesimistulosValidationDefinition PESIMISTULOS_C = PESIMISTULOS_B;
	private static final PesimistulosValidationDefinition PESIMISTULOS_D = PESIMISTULOS_B;
	private static final PesimistulosValidationDefinition PESIMISTULOS_E = PESIMISTULOS_B;
	private static final PesimistulosValidationDefinition PESIMISTULOS_F = PESIMISTULOS_B;
	private static final PesimistulosValidationDefinition PESIMISTULOS_K = PESIMISTULOS_B;
	private static final PesimistulosValidationDefinition PESIMISTULOS_L = new PesimistulosValidationDefinition().setSallitut(Havainto.EMOJA, Havainto.MUNIA);
	private static final PesimistulosValidationDefinition PESIMISTULOS_M = PESIMISTULOS_L;
	private static final PesimistulosValidationDefinition PESIMISTULOS_N = PESIMISTULOS_L;
	private static final PesimistulosValidationDefinition PESIMISTULOS_O = PESIMISTULOS_L;
	private static final PesimistulosValidationDefinition PESIMISTULOS_P = new PesimistulosValidationDefinition().setSallitut(Havainto.EMOJA, Havainto.MUNIA, Havainto.PESAP).setVaaditut(Havainto.KUOLLEITA);
	private static final PesimistulosValidationDefinition PESIMISTULOS_Q = new PesimistulosValidationDefinition().setSallitut(Havainto.EMOJA, Havainto.MUNIA, Havainto.KUOLLEITA).setVaaditut(Havainto.PESAP);
	private static final PesimistulosValidationDefinition PESIMISTULOS_R = PESIMISTULOS_Q;
	private static final PesimistulosValidationDefinition PESIMISTULOS_T = PESIMISTULOS_P;
	private static final PesimistulosValidationDefinition PESIMISTULOS_V = new PesimistulosValidationDefinition().setSallitut(Havainto.EMOJA, Havainto.MUNIA, Havainto.KUOLLEITA, Havainto.PESAP).setVaaditut(Havainto.LENTOP);
	private static final PesimistulosValidationDefinition PESIMISTULOS_Y = new PesimistulosValidationDefinition().setSallitut(Havainto.EMOJA);
	private static final PesimistulosValidationDefinition PESIMISTULOS_X = new PesimistulosValidationDefinition().setSallitut(Havainto.EMOJA);

	public static Map<String, PesimistulosValidationDefinition> PESIMISTULOS_VALIDATION_DEFINITIONS = initPesimistulosValidationDefinitions();

	private static HashMap<String, PesimistulosValidationDefinition> initPesimistulosValidationDefinitions() {
		HashMap<String, PesimistulosValidationDefinition> map = new HashMap<>();
		map.put("A", PESIMISTULOS_A);
		map.put("B", PESIMISTULOS_B);
		map.put("C", PESIMISTULOS_C);
		map.put("D", PESIMISTULOS_D);
		map.put("E", PESIMISTULOS_E);
		map.put("F", PESIMISTULOS_F);
		map.put("K", PESIMISTULOS_K);
		map.put("L", PESIMISTULOS_L);
		map.put("M", PESIMISTULOS_M);
		map.put("N", PESIMISTULOS_N);
		map.put("O", PESIMISTULOS_O);
		map.put("P", PESIMISTULOS_P);
		map.put("Q", PESIMISTULOS_Q);
		map.put("R", PESIMISTULOS_R);
		map.put("T", PESIMISTULOS_T);
		map.put("V", PESIMISTULOS_V);
		map.put("Y", PESIMISTULOS_Y);
		map.put("X", PESIMISTULOS_X);
		return map;
	}

	public static final String	REPORT_PESIMISTULOKSET	= "report_pesimistulokset";
	public static final String	REPORT_POIKASMAARAT	= "report_poikasmaarat";
	public static final String	REPORT_HAVAINNOIJALISTAUS	= "report_havainnoijalistaus";
	public static final String	REPORT_KANSILEHDET	= "report_kansilehdet";
	public static final String	TAMAN_TYYPPINEN_PESA_EI_VOI_SIJAITA_PUUSSA	= "TAMAN_TYYPPINEN_PESA_EI_VOI_SIJAITA_PUUSSA";
	public static final String	PESA_IS_DEAD_CAN_NOT_INSERT	= "PESA_IS_DEAD_CAN_NOT_INSERT";
	public static final String	REVIIRI_ALREADY_HAS_SUCCESSFUL_NESTING	= "REVIIRI_ALREADY_HAS_SUCCESSFUL_NESTING";
	public static final String	ILMOITETTU_REVIIRIN_NIMI_JA_REVIIRI_EIVAT_TASMAA	= "ILMOITETTU_REVIIRIN_NIMI_JA_REVIIRI_EIVAT_TASMAA";
	public static final String	INVALID_TARK_VUOSI_TARK_PVM_YYYY	= "INVALID_TARK_VUOSI_TARK_PVM_YYYY";
	public static final String	INVALID_PESIMISTULOS_POIKASET	= "INVALID_PESIMISTULOS_POIKASET";
	public static final String	INVALID_PESIMISTULOS_MUNAT	= "INVALID_PESIMISTULOS_MUNAT";
	public static final String	INVALID_PESIMISTULOS_LENTOPOIKASET	= "INVALID_PESIMISTULOS_LENTOPOIKASET";
	public static final String	MUULAJI_CAN_NOT_BE_PANHAL	= "MUULAJI_CAN_NOT_BE_PANHAL";
	public static final String	PESIMISTULOS_JA_HAVAINTO_RISTIRIIDASSA	= "PESIMISTULOS_JA_HAVAINTO_RISTIRIIDASSA";
	public static final String	EI_TARPEEKSI_HAVAINTOJA_PESIMISTULOKSELLE	= "EI_TARPEEKSI_HAVAINTOJA_PESIMISTULOKSELLE_";
	public static final String	TIIVISTETYT_LKMT_VIRHE	= "TIIVISTETYT_LKMT_VIRHE";

}
