package fi.hy.pese.framework.main.db;

import java.util.List;

import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;

public class RowsToListResultHandler extends RowCombiningResultHandler {

	private final List<_PesaDomainModelRow> list;

	public RowsToListResultHandler(_DAO<_PesaDomainModelRow> dao, List<_PesaDomainModelRow> list) {
		super(dao);
		this.list = list;
	}

	@Override
	protected boolean handle(_PesaDomainModelRow row) {
		list.add(row);
		return true;
	}

}
