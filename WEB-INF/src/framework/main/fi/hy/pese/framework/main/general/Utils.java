package fi.hy.pese.framework.main.general;

import java.util.HashSet;
import java.util.Set;

import fi.hy.pese.framework.main.app.functionality.validate._Validator;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._Row;
import fi.hy.pese.framework.main.general.data._Table;


public class Utils {
	
	/**
	 * Column value as double. Delimiter can be either , or .
	 * @param c
	 * @return
	 * @throws NumberFormatException if value is not a valid Double
	 */
	public static Double toDouble(_Column c) throws NumberFormatException {
		String value = c.getValue();
		value = value.replace(',', '.');
		return Double.valueOf(value);
	}
	
	public static void debug(_Row row) {
		System.out.println(debugS(row));
	}
	
	public static String debugS(_Row row) {
		StringBuilder b = new StringBuilder();
		for (_Table t : row) {
			if (!t.hasValues()) continue;
			b.append(t.getFullname()).append(": ").append(debugS(t));
		}
		return b.toString();
	}
	
	public static void debug(_Table t) {
		System.out.print(debugS(t));
	}
	
	public static String debugS(_Table table) {
		StringBuilder b = new StringBuilder();
		for (_Column c : table) {
			if (c.hasValue()) {
				b.append(c.getFullname()).append("|").append(c.getValue()).append("|");
			}
		}
		return b.toString();
	}
	
	/**
	 * Splits column's name. For example pesa.nimi
	 * @param columnFullname
	 * @return [0] = table name, [1] = column name
	 * @throws IllegalArgumentException if the name is not a fullname (is not in the form <tablename>.<columname>)
	 */
	public static String[] splitColumnFullname(String columnFullname) throws IllegalArgumentException {
		if (!columnFullname.contains(".")) { throw new IllegalArgumentException(columnFullname + " is not of form <tablename>.<columname>"); }
		String[] arr = new String[2];
		int i = columnFullname.lastIndexOf('.');
		arr[0] = columnFullname.substring(0, i);
		arr[1] = columnFullname.substring(i + 1);
		return arr;
	}
	
	/**
	 * Returns the name of the table from a column full name, for example for "pesa.nimi" returns "pesa".
	 * @param columnFullname
	 * @return
	 * @throws IllegalArgumentException if the name is not a fullname (is not in the form <tablename>.<columname>)
	 */
	public static String tableName(String columnFullname) throws IllegalArgumentException {
		return splitColumnFullname(columnFullname)[0];
	}
	
	
	/**
	 * Returns the name of the column from a column full name, for example for "pesa.nimi" returns "nimi";
	 * @param columnFullname
	 * @return
	 * @throws IllegalArgumentException if the name is not a fullname (is not in the form <tablename>.<columname>)
	 */
	public static String columnName(String columnFullname) throws IllegalArgumentException {
		return splitColumnFullname(columnFullname)[1];
	}
	
	/**
	 * Removes the last Const.SEPARATOR from the StringBuilder
	 * @param query
	 */
	public static void removeLastSeparator(StringBuilder query) {
		if (query.length() < 1) return;
		query.delete(query.length() - Const.SEPARATOR.length(), query.length());
	}
	
	public static void debug(_Validator validator) {
		fi.luomus.commons.utils.Utils.debug(validator.errors());
		fi.luomus.commons.utils.Utils.debug(validator.warnings());
	}
	
	public static int sum(_Column ... columns) {
		int sum = 0;
		for (_Column c : columns) {
			if (c.hasValue()) {
				try { sum += c.getIntegerValue(); }
				catch (NumberFormatException e) { }
			}
		}
		return sum;
	}
	
	public static String debugS(_Table table, String[] ignore) {
		Set<String> ignoreThese = new HashSet<>();
		for (String s : ignore) {
			ignoreThese.add(s);
		}
		StringBuilder b = new StringBuilder();
		for (_Column c : table) {
			if (ignoreThese.contains(c.getName())) continue;
			if (c.hasValue()) {
				//b.append(c.getFullname()).append("|").append(c.getValue()).append("|");
				b.append(c.getName()).append(" ").append(trimToLength(c.getValue(), 7)).append(" ");
			}
		}
		return b.toString();
	}
	
	public static String trimToLength(String text, int length) {
		text += 	"                                                                                                                                      ";
		return text.substring(0, length);
	}
	
	public static Integer onlyNumbers(String vastaanottaja) {
		String numbers = "";
		for (char c : vastaanottaja.toCharArray()) {
			if (Character.isDigit(c)) {
				numbers += c;
			}
		}
		if (numbers.length() > 0) {
			return Integer.valueOf(numbers);
		}
		return null;
	}
	
}
