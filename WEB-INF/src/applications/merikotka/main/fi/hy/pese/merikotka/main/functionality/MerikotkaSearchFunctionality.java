package fi.hy.pese.merikotka.main.functionality;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.app.functionality.NestSorter;
import fi.hy.pese.framework.main.app.functionality.SearchFunctionality;
import fi.hy.pese.framework.main.general.ReportWriter;
import fi.hy.pese.framework.main.general._PDFDataMapGenerator;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.main.general.data._ResultSorter;
import fi.hy.pese.framework.main.general.data._Table;
import fi.hy.pese.merikotka.main.app.MerikotkaConst;
import fi.hy.pese.merikotka.main.functionality.reports.MerikotkaReport_pesimistulostenYhteenvetolista;
import fi.hy.pese.merikotka.main.functionality.reports.MerikotkaReport_reviirilista;
import fi.hy.pese.merikotka.main.functionality.reports.MerikotkaReport_tarkastamattomat;
import fi.luomus.commons.tipuapi.TipuAPIClient;
import fi.luomus.commons.utils.Utils;
import fi.luomus.commons.xml.Document.Node;

public class MerikotkaSearchFunctionality extends SearchFunctionality {

	public MerikotkaSearchFunctionality(_Request<_PesaDomainModelRow> request) {
		super(request);
	}

	private MerikotkaDataToMapForPDFPrintingGenerator	dataToMapForPDFPrintingGenerator	= null;

	@Override
	public void preProsessingHook() throws Exception {
		super.preProsessingHook();
		// "hideAdvancedSearch"
	}

	@Override
	public String nestingOutput(_Table tarkastus) {
		return MerikotkaConst.poikasCount(tarkastus);
	}

	@Override
	public _ResultSorter<_PesaDomainModelRow> sorter() throws Exception {
		return new NestSorter(new MerikotkaComparator());
	}

	public static class MerikotkaComparator implements Comparator<_PesaDomainModelRow> {
		@Override
		public int compare(_PesaDomainModelRow first, _PesaDomainModelRow second) {
			String kuntaOfFirst = first.getVuosi().get("reviirin_kunta").getValue();
			String kuntaOfSecond = second.getVuosi().get("reviirin_kunta").getValue();
			int result = kuntaOfFirst.compareTo(kuntaOfSecond);
			if (result != 0) return result;
			result = first.getReviiri().get("reviirinimi").getValue().compareTo(second.getReviiri().get("reviirinimi").getValue());
			if (result != 0) return result;
			result = first.getReviiri().get("reviiri_id").getValue().compareTo(second.getReviiri().get("reviiri_id").getValue());
			if (result != 0) return result;
			result = first.getPesa().get("pesanimi").getValue().compareTo(second.getPesa().get("pesanimi").getValue());
			if (result != 0) return result;
			result = first.getPesa().get("pesa_id").getValue().compareTo(second.getPesa().get("pesa_id").getValue());
			return result;
		}

	}

	@Override
	public _PDFDataMapGenerator pdfDataMapGenerator() {
		if (dataToMapForPDFPrintingGenerator == null) {
			try {
				dataToMapForPDFPrintingGenerator = new MerikotkaDataToMapForPDFPrintingGenerator(dao, data.selections(), this, data.uiTexts());
			} catch (Exception e) {
				throw new RuntimeException("Unable to initialize pdf generator", e);
			}
		}
		return dataToMapForPDFPrintingGenerator;
	}

	private static final String	DEAD_NESTS_SQL	= deadNestSql();
	private static final String	LIVE_NESTS_SQL	= liveNestSql();

	private static String deadNestSql() {
		StringBuilder query = new StringBuilder();
		query.append(" pesavakio.pesa_id IN (SELECT DISTINCT pesa_id FROM pesatarkastus WHERE pesa_kunto IN ( ");
		Utils.toCommaSeperatedStatement(query, MerikotkaConst.PESAN_KUNTO__DEAD, true);
		query.append(" ) ) ");
		return query.toString();
	}
	private static String liveNestSql() {
		StringBuilder query = new StringBuilder();
		query.append(" pesavakio.pesa_id NOT IN (SELECT DISTINCT pesa_id FROM pesatarkastus WHERE pesa_kunto IN ( ");
		Utils.toCommaSeperatedStatement(query, MerikotkaConst.PESAN_KUNTO__DEAD, true);
		query.append(" ) ) ");
		return query.toString();
	}

	@Override
	public String deadNestsQuery()  {
		return DEAD_NESTS_SQL;
	}

	@Override
	public String liveNestsQuery() {
		return LIVE_NESTS_SQL;
	}

	@Override
	protected void applicationPrintReport(String reportName) {
		ReportWriter<_PesaDomainModelRow> writer = null;
		if (reportName.equals(MerikotkaConst.REPORT_PESIMISTULOKSET)) {
			writer = new MerikotkaReport_pesimistulostenYhteenvetolista(request, reportName);
		} else if (reportName.equals(MerikotkaConst.REPORT_REVIIRIT)) {
			writer = new MerikotkaReport_reviirilista(request, reportName);
		} else if (reportName.equals(MerikotkaConst.REPORT_TARKASTAMATTOMAT)) {
			writer = new MerikotkaReport_tarkastamattomat(request, reportName, this);
		} else if (reportName.equals(MerikotkaConst.REPORT_VIRANOMAIS)) {
			writer = new MerikotkaReport_pesimistulostenYhteenvetolista(request, MerikotkaConst.REPORT_VIRANOMAIS, true);
		}
		if (writer != null) {
			writer.produce();
		} else {
			data.addToNoticeTexts(reportName + " not implemented");
		}
	}

	@Override
	protected void additionalSearchConditionsHook(List<String> additionalQueries) {
		if (data.contains(Const.SUURALUE)) {
			addSuuralueSearchQuery(additionalQueries);
		}
		if (data.contains(Const.YMPARISTOKESKUS)) {
			addYmparistokeskusSearchQuery(additionalQueries);
		}
	}

	private void addSuuralueSearchQuery(List<String> additionalQueries) {
		List<String> suuralueet = Utils.valuelist(data.get(Const.SUURALUE));
		List<String> suuralueenKunnat = new LinkedList<>();
		for (Node n : request.getTipuApiResource(TipuAPIClient.MUNICIPALITIES).getAll()) {
			if (suuralueet.contains(n.getNode("merikotka-suuralue").getContents())) {
				suuralueenKunnat.add(n.getNode("id").getContents());
			}
		}
		StringBuilder statement = new StringBuilder();
		statement.append(data.searchparameters().getVuosi().get("reviirin_kunta").getFullname());
		statement.append(" IN ( ");
		Utils.toCommaSeperatedStatement(statement, suuralueenKunnat, true);
		statement.append(" ) ");
		additionalQueries.add(statement.toString());
	}

	private void addYmparistokeskusSearchQuery(List<String> additionalQueries) {
		String ympkesk = data.get(Const.YMPARISTOKESKUS);
		List<String> ympkeskKunnat = new LinkedList<>();
		for (Node n : request.getTipuApiResource(TipuAPIClient.MUNICIPALITIES).getAll()) {
			if (n.getNode("ely-centre").getContents().equals(ympkesk)) {
				ympkeskKunnat.add(n.getNode("id").getContents());
			}
		}
		StringBuilder statement = new StringBuilder();
		statement.append(data.searchparameters().getVuosi().get("reviirin_kunta").getFullname());
		statement.append(" IN ( ");
		Utils.toCommaSeperatedStatement(statement, ympkeskKunnat, true);
		statement.append(" ) ");
		additionalQueries.add(statement.toString());
	}

}
