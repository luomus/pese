package fi.hy.pese.ulko.main.functionality;

import java.util.Date;

import org.apache.http.client.methods.HttpGet;

import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.app.functionality.BaseFunctionality;
import fi.hy.pese.framework.main.app.functionality.validate.Validator;
import fi.hy.pese.framework.main.app.functionality.validate._Validator;
import fi.hy.pese.framework.main.general.data._Table;
import fi.hy.pese.ulko.main.app._UlkoDomainModelRow;
import fi.luomus.commons.http.HttpClientService;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.Utils;

public class ReceiveFromLoydosFunctionality extends BaseFunctionality<_UlkoDomainModelRow> {

	public ReceiveFromLoydosFunctionality(_Request<_UlkoDomainModelRow> request) {
		super(request);
	}

	private static class ReceiveFromLoydosValidator extends Validator<_UlkoDomainModelRow> {

		public ReceiveFromLoydosValidator(_Request<_UlkoDomainModelRow> request) {
			super(request);
		}

		@Override
		public void validate() throws Exception {
			String loydosid = data.get("loydosid");
			checkNotNull("Virhe otettaessa vastaan Löydöksestä. ID", loydosid);
		}

	}

	@Override
	public _Validator validator() throws Exception {
		return new ReceiveFromLoydosValidator(request);
	}

	@Override
	public void afterSuccessfulValidation() throws Exception {
		String loydosid = data.get("loydosid");
		SubmissionData submissionData = getSubmissionData(loydosid);

		_UlkoDomainModelRow row = data.updateparameters();
		_Table pyynto = row.getPyynto();
		pyynto.get("tila").setValue("1");
		pyynto.get("roskiin").setValue("E");
		pyynto.get("loydosid").setValue(loydosid);

		_Table loyto = row.getLoyto();

		JSONObject submission = submissionData.jsonObject;
		//loyto.get("alkup_kirje_sisalto").setValue( new XMLWriter(new XMLReader().parse("<foo>"+XML.toString(new org.json.JSONObject(submissionData.jsonString)) + "</foo>")).generateXML()  );
		loyto.get("alkup_kirje_sisalto").setValue(formatJSONtoLaymenReadeable(submissionData.jsonString));

		JSONObject location = submission.getObject("location");
		String kunta = location.getString("municipality");
		if (location.getArray("coordinates").size() > 0) {
			JSONObject coordinates = location.getArray("coordinates").iterateAsObject().get(0);
			loyto.get("wgs84_leveys").setValue(tryToGetDouble(coordinates, "latitude"));
			loyto.get("wgs84_pituus").setValue(tryToGetDouble(coordinates, "longitude"));
		}
		loyto.get("koord_ilm_tyyppi").setValue("W");
		loyto.get("kunta").setValue(kunta);
		loyto.get("tarkempi_paikka").setValue(location.getString("description"));
		String maa = location.getString("country");
		if ("FI".equals(maa)) {
			loyto.get("euring_paikka").setValue(MunicipalityToEuringPlaceQuery.resolve(kunta, request));
		} else {
			loyto.get("kunta").setValue(maa + ", " + kunta);
		}
		JSONObject ilmoittaja = submission.getObject("submitter");
		JSONObject loytaja = submission.getObject("discoverer");
		if (loytaja.isEmpty()) {
			setPerson("loytaja", ilmoittaja, submission.getString("submitterRingerNumber"));
		} else {
			setPerson("loytaja", loytaja, submission.getString("submitterRingerNumber"));
			setPerson("ilmoittaja", ilmoittaja, submission.getString("discovererRingerNumber"));
		}

		JSONObject observation = submission.getObject("observation");
		String dateString = observation.getString("date");
		if (given(dateString)) {
			Date date = DateUtils.convertToDate(dateString, "yyyy-MM-dd");
			loyto.get("pvm").setValue(DateUtils.format(date, "dd.MM.yyyy"));
		}
		loyto.get("aika_tarkkuus").setValue(observation.getString("accuracy"));

		loyto.get("kommentti").setValue(removeNewlines(observation.getString("additionalInfo")));
		loyto.get("rengas_selite").setValue(observation.getString("ringInfo"));
		loyto.get("sukupuoli").setValue(observation.getString("gender"));

		appendToCommentIfGiven("Species", observation.getString("species"));
		appendToCommentIfGiven("Age", observation.getString("age"));

		appendToCommentIfGiven("", observation.getString("discoveryOrDeathReason"));
		appendToCommentIfGiven("Dead for", observation.getString("deadDuration"));

		String birdState = observation.getString("birdState");
		if (given(birdState)) {
			if (birdState.startsWith("7")) {
				birdState = "7";
			}
			loyto.get("linnun_tila").setValue(birdState);
		}

		String kuvaIds = "";
		for (String attachmentID : submission.getArray("attachments")) {
			kuvaIds += attachmentID +";";
		}
		if (kuvaIds.endsWith(",")) {
			kuvaIds = Utils.removeLastChar(kuvaIds);
		}
		pyynto.get("loydos_kuvaid").setValue(kuvaIds);

		data.setPage("main_form");
		data.set("fromloydos", "true");
	}

	private String tryToGetDouble(JSONObject object, String field) {
		if (object.hasKey(field)) {
			return Double.toString(object.getDouble(field));
		}
		return "";
	}
	private String formatJSONtoLaymenReadeable(String json) {
		return json.replace("{", "\n").
				replace("}", "\n\n").
				replace(",\"", ",\n\"").
				replace("\\r\\n", " ").
				replace("[", "").
				replace("]", "").
				replace("\n,\n", "\n").
				replace("\"", "");
	}

	private void appendToCommentIfGiven(String prefixText, String value) {
		if (given(value)) {
			value = removeNewlines(value);
			if (given(prefixText)) {
				prefixText += ": ";
			}
			data.updateparameters().getLoyto().get("kommentti").appendValue("\n" + prefixText + value);
		}
	}

	private String removeNewlines(String value) {
		if (value == null) return null;
		return value.replace("\n", " ").replace("\r", " ").trim();
	}

	private boolean given(String value) {
		return value != null && value.length() > 0;
	}

	private void setPerson(String fieldprefix, JSONObject data, String rengastajanumero) {
		_Table pyynto = this.data.updateparameters().getPyynto();
		pyynto.get(fieldprefix+"_renro").setValue(rengastajanumero);
		pyynto.get(fieldprefix+"_nimi").setValue(data.getString("firstName") + " " + data.getString("lastName"));
		pyynto.get(fieldprefix+"_osoite").setValue(data.getString("street"));
		pyynto.get(fieldprefix+"_postinumero").setValue(data.getString("postalCode"));
		pyynto.get(fieldprefix+"_postitoimipaikka").setValue(data.getString("city"));
		pyynto.get(fieldprefix+"_puhelin").setValue(data.getString("phone"));
		pyynto.get(fieldprefix+"_email").setValue(data.getString("email"));
	}

	private static class SubmissionData {
		public final JSONObject jsonObject;
		public final String jsonString;
		public SubmissionData(JSONObject jsonObject, String jsonString) {
			this.jsonObject = jsonObject;
			this.jsonString = jsonString;
		}
	}
	private SubmissionData getSubmissionData(String loydosid) throws Exception {
		HttpClientService client = null;
		try {
			String uri = config.get("Loydos_uri") + "/admin/api/submissions/rengasloyto/";
			client = new HttpClientService(uri, config.get("Loydos_username"), config.get("Loydos_password"));
			String response = client.contentAsString(new HttpGet(uri + loydosid));
			return new SubmissionData(new JSONObject(response), response);
		} finally {
			if (client != null) client.close();
		}
	}

	@Override
	public void afterFailedValidation() throws Exception {
		data.setPage("main_form");
	}

	@Override
	public void postProsessingHook() throws Exception {
		new LoydosUtil(request).setLoydosValuesToData();
	}



}
