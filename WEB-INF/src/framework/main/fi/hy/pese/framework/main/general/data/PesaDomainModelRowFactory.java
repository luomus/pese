package fi.hy.pese.framework.main.general.data;

public class PesaDomainModelRowFactory implements _RowFactory<_PesaDomainModelRow> {

	private final String joins;
	private final String orderby;
	private final PesaDomainModelDatabaseStructure	rowStructure;

	public PesaDomainModelRowFactory(PesaDomainModelDatabaseStructure rowStructure, String joins, String orderby) {
		this.joins = joins;
		this.orderby = orderby;
		this.rowStructure = rowStructure;
	}

	@Override
	public String joins() {
		return joins;
	}

	@Override
	public String orderby() {
		return orderby;
	}

	@Override
	public _PesaDomainModelRow newRow() {
		return new PesaDomainModelRow(rowStructure);
	}

	@Override
	public _PesaDomainModelRow newRow(String prefix) {
		return new PesaDomainModelRow(rowStructure, prefix);
	}

}
