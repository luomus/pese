package fi.hy.pese.framework.main.app.functionality.validate;

import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data._Row;
import fi.hy.pese.framework.main.general.data._Table;

public class LoginValidatorWithUsersStoredToDatabase<T extends _Row> extends Validator<T> {

	public LoginValidatorWithUsersStoredToDatabase(_Request<T> request) {
		super(request);
	}

	@Override
	public void validate() throws Exception {
		if (data.action().equals(Const.CHECK_LOGIN)) {

			String username = data.get(Const.LOGIN_USERNAME);
			String password = data.get(Const.LOGIN_PASSWORD);

			if (isNull(username) || isNull(password)) {
				error(Const.LOGIN_FIELDS, LOGIN_INCORRECT);
				return;
			}

			_Table table = dao.newTableByName("kayttaja");
			table.get("username").setValue(username);
			table.get("password").setValue(password);

			if (!dao.checkValuesExists(table)) {
				error(Const.LOGIN_FIELDS, LOGIN_INCORRECT);
			}

		}
	}

}
