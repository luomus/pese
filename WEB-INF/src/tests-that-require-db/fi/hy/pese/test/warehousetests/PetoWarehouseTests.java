package fi.hy.pese.test.warehousetests;

import java.io.File;
import java.net.URL;

import fi.hy.pese.framework.main.db._DAO;
import fi.hy.pese.framework.main.general.WarehouseRowGenerator;
import fi.hy.pese.framework.main.general.WarehouseRowGenerator.WarehouseGenerationDefinition;
import fi.hy.pese.framework.main.general.data.PesaDomainModelDatabaseStructure;
import fi.hy.pese.framework.main.general.data.PesaDomainModelRowFactory;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.test.TestConfig;
import fi.hy.pese.framework.test.db.DAOStub;
import fi.hy.pese.peto.main.app.PetoConst;
import fi.hy.pese.peto.main.app.PetoDatabaseStructureCreator;
import fi.hy.pese.peto.main.app.PetoFactory;
import fi.hy.pese.peto.main.app.PetoWarehouseDefinition;
import fi.laji.datawarehouse.etl.models.dw.DwRoot;
import fi.laji.datawarehouse.etl.models.dw.Gathering;
import fi.luomus.commons.utils.FileUtils;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class PetoWarehouseTests {
	
	public static Test suite() {
		return new TestSuite(PetoWarehouseTests.class.getDeclaredClasses());
	}
	
	public static class WhenGeneratingWarehouseDocument extends TestCase {
		
		public class FakeDAO<T extends _PesaDomainModelRow> extends DAOStub<T> {
			public FakeDAO() {}
			
		}
		
		private PetoFactory application;
		private _DAO<_PesaDomainModelRow> dao;
		private WarehouseRowGenerator generator;
		private _PesaDomainModelRow row;
		
		@Override
		protected void setUp() throws Exception {
			application = new PetoFactory(TestConfig.getConfigFile("pese_peto.config"));
			
			PesaDomainModelDatabaseStructure structure = PetoDatabaseStructureCreator.hardCoded();
			application.setRowFactory(new PesaDomainModelRowFactory(structure, null, null));
			application.loadUITexts();
			dao = application.dao(PetoWarehouseTests.class.getSimpleName());
			WarehouseGenerationDefinition definition = PetoWarehouseDefinition.instance();
			generator = new WarehouseRowGenerator(definition, dao, application);
			row = dao.newRow();
		}
		
		@Override
		protected void tearDown() throws Exception {
			if (application != null) if (dao != null) application.release(dao);
		}
		
		public void test__delete() throws Exception {
			DwRoot root = generator.deletedEntry(123);
			assertRoot("peto_1_delete.json", root);
		}
		
		public void test__not_inspected() throws Exception {
			row.getPesa().getUniqueNumericIdColumn().setValue(1);
			row.getTarkastus().getUniqueNumericIdColumn().setValue(100);
			row.getTarkastus().get("vuosi").setValue(2020);
			row.getTarkastus().get("tarkastettu").setValue("E");
			DwRoot root = generator.generate(row);
			assertRoot("peto_2_not_inspected.json", root);
		}
		
		public void test__common() throws Exception {
			commonTestData();
			DwRoot root = generator.generate(row);
			assertRoot("peto_3_common.json", root);
		}
		
		private void commonTestData() {
			row.getPesa().getUniqueNumericIdColumn().setValue(1);
			row.getTarkastus().getUniqueNumericIdColumn().setValue(100);
			row.getTarkastus().get("vuosi").setValue(2020);
			row.getTarkastus().get("tarkastettu").setValue("K");

			row.getTarkastus().get("kommentti_edellinen_laji").setValue("PANHAL");
			row.getPesa().get("kunta").setValue("UTSJOK");
			row.getPesa().get("kyla").setValue("KOHMOKOTKA");
			row.getPesa().get("eur_leveys").setValue(7721234);
			row.getPesa().get("eur_pituus").setValue(501234);
			row.getPesa().get("koord_ilm_mittaustapa").setValue("K");
			row.getPesa().get("loyt_vuosi").setValue(2013);
			row.getTarkastus().get("pesa_sijainti").setValue("M");
			row.getTarkastus().get("maasto_1").setValue("F");
			row.getTarkastus().get("maasto_2").setValue("R");
			row.getPesa().get("kommentti").setValue("Should not sent");
			row.getTarkastus().get("kommentti_kaynnit").setValue("Should not sent");

			row.getTarkastus().get("tarkastaja").setValue(846);
			row.getTarkastus().get("pesiva_laji").setValue("BUTLAG");

			row.getKaynnit().get(1).getUniqueNumericIdColumn().setValue(12345);
			row.getKaynnit().get(1).get("pvm").setValue("16.6.2004");
			row.getKaynnit().get(1).get("pvm_tarkkuus").setValue("1");
			row.getKaynnit().get(1).get("pesa_kunto").setValue("0");
			row.getKaynnit().get(1).get("munia_lkm").setValue("1");
			row.getKaynnit().get(1).get("kuolleita_lkm").setValue("1");

			row.getTarkastus().get("pesimistulos").setValue("P");
			row.getTarkastus().get("epaonnistuminen").setValue("O");
			
			row.getTarkastus().get("nayte_munia").setValue("K");
		}

		public void test__multi_kaynti() throws Exception {
			commonTestData();
			row.getKaynnit().get(2).getUniqueNumericIdColumn().setValue(12346);
			row.getKaynnit().get(2).get("pvm").setValue("25.6.2004");
			row.getKaynnit().get(2).get("pvm_tarkkuus").setValue("1");
			row.getKaynnit().get(2).get("pesa_kunto").setValue("0");
			row.getKaynnit().get(2).get("kuolleita_lkm").setValue("1");

			DwRoot root = generator.generate(row);
			assertRoot("peto_4_multi_kaynti.json", root);
		}

		public void test__forbidden_use() throws Exception {
			commonTestData();
			row.getColumn("pesa.suojelu_kaytto").setValue("E");
			DwRoot root = generator.generate(row);
			assertRoot("peto_5_forbidden_use.json", root);
		}
		
		public void test__muulaji() throws Exception {
			commonTestData();
			row.getTarkastus().get("pesiva_laji").setValue("");
			row.getTarkastus().get("muu_laji").setValue("BUCCLA");
			DwRoot root = generator.generate(row);
			assertRoot("peto_6_other_species.json", root);
		}
		
		public void test__date_accuracy() throws Exception {
			commonTestData();
			row.getKaynnit().get(1).get("pvm_tarkkuus").setValue("2");
			DwRoot root = generator.generate(row);
			for (Gathering g : root.getPublicDocument().getGatherings()) {
				assertEquals("2004-06-01 - 2004-06-30", g.getDisplayDateTime());
			}
			
			row.getKaynnit().get(1).get("pvm_tarkkuus").setValue("3");
			root = generator.generate(row);
			for (Gathering g : root.getPublicDocument().getGatherings()) {
				assertEquals("2020-01-01 - 2020-12-31", g.getDisplayDateTime());
			}
		}
		
		public void test__dead_nest() throws Exception {
			commonTestData();
			row.getColumn("kaynti.1.pesa_kunto").setValue(PetoConst.DEAD_NEST_CODES.iterator().next());
			DwRoot root = generator.generate(row);
			assertRoot("peto_7_dead_nest.json", root);
		}

		public void test__no_name_public() throws Exception {
			commonTestData();

			row.getColumn("tarkastus.tarkastaja").setValue("1146");
			DwRoot root = generator.generate(row);
			assertEquals("[Turku \"falco-Kerho\"]", root.getPrivateDocument().getGatherings().get(0).getTeam().toString());
			assertEquals("[Hidden]", root.getPublicDocument().getGatherings().get(0).getTeam().toString());
		}

		public void test__coordinate_uncertainty() throws Exception {
			commonTestData();
			
			row.getPesa().get("koord_ilm_mittaustapa").setValue("X");
			
			DwRoot root = generator.generate(row);
			assertEquals("7721234.0 : 7721235.0 : 501234.0 : 501235.0 : EUREF : 1000", root.getPublicDocument().getGatherings().get(0).getCoordinates().toString());
		}

		private void assertRoot(String filename, DwRoot root) {
			URL url = this.getClass().getResource(filename);
			File file = new File(url.getFile());
			try {
				String data = FileUtils.readContents(file);
				assertEquals(data, root.toJSON().beautify());
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
		
	}
	
}
