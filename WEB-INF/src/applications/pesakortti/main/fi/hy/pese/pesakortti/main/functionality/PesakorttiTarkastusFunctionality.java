package fi.hy.pese.pesakortti.main.functionality;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.app.functionality.TarkastusFunctionality;
import fi.hy.pese.framework.main.app.functionality.validate._Validator;
import fi.hy.pese.framework.main.general.PesimisenBiologisetRajatUtil;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.main.general.data._Table;
import fi.hy.pese.pesakortti.main.app.PesakorttiConst;
import fi.hy.pese.pesakortti.main.functionality.validation.PesakorttiTarkastusValidator;
import fi.luomus.commons.tipuapi.TipuAPIClient;
import fi.luomus.commons.utils.CoordinateConverter;
import fi.luomus.commons.xml.Document.Node;

public class PesakorttiTarkastusFunctionality extends TarkastusFunctionality {
	
	public PesakorttiTarkastusFunctionality(_Request<_PesaDomainModelRow> request) {
		super(request);
	}
	
	@Override
	public void preProsessingHook() throws Exception {
		super.preProsessingHook();
		boolean poikasvaihe = false;
		for (_Table kaynti : request.data().updateparameters().getKaynnit()) {
			if (kaynti.hasValues() || given(data.get("PesinnanVaihe."+kaynti.getIndex()))) {
				poikasvaihe = pesinnanVaiheToSeparatedColumnsFromData(poikasvaihe, kaynti, kaynti.getIndex());
				_Column minutes = kaynti.get("klo_min");
				if (minutes.getValue().length() == 1) {
					minutes.setValue("0" + minutes.getValue());
				}
			}
		}
		if (!data.contains("mode")) {
			data.set("mode", "");
		}
		_Column koordTyyppi = request.data().updateparameters().getColumn("pesa.koord_ilm_tyyppi");
		if (koordTyyppi.getValue().equals("NULL")) {
			koordTyyppi.clearValue();
		}
	}
	
	protected boolean pesinnanVaiheToSeparatedColumnsFromData(boolean poikasvaihe, _Table kaynti, int i) {
		String value = data.get("PesinnanVaihe."+i).trim();
		if (value.length() < 1) return poikasvaihe;
		try {
			Integer ika = Integer.valueOf(value);
			if (poikasvaihe) {
				kaynti.get("ika_poikaset").setValue(ika);
			} else {
				if (kaynti.get("lkm_poikaset").hasValue() || kaynti.get("lkm_poikaset_kuolleet").hasValue()) {
					kaynti.get("ika_poikaset").setValue(ika);
					return true;
				}
				kaynti.get("ika_munat").setValue(ika);
				return false;
			}
		} catch (NumberFormatException e) {
			if (!poikasvaihe && value.startsWith("P")) {
				poikasvaihe = true;
			}
			kaynti.get("pesinnan_vaihe").setValue(value);
		}
		return poikasvaihe;
	}
	
	@Override
	public void postProsessingHook() throws Exception {
		super.postProsessingHook();
		for (_Table kaynti : request.data().updateparameters().getKaynnit()) {
			if (!kaynti.hasValues()) continue;
			pesinnanVaiheToDataFromSeparatedColumns(kaynti.getIndex(), kaynti);
			_Column min = kaynti.get("klo_min");
			if (min.getValue().length() == 1) {
				min.setValue("0" + min.getValue());
			}
		}
	}
	
	private void pesinnanVaiheToDataFromSeparatedColumns(int i, _Table kaynti) {
		_Column pesinnanVaihe = kaynti.get("pesinnan_vaihe");
		_Column ika_munat = kaynti.get("ika_munat");
		_Column ika_poikaset = kaynti.get("ika_poikaset");
		if (ika_poikaset.hasValue()) {
			data.set("PesinnanVaihe."+i, ika_poikaset.getValue());
		} else if (ika_munat.hasValue()) {
			data.set("PesinnanVaihe."+i, ika_munat.getValue());
		} else {
			data.set("PesinnanVaihe."+i, pesinnanVaihe.getValue());
		}
	}
	
	
	@Override
	public _Validator validator() {
		try {
			List<_Table> results = dao.returnTable("ponttoalue");
			Map<Integer, _Table> ponttoalueet = new HashMap<>();
			for (_Table t : results) {
				ponttoalueet.put(t.getUniqueNumericIdColumn().getIntegerValue(), t);
			}
			return new PesakorttiTarkastusValidator(request, ponttoalueet, PesimisenBiologisetRajatUtil.getInstance(config));
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public void applicationSpecificDatamanipulation() throws Exception {
		String action = data.action();
		if (action.equals(Const.FILL_FOR_INSERT)) {
			selectWhatResultToUseForThisTarkYear();
			PesakorttiConst.removeTarkastusValuesButKeepSpecific(data.updateparameters().getTarkastus());
			moveTarkVuosiFromDataToUpdateparameters();
		} else if (action.equals(Const.INSERT_FIRST) || action.equals(Const.INSERT) || action.equals(Const.UPDATE)) {
			preInsertUpdate();
		}
	}
	
	private void moveTarkVuosiFromDataToUpdateparameters() {
		_Column tark_vuosi = data.updateparameters().getTarkastus().get("vuosi");
		tark_vuosi.setValue(data.get("tark_vuosi"));
	}
	
	private void selectWhatResultToUseForThisTarkYear() throws Exception {
		if (data.results().size() < 1) throw new Exception("No results");
		int tark_vuosi = Integer.valueOf(data.get("tark_vuosi"));
		_PesaDomainModelRow useAsBase = null;
		for (_PesaDomainModelRow row : data.results()) {
			useAsBase = row;
			int row_tark_vuosi = Integer.valueOf(row.getTarkastus().get("vuosi").getValue());
			if (tark_vuosi > row_tark_vuosi) break;
		}
		data.setUpdateparameters(data.copyOf(useAsBase));
	}
	
	private void preInsertUpdate() throws Exception, SQLException {
		convertCoordinates();
		setIds();
	}
	
	private void setIds() throws SQLException {
		String action = data.action();
		_PesaDomainModelRow params = data.updateparameters();
		String pesa_id = null;
		String tarkastus_id = null;
		if (action.equals(Const.INSERT_FIRST)) {
			pesa_id = dao.returnAndSetNextId(params.getPesa());
			tarkastus_id = dao.returnAndSetNextId(params.getTarkastus());
		} else if (action.equals(Const.INSERT)) {
			pesa_id = params.getPesa().getUniqueNumericIdColumn().getValue();
			tarkastus_id = dao.returnAndSetNextId(params.getTarkastus());
		} else if (action.equals(Const.UPDATE)) {
			pesa_id = params.getPesa().getUniqueNumericIdColumn().getValue();
			tarkastus_id = params.getTarkastus().getUniqueNumericIdColumn().getValue();
		}
		params.getTarkastus().get("pesa").setValue(pesa_id);
		
		for (_Table kaynti : params.getKaynnit().values()) {
			if (kaynti.hasValues()) {
				kaynti.get("tarkastus").setValue(tarkastus_id);
			}
		}
		
		for (_Table poikanen : params.getPoikaset().values()) {
			if (poikanen.hasValues()) {
				poikanen.get("tarkastus").setValue(tarkastus_id);
			}
		}
		
		for (_Table aikuinen : params.getAikuiset().values()) {
			if (aikuinen.hasValues()) {
				aikuinen.get("tarkastus").setValue(tarkastus_id);
			}
		}
		
		for (_Table muna : params.getMunat().values()) {
			if (muna.hasValues()) {
				muna.get("tarkastus").setValue(tarkastus_id);
			}
		}
	}
	
	protected void convertCoordinates() throws Exception {
		_Table pesa = data.updateparameters().getPesa();
		if (!pesa.get("koord_ilm_tyyppi").hasValue()) {
			coordsByKunta(pesa);
			return;
		}
		CoordinateConverter converter = new CoordinateConverter();
		switch (pesa.get("koord_ilm_tyyppi").getValue().charAt(0)) {
			case 'Y':
				converter.setYht_leveys(pesa.get("yht_leveys").getValue());
				converter.setYht_pituus(pesa.get("yht_pituus").getValue());
				break;
			case 'E':
				converter.setEur_leveys(pesa.get("eur_leveys").getValue());
				converter.setEur_pituus(pesa.get("eur_pituus").getValue());
				break;
		}
		converter.convert();
		pesa.get("yht_leveys").setValue(converter.getYht_leveys().toString());
		pesa.get("yht_pituus").setValue(converter.getYht_pituus().toString());
		pesa.get("eur_leveys").setValue(converter.getEur_leveys().toString());
		pesa.get("eur_pituus").setValue(converter.getEur_pituus().toString());
		pesa.get("ast_leveys").setValue(converter.getAst_leveys().toString());
		pesa.get("ast_pituus").setValue(converter.getAst_pituus().toString());
		pesa.get("des_leveys").setValue(converter.getDes_leveys().toString());
		pesa.get("des_pituus").setValue(converter.getDes_pituus().toString());
	}
	
	protected void coordsByKunta(_Table pesa) throws Exception {
		String kuntalyh = pesa.get("kunta").getValue();
		Node kunta = request.getTipuApiResource(TipuAPIClient.MUNICIPALITIES).getById(kuntalyh);
		if (kunta == null) throw new IllegalStateException("No such kunta: " + kuntalyh);
		
		pesa.get("des_leveys").setValue(kunta.getAttribute("centerpoint-lat"));
		pesa.get("des_pituus").setValue(kunta.getAttribute("centerpoint-lon"));
		pesa.get("koord_tarkkuus").setValue(kunta.getAttribute("radius") + "000");
		
		CoordinateConverter coordConverter = new CoordinateConverter();
		coordConverter.setDes_leveys(pesa.get("des_leveys").getDoubleValue());
		coordConverter.setDes_pituus(pesa.get("des_pituus").getDoubleValue());
		
		coordConverter.convert();
		pesa.get("yht_leveys").setValue(coordConverter.getYht_leveys().toString());
		pesa.get("yht_pituus").setValue(coordConverter.getYht_pituus().toString());
		pesa.get("eur_leveys").setValue(coordConverter.getEur_leveys().toString());
		pesa.get("eur_pituus").setValue(coordConverter.getEur_pituus().toString());
		pesa.get("ast_leveys").setValue(coordConverter.getAst_leveys().toString());
		pesa.get("ast_pituus").setValue(coordConverter.getAst_pituus().toString());
		
		pesa.get("koord_ilm_tyyppi").setValue("Y");
		pesa.get("koord_mittaustapa").setValue("X");
	}
	
	@Override
	protected void doInsertFirst() throws Exception {
		_PesaDomainModelRow params = data.updateparameters();
		dao.executeInsert(params.getPesa());
		dao.executeInsert(params.getTarkastus());
		insertKaynnit(params);
		insertPoikaset(params);
		insertAikuiset(params);
		insertMunat(params);
	}
	
	@Override
	protected void doInsert() throws Exception {
		_PesaDomainModelRow params = data.updateparameters();
		updateIfChanges(params.getPesa());
		dao.executeInsert(params.getTarkastus());
		insertKaynnit(params);
		insertPoikaset(params);
		insertAikuiset(params);
		insertMunat(params);
	}
	
	@Override
	protected void doUpdate() throws Exception {
		_PesaDomainModelRow params = data.updateparameters();
		updateIfChanges(params.getPesa());
		updateIfChanges(params.getTarkastus());
		updateKaynnit(params);
		updatePoikaset(params);
		updateAikuiset(params);
		updateMunat(params);
	}
	
	
	private void insertMunat(_PesaDomainModelRow params) throws SQLException {
		for (_Table muna : params.getMunat().values()) {
			if (muna.hasValues()) {
				muna.get("laji").setValue(params.getTarkastus().get("pesiva_laji"));
				fetchIdAndInsert(muna);
			}
		}
	}
	
	@Override
	protected boolean prevOlosuhdeIsForThisYear(_Table currentTarkastus, _Table prevOlosuhde) {
		throw new UnsupportedOperationException("Olosuhde table not used by pesakortti-app");
	}
	
	
}
