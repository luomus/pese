<!DOCTYPE html 
  PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="${system.language}">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
     
	<title>${texts["title_"+system.page]}</title>
	
	<link rel="stylesheet" href="${system.commonURL}/lintuvaara_shared_layout.css?${system.last_update_timestamp}" type="text/css" />
	<link rel="stylesheet" href="${system.commonURL}/lintuvaara_small_header.css?${system.last_update_timestamp}" type="text/css" />
	<link rel="stylesheet" href="${system.baseURL}/style.css?${system.last_update_timestamp}" type="text/css" />
	
	<script type="text/javascript" src="${system.commonURL}/jquery-latest.js?${system.last_update_timestamp}"></script>
	<script type="text/javascript" src="${system.commonURL}/jquery-ui/js/jquery-ui-1.10.4.min.js?${system.last_update_timestamp}"></script>
	<link rel="stylesheet" href="${system.commonURL}/jquery-ui/css/jquery-ui-1.10.4.min.css?${system.last_update_timestamp}" type="text/css" />
	
	<script type="text/javascript" src="${system.commonURL}/pese.js?${system.last_update_timestamp}"></script>
	
	<link rel="stylesheet" href="${system.commonURL}/pese_lomakevastaanottajat.css?${system.last_update_timestamp}" type="text/css" />
	
	<script type="text/javascript" src="${system.commonURL}/jquery.tablesorter.min.js"></script> 
	<script type="text/javascript" src="${system.commonURL}/jquery.metadata.js"></script>
    <script type="text/javascript"> 
    	jQuery(document).ready(function(){
    		$("#resultsTable").tablesorter();
    	}); 
    </script>
    
    <link rel="stylesheet" href="${system.commonURL}/chosen.css" type="text/css" media="screen, print" charset="utf-8" />
 	<script type="text/javascript" src="${system.commonURL}/chosen.jquery.js"></script>
  	<script type="text/javascript">
		jQuery(document).ready(function(){
			jQuery(".chosen").chosen({ search_contains: true });
		});
  	</script>
  	
</head> 

<body>

<div id="header">
    <div id="mainlogo"> 
        <img src="${system.baseURL}/img/lintuvaara.gif" alt="Lintuvaaraan" />  
          <#if system.StagingMode>     <span style="color: red; font-weight: bold; font-size: 200%">TESTIYMPÄRISTÖ</span></#if>
          <#if system.DevelopmentMode> <span style="color: red; font-weight: bold; font-size: 200%">KEHITYSYMPÄRISTÖ</span></#if>
    </div>
    
    <div id="lintuvaaraLink"> 
        <a href="${data.lintuvaaraURL}"> LINTUVAARA </a> 
    </div>
    
    <div id="infoContainer">
      
      <div id="info">                  
          <ul id="infoTabs">
              <li><a href="${system.baseURL}?page=logout">Kirjaudu ulos</a></li>
              <li id="currentUserInfo">Tervetuloa, ${system.user_name!system.user_id}</li>
          </ul>
      </div>
    </div>
  </div>
  			
<div id="historyActions" class="hidden">
  <#if history_actions?has_content>
    <a href="#"> Sulje  <span class="icon">X</span> </a>
    <table>
      <caption> Viimeksi käsitellyt pesät</caption>
      <tr> 
        <th> Aika </th>
        <th> Käyttäjä </th>
        <th> Toiminto </th>
        <th colspan="2"> Pesä </th>
        <th> Vuosi </th>
      </tr>
    <#list history_actions as action>
      <tr>
        <td> ${action.date} </td>
        <td> ${action.user} </td>
        <td> ${texts["history_actionlist_text_" + action.action]} </td>
        <td style="text-align: center;"> ${action.row.pesa.pesa_id} </td>
        <td> ${action.row.pesa.tarkka_sijainti}, ${action.row.pesa.pesanimi}  </td>
        <td> <a href="${system.baseURL}?page=tarkastus&amp;action=fill_for_update&amp;updateparameters.pesatarkastus.p_tarkastus_id=${action.row.tarkastus.p_tarkastus_id}" class="button-link"> ${action.row.tarkastus.tark_vuosi} </a>  </td>
      </tr>
    </#list>
    </table>
  <#else>
    <a href="#"> Sulje  <span class="icon">X</span> </a>  <br />
    Ei mitään näytettävää. (Ehkä järjestelmä on juuri käynnistetty uudelleen)
  </#if>
  
</div>

<div id="historyActions_show">
  <a href="#"> Viimeksi käsitellyt pesät <span class="icon">+</span> </a>
</div>

<script type="text/javascript">
    $("#historyActions").hide();
    $("#historyActions_show").click(function () {
      $(this).hide();
      $("#historyActions").show('fast');
      return true;
    });
    $("#historyActions").click(function () {
      $(this).hide();
      $("#historyActions_show").show('fast');
      return true;
    });
</script>


<table border="2" cellpadding="4" cellspacing="8" align="center">
	<tr>
		<td><a href="${system.baseURL}?page=search">${texts.menu_search}</a></td>
  	    <td><a href="${system.baseURL}?page=kirjekyyhky_management">ETEINEN (${data.kirjekyyhky_count})</a></td>
		<td><a href="${system.baseURL}?page=insert_first">${texts.menu_insert_first}</a></td>
		<td><a href="${system.baseURL}?page=tablemanagement&amp;table=reviiri">${texts.menu_reviirit}</a></td>
		<td><a href="${system.baseURL}?page=pesinnat">POISTOT</a></td>
		<td><a href="${system.baseURL}?page=filemanagement&amp;folder=report">${texts.menu_filemanagement}</a></td>
		<td><a href="${system.baseURL}?page=reports">${texts.menu_report}</a></td>
		<td><a href="${system.baseURL}?page=aputiedot_menu">APUTIEDOT</a></td>
		<td><a href="${system.baseURL}?page=logout">${texts.menu_logout}</a></td>
	</tr>
</table>



<br /><br />