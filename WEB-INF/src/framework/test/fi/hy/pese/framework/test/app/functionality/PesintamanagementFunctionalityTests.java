package fi.hy.pese.framework.test.app.functionality;

import java.io.FileNotFoundException;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fi.hy.pese.framework.main.app.BaseFactoryForApplicationSpecificationFactory;
import fi.hy.pese.framework.main.app.FunctionalityExecutor;
import fi.hy.pese.framework.main.app.Request;
import fi.hy.pese.framework.main.app.RequestImpleData;
import fi.hy.pese.framework.main.app._Functionality;
import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.app.functionality.PesintamanagementFunctionality;
import fi.hy.pese.framework.main.app.functionality.validate.PesintamanagementValidator;
import fi.hy.pese.framework.main.app.functionality.validate._Validator;
import fi.hy.pese.framework.main.db._DAO;
import fi.hy.pese.framework.main.db._ResultSet;
import fi.hy.pese.framework.main.db._ResultsHandler;
import fi.hy.pese.framework.main.db.oraclesql.OracleSQL_DAO;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data.Data;
import fi.hy.pese.framework.main.general.data.DatabaseTableStructure;
import fi.hy.pese.framework.main.general.data.PesaDomainModelDatabaseStructure;
import fi.hy.pese.framework.main.general.data.PesaDomainModelRowFactory;
import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.main.general.data._RowFactory;
import fi.hy.pese.framework.main.general.data._Table;
import fi.luomus.commons.containers.Selection;
import fi.luomus.commons.db.connectivity.TransactionConnection;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class PesintamanagementFunctionalityTests {

	public static Test suite() {
		return new TestSuite(PesintamanagementFunctionalityTests.class.getDeclaredClasses());
	}

	public static class WhenUsingPesintamangementFunctionality extends TestCase {

		private class PesintamanagementFunctTestFactory extends BaseFactoryForApplicationSpecificationFactory<_PesaDomainModelRow> {
			public PesintamanagementFunctTestFactory() throws FileNotFoundException {
				super();
			}

			@Override
			public _DAO<_PesaDomainModelRow> dao(String userId) throws SQLException {
				return new PesintamanagementFuncTestFakeDao(null, new PesaDomainModelRowFactory(structure, "", ""));
			}

			@Override
			public _Functionality<_PesaDomainModelRow> functionalityFor(String page, _Request<_PesaDomainModelRow> request) throws UnsupportedOperationException {
				return null;
			}

			@Override
			protected Map<String, Selection> fetchSelections(_DAO<_PesaDomainModelRow> dao) throws Exception {
				return null;
			}
		}

		private class PesintamanagementFuncTestFakeDao extends OracleSQL_DAO<_PesaDomainModelRow> {

			public PesintamanagementFuncTestFakeDao(TransactionConnection con, _RowFactory<_PesaDomainModelRow> rowFactory) {
				super(con, rowFactory);
			}

			@Override
			public String returnAndSetNextId(_Table table) {
				return "";
			}

			@Override
			public boolean checkRowExists(_Table table) throws SQLException {
				if (table.getFullname().equals("pesa")) {
					if (table.getUniqueNumericIdColumn().getValue().equals("1")) {
						return true;
					}
					return false;
				}
				throw new SQLException("not defined");
			}

			@Override
			public void executeSearch(_PesaDomainModelRow row, _ResultsHandler handler, String... additional) throws SQLException {
				if (row.getPesa().getUniqueNumericIdColumn().getValue().equals("1")) {
					handler.process(new _ResultSet() {
						private int i = 0;
						@Override
						public boolean next() throws SQLException {
							return i++ < 1;
						}

						@Override
						public String getString(String columnLabel) throws SQLException {
							return "1";
						}

						@Override
						public String getString(int i) throws SQLException {
							return "1";						}

						@Override
						public Date getDate(String columnLabel) throws SQLException {
							return null;
						}

						@Override
						public Date getDate(int i) throws SQLException {
							return null;
						}
					});
				}
				if (row.getTarkastus().getUniqueNumericIdColumn().getValue().equals("1000")) {
					handler.process(new _ResultSet() {
						private int i = 0;
						@Override
						public boolean next() throws SQLException {
							return i++ < 1;
						}

						@Override
						public String getString(String columnLabel) throws SQLException {
							return null;
						}

						@Override
						public String getString(int i) throws SQLException {
							return "1000";
						}

						@Override
						public Date getDate(String columnLabel) throws SQLException {
							return null;
						}

						@Override
						public Date getDate(int i) throws SQLException {
							return null;
						}
					});
				}
			}

			public Set<String> deletedNests = new HashSet<>();
			public Set<String> deletedInspections = new HashSet<>();
			@Override
			public void executeDelete(_Table table) throws SQLException {
				if (table.getName().equals("pesa")) {
					deletedNests.add(table.getUniqueNumericIdColumn().getValue());
				}
				if (table.getName().equals("tarkastus")) {
					deletedInspections.add(table.getUniqueNumericIdColumn().getValue());
				}
			}

			public List<_Table> insertedTables = new ArrayList<>();

			@Override
			public void executeInsert(_Table table) throws SQLException {
				if (table.getName().equals(structure.getLogTable().getName())) {
					insertedTables.add(table);
				}
			}
		}

		private PesintamanagementFunctionality functionality;
		private Request<_PesaDomainModelRow> request;
		private Data<_PesaDomainModelRow> data;
		private PesaDomainModelDatabaseStructure structure = init();
		private PesintamanagementFuncTestFakeDao dao;
		HashMap<String, String> uiTexts = initTexts();

		private PesaDomainModelDatabaseStructure init() {
			structure = new PesaDomainModelDatabaseStructure();
			DatabaseTableStructure pesa = new DatabaseTableStructure("pesa");
			pesa.addColumn("id", _Column.INTEGER, 5);
			pesa.get("id").setTypeToUniqueNumericIncreasingId();
			DatabaseTableStructure tarkastus = new DatabaseTableStructure("tarkastus");
			tarkastus.addColumn("id", _Column.INTEGER, 5);
			tarkastus.get("id").setTypeToUniqueNumericIncreasingId();
			DatabaseTableStructure kaynti = new DatabaseTableStructure("kaynti");
			kaynti.addColumn("id", _Column.INTEGER, 5);
			kaynti.get("id").setTypeToUniqueNumericIncreasingId();
			structure.setPesa(pesa);
			structure.setTarkastus(tarkastus);
			structure.setKaynti(kaynti);

			DatabaseTableStructure loki = new DatabaseTableStructure("muutosloki");
			loki.addColumn("id", _Column.INTEGER, 22);
			loki.addColumn("pesa", _Column.INTEGER, 22);
			loki.addColumn("kirj_pvm", _Column.DATE);
			loki.addColumn("muutos_pvm", _Column.DATE);
			loki.addColumn("kayttaja", _Column.VARCHAR, 120);
			loki.addColumn("kirjaus", _Column.VARCHAR, 500);
			loki.get("id").setTypeToUniqueNumericIncreasingId();
			structure.setLogTable(loki);
			return structure;
		}

		@Override
		public void setUp() throws Exception {
			data = new Data<>(new PesaDomainModelRowFactory(structure, "", ""));
			PesintamanagementFunctTestFactory factory = new PesintamanagementFunctTestFactory();
			RequestImpleData<_PesaDomainModelRow> reqData = new RequestImpleData<>();
			reqData.setData(data).setDao(factory.dao("userid")).setApplication(factory);
			request = new Request<>(reqData);
			functionality = new PesintamanagementFunctionality(request);
			dao = ((PesintamanagementFuncTestFakeDao) request.dao());
			data.setPage(Const.PESINTAMANAGEMENT_PAGE);
			data.setUserName("EP");
			data.setUITexts(uiTexts);
		}

		private HashMap<String, String> initTexts() {
			HashMap<String, String> uiTexts = new HashMap<>();
			uiTexts.put(Const.DELETE_NEST, "Pesä ja kaikki sen tiedot poistettu");
			uiTexts.put(Const.DELETE_TARKASTUKSET, "Pesän <id> vuoden <vuosi> tarkastus poistettu");
			uiTexts.put(Const.MOVE_TARKASTUKSET, "Pesän <id> vuoden <vuosi> tarkastus siiretty pesään <id2>");
			uiTexts.put(Const.MOVE_TARKASTUKSET_TO_NEW, "Pesän <id> vuoden <vuosi> tarkastus siiretty pesään uuteen pesään jonka ID:ksi tuli <id2>");
			return uiTexts;
		}

		public void test___when_selecting_nest_accepts_only_integers() throws Exception {
			data.setAction(Const.SELECT_NEST);
			data.updateparameters().getPesa().getUniqueNumericIdColumn().setValue("a");
			FunctionalityExecutor.execute(functionality);
			assertEquals(_Validator.MUST_BE_INTEGER, error("pesa.id"));
		}

		public void test___when_selecting_nest_nest_id_must_be_given() throws Exception {
			data.setAction(Const.SELECT_NEST);
			data.updateparameters().getPesa().getUniqueNumericIdColumn().setValue("");
			FunctionalityExecutor.execute(functionality);
			assertEquals(_Validator.CAN_NOT_BE_NULL, error("pesa.id"));
		}

		private String error(String field) {
			return data.errors().get(field);
		}

		public void test___when_selecting_nest_nest___it_must_exist() throws Exception {
			data.setAction(Const.SELECT_NEST);
			data.updateparameters().getPesa().getUniqueNumericIdColumn().setValue("1");
			FunctionalityExecutor.execute(functionality);
			assertTrue("Should not have errors", data.errors().size() == 0);
		}

		public void test___when_selecting_nest_nest___it_must_exist2() throws Exception {
			data.setAction(Const.SELECT_NEST);
			data.updateparameters().getPesa().getUniqueNumericIdColumn().setValue("2");
			FunctionalityExecutor.execute(functionality);
			assertEquals(_Validator.DOES_NOT_EXIST, error("pesa.id"));
		}

		public void test___when_deleting_nest() throws Exception {
			data.setAction(Const.DELETE_NEST);
			data.updateparameters().getPesa().getUniqueNumericIdColumn().setValue("1");
			FunctionalityExecutor.execute(functionality);
			assertTrue("nest is deleted", dao.deletedNests.contains("1"));
			_Table logEntry = dao.insertedTables.get(0);
			assertEquals("1", logEntry.get("pesa").getValue());
			assertEquals("EP", logEntry.get("kayttaja").getValue());
			assertEquals("Pesä ja kaikki sen tiedot poistettu", logEntry.get("kirjaus").getValue());
		}

		public void test___does_not_allow_to_delete_all_inspections_of_a_nest() throws Exception {
			data.setAction(Const.DELETE_TARKASTUKSET);
			data.updateparameters().getPesa().getUniqueNumericIdColumn().setValue("1");
			data.updateparameters().getTarkastus().getUniqueNumericIdColumn().setValue("1000");
			FunctionalityExecutor.execute(functionality);
			assertEquals(PesintamanagementValidator.CAN_NOT_DELETE_ALL_TARKASTUKSET, error("tarkastus.id"));
			assertTrue(dao.deletedInspections.isEmpty());
		}

		// TODO  testejä puuttu monia

		//		public void test___when_deleting_inspecionts() throws Exception {
		//			data.setAction(Const.DELETE_TARKASTUKSET);
		//			data.updateparameters().getPesa().getUniqueNumericIdColumn().setValue("2");
		//			data.updateparameters().getTarkastus().getUniqueNumericIdColumn().setValue("1000");
		//			FunctionalityExecutor.execute(functionality);
		//			Utils.debug(data.errors());
		//			assertTrue("tarkastus is deleted", dao.deletedInspections.contains("1000"));
		//			_Table logEntry = dao.insertedTables.get(0);
		//			assertEquals("1", logEntry.get("pesa").getValue());
		//			assertEquals("EP", logEntry.get("kayttaja").getValue());
		//			assertEquals("Pesän 1 vuoden 2010 tarkastus poistettu", logEntry.get("kirjaus").getValue());
		//		}

	}

}
