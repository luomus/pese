package fi.hy.pese.framework.main.general.data;

import java.util.Collection;
import java.util.Comparator;

public interface _IndexedTablegroup extends Iterable<_Table> {

	/**
	 * Note index starts at 1
	 * @param i index - starts at 1
	 * @return
	 */
	_Table get(int i);

	Collection<_Table> values();

	void clearAllValues();

	int getStaticCount();

	String getBasetablename();

	_Table getBasetable();

	boolean hasValues();

	_Table getNext();

	void sortUsing(Comparator<_Table> comparator);

}
