package fi.hy.pese.merikotka.main.functionality.reports;

import java.util.ArrayList;
import java.util.List;

import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.general.ReportWriter;
import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.main.general.data._Table;
import fi.hy.pese.merikotka.main.app.MerikotkaConst;
import fi.luomus.commons.tipuapi.TipuAPIClient;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.Utils;

/**
 * Example:
 *
 AJETTU: |24.05.2010||PARAMETREILLA: |tark_vuosi:|2000-2006|suuralue:|E|
 * r_kunta|reviiri_id|reviiri|pesa_id|pesa|yht_leveys|yht_pituus|kunto|2006|2005|2004|2003|2002|2001|2000|
 * KUUSAM|79|Laajusjärvi||||||1|2|2|||||
 * |||555|Laajusjärvi I|7342399|3621485|||U||||||
 * |||593|Laajusjärvi II|7342345|3621833|P|A|U||||||
 * |||112|Laajusjärvi III|7343798|3620618|T|A|2|2|||||
 * |||676|Laajusjärvi IV|7342680|3622204|P|1|||||||
 * KUUSAM|73|Nuottivaara||||||1|A|K|||||
 * |||700|Kokkosuo 1|7328601|3617656|K|1|||||||
 * |||104|Nuottivaara 1|7327155|3620625|P|A|A|K|||||
 * KUUSAM|324|Papuvaara|||||||A||||||
 * |||592|Papuvaara I|7331001|3625153|||A||||||
 * KUUSAM|348|Pesosjärvi||||||1|||||||
 * |||759|Pesosjärvi|7357800|3612300|P|1|||||||
 * KUUSAM|74|Porosaari||||||1|A|1|1|2|||
 * |||105|Porosaari 1|7315920|3605160|A|A|A|1|1|2|||
 * |||701|Raatelampi II|7317580|3607823|P|1|||||||
 * POSIO|78|Hankaanjärvet||||||3|1|2|||||
 * |||111|Hankaanjärvet I (Tälvänsuo 1)|7318144|3554757|P|U|A|2|||||
 * |||511|Hankaanjärvet II|7318214|3554925|P|3|1||||||
 * RANUA|75|Tervonkangas||||||A|A|A|A||||
 * |||106|Tervonsuo 1|7301673|3467970|A|A|A|A|A||||
 * SSALMI|321|Säynäjäsuo||||||1|M||||||
 * |||581|Säynäjäsuo 1|7190490|3599813|P|1|M||||||
 * TAIVAL|72|Kulmalampi||||||A|A|A|A||||
 * |||486|Kulmalamminkukkula 1|7286754|3535820|P|A|A||A||||
 * |||103|Mulkkuvaara|7286890|3537760|A|A|A|A|A||||
 * |||485|Soidinvaara|7288140|3536820|P|A|A||A||||
 * TAIVAL|71|Mustavaara||||||M|1|1|K|1||1|
 * |||487|Mustavaara 1|7300460|3550290|P|A|A||A||||
 * |||488|Mustavaara 2|7300596|3550349|T|A|A||A||||
 * |||489|Valkealehto 1|7302030|3555110|T|A|A||A|||1|
 * |||102|Valkealehto 2|7302920|3555160|P|M|1|1|K|1|||
 */
public class MerikotkaReport_pesimistulostenYhteenvetolista extends ReportWriter<_PesaDomainModelRow> {
	
	private static final String[]	FIELDS	=
		{
		 "vuosi.reviirin_kunta", "reviiri.reviiri_id", "reviiri.reviirinimi",
		 "pesavakio.pesa_id", "pesavakio.pesanimi",
		 "pesavakio.yht_leveys", "pesavakio.yht_pituus",
		 "pesavakio.eur_leveys", "pesavakio.eur_pituus",
		 "pesatarkastus.pesa_kunto"
		};
	private final boolean viranomaisraportti;
	private final Integer current_year = DateUtils.getCurrentYear();
	
	public MerikotkaReport_pesimistulostenYhteenvetolista(_Request<_PesaDomainModelRow> request, String filenamePrefix) {
		super(request, filenamePrefix);
		viranomaisraportti = false;
	}
	
	public MerikotkaReport_pesimistulostenYhteenvetolista(_Request<_PesaDomainModelRow> request, String filenamePrefix, boolean viranomaisRaportti) {
		super(request, filenamePrefix);
		this.viranomaisraportti = viranomaisRaportti;
	}
	
	@Override
	public boolean produceSpecific() {
		List<Integer> years = yearsToPrint(request.data().searchparameters().getTarkastus().get("tark_vuosi"));
		if (years.isEmpty()) {
			request.data().errors().put(filenamePrefix, MerikotkaConst.INVALID_REPORT_YEARS);
			return false;
		}
		headerLine();
		newLine();
		newLine();
		columnDescriptions(years);
		newLine();
		rows(years);
		return true;
	}
	
	/**
	 * KUUSAM|374|Kuusijärvi||||||1|1| | |
	 * |||831|Kuusijärvi 1|7352019|3613878|P|1|1| | |
	 * KUUSAM|375|Käsmäjärvi||||||1|K| | |
	 * |||833|Lehtoaho (Liettilammet)|7298914|3587366|P|1|K| | |
	 * KUUSAM|79|Laajusjärvi||||||1|1|1|2|
	 * |||555|Laajusjärvi I|7342399|3621485|A|A|A| |U|
	 * |||593|Laajusjärvi II|7342345|3621833|A|A|A|A|U|
	 * |||112|Laajusjärvi III|7343798|3620618|A|U|A|A|2|
	 * |||676|Laajusjärvi IV|7342680|3622204|P|1|1|1| |
	 * @param years
	 */
	private void rows(List<Integer> years) {
		List<Reviiri> reviiriRows = new ArrayList<>();
		Reviiri currentReviiri = null;
		Pesa currentPesa = null;
		
		for (_PesaDomainModelRow row : request.data().results()) {
			String reviiri_id = row.getReviiri().getUniqueNumericIdColumn().getValue();
			String pesa_id = row.getPesa().getUniqueNumericIdColumn().getValue();
			if (reviiriChanges(currentReviiri, reviiri_id)) {
				currentReviiri = new Reviiri(row);
				reviiriRows.add(currentReviiri);
			}
			if (currentReviiri == null) throw new IllegalStateException();
			if (pesaChanges(currentPesa, pesa_id)) {
				currentPesa = currentReviiri.newPesa(row);
				
			}
			if (currentPesa == null) throw new IllegalStateException();
			currentPesa.addTarkastus(row.getTarkastus());
		}
		
		for (Reviiri reviiri : reviiriRows) {
			if (reviiri.pesarows().isEmpty()) continue;
			newLine();
			reviiriRow(years, reviiri);
			for (Pesa pesa : reviiri.pesarows()) {
				pesaRows(years, pesa, reviiri);
			}
		}
	}
	
	/**
	 * KUNTA|REVIIRI_ID|REVIIRI|PESA_ID|PESA|LEV|PIT|KUNTO|2008|2007|2006|2005|
	 * @param years
	 */
	private void columnDescriptions(List<Integer> years) {
		writeHeaders(FIELDS);
		for (Integer year : years) {
			write(year.toString());
			separator();
		}
		if (viranomaisraportti) {
			write("luokka"); separator();
			write("palsta"); separator();
			write("palsta"); separator();
			write("viimeisin");
		}
		newLine();
		separator(); write("id"); separator(2); write("id"); separator(7);
		separator(years.size());
		if (viranomaisraportti) {
			separator();
			write("rauh"); separator();
			write("omist"); separator();
			write("tarkastusv."); separator();
		}
	}
	
	private void pesaRows(List<Integer> years, Pesa pesa, Reviiri reviiri) {
		write(reviiri);
		write(pesa);
		for (Integer year : years) {
			write(pesa.pesimistulosForYear(year));
			separator();
		}
		if (viranomaisraportti) {
			write(pesa.luokka()); separator();
			write(pesa.palstaRauhoitus()); separator();
			write(pesa.palstaOmistaja()); separator();
			write(pesa.viimeisinTarkastusvuosi()); separator();
		}
		newLine();
	}
	
	private void reviiriRow(List<Integer> years, Reviiri reviiri) {
		write(reviiri);
		write("***");
		separator(7);
		for (Integer year : years) {
			write(bestPesimistulos(reviiri, year)); separator();
		}
		newLine();
	}
	
	private String bestPesimistulos(Reviiri reviiri, Integer year) {
		String bestPesimistulos = "";
		for (Pesa pesa : reviiri.pesarows()) {
			String thisPesimistulos = pesa.pesimistulosForYear(year);
			if (MerikotkaConst.firstPesimistulosBetterThanSecond(thisPesimistulos, bestPesimistulos)) {
				bestPesimistulos = thisPesimistulos;
			}
		}
		return bestPesimistulos;
	}
	
	private void write(Pesa pesa) {
		write(pesa.id());
		separator();
		write(pesa.nimi());
		separator();
		write(pesa.yht_lev());
		separator();
		write(pesa.yht_pit());
		separator();
		write(pesa.eur_lev());
		separator();
		write(pesa.eur_pit());
		separator();
		write(pesa.kunto());
		separator();
	}
	
	private void write(Reviiri reviiri) {
		write(reviiri.kunta());
		separator();
		write(reviiri.id());
		separator();
		write(reviiri.nimi());
		separator();
	}
	
	private boolean reviiriChanges(Reviiri currentReviiri, String reviiri_id) {
		return currentReviiri == null || !reviiri_id.equals(currentReviiri.id());
	}
	
	private boolean pesaChanges(Pesa currentPesa, String pesa_id) {
		return currentPesa == null || !pesa_id.equals(currentPesa.id());
	}
	
	private class Reviiri {
		private final List<Pesa>	pesarows	= new ArrayList<>();
		private final String		id;
		private final String		nimi;
		private final String		kunta;
		
		public Reviiri(_PesaDomainModelRow row) {
			this.id = row.getReviiri().getUniqueNumericIdColumn().getValue();
			this.nimi = row.getReviiri().get("reviirinimi").getValue();
			this.kunta = row.getVuosi().get("reviirin_kunta").getValue();
		}
		
		public Pesa newPesa(_PesaDomainModelRow row) {
			Pesa pesarow = new Pesa(row, this);
			pesarows.add(pesarow);
			return pesarow;
		}
		
		public List<Pesa> pesarows() {
			return pesarows;
		}
		
		public String id() {
			return id;
		}
		
		public String nimi() {
			return nimi;
		}
		
		public String kunta() {
			return request.data().selections().get(TipuAPIClient.MUNICIPALITIES).get(kunta);
		}
	}
	
	private class Pesa {
		private static final String	KORISTELTU	= "K";
		private static final String	ASUMATON	= "A";
		private static final int	KYMMENEN_VUOTTA	= 10;
		private static final int	VIISI_VUOTTA	= 5;
		private static final String	YKKÖSLUOKKA	= "HYVÄ";
		private static final String	KAKKOSLUOKKA	= "KESKINK.";
		private static final String	HYVIN_PUUSSA	= "P";
		private static final int	UUDEN_PESAN_RAJAUS	= 3;
		private final String		id;
		private final String		nimi;
		private final String		yht_lev;
		private final String		yht_pit;
		private final String		eur_lev;
		private final String		eur_pit;
		private final List<_Table>	tarkastukset	= new ArrayList<>();
		private final String 		palsta_rauhoitus;
		private final String		palsta_omistaja;
		private final _Table		pesa;
		private final Reviiri		reviiri;
		
		public Pesa(_PesaDomainModelRow row, Reviiri reviiri) {
			this.pesa = row.getPesa();
			this.id = pesa.getUniqueNumericIdColumn().getValue();
			this.nimi = pesa.get("pesanimi").getValue();
			this.yht_lev = pesa.get("yht_leveys").getValue();
			this.yht_pit = pesa.get("yht_pituus").getValue();
			this.eur_lev = pesa.get("eur_leveys").getValue();
			this.eur_pit = pesa.get("eur_pituus").getValue();
			String rauh = row.getOlosuhde().get("palsta_rauhoitus").getSelectionDesc(request.data().selections());
			String omist = row.getOlosuhde().get("palsta_omistaja").getSelectionDesc(request.data().selections());
			this.palsta_rauhoitus = Utils.trimToLength(rauh, 10);
			this.palsta_omistaja = Utils.trimToLength(omist, 10);
			this.reviiri = reviiri;
		}
		
		public String luokka() {
			// jos pesä on uusi, luokka = 1
			Integer perustamisvuosi = perustamisvuosi();
			if (perustamisvuosi != null) {
				if (perustamisvuosi >= current_year - UUDEN_PESAN_RAJAUS) {
					return YKKÖSLUOKKA;
				}
			}
			
			// muuten, jos pesä on reviirin ainoa,...
			if (reviirinPesienMaara() == 1) {
				if (pesimistulosLkmParempikuin(ASUMATON, VIISI_VUOTTA) >= 1) {
					return YKKÖSLUOKKA;
				}
				if (pesimistulosLkmParempikuin(ASUMATON, KYMMENEN_VUOTTA) >= 2) {
					if (kunto().equals(HYVIN_PUUSSA)) {
						return YKKÖSLUOKKA;
					}
				}
			}
			
			// muuten, jos pesän reviirillä on useita pesiä..
			if (pesimistulosLkmParempikuin(KORISTELTU, VIISI_VUOTTA) >= 1) {
				return YKKÖSLUOKKA;
			}
			if (pesimistulosLkmParempikuin(KORISTELTU, KYMMENEN_VUOTTA) >= 2) {
				if (kunto().equals(HYVIN_PUUSSA)) {
					return YKKÖSLUOKKA;
				}
			}
			return KAKKOSLUOKKA;
		}
		
		private int pesimistulosLkmParempikuin(String parempiKuin, int vuosirajaus) {
			int varmojenPesintojenMaara = 0;
			for (_Table tarkastus : tarkastukset) {
				if (tarkVuosi(tarkastus) >= current_year - vuosirajaus) {
					if (MerikotkaConst.firstPesimistulosBetterThanSecond(tarkastus.get("pesimistulos").getValue(), parempiKuin)) {
						varmojenPesintojenMaara++;
					}
				}
			}
			return varmojenPesintojenMaara;
		}
		
		private Integer tarkVuosi(_Table tarkastus) {
			return Integer.valueOf(tarkastus.get("tark_vuosi").getValue());
		}
		
		public int reviirinPesienMaara() {
			return reviiri.pesarows().size();
		}
		
		private Integer perustamisvuosi() {
			Integer perustamisvuosi = null;
			_Column rak_v = pesa.get("rak_vuosi");
			_Column rak_v_tark = pesa.get("rak_vuosi_tark");
			_Column rak_v_teko = pesa.get("rak_vuosi_teko");
			_Column rak_v_teko_tark = pesa.get("rak_vuosi_teko_tark");
			String rak_vuosi = null;
			String tarkkuus = null;
			if (rak_v.hasValue() && rak_v_tark.hasValue()) {
				rak_vuosi = rak_v.getValue();
				tarkkuus = rak_v_tark.getValue();
			} else if (rak_v_teko.hasValue() && rak_v_teko_tark.hasValue()) {
				rak_vuosi = rak_v_teko.getValue();
				tarkkuus = rak_v_teko_tark.getValue();
			}
			if (rak_vuosi != null && tarkkuus != null) {
				int tarkkuuslisa;
				if (tarkkuus.equals("5") || tarkkuus.equals("4")) {
					tarkkuuslisa = 10;
				} else if (tarkkuus.equals("3")) {
					tarkkuuslisa = 5;
				} else {
					tarkkuuslisa = Integer.valueOf(tarkkuus);
				}
				perustamisvuosi = Integer.valueOf(rak_vuosi) + tarkkuuslisa;
			}
			return perustamisvuosi;
		}
		
		public String viimeisinTarkastusvuosi() {
			for (_Table tarkastus : tarkastukset) {
				String pesimistulos = tarkastus.get("pesimistulos").getValue();
				if (pesimistulos.equals("U") || pesimistulos.equals("")) continue;
				return tarkastus.get("tark_vuosi").getValue();
			}
			return "";
		}
		
		public String id() {
			return id;
		}
		
		public String nimi() {
			return nimi;
		}
		
		public String yht_lev() {
			return yht_lev;
		}
		
		public String yht_pit() {
			return yht_pit;
		}
		
		public String eur_pit() {
			return eur_pit;
		}
		
		public String eur_lev() {
			return eur_lev;
		}
		
		public String kunto() {
			for (_Table tarkastus : tarkastukset) {
				String kunto = tarkastus.get("pesa_kunto").getValue();
				if (!kunto.equals("")) { return kunto; }
			}
			return "";
		}
		
		public void addTarkastus(_Table tarkastus) {
			tarkastukset.add(tarkastus);
		}
		
		public String pesimistulosForYear(Integer year) {
			for (_Table tarkastus : tarkastukset) {
				Integer thisYear = tarkVuosi(tarkastus);
				if (year.equals(thisYear)) {
					String poikasCount = MerikotkaConst.poikasCount(tarkastus);
					if (poikasCount.equals("") || poikasCount.equals("0")) { return tarkastus.get("pesimistulos").getValue(); }
					return poikasCount;
				}
			}
			return "";
		}
		
		public String palstaRauhoitus() {
			return palsta_rauhoitus;
		}
		
		public String palstaOmistaja() {
			return palsta_omistaja;
		}
	}
	
}
