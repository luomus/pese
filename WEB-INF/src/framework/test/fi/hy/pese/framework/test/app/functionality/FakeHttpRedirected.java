package fi.hy.pese.framework.test.app.functionality;

import fi.luomus.commons.http.HttpStatus;

public class FakeHttpRedirected extends HttpStatus {

	private boolean statusWritten;
	public String redirectedTo;

	public FakeHttpRedirected() {
		super(null);
	}

	/**
	 * Redirects the request to the uri with status 302
	 * @param uri
	 */
	@Override
	public void redirectTo(String uri) {
		redirectTo(uri, 302);
	}

	@Override
	public void redirectTo(String uri, int status) {
		status(status);
		redirectedTo = uri;
	}

	/**
	 * Redirects the request to the uri with status 303
	 * @param uri
	 */
	@Override
	public void redirectTo303(String uri) {
		status(303);
	}

	/**
	 * Status 403
	 */
	@Override
	public void status403() {
		status(403);
	}

	/**
	 * Status 404
	 */
	@Override
	public void status404() {
		status(404);
	}

	/**
	 * Status 500
	 */
	@Override
	public void status500() {
		status(500);
	}

	/**
	 * Set status
	 * @param statusCode
	 */
	@Override
	public void status(int statusCode) {
		statusWritten = true;
	}

	@Override
	public boolean isHttpStatusCodeSet() {
		return statusWritten;
	}
	
}
