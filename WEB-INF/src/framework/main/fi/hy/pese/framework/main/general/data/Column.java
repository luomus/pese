package fi.hy.pese.framework.main.general.data;


import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fi.luomus.commons.containers.DateValue;
import fi.luomus.commons.containers.Selection;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.Utils;

public class Column implements _Column, Serializable {

	private static final long	serialVersionUID	= -8065802641751223797L;
	private final DatabaseColumnStructure	column;
	private final Map<String, String>		valueMap;
	private final Table parentTable;

	public Column(Table parentTable, DatabaseColumnStructure structure, Map<String, String> valueMap) {
		this.column = structure;
		this.valueMap = valueMap;
		this.parentTable = parentTable;
	}

	@Override
	public String getAssosiatedSelection() {
		return column.getAssosiatedSelection();
	}

	@Override
	public int getDatatype() {
		return column.getDatatype();
	}

	@Override
	public DateValue getDateValue() throws UnsupportedOperationException {
		exceptionIfNotDate();
		return DateUtils.convertToDateValue(getValue());
	}

	private void exceptionIfNotDate() {
		if (!columnIsDate()) throw new UnsupportedOperationException("Not a date column");
	}

	@Override
	public int getDecimalSize() throws UnsupportedOperationException {
		return column.getDecimalSize();
	}

	@Override
	public int getFieldType() {
		return column.getFieldType();
	}

	@Override
	public String getFullname() {
		return parentTable.getFullname() + "." + this.getName();
	}

	@Override
	public int getInputSize() {
		return column.getInputSize();
	}

	@Override
	public String getInputType() {
		return column.getInputType();
	}

	@Override
	public String getModifiability() {
		return column.getModifiability();
	}

	@Override
	public String getName() {
		return column.getName();
	}

	@Override
	public _ReferenceKey getReferenceColumn() {
		return column.getReferenceColumn();
	}

	@Override
	public Set<_ReferenceKey> getReferencingColumns() {
		return column.getReferencingColumns();
	}

	@Override
	public int getSize() {
		return column.getSize();
	}

	@Override
	public String getTablename() {
		return parentTable.getFullname();
	}

	@Override
	public String getDatabaseTablename() {
		return column.getTablename();
	}

	@Override
	public String getValue() {
		String value = valueMap.get(getName());
		value = format(value);
		return value;
	}

	private String format(String value) {
		if (value == null) {
			value = "";
		}
		if (columnIsRowid()) return value;
		if (!column.allowWhitespace() && column.getInputType() != _Column.INPUT_TYPE_TEXTAREA) {
			value = value.replaceAll("\\s+", " ").trim();
		}
		if (columnIsIntegerColumn()) return Utils.formatToNumber(value);
		if (columnIsDecimalColumn()) return Utils.formatToDecimal(value, getDecimalSize());
		if (columnIspasswordColumn()) return Utils.encrypt(value);
		if (columnIsVarcharColumn() && (columnisPartOfKey() || isUppercaseColumn())) return value.toUpperCase();
		return value;
	}

	@Override
	public List<String> getValueList() {
		return Utils.valuelist(this.getValue());
	}

	@Override
	public Double getDoubleValue() throws NumberFormatException {
		return Utils.formatToDouble(getValue());
	}

	@Override
	public Integer getIntegerValue() throws NumberFormatException {
		return Integer.valueOf(getValue());
	}

	@Override
	public boolean isUppercaseColumn() {
		return column.isUppercaseColumn();
	}

	@Override
	public void setValue(String value) {
		if (value == null) value = "";
		valueMap.put(getName(), value);
	}

	private boolean columnIsDecimalColumn() {
		return getDatatype() == _Column.DECIMAL;
	}

	private boolean columnIspasswordColumn() {
		return getFieldType() == _Column.PASSWORD_COLUMN;
	}

	private boolean columnIsRowid() {
		return getDatatype() == _Column.ROWID;
	}

	private boolean columnIsDate() {
		return getDatatype() == _Column.DATE;
	}

	private boolean columnIsIntegerColumn() {
		return getDatatype() == _Column.INTEGER;
	}

	private boolean columnisPartOfKey() {
		return getFieldType() == _Column.IMMUTABLE_PART_OF_A_KEY;
	}

	private boolean columnIsVarcharColumn() {
		return getDatatype() == _Column.VARCHAR;
	}

	@Override
	public String toString() {
		return getValue();
	}

	@Override
	public boolean hasValue() {
		return getValue().length() > 0;
	}

	@Override
	public String getUrlEncodedValue() {
		return Utils.urlEncode(this.getValue());
	}

	@Override
	public boolean isRequired() {
		return column.isRequired();
	}

	@Override
	public boolean hasAssosiatedSelection() {
		return column.hasAssosiatedSelection();
	}

	@Override
	public boolean isRowIdColumn() {
		return column.getFieldType() == _Column.ROWID;
	}

	@Override
	public double getMinValue() throws UnsupportedOperationException {
		return column.getMinValue();
	}

	@Override
	public double getMaxValue() throws UnsupportedOperationException {
		return column.getMaxValue();
	}

	@Override
	public void setValue(_Column column) {
		setValue(column.getValue());
	}

	@Override
	public void clearValue() {
		setValue("");
	}

	@Override
	public void setValue(Integer value) {
		if (value == null) {
			this.setValue("");
		} else {
			this.setValue(Integer.toString(value));
		}
	}

	@Override
	public void setValue(Double value) {
		if (value == null) {
			this.setValue("");
		} else {
			this.setValue(Double.toString(value));
		}
	}

	@Override
	public String getSelectionDesc(Map<String, Selection> selections) {
		return selections.get(this.getAssosiatedSelection()).get(this.getValue());
	}

	@Override
	public boolean kirjekyyhkyRequired() {
		return column.kirjekyyhkyRequired();
	}

	@Override
	public void appendValue(String value) {
		this.setValue(this.getValue() + " " + value);
	}

}
