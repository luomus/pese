package fi.hy.pese.framework.main.general.data;

import java.io.Serializable;

/**
 * Database reference; Column C in table T is referenced by ReferenceKey {'referringTable', 'referringTableColumn'}
 */
public class ReferenceKey implements _ReferenceKey, Serializable {

	private static final long	serialVersionUID	= 562804061470728669L;
	private final String	table;
	private final String	column;

	/**
	 * Reference key
	 * @param tableName name of table
	 * @param columnName name of column
	 */
	public ReferenceKey(String tableName, String columnName) {
		this.table = tableName;
		this.column = columnName;
	}

	public ReferenceKey(_Column column) {
		this.table = column.getDatabaseTablename();
		this.column = column.getName();
	}

	/**
	 * Name of the referring table
	 * @return
	 */
	@Override
	public String table() {
		return this.table;
	}

	/**
	 * Name of the referring table's column
	 * @return
	 */
	@Override
	public String column() {
		return this.column;
	}

	@Override
	public boolean equals(_Column c) {
		return c.getDatabaseTablename().equals(table) && c.getName().equals(column);
	}

	@Override
	public boolean equals(String s) {
		if (s == null) return false;
		return s.equals(this.toString());
	}

	@Override
	public String toString() {
		return table + "." + column;
	}
}
