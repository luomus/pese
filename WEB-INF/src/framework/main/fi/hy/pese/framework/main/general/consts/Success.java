package fi.hy.pese.framework.main.general.consts;

public class Success {
	
	public static final String	UPDATE					= "success_update";
	public static final String	INSERT					= "success_insert";
	public static final String	DELETE					= "success_delete";
	
	public static final String	TARKASTUS_UPDATE		= "success_tarkastus_update";
	public static final String	TARKASTUS_INSERT		= "success_tarkastus_insert";
	public static final String	TARKASTUS_INSERT_FIRST	= "success_tarkastus_insert_first";
	public static final String	TARKASTUS_DELETE		= "success_tarkastus_delete";
	public static final String	TARKASTUS_DELETE_MULTIPLE	= "success_tarkastus_delete_multiple";
	public static final String	TARKASTUS_MOVE			= "success_tarkastus_move";
	public static final String	TARKASTUS_MOVE_MULTIPLE		= "success_tarkastus_move_multiple";
	public static final String	TARKASTUS_MOVE_NEST_DELETE			= "success_tarkastus_move_nest_deleted";
	public static final String	TARKASTUS_MOVE_MULTIPLE_NEST_DELETE		= "success_tarkastus_move_multiple_nest_deleted";
	public static final String	PESA_DELETE				= "success_pesa_delete";
	
	public static final String	SAVED					= "success_saved";
	
}
