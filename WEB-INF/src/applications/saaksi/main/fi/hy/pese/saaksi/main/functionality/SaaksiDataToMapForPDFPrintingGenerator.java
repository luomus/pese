package fi.hy.pese.saaksi.main.functionality;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.app.functionality.SearchFunctionality;
import fi.hy.pese.framework.main.db.FirstResultRowToTableHandler;
import fi.hy.pese.framework.main.db.FirstResultRowToTableHandler.NoResultsException;
import fi.hy.pese.framework.main.db.RowsToListResultHandler;
import fi.hy.pese.framework.main.db._DAO;
import fi.hy.pese.framework.main.general._PDFDataMapGenerator;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.main.general.data._Table;
import fi.hy.pese.saaksi.main.app.SaaksiConst;
import fi.luomus.commons.containers.Selection;
import fi.luomus.commons.pdf.PDFWriter;
import fi.luomus.commons.tipuapi.TipuAPIClient;
import fi.luomus.commons.tipuapi.TipuApiResource;
import fi.luomus.commons.xml.Document.Node;

public class SaaksiDataToMapForPDFPrintingGenerator implements _PDFDataMapGenerator {
	
	private final _DAO<_PesaDomainModelRow>						dao;
	private final Map<String, Selection>	selections;
	private final SearchFunctionality		searchFunctionality;
	private final TipuApiResource ringers;
	
	public SaaksiDataToMapForPDFPrintingGenerator(_Request<_PesaDomainModelRow> request, SearchFunctionality searchFunctionality) {
		this.dao = request.dao();
		this.selections = request.data().selections();
		this.ringers = request.getTipuApiResource(TipuAPIClient.RINGERS);
		this.searchFunctionality = searchFunctionality;
	}
	
	/**
	 * This function is called to build the application specific data for PDF printing (esitäytetty tarkastuslomake).
	 */
	@Override
	public Map<String, String> tarkastusDataFor(String pesaid, String year) throws Exception {
		Map<String, String> map = new HashMap<>();
		tarkVuosiToWhereItIsNeeded(year, map);
		prevTarkastusOnlyInspectedToMap(pesaid, map);
		
		_PesaDomainModelRow previusYear = previousTarkastusFor(pesaid);
		headerFields(map, previusYear);
		coordinatesToMap(map, previusYear);
		seuraavaTarkastaja(map, previusYear.getPesa());
		
		measurementAccuracyPair(map, previusYear.getColumn("olosuhde.pesap_latvah_tark"));
		measurementAccuracyPair(map, previusYear.getColumn("olosuhde.pesap_latvay_tark"));
		measurementAccuracyPair(map, previusYear.getColumn("olosuhde.pesap_tyvih_tark"));
		measurementAccuracyPair(map, previusYear.getColumn("olosuhde.pesap_tyviy_tark"));
		
		if (previusYear.getColumn("olosuhde.rauhoitustaulu").getValue().equals("K")) {
			map.put("rauhoitustaulu", PDFWriter.SELECTED_CHECKBOX);
		}
		
		everythingFromRowToMap(map, previusYear);
		
		generateFilenameToMap(pesaid, year, map, previusYear);
		return map;
	}
	
	private void measurementAccuracyPair(Map<String, String> map, _Column c) {
		if (!c.hasValue()) return;
		String tark = "A";
		if (c.getValue().equals("3") || c.getValue().equals("1")) {
			tark = "T";
		}
		map.put(c.getFullname() +"_" + tark, PDFWriter.SELECTED_CHECKBOX);
	}
	
	private void coordinatesToMap(Map<String, String> map, _PesaDomainModelRow result) {
		_Table pesa = result.getPesa();
		String koord_tyyppi = pesa.get("koord_tyyppi").getValue();
		String leveys = "";
		String pituus = "";
		if (koord_tyyppi.equals("88") || koord_tyyppi.equals("89") || koord_tyyppi.equals("90") || koord_tyyppi.equals("91")) {
			leveys = pesa.get("yht_leveys").getValue();
			pituus = pesa.get("yht_pituus").getValue();
			map.put("pesa.koord_tyyppi_Y", PDFWriter.SELECTED_CHECKBOX);
		} else if (koord_tyyppi.equals("70")) {
			leveys = pesa.get("eur_leveys").getValue();
			pituus = pesa.get("eur_pituus").getValue();
			map.put("pesa.koord_tyyppi_E", PDFWriter.SELECTED_CHECKBOX);
		} else if (koord_tyyppi.equals("61") || koord_tyyppi.equals("65")) {
			leveys = pesa.get("yht_leveys").getValue();
			pituus = pesa.get("yht_pituus").getValue();
			map.put("pesa.koord_tyyppi_A", PDFWriter.SELECTED_CHECKBOX);
		}
		map.put("koordLeveys", leveys);
		map.put("koordPituus", pituus);
	}
	
	private void everythingFromRowToMap(Map<String, String> map, _PesaDomainModelRow result) {
		tableToMap(map, result.getPesa());
		// tableToMap(map, result.getTarkastus());
		tableToMap(map, result.getOlosuhde());
	}
	
	private void tableToMap(Map<String, String> map, _Table table) {
		for (_Column c : table) {
			if (c.hasAssosiatedSelection()) {
				map.put(c.getFullname() + "_" + c.getValue(), PDFWriter.SELECTED_CHECKBOX);
				map.put(c.getAssosiatedSelection() + "_" + c.getValue().toUpperCase(), PDFWriter.SELECTED_CHECKBOX);
				if (c.getAssosiatedSelection().equals(SaaksiConst.K_E) && c.getValue().equals("1")) {
					map.put(c.getFullname(), "K");
				} else {
					try {
						map.put(c.getFullname(), selections.get(c.getAssosiatedSelection()).get(c.getValue()));
					} catch (IllegalArgumentException e) {
						map.put(c.getFullname(), c.getValue());
					}
				}
			}
			map.put(c.getFullname(), c.getValue());
			
		}
	}
	
	private void headerFields(Map<String, String> map, _PesaDomainModelRow previusYear) {
		map.put("laani2", previusYear.getColumn("reviiri.hallintolaani").getValue());
		map.put("kunta2", previusYear.getColumn("reviiri.hallintokunta").getValue());
		map.put("kunta.kunimi", previusYear.getColumn("pesa.kunta").getSelectionDesc(selections));
		map.put("kyla2", previusYear.getColumn("pesa.kyla").getValue());
		map.put("reviiri", previusYear.getColumn("reviiri.reviirinimi").getValue() + " (" + previusYear.getColumn("reviiri.reviiriid").getValue() + ")");
		map.put("pesanimi2", previusYear.getColumn("pesa.pesanimi").getValue());
		map.put("pesaid2", previusYear.getColumn("pesa.pesaid").getValue());
	}
	
	private void prevTarkastusOnlyInspectedToMap(String pesaid, Map<String, String> map) throws SQLException {
		_Table prev_tarkastus = previousTarkastusOnlyInspected(pesaid);
		prevTarkastajaToMap(map, prev_tarkastus);
		prevPesimistulosToMap(map, prev_tarkastus);
		prevPesaKuntoToMap(map, prev_tarkastus);
		prevTarkPvmToMap(map, prev_tarkastus);
	}
	
	private Selection	pesimistulokset	= null;
	
	private Selection pesimistuloksetSelection() {
		if (pesimistulokset == null) {
			pesimistulokset = selections.get(dao.newRow().getTarkastus().get("pesimistulos").getAssosiatedSelection());
		}
		return pesimistulokset;
	}
	
	private StringBuilder buildPesimistulosText(String pesimistulos, String poikasCount) {
		StringBuilder text = new StringBuilder();
		text.append(pesimistulos);
		if (poikasCount.length() > 0) text.append(" (").append(poikasCount).append(")");
		text.append(" - ");
		text.append(pesimistuloksetSelection().get(pesimistulos));
		return text;
	}
	
	private void generateFilenameToMap(String pesaid, String year, Map<String, String> map, _PesaDomainModelRow result) {
		StringBuilder filename = new StringBuilder("TARKASTAJA_");
		filename.append(map.get("pesa.seur_rengastaja")).append("_");
		filename.append(result.getColumn("reviiri.hallintolaani"));
		filename.append("_").append(result.getColumn("reviiri.hallintokunta"));
		filename.append("_").append(pesaid).append("_").append(year);
		map.put(Const.FILENAME, filename.toString());
	}
	
	private void prevTarkPvmToMap(Map<String, String> map, _Table tarkastus) {
		map.put("edellinenTarkPvm", tarkastus.get("tark_pvm").getValue());
	}
	
	private void prevPesaKuntoToMap(Map<String, String> map, _Table tarkastus) {
		_Column pesan_kunto = tarkastus.get("kunto");
		String prev_pesa_kunto = pesan_kunto.getValue();
		String prev_pesa_kunto_desc = selections.get(pesan_kunto.getAssosiatedSelection()).get(prev_pesa_kunto);
		prev_pesa_kunto_desc = prev_pesa_kunto + " - " + prev_pesa_kunto_desc;
		map.put("edellinenKunto", prev_pesa_kunto_desc);
	}
	
	private void prevPesimistulosToMap(Map<String, String> map, _Table tarkastus) {
		_Column pesimistulos = tarkastus.get("pesimistulos");
		String prev_pesimistulos = pesimistulos.getValue();
		String poikasCount = searchFunctionality.nestingOutput(tarkastus);
		StringBuilder prev_pesimistulos_text = buildPesimistulosText(prev_pesimistulos, poikasCount);
		map.put("edellinenPesimistulos", prev_pesimistulos_text.toString());
	}
	
	private void prevTarkastajaToMap(Map<String, String> map, _Table tarkastus) {
		_Column tarkastaja = tarkastus.get("rengastaja");
		String prev_tarkastaja = tarkastaja.getValue();
		prev_tarkastaja = selections.get(tarkastaja.getAssosiatedSelection()).get(prev_tarkastaja);
		map.put("edellinenRengastaja", prev_tarkastaja);
	}
	
	private _PesaDomainModelRow previousTarkastusFor(String pesaid) throws SQLException {
		_PesaDomainModelRow searchParams = dao.newRow();
		searchParams.getPesa().get("pesaid").setValue(pesaid);
		List<_PesaDomainModelRow> results = new LinkedList<>();
		dao.executeSearch(searchParams, new RowsToListResultHandler(dao, results));
		_PesaDomainModelRow result = results.get(0);
		return result;
	}
	
	private _Table previousTarkastusOnlyInspected(String pesaid) throws SQLException {
		_Table searchParams = dao.newRow().getTarkastus();
		searchParams.get("pesaid").setValue(pesaid);
		searchParams.get("vaihtop_laiminl").setValue("!J");
		_Table resultTable = dao.newRow().getTarkastus();
		try {
			dao.executeSearch(searchParams, new FirstResultRowToTableHandler(resultTable), "tark_pvm DESC");
		} catch (NoResultsException e) {
			searchParams.get("vaihtop_laiminl").setValue("");
			dao.executeSearch(searchParams, new FirstResultRowToTableHandler(resultTable), "tark_pvm DESC");
		}
		return resultTable;
	}
	
	private void tarkVuosiToWhereItIsNeeded(String year, Map<String, String> map) {
		map.put("vuosi", year);
		map.put("vuosi2", year);
	}
	
	private void seuraavaTarkastaja(Map<String, String> map, _Table pesa) {
		String nro = pesa.get("seur_rengastaja").getValue();
		if (nro.equals("-999")) return;
		Node ringer = ringers.getById(nro);
		String name = ringer.getNode("firstname").getContents() + " " + ringer.getNode("lastname").getContents();
		String address = "";
		for (Node n : ringer.getNode("address")) {
			address += n.getContents() + " ";
		}
		map.put("rengastajaRenro", nro);
		map.put("rengastajaNimi", name);
		map.put("rengastajaOsoite", address);
		map.put("rengastajaPuhelin", ringer.getNode("mobile-phone").getContents());
	}
	
	@Override
	public Map<String, String> tarkastusDataFor(String tarkastusid) throws Exception {
		throw new UnsupportedOperationException();
	}
	
}
