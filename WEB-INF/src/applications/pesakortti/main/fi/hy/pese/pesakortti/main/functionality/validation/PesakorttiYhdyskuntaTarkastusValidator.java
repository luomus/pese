package fi.hy.pese.pesakortti.main.functionality.validation;

import java.util.Iterator;
import java.util.Map;

import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.app.functionality.validate._Validator;
import fi.hy.pese.framework.main.general.PesimisenBiologisetRajatUtil.Rajat;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.main.general.data._Table;
import fi.hy.pese.pesakortti.main.app.PesakorttiConst;

public class PesakorttiYhdyskuntaTarkastusValidator extends PesakorttiTarkastusValidator {

	private final boolean kirjekyyhkyOnly;

	public PesakorttiYhdyskuntaTarkastusValidator(_Request<_PesaDomainModelRow> request, boolean kirjekyyhkyOnly, Map<Integer, _Table> ponttoalueet, Map<String, Rajat> rajat) {
		super(request, kirjekyyhkyOnly, ponttoalueet, rajat);
		this.kirjekyyhkyOnly = kirjekyyhkyOnly;
	}

	public PesakorttiYhdyskuntaTarkastusValidator(_Request<_PesaDomainModelRow> request, Map<Integer, _Table> ponttoalueet, Map<String, Rajat> rajat) {
		super(request, false, ponttoalueet, rajat);
		this.kirjekyyhkyOnly = false;
	}

	@Override
	public void validate() throws Exception {
		if (kirjekyyhkyOnly || data.action().equals(Const.INSERT_FIRST)) {

			params.getTarkastus().get("yhdyskunnassa").setValue("K");
			_Table ekaKaynti = params.getKaynnit().get(1);
			if (ekaKaynti.get("pvm").hasValue()) {
				if (checkDate(ekaKaynti.get("pvm"))) {
					String vuosi = ekaKaynti.get("pvm").getDateValue().getYear();
					params.getTarkastus().get("vuosi").setValue(vuosi);
				}
			}

			super.validate();

			removeKayntiErrorsAndWarnings();
			removeError(tarkastus.get("pesimistulos"));
			removeErrorsAndWarnings(tarkastus.get("pesimistulos"));

			if (params.getKaynnit().values().size() < 2) {
				error(_Validator.UNDEFINED_FIELD, "VAHINTAAN_KAKSI_PESAA");
			}
			checkNotNull(ekaKaynti.get("pvm"));
			checkNotNull(ekaKaynti.get("pvm_tarkkuus"));

			checkNotNull(tarkastus.get("pesiva_laji"));
		}
	}

	private void removeKayntiErrorsAndWarnings() {
		Iterator<Map.Entry<String, String>> i = errors.entrySet().iterator();
		while (i.hasNext()) {
			Map.Entry<String, String> e = i.next();
			if (e.getKey().endsWith(".pvm")) {
				if (e.getKey().equals("kaynti.1.pvm")) continue;
				i.remove();
			} else if (e.getKey().endsWith(".pvm_tarkkuus")) {
				if (e.getKey().equals("kaynti.1.pvm_tarkkuus")) continue;
				i.remove();
			}
		}
		i = warnings.entrySet().iterator();
		while (i.hasNext()) {
			Map.Entry<String, String> e = i.next();
			if (e.getKey().endsWith(".pesinnan_vaihe") && e.getValue().equals(PesakorttiConst.PESINTA_ETENEE_VAARAAN_SUUNTAAN)) {
				i.remove();
			} else if (e.getKey().endsWith(".pvm") && e.getValue().equals(PesakorttiConst.OLETKO_VARMA_ETTEI_KYSE_OLE_YHDYSKUNTAILMOITUKSESTA)) {
				i.remove();
			} else if (e.getValue().equals(PesakorttiConst.MAX_PESINTANOPEUS_YLITTYNYT)) {
				i.remove();
			}
		}
	}
}
