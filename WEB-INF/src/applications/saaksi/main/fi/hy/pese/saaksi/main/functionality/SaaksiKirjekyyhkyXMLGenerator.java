package fi.hy.pese.saaksi.main.functionality;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.db._DAO;
import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.main.general.data._Row;
import fi.hy.pese.framework.main.general.data._Table;
import fi.hy.pese.framework.main.kirjekyyhky.KirjekyyhkyXMLGeneratorUtility;
import fi.hy.pese.framework.main.kirjekyyhky.KirjekyyhkyXMLGeneratorUtility.StructureDefinition;
import fi.hy.pese.framework.main.kirjekyyhky._KirjekyyhkyXMLGenerator;
import fi.hy.pese.saaksi.main.app.SaaksiConst;
import fi.luomus.commons.config.Config;
import fi.luomus.commons.containers.SelectionImple;
import fi.luomus.commons.containers.SelectionOptionImple;
import fi.luomus.commons.tipuapi.TipuAPIClient;
import fi.luomus.commons.utils.FileUtils;

public class SaaksiKirjekyyhkyXMLGenerator implements _KirjekyyhkyXMLGenerator {

	private static final String	EUREF_FIN	= "70";
	private static final int	POIKASET_COUNT	= 4;
	private static final String[]	POIKANEN_COLUMNS_TO_EXCLUDE	= { "poikanenid", "tarkastusid" };
	private final _Request<_PesaDomainModelRow> request;
	private final Config config;
	private final _DAO<_PesaDomainModelRow> dao;
	private final KirjekyyhkyXMLGeneratorUtility generator;
	private final List<String> producedDataXMLFiles = new LinkedList<>();

	public SaaksiKirjekyyhkyXMLGenerator(_Request<_PesaDomainModelRow> request) throws Exception {
		this.request = request;
		this.config = request.config();
		this.dao = request.dao();
		this.generator = initGenerator();
	}

	@Override
	public File writeFormDataXML(Map<String, String> data, String filename) throws IllegalArgumentException {
		filename += ".xml";
		List<String> owners = new ArrayList<>();

		if (data.containsKey("pesa.seur_rengastaja")) {
			owners.add(data.get("pesa.seur_rengastaja"));
		}
		for (String lomakeVastaanottaja : getLomakeVastaanottajat(data)) {
			if (!owners.contains(lomakeVastaanottaja)) {
				owners.add(lomakeVastaanottaja);
			}
		}

		data.put("koord_leveys", data.get("koordLeveys"));
		data.put("koord_pituus", data.get("koordPituus"));

		data.put("tarkastus.vuosi", data.get("vuosi"));
		data.put("tarkastus.rengastaja", owners.get(0));

		String reviirinimi = data.get("reviiri");
		if (reviirinimi != null) {
			if (reviirinimi.contains("(")) {
				reviirinimi = reviirinimi.substring(0, reviirinimi.lastIndexOf("("));
			}
			data.put("reviiri.reviirinimi", reviirinimi);
		}

		String koordTyyppi = data.get("pesa.koord_tyyppi");
		if (koordTyyppi.equals(EUREF_FIN)) {
			data.put("pesa.koord_tyyppi", "E");
		} else {
			data.put("pesa.koord_tyyppi", "Y");
		}

		File file = new File(config.kirjekyyhkyFolder(), filename);
		try {
			String xml = generator.generateDataXML(data, owners);
			FileUtils.writeToFile(file, xml);
			producedDataXMLFiles.add(filename);
		} catch (Exception e) {
			e.printStackTrace();
			request.data().addToNoticeTexts("VIRHE: " + e.getMessage() + " " + e.getStackTrace()[0]);
			return null;
		}
		return file;
	}

	private List<String> getLomakeVastaanottajat(Map<String, String> data) {
		String pesaid = data.get("pesa.pesaid");
		_Table searchParams = dao.newRow().getLomakeVastaanottajat();
		searchParams.get("pesa").setValue(pesaid);
		List<String> vastaanottajat = new ArrayList<>();
		try {
			for (_Table t : dao.returnTables(searchParams)) {
				vastaanottajat.add(t.get("vastaanottaja").getValue());
			}
		} catch (SQLException e) {
			throw new RuntimeException("Lomake vastaanottajat pesälle " + pesaid);
		}
		return vastaanottajat;
	}

	@Override
	public List<String> producedFormDataXMLFiles() {
		return producedDataXMLFiles;
	}

	public File writeStructureXML() throws Exception {
		String xml = generator.generateStructureXML();
		// System.out.println(xml);
		File file = new File(config.reportFolder(), "saaksi.xml");
		try {
			FileUtils.writeToFile(file, xml, request.config().characterEncoding());
		} catch (Exception e) {
			request.data().addToNoticeTexts("VIRHE: " + e.getMessage() + " " + e.getStackTrace()[0]);
		}
		return file;
	}

	private KirjekyyhkyXMLGeneratorUtility initGenerator() throws FileNotFoundException, IOException {
		KirjekyyhkyXMLGeneratorUtility generator = new KirjekyyhkyXMLGeneratorUtility();
		StructureDefinition definition = generator.getDefinition();

		definition.typeID = "pandion";
		definition.title = "Sääksitutkimus";

		definition.menutexts.newForm = "Ilmoita uusi pesä";
		definition.menutexts.printEmptyPdf = "Tulosta tyhjä lomake";
		// definition.menutexts.printInstructions = "Tulosta täyttöohje";

		definition.titleFields.add("tarkastus.vuosi");
		definition.titleFields.add("pesa.kunta");
		definition.titleFields.add("pesa.kyla");
		definition.titleFields.add("reviiri.reviirinimi");
		definition.titleFields.add("pesa.pesanimi");
		definition.titleFields.add("pesa.pesaid");
		definition.titleFields.add("tarkastus.rengastaja");

		definition.validation.URI = config.get("ValidatonServiceURL");
		definition.validation.username = config.get("ValidatonServiceUsername");
		definition.validation.password = config.get("ValidatonServicePassword");

		definition.coordinateFields.leveys = "koord_leveys";
		definition.coordinateFields.pituus = "koord_pituus";
		definition.coordinateFields.tyyppi = "pesa.koord_tyyppi";
		definition.coordinateFields.mittaustapa = "pesa.koord_mittaustapa";
		definition.coordinateFields.setTitleFields("reviiri.reviirinimi", "pesa.pesanimi", "pesa.pesaid");

		_Row row = dao.newRow();
		definition.row = row;
		generator.excludeColumn("pesa", "yht_leveys");
		generator.excludeColumn("pesa", "yht_pituus");
		generator.excludeColumn("pesa", "ast_leveys");
		generator.excludeColumn("pesa", "ast_pituus");
		generator.excludeColumn("pesa", "des_leveys");
		generator.excludeColumn("pesa", "des_pituus");
		generator.excludeColumn("pesa", "eur_leveys");
		generator.excludeColumn("pesa", "eur_pituus");
		generator.excludeColumn("olosuhde", "olosuhdeid");
		generator.excludeColumn("olosuhde", "pesaid");
		generator.excludeColumn("olosuhde", "ilm_pvm");
		generator.excludeColumn("tarkastus", "tarkastusid");
		generator.excludeColumn("tarkastus", "pesaid");
		generator.excludeColumn("pesa", "koord_tyyppi");

		for (int i = 1; i <= POIKASET_COUNT; i++) {
			for (String c : POIKANEN_COLUMNS_TO_EXCLUDE) {
				generator.excludeColumn("poikanen."+i, c);
			}
		}

		generator.addCustomColumn("koord_leveys", _Column.INTEGER, 7);
		generator.addCustomColumn("koord_pituus", _Column.INTEGER, 7);

		generator.addCustomColumn("pesa.koord_tyyppi", _Column.VARCHAR, "koord_tyyppi");

		definition.selections = request.data().selections();
		SelectionImple koordTyyppi = new SelectionImple("koord_tyyppi");
		koordTyyppi.addOption(new SelectionOptionImple("Y", "Yhtenäiskoordinaatti"));
		koordTyyppi.addOption(new SelectionOptionImple("E", "EUREF-FIN"));
		definition.selections.put("koord_tyyppi", koordTyyppi);

		generator.includeTable("pesa");
		generator.includeTable("olosuhde");
		generator.includeTable("tarkastus");
		generator.includeTable("reviiri");
		generator.includeTable("poikanen.1");
		generator.includeTable("poikanen.2");
		generator.includeTable("poikanen.3");
		generator.includeTable("poikanen.4");
		generator.includeTable("kaynti.1");
		generator.includeTable("kaynti.2");
		generator.includeTable("kaynti.3");
		generator.includeTable("kaynti.4");

		generator.excludeSelection(SaaksiConst.TAULUT);
		generator.excludeSelection(TipuAPIClient.RINGERS);
		generator.excludeSelection(TipuAPIClient.MUNICIPALITIES);
		generator.excludeSelection(TipuAPIClient.SPECIES);
		generator.excludeSelection(TipuAPIClient.ELY_CENTRES);
		generator.excludeSelection("tarkastus.fenologia");
		generator.excludeSelection(SaaksiConst.REVIIRIT);

		generator.defineTipuApiSelection("pesa", "seur_rengastaja", TipuAPIClient.RINGERS);
		generator.defineTipuApiSelection("pesa", "kunta", TipuAPIClient.MUNICIPALITIES);
		generator.defineTipuApiSelection("tarkastus", "rengastaja", TipuAPIClient.RINGERS);
		generator.defineTipuApiSelection("tarkastus", "muulaji", TipuAPIClient.SPECIES);
		generator.defineTipuApiSelection("reviiri", "hallintokunta", TipuAPIClient.MUNICIPALITIES);

		generator.noCodesForSelection(SaaksiConst.K_E);

		generator.userAsDefaultValue("pesa.seur_rengastaja");
		generator.userAsDefaultValue("tarkastus.rengastaja");

		generator.setCustomFieldRequired("koord_leveys");
		generator.setCustomFieldRequired("koord_pituus");
		generator.setCustomFieldRequired("pesa.koord_tyyppi");

		Map<String, String> fieldTitles = new HashMap<>();
		for (Entry<String, String> e : request.data().uiTexts().entrySet()) {
			if (e.getKey().startsWith("tarkastuslomake.")) {
				// if (e.getKey().startsWith("tarkastuslomake.aikuinen.")) continue;
				fieldTitles.put(e.getKey().substring("tarkastuslomake.".length()), e.getValue());
			} else {
				if (!fieldTitles.containsKey(e.getKey())) {
					fieldTitles.put(e.getKey(), e.getValue());
				}
			}
		}
		definition.fieldTitles = fieldTitles;

		definition.layout = FileUtils.readContents(new File(config.templateFolder(), "layout.xml"));

		definition.emptyPDF = new File(config.templateFolder(), "saaksilomake-uusi-pesailmoitus.pdf");
		//definition.instructionsPDF = new File(config.templateFolder(), "merikotkatutkimus-ohjeet.pdf");
		return generator;
	}

}
