package fi.hy.pese.saaksi.main.functionality;

import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.app.functionality.KirjekyyhkyValidationServiceFunctionality;
import fi.hy.pese.framework.main.app.functionality.validate._Validator;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.saaksi.main.app.SaaksiConst;
import fi.hy.pese.saaksi.main.functionality.validator.SaaksiTarkastusValidator;

public class SaaksiKirjekyyhkyValidationServiceFunctionality extends KirjekyyhkyValidationServiceFunctionality<_PesaDomainModelRow> {

	private static final boolean	KIRJEKYYHKY_VALIDATIONS_ONLY	= true;

	public SaaksiKirjekyyhkyValidationServiceFunctionality(_Request<_PesaDomainModelRow> request) {
		super(request);
	}

	@Override
	public _Validator validator() {
		return new SaaksiTarkastusValidator(request, KIRJEKYYHKY_VALIDATIONS_ONLY);
	}

	@Override
	public void preProsessingHook() throws Exception {
		super.preProsessingHook();
		SaaksiConst.kirjekyyhkyModelToData(data.getAll(), data.updateparameters());
	}




}
