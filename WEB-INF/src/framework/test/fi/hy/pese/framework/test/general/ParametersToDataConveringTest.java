package fi.hy.pese.framework.test.general;

import java.util.HashMap;
import java.util.Map;

import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data.Data;
import fi.hy.pese.framework.main.general.data.DatabaseTableStructure;
import fi.hy.pese.framework.main.general.data.ParameterMap;
import fi.hy.pese.framework.main.general.data.PesaDomainModelDatabaseStructure;
import fi.hy.pese.framework.main.general.data.PesaDomainModelRowFactory;
import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class ParametersToDataConveringTest {

	public static Test suite() {
		return new TestSuite(ParametersToDataConveringTest.class.getDeclaredClasses());
	}

	public static class UsingParamterMapToDataConverter extends TestCase {

		private Map<String, String>	params;
		private Data<_PesaDomainModelRow>				data;
		private PesaDomainModelDatabaseStructure	rowStructure;

		@Override
		protected void setUp() throws Exception {
			rowStructure = new PesaDomainModelDatabaseStructure();

			rowStructure.setPesa(new DatabaseTableStructure("pesavakio"));
			rowStructure.getPesa().addColumn("pesa_id", _Column.INTEGER, 5);
			rowStructure.getPesa().addColumn("pesanimi", _Column.VARCHAR, 20);

			DatabaseTableStructure kunta = new DatabaseTableStructure("kunta");
			kunta.addColumn("kulyh", _Column.VARCHAR, 6);
			kunta.addColumn("liitos_pvm", _Column.DATE);
			rowStructure.defineCustomTable(kunta);

			DatabaseTableStructure poikanen = new DatabaseTableStructure("poikanen");
			poikanen.addColumn("id", _Column.INTEGER, 5);
			rowStructure.setPoikanen(poikanen);

			data = new Data<>(new PesaDomainModelRowFactory(rowStructure, "", ""));

			params = new HashMap<>();
			params.put(Const.PAGE, "sivutunniste");
			params.put(Const.ACTION, "tauluhallinta_lisaa");
			params.put(Const.UPDATEPARAMETERS + ".pesavakio.pesanimi", "Pesän nimi");
			params.put(Const.UPDATEPARAMETERS + ".kunta.kulyh", "HELSIN");
			params.put(Const.UPDATEPARAMETERS + ".pesavakio.eioletallaista", "huuhaa");
			params.put(Const.SEARCHPARAMETERS + ".pesavakio.pesa_id", "54");
		}

		public void test_it_returns_the_page() {
			ParameterMap p = new ParameterMap(params);
			p.convertTo(data);
			assertEquals("sivutunniste", data.page());
		}

		public void test_it_sets_the_action() {
			ParameterMap p = new ParameterMap(params);
			p.convertTo(data);
			assertEquals("tauluhallinta_lisaa", data.action());
		}

		public void test_it_sets_values_to_updateparameters() {
			ParameterMap p = new ParameterMap(params);
			p.convertTo(data);
			assertEquals("HELSIN", data.updateparameters().getTableByName("kunta").get("kulyh").getValue());
			assertEquals("Pesän nimi", data.updateparameters().getPesa().get("pesanimi").getValue());
			try {
				data.updateparameters().getPesa().get("eioletallaista").getValue();
				fail("Should not contain column 'eioletallaista'");
			} catch (IllegalArgumentException e) {
			}
		}

		public void test_it_sets_values_to_searchparameters() {
			ParameterMap p = new ParameterMap(params);
			p.convertTo(data);
			assertEquals("54", data.searchparameters().getPesa().get("pesa_id").getValue());
		}

		public void test_it_doesnt_set_updateparameters_to_searchparameters() {
			ParameterMap p = new ParameterMap(params);
			p.convertTo(data);
			assertEquals("54", data.searchparameters().getPesa().get("pesa_id").getValue());
			assertEquals("Pesän nimi", data.updateparameters().getPesa().get("pesanimi").getValue());
			assertTrue(data.updateparameters().getPesa().get("pesa_id").getValue() == "");
			assertTrue(data.searchparameters().getPesa().get("pesanimi").getValue() == "");
		}

		public void test_it_sets_others_to_misc_texts() {
			params.put("paluusivu", "haku");
			ParameterMap p = new ParameterMap(params);
			p.convertTo(data);
			assertEquals("haku", data.get("paluusivu"));
			assertEquals("huuhaa", data.get(Const.UPDATEPARAMETERS + ".pesavakio.eioletallaista"));
		}

		public void test_it_doesnt_put_to_misc_texts_the_table_related_fields_or_page_action() {
			ParameterMap p = new ParameterMap(params);
			p.convertTo(data);
			assertEquals("", data.get(Const.PAGE));
			assertEquals("", data.get(Const.ACTION));
			assertEquals("", data.get(Const.UPDATEPARAMETERS + ".kunta.kulyh"));
			assertEquals("", data.get(Const.SEARCHPARAMETERS + ".kunta.kulyh"));
		}

		public void test_it_combines_dates_to_single_value() {
			params.put(Const.SEARCHPARAMETERS + ".kunta.liitos_pvm." + Const.YEAR, "2005");
			params.put(Const.SEARCHPARAMETERS + ".kunta.liitos_pvm." + Const.MONTH, "6");
			params.put(Const.SEARCHPARAMETERS + ".kunta.liitos_pvm." + Const.DAY, "15");

			params.put(Const.UPDATEPARAMETERS + ".kunta.liitos_pvm." + Const.YEAR, "849");
			params.put(Const.UPDATEPARAMETERS + ".kunta.liitos_pvm." + Const.MONTH, "");
			params.put(Const.UPDATEPARAMETERS + ".kunta.liitos_pvm." + Const.DAY, null);

			ParameterMap p = new ParameterMap(params);
			p.convertTo(data);
			String date = data.searchparameters().getTableByName("kunta").get("liitos_pvm").getValue();
			assertEquals("15.06.2005", date);
			date = data.updateparameters().getTableByName("kunta").get("liitos_pvm").getValue();
			assertEquals("..849", date);
		}

		public void test_it_sets_rowids() {
			params.put(Const.UPDATEPARAMETERS + ".kunta.rowid", "AAAABC");
			ParameterMap p = new ParameterMap(params);
			p.convertTo(data);
			assertEquals("AAAABC", data.updateparameters().getTableByName("kunta").getRowidValue());
		}

		public void test_language() {
			params.put(Const.LANGUAGE, "EN");
			ParameterMap p = new ParameterMap(params);
			p.convertTo(data);
			assertEquals("EN", data.get(Const.LANGUAGE));
			// assertEquals("EN", data.language());
			// Yep.. Language is put to data by Control before params.converTo(data) is called, not by convertTo()
		}

		public void test_converting_poikanen_data() {
			params.put(Const.SEARCHPARAMETERS + ".poikanen.1.id", "5");
			params.put(Const.SEARCHPARAMETERS + ".poikanen.2.id", "6");
			params.put(Const.UPDATEPARAMETERS + ".poikanen.2.rowid", "AAA");
			params.put(Const.UPDATEPARAMETERS + ".poikanen.44.id", "234");
			ParameterMap p = new ParameterMap(params);
			p.convertTo(data);
			assertEquals("5", data.searchparameters().getPoikaset().get(1).get("id").getValue());
			assertEquals("6", data.searchparameters().getPoikaset().get(2).get("id").getValue());
			assertEquals("AAA", data.updateparameters().getPoikaset().get(2).getRowidValue());
			assertEquals("234", data.updateparameters().getPoikaset().get(44).get("id").getValue());
			assertEquals("", data.searchparameters().getPoikaset().get(44).get("id").getValue());
		}

	}
}
