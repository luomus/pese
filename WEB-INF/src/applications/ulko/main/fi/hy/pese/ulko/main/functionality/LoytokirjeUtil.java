package fi.hy.pese.ulko.main.functionality;

import java.io.File;

import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._Table;
import fi.hy.pese.ulko.main.app._UlkoDomainModelRow;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.Utils;

public class LoytokirjeUtil extends RecoveryLetterUtil {

	private static final String	FI	= "FI";
	protected final _Table rengastus;
	private String vastaanottaja = "loytaja";

	public LoytokirjeUtil(_Table pyynto, _Table loyto, _Table rengastus, _Request<_UlkoDomainModelRow> request) {
		super(pyynto, loyto, request);
		this.rengastus = rengastus;
		toMap(rengastus);
	}

	public LoytokirjeUtil setVastaanottaja(String vastaanottaja) {
		this.vastaanottaja = vastaanottaja;
		return this;
	}

	@Override
	public File write(boolean kansilehti) throws Exception {
		dataMap.put("loytaja", henkilo("loytaja", "\n", false));
		dataMap.put("ilmoittaja", henkilo("ilmoittaja", "\n", false));
		dataMap.put("renkaat", renkaat(rengastus, FI));
		dataMap.put("loyto.aika", aika(loyto, FI));
		dataMap.put("rengastus.aika", aika(rengastus, FI));
		dataMap.put("loyto.paikka", paikka(loyto));
		dataMap.put("rengastus.paikka", paikka(rengastus));
		dataMap.put("loyto.koordinaatit", koordinaatit(loyto, FI));
		dataMap.put("rengastus.koordinaatit", koordinaatit(rengastus, FI));

		dataMap.put("loyto.laji", laji(loyto, FI) + " " + laji(loyto, LA));
		dataMap.put("rengastus.laji", laji(rengastus, FI) + " " + laji(rengastus, LA));

		dataMap.put("loyto.sukupuoli", getValueDescription(loyto.get("sukupuoli"), FI));
		dataMap.put("loyto.ika", ika(loyto, FI));
		dataMap.put("rengastus.sukupuoli", getValueDescription(rengastus.get("sukupuoli"), FI));
		dataMap.put("rengastus.ika", ika(rengastus, FI));

		_Column loytoPvm = loyto.get("pvm");
		_Column rengastusPvm = rengastus.get("pvm");
		if (loytoPvm.hasValue() && rengastusPvm.hasValue()) {
			String humanized = DateUtils.humanizeDifference(rengastusPvm.getDateValue(), loytoPvm.getDateValue(), DateUtils.FI);
			dataMap.put("kului_paivaa", humanized +".");
		}

		_Column loytoLat = loyto.get("wgs84_leveys");
		_Column loytoLon = loyto.get("wgs84_pituus");
		_Column rengLat = rengastus.get("wgs84_leveys");
		_Column rengLon = rengastus.get("wgs84_pituus");

		if (given(loytoLat, loytoLon, rengLat, rengLon)) {
			long distance = (long) Utils.distance(rengLat.getDoubleValue(), rengLon.getDoubleValue(), loytoLat.getDoubleValue(), loytoLon.getDoubleValue());
			dataMap.put("sijaitsee_km_paassa", Long.toString(distance));

			double bearing = Utils.bearing(rengLat.getDoubleValue(), rengLon.getDoubleValue(), loytoLat.getDoubleValue(), loytoLon.getDoubleValue());
			bearing = Math.round(bearing * 10) / 10;
			dataMap.put("suuntaan_astetta", Double.toString(bearing));
		} else {
			dataMap.put("sijaitsee_km_paassa", "Puuttuu");
			dataMap.put("suuntaan_astetta", "Puuttuu");
		}

		File kirje = writer.writePdf("letter_to_finder", dataMap, "loytokirje_" + diario);
		if (kansilehti) {
			dataMap.put("kansilehti", dataMap.get(vastaanottaja));
			writer.writePdf("kansilehti", dataMap, "loytokirje_" + diario + "_kansilehti");
			return writer.createCollection("loytokirje_" + diario + "_ " + vastaanottaja + "lle_kansilehdella");
		}
		request.data().addToNoticeTexts(writer.noticeTexts());
		return kirje;
	}

	private boolean given(_Column...columns) {
		for (_Column c : columns) {
			if (!c.hasValue()) return false;
		}
		return true;
	}

}
