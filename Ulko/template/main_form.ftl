<#include "header.ftl">

<#if updateparameters.pyynto.id.hasValue()>
	<h1>Diario ${updateparameters.pyynto.id}</h1>
	<h3>${selections["pyynto.tila"][updateparameters.pyynto.tila]}</h3>
<#elseif data.fromloydos??>
	<h1>Vastaanotto Löydöksestä</h1>
<#else>
	<h1>Avaa uusi tapaus</h1>
</#if>

<@statustexts system texts />
<#if (errors?size > 0) || (warnings?size > 0)><p class="error">Ei tallennettu, korjaa virheet tai korjaa/kuittaa varoitukset</p></#if> 
<@errorlist errors texts />
<@warninglist warnings texts />

<#if lists.emails??><#list lists.emails as email><b>Lähetetty osoitteeseen: </b> ${email}<br /></#list></#if>

<#list files as file>
    <span class="file"> <a target="_new" href="${system.baseURL}?page=filemanagement&amp;action=show&amp;folder=pdf&amp;filename=${file}">${file}</a> </span>
</#list>


<#if updateparameters.pyynto.roskiin == "K">
	<button onclick="$('#updateparameters\\.pyynto\\.roskiin').val('E').trigger('liszt:updated'); $('#form').submit();">
		Siirrä pois roskiksesta
	</button>
</#if>


<form action="${system.baseURL}" method="post" id="form">
<input type="hidden" name="action" value="update" />
<@inputHidden "updateparameters" updateparameters.pyynto.rowid />
<@inputHidden "updateparameters" updateparameters.pyynto.id />
<@inputHidden "updateparameters" updateparameters.loyto.rowid />
<@inputHidden "updateparameters" updateparameters.loyto.id />
<@inputHidden "updateparameters" updateparameters.rengastus.rowid />
<@inputHidden "updateparameters" updateparameters.rengastus.id />
<@inputHidden "updateparameters" updateparameters.pyynto.rengaspyynto_lahetetty />
<@inputHidden "updateparameters" updateparameters.pyynto.loytokirje_lahetetty />
<input type="hidden" name="bypassed_warnings" id="bypassed_warnings" value="${data.bypassed_warnings!""}" /> 
<#if data.fromloydos??>
	<input type="hidden" name="fromloydos" value="true" />
</#if>
<@inputHidden "updateparameters" updateparameters.pyynto.loydosid />
<@inputHidden "updateparameters" updateparameters.pyynto.loydos_kuvaid />


<div style="float: right; position: relative; top: -40px;"><input type="submit" value="Tallenna" />
<button onclick="if (confirm('Peruutetaanko muutokset?')) {location.href = '${system.baseURL}?id=${updateparameters.pyynto.id}'; } return false;">Peruuta</button>
</div>

<table id="main_form">
<tr>
<td>

	<table id="loytaja">
		<caption>Löytäjä</caption>
		<tr>
			<td><label>${texts["pyynto.loytaja_renro"]}</label></td>
			<td><@selectionChosen selections "updateparameters" updateparameters.pyynto.loytaja_renro 1 false true true errors /></td>
		</tr>
		<tr>
			<td><label>${texts["pyynto.loytaja_nimi"]}</label></td>
			<td><@inputText "updateparameters" updateparameters.pyynto.loytaja_nimi true errors /></td>
		</tr>
		<tr>
			<td><label>${texts["pyynto.loytaja_osoite"]}</label></td>
			<td><@textarea "updateparameters" updateparameters.pyynto.loytaja_osoite 1 30 errors /></td>
		</tr>
		<tr>
			<td><label>${texts["pyynto.loytaja_postinumero"]}</label></td>
			<td><@inputText "updateparameters" updateparameters.pyynto.loytaja_postinumero true errors /></td>
		</tr>
		<tr>
			<td><label>${texts["pyynto.loytaja_postitoimipaikka"]}</label></td>
			<td><@inputText "updateparameters" updateparameters.pyynto.loytaja_postitoimipaikka true errors /></td>
		</tr>
		<tr>
			<td><label>${texts["pyynto.loytaja_puhelin"]}</label></td>
			<td><@inputText "updateparameters" updateparameters.pyynto.loytaja_puhelin true errors /></td>
		</tr>
		<tr>
			<td><label>${texts["pyynto.loytaja_email"]}</label></td>
			<td><@inputText "updateparameters" updateparameters.pyynto.loytaja_email true errors /></td>
		</tr>
	</table>
	
	<script type="text/javascript">
		// <![CDATA[
		$("#updateparameters\\.pyynto\\.loytaja_renro").change(function() {
			var renro = $(this).val();
			if (renro == '') {
				return; 
			}
  			$.get('${system.baseURL}?page=ringerInfo&renro='+renro, function(response) {
  			    var ringer = $(response).find("ringer");
  			    var address = ringer.find("address");
  				var nimi = ringer.find("firstname").text() + ' ' + ringer.find("lastname").text();
  				$("#updateparameters\\.pyynto\\.loytaja_nimi").val(nimi);
  				$("#updateparameters\\.pyynto\\.loytaja_osoite").val(address.find("street").text());
  				$("#updateparameters\\.pyynto\\.loytaja_postinumero").val(address.find("postcode").text());
  				$("#updateparameters\\.pyynto\\.loytaja_postitoimipaikka").val(address.find("city").text());
  				$("#updateparameters\\.pyynto\\.loytaja_puhelin").val(ringer.find("mobile-phone").text());
  				$("#updateparameters\\.pyynto\\.loytaja_email").val(ringer.find("email").text());
 			}, "xml");
		});
		// ]]>
	</script>
	
</td>
<td>

	<table id="ilmoittaja">
		<caption>Ilmoittaja</caption>
		<tr>
			<td><label>${texts["pyynto.ilmoittaja_renro"]}</label></td>
			<td><@selectionChosen selections "updateparameters" updateparameters.pyynto.ilmoittaja_renro 1 false true true errors /></td>
		</tr>
		<tr>
			<td><label>${texts["pyynto.ilmoittaja_nimi"]}</label></td>
			<td><@inputText "updateparameters" updateparameters.pyynto.ilmoittaja_nimi true errors /></td>
		</tr>
		<tr>
			<td><label>${texts["pyynto.ilmoittaja_osoite"]}</label></td>
			<td><@textarea "updateparameters" updateparameters.pyynto.ilmoittaja_osoite 1 30 errors /></td>
		</tr>
		<tr>
			<td><label>${texts["pyynto.ilmoittaja_postinumero"]}</label></td>
			<td><@inputText "updateparameters" updateparameters.pyynto.ilmoittaja_postinumero true errors /></td>
		</tr>
		<tr>
			<td><label>${texts["pyynto.ilmoittaja_postitoimipaikka"]}</label></td>
			<td><@inputText "updateparameters" updateparameters.pyynto.ilmoittaja_postitoimipaikka true errors /></td>
		</tr>
		<tr>
			<td><label>${texts["pyynto.ilmoittaja_puhelin"]}</label></td>
			<td><@inputText "updateparameters" updateparameters.pyynto.ilmoittaja_puhelin true errors /></td>
		</tr>
		<tr>
			<td><label>${texts["pyynto.ilmoittaja_email"]}</label></td>
			<td><@inputText "updateparameters" updateparameters.pyynto.ilmoittaja_email true errors /></td>
		</tr>
	</table>
	
	<script type="text/javascript">
		// <![CDATA[
		$("#updateparameters\\.pyynto\\.ilmoittaja_renro").change(function() {
			var renro = $(this).val();
			if (renro == '') {
				return; 
			}
  			$.get('${system.baseURL}?page=ringerInfo&renro='+renro, function(response) {
  			    var ringer = $(response).find("ringer");
  			    var address = ringer.find("address");
  				var nimi = ringer.find("firstname").text() + ' ' + ringer.find("lastname").text();
  				$("#updateparameters\\.pyynto\\.ilmoittaja_nimi").val(nimi);
  				$("#updateparameters\\.pyynto\\.ilmoittaja_osoite").val(address.find("street").text());
  				$("#updateparameters\\.pyynto\\.ilmoittaja_postinumero").val(address.find("postcode").text());
  				$("#updateparameters\\.pyynto\\.ilmoittaja_postitoimipaikka").val(address.find("city").text());
  				$("#updateparameters\\.pyynto\\.ilmoittaja_puhelin").val(ringer.find("mobile-phone").text());
  				$("#updateparameters\\.pyynto\\.ilmoittaja_email").val(ringer.find("email").text());
 			}, "xml");
		});
		// ]]>
	</script>
	
</td>
</tr>
<tr>
<td>

<table>
	<caption>Löydön tiedot</caption>
	<#list updateparameters.loyto.columns as column>
		<#if column.fieldType == NORMAL_FIELD>
			<#if column.name == "pvm"><tr><th colspan="2">Aika</th></tr></#if>
			<#if column.name == "euring_paikka"><tr><th colspan="2">Paikka</th></tr></#if>
			<#if column.name == "koord_ilm_tyyppi"><tr><th colspan="2">Koordinaatit</th></tr></#if>
			<#if column.name == "laji"><tr><th colspan="2">Lintu</th></tr></#if>
			<#if column.name == "linnun_tila"><tr><th colspan="2">Löydöstä</th></tr></#if>
			<#if column.name?ends_with("_pituus")> 
				<tr class="coordinatePair_bottom">
			<#else>
				<#if column.name?ends_with("_leveys")>
					<tr class="coordinatePair_top">
				<#else>
					<tr>
				</#if>
			</#if>
				<td><label>${texts[column.fullname]}</label></td>
				<td>
					<#if column.name == "kunta"><@inputSelectCombo "updateparameters" column true true errors /> <#else>
					<#if column.inputType == "TEXT">	<@inputText "updateparameters" column true errors />	</#if>
        			<#if column.inputType == "TEXTAREA"><@textarea "updateparameters" column 2 30 errors />	</#if>
        			<#if column.inputType == "SELECT"> 	<@selectionChosen selections "updateparameters" column 1 false true true errors />	</#if>
        			<#if column.inputType == "DATE">	<@date "updateparameters" column errors />	</#if>
        			<#if column.name?starts_with("wgs84_ast_")> (aammss) </#if>
        			<#if column.name == "paino"> g </#if>
        			<#if column.name == "siipi"> mm </#if>
        			</#if>
        		</td>
        	</tr>
		</#if>
		<#if column.name == "scheme">
			<tr>
				<td><label>${texts["pyynto.cc_rengastustietopyynto_email"]}</label></td>
				<td> <@inputText "updateparameters" updateparameters.pyynto.cc_rengastustietopyynto_email true errors /> </td>
			</tr>
		</#if>
	</#list>
</table>

</td>
<td>

<table>
	<caption>Rengastuksen tiedot<#if !updateparameters.pyynto.id.hasValue()> (eivät pakolliset tässä vaiheessa)</#if></caption>
	<#list updateparameters.rengastus.columns as column>
		<#if column.name == "rengas_varmennettu" || column.name == "loytotapa" || column.name == "loytotapa_tarkkuus" || column.name?starts_with("eur_") || column.name?starts_with("yht_")>
			<#assign hidden = true> <#else> <#assign hidden = false>
		</#if>
		<#if column.fieldType == NORMAL_FIELD>
			<#if column.name == "pvm"><tr><th colspan="2">Aika</th></tr></#if>
			<#if column.name == "euring_paikka"><tr><th colspan="2">Paikka</th></tr></#if>
			<#if column.name == "koord_ilm_tyyppi"><tr><th colspan="2">Koordinaatit</th></tr></#if>
			<#if column.name == "laji"><tr><th colspan="2">Lintu</th></tr></#if>
			<#if column.name == "linnun_tila"><tr><th colspan="2">Rengastuksesta</th></tr></#if>
			<#if hidden> 
				<tr style="visibility: hidden;">
			<#else>
				<#if column.name?ends_with("_pituus")> 
					<tr class="coordinatePair_bottom">
				<#else>
					<#if column.name?ends_with("_leveys")>
						<tr class="coordinatePair_top">
					<#else>
						<tr>
				</#if>
				</#if>
			</#if>
				<td><label>${texts[column.fullname]}</label></td>
				<td>
					<#if column.inputType == "TEXT">	<@inputText "updateparameters" column true errors />	</#if>
        			<#if column.inputType == "TEXTAREA"><@textarea "updateparameters" column 2 33 errors />	</#if>
        			<#if column.inputType == "SELECT"> 	<@selectionChosen selections "updateparameters" column 1 false true true errors /> 	</#if>
        			<#if column.inputType == "DATE">	<@date "updateparameters" column errors />	</#if>
        			<#if column.name?starts_with("wgs84_ast_")> (aammss) </#if>
        			<#if column.name == "paino"> g </#if>
        			<#if column.name == "siipi"> mm </#if>
        		</td>
        	</tr>
		</#if>
		<#if column.name == "tarkempi_paikka">
			<tr><td> <div class="sameAsChosenComponentHeight"></div> </td></tr>
		</#if>
		<#if column.name == "rengas_selite">
			<tr><td> <div class="sameAsInputHeight"></div> </td></tr>
		</#if>
	</#list>
</table>

</td>
</tr>
<tr>
<td colspan="2">

	<table style="width: 100%; max-width: none;">
		<caption>Tapaus</caption>
		<#if updateparameters.pyynto.loydosid.hasValue()>
			<tr>
				<td><label>Linkki Löydökseen</label></td>
				<td><a href="${data.loydos_link_url}" target="_blank">Linkki</a></td>
			</tr>
			<#if updateparameters.pyynto.loydos_kuvaid.hasValue()>
			<tr>
				<td><label>Kuvalinkit</label></td>
				<td>
					<#list lists.loydos_kuva_links as kuvaurl>
						<a href="${kuvaurl}" target="_blank">Kuva</a>
					</#list>
				</td>
			</tr>
			</#if>
		</#if>
		<#if updateparameters.pyynto.id.hasValue()>
		<tr>
			<td><label>Siirrä roskiin!</label></td>
			<td><@selectionChosen selections "updateparameters" updateparameters.pyynto.roskiin 1 false true true errors /></td>
		</tr>
		<tr>
			<td><label>Vaihda tila!</label></td>
			<td><@selectionChosen selections "updateparameters" updateparameters.pyynto.tila 1 false true true errors /></td>
		</tr>
		<tr>
			<td><label>Rengaspyyntö lähetetty pvm</label></td>
			<td>${updateparameters.pyynto.rengaspyynto_lahetetty}</td>
		</tr>
		<tr>
			<td><label>Löytökirje lähetetty pvm</label></td>
			<td>${updateparameters.pyynto.loytokirje_lahetetty}</td>
		</tr>
		</#if>
		<tr>
			<td><label>${texts["pyynto.kommentti"]}</label></td>
			<td><@textarea "updateparameters" updateparameters.pyynto.kommentti 2 70 errors /></td>
		</tr>
	</table>
	
</td>
</tr>
</table>

<a name="tools"> </a>
 
<input type="submit" value="Tallenna" />
<button onclick="if (confirm('Peruutetaanko muutokset?')) {location.href = '${system.baseURL}?id=${updateparameters.pyynto.id}'; } return false;">Peruuta</button>

</form>


<#if updateparameters.pyynto.roskiin != "K">
<#if updateparameters.pyynto.tila == "">
	<div class="toolbox">
		<p>Kopioi tähän löydön tiedot löytölomakkeen generoimasta sähköpostista:</p> 
		<p><textarea rows="20" cols="70" id="loytolomaketiedot"></textarea></p>
		<p><button onclick="syotaLoytotiedot(); return false;">Syötä</button></p>
	</div>
	<script type="text/javascript">
	// <![CDATA[
		function syotaLoytotiedot() {
			var data = $("#loytolomaketiedot").val();
			$.post("${system.baseURL}?page=loytotiedot", { data: data }, function(response) {
				$(response).find("data").children().each(function() {
					var field = $(this).prop("tagName");
					var value = $(this).text();
					var fieldID = field.replace(new RegExp("\\.", "g"), "\\.");
			    	$("#"+fieldID).val(value);
			    	$("#"+fieldID).trigger("liszt:updated");
			    	$("#"+fieldID+"\\.input").val(value);
			    	$("#"+fieldID+"\\.input").trigger("liszt:updated");
			    	$("#"+fieldID+"\\.selector").val(value);
			    	$("#"+fieldID+"\\.selector").trigger("liszt:updated");
				});
 			}, "xml");
 			$("#updateparameters\\.loyto\\.alkup_kirje_sisalto").val(data);
		}
	// ]]>
	</script>
</#if>

	
<#if updateparameters.pyynto.tila == "1">
	<#if updateparameters.loyto.scheme.hasValue() && updateparameters.loyto.koord_ilm_tyyppi.hasValue() && (errors?size <= 0) && (warnings?size <= 0)>
	<div class="toolbox">
		<p>Lähetä löytötiedot ulkomaiseen rengastuskeskukseen ja pyydä rengastustiedot:</p>
		<p>
			<button onclick="esikatsele()">Esikatsele</button>
			<button onclick="laheta();">Lähetä</button> 
		</p>
	</div>
	<div class="toolbox" id="mustSave">
		<p>Lähetä löytötiedot ulkomaiseen rengastuskeskukseen ja pyydä rengastustiedot:</p>
		<p><strong>Tallenna ensin muutokset!</strong></p>
	</div>

	<script type="text/javascript">
	// <![CDATA[
	    function esikatsele() {
	    	window.open('${system.baseURL}?action=esikatselu_rengastustietopyynto&id=${updateparameters.pyynto.id}', 'new', 'height=800,width=1200');
	    }
	    function laheta() {
	    	if (confirm('Lähetetäänkö varmasti?')) {
	    		location.href = '${system.baseURL}?action=laheta_rengastustietopyynto&id=${updateparameters.pyynto.id}'; 
	    	}
	    }
		$(":input").bind("keyup change",function() {
			$(".toolbox").hide();
			$("#mustSave").show();
		});
	// ]]>
	</script>
	<#else>
		<div class="toolbox">
			<p>Lähetä löytötiedot ulkomaiseen rengastuskeskukseen ja pyydä rengastustiedot:</p>
			<#if !updateparameters.loyto.scheme.hasValue()> <p><strong>Täytä ensin scheme!</strong></p> </#if>
			<#if !updateparameters.loyto.koord_ilm_tyyppi.hasValue()> <p><strong>Täytä ensin koordinaatit!</strong></p> </#if>
			<#if (errors?size > 0) || (warnings?size > 0)> <p><strong>Korjaa ensin virheet</strong></p> </#if>
		</div>
	</#if>
</#if>

<#if updateparameters.pyynto.tila == "2">
	<div class="toolbox">
		<p>Kopioi tähän rengastuksen tiedot EURING2000 -rimpsuna:</p> 
		<p style="color: red">TODO</p>
		<p><textarea rows="5" cols="70" id="euringRimpsu"></textarea></p>
		<p><button onclick="syotaEuringRimpsu(); return false;">Syötä</button></p>
	</div>
	<script type="text/javascript">
	// <![CDATA[
		function syotaEuringRimpsu() {
			var data = $("#euringRimpsu").val();
			alert(data);
 			$("#updateparameters\\.rengastus\\.alkup_kirje_sisalto").val(data);
 			$("#euringRimpsu").val('');
		}
	// ]]>
	</script>
</#if>

<#if updateparameters.pyynto.tila == "3">
    <#if (errors?size <= 0) && (warnings?size <= 0)>
	<div class="toolbox">
		<p>Lähetä kirje löytäjälle ja ilmoittajalle:</p>
		<p>
			<button onclick="esikatsele();">Esikatsele</button>
			<button onclick="laheta()">Lähetä</button> 
		</p>
	</div>
	<div class="toolbox" id="mustSave">
		<p>Lähetä kirje löytäjälle ja ilmoittajalle:</p>
		<p><strong>Tallenna ensin muutokset!</strong></p>
	</div>
	<script type="text/javascript">
	// <![CDATA[
	    function esikatsele() {
	    	window.open('${system.baseURL}?action=esikatselu_loytokirje&id=${updateparameters.pyynto.id}', 'new', 'height=800,width=1200');
	    }
	    function laheta() {
	    	if (confirm('Lähetetäänkö varmasti?')) {
	    		location.href = '${system.baseURL}?action=laheta_loytokirje&id=${updateparameters.pyynto.id}'; 
	    	}
	    }
		$(":input").bind("keyup change",function() {
			$(".toolbox").hide();
			$("#mustSave").show();
		});
	// ]]>
	</script>
	<#else>
		<div class="toolbox">
			<p>Lähetä kirje löytäjälle ja ilmoittajalle:</p>
			<p><strong>Korjaa ensin virheet</strong></p>
		</div>
	</#if>
</#if>

<#if updateparameters.pyynto.tila == "4">
	<div class="toolbox">
		<p>Esikatsele uudelleen kirjeitä:</p>
		<p>
			<button onclick="esikatsele_rengaspyyntoa()">Esikatsele ulkomaisen schemen kirjettä</button>
			<button onclick="esikatsele_loytokirjetta()">Esikatsele löytäjän kirjettä</button>
		</p>
	</div>
	<script type="text/javascript">
	// <![CDATA[
	    function esikatsele_rengaspyyntoa() {
	    	window.open('${system.baseURL}?action=esikatselu_rengastustietopyynto&id=${updateparameters.pyynto.id}', 'new', 'height=800,width=1200');
	    }
	    function esikatsele_loytokirjetta() {
	    	window.open('${system.baseURL}?action=esikatselu_loytokirje&id=${updateparameters.pyynto.id}', 'new', 'height=800,width=1200');
	    }	    
	// ]]>
	</script>
</#if>

</#if> <#-- jos ei roskissa -->

	<script type="text/javascript">
	// <![CDATA[
		$("#updateparameters\\.loyto\\.kunta\\.input").change(municipalityToEuringPlace);
		$("#updateparameters\\.loyto\\.kunta\\.selector").change(municipalityToEuringPlace);
		
		function municipalityToEuringPlace() {
			var municipalityID = $(this).val();
			$.get("${system.baseURL}?page=municipalityToEuringPlace&municipalityID="+municipalityID, function(response) {
				$(response).find("data").children().each(function() {
					var field = $(this).prop("tagName");
					var value = $(this).text();
					var fieldID = field.replace(new RegExp("\\.", "g"), "\\.");
			    	$("#"+fieldID).val(value);
			    	$("#"+fieldID).trigger("liszt:updated");
			    	$("#"+fieldID+"\\.input").val(value);
			    	$("#"+fieldID+"\\.input").trigger("liszt:updated");
			    	$("#"+fieldID+"\\.selector").val(value);
			    	$("#"+fieldID+"\\.selector").trigger("liszt:updated");
				});
 			}, "xml");
		}
	// ]]>
	</script>
	
<#include "footer.ftl">
