package fi.hy.pese.framework.main.db;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import fi.hy.pese.framework.main.db.oraclesql.NothingToDeleteException;
import fi.hy.pese.framework.main.general.data._Data;
import fi.hy.pese.framework.main.general.data._IndexedTablegroup;
import fi.hy.pese.framework.main.general.data._ReferenceKey;
import fi.hy.pese.framework.main.general.data._Row;
import fi.hy.pese.framework.main.general.data._Table;
import fi.luomus.commons.containers.Selection;

/**
 * DAO (Data Access Object (or a "wierd variation" of a normal DAO)) is used to to get and alter data in the database.
 * Each DAO object is a connection and should be closed after use.
 * Provides methods for transactions.
 * The DAO may also handle logging data changes, but this is not required.
 */
public interface _DAO<T extends _Row> {

	/**
	 * Executes given update statement
	 * @param sql
	 * @param values
	 * @return the number of rows alterted
	 * @throws SQLException
	 */
	int executeUpdate(String sql, String... values) throws SQLException;

	/**
	 * Executes given query and uses the resulthandler to process the results
	 * @param sql
	 * @param resultHandler
	 * @param values
	 * @throws SQLException
	 */
	void executeSearch(String sql, _ResultsHandler resultHandler, String... values) throws SQLException;

	/**
	 * Creates a new, empty table from the databasestructure, matching the given database table
	 * @param name name of the database table
	 * @return
	 * @throws IllegalArgumentException if table by the given name is not defined in the database structure
	 */
	_Table newTableByName(String name) throws IllegalArgumentException;

	/**
	 * Creates a new row
	 * @return
	 */
	T newRow();

	/**
	 * Adds to data "lists"-leaf list of all searches that have been saved
	 * @param data
	 * @throws SQLException
	 */
	void addSavedSearchesListingToData(_Data<T> data) throws SQLException;

	/**
	 * Checks wheter there is a row in the database table matching the rowid or keys
	 * @param table
	 * @return true if there is a row, false if not
	 * @throws SQLException
	 */
	boolean checkRowExists(_Table table) throws SQLException;

	/**
	 * Checks whether there is a row in the database matching the rowid and keys of the table
	 * @param table
	 * @return true if rowid and keys match, false if not
	 * @throws SQLException
	 */
	boolean checkRowIDMatchesKeys(_Table table) throws SQLException;

	/**
	 * Checks whether the database contains at least one row with reference column set to the given value
	 * @param value
	 * @param referenceKey database column reference
	 * @return true if value exists, false if not
	 * @throws SQLException
	 */
	boolean checkReferenceValueExists(String value, _ReferenceKey referenceKey) throws SQLException;

	/**
	 * Checks wheter the database table contains at least one row with the values set in the parameters.
	 * Those columns that have no value set in the search parameters are ignored.
	 * @param parameters
	 * @return true if values exists, false if not
	 * @throws SQLException
	 */
	boolean checkValuesExists(_Table table) throws SQLException;

	// public void close() throws SQLException;

	/**
	 * Commits transaction. No affect transaction has not been started.
	 * @throws SQLException
	 */
	void commit() throws SQLException;

	/**
	 * Deletes table row identified by rowid from database
	 * @param table
	 * @throws SQLException
	 * @throws NothingToDeleteException
	 */
	void executeDelete(_Table table) throws SQLException, NothingToDeleteException;

	/**
	 * Inserts table row to database and adds new rowid to table. If table has unique numerid id it's value must be already set.
	 * @param table
	 * @throws SQLException
	 */
	void executeInsert(_Table table) throws SQLException;

	/**
	 * Finds results from database matching the searchparamters and uses the resulthandler to process the results
	 * @param parameters
	 * @param resultHandler
	 * @throws SQLException
	 */
	void executeSearch(_Table parameters, _ResultsHandler resultHandler) throws SQLException;

	/**
	 * Finds results from database matching the searchparamters and uses the resulthandler to process the results
	 * @param parameters
	 * @param resultHandler
	 * @param orderBy
	 * @throws SQLException
	 */
	void executeSearch(_Table parameters, _ResultsHandler resultHandler, String... orderBy) throws SQLException;

	/**
	 * Finds results from database matching the searchparamters and uses the resulthandler to process the results
	 * @param parameters
	 * @param resultdata
	 * @param additional additional where clauses
	 * @throws SQLException
	 */
	void executeSearch(T parameters, _ResultsHandler resultHandler, String... additional) throws SQLException;

	/**
	 * Updates database table row identified by rowid
	 * @param table
	 * @throws SQLException
	 */
	void executeUpdate(_Table table) throws SQLException;

	/**
	 * Returns a map of selections, read from the database code table. For example if the code table (aputaulu) contains rows: <br>
	 * pesavakio koord_mittaustapa G GPS 1 <br>
	 * pesavakio koord_mittaustapa K Kartta 2 <br>
	 * a selection by name "pesavakio.koord_mittaustapa" will be created, and it will have two options G - GPS and K - Kartta
	 * @param codeTable name of code table (aputaulu) in the database
	 * @param tableField name of the table column in the database (taulu)
	 * @param fieldField name of the field column in the database (kentta)
	 * @param valueField name of the value column in the database (arvo)
	 * @param descField name of the description column in the database (selite)
	 * @param groupField name of the column in the database that contains info on which group this selection belongs to (ryhmittely)
	 * @param orderBy the order of options
	 * @return a map of selections
	 * @throws SQLException
	 */
	Map<String, Selection> returnCodeSelections(String codeTable, String tableField, String fieldField, String valueField, String descField, String groupField, String[] orderBy) throws SQLException;

	/**
	 * Returns a selection containing options, read from the database
	 * @param tablename name of database table, also will be the name of the selection
	 * @param valuefield name of the column used as value of options
	 * @param descriptionfield name of the column used as text of options
	 * * @param groupField name of the column in the database that contains info on which group this selection belongs to (ryhmittely)
	 * @param orderBy the order of options; if not given will be ordered by valuefield
	 * @return a selection
	 * @throws SQLException for example if table/columns do not exist
	 */
	Selection returnSelections(String tablename, String valuefield, String descriptionfield, String[] orderBy) throws SQLException;

	/**
	 * Returns the epoch when selections were last updated to database
	 * @return update time in seconds, unix timestamp
	 * @throws SQLException
	 */
	long returnSelectionsUpdatedTime() throws SQLException;

	/**
	 * Rolls back a transaction. Has no effect if transaction is not started.
	 * @throws SQLException
	 */
	void rollback() throws SQLException;

	/**
	 * Updates selections updated timestamp
	 * @throws SQLException
	 */
	void setSelectionsUpdateTime() throws SQLException;

	/**
	 * Starts a transaction. Changes must be commited() to go into effect
	 * @throws SQLException
	 */
	void startTransaction() throws SQLException;

	/**
	 * Returns value of one column in table identified by rowid
	 * @param table name of the table
	 * @param valueColumn name of the column
	 * @param rowid rowid
	 * @return the value
	 * @throws SQLException if row identified by rowid is not found (or if there is no valueColumn, or if connection not open, etc, etc).
	 */
	String returnValueByRowid(String table, String valueColumn, String rowid) throws SQLException;

	/**
	 * Returns a table by keys. Throws SQLException if the table is not found from the database structure, or if it doesn't have a numeric id column or multipart key column(s).
	 * Multipart key values must be given in the order that the key columns appear in the table's tablestructure.
	 * @param tablename
	 * @param keys
	 * @return
	 * @throws SQLException
	 */
	_Table returnTableByKeys(String tablename, String... keys) throws SQLException;

	/**
	 * Returns a table by rowid. Throws SQLException if the table is not found from the database structure, or if no row found that matches the rowid.
	 * @param tablename
	 * @param rowid
	 * @return
	 * @throws SQLException
	 */
	_Table returnTableByRowid(String tablename, String rowid) throws SQLException;

	/**
	 * For tables with numeric unique id this returns the next id from sequence matching the name of this table. It also sets the id value into the table.
	 * If sequence is not found SQLException is thrown.
	 * If table does not have unique numeric id SQLException is thrown.
	 * @param table
	 * @return
	 * @throws SQLException
	 */
	String returnAndSetNextId(_Table table) throws SQLException;

	/**
	 * Returns all entries in table
	 * @param tablename
	 * @return
	 * @throws SQLException
	 */
	List<_Table> returnTable(String tablename) throws SQLException;

	/**
	 * Returns a prepared statement that the user has to make sure will be closed
	 * @param string
	 * @return
	 * @throws SQLException
	 */
	PreparedStatement prepareStatement(String string) throws SQLException;

	/**
	 * Returns a list of all tables that match search params
	 * @param searchparams
	 * @return
	 * @throws SQLException
	 */
	List<_Table> returnTables(_Table searchparams) throws SQLException;

	/**
	 * Returns a list of all tables that match search params
	 * @param searchparams
	 * @param orderBy
	 * @return
	 * @throws SQLException
	 */
	List<_Table> returnTables(_Table searchparams, String... orderBy) throws SQLException;

	/**
	 * Returns a list of all tables that match search params returning only first results
	 * @param searchparams
	 * @param limit
	 * @param orderBy
	 * @return
	 * @throws SQLException
	 */
	List<_Table> returnTables(_Table searchparams, int limit, String... orderBy) throws SQLException;

	/**
	 * Deletes all tables identified by rowid from database
	 * @param tables
	 * @throws SQLException
	 * @throws NothingToDeleteException
	 */
	void executeDelete(_IndexedTablegroup tables) throws SQLException, NothingToDeleteException;

	/**
	 * Mark record altered and changes to be send for warehousing
	 * @param id
	 * @throws SQLException
	 */
	void markForWarehousing(int rowId) throws SQLException;

	/**
	 * Mark handling of record to have failed
	 * @param transactionEntry
	 * @throws SQLException
	 */
	void markWarehousingAttempted(_Table transactionEntry) throws SQLException;

	/**
	 * Mark record to be successfully transferred to warehouse
	 * @param transactionEntry
	 * @throws SQLException
	 * @throws NothingToDeleteException
	 */
	void markWarehousingSuccess(_Table transactionEntry) throws NothingToDeleteException, SQLException;

	/**
	 * Mark all records as unattempted
	 * @throws SQLException
	 */
	void markAllWarehousingUnattempted() throws SQLException;

}
