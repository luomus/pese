
<#include "header.ftl">

<#if lists.selectedFields??>
  <#assign resultcolumns = lists.selectedFields>
<#else>
  <#assign resultcolumns = [ "pesa.kunta", "pesa.yht_leveys", "pesa.yht_pituus", "tarkastus.vuosi", "tarkastus.pesiva_laji", "tarkastus.tarkastaja", "tarkastus.pesimistulos"  ]>
</#if>

<#-- ----------------------------------------------------------------------------------------------------- -->
<#-- Macro to generate coordinate selection input/select combination ------------------------------------- -->
<#macro coordinateSelection errors searchName>
  <#assign inputId = "CoordinateSearchRadius.input."+searchName>
  <#assign selectorId = "CoordinateSearchRadius.selector."+searchName>
  <input type="text" name="CoordinateSearchRadius" id="${inputId}" size="7" value="${(data.CoordinateSearchRadius)!""}" <@checkerror errors "CoordinateSearchRadius" />  /> m 
  <select name="${selectorId}" id="${selectorId}" <@checkerror errors "CoordinateSearchRadius" /> onchange="syncInputElementWithSelectElement('${inputId}', '${selectorId}');">
        <option value=""></option>
              <option value="500"    <#if (data.CoordinateSearchRadius == "200") >    selected="selected" </#if> >200 m</option>
              <option value="1000"   <#if (data.CoordinateSearchRadius == "1000") >   selected="selected" </#if> >1000 m</option>
              <option value="10000"  <#if (data.CoordinateSearchRadius == "10000") >  selected="selected" </#if> >10 km</option>
              <option value="50000"  <#if (data.CoordinateSearchRadius == "50000") >  selected="selected" </#if> >50 km</option>
              <option value="100000" <#if (data.CoordinateSearchRadius == "100000") > selected="selected" </#if> >100 km</option>
    </select>
</#macro>
<#-- ----------------------------------------------------------------------------------------------------- -->


<h1>${texts.search_title}</h1>

<@statustexts system texts />
<@errorlist errors texts />

<#if system.action == "search">
  <#if data.row_count == "0"> 
    Haku ei tuottanut tuloksia 
  <#else> 
    ${texts.search_result_pesa_count} ${data.item_count} &nbsp; &nbsp; Tarkastuksia: ${data.row_count} <br /> <br />
  </#if>
</#if>

<#-- --------------------------------------------------------------------------------------------------------------------------------------------- -->
<#-- -------------------------- RESULTS ---------------------------------------------------------------------------------------------------------- -->
<#if (results?size > 0)>
  
  
  <table id="resultsTable" class="tablesorter"> 
    <thead> 
      <tr> 
        <th class="{sorter: false}"> Haavi </th>
        <th class="{sorter: false} "> &nbsp; </th>
        <#-- ---------- result field descriptions ----------------------- -->
          <#list lists.resultColumns as colName> <#assign column = searchparameters.getColumn(colName)>
              <th <#if column.datatype == INTEGER || column.datatype == DECIMAL>class="{sorter: 'digit'}"</#if>  >${texts[colName]!colName}</th>
          </#list>
		          
          <#-- ----------------------- show all / show less  link ----------------------- -->  
          <th align="right" class="{sorter: false}"> <a id="showAll" href="javascript:void(0);" onclick=" changeVisibilityOfAll('<#list results as i>${i.pesa.id},</#list>', '${texts.search_show_all}',  '${texts.search_hide_all}', '${texts.search_show_more}', '${texts.search_show_less}'   ); this.blur(); "> ${texts.search_show_all} </a> </th>
          <th class="{sorter: false} "> &nbsp; </th>
          <#-- -------------------------- ----------------------- ----------------------- -->
  
        <#-- ----------------------- ----------------------- ------------ -->
      </tr>
    </thead>
    
    <tbody>
    
    <#assign prevResult = "">
    <#list results as result>  
      <#assign id = result.pesa.id><#if id == prevResult> <#assign i = i + 1><tr class="hidden" id="result_row_${id}_${i}"><td>&nbsp;</td><#else> <#assign i = 0><tr> <td class="haavi"> <input type="checkbox" name="nest_entry_${id}" onchange="toNet(${id});" /> </td> </#if>
      <td> <a class="button-link" href="${system.baseURL}?page=tarkastus&amp;action=fill_for_update&amp;updateparameters.tarkastus.id=${result.tarkastus.id.value}">${result.pesa.id.value}</a> </td> 
      <#list lists.resultColumns as colName><#assign column = result.getColumn(colName)> <#if column.inputType == "SELECT"> <td>${ column.value } - ${ selections[column.assosiatedSelection][ column.value ] }</td> <#else> <td>${column.value}</td> </#if> </#list>
      <td>  <#if id != prevResult> <a id="visibility_link_${id}" href="javascript:void(0);" class="show_more" onclick=" changeVisibility( ${id}, '${texts.search_show_more}', '${texts.search_show_less}' ); blur(); return false; "> ${texts.search_show_more} </a> <br /> <p style="margin-bottom: 3px;"><a target="_karttapaikka" href="https://asiointi.maanmittauslaitos.fi/karttapaikka/?share=customMarker&n=${result.pesa.eur_leveys}&amp;e=${result.pesa.eur_pituus}&amp;title=${result.pesa.nimi.getUrlEncodedValue()}(${result.pesa.id})">kartalle</a></p> </#if> </td>
      <td>  <#if id != prevResult> <form action="${system.baseURL}" method="post"> Uusi tarkastus vuodelle <input type="text" size="4" name="tark_vuosi" value="${data.tark_vuosi!""}" <@checkerror errors "tark_vuosi" /> /> <input type="hidden" name="page" value="tarkastus" /> <input type="hidden" name="action" value="fill_for_insert" /> <input type="hidden" name="updateparameters.pesa.id" value="${result.pesa.id}" /> <input type="submit" value="Etene" /> </form> </#if> </td>
      </tr><#assign prevResult = result.pesa.id> 
    </#list>
     
    </tbody>
  </table>
  
  <#if data.item_count != "1">
   <a href="javascript:selectAll();" style="font-size: 10px;">&raquo; lisää kaikki pesät haaviin</a>
   <br />
  </#if>
  
  <#if data.search_type??>
  <br />
  <b> Suorita toiminto haavissa oleville pesille: </b>
  <table class="haavi">
    <tr>
      <td> Tulosta vastaukset: </td>
      <td> 
            <#if data.search_type == "quickSearch"> 
              <button onclick="if (!netIsEmpty()) {getIdsFromNetTo(document.getElementsByName('searchparameters.pesa.id')[1]); document.getElementById('print_to_textfile_quick').value='1'; document.search.submit();} else alert('Haavi on tyhjä');">
                Tulosta vastaukset tekstitiedostoon
              </button> 
            <#else>
              <button onclick="if (!netIsEmpty()) {generateAdvancedSearch(); getIdsFromNetTo(document.getElementsByName('searchparameters.pesa.id')[2]); document.getElementById('print_to_textfile_advanced').value='1'; document.advancedSearch.submit();} else alert('Haavi on tyhjä');">
                Tulosta vastaukset tekstitiedostoon
              </button>
            </#if>
      </td>
    </tr>
    <tr>
      <td>Esitäytetty lomake:</td>
      <td>
          <form action="${system.baseURL}" method="post" name="print_pdf">
          <input type="hidden" name="page"   value="${system.page}" />
          <input type="hidden" name="action" value="print_pdf" />
          <input type="hidden" name="searchparameters.pesa.id" value="" id="idsToPrint" /> 
          
          Vuodelle: <input type="text" size="4" name="yyyy" value="${system.currentYear}" /> 
          
          <input type="submit" onclick="if (!netIsEmpty()) {getIdsFromNetTo(document.getElementById('idsToPrint')); } else { alert('Haavi on tyhjä'); return false;}" value="Tulosta" />
          
          Kokoelman tiedostonimi:
          <input type="text" size="20" name="collection_filename" value="" />.pdf   &nbsp; &nbsp;     
                     
          (kokoelmaa ei luoda, jos tiedostonimeä ei anneta)
          </form>
      </td>
    </tr> 
  </table>
  </#if>
  
  <br /><br /><br />
  
</#if>
<#-- -------------------------- result listing ends ---------------------------------------------------------------------------------------------- -->
<#-- --------------------------------------------------------------------------------------------------------------------------------------------- -->

<#list files as file>
  <#if system.action == "print_pdf">
    <span class="file"> <a target="_new" href="${system.baseURL}?page=filemanagement&amp;action=show&amp;folder=pdf&amp;filename=${file}">${file}</a> </span> <br />
  <#else>
    <span class="file"> <a target="_new" href="${system.baseURL}?page=filemanagement&amp;action=show&amp;folder=report&amp;filename=${file}">${file}</a> </span> <br />
  </#if>
</#list>


<#-- --------------------------------------------------------------------------------------------------------------------------------------------- -->
<#-- -------------------------- QUICK SEARCH  ---------------------------------------------------------------------------------------------------- -->

<div id="quickSearch" <#if (data.search_type!"") == "advancedSearch">class="hidden"</#if> >

<h2>${texts.search_quick}</h2>

<form action="${system.baseURL}" method="post" name="search" onsubmit="return confirmSearch(document.search, '${texts.search_confirm_search_all}');" >
<input type="hidden" name="page"   value="${system.page}" />
<input type="hidden" name="action" value="search" />
<input type="hidden" name="search_type" value="quickSearch" />


<input type="hidden" name="selectedFields" value=" 
<#list resultcolumns as c>
  ${c},
</#list>
" />

<input type="hidden" name="print_to_textfile" value="" id="print_to_textfile_quick" />


    
<table>
  <tr>
    <td> 
      Vuosi:</td><td> <@inputText "searchparameters" searchparameters.tarkastus.vuosi false errors />
    </td>
  </tr>
  <tr>
    <td> 
      Havainoijanro:</td><td> <@selectionChosen selections "searchparameters" searchparameters.tarkastus.tarkastaja 1 false true true errors />
    </td>
  </tr>
  <tr>
    <td>Pesän ID: </td><td><@inputText "searchparameters" searchparameters.pesa.id true errors  /> </td>
  </tr>
  <tr>
    <td>Kortin numero: </td><td><@inputText "searchparameters" searchparameters.tarkastus.vanha_kortti true errors  /> </td>
  </tr>
  <tr>    
  	<td>${texts.search_quick_pesa_kunta}: </td><td><@selection selections "searchparameters" searchparameters.pesa.kunta 1 false true false errors /> </td>
  </tr>
  <tr>
  	<td colspan="2">Pönttöalueen numero: <@inputText "searchparameters" searchparameters.pesa.ponttoalue_nro true errors  /> 
  	     Pöntön numero: <@inputText "searchparameters" searchparameters.pesa.pontto_nro true errors  /> </td>
  </tr>
</table>


<input type="submit" value="${texts.search}"  /> 
<input type="submit" value="${texts.clear}"  onclick="document.clear.submit(); return false;" />

</form>

<#----------------- clear button ------------------------------>
  <form action="${system.baseURL}" method="get" name="clear">
  <input type="hidden" name="page"  value="${system.page}" />
  </form>
  
<#----------------- ------------ ------------------------------>


<br />
   

</div>

<#-- -------------------------- quick search ends  ----------------------------------------------------------------------------------------------- -->
<#-- --------------------------------------------------------------------------------------------------------------------------------------------- -->

<br /><br />

<#-- --------------------------------------------------------------------------------------------------------------------------------------------- -->
<#-- -------------------------- ADVANCED SEARCH -------------------------------------------------------------------------------------------------- -->

<div id="advancedSearch" <#if (data.search_type!"") == "quickSearch">class="hidden"</#if> >

<h2>${texts.search_advanced}</h2>

<form action="javascript:generateAdvancedSearch();" name="fieldSelections">
<table>
  <thead>
    <tr>
      <th>${texts.search_pesa}</th>
      <th>${texts.search_tarkastus}</th>
      <th>${texts.search_kaynti}</th>
      <th>${texts.search_aikuinen}</th>
      <th>${texts.search_poikanen}</th>
      <th>${texts.search_muna}</th>
    </tr>
  </thead>
  
  <tbody>
    <tr>
      <td> <@fieldSelection searchparameters.pesa "pesa" texts /> </td>
      <td> <@fieldSelection searchparameters.tarkastus "tarkastus" texts /> </td>
      <td> <@fieldSelection searchparameters.kaynti "kaynti" texts /> </td>
      <td> <@fieldSelection searchparameters.aikuinen "aikuinen" texts /> </td>
      <td> <@fieldSelection searchparameters.poikanen "poikanen" texts /> </td>
      <td> <@fieldSelection searchparameters.muna "muna" texts /> </td>
    </tr>
  </tbody>
</table>
</form>


<br />


<form action="${system.baseURL}" method="post" name="advancedSearch">
<input type="hidden" name="page"   value="${system.page}" />
<input type="hidden" name="action" value="search" />
<input type="hidden" name="search_type" value="advancedSearch" />
<input type="hidden" name="selectedFields" id="selectedFields" value="" />
<input type="hidden" name="print_to_textfile" value="" id="print_to_textfile_advanced" />

<table>
  <tr>
    
    <td style="vertical-align: top;">
     
      <table id="advanced_search_table" <#if (data.search_type!"") != "advancedSearch"> </#if> >
        <thead>
          <tr>
            <th>${texts.search_pesa}</th>
            <th>${texts.search_tarkastus}</th>
            <th>${texts.search_kaynti}</th>
            <th>${texts.search_aikuinen}</th>
            <th>${texts.search_poikanen}</th>
            <th>${texts.search_muna}</th>
          </tr>
        </thead>
        
        <tbody>
          <tr>
            <td valign="top"> <@searchFields searchparameters.pesa texts /> </td>
            <td valign="top"> <@searchFields searchparameters.tarkastus texts /> </td>
            <td valign="top"> <@searchFields searchparameters.kaynti texts /> </td>
            <td valign="top"> <@searchFields searchparameters.aikuinen texts /> </td>
            <td valign="top"> <@searchFields searchparameters.poikanen texts /> </td>
            <td valign="top"> <@searchFields searchparameters.muna texts /> </td>
          </tr>
        </tbody>
      </table>
      
    </td>
    
    <td style="vertical-align: top;">
      
      <br /> 
      
      <select name="NestCondition" size="3">
        <option value="0" <#if (data.NestCondition == "0") > selected="selected" </#if> >Pesimiskelpoiset</option>
        <option value="1" <#if (data.NestCondition == "1") > selected="selected" </#if> >Pesimiskelvottomat</option>
        <option value="2" <#if (data.NestCondition == "2") > selected="selected" </#if> >Kaikki</option>
      </select>
      
      <table>
        <tr>
          <td>Koordinaattihaun säde: </td>
        </tr>
        <tr>
          <td>  
            <@coordinateSelection errors "advanced" />
          </td>
        </tr>
      </table>   
    
    </td>
    
  </tr>
</table>

<input type="submit" value="${texts.search}" onclick=" generateAdvancedSearch(); if (confirmSearch(document.advancedSearch, '${texts.search_confirm_search_all}')) document.advancedSearch.submit(); else return false;" />
<input type="submit" value="${texts.clear}"  onclick="document.clear.submit(); return false; " />   
<input type="submit" name="print_to_textfile" value="Tulosta vastaukset tekstitiedostoon" onclick=" generateAdvancedSearch(); if (confirmSearch(document.advancedSearch, 'Oletko varma että haluat tulostaa kaiken?')) document.advancedSearch.submit(); else return false;" />

</form>

<br />

</div>

<#-- ------------------------- advanced search ends  --------------------------------------------------------------------------------------------- -->
<#-- --------------------------------------------------------------------------------------------------------------------------------------------- -->



<#include "footer.ftl">

