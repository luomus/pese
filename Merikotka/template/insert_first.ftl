<#include "navi.ftl">
<#include "macro.ftl">

<div class="tablemanagement">

<h1>${texts.tarkastus_title_insert_first}</h1> 
<br />
<@errorlist errors texts />

<form action="${system.baseURL}" method="post">
<input type="hidden" name="page"   value="${system.page}" />
<input type="hidden" name="action" value="check" />

  <table>
    <tr>
      <td>Reviiri: </td>
      <td><@selection selections "updateparameters" updateparameters.vuosi.reviiri_id 1 false true false errors /></td>				
    </tr>
    <tr>
      <td>Pesintävuosi:</td>
      <td><@inputText "updateparameters" updateparameters.tarkastus.tark_vuosi true errors /></td>
    </tr>
    <tr>
      <td colspan="2"><input type="submit" value="Etene" /></td>
    </tr>
  </table>
  
</form>


</div>
</body>
</html>
