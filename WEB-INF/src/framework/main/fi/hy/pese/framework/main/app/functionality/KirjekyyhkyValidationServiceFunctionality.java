package fi.hy.pese.framework.main.app.functionality;

import java.util.Map;

import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.app.functionality.validate._Validator;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data._Row;
import fi.luomus.commons.xml.Document;
import fi.luomus.commons.xml.Document.Node;
import fi.luomus.commons.xml.XMLWriter;

public abstract class KirjekyyhkyValidationServiceFunctionality<T extends _Row> extends BaseFunctionality<T> {

	public KirjekyyhkyValidationServiceFunctionality(_Request<T> request) {
		super(request);
	}

	@Override
	public abstract _Validator validator() throws Exception;

	@Override
	public void preProsessingHook() throws Exception {
		data.setAction(Const.INSERT);
		data.setPage(Const.TARKASTUS_PAGE);
	}

	@Override
	public void afterSuccessfulValidation() throws Exception {
		String xml = noErrorsMessage();
		data.setXMLOutput(xml);
	}

	private String noErrorsMessage() {
		Document document = new Document("validation-response");
		document.getRootNode().addAttribute("errors", "no").addAttribute("warnings", "no");
		String xml = new XMLWriter(document).generateXML();
		return xml;
	}

	//
	//	<validation-response errors="no" warnings="no" />
	//
	//
	//	<validation-response errors="yes">
	//		<error field="pesa.nimi" text="Pesän nimi on liian pitkä" />
	//		<error field="reviiri" text="Pakollinen kenttä" />
	//	</validation-response>

	@Override
	public void afterFailedValidation() throws Exception {
		String xml = errorsAsXML();
		data.setXMLOutput(xml);
	}

	private String errorsAsXML() {
		Map<String, String> texts = data.uiTexts();
		Document document = new Document("validation-response");
		Node root = document.getRootNode();

		if (data.hasErrors()) {
			root.addAttribute("errors", "yes");
			Node errors = root.addChildNode("errors");
			for (Map.Entry<String, String> e : data.errors().entrySet()) {
				Node errorNode = errors.addChildNode("error");
				addTexts(texts, e, errorNode);
			}
		} else {
			root.addAttribute("errors", "no");
		}

		if (data.hasWarnings()) {
			root.addAttribute("warnings", "yes");
			Node warnings = root.addChildNode("warnings");
			for (Map.Entry<String, String> e : data.warnings().entrySet()) {
				Node warningNode = warnings.addChildNode("warning");
				addTexts(texts, e, warningNode);
			}
		} else {
			root.addAttribute("warnings", "no");
		}

		String xml = new XMLWriter(document).generateXML();
		return xml;
	}

	private void addTexts(Map<String, String> texts, Map.Entry<String, String> e, Node errorNode) {
		String fieldName = e.getKey();
		String fieldTitle = texts.get(fieldName);
		String errorText = texts.get(e.getValue());
		if (notGiven(fieldTitle)) {
			request.getErrorReporter().report("Localized text was not found field name: " +fieldName);
			fieldTitle = fieldName;
		}
		if (notGiven(errorText)) {
			request.getErrorReporter().report("Localized text was not found for error: " +e.getValue());
			errorText = e.getValue();
		}
		errorNode.addAttribute("field-name", fieldName);
		errorNode.addAttribute("field-title", fieldTitle);
		errorNode.addAttribute("text", errorText);
	}

	private boolean notGiven(String value) {
		return value == null || value.length() < 1;
	}

	@Override
	public void postProsessingHook() throws Exception {
		data.setPage(Const.XML_OUTPUT_PAGE);
	}
}
