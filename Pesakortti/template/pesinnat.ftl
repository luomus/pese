
<#include "header.ftl">


<h1>${texts.pesintamanagement_title}</h1>

<br />

<@statustexts system texts />
<#if data.new_nest_id??>
  <p>Uuden pesän ID:ksi tuli:  <a href="${system.baseURL}?page=search&amp;action=search&amp;searchparameters.pesa.id=${data.new_nest_id}">${data.new_nest_id}</a></p>
  <br />
</#if>

<@errorlist errors texts />

<p>Tämän toiminnon avulla siirretään tai poistetaan tarkastuksia, tai poistetaan pesiä. Aloita valitsemalla pesä. </p>

<form action="${system.baseURL}" method="post" name="search">
<input type="hidden" name="page"   value="${system.page}" />
<input type="hidden" name="action" value="select_nest" />

<table>
  <tr>
    <td>Pesän ID:</td>
    <td> 
      <#assign pesaidColumn = updateparameters.pesa.getUniqueNumericIdColumn() />
      <@inputText "updateparameters" pesaidColumn true errors />
    </td>
    <td>
    	<input type="submit" value="Etene" />
    </td>
  </tr>
</table>

</form>


<#if system.action == "select_nest">

  <br />

  <table>
    <caption>Pesä</caption>
    <tr>
      <th>Kunta</th> <th>Tarkempi paikka</th> <th>ID</th>  
      <td rowspan="2">
      
        <form action="${system.baseURL}" method="post">
        <input type="hidden" name="page"   value="${system.page}" />
        <input type="hidden" name="action" value="delete_nest" />
        <@inputHidden "updateparameters" updateparameters.pesa.id />
        
        Poista tämä pesä ja kaikki sen tarkastukset 
        <input type="submit" onclick="return confirm('Oletko varma?');" value="Poista" />
        
        </form>
        
      </td>
    </tr>
    <tr>   
    
      <#assign kunta = results[0].pesa.kunta> 
      <td> ${selections[kunta.assosiatedSelection][kunta.value]} </td>
      <td> ${results[0].pesa.tarkempi_paikka} </td>
      <td> ${results[0].pesa.id} </td>
      
    </tr>
  </table>
  
  <br />
  
  <#if results??>
  
    <form action="${system.baseURL}" method="post" name="tarkastuslistform">
    <input type="hidden" name="page"   value="${system.page}" />
    <input type="hidden" name="action" value="" id="action" />
    <@inputHidden "updateparameters" updateparameters.pesa.getUniqueNumericIdColumn() />
    
      <table>
        <caption>Pesän tarkastukset</caption>
        <tr>
          <th> &nbsp; </th> <th>Vuosi</th> <th>Pesivä laji</th> <th>Pesimistulos</th>
        </tr>
        
      	<#list results as i>
      	  <tr>
      	    <td> <input type="checkbox" name="updateparameters.tarkastus.id" value="${i.tarkastus.getUniqueNumericIdColumn()}" /> </td>
      	    <td> ${i.tarkastus.vuosi} </td>
      	    <td> ${i.tarkastus.pesiva_laji} </td>
      	    <td> ${i.tarkastus.pesimistulos} </td>
          </tr>
        </#list>
      </table>
      
      <br />
      
      <table>
        <tr>
          <td> <b>Poista</b> kaikki valitut tarkastukset. </td> 
          <td> <input type="submit" onclick="document.tarkastuslistform['action'].value = 'delete_tarkastukset'; return confirm('Haluatko varmasti poistaa valitut tarkastukset?');" value="Poista" /> </td>
          <td class="ohje"> (Huom: Jos halutaan poistaa kaikki tarkastukset täytyy poistaa koko pesä) </td>
        </tr>
        <tr>
          <td> <b>Siirrä</b> kaikki valitut tarkastukset <b>toiseen pesään</b>. 
               Pesän ID: <input type="text" size="7" name="move_to_nest_id" <@checkerror errors "move_to_nest_id" /> value="${(data.move_to_nest_id)!""}" /> 
          </td> 
          <td> <input type="submit" onclick="document.tarkastuslistform['action'].value = 'move_tarkastukset'; return confirm('Haluatko varmasti siirtää valitut tarkastukset valitsemaasi pesään?');" value="Siirrä" /> </td>
          <td class="ohje"> (Huom: Jos siirretään kaikki tarkastukset, lähtöpesä poistetaan kokonaan) </td>
        </tr>
        <tr>
          <td> <b>Siirrä</b> kaikki valitut tarkastukset <b>uuteen pesään</b>. <br />Uuden pesän perustiedoiksi kopioidaan nykyisen pesän perustiedot.</td> 
          <td> <input type="submit" onclick="document.tarkastuslistform['action'].value = 'move_tarkastukset_to_new'; return confirm('Haluatko varmasti siirtää valitut tarkastukset uuteen pesään?');" value="Siirrä" /> </td>
          <td class="ohje"> (Huom: Jos siirretään kaikki tarkastukset, lähtöpesä poistetaan kokonaan) </td>
        </tr>
      </table>
      
   	</form>
   	
  </#if>
  
</#if>





<#include "footer.ftl">