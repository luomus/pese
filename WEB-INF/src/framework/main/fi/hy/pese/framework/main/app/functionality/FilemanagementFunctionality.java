package fi.hy.pese.framework.main.app.functionality;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.app.functionality.validate.FilemanagementFunctionalityValidator;
import fi.hy.pese.framework.main.app.functionality.validate._Validator;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data._Row;
import fi.luomus.commons.services.FileServlet;
import fi.luomus.commons.services.FileServlet.FileRequest;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.FileCompresser;
import fi.luomus.commons.utils.Utils;

public class FilemanagementFunctionality<T extends _Row> extends BaseFunctionality<T> {

	private static final String	COMPRESSED_FILES_PREFIX	= "selected_files_";
	public static final String PRODUCED_PDF_FILES = "produced_pdf_files";
	public static final String PRODUCED_REPORT_FILES = "produced_report_files";
	public static final String PDF_FOLDER = "pdf";
	public static final String REPORT_FOLDER = "report";
	public static final String FOLDER = "folder";
	public static final String FILENAME = "filename";

	private final _FileListGenerator filelistGenerator;

	public interface _FileListGenerator {
		List<File> pdfFiles();
		List<File> reportFiles();
	}
	public static class FileListGeneratorImple implements _FileListGenerator {
		private final String pdfFolder;
		private final String reportFolder;
		public FileListGeneratorImple(String pdfFolder, String reportFolder) {
			this.pdfFolder = pdfFolder;
			this.reportFolder = reportFolder;
		}
		@Override
		public List<File> pdfFiles() {
			return listFiles(pdfFolder);
		}
		@Override
		public List<File> reportFiles() {
			return listFiles(reportFolder);
		}
		private List<File> listFiles(String folderPath) {
			List<File> files = new ArrayList<>();
			if (folderPath == null) return files;
			java.io.File folder = new java.io.File(folderPath);
			folder.mkdirs();
			for (java.io.File f : folder.listFiles()) {
				if (f.isDirectory()) continue;
				files.add(new File(f));
			}
			java.util.Collections.sort(files, new Comparator<File>() {
				@Override
				public int compare(File o1, File o2) {
					return o2.date.compareTo(o1.date);
				}
			});
			return files;
		}
	}

	public static class File {
		private final String name;
		private final String date;
		public File(String name, String date) {
			this.name = Utils.toHTMLEntities(name);
			this.date = Utils.toHTMLEntities(date);
		}
		public File(java.io.File file) {
			this.name = Utils.toHTMLEntities(file.getName());
			this.date = Utils.toHTMLEntities(DateUtils.format(file.lastModified(), "yyyy-MM-dd HH:mm:ss"));
		}
		public String getName() {
			return name;
		}
		public String getDate() {
			return date;
		}
	}

	public FilemanagementFunctionality(_Request<T> request, _FileListGenerator filelistGenerator) {
		super(request);
		this.filelistGenerator = filelistGenerator;
	}

	@Override
	public _Validator validator() {
		return new FilemanagementFunctionalityValidator<>(request);
	}

	@Override
	public void preProsessingHook() throws Exception {
		if (!data.contains(FOLDER)) {
			data.set(FOLDER, REPORT_FOLDER);
		}
	}

	@Override
	public void afterSuccessfulValidation() throws Exception {
		String folderType = folderType();
		String folder = folderFor(folderType);
		String filename = data.get(FilemanagementFunctionality.FILENAME);
		if (data.action().equals(Const.DELETE)) {
			for (String file : Utils.valuelist(filename)) {
				deleteFile(folder, file);
			}
		}
		if (data.action().equals(Const.SHOW)) {
			showFile(folder, filename);
		}
		if (data.action().equals(Const.COMPRESS_AND_SHOW)) {
			compressAndShow(folder, filename);
		}
	}

	private void compressAndShow(String folder, String filename) throws Exception {
		java.io.File destination = new java.io.File(filename(folder, data.userId()+"_"+COMPRESSED_FILES_PREFIX+DateUtils.getFilenameDatetime()+".zip"));
		FileCompresser compresser = new FileCompresser(destination);
		for (String file : Utils.valuelist(filename)) {
			if (!compresser.addToZip(new java.io.File(filename(folder, file)))) {
				data.addToNoticeTexts("Failed to compress "+file);
			}
		}
		compresser.close();
		showFile(folder, destination.getName());
	}
	
	private void deleteFile(String folder, String filename) {
		java.io.File f = new java.io.File(filename(folder, filename));
		if (f.exists()) {
			f.delete();
		}
	}
	
	private void showFile(String folder, String filename) throws Exception {
		FileRequest fileRequest = createFilerequest(folder, filename);
		String token =  FileServlet.authorizeNewFileRequest(fileRequest);
		request.redirecter().redirectTo(config.baseURL()+"/File/"+filename+"?token="+token);
	}

	private String folderType() throws IllegalArgumentException {
		if (!data.contains(FilemanagementFunctionality.FOLDER)) {
			throw new IllegalArgumentException("Folder type not defined");
		}
		String folderType = data.get(FilemanagementFunctionality.FOLDER);
		return folderType;
	}

	private String folderFor(String folderType) throws Exception {
		String folder = "";
		if (folderType.equals(FilemanagementFunctionality.PDF_FOLDER)) {
			folder = config.pdfFolder();
		} else if (folderType.equals(FilemanagementFunctionality.REPORT_FOLDER)) {
			folder = config.reportFolder();
		} else {
			throw new Exception("Folder type: " +folderType +" is undefined");
		}
		return folder;
	}
	
	private FileRequest createFilerequest(String folder, String filename) throws Exception {
		FileRequest fileRequest;
		if (filename.toLowerCase().endsWith(".pdf")) {
			fileRequest = new FileRequest(filename(folder, filename), FileRequest.CONTENT_TYPE.PDF);
		} else if (filename.toLowerCase().endsWith(".txt") || filename.toLowerCase().endsWith(".xml")) {
			fileRequest = new FileRequest(filename(folder, filename), FileRequest.CONTENT_TYPE.PLAINTEXT, config.characterEncoding());
		} else if (filename.toLowerCase().endsWith(".zip")) {
			fileRequest = new FileRequest(filename(folder, filename), FileRequest.CONTENT_TYPE.ZIP);
		} else {
			throw new Exception("No file type defined for filename: " +filename);
		}
		return fileRequest;
	}

	private String filename(String folder, String filename) {
		return folder + java.io.File.separatorChar + filename;
	}

	@Override
	public void afterFailedValidation() throws Exception {
		data.setAction("");
	}

	@Override
	public void postProsessingHook() throws Exception {
		if (!data.action().equals(Const.SHOW)) {
			generateFileLists();
		}
	}

	private void generateFileLists() {
		List<File> pdfList = filelistGenerator.pdfFiles();
		List<File> reportList = filelistGenerator.reportFiles();
		for (File f : pdfList) {
			data.addToList(PRODUCED_PDF_FILES, f);
		}
		for (File f : reportList) {
			data.addToList(PRODUCED_REPORT_FILES, f);
		}
	}


}
