package fi.hy.pese.framework.main.general;

import java.util.ArrayList;
import java.util.List;

import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._Table;
import fi.luomus.commons.utils.LogUtils;
import fi.luomus.commons.utils.Utils;

public class Logger implements _Logger {

	private final List<String>	logMessages;
	private final String		userId;
	private final String		logFolder;
	private final String		filePrefix;

	public Logger(String userId, String logFolder, String filePrefix) {
		this.logMessages = new ArrayList<>(80);
		this.userId = userId;
		this.logFolder = logFolder;
		this.filePrefix = filePrefix;
	}

	@Override
	public void commitMessages() {
		if (!logMessages.isEmpty()) {
			LogUtils.write(logMessages.toString(), userId, logFolder, filePrefix);
			logMessages.clear();
		}
	}

	@Override
	public void log(String message) {
		logMessages.add(message);
	}

	@Override
	public void logInsert(_Table table) {
		logMessages.add("INSERTS ROW TO " + table.getFullname());
		for (_Column c : table) {
			if (c.getFieldType() == _Column.ROWID) continue;
			if (!c.hasValue()) continue;
			logMessages.add(c.getName() + "=" + c.getValue());
		}
	}

	@Override
	public void logUpdate(_Table oldValues, _Table newValues) {
		logMessages.add("UPDATES ROW OF " + newValues.getFullname() + " (" + keys(oldValues) + ")");
		for (_Column newValueColumn : newValues) {
			if (newValueColumn.getFieldType() == _Column.ROWID) continue;
			if (newValueColumn.getFieldType() == _Column.DATE_ADDED_COLUMN) continue;
			if (newValueColumn.getFieldType() == _Column.DATE_MODIFIED_COLUMN) continue;
			if (newValueColumn.getFieldType() == _Column.UNIQUE_NUMERIC_INCREASING_ID) continue;
			String name = newValueColumn.getName();
			String oldValue = oldValues.get(name).getValue();
			String newValue = newValueColumn.getValue();
			if (!newValue.equals(oldValue)) {
				logMessages.add(name + ": " + oldValue + " => " + newValue);
			}
		}
	}

	private StringBuilder keys(_Table oldValues) {
		StringBuilder keys = new StringBuilder();
		for (_Column c : oldValues) {
			if (isKey(c)) keys.append(c.getValue()).append(",");
		}
		Utils.removeLastChar(keys);
		return keys;
	}

	private boolean isKey(_Column c) {
		return c.getFieldType() == _Column.UNIQUE_NUMERIC_INCREASING_ID || c.getFieldType() == _Column.IMMUTABLE_PART_OF_A_KEY;
	}

	@Override
	public void logDelete(_Table table) {
		logMessages.add("DELETES ROW FROM " + table.getFullname());
		logAllValues(table);
	}

	private void logAllValues(_Table table) {
		for (_Column c : table) {
			if (c.getFieldType() == _Column.ROWID) continue;
			logMessages.add(c.getName() + "=" + c.getValue());
		}
	}

}
