package fi.hy.pese.merikotka.main.functionality;

import java.sql.SQLException;

import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.app.functionality.TarkastusFunctionality;
import fi.hy.pese.framework.main.app.functionality.validate._Validator;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._IndexedTablegroup;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.main.general.data._Table;
import fi.hy.pese.merikotka.main.app.MerikotkaConst;
import fi.hy.pese.merikotka.main.db.MerikotkaDAO;
import fi.hy.pese.merikotka.main.functionality.validation.MerikotkaTarkastusValidator;
import fi.luomus.commons.containers.DateValue;
import fi.luomus.commons.utils.CoordinateConverter;
import fi.luomus.commons.utils.Utils;

public class MerikotkaTarkastusFunctionality extends TarkastusFunctionality {

	public MerikotkaTarkastusFunctionality(_Request<_PesaDomainModelRow> request) {
		super(request);
	}

	@Override
	public _Validator validator() {
		return new MerikotkaTarkastusValidator(request);
	}

	@Override
	public void applicationSpecificDatamanipulation() throws Exception {
		String action = data.action();
		if (action.equals(Const.FILL_FOR_INSERT)) {
			selectWhatResultToUseForThisTarkYear();
			moveTarkVuosiFromDataToUpdateparameters();
			MerikotkaConst.removeTarkastusValuesButKeepSpecific(data.updateparameters().getTarkastus());
		} else if (action.equals(Const.FILL_FOR_INSERT_FIRST)) {
			preselectKunnat();
		} else if (action.equals(Const.INSERT_FIRST) || action.equals(Const.INSERT) || action.equals(Const.UPDATE)) {
			preInsertUpdate(action);
		}
	}

	private void selectWhatResultToUseForThisTarkYear() throws Exception {
		if (data.results().size() < 1) throw new Exception("No results");
		int tark_vuosi = Integer.valueOf(data.get("tark_vuosi"));
		_PesaDomainModelRow useAsBase = null;
		for (_PesaDomainModelRow row : data.results()) {
			useAsBase = row;
			int row_tark_vuosi = Integer.valueOf(row.getTarkastus().get("tark_vuosi").getValue());
			if (tark_vuosi > row_tark_vuosi) break;
		}
		if (useAsBase == null) throw new IllegalStateException();
		if (useAsBase == lastRow(data.results())) {
			useAsBase.getOlosuhde().setRowidValue("");
		}
		data.setUpdateparameters(data.copyOf(useAsBase));
	}
	
	private void preInsertUpdate(String action) throws Exception, SQLException {
		_PesaDomainModelRow params = data.updateparameters();
		convertCoordinates(params.getPesa());
		setVuosi(params);
		setIds(action, params);
		setPoikanenValues(params);
		ifChangesToReviiriKuntaUpdateAllReviiriKunnatForVuosi(params);
		_Column seurTark = params.getPesa().get("seur_tarkastaja");
		if (!seurTark.hasValue()) {
			seurTark.setValue(params.getTarkastus().get("tarkastaja1_id"));
		}
	}

	private void ifChangesToReviiriKuntaUpdateAllReviiriKunnatForVuosi(_PesaDomainModelRow params) throws SQLException {
		_Table vuosi = params.getVuosi();
		String prev_reviiri_kunta = MerikotkaDAO.returnKuntaForReviiri(vuosi.get("reviiri_id").getValue(), dao);
		if (!prev_reviiri_kunta.equals(vuosi.get("reviirin_kunta").getValue())) {
			MerikotkaDAO.updateReviiriKunnatForVuosi(vuosi.get("reviiri_id").getValue(), vuosi.get("vuosi").getValue(), vuosi.get("reviirin_kunta").getValue(), dao);
		}
	}

	private void setIds(String action, _PesaDomainModelRow params) throws SQLException {
		String pesa_id = null;
		String tarkastus_id = null;
		if (action.equals(Const.INSERT_FIRST)) {
			pesa_id = dao.returnAndSetNextId(params.getPesa());
			tarkastus_id = dao.returnAndSetNextId(params.getTarkastus());
		} else if (action.equals(Const.INSERT)) {
			pesa_id = params.getPesa().get("pesa_id").getValue();
			tarkastus_id = dao.returnAndSetNextId(params.getTarkastus());
		} else if (action.equals(Const.UPDATE)) {
			pesa_id = params.getPesa().get("pesa_id").getValue();
			tarkastus_id = params.getTarkastus().get("p_tarkastus_id").getValue();
		}
		params.getVuosi().get("pesa_id").setValue(pesa_id);
		params.getOlosuhde().get("pesa_id").setValue(pesa_id);
		params.getTarkastus().get("pesa_id").setValue(pesa_id);

		tarkIdTo(params.getPoikaset(), tarkastus_id);
		tarkIdTo(params.getAikuiset(), tarkastus_id);
	}

	private void tarkIdTo(_IndexedTablegroup map, String tarkastus_id) {
		for (_Table t : map.values()) {
			if (t.hasValues()) {
				t.get("tarkastus_id").setValue(tarkastus_id);
			}
		}
	}

	private void setPoikanenValues(_PesaDomainModelRow params) {
		String maxIka = "";
		boolean toinenMittausGiven = false;
		for (_Table poikanen : params.getPoikaset().values()) {
			if (poikanen.hasValues()) {
				if (poikanen.get("siipi_pituus").hasValue()) {
					String ika = MerikotkaConst.poikanenIka(poikanen.get("siipi_pituus").getValue(), poikanen.get("sukupuoli").getValue());
					poikanen.get("poikasen_ika").setValue(ika);
					maxIka = Utils.maxOf(maxIka, ika);
				}
				if (poikanen.get("siipi_pituus_2").hasValue()) {
					String ika = MerikotkaConst.poikanenIka(poikanen.get("siipi_pituus_2").getValue(), poikanen.get("sukupuoli").getValue());
					poikanen.get("poikasen_ika_2").setValue(ika);
					maxIka = Utils.maxOf(maxIka, ika);
					toinenMittausGiven = true;
				}
			}
		}

		DateValue mittaus_pvm_1 = params.getTarkastus().get("mittaus_pvm_1").getDateValue();
		DateValue mittaus_pvm_2 = params.getTarkastus().get("mittaus_pvm_2").getDateValue();
		DateValue mittausPvm = toinenMittausGiven ? mittaus_pvm_2 : mittaus_pvm_1;
		String kuoriutumispvm = MerikotkaConst.kuoriutumispaiva(mittausPvm, maxIka);
		params.getTarkastus().get("kuoriutumispaiva").setValue(kuoriutumispvm);
	}

	private void setVuosi(_PesaDomainModelRow params) {
		String vuosi = params.getTarkastus().get("tark_vuosi").getValue();
		params.getOlosuhde().get("havaintojen_vuosi").setValue(vuosi);
		params.getVuosi().get("vuosi").setValue(vuosi);
	}

	private void convertCoordinates(_Table pesa) throws Exception {
		CoordinateConverter converter = new CoordinateConverter();
		switch (pesa.get("koord_tyyppi").getValue().charAt(0)) {
			case 'Y':
				converter.setYht_leveys(pesa.get("yht_leveys").getValue());
				converter.setYht_pituus(pesa.get("yht_pituus").getValue());
				break;
			case 'E':
				converter.setEur_leveys(pesa.get("eur_leveys").getValue());
				converter.setEur_pituus(pesa.get("eur_pituus").getValue());
				break;
			case 'A':
				converter.setAst_leveys(pesa.get("ast_leveys").getValue());
				converter.setAst_pituus(pesa.get("ast_pituus").getValue());
				break;
		}
		converter.convert();
		pesa.get("yht_leveys").setValue(converter.getYht_leveys().toString());
		pesa.get("yht_pituus").setValue(converter.getYht_pituus().toString());
		pesa.get("eur_leveys").setValue(converter.getEur_leveys().toString());
		pesa.get("eur_pituus").setValue(converter.getEur_pituus().toString());
		pesa.get("ast_leveys").setValue(converter.getAst_leveys().toString());
		pesa.get("ast_pituus").setValue(converter.getAst_pituus().toString());
		pesa.get("des_leveys").setValue(converter.getDes_leveys().toString());
		pesa.get("des_pituus").setValue(converter.getDes_pituus().toString());
	}

	private void moveTarkVuosiFromDataToUpdateparameters() {
		_Column tark_vuosi = data.updateparameters().getTarkastus().get("tark_vuosi");
		tark_vuosi.setValue(data.get("tark_vuosi"));
	}

	private void preselectKunnat() throws SQLException {
		_Table vuosi = data.updateparameters().getVuosi();
		String kunta = MerikotkaDAO.returnKuntaForReviiri(vuosi.get("reviiri_id").getValue(), dao);
		vuosi.get("reviirin_kunta").setValue(kunta);
		vuosi.get("pesan_kunta").setValue(kunta);
	}

	@Override
	protected void doInsertFirst() throws Exception {
		_PesaDomainModelRow params = data.updateparameters();
		dao.executeInsert(params.getPesa());
		dao.executeInsert(params.getVuosi());
		fetchIdAndInsert(params.getOlosuhde());
		dao.executeInsert(params.getTarkastus());
		insertPoikaset(params);
		insertAikuiset(params);
	}

	@Override
	protected void doInsert() throws Exception {
		_PesaDomainModelRow params = data.updateparameters();
		updateIfChanges(params.getPesa());
		dao.executeInsert(params.getVuosi());
		insertIfChanges(params.getOlosuhde(), "havaintojen_vuosi");
		dao.executeInsert(params.getTarkastus());
		insertPoikaset(params);
		insertAikuiset(params);
	}

	@Override
	protected void doUpdate() throws Exception {
		_PesaDomainModelRow params = data.updateparameters();
		updateIfChanges(params.getPesa());
		updateIfChanges(params.getVuosi());
		updateOrInsertOlosuhdeIfChanges(params, "havaintojen_vuosi");
		updateIfChanges(params.getTarkastus());
		updatePoikaset(params);
		updateAikuiset(params);
	}

	@Override
	protected boolean prevOlosuhdeIsForThisYear(_Table current_tarkastus, _Table prev_olosuhde) {
		return prev_olosuhde.get("havaintojen_vuosi").getValue().equals(current_tarkastus.get("tark_vuosi").getValue());
	}

}
