package fi.hy.pese.merikotka.main.app;

import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.main.general.data._Table;
import fi.luomus.commons.containers.DateValue;
import fi.luomus.commons.utils.CoordinateConverter;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.Utils;

public class MerikotkaConst {

	public static final String		NO												= "E";
	public static final String		YES												= "K";

	public static final Collection<String>	TARKASTUS_FIELDS_TO_BE_PREFILLED_FOR_INSERTS	= Utils.collection(
			"tark_vuosi", "pesa_korkeus", "pesa_halk_max", "pesa_halk_min", "et_tie", "et_moottorikelkka", "et_kalaviljely", "et_as", "as_lkm_500", "as_lkm_1000");


	public static final String[]	ETAISYYS_FIELDS	= {
	                            	               	   "pesavakio.et_meri", "pesavakio.et_jarvi", "pesamuuttuva.et_avosuo", "pesatarkastus.et_as", "pesatarkastus.et_tie", "pesatarkastus.et_moottorikelkka",
	                            	               	   "pesatarkastus.et_kalaviljely", "pesamuuttuva.et_ilmajohto", "pesamuuttuva.et_viljapelto", "pesamuuttuva.et_avohakkuu", "pesamuuttuva.et_siemenpuusto"
	};

	public static final String		TAULUT											= "taulut";
	public static final String		REVIIRIT										= "reviirit";
	public static final String		SUURALUEET										= "suuralueet";
	public static final String		APUTAULU										= "aputaulu";
	public static final String		YES_NO_SELECTION								= "k_e";
	public static final String		KOIRAS											= "K";
	public static final String		NAARAS											= "N";
	public static final String		LUULTAVASTI_KOIRAS								= "L";
	public static final String		LUULTAVASTI_NAARAS								= "O";

	public static final int[]		KOIRAS_SIIPI_TO_IKA_TABLE						= {
	                         		                         						   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 25, 34, 43, 52, 62, 71, 80, 89, 98, 108, 117, 126, 135, 144, 154, 163, 172, 181, 190, 200, 209, 218, 227, 236, 246, 255, 264, 273, 282, 291, 300,
	                         		                         						   310, 319, 328, 337, 346, 356, 365, 374, 383, 392, 402, 411, 420, 429, 438, 448, 457, 466, 475, 484, 494, 503, 512, 521, 530, 540, 549, 558, 567, 576
	};
	public static final int[]		NAARAS_SIIPI_TO_IKA_TABLE						= {
	                         		                         						   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 52, 60, 69, 77, 86, 94, 103, 111, 120, 128, 136, 145, 153, 161, 170, 179, 187, 196, 204, 212, 221, 229, 238, 246, 255, 263, 271, 280, 288, 297,
	                         		                         						   305, 314, 322, 331, 339, 348, 356, 364, 373, 381, 390, 398, 407, 415, 424, 432, 441, 449, 458, 466, 474, 483, 491, 500, 508, 517, 525, 534, 542, 551, 559
	};
	public static final int[]		TUNTEMATON_SIIPI_TO_IKA_TABLE					= {
	                         		                             					   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 40, 49, 58, 67, 76, 85, 94, 102, 111, 120, 129, 138, 147, 155, 164, 173, 182, 191, 199, 208, 217, 226, 235, 244, 253, 261, 270, 279, 288, 297, 306,
	                         		                             					   314, 323, 332, 341, 350, 358, 367, 376, 385, 394, 403, 412, 420, 429, 438, 447, 456, 465, 474, 482, 491, 500, 509, 518, 526, 535, 544, 553, 562, 571
	};
	public static final Calendar	NOLLAPVM										= new GregorianCalendar(2000, 2, 1);

	public static final String		PREV_TARKASTUS_PESA_KUNTO						= "prev_tarkastus_pesa_kunto";
	public static final String		PREV_TARKASTUS_PESIMISTULOS						= "prev_tarkastus_pesimistulos";
	public static final String		PREV_TARKASTUS_TARKASTAJA						= "prev_tarkastus_tarkastaja";
	public static final String		PREV_TARKASTUS_TARK_PVM							= "prev_tarkastus_tark_pvm";
	public static final String		PREV_PARAS_TARKASTUS_PESIMISTULOS				= "prev_paras_tarkastus_pesimistulos";
	public static final String		PREV_PARAS_TARKASTUS_PESA_ID					= "prev_paras_tarkastus_pesa_id";
	public static final String		PREV_PARAS_TARKASTUS_VUOSI						= "prev_paras_tarkastus_vuosi";
	public static final String		REVIIRILISTA									= "reviirilista";
	public static final String		REVIIRILISTA_K_E								= "reviirilista_k_e";
	public static final String		PESA_KOORD_TYYPPI_YHT							= "pesa.koord_tyyppi.yht";
	public static final String		PESA_KOORD_TYYPPI_EUR							= "pesa.koord_tyyppi.eur";
	public static final String		PESA_KOORD_TYYPPI_AST							= "pesa.koord_tyyppi.ast";
	public static final String		PESA_KOORD_LEVEYS								= "pesa.koord_leveys";
	public static final String		PESA_KOORD_PITUUS								= "pesa.koord_pituus";
	public static final String		PESA_LUONNONPESA_K_E							= "pesa.luonnonpesa_k_e";
	public static final String		PESA_TEKOPESA_K_E								= "pesa.tekopesa_k_e";
	public static final String		PESA_PESANIMI_2									= "pesa.pesanimi_2";

	/**
	 * Laskee poikasen iän (päivinä) siiven pituuden ja poikasen sukupuolen (jos tiedossa) perusteella
	 * @param siipi
	 * @param sukupuoli
	 * @return ikä päivinä
	 */
	public static String poikanenIka(String siipi, String sukupuoli) {
		int thisSiipi = Integer.valueOf(siipi);
		if (thisSiipi < 1) return "0";

		int[] siipiToIkaTable = MerikotkaConst.TUNTEMATON_SIIPI_TO_IKA_TABLE;
		if (sukupuoli.equals(MerikotkaConst.KOIRAS)) siipiToIkaTable = MerikotkaConst.KOIRAS_SIIPI_TO_IKA_TABLE;
		if (sukupuoli.equals(MerikotkaConst.NAARAS)) siipiToIkaTable = MerikotkaConst.NAARAS_SIIPI_TO_IKA_TABLE;

		int index = -1;
		for (int siipiFromTable : siipiToIkaTable) {
			if (siipiFromTable > thisSiipi) break;
			index++;
		}
		if (index == -1) return "0";
		if (siipiToIkaTable[index] == 0) return "0";
		return Integer.toString(index);
	}

	/**
	 * Palauttaa kuoriutumispäivän ja ns. "nollapäivän" välisen eron päivinä.
	 * Nollapäivä on vuoden 2000 maaliskuun ensimmäisen päivän järjestysluku.
	 * Kuoriutumispäivä on mittauspäivämäärän ja siipien koosta lasketun poikasen iän erotus.
	 * Palauttaa "" jos mittauspäivämäärä tai mitat ovat virhellisiä tai niitä ei ole annettu.
	 * @param mittausPvm se mittauspäivämäärä jona annetut siivenmitat on mitattu
	 * @param ika ikä päivissä joka on laskettu annetuista siivenmitoista
	 * @return erotus nollapäivään verrattuna, voi siis olla myös negatiivinen
	 */
	public static String kuoriutumispaiva(DateValue mittausPvm, String... ika) {
		String maxIka = Utils.maxOf(ika);
		if (maxIka.length() < 1) return "";
		Calendar mittaus = new GregorianCalendar();
		try {
			mittaus.setTime(DateUtils.convertToDate(mittausPvm));
		} catch (Exception e) {
			return "";
		}
		Integer kuoriutumispvm = mittaus.get(Calendar.DAY_OF_YEAR) - Integer.valueOf(maxIka);
		Integer erotus = kuoriutumispvm - MerikotkaConst.NOLLAPVM.get(Calendar.DAY_OF_YEAR);
		return erotus.toString();
	}

	public static final String	JOINS	= joins();

	private static String joins() {
		StringBuilder joins = new StringBuilder();
		joins.append("       vuosi.pesa_id         =  pesatarkastus.pesa_id                 ");
		joins.append(" AND   reviiri.reviiri_id    =  vuosi.reviiri_id                      ");
		joins.append(" AND   pesavakio.pesa_id     =  pesatarkastus.pesa_id                 ");
		joins.append(" AND   pesatarkastus.pesa_id =  pesamuuttuva.pesa_id                  ");
		joins.append(" AND   vuosi.vuosi           =  pesatarkastus.tark_vuosi              ");
		joins.append(" AND   pesatarkastus.p_tarkastus_id = poikanen.tarkastus_id(+)        ");
		joins.append(" AND   pesatarkastus.p_tarkastus_id = aikuinen.tarkastus_id(+)        ");
		joins.append(" AND   havaintojen_vuosi =                                            ");
		joins.append("           (SELECT MAX(havaintojen_vuosi)                             ");
		joins.append("            FROM   pesamuuttuva o2                                    ");
		joins.append("            WHERE  o2.pesa_id = pesatarkastus.pesa_id                 ");
		joins.append("            AND    havaintojen_vuosi <= pesatarkastus.tark_vuosi  )   ");
		return joins.toString();
	}

	public static final String	ORDER_BY	= orderBy();

	private static String orderBy() {
		return " pesavakio.pesa_id , pesatarkastus.tark_vuosi DESC, poikanen.poikanen_id, aikuinen.aikuinen_id ";
	}

	public static final Collection<String>	PESAN_KUNTO__DEAD							= Utils.collection("Y", "U", "D");
	public static final Collection<String>	PESAN_KUNTO__ALIVE							= Utils.collection("P", "O", "K", "N", "R", "J");
	public static final Collection<String>	PESIMISTULOS_SUCCESS						= Utils.collection("P", "R", "L");
	public static final Collection<String>	PESIMISTULOS_POIKANEN_NOT_ALLOWED			= Utils.collection("A", "K", "J", "M", "U");
	public static final Collection<String>	PESIMISTULOS_MUNA_NOT_ALLOWED				= Utils.collection("A", "K", "J", "U");
	public static final Collection<String>	PESIMISTULOS_A__ALLOWED_NAHNDYT_MERKIT		= Utils.collection("A", "F", "X");
	public static final Collection<String>	PESIMISTULOS_K__ALLOWED_NAHNDYT_MERKIT		= Utils.collection("B", "C", "D", "E", "F", "H", "J", "Y", "X");
	public static final Collection<String>	PESIMISTULOS_J__ALLOWED_NAHNDYT_MERKIT		= Utils.collection("B", "C", "D", "E", "F", "H", "J", "Y", "X");
	public static final Collection<String>	PESIMISTULOS_M__ALLOWED_NAHNDYT_MERKIT		= Utils.collection("H", "J", "K", "M", "N");
	public static final Collection<String>	PESIMISTULOS_P__ALLOWED_NAHNDYT_MERKIT		= Utils.collection("P", "Q", "Y");
	public static final Collection<String>	PESIMISTULOS_R__ALLOWED_NAHNDYT_MERKIT		= Utils.collection("R", "T", "V", "Y");
	public static final Collection<String>	PESIMISTULOS_L__ALLOWED_NAHNDYT_MERKIT		= Utils.collection("V", "Y");
	public static final Collection<String>	PESIMISTULOS_U__ALLOWED_NAHNDYT_MERKIT		= Utils.collection("U");
	public static final Collection<String>	PESIMISTULOS_IN_RISING_SUCCESS_ORDER		= Utils.collection("U", "A", "K", "J", "M", "P", "R", "L", "1", "2", "3", "7", "8", "9");

	public static final String				INVALID_TARK_VUOSI_TARK_PVM_YYYY			= "INVALID_TARK_VUOSI_TARK_PVM_YYYY";
	public static final String				INVALID_RAK_VUOSI							= "INVALID_RAK_VUOSI";
	public static final String				INVALID_LOYT_VUOSI							= "INVALID_LOYT_VUOSI";
	public static final String				INVALID_RAUH_AIKA_ALKU_LOPPU				= "INVALID_RAUH_AIKA_ALKU_LOPPU";
	public static final String				INVALID_TUHOUTUMISVUOSI_PESAN_KUNTO			= "INVALID_TUHOUTUMISVUOSI_PESAN_KUNTO";
	public static final String				INVALID_KORK_TARK							= "INVALID_KORK_TARK";
	public static final String				INVALID_PESIMISTULOS_POIKASET				= "INVALID_PESIMISTULOS_POIKASET";
	public static final String				INVALID_PESIMISTULOS_MUNAT					= "INVALID_PESIMISTULOS_MUNAT";
	public static final String				INVALID_PESIMISTULOS_NAHDYT_MERKIT			= "INVALID_PESIMISTULOS_NAHDYT_MERKIT";
	public static final String				MUULAJI_CAN_NOT_BE_HALALB					= "MUULAJI_CAN_NOT_BE_HALALB";
	public static final String				MUULAJI_RINNAKKAINEN					    = "MUULAJI_RINNAKKAINEN";
	public static final String				INVALID_MUULAJI_PESIMISTULOS				= "INVALID_MUULAJI_PESIMISTULOS";
	public static final String				INVALID_TARK_TAPA							= "INVALID_TARK_TAPA";
	public static final String				REVIIRI_ALREADY_HAS_SUCCESSFUL_NESTING		= "REVIIRI_ALREADY_HAS_SUCCESSFUL_NESTING";
	public static final String				PESA_IS_DEAD_CAN_NOT_INSERT					= "PESA_IS_DEAD_CAN_NOT_INSERT";
	public static final String				NILKKA_MIN_MUST_BE_SMALLER_THAN_NILKKA_MAX	= "NILKKA_MIN_MUST_BE_SMALLER_THAN_NILKKA_MAX";
	public static final String				INVALID_NAHDYT_MERKIT_POIKASCOUNT			= "INVALID_NAHDYT_MERKIT_POIKASCOUNT";
	public static final String				INVALID_REPORT_YEARS						= "INVALID_REPORT_YEARS";
	public static final String				SHOULD_NOT_GIVE_KUVAAJA_UNLESS_VALUE_IS_YES	= "SHOULD_NOT_GIVE_KUVAAJA_UNLESS_VALUE_IS_YES";
	public static final String	RENKAAN_TAYTYY_ALKAA_KIRJAIMELLA	= "RENKAAN_TAYTYY_ALKAA_KIRJAIMELLA";
	public static final String	AIK_LKM_AIKUINEN_INFO_RISTIRIITA	= "AIK_LKM_AIKUINEN_INFO_RISTIRIITA";
	public static final String	RENKAASSA_EI_SAA_OLLA_VALIMERKKEJA	= "RENKAASSA_EI_SAA_OLLA_VALIMERKKEJA";

	public static final String				REVIIRILISTA_KYLLA_EI_TEXT					= "REVIIRILISTA_KYLLA_EI_TEXT";
	public static final String				REVIIRILISTA_EI_VAIHTOPESIA_TEXT			= "REVIIRILISTA_EI_VAIHTOPESIA_TEXT";
	public static final int					REVIIRILISTA_MAX_LENGTH						= 30;

	public static final String				REPORT_PESIMISTULOKSET						= "report_pesimistulokset";
	public static final String				REPORT_REVIIRIT								= "report_reviirit";
	public static final String				REPORT_TARKASTAMATTOMAT						= "report_tarkastamattomat";
	public static final String				REPORT_VIRANOMAIS							= "report_viranomais";
	public static final String	EI_SAA_ILMOITTAA_JOS_PESAA_EI_TARKASTETTU	= "EI_SAA_ILMOITTAA_JOS_PESAA_EI_TARKASTETTU";
	public static final String	ILMOITETTU_REVIIRIN_NIMI_JA_REVIIRI_EIVAT_TASMAA	= "ILMOITETTU_REVIIRIN_NIMI_JA_REVIIRI_EIVAT_TASMAA";
	public static final String	TOISEN_MITTAUKSEN_VUOSI_OLTAVA_TARKASTUSVUOSI	= "TOISEN_MITTAUKSEN_VUOSI_OLTAVA_TARKASTUSVUOSI";
	public static final String	PESAN_SIJAINTI	= "PESAN_SIJAINTI";
	public static final String	TOINEN_PESA_LAHELLA	= "TOINEN_PESA_LAHELLA";
	public static final String	INVALID_R_TAULU_POISTO_KIINNITYS	= "INVALID_R_TAULU_POISTO_KIINNITYS";






	public static void removeTarkastusValuesButKeepSpecific(_Table tarkastus) {
		Map<String, String> values = specificValuesToMap(tarkastus);
		tarkastus.clearAllValues();
		restoreSpecificValues(tarkastus, values);
	}

	private static void restoreSpecificValues(_Table tarkastus, Map<String, String> values) {
		for (String f : TARKASTUS_FIELDS_TO_BE_PREFILLED_FOR_INSERTS) {
			tarkastus.get(f).setValue(values.get(f));
		}
	}

	private static Map<String, String> specificValuesToMap(_Table tarkastus) {
		Map<String, String> values = new HashMap<>();
		for (String f : TARKASTUS_FIELDS_TO_BE_PREFILLED_FOR_INSERTS) {
			values.put(f, tarkastus.get(f).getValue());
		}
		return values;
	}

	public static String poikasCount(_Table tarkastus) {
		String count = Utils.maxOf(tarkastus.get("elavia_lkm").getValue(), tarkastus.get("lentopoik_lkm").getValue(), tarkastus.get("reng_poik_lkm").getValue());
		if (count.equals("0")) return "";
		if (count.equals("7")) {
			return "1";
		}
		if (count.equals("8")) {
			return "2";
		}
		if (count.equals("9")) {
			return "3";
		}
		return count;
	}

	public static boolean firstPesimistulosBetterThanSecond(String first, String second) {
		if (second.equals("")) return true;
		if (first.equals("")) return false;
		if (!MerikotkaConst.PESIMISTULOS_IN_RISING_SUCCESS_ORDER.contains(first)) throw new IllegalStateException("Pesimistulos code: '" + first + "' not found");
		if (!MerikotkaConst.PESIMISTULOS_IN_RISING_SUCCESS_ORDER.contains(second)) throw new IllegalStateException("Pesimistulos code: '" + second + "' not found");
		int thisIndex = 0;
		int bestIndex = 0;
		for (String code : MerikotkaConst.PESIMISTULOS_IN_RISING_SUCCESS_ORDER) {
			if (code.equals(first)) break;
			thisIndex++;
		}
		for (String code : MerikotkaConst.PESIMISTULOS_IN_RISING_SUCCESS_ORDER) {
			if (code.equals(second)) break;
			bestIndex++;
		}
		return thisIndex > bestIndex;
	}

	//private static final String	SELECT_KUNTA_ID_FROM_KUNTA_WHERE_KUNTA_TUNNUS	= " SELECT kunta_id FROM kunta WHERE kunta_tunnus = ? ";

	public static void kirjekyyhkyModelToData(Map<String, String> datamap, _PesaDomainModelRow kirjekyyhkyparameters) {
		String leveys = datamap.get(Const.UPDATEPARAMETERS + ".koord_leveys");
		String pituus = datamap.get(Const.UPDATEPARAMETERS + ".koord_pituus");
		String kunta = datamap.get(Const.UPDATEPARAMETERS + ".kunta");

		kirjekyyhkyparameters.getVuosi().get("pesan_kunta").setValue(kunta);
		kirjekyyhkyparameters.getVuosi().get("reviirin_kunta").setValue(kunta);

		String koord_tyyppi = kirjekyyhkyparameters.getPesa().get("koord_tyyppi").getValue();
		_Column levCol = null;
		_Column pitCol = null;
		if (koord_tyyppi.equals("Y")) {
			levCol = kirjekyyhkyparameters.getPesa().get("yht_leveys");
			pitCol = kirjekyyhkyparameters.getPesa().get("yht_pituus");
		} else if (koord_tyyppi.equals("E")) {
			levCol = kirjekyyhkyparameters.getPesa().get("eur_leveys");
			pitCol = kirjekyyhkyparameters.getPesa().get("eur_pituus");
		} else if (koord_tyyppi.equals("A")) {
			levCol = kirjekyyhkyparameters.getPesa().get("ast_leveys");
			pitCol = kirjekyyhkyparameters.getPesa().get("ast_pituus");
		}
		if (levCol != null && pitCol != null) {
			convertCoordinates(kirjekyyhkyparameters, leveys, pituus, koord_tyyppi);
			levCol.setValue(leveys);
			pitCol.setValue(pituus);
		}

		for (_Table aikuinen : kirjekyyhkyparameters.getAikuiset()) {
			if (!aikuinen.hasValues()) continue;
			handleRengas(aikuinen.get("rengas_oikea"));
			handleRengas(aikuinen.get("rengas_vasen"));
		}
		for (_Table poikanen : kirjekyyhkyparameters.getPoikaset()) {
			if (!poikanen.hasValues()) continue;
			handleRengas(poikanen.get("rengas_oikea"));
			handleRengas(poikanen.get("rengas_vasen"));
		}
	}

	private static void handleRengas(_Column rengas) {
		String value = rengas.getValue();
		value = Utils.removeWhitespace(value);
		value = value.replace("-", "");
		rengas.setValue(value);
	}

	private static void convertCoordinates(_PesaDomainModelRow kirjekyyhkyparameters, String leveys, String pituus, String koord_tyyppi) {
		try {
			CoordinateConverter converter = new CoordinateConverter();
			if (koord_tyyppi.equals("Y")) {
				converter.setYht_leveys(leveys);
				converter.setYht_pituus(pituus);
			} else if (koord_tyyppi.equals("E")) {
				converter.setEur_leveys(leveys);
				converter.setEur_pituus(pituus);
			}
			converter.convert();
			kirjekyyhkyparameters.getPesa().get("yht_leveys").setValue(converter.getYht_leveys().toString());
			kirjekyyhkyparameters.getPesa().get("yht_pituus").setValue(converter.getYht_pituus().toString());
			kirjekyyhkyparameters.getPesa().get("eur_leveys").setValue(converter.getEur_leveys().toString());
			kirjekyyhkyparameters.getPesa().get("eur_pituus").setValue(converter.getEur_pituus().toString());
			kirjekyyhkyparameters.getPesa().get("ast_leveys").setValue(converter.getAst_leveys().toString());
			kirjekyyhkyparameters.getPesa().get("ast_pituus").setValue(converter.getAst_pituus().toString());
		} catch (Exception e) {
			// Something wrong with coordinates... we do nothing, validation will pick this up later.
		}
	}
}
