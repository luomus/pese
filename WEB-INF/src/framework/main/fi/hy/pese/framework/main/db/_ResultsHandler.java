package fi.hy.pese.framework.main.db;

import java.sql.SQLException;

/**
 * Handles the results of a query. For example might add the results to _Data -structure results-leaf.
 */
public interface _ResultsHandler {

	/**
	 * Handles the results of a query.
	 * @param rs
	 * @throws SQLException
	 */
	void process(_ResultSet rs) throws SQLException;

}
