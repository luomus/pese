package fi.hy.pese.ulko.main.app;

import java.sql.SQLException;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import fi.hy.pese.framework.main.app.BaseFactoryForApplicationSpecificationFactory;
import fi.hy.pese.framework.main.app._Functionality;
import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.app.functionality.FilemanagementFunctionality;
import fi.hy.pese.framework.main.app.functionality.FilemanagementFunctionality.FileListGeneratorImple;
import fi.hy.pese.framework.main.app.functionality.LintuvaaraLoginFunctionality;
import fi.hy.pese.framework.main.app.functionality.LoginFunctionalityWithSingleUser;
import fi.hy.pese.framework.main.app.functionality.LogoutFunctionality;
import fi.hy.pese.framework.main.app.functionality.RingerInfoQueryFunctionality;
import fi.hy.pese.framework.main.app.functionality.TablemanagementFunctionality;
import fi.hy.pese.framework.main.db.SingleValueResultHandler;
import fi.hy.pese.framework.main.db._DAO;
import fi.hy.pese.framework.main.general.SelectionsLoader;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data.ParameterMap;
import fi.hy.pese.framework.main.general.data._Data;
import fi.hy.pese.ulko.main.functionality.CopyFunctionality;
import fi.hy.pese.ulko.main.functionality.LoytotiedotQuery;
import fi.hy.pese.ulko.main.functionality.MunicipalityToEuringPlaceQuery;
import fi.hy.pese.ulko.main.functionality.ReceiveFromLoydosFunctionality;
import fi.hy.pese.ulko.main.functionality.UlkoFunctionality;
import fi.hy.pese.ulko.main.functionality.UlkoSearchFunctionality;
import fi.luomus.commons.config.Config;
import fi.luomus.commons.containers.Selection;
import fi.luomus.commons.containers.SelectionImple;
import fi.luomus.commons.containers.SelectionOptionImple;
import fi.luomus.commons.utils.AESDecryptor;
import fi.luomus.commons.utils.SingleObjectCache;

public class UlkoFactory extends BaseFactoryForApplicationSpecificationFactory<_UlkoDomainModelRow> {

	public UlkoFactory(String configFileName) throws Exception {
		super(configFileName);
	}

	@Override
	public _Data<_UlkoDomainModelRow> initData(String language, ParameterMap params) throws Exception {
		_Data<_UlkoDomainModelRow> data = super.initData(language, params);
		data.set(Const.LINTUVAARA_URL, getLintuvaaraURL());
		return data;
	}

	private _Data<_UlkoDomainModelRow> loadBinCounts(_Data<_UlkoDomainModelRow> data) throws SQLException {
		_DAO<_UlkoDomainModelRow> dao = this.dao("mainmenu");
		data.set("pyynto_count_1", getCount(dao, "1", "E"));
		data.set("pyynto_count_2", getCount(dao, "2", "E"));
		data.set("pyynto_count_3", getCount(dao, "3", "E"));
		data.set("pyynto_count_4", getCount(dao, "4", "E"));
		data.set("pyynto_count_roskakori", getCount(dao, "%", "K"));
		return data;
	}

	private String getCount(_DAO<_UlkoDomainModelRow> dao, String tila, String roskiin) throws SQLException {
		SingleValueResultHandler handler = new SingleValueResultHandler();
		dao.executeSearch(" SELECT COUNT(1) FROM pyynto WHERE tila like ? AND roskiin = ?", handler, tila, roskiin);
		return handler.value;
	}

	@Override
	public String frontpage() {
		return Const.MAIN_PAGE; // Warning: frontpage must be defined bellow (functionalityFor()) or an infinite loop will happen
	}

	private AESDecryptor	decryptor	= null;

	private AESDecryptor getAESDecryptor() {
		if (decryptor == null) {
			try {
				decryptor = new AESDecryptor(config().get(Config.LINTUVAARA_PUBLIC_RSA_KEY));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return decryptor;
	}

	@Override
	public _Functionality<_UlkoDomainModelRow> functionalityFor(String page, _Request<_UlkoDomainModelRow> request) {
		if (page.equals(Const.LOGIN_PAGE)) {
			if (config().developmentMode()) {
				return new LoginFunctionalityWithSingleUser<>(request, "z", "z");
			}
			return new LintuvaaraLoginFunctionality<>(request, getAESDecryptor(), getErrorReporter());
		}
		try {
			loadBinCounts(request.data());
		} catch (SQLException e) {
		}
		if (page.equals(Const.LOGOUT_PAGE)) {
			return new LogoutFunctionality<>(request);
		}
		else if (page.equals(Const.MAIN_PAGE)) {
			return new UlkoFunctionality(request);
		} else if (page.equals("copy")) {
			return new CopyFunctionality(request);
		}
		else if (page.equals(Const.SEARCH_PAGE)) {
			return new UlkoSearchFunctionality(request);
		}
		else if (page.equals(Const.TABLEMANAGEMENT_PAGE)) {
			return new TablemanagementFunctionality<>(request);
		}
		else if (page.equals("ringerInfo")) {
			return new RingerInfoQueryFunctionality<>(request);
		}
		else if (page.equals("loytotiedot")) {
			return new LoytotiedotQuery(request);
		} else if (page.equals("municipalityToEuringPlace")) {
			return new MunicipalityToEuringPlaceQuery(request);
		}
		else if (page.equals(Const.FILEMANAGEMENT_PAGE)) {
			return new FilemanagementFunctionality<>(request, new FileListGeneratorImple(request.config().pdfFolder(), null));
		}
		else if (page.equals("receive")) {
			return new ReceiveFromLoydosFunctionality(request);
		}
		else {
			request.data().setPage(frontpage());
			return functionalityFor(frontpage(), request);
		}
	}

	private final SelectionsLoader<_UlkoDomainModelRow> selectionsLoader = new SelectionsLoader<>(this);

	private final SingleObjectCache<Map<String, Selection>> cachedSelections = new SingleObjectCache<>(selectionsLoader, 15, TimeUnit.MINUTES);

	@Override
	public synchronized Map<String, Selection> selections(_DAO<_UlkoDomainModelRow> dao) throws Exception {
		Map<String, Selection> selections = cachedSelections.get();

		SelectionImple tila = new SelectionImple("pyynto.tila");
		tila.addOption(new SelectionOptionImple("1", "Löytö syötetty, rengastustietoja ei pyydetty"));
		tila.addOption(new SelectionOptionImple("2", "Rengastustiedot pyydetty, odottaa vastausta"));
		tila.addOption(new SelectionOptionImple("3", "Rengastustiedot vastaanotettu, odottaa löytökirjeen lähettämistä"));
		tila.addOption(new SelectionOptionImple("4", "Loppuunkäsitelty"));
		selections.put(tila.getName(), tila);

		return selections;
	}

	@Override
	public Map<String, Selection> fetchSelections(_DAO<_UlkoDomainModelRow> dao) throws Exception {
		throw new UnsupportedOperationException();
	}

}
