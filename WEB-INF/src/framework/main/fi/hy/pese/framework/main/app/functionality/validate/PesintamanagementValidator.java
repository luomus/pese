package fi.hy.pese.framework.main.app.functionality.validate;

import java.sql.SQLException;

import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.db.CountCombinedRowsResultHandler;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.main.general.data._Table;

public class PesintamanagementValidator extends Validator<_PesaDomainModelRow> {

	public static final String	CAN_NOT_DELETE_ALL_TARKASTUKSET	= "CAN_NOT_DELETE_ALL_TARKASTUKSET";


	public PesintamanagementValidator(_Request<_PesaDomainModelRow> request) {
		super(request);
	}

	@Override
	public void validate() throws Exception {
		if (data.action().equals(Const.SELECT_NEST)) {
			validateNestExists();
		}
		if (data.action().equals(Const.DELETE_TARKASTUKSET)) {
			validateOkToDeleteTarkastukset();
			if (hasErrors()) {
				validateNestExists();
			}
		}
		if (data.action().equals(Const.MOVE_TARKASTUKSET) || data.action().equals(Const.MOVE_TARKASTUKSET_TO_NEW)) {
			validateOkToMoveTarkastukset();
			if (hasErrors()) {
				validateNestExists();
			}
		}
	}

	private void validateOkToMoveTarkastukset() throws SQLException {
		_Column tarkastusIDs = data.updateparameters().getTarkastus().getUniqueNumericIdColumn();
		checkTarkastusListValid(tarkastusIDs);

		if (data.action().equals(Const.MOVE_TARKASTUKSET)) {
			String nestId = data.get(Const.MOVE_TO_NEST_ID);
			checkNotNull(Const.MOVE_TO_NEST_ID, nestId);
			if (hasErrors()) return;
			checkInteger(Const.MOVE_TO_NEST_ID, nestId);
			if (hasErrors()) return;
			_Table table = data.newRow().getPesa();
			table.getUniqueNumericIdColumn().setValue(nestId);
			boolean exists = dao.checkRowExists(table);
			if (!exists) {
				error(Const.MOVE_TO_NEST_ID, _Validator.DOES_NOT_EXIST);
			}
		}
	}

	private void validateOkToDeleteTarkastukset() throws SQLException {
		_Column tarkastusIDs = data.updateparameters().getTarkastus().getUniqueNumericIdColumn();
		checkTarkastusListValid(tarkastusIDs);
		if (hasErrors()) return;

		int countToBeDeleted = tarkastusIDs.getValueList().size();
		CountCombinedRowsResultHandler resultsHandler = new CountCombinedRowsResultHandler(dao);
		_PesaDomainModelRow parameters = data.newRow();
		parameters.getPesa().getUniqueNumericIdColumn().setValue(data.updateparameters().getPesa().getUniqueNumericIdColumn().getValue());
		dao.executeSearch(parameters, resultsHandler);
		if (countToBeDeleted >= resultsHandler.numberOfRows) {
			error(tarkastusIDs, CAN_NOT_DELETE_ALL_TARKASTUKSET);
		}
	}
	
	private void checkTarkastusListValid(_Column tarkastusIDs) {
		if (isNull(tarkastusIDs)) {
			error (tarkastusIDs, _Validator.AT_LEAST_ONE_MUST_BE_SELECTED);
			return;
		}
		for (@SuppressWarnings("unused") String id : tarkastusIDs.getValueList()) {
			// TODO check exists
		}
	}

	private void validateNestExists() throws SQLException {
		_Column pesaId = data.updateparameters().getPesa().getUniqueNumericIdColumn();
		checkNotNull(pesaId);
		checkAsUpdateparameter(pesaId);
		if (hasErrors()) return;

		boolean exists = dao.checkRowExists(data.updateparameters().getPesa());
		if (!exists) {
			error(pesaId, _Validator.DOES_NOT_EXIST);
		}
	}

}
