package fi.hy.pese.framework.test.view;

import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import fi.hy.pese.framework.main.app.functionality.validate._Validator;
import fi.hy.pese.framework.main.general.data.Data;
import fi.hy.pese.framework.main.general.data.DatabaseTableStructure;
import fi.hy.pese.framework.main.general.data.PesaDomainModelDatabaseStructure;
import fi.hy.pese.framework.main.general.data.PesaDomainModelRowFactory;
import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.main.view.FreemarkerTemplateViewer;
import fi.hy.pese.framework.test.TestConfig;
import fi.luomus.commons.config.Config;
import fi.luomus.commons.containers.Selection;
import fi.luomus.commons.containers.SelectionImple;
import fi.luomus.commons.containers.SelectionOptionImple;
import fi.luomus.commons.languagesupport.LocalizedTextsContainerImple;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class UsingTheFreemarkerTemplateViewerToImplementTheViewerInterface {

	public static Test suite() {
		return new TestSuite(UsingTheFreemarkerTemplateViewerToImplementTheViewerInterface.class.getDeclaredClasses());
	}

	public static class WhenViewPageMethodIsCalled extends TestCase {

		private FreemarkerTemplateViewer	viewer;
		private Data<_PesaDomainModelRow>	data;
		private PrintWriter				pwriter;
		private ByteArrayOutputStream	out;

		@Override
		protected void setUp() throws Exception {
			Config c = TestConfig.getConfig();
			viewer = new FreemarkerTemplateViewer(c.templateFolder());
			out = new ByteArrayOutputStream();
			pwriter = new PrintWriter(out);
			data = new Data<>(new PesaDomainModelRowFactory(new PesaDomainModelDatabaseStructure(), "", ""));
		}

		public void test_it_opens_the_correct_template() throws Exception {
			viewer.viewPage("test1", data, pwriter);
			assertEquals("test", out.toString());
		}

		public void test_it_reads_misc_parameters_from_data() throws Exception {
			data.set("testkey", "testvalue");
			viewer.viewPage("test2", data, pwriter);
			assertEquals("test:testvalue", out.toString());
		}

		public void test_it_reads_misc_lists_from_data() throws Exception {
			Map<String, String> pesa4 = new HashMap<>();
			pesa4.put("id", "4");
			pesa4.put("nimi", "nelosen nimi");
			Map<String, String> pesa7 = new HashMap<>();
			pesa7.put("id", "7");
			pesa7.put("nimi", "seiskan nimi");
			data.addToList("pesat", pesa4);
			data.addToList("pesat", pesa7);
			viewer.viewPage("test6", data, pwriter);
			assertEquals("4:nelosen nimi|7:seiskan nimi|", out.toString());
		}

		public void test_it_reads_table_related_userinput_from_data() throws Exception {
			PesaDomainModelDatabaseStructure tablesTemplate = new PesaDomainModelDatabaseStructure();
			tablesTemplate.setPesa(new DatabaseTableStructure("pesavakio"));
			tablesTemplate.getPesa().addColumn("pesanimi", _Column.VARCHAR, 20);
			resetData(tablesTemplate);
			data.updateparameters().getPesa().get("pesanimi").setValue("nimipesalle");
			viewer.viewPage("test7", data, pwriter);
			assertEquals("nimipesalle", out.toString());
		}

		public void test_it_reads_table_related_poikanen_data_from_data() throws Exception {
			PesaDomainModelDatabaseStructure template = new PesaDomainModelDatabaseStructure();
			DatabaseTableStructure poikanen = new DatabaseTableStructure("poikanen");
			poikanen.addColumn("poikanen_id", _Column.INTEGER, 5);
			template.setPoikanen(poikanen);
			resetData(template);
			data.updateparameters().getColumn("poikanen.1.poikanen_id").setValue("100");
			data.updateparameters().getColumn("poikanen.2.poikanen_id").setValue("200");
			viewer.viewPage("test22", data, pwriter);
			assertEquals("100 | 200", out.toString());
		}

		public void test_it_reads_table_related_resultlistings_data() throws Exception {
			PesaDomainModelDatabaseStructure tablesTemplate = new PesaDomainModelDatabaseStructure();
			tablesTemplate.setPesa(new DatabaseTableStructure("pesavakio"));
			tablesTemplate.getPesa().addColumn("pesanimi", _Column.VARCHAR, 20);
			resetData(tablesTemplate);

			_PesaDomainModelRow resultrow = data.newResultRow();
			resultrow.getPesa().get("pesanimi").setValue("ekan nimi");
			resultrow = data.newResultRow();
			resultrow.getPesa().get("pesanimi").setValue("tokan nimi");

			viewer.viewPage("test8", data, pwriter);
			assertEquals("ekan nimi|tokan nimi|", out.toString());
		}

		public void test_it_reads_table_related_poikanen_result_data_from_data() throws Exception {
			PesaDomainModelDatabaseStructure template = new PesaDomainModelDatabaseStructure();
			DatabaseTableStructure poikanen = new DatabaseTableStructure("poikanen");
			poikanen.addColumn("poikanen_id", _Column.INTEGER, 5);
			template.setPoikanen(poikanen);
			resetData(template);
			_PesaDomainModelRow row = data.newResultRow();
			row.getColumn("poikanen.1.poikanen_id").setValue("100");
			row.getColumn("poikanen.2.poikanen_id").setValue("200");
			viewer.viewPage("test23", data, pwriter);
			assertEquals("100 | 200", out.toString());
		}

		public void test_it_reads_data_needed_for_selections() throws Exception {
			Map<String, Selection> selections = new HashMap<>();
			SelectionImple kunnat = new SelectionImple("kunnat");
			kunnat.addOption(new SelectionOptionImple("HELSIN", "Helsinki"));
			kunnat.addOption(new SelectionOptionImple("ESPOO", "Espoo"));
			selections.put("kunnat", kunnat);
			data.setSelections(selections);
			viewer.viewPage("test9", data, pwriter);
			assertEquals("<select name=\"kunta\"><option value=\"HELSIN\">Helsinki</option><option value=\"ESPOO\">Espoo</option></select>", out.toString());
		}

		public void test_it_reads_url_action_and_success_failure_and_notice_texts_from_data() throws Exception {
			data.setBaseURL("http://localhost/");
			data.setAction("poista");
			data.setSuccessText("Muokkaus onnistui.");
			data.setFailureText("Jotain epäonnistui");
			data.addToNoticeTexts("Tulostettiin 3 raporttia");
			viewer.viewPage("test12", data, pwriter);
			assertEquals("http://localhost/|poista|Muokkaus onnistui.|Jotain epäonnistui|Tulostettiin 3 raporttia", out.toString());
		}

		public void test_it_reads_produced_files_list_from_data() throws Exception {
			List<String> files = new LinkedList<>();
			files.add("tulostukset/pandion/raportit/raportti1.txt");
			files.add("tulostukset/pandion/raportit/raportti2.txt");
			data.addToProducedFiles(files);
			data.setBaseURL("http://localhost/");
			viewer.viewPage("test13", data, pwriter);
			assertEquals("http://localhost/tulostukset/pandion/raportit/raportti1.txt|http://localhost/tulostukset/pandion/raportit/raportti2.txt|", out.toString());
		}

		public void test_it_reads_uitexts_from_data() throws Exception {
			LocalizedTextsContainerImple uiTexts = new LocalizedTextsContainerImple("FI");
			uiTexts.setText("send", "Lähetä", "FI");
			uiTexts.setText("cancel", "Peruuta", "FI");
			data.setUITexts(uiTexts.getAllTexts("FI"));
			viewer.viewPage("test14", data, pwriter);
			assertEquals("Lähetä Peruuta", out.toString());
		}

		public void test_it_lists_maps_as_lists__in_correct_order() throws Exception {
			PesaDomainModelDatabaseStructure tablesTemplate = new PesaDomainModelDatabaseStructure();
			tablesTemplate.setPesa(new DatabaseTableStructure("pesavakio"));
			tablesTemplate.getPesa().addColumn("pesaid", _Column.INTEGER, 5);
			tablesTemplate.getPesa().addColumn("pesanimi", _Column.VARCHAR, 20);
			resetData(tablesTemplate);
			data.updateparameters().getPesa().get("pesaid").setValue("5");
			data.updateparameters().getPesa().get("pesanimi").setValue("nimipesalle");
			data.set("table", "pesa");
			viewer.viewPage("test15", data, pwriter);
			assertTrue(out.toString().contains("pesaid = 5; pesanimi = nimipesalle;"));
		}

		public void test_it_marks_erroreus_fields() throws Exception {
			PesaDomainModelDatabaseStructure tablesTemplate = new PesaDomainModelDatabaseStructure();
			tablesTemplate.setPesa(new DatabaseTableStructure("pesa"));
			tablesTemplate.getPesa().addColumn("nimi", _Column.VARCHAR, 20);
			resetData(tablesTemplate);

			viewer.viewPage("test16", data, pwriter);
			assertFalse("should not mark any fields erroreus", out.toString().contains("class=\"error\""));

			out.reset();

			LinkedHashMap<String, String> errors = new LinkedHashMap<>();
			_Column nimi = data.updateparameters().getPesa().get("nimi");
			errors.put(nimi.getFullname(), _Validator.CAN_NOT_BE_NULL);
			data.setErrors(errors);

			viewer.viewPage("test16", data, pwriter);
			assertTrue("should mark field erroreous", out.toString().contains("class=\"error\""));
		}

		public void test_it_lists_Errors() throws Exception {
			HashMap<String, String> texts = new HashMap<>();
			texts.put("pesa.nimi", "Pesän nimi");
			texts.put(_Validator.TOO_LONG, "Teksti on liian pitkä");
			data.setUITexts(texts);
			LinkedHashMap<String, String> errors = new LinkedHashMap<>();
			errors.put("pesa.nimi", _Validator.TOO_LONG);
			errors.put("pesa.kunta", _Validator.CAN_NOT_BE_NULL);
			data.setErrors(errors);
			viewer.viewPage("test17", data, pwriter);
			assertTrue(out.toString().contains("Pesän nimi : Teksti on liian pitkä"));
			assertTrue(out.toString().contains("pesa.kunta : " + _Validator.CAN_NOT_BE_NULL));
		}

		public void test_it_seperates_date_columns_to_three_seperate_fields() throws Exception {
			PesaDomainModelDatabaseStructure tablestemplate = new PesaDomainModelDatabaseStructure();
			tablestemplate.setTarkastus(new DatabaseTableStructure("pesatarkastus"));
			tablestemplate.getTarkastus().addColumn("tark_pvm", _Column.DATE);
			resetData(tablestemplate);

			data.searchparameters().getTarkastus().get("tark_pvm").setValue("5.10.2009");
			viewer.viewPage("test18", data, pwriter);
			assertEquals("05.10.2009", out.toString());

			//			out.reset();
			//			data.searchparameters().getTarkastus().get("tark_pvm").setValue(null);
			//			viewer.viewPage("test18", data, pwriter);
			//			assertEquals("..", out.toString());

			out.reset();
			data.searchparameters().getTarkastus().get("tark_pvm").setValue("");
			viewer.viewPage("test18", data, pwriter);
			assertEquals("..", out.toString());

			out.reset();
			data.searchparameters().getTarkastus().get("tark_pvm").setValue("..");
			viewer.viewPage("test18", data, pwriter);
			assertEquals("..", out.toString());

			out.reset();
			data.searchparameters().getTarkastus().get("tark_pvm").setValue("5..");
			viewer.viewPage("test18", data, pwriter);
			assertEquals("05..", out.toString());

			out.reset();
			data.searchparameters().getTarkastus().get("tark_pvm").setValue("asdasd");
			viewer.viewPage("test18", data, pwriter);
			assertEquals("asdasd..", out.toString());

			out.reset();
			data.searchparameters().getTarkastus().get("tark_pvm").setValue("5.6.7.8.9.10");
			viewer.viewPage("test18", data, pwriter);
			assertEquals("05.06.7", out.toString());
		}

		public void test_it_reads_page_from_data() throws Exception {
			data.setPage("sivu");
			viewer.viewPage("test19", data, pwriter);
			assertEquals("sivu", out.toString());
		}

		public void test_it_reads_rowids() throws Exception {
			PesaDomainModelDatabaseStructure tablestemplate = new PesaDomainModelDatabaseStructure();
			tablestemplate.setTarkastus(new DatabaseTableStructure("tarkastus"));
			resetData(tablestemplate);

			data.searchparameters().getTarkastus().setRowidValue("AAAAABA");
			data.newResultRow().getTarkastus().setRowidValue("AAAAABA");
			viewer.viewPage("test20", data, pwriter);
			assertEquals("AAAAABA|AAAAABA", out.toString());
		}

		public void test_using_selections() throws Exception {
			PesaDomainModelDatabaseStructure structure = new PesaDomainModelDatabaseStructure();
			DatabaseTableStructure pesa = new DatabaseTableStructure("pesavakio");
			pesa.addColumn("kunta", _Column.VARCHAR, 6);
			pesa.get("kunta").setAssosiatedSelection("kunnat");
			structure.setPesa(pesa);

			resetData(structure);
			Map<String, Selection> selections = new HashMap<>();
			SelectionImple kunnat = new SelectionImple("foo");
			kunnat.addOption(new SelectionOptionImple("VANTAA", "Vantaa"));
			kunnat.addOption(new SelectionOptionImple("HELSIN", "Helsinki"));
			selections.put("kunnat", kunnat);
			data.setSelections(selections);

			data.searchparameters().getPesa().get("kunta").setValue("HELSIN");
			viewer.viewPage("test24", data, pwriter);
			assertEquals("Helsinki", out.toString().trim());
		}

		private void resetData(PesaDomainModelDatabaseStructure structure) {
			data = new Data<>(new PesaDomainModelRowFactory(structure, "", ""));
		}

	}

}
