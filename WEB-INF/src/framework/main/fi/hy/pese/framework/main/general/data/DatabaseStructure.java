package fi.hy.pese.framework.main.general.data;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public class DatabaseStructure {

	protected final Map<String, DatabaseTableStructure>	tableStructures;
	private DatabaseTableStructure						savedSearches;
	private DatabaseTableStructure						log;
	private DatabaseTableStructure						transactionEntry;
	private DatabaseTableStructure						aputaulu;
	private DatabaseTableStructure						lomakeVastaanottajat;

	public DatabaseStructure() {
		tableStructures = new LinkedHashMap<>(20);
		savedSearches = new DatabaseTableStructure(_Table.UNINITIALIZED_TABLE);
		log = new DatabaseTableStructure(_Table.UNINITIALIZED_TABLE);
		transactionEntry = new DatabaseTableStructure(_Table.UNINITIALIZED_TABLE);
		aputaulu = new DatabaseTableStructure(_Table.UNINITIALIZED_TABLE);
		lomakeVastaanottajat = new DatabaseTableStructure(_Table.UNINITIALIZED_TABLE);
	}

	protected void toTableStructuremap(DatabaseTableStructure t) {
		tableStructures.put(t.getName().toLowerCase(), t);
	}

	public Collection<DatabaseTableStructure> getAllTables() {
		return tableStructures.values();
	}

	public void setSavedSearches(DatabaseTableStructure t) {
		savedSearches = t;
		toTableStructuremap(t);
	}

	public DatabaseTableStructure getSavedSearches() {
		return savedSearches;
	}

	public void setLogTable(DatabaseTableStructure t) {
		log = t;
		toTableStructuremap(t);
	}

	public DatabaseTableStructure getLogTable() {
		return log;
	}

	public void setTransactionEntry(DatabaseTableStructure t) {
		transactionEntry = t;
		toTableStructuremap(t);
	}

	public DatabaseTableStructure getTransactionEntry() {
		return transactionEntry;
	}

	public void setAputaulu(DatabaseTableStructure t) {
		aputaulu = t;
		toTableStructuremap(t);
	}

	public DatabaseTableStructure getAputaulu() {
		return aputaulu;
	}

	public DatabaseTableStructure getTableStructureByName(String name) throws IllegalArgumentException {
		DatabaseTableStructure t = tableStructures.get(name.toLowerCase());
		if (t != null) return t;
		throw new IllegalArgumentException("TableStructure " + name + " not found");
	}

	public DatabaseColumnStructure getColumn(String columnFullname) {
		int i = columnFullname.lastIndexOf('.');
		String tablename = columnFullname.substring(0, i);
		String columnname = columnFullname.substring(i + 1);
		return getTableStructureByName(tablename).get(columnname);
	}


	public void defineCustomTable(DatabaseTableStructure table) {
		if (table == null) throw new IllegalArgumentException("Table was null");
		String name = table.getName().toLowerCase();
		if (tableStructures.containsKey(name)) {
			throw new IllegalArgumentException("Table with name " + table.getName() + " already defined");
		}
		tableStructures.put(name, table);
	}

	public void setLomakeVastaanottajat(DatabaseTableStructure t) {
		lomakeVastaanottajat = t;
		toTableStructuremap(t);
	}

	public DatabaseTableStructure getLomakeVastaanottajat() {
		return lomakeVastaanottajat;
	}

}
