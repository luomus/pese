package fi.hy.pese.framework.main.app.functionality;

import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.app.functionality.validate.LoginValidatorForLintuvaaraLogin;
import fi.hy.pese.framework.main.app.functionality.validate._Validator;
import fi.hy.pese.framework.main.general.data._Row;
import fi.luomus.commons.config.Config;
import fi.luomus.commons.reporting.ErrorReporter;
import fi.luomus.commons.utils.AESDecryptor;
import fi.luomus.commons.utils.URIBuilder;
import fi.luomus.commons.utils.Utils;

public class LintuvaaraLoginFunctionality<T extends _Row> extends BaseFunctionality<T> {

	public static final String		DATA		= "data";
	public static final String		IV			= "iv";
	public static final String		KEY			= "key";
	public static final String		SERVICE_URI	= "service_uri";

	private final String			lintuvaaraURI;
	private final ErrorReporter	errorReporter;
	private final AESDecryptor		decryptor;

	public LintuvaaraLoginFunctionality(_Request<T> request, AESDecryptor decryptor, ErrorReporter errorReporter) {
		super(request);
		this.lintuvaaraURI = request.config().get(Config.LINTUVAARA_URL);
		this.errorReporter = errorReporter;
		this.decryptor = decryptor;
	}

	@Override
	public void preProsessingHook() throws Exception {
		
	}

	@Override
	public _Validator validator() {
		return new LoginValidatorForLintuvaaraLogin<>(decryptor, errorReporter, request);
	}

	@Override
	public void afterFailedValidation() throws Exception {
		URIBuilder uri = new URIBuilder(lintuvaaraURI + "/sessions");
		uri.addParameter("service", config.systemId());
		uri.addParameter(SERVICE_URI, request.getOriginalRequestURI());

		request.redirecter().redirectTo(uri.toString());
	}

	@Override
	public void afterSuccessfulValidation() throws Exception {
		session.authenticateFor(request.config().systemId());
		String redirect_url = request.config().baseURL();
		if (data.contains(SERVICE_URI)) {
			redirect_url = Utils.urlDecode(data.get(SERVICE_URI));
		}
		request.redirecter().redirectTo(redirect_url);
	}

	@Override
	public void postProsessingHook() throws Exception {}

}
