package fi.hy.pese.framework.main.app.functionality;

import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.app.functionality.validate.LoginValidatorWithUsersStoredToDatabase;
import fi.hy.pese.framework.main.app.functionality.validate._Validator;
import fi.hy.pese.framework.main.general.data._Row;

public class LoginFunctionalityWithUsersStoredToDatabase<T extends _Row> extends AbstractLoginFunctionality<T> {

	public LoginFunctionalityWithUsersStoredToDatabase(_Request<T> request) {
		super(request);
	}

	@Override
	public _Validator validator() {
		return new LoginValidatorWithUsersStoredToDatabase<>(request);
	}

}
