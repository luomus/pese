package fi.hy.pese.framework.main.db;

import java.sql.SQLException;

import fi.hy.pese.framework.main.general.data._Data;
import fi.hy.pese.framework.main.general.data._Row;
import fi.hy.pese.framework.main.general.data._Table;

public class RowsToData_Single_Table_ResultHandler<T extends _Row> extends BaseResultsHandler {

	private final _Data<T>		data;
	private final String	tablename;
	private final int limit;

	public RowsToData_Single_Table_ResultHandler(_Data<T> data, String tablename) {
		this(data, tablename, Integer.MAX_VALUE);
	}

	public RowsToData_Single_Table_ResultHandler(_Data<T> data, String tablename, int limit) {
		this.data = data;
		this.tablename = tablename;
		this.limit = limit;
	}
	
	@Override
	public void process(_ResultSet rs) throws SQLException {
		int count = 0;
		while (rs.next()) {
			if (count++ > limit) return;
			_Table resultrow = data.newResultRow().getTableByName(tablename);
			setResultsToTable(rs, resultrow);
		}
	}

}
