package fi.hy.pese.rengastus.main.app;

import fi.hy.pese.framework.main.general.data._Row;
import fi.hy.pese.framework.main.general.data._Table;

public interface _RengastusDomainModelRow extends _Row {

	_Table getKontrolli();

}
