package fi.hy.pese.peto.main.functionality;

import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.app.functionality.KirjekyyhkyValidationServiceFunctionality;
import fi.hy.pese.framework.main.app.functionality.validate._Validator;
import fi.hy.pese.framework.main.general.PesimisenBiologisetRajatUtil;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.peto.main.app.PetoConst;
import fi.hy.pese.peto.main.functionality.validation.PetoTarkastusValidator;

public class PetoKirjekyyhkyValidationServiceFunctionality extends KirjekyyhkyValidationServiceFunctionality<_PesaDomainModelRow> {


	private static final boolean	KIRJEKYYHKY_VALIDATIONS_ONLY	= true;

	public PetoKirjekyyhkyValidationServiceFunctionality(_Request<_PesaDomainModelRow> request) {
		super(request);
	}

	@Override
	public _Validator validator() throws Exception {
		return new PetoTarkastusValidator(request, KIRJEKYYHKY_VALIDATIONS_ONLY, PesimisenBiologisetRajatUtil.getInstance(request.config()));
	}

	@Override
	public void preProsessingHook() throws Exception {
		super.preProsessingHook();
		PetoConst.kirjekyyhkyModelToData(data.getAll(), data.updateparameters());
	}

}
