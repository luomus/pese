package fi.hy.pese.merikotka.test;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fi.hy.pese.framework.main.app.Request;
import fi.hy.pese.framework.main.app.RequestImpleData;
import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.app.functionality.validate._Validator;
import fi.hy.pese.framework.main.db._DAO;
import fi.hy.pese.framework.main.db._ResultSet;
import fi.hy.pese.framework.main.db._ResultsHandler;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data.Data;
import fi.hy.pese.framework.main.general.data.PesaDomainModelDatabaseStructure;
import fi.hy.pese.framework.main.general.data.PesaDomainModelRowFactory;
import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._Data;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.main.general.data._ReferenceKey;
import fi.hy.pese.framework.main.general.data._Row;
import fi.hy.pese.framework.main.general.data._RowFactory;
import fi.hy.pese.framework.main.general.data._Table;
import fi.hy.pese.framework.test.TestConfig;
import fi.hy.pese.framework.test.db.DAOStub;
import fi.hy.pese.framework.test.db.PreparedStatementStub;
import fi.hy.pese.framework.test.db.ResultSetStub;
import fi.hy.pese.merikotka.main.app.MerikotkaConst;
import fi.hy.pese.merikotka.main.app.MerikotkaDatabaseStructureCreator;
import fi.hy.pese.merikotka.main.app.MerikotkaFactory;
import fi.hy.pese.merikotka.main.functionality.validation.MerikotkaTarkastusValidator;
import fi.luomus.commons.containers.Selection;
import fi.luomus.commons.containers.SelectionImple;
import fi.luomus.commons.containers.SelectionOptionImple;
import fi.luomus.commons.tipuapi.TipuAPIClient;
import fi.luomus.commons.tipuapi.TipuApiResource;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.xml.Document.Node;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class MerikotkaTarkastusValidationTest {
	
	public static Test suite() {
		return new TestSuite(MerikotkaTarkastusValidationTest.class.getDeclaredClasses());
	}
	
	public static class WhenSendingTheForm extends TestCase {
		
		private static final String	THIS_REVIIRI_DOESNT_HAVE_SUCCESSFUL	= "2";
		private static final String	THIS_REVIIRI_HAS_SUCCESSFUL			= "1";
		private static final String	CONFLICT_PESA_ID					= "242";
		
		private final static TipuApiResource kunnat = initMunicipalities();
		
		private static TipuApiResource initMunicipalities() {
			TipuApiResource kunnatResource = new TipuApiResource();
			Node helsinki = new Node("municipality");
			helsinki.addChildNode("id").setContents("HELSIN");
			helsinki.addChildNode("merikotka-suuralue").setContents("A");
			helsinki.addAttribute("centerpoint-lon", "25.0").addAttribute("centerpoint-lat", "60.0").addAttribute("radius", "30");
			kunnatResource.add(helsinki);
			return kunnatResource;
		}
		
		public class FakeDAO extends DAOStub<_PesaDomainModelRow> {
			
			private static final String	TOISEN_PESAN_ID_REVIIRILTA_1	= "3033";
			private static final String	TOISEN_PESAN_ID_REVIIRILTA_2	= "3034";
			
			public FakeDAO() {}
			
			@Override
			public List<_Table> returnTables(_Table searchParams ){
				if (searchParams.getName().equals("vuosi") && searchParams.get("reviiri_id").getValue().equals("1")) {
					_Table toinenVuosirivi = newRow().getVuosi();
					toinenVuosirivi.get("pesa_id").setValue(TOISEN_PESAN_ID_REVIIRILTA_1);
					return fi.luomus.commons.utils.Utils.list(toinenVuosirivi);
				}
				if (searchParams.getName().equals("vuosi") && searchParams.get("reviiri_id").getValue().equals("2")) {
					_Table toinenVuosirivi = newRow().getVuosi();
					toinenVuosirivi.get("pesa_id").setValue(TOISEN_PESAN_ID_REVIIRILTA_2);
					return fi.luomus.commons.utils.Utils.list(toinenVuosirivi);
				}
				throw new UnsupportedOperationException();
			}
			@Override
			public _Table returnTableByKeys(String tableName, String ... keys) {
				if (tableName.equals("pesavakio") && keys[0].equals(TOISEN_PESAN_ID_REVIIRILTA_1)) {
					_Table toinenPesa = newRow().getPesa();
					toinenPesa.get("yht_leveys").setValue("6670000");
					toinenPesa.get("yht_pituus").setValue("3390900");
					return toinenPesa;
				}
				if (tableName.equals("pesavakio") && keys[0].equals(TOISEN_PESAN_ID_REVIIRILTA_2)) {
					_Table toinenPesa = newRow().getPesa();
					toinenPesa.get("yht_leveys").setValue("6674423");
					toinenPesa.get("yht_pituus").setValue("3390958");
					return toinenPesa;
				}
				throw new UnsupportedOperationException();
			}
			
			@Override
			public _PesaDomainModelRow newRow() {
				return new PesaDomainModelRowFactory(databaseStructure, "", "").newRow();
			}
			
			@Override
			public PreparedStatement prepareStatement(String string) throws SQLException {
				return new NestsCloseByFakePreparedStatement();
			}
			
			private class NestsCloseByFakePreparedStatement extends PreparedStatementStub {
				@Override
				public ResultSet executeQuery() throws SQLException {
					return new ResultSetStub();
				}
			}
			
			@Override
			public boolean checkReferenceValueExists(String value, _ReferenceKey referenceKey) throws SQLException {
				if (value.equals("8888")) return false;
				if (value.equals("FOOOBAR")) return false;
				return true;
			}
			
			@Override
			public void executeSearch(String sql, _ResultsHandler resultHandler, String... values) throws SQLException {
				class FakeResultSet implements _ResultSet {
					private final String[]	values;
					
					public FakeResultSet(String... values) {
						this.values = values;
					}
					
					boolean	doneOnce	= false;
					
					@Override
					public boolean next() throws SQLException {
						if (doneOnce) return false;
						if (values[0].equals(THIS_REVIIRI_HAS_SUCCESSFUL)) {
							doneOnce = true;
							return true;
						}
						return false;
					}
					
					@Override
					public String getString(int i) throws SQLException {
						return CONFLICT_PESA_ID;
					}
					
					@Override
					public String getString(String columnLabel) throws SQLException {
						return null;
					}
					
					@Override
					public Date getDate(String columnLabel) throws SQLException {
						return null;
					}
					
					@Override
					public Date getDate(int i) throws SQLException {
						return null;
					}
				}
				
				FakeResultSet rs = new FakeResultSet(values);
				resultHandler.process(rs);
			}
		}
		
		private static final String	EI_TARKASTETTU	= "8";
		private static final String	ZERO			= "0";
		private static final String	HELSIN			= "HELSIN";
		private _Data<_PesaDomainModelRow>				data;
		private _Validator			validator;
		private _Row				params;
		private _DAO<_PesaDomainModelRow>				dao;
		private final PesaDomainModelDatabaseStructure	databaseStructure = MerikotkaDatabaseStructureCreator.hardCoded();
		private _Request<_PesaDomainModelRow>			request;
		private final Map<String,Selection> selections = setSelections();
		
		private static class TestFactory extends MerikotkaFactory {
			public TestFactory(String configFile) throws Exception {
				super(configFile);
			}
			@Override
			public Map<String, Selection> fetchSelections(_DAO<_PesaDomainModelRow> dao)  {
				return null;
			}
			@Override
			public TipuApiResource getTipuApiResource(String resourcename)  {
				if (resourcename.equals(TipuAPIClient.MUNICIPALITIES)) {
					return kunnat;
				}
				throw new IllegalArgumentException("Not implemeted for resource " + resourcename);
			}
		}
		
		@Override
		protected void setUp() throws Exception {
			_RowFactory<_PesaDomainModelRow> rowFactory = new PesaDomainModelRowFactory(databaseStructure, "", "");
			data = new Data<>(rowFactory);
			data.setPage(Const.TARKASTUS_PAGE);
			data.setAction(Const.INSERT_FIRST);
			data.setSelections(selections);
			dao = new FakeDAO();
			MerikotkaFactory factory = new TestFactory(TestConfig.getConfigFile("pese_merikotka.config"));
			factory.setRowFactory(rowFactory);
			request = new Request<>(new RequestImpleData<_PesaDomainModelRow>().setData(data).setDao(dao).setApplication(factory));
			resetValidator();
			params = data.updateparameters();
		}
		
		private static Map<String, Selection> setSelections() {
			Map<String, Selection> selections = new HashMap<>();
			
			SelectionImple epaonn = new SelectionImple("pesatarkastus.epaonni_syy");
			epaonn.addOption(new SelectionOptionImple("L", "Foobar"));
			selections.put(epaonn.getName(), epaonn);
			
			SelectionImple koordtyyppi = new SelectionImple("pesavakio.koord_tyyppi");
			koordtyyppi.addOption(new SelectionOptionImple("E", "Euref"));
			koordtyyppi.addOption(new SelectionOptionImple("Y", "Yhtenäis"));
			koordtyyppi.addOption(new SelectionOptionImple("A", "Aste"));
			selections.put(koordtyyppi.getName(), koordtyyppi);
			
			SelectionImple koordmittaus = new SelectionImple("pesavakio.koord_mittaus");
			koordmittaus.addOption(new SelectionOptionImple("G", "GPS"));
			selections.put(koordmittaus.getName(), koordmittaus);
			
			SelectionImple koordtark = new SelectionImple("pesavakio.koord_tark");
			koordtark.addOption(new SelectionOptionImple("1", "Tarkka"));
			selections.put(koordtark.getName(), koordtark);
			
			SelectionImple kunnat = new SelectionImple(TipuAPIClient.MUNICIPALITIES);
			kunnat.addOption(new SelectionOptionImple(HELSIN, "Helsinki"));
			kunnat.addOption(new SelectionOptionImple("INKOO", "Inkooo"));
			selections.put(kunnat.getName(), kunnat);
			
			SelectionImple kunto = new SelectionImple("pesatarkastus.pesa_kunto");
			kunto.addOption(new SelectionOptionImple("P", "Puussa"));
			kunto.addOption(new SelectionOptionImple("U", "Tuho"));
			selections.put(kunto.getName(), kunto);
			
			SelectionImple tark = new SelectionImple("pesamuuttuva.korkeus_tark");
			tark.addOption(new SelectionOptionImple("M", "mitattu"));
			selections.put(tark.getName(), tark);
			
			SelectionImple pvmtark = new SelectionImple("pesatarkastus.tark_pvm_tark");
			pvmtark.addOption(new SelectionOptionImple("1", "tarkka"));
			selections.put(pvmtark.getName(), pvmtark);
			
			SelectionImple pesimist = new SelectionImple("pesatarkastus.pesimistulos");
			pesimist.addOption(new SelectionOptionImple("A", "Asumaton"));
			pesimist.addOption(new SelectionOptionImple("K", "Koristl"));
			pesimist.addOption(new SelectionOptionImple("J", "Jotakuinkin koristelty"));
			pesimist.addOption(new SelectionOptionImple("M", "Munittu"));
			pesimist.addOption(new SelectionOptionImple("P", "Poikasia"));
			pesimist.addOption(new SelectionOptionImple("R", "Reng poik"));
			pesimist.addOption(new SelectionOptionImple("L", "Lentopoik"));
			pesimist.addOption(new SelectionOptionImple("U", "Ei ilmoitettu"));
			selections.put(pesimist.getName(), pesimist);
			
			SelectionImple merkit = new SelectionImple("pesatarkastus.nahdyt_merkit");
			merkit.addOption(new SelectionOptionImple("Q", "Foobar"));
			merkit.addOption(new SelectionOptionImple("A", "Foobar"));
			merkit.addOption(new SelectionOptionImple("R", "Foobar"));
			merkit.addOption(new SelectionOptionImple("X", "Foobar"));
			selections.put(merkit.getName(), merkit);
			
			SelectionImple lkm = new SelectionImple("pesatarkastus.elavia_lkm");
			lkm.addOption(new SelectionOptionImple("0", "Ei yhtään"));
			lkm.addOption(new SelectionOptionImple("2", "Kaksi"));
			lkm.addOption(new SelectionOptionImple("3", "Kolme"));
			selections.put(lkm.getName(), lkm);
			
			SelectionImple lajit = new SelectionImple(TipuAPIClient.SPECIES);
			lajit.addOption(new SelectionOptionImple("HALALB", "HALALB"));
			lajit.addOption(new SelectionOptionImple("ACCGEN", "ACCGEN"));
			lajit.addOption(new SelectionOptionImple("MOTALB", "MOTALB"));
			selections.put(lajit.getName(), lajit);
			
			SelectionImple tarktapa = new SelectionImple("pesatarkastus.tark_tapa");
			tarktapa.addOption(new SelectionOptionImple(EI_TARKASTETTU, "Ei tarkastettu"));
			tarktapa.addOption(new SelectionOptionImple("1", "Kiikaroitu tai jotain"));
			selections.put(tarktapa.getName(), tarktapa);
			
			SelectionImple mittaustapa = new SelectionImple("poikanen.siipi_pituus_m");
			mittaustapa.addOption(new SelectionOptionImple("M", "Minimimitta"));
			selections.put(mittaustapa.getName(), mittaustapa);
			
			SelectionImple reviiri = new SelectionImple(MerikotkaConst.REVIIRIT);
			reviiri.addOption(new SelectionOptionImple(THIS_REVIIRI_HAS_SUCCESSFUL, "reviirinimi"));
			reviiri.addOption(new SelectionOptionImple(THIS_REVIIRI_DOESNT_HAVE_SUCCESSFUL, "reviirinimi"));
			selections.put(reviiri.getName(), reviiri);
			
			addSelection(selections, TipuAPIClient.RINGERS, "100");
			addSelection(selections, "pesatarkastus.aikuisia_lkm", "2");
			addSelection(selections, "pesatarkastus.pesimist_tark", "V");
			addSelection(selections, MerikotkaConst.YES_NO_SELECTION, "K", "E");
			
			return selections;
		}
		
		private static void addSelection(Map<String, Selection> selections, String name, String ... values) {
			SelectionImple selection = new SelectionImple(name);
			for (String value : values) {
				selection.addOption(new SelectionOptionImple(value, value));
			}
			selections.put(selection.getName(), selection);
		}
		
		@Override
		protected void tearDown() throws Exception {}
		
		public void test__() throws Exception {
			validator.validate();
		}
		
		public void test__general_type_validations() throws Exception {
			column("pesavakio.pesanimi").setValue("liiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii an piiiiiiiiiiiiiiiiiiitkä");
			column("pesatarkastus.et_as").setValue("a");
			validator.validate();
			assertEquals(_Validator.TOO_LONG, error("pesavakio.pesanimi"));
			assertEquals(_Validator.MUST_BE_INTEGER, error("pesatarkastus.et_as"));
		}
		
		private String error(String column) {
			return validator.errors().get(column);
		}
		
		private String warning(String column) {
			return validator.warnings().get(column);
		}
		
		private _Column column(String column) {
			return params.getColumn(column);
		}
		
		public void test___ids___insert_first() throws Exception {
			data.setAction(Const.INSERT_FIRST);
			validator.validate();
			assertEquals(null, error("pesavakio.pesa_id"));
			assertEquals(null, error("pesatarkastus.p_tarkastus_id"));
		}
		
		public void test___ids___insert() throws Exception {
			data.setAction(Const.INSERT);
			validator.validate();
			assertEquals(_Validator.CAN_NOT_BE_NULL, error("pesavakio.pesa_id"));
			assertEquals(null, error("pesatarkastus.p_tarkastus_id"));
		}
		
		public void test___ids___update() throws Exception {
			data.setAction(Const.UPDATE);
			validator.validate();
			assertEquals(_Validator.CAN_NOT_BE_NULL, error("pesavakio.pesa_id"));
			assertEquals(_Validator.CAN_NOT_BE_NULL, error("pesatarkastus.p_tarkastus_id"));
		}
		
		public void test__required_fields() throws Exception {
			validator.validate();
			assertEquals(_Validator.CAN_NOT_BE_NULL, error("pesatarkastus.tark_vuosi"));
			assertEquals(_Validator.CAN_NOT_BE_NULL, error("vuosi.pesan_kunta"));
			assertEquals(_Validator.CAN_NOT_BE_NULL, error("vuosi.reviirin_kunta"));
			assertEquals(_Validator.CAN_NOT_BE_NULL, error("vuosi.reviiri_id"));
			assertEquals(_Validator.CAN_NOT_BE_NULL, error("pesavakio.pesanimi"));
			assertEquals(_Validator.CAN_NOT_BE_NULL, error("pesavakio.koord_mittaus"));
			assertEquals(_Validator.CAN_NOT_BE_NULL, error("pesavakio.koord_tyyppi"));
			assertEquals(_Validator.CAN_NOT_BE_NULL, error("pesavakio.koord_tark"));
			assertEquals(_Validator.CAN_NOT_BE_NULL, error("pesamuuttuva.pesa_mit_pvm"));
			assertEquals(_Validator.CAN_NOT_BE_NULL, error("pesamuuttuva.ymp_mit_pvm"));
			assertEquals(_Validator.CAN_NOT_BE_NULL, error("pesatarkastus.tark_pvm"));
			assertEquals(_Validator.CAN_NOT_BE_NULL, error("pesatarkastus.tarkastaja1_id"));
			assertEquals(_Validator.CAN_NOT_BE_NULL, error("pesatarkastus.tark_pvm_tark"));
			assertEquals(_Validator.CAN_NOT_BE_NULL, error("pesatarkastus.pesimistulos"));
			assertEquals(_Validator.CAN_NOT_BE_NULL, error("pesatarkastus.pesimist_tark"));
			assertEquals(_Validator.CAN_NOT_BE_NULL, error("pesatarkastus.nahdyt_merkit"));
			assertEquals(_Validator.CAN_NOT_BE_NULL, error("pesatarkastus.tark_tapa"));
		}
		
		public void test__mittaus_pvm_1__mittaaja_1___required_if__1st_measurements_given() throws Exception {
			column("poikanen.1.siipi_pituus").setValue("3");
			validator.validate();
			assertEquals(_Validator.CAN_NOT_BE_NULL, error("pesatarkastus.mittaus_pvm_1"));
			assertEquals(_Validator.CAN_NOT_BE_NULL, error("pesatarkastus.mittaaja_id_1"));
		}
		
		public void test__mittaus_pvm_1__mittaaja_1___required_if__1st_measurements_given2() throws Exception {
			validator.validate();
			assertEquals(null, error("pesatarkastus.mittaus_pvm_1"));
			assertEquals(null, error("pesatarkastus.mittaaja_id_1"));
		}
		
		public void test__mittaus_pvm_1__mittaaja_1___required_if__1st_measurements_given3() throws Exception {
			column("pesatarkastus.mittaus_pvm_1").setValue("3.6.2010");
			column("pesatarkastus.mittaaja_id_1").setValue("3");
			validator.validate();
			assertEquals(_Validator.MUST_BE_NULL, error("pesatarkastus.mittaus_pvm_1"));
			assertEquals(_Validator.MUST_BE_NULL, error("pesatarkastus.mittaaja_id_1"));
		}
		
		public void test__tark_pvm_2___mittaaja_2___required_if__2nd_measurements_given() throws Exception {
			column("poikanen.1.siipi_pituus").setValue("3");
			validator.validate();
			assertEquals(null, error("pesatarkastus.tark_pvm_2"));
			assertEquals(null, error("pesatarkastus.mittaaja_id_2"));
		}
		
		public void test__tark_pvm_2__mittaaja_2___required_if__2nd_measurements_given2() throws Exception {
			column("poikanen.1.siipi_pituus_2").setValue("3");
			validator.validate();
			assertEquals(_Validator.CAN_NOT_BE_NULL, error("pesatarkastus.mittaus_pvm_2"));
			assertEquals(_Validator.CAN_NOT_BE_NULL, error("pesatarkastus.mittaaja_id_2"));
		}
		
		public void test__epaonn_tark__required_if__epaonnistuminen_given() throws Exception {
			validator.validate();
			assertEquals(null, error("pesatarkastus.epaonn_tark"));
		}
		
		public void test__epaonn_tark__required_if__epaonnistuminen_given2() throws Exception {
			column("pesatarkastus.epaonni_syy").setValue("L");
			validator.validate();
			assertEquals(_Validator.CAN_NOT_BE_NULL, error("pesatarkastus.epaonni_tark"));
		}
		
		public void test__tark_vuosi__must_be_the_same_or_max_one_year_earlier__than_tark_pvm_yyyy() throws Exception {
			column("pesatarkastus.tark_vuosi").setValue("2000");
			column("pesatarkastus.tark_pvm").setValue("20.6.2000");
			validator.validate();
			assertEquals(null, error("pesatarkastus.tark_vuosi"));
			assertEquals(null, error("pesatarkastus.tark_pvm"));
		}
		
		public void test__tark_vuosi__must_be_the_same_or_max_one_year_earlier__than_tark_pvm_yyyy2() throws Exception {
			column("pesatarkastus.tark_vuosi").setValue("2000");
			column("pesatarkastus.tark_pvm").setValue("20.6.2001");
			validator.validate();
			assertEquals(null, error("pesatarkastus.tark_vuosi"));
			assertEquals(null, error("pesatarkastus.tark_pvm"));
		}
		
		public void test__tark_vuosi__must_be_the_same_or_max_one_year_earlier__than_tark_pvm_yyyy3() throws Exception {
			column("pesatarkastus.tark_vuosi").setValue("2000");
			column("pesatarkastus.tark_pvm").setValue("20.6.2002");
			validator.validate();
			assertEquals(null, error("pesatarkastus.tark_vuosi"));
			assertEquals(MerikotkaConst.INVALID_TARK_VUOSI_TARK_PVM_YYYY, error("pesatarkastus.tark_pvm"));
		}
		
		public void test__tark_vuosi__must_be_the_same_or_max_one_year_earlier__than_tark_pvm_yyyy4() throws Exception {
			column("pesatarkastus.tark_vuosi").setValue("2001");
			column("pesatarkastus.tark_pvm").setValue("20.6.2000");
			validator.validate();
			assertEquals(null, error("pesatarkastus.tark_vuosi"));
			assertEquals(MerikotkaConst.INVALID_TARK_VUOSI_TARK_PVM_YYYY, error("pesatarkastus.tark_pvm"));
		}
		
		public void test__tark_vuosi__must_be_the_same_or_max_one_year_earlier__than_tark_pvm_yyyy____invalid_date() throws Exception {
			column("pesatarkastus.tark_vuosi").setValue("2001");
			column("pesatarkastus.tark_pvm").setValue("20.6.200a");
			validator.validate();
			assertEquals(null, error("pesatarkastus.tark_vuosi"));
			assertEquals(_Validator.INVALID_DATE, error("pesatarkastus.tark_pvm"));
		}
		
		public void test__coordinates_given__are_valid() throws Exception {
			column("pesavakio.koord_tyyppi").setValue("F");
			validator.validate();
			assertEquals(_Validator.ASSOSIATED_SELECTION_DOES_NOT_CONTAIN_THE_VALUE, error("pesavakio.koord_tyyppi"));
		}
		
		public void test__coordinates_given__are_valid2() throws Exception {
			column("pesavakio.koord_tyyppi").setValue("Y");
			validator.validate();
			assertEquals(null, error("pesavakio.koord_tyyppi"));
			assertEquals(_Validator.CAN_NOT_BE_NULL, error("pesavakio.yht_leveys"));
			assertEquals(_Validator.CAN_NOT_BE_NULL, error("pesavakio.yht_pituus"));
		}
		
		public void test__coordinates_given__are_valid3() throws Exception {
			column("pesavakio.koord_tyyppi").setValue("Y");
			column("pesavakio.yht_leveys").setValue("asdsa");
			column("pesavakio.yht_pituus").setValue("36");
			validator.validate();
			assertEquals(null, error("pesavakio.koord_tyyppi"));
			assertEquals(_Validator.MUST_BE_INTEGER, error("pesavakio.yht_leveys"));
			assertEquals(_Validator.INVALID_VALUE, error("pesavakio.yht_pituus"));
		}
		
		public void test__coordinates_given__are_valid4() throws Exception {
			column("pesavakio.koord_tyyppi").setValue("E");
			column("pesavakio.eur_leveys").setValue("asdsa");
			column("pesavakio.eur_pituus").setValue("7500000");
			validator.validate();
			assertEquals(null, error("pesavakio.koord_tyyppi"));
			assertEquals(_Validator.MUST_BE_INTEGER, error("pesavakio.eur_leveys"));
			assertEquals(_Validator.INVALID_VALUE, error("pesavakio.eur_pituus"));
		}
		
		public void test__coordinates_given__are_valid5() throws Exception {
			column("pesavakio.koord_tyyppi").setValue("A");
			column("pesavakio.ast_leveys").setValue("asdsa");
			column("pesavakio.ast_pituus").setValue("192100");
			validator.validate();
			assertEquals(null, error("pesavakio.koord_tyyppi"));
			assertEquals(_Validator.MUST_BE_INTEGER, error("pesavakio.ast_leveys"));
			assertEquals(_Validator.INVALID_VALUE, error("pesavakio.ast_pituus"));
		}
		
		public void test__coordinates_are_inside_selected_kunta() throws Exception {
			column("pesavakio.koord_tyyppi").setValue("Y");
			column("pesavakio.yht_leveys").setValue("6678730");
			column("pesavakio.yht_pituus").setValue("3387147");
			column("vuosi.pesan_kunta").setValue(HELSIN);
			validator.validate();
			assertEquals(null, error("pesavakio.ast_leveys"));
			assertEquals(null, error("pesavakio.ast_pituus"));
			assertEquals(null, error("pesavakio.koord_tyyppi"));
			assertEquals(null, error("vuosi.p_kunta_id"));
		}
		
		public void test__coordinates_are_inside_selected_kunta2() throws Exception {
			column("pesavakio.koord_tyyppi").setValue("Y");
			column("pesavakio.yht_leveys").setValue("6678730");
			column("pesavakio.yht_pituus").setValue("3387147");
			column("vuosi.pesan_kunta").setValue("asdsad");
			validator.validate();
			assertEquals(null, error("pesavakio.ast_leveys"));
			assertEquals(null, error("pesavakio.ast_pituus"));
			assertEquals(null, error("pesavakio.koord_tyyppi"));
			assertEquals(_Validator.ASSOSIATED_SELECTION_DOES_NOT_CONTAIN_THE_VALUE, error("vuosi.pesan_kunta"));
		}
		
		public void test__coordinates_are_inside_selected_kunta3() throws Exception {
			column("pesavakio.koord_tyyppi").setValue("Y");
			column("pesavakio.yht_leveys").setValue("6708000");
			column("pesavakio.yht_pituus").setValue("3388000");
			column("vuosi.pesan_kunta").setValue(HELSIN);
			validator.validate();
			assertEquals(null, error("pesavakio.ast_leveys"));
			assertEquals(_Validator.COORDINATES_NOT_INSIDE_REGION, error("pesavakio.yht_pituus"));
			assertEquals(_Validator.COORDINATES_NOT_INSIDE_REGION, error("pesavakio.yht_leveys"));
			assertEquals(null, error("vuosi.pesan_kunta"));
		}
		
		public void test__years() throws Exception {
			validator.validate();
			assertEquals(null, error("pesavakio.rak_vuosi"));
			assertEquals(null, error("pesavakio.rak_vuosi_teko"));
			assertEquals(null, error("pesavakio.loyt_vuosi"));
			assertEquals(null, error("pesavakio.tuhoutumisvuosi"));
			
			resetValidator();
			String value = "a";
			String errotext = _Validator.MUST_BE_INTEGER;
			column("pesavakio.rak_vuosi").setValue(value);
			column("pesavakio.rak_vuosi_teko").setValue(value);
			column("pesavakio.loyt_vuosi").setValue(value);
			column("pesavakio.tuhoutumisvuosi").setValue(value);
			column("pesatarkastus.tark_vuosi").setValue(value);
			validator.validate();
			assertEquals(errotext, error("pesavakio.rak_vuosi"));
			assertEquals(errotext, error("pesavakio.rak_vuosi_teko"));
			assertEquals(errotext, error("pesavakio.loyt_vuosi"));
			assertEquals(errotext, error("pesavakio.tuhoutumisvuosi"));
			assertEquals(errotext, error("pesatarkastus.tark_vuosi"));
			
			resetValidator();
			value = Integer.toString((int)(_Validator.MIN_YEAR - 1.0));
			errotext = _Validator.TOO_SMALL;
			column("pesavakio.rak_vuosi").setValue(value);
			column("pesavakio.rak_vuosi_teko").setValue(value);
			column("pesavakio.loyt_vuosi").setValue(value);
			column("pesavakio.tuhoutumisvuosi").setValue(value);
			column("pesatarkastus.tark_vuosi").setValue(value);
			validator.validate();
			assertEquals(errotext, error("pesavakio.rak_vuosi"));
			assertEquals(errotext, error("pesavakio.rak_vuosi_teko"));
			assertEquals(errotext, error("pesavakio.loyt_vuosi"));
			assertEquals(errotext, error("pesavakio.tuhoutumisvuosi"));
			assertEquals(errotext, error("pesatarkastus.tark_vuosi"));
			
			resetValidator();
			value = Integer.toString( (DateUtils.getCurrentYear() + 1) );
			errotext = _Validator.CAN_NOT_BE_IN_THE_FUTURE;
			column("pesavakio.rak_vuosi").setValue(value);
			column("pesavakio.rak_vuosi_teko").setValue(value);
			column("pesavakio.loyt_vuosi").setValue(value);
			column("pesavakio.tuhoutumisvuosi").setValue(value);
			column("pesatarkastus.tark_vuosi").setValue(value);
			validator.validate();
			assertEquals(errotext, error("pesavakio.rak_vuosi"));
			assertEquals(errotext, error("pesavakio.rak_vuosi_teko"));
			assertEquals(errotext, error("pesavakio.loyt_vuosi"));
			assertEquals(errotext, error("pesavakio.tuhoutumisvuosi"));
			assertEquals(errotext, error("pesatarkastus.tark_vuosi"));
		}
		
		public void test__rakv_tark_required_if_year_given() throws Exception {
			column("pesavakio.rak_vuosi").setValue("2002");
			validator.validate();
			assertEquals(_Validator.CAN_NOT_BE_NULL, error("pesavakio.rak_vuosi_tark"));
			
			resetValidator();
			column("pesavakio.rak_vuosi").setValue("");
			validator.validate();
			assertEquals(null, error("pesavakio.rak_vuosi_tark"));
			
			resetValidator();
			column("pesavakio.rak_vuosi_teko").setValue("2002");
			validator.validate();
			assertEquals(_Validator.CAN_NOT_BE_NULL, error("pesavakio.rak_vuosi_teko_tark"));
			
			resetValidator();
			column("pesavakio.rak_vuosi_teko").setValue("");
			validator.validate();
			assertEquals(null, error("pesavakio.rak_vuosi_teko_tark"));
		}
		
		public void test__rakv_not_earlier_than_loytov() throws Exception {
			validator.validate();
			assertEquals(null, error("pesavakio.rak_vuosi"));
			assertEquals(null, error("pesavakio.loyt_vuosi"));
			
			resetValidator();
			column("pesavakio.rak_vuosi").setValue("2005");
			validator.validate();
			assertEquals(null, error("pesavakio.rak_vuosi"));
			assertEquals(null, error("pesavakio.loyt_vuosi"));
			
			resetValidator();
			column("pesavakio.loyt_vuosi").setValue("2005");
			column("pesavakio.rak_vuosi").setValue("");
			validator.validate();
			assertEquals(null, error("pesavakio.rak_vuosi"));
			assertEquals(null, error("pesavakio.loyt_vuosi"));
			
			resetValidator();
			column("pesavakio.loyt_vuosi").setValue("2005");
			column("pesavakio.rak_vuosi").setValue("2005");
			validator.validate();
			assertEquals(null, error("pesavakio.rak_vuosi"));
			assertEquals(null, error("pesavakio.loyt_vuosi"));
			
			resetValidator();
			column("pesavakio.loyt_vuosi").setValue("2005");
			column("pesavakio.rak_vuosi").setValue("2002");
			validator.validate();
			assertEquals(null, error("pesavakio.rak_vuosi"));
			assertEquals(null, error("pesavakio.loyt_vuosi"));
			
			resetValidator();
			column("pesavakio.loyt_vuosi").setValue("2004");
			column("pesavakio.rak_vuosi").setValue("2005");
			validator.validate();
			assertEquals(MerikotkaConst.INVALID_RAK_VUOSI, error("pesavakio.rak_vuosi"));
			assertEquals(MerikotkaConst.INVALID_RAK_VUOSI, error("pesavakio.loyt_vuosi"));
		}
		
		public void test__rakv_teko__not_earlier_than_loytov() throws Exception {
			validator.validate();
			assertEquals(null, error("pesavakio.rak_vuosi_teko"));
			assertEquals(null, error("pesavakio.loyt_vuosi"));
			
			resetValidator();
			column("pesavakio.rak_vuosi_teko").setValue("2005");
			validator.validate();
			assertEquals(null, error("pesavakio.rak_vuosi_teko"));
			assertEquals(null, error("pesavakio.loyt_vuosi"));
			
			resetValidator();
			column("pesavakio.loyt_vuosi").setValue("2005");
			column("pesavakio.rak_vuosi_teko").setValue("");
			validator.validate();
			assertEquals(null, error("pesavakio.rak_vuosi_teko"));
			assertEquals(null, error("pesavakio.loyt_vuosi"));
			
			resetValidator();
			column("pesavakio.loyt_vuosi").setValue("2005");
			column("pesavakio.rak_vuosi_teko").setValue("2005");
			validator.validate();
			assertEquals(null, error("pesavakio.rak_vuosi_teko"));
			assertEquals(null, error("pesavakio.loyt_vuosi"));
			
			resetValidator();
			column("pesavakio.loyt_vuosi").setValue("2005");
			column("pesavakio.rak_vuosi_teko").setValue("2002");
			validator.validate();
			assertEquals(null, error("pesavakio.rak_vuosi_teko"));
			assertEquals(null, error("pesavakio.loyt_vuosi"));
			
			resetValidator();
			column("pesavakio.loyt_vuosi").setValue("2004");
			column("pesavakio.rak_vuosi_teko").setValue("2005");
			validator.validate();
			assertEquals(MerikotkaConst.INVALID_RAK_VUOSI, error("pesavakio.rak_vuosi_teko"));
			assertEquals(MerikotkaConst.INVALID_RAK_VUOSI, error("pesavakio.loyt_vuosi"));
		}
		
		public void test__tuhoutumisv_before_rakennusv() throws Exception {
			column("pesavakio.rak_vuosi").setValue("2007");
			column("pesavakio.tuhoutumisvuosi").setValue("2004");
			validator.validate();
			assertEquals(MerikotkaConst.INVALID_RAK_VUOSI, error("pesavakio.rak_vuosi"));
			assertEquals(MerikotkaConst.INVALID_RAK_VUOSI, error("pesavakio.tuhoutumisvuosi"));
		}
		
		public void test__tuhoutumisv_before_loytvuosi() throws Exception {
			column("pesavakio.loyt_vuosi").setValue("2007");
			column("pesavakio.tuhoutumisvuosi").setValue("2004");
			validator.validate();
			assertEquals(MerikotkaConst.INVALID_LOYT_VUOSI, error("pesavakio.loyt_vuosi"));
			assertEquals(MerikotkaConst.INVALID_LOYT_VUOSI, error("pesavakio.tuhoutumisvuosi"));
		}
		
		public void test__rauh_aika() throws Exception {
			column("pesamuuttuva.rauh_aika_alku").setValue("1.1.2007");
			column("pesamuuttuva.rauh_aika_loppu").setValue("25.10.2004");
			validator.validate();
			assertEquals(MerikotkaConst.INVALID_RAUH_AIKA_ALKU_LOPPU, error("pesamuuttuva.rauh_aika_alku"));
			assertEquals(MerikotkaConst.INVALID_RAUH_AIKA_ALKU_LOPPU, error("pesamuuttuva.rauh_aika_loppu"));
		}
		
		public void test__kunto_bad__tuhoutumisvuosi_required() throws Exception {
			column("pesatarkastus.pesa_kunto").setValue("U");
			column("pesavakio.tuhoutumisvuosi").setValue("");
			validator.validate();
			assertEquals(MerikotkaConst.INVALID_TUHOUTUMISVUOSI_PESAN_KUNTO, error("pesatarkastus.pesa_kunto"));
			assertEquals(MerikotkaConst.INVALID_TUHOUTUMISVUOSI_PESAN_KUNTO, error("pesavakio.tuhoutumisvuosi"));
		}
		
		public void test__kunto_good__tuhoutumisvuosi_not_permitted() throws Exception {
			column("pesatarkastus.pesa_kunto").setValue("P");
			column("pesavakio.tuhoutumisvuosi").setValue("2008");
			column("pesatarkastus.tark_vuosi").setValue("2008");
			validator.validate();
			assertEquals(MerikotkaConst.INVALID_TUHOUTUMISVUOSI_PESAN_KUNTO, error("pesatarkastus.pesa_kunto"));
			assertEquals(MerikotkaConst.INVALID_TUHOUTUMISVUOSI_PESAN_KUNTO, error("pesavakio.tuhoutumisvuosi"));
		}
		
		public void test__korkeus_and_tark__or_neither() throws Exception {
			column("pesamuuttuva.korkeus").setValue("10");
			column("pesamuuttuva.korkeus_tark").setValue("M");
			validator.validate();
			assertEquals(null, error("pesamuuttuva.korkeus"));
			assertEquals(null, error("pesamuuttuva.korkeus_tark"));
			
			resetValidator();
			column("pesamuuttuva.korkeus").setValue("");
			column("pesamuuttuva.korkeus_tark").setValue("");
			validator.validate();
			assertEquals(null, error("pesamuuttuva.korkeus"));
			assertEquals(null, error("pesamuuttuva.korkeus_tark"));
			
			resetValidator();
			column("pesamuuttuva.korkeus").setValue("10");
			column("pesamuuttuva.korkeus_tark").setValue("");
			validator.validate();
			assertEquals(null, error("pesamuuttuva.korkeus"));
			assertEquals(MerikotkaConst.INVALID_KORK_TARK, error("pesamuuttuva.korkeus_tark"));
			
			resetValidator();
			column("pesamuuttuva.korkeus").setValue("");
			column("pesamuuttuva.korkeus_tark").setValue("M");
			validator.validate();
			assertEquals(null, error("pesamuuttuva.korkeus"));
			assertEquals(MerikotkaConst.INVALID_KORK_TARK, error("pesamuuttuva.korkeus_tark"));
		}
		
		public void test__halk_max___can_not_be_smaller_than__halk_min() throws Exception {
			column("pesatarkastus.pesa_halk_min").setValue("20");
			column("pesatarkastus.pesa_halk_max").setValue("19");
			validator.validate();
			assertEquals(_Validator.INVALID_VALUE, error("pesatarkastus.pesa_halk_min"));
			assertEquals(_Validator.INVALID_VALUE, error("pesatarkastus.pesa_halk_max"));
		}
		
		public void test___pesimistulos__poikanen_not_allowed() throws Exception {
			column("pesatarkastus.pesimistulos").setValue("A");
			column("poikanen.3.rengas_oikea").setValue("fooo");
			validator.validate();
			assertEquals(MerikotkaConst.INVALID_PESIMISTULOS_POIKASET, error("pesatarkastus.pesimistulos"));
		}
		
		public void test___if__pesinnan_merkit__QRT_or_V___poikascount_must_be_above_zero() throws Exception {
			column("pesatarkastus.nahdyt_merkit").setValue("Q");
			column("pesatarkastus.elavia_lkm").setValue("");
			validator.validate();
			assertEquals(MerikotkaConst.INVALID_NAHDYT_MERKIT_POIKASCOUNT, error("pesatarkastus.elavia_lkm"));
			assertEquals(null, error("pesatarkastus.pesa_merkit"));
		}
		
		public void test___if__pesinnan_merkit__QRT_or_V___poikascount_must_be_above_zero2() throws Exception {
			column("pesatarkastus.nahdyt_merkit").setValue("Q");
			column("pesatarkastus.elavia_lkm").setValue("0");
			validator.validate();
			assertEquals(MerikotkaConst.INVALID_NAHDYT_MERKIT_POIKASCOUNT, error("pesatarkastus.elavia_lkm"));
			assertEquals(null, error("pesatarkastus.pesa_merkit"));
		}
		
		public void test___if__pesinnan_merkit__QRT_or_V___poikascount_above_zero3() throws Exception {
			column("pesatarkastus.nahdyt_merkit").setValue("A");
			column("pesatarkastus.elavia_lkm").setValue("0");
			validator.validate();
			assertEquals(null, error("pesatarkastus.elavia_lkm"));
			assertEquals(null, error("pesatarkastus.pesa_merkit"));
		}
		
		public void test___if__pesinnan_merkit__QRT_or_V___poikascount_must_be_above_zero4() throws Exception {
			column("pesatarkastus.nahdyt_merkit").setValue("Q");
			column("pesatarkastus.elavia_lkm").setValue("3");
			validator.validate();
			assertEquals(null, error("pesatarkastus.elavia_lkm"));
			assertEquals(null, error("pesatarkastus.pesa_merkit"));
		}
		
		public void test___pesimistulos__counts_not_allowed() throws Exception {
			column("pesatarkastus.pesimistulos").setValue("A");
			column("pesatarkastus.elavia_lkm").setValue("2");
			column("pesatarkastus.reng_poik_lkm").setValue("2");
			column("pesatarkastus.lentopoik_lkm").setValue("2");
			validator.validate();
			assertEquals(MerikotkaConst.INVALID_PESIMISTULOS_POIKASET, error("pesatarkastus.elavia_lkm"));
			assertEquals(MerikotkaConst.INVALID_PESIMISTULOS_POIKASET, error("pesatarkastus.reng_poik_lkm"));
			assertEquals(MerikotkaConst.INVALID_PESIMISTULOS_POIKASET, error("pesatarkastus.lentopoik_lkm"));
			
			resetValidator();
			column("pesatarkastus.pesimistulos").setValue("A");
			column("pesatarkastus.elavia_lkm").setValue(ZERO);
			column("pesatarkastus.reng_poik_lkm").setValue(ZERO);
			column("pesatarkastus.lentopoik_lkm").setValue(ZERO);
			validator.validate();
			assertEquals(null, error("pesatarkastus.elavia_lkm"));
			assertEquals(null, error("pesatarkastus.reng_poik_lkm"));
			assertEquals(null, error("pesatarkastus.lentopoik_lkm"));
		}
		
		public void test___allowed_pesimistulos__nahdyt_merkit__combinations() throws Exception {
			column("pesatarkastus.pesimistulos").setValue("A");
			column("pesatarkastus.nahdyt_merkit").setValue("A");
			validator.validate();
			assertEquals(null, error("pesatarkastus.pesimistulos"));
			assertEquals(null, error("pesatarkastus.nahdyt_merkit"));
			
			resetValidator();
			column("pesatarkastus.pesimistulos").setValue("A");
			column("pesatarkastus.nahdyt_merkit").setValue("R");
			validator.validate();
			assertEquals(MerikotkaConst.INVALID_PESIMISTULOS_NAHDYT_MERKIT, error("pesatarkastus.pesimistulos"));
			assertEquals(MerikotkaConst.INVALID_PESIMISTULOS_NAHDYT_MERKIT, error("pesatarkastus.nahdyt_merkit"));
			
			resetValidator();
			column("pesatarkastus.pesimistulos").setValue("L");
			column("pesatarkastus.nahdyt_merkit").setValue("X");
			validator.validate();
			assertEquals(MerikotkaConst.INVALID_PESIMISTULOS_NAHDYT_MERKIT, error("pesatarkastus.pesimistulos"));
			assertEquals(MerikotkaConst.INVALID_PESIMISTULOS_NAHDYT_MERKIT, error("pesatarkastus.nahdyt_merkit"));
			
			resetValidator();
			column("pesatarkastus.pesimistulos").setValue("K");
			column("pesatarkastus.nahdyt_merkit").setValue("X");
			validator.validate();
			assertEquals(null, error("pesatarkastus.pesimistulos"));
			assertEquals(null, error("pesatarkastus.nahdyt_merkit"));
		}
		
		public void test___muulaji__and__rak_laji___references_laji() throws Exception {
			column("pesavakio.rak_laji").setValue("FOOOBAR");
			column("pesatarkastus.muulaji").setValue("FOOOBAR");
			validator.validate();
			assertEquals(_Validator.ASSOSIATED_SELECTION_DOES_NOT_CONTAIN_THE_VALUE, error("pesavakio.rak_laji"));
			assertEquals(_Validator.ASSOSIATED_SELECTION_DOES_NOT_CONTAIN_THE_VALUE, error("pesatarkastus.muulaji"));
			
			resetValidator();
			column("pesatarkastus.muulaji").setValue("HALALB");
			validator.validate();
			assertEquals(MerikotkaConst.MUULAJI_CAN_NOT_BE_HALALB, error("pesatarkastus.muulaji"));
			
			resetValidator();
			column("pesatarkastus.muulaji").setValue("ACCGEN");
			column("pesatarkastus.pesimistulos").setValue("R");
			validator.validate();
			assertEquals(MerikotkaConst.INVALID_MUULAJI_PESIMISTULOS, error("pesatarkastus.muulaji"));
		}
		
		public void test___muulaji__rinnakkainen__ei_saa_olla_merikotk_tai_vastarakki() throws Exception {
			column("pesatarkastus.muulaji_rinnakkainen").setValue("HALALB");
			validator.validate();
			assertEquals(MerikotkaConst.MUULAJI_RINNAKKAINEN, error("pesatarkastus.muulaji_rinnakkainen"));
			
			resetValidator();
			column("pesatarkastus.muulaji_rinnakkainen").setValue("MOTALB");
			validator.validate();
			assertEquals(MerikotkaConst.MUULAJI_RINNAKKAINEN, error("pesatarkastus.muulaji_rinnakkainen"));
			
			resetValidator();
			column("pesatarkastus.muulaji_rinnakkainen").setValue("ACCGEN");
			validator.validate();
			assertEquals(null, error("pesatarkastus.muulaji_rinnakkainen"));
		}
		
		public void test___r_taulu() throws Exception {
			column("pesavakio.r_taulu_pvm").setValue("6.6.2000");
			column("pesavakio.r_taulu_poisto_pvm").setValue("1.7.2001");
			validator.validate();
			assertEquals(null, error("pesavakio.r_taulu_pvm"));
			assertEquals(null, error("pesavakio.r_taulu_poisto_pvm"));
			
			resetValidator();
			column("pesavakio.r_taulu_pvm").setValue("6.6.2000");
			column("pesavakio.r_taulu_poisto_pvm").setValue("1.1.2000");
			validator.validate();
			assertEquals(null, error("pesavakio.r_taulu_pvm"));
			assertEquals(MerikotkaConst.INVALID_R_TAULU_POISTO_KIINNITYS, error("pesavakio.r_taulu_poisto_pvm"));
		}
		
		private void resetValidator() {
			validator = new MerikotkaTarkastusValidator(request);
		}
		
		public void test__tark_tapa() throws Exception {
			column("pesatarkastus.tark_tapa").setValue(EI_TARKASTETTU);
			column("pesatarkastus.epaonni_syy").setValue("B");
			validator.validate();
			assertEquals(MerikotkaConst.INVALID_TARK_TAPA, error("pesatarkastus.tark_tapa"));
			
			column("pesatarkastus.tark_tapa").setValue(EI_TARKASTETTU);
			column("pesatarkastus.reng_poik_lkm").setValue(THIS_REVIIRI_DOESNT_HAVE_SUCCESSFUL);
			validator.validate();
			assertEquals(MerikotkaConst.INVALID_TARK_TAPA, error("pesatarkastus.tark_tapa"));
		}
		
		public void test__siipi_pituus_m__required__if_siipi_pituus_given() throws Exception {
			column("poikanen.2.siipi_pituus").setValue("30");
			column("poikanen.2.siipi_pituus_m").setValue("M");
			validator.validate();
			assertEquals(null, error("poikanen.2.siipi_pituus"));
			assertEquals(null, error("poikanen.2.siipi_pituus_m"));
			
			resetValidator();
			column("poikanen.2.siipi_pituus").setValue("30");
			column("poikanen.2.siipi_pituus_m").setValue("");
			validator.validate();
			assertEquals(null, error("poikanen.2.siipi_pituus"));
			assertEquals(_Validator.CAN_NOT_BE_NULL, error("poikanen.2.siipi_pituus_m"));
			
			resetValidator();
			column("poikanen.2.siipi_pituus_2").setValue("30");
			column("poikanen.2.siipi_pituus_m_2").setValue("");
			validator.validate();
			assertEquals(null, error("poikanen.2.siipi_pituus_2"));
			assertEquals(_Validator.CAN_NOT_BE_NULL, error("poikanen.2.siipi_pituus_m_2"));
			
			resetValidator();
			column("poikanen.2.siipi_pituus").setValue("");
			column("poikanen.2.siipi_pituus_m").setValue("M");
			validator.validate();
			assertEquals(_Validator.CAN_NOT_BE_NULL, error("poikanen.2.siipi_pituus"));
			assertEquals(null, error("poikanen.2.siipi_pituus_m"));
			
			resetValidator();
			column("poikanen.2.siipi_pituus_2").setValue("");
			column("poikanen.2.siipi_pituus_m_2").setValue("M");
			validator.validate();
			assertEquals(_Validator.CAN_NOT_BE_NULL, error("poikanen.2.siipi_pituus_2"));
			assertEquals(null, error("poikanen.2.siipi_pituus_m_2"));
		}
		
		public void test__tark_tunti() throws Exception {
			column("pesatarkastus.tark_tunti").setValue("");
			validator.validate();
			assertEquals(null, error("pesatarkastus.tark_tunti"));
			
			resetValidator();
			column("pesatarkastus.tark_tunti").setValue("24");
			validator.validate();
			assertEquals(_Validator.INVALID_VALUE, error("pesatarkastus.tark_tunti"));
			
			resetValidator();
			column("pesatarkastus.tark_tunti").setValue("-1");
			validator.validate();
			assertEquals(_Validator.INVALID_VALUE, error("pesatarkastus.tark_tunti"));
		}
		
		public void test__insert_first_doesnt_require_pesaid() throws Exception {
			validator.validate();
			assertEquals(null, error("pesavakio.pesa_id"));
		}
		
		public void test__insert__requires_pesaids() throws Exception {
			data.setAction(Const.INSERT);
			validator.validate();
			assertEquals(_Validator.CAN_NOT_BE_NULL, error("pesavakio.pesa_id"));
		}
		
		public void test__update___requires_pesaids() throws Exception {
			data.setAction(Const.UPDATE);
			validator.validate();
			assertEquals(_Validator.CAN_NOT_BE_NULL, error("pesavakio.pesa_id"));
		}
		
		public void test___only_one_successful_nesting_for_reviiri_per_year____no_errors_for_unsuccessful() throws Exception {
			params.getColumn("pesatarkastus.pesimistulos").setValue("U");
			params.getColumn("vuosi.reviiri_id").setValue(THIS_REVIIRI_HAS_SUCCESSFUL);
			params.getColumn("vuosi.vuosi").setValue("2009");
			validator.validate();
			assertEquals(null, error("pesatarkastus.pesimistulos"));
		}
		
		public void test___only_one_successful_nesting_for_reviiri_per_year____no_errors_for_successful_if_no_others_for_year() throws Exception {
			params.getColumn("pesatarkastus.pesimistulos").setValue("U");
			params.getColumn("vuosi.reviiri_id").setValue(THIS_REVIIRI_DOESNT_HAVE_SUCCESSFUL);
			params.getColumn("vuosi.vuosi").setValue("2009");
			validator.validate();
			assertEquals(null, error("pesatarkastus.pesimistulos"));
		}
		
		public void test___only_one_successful_nesting_for_reviiri_per_year____error_because_successful_and_reviiri_has_succesful() throws Exception {
			params.getColumn("pesatarkastus.pesimistulos").setValue("P");
			params.getColumn("vuosi.reviiri_id").setValue(THIS_REVIIRI_HAS_SUCCESSFUL);
			params.getColumn("vuosi.vuosi").setValue("2009");
			validator.validate();
			assertEquals(MerikotkaConst.REVIIRI_ALREADY_HAS_SUCCESSFUL_NESTING, error("pesatarkastus.pesimistulos"));
			assertEquals(CONFLICT_PESA_ID, data.getLists().get(Const.RISTIRIIDAT).get(0));
		}
		
		public void test___nilkka_min_must_be_smaller_than_nilkka_max() throws Exception {
			params.getColumn("poikanen.1.nilkka_min").setValue("5");
			params.getColumn("poikanen.1.nilkka_max").setValue("10");
			params.getColumn("poikanen.1.nilkka_min_2").setValue("5");
			params.getColumn("poikanen.1.nilkka_max_2").setValue("10");
			validator.validate();
			assertEquals(null, error("poikanen.1.nilkka_min"));
			assertEquals(null, error("poikanen.1.nilkka_max"));
			assertEquals(null, error("poikanen.1.nilkka_min_2"));
			assertEquals(null, error("poikanen.1.nilkka_max_2"));
		}
		
		public void test___nilkka_min_must_be_smaller_than_nilkka_max2() throws Exception {
			params.getColumn("poikanen.1.nilkka_min").setValue("7");
			params.getColumn("poikanen.1.nilkka_max").setValue("7");
			params.getColumn("poikanen.1.nilkka_min_2").setValue("12");
			params.getColumn("poikanen.1.nilkka_max_2").setValue("11");
			validator.validate();
			assertEquals(MerikotkaConst.NILKKA_MIN_MUST_BE_SMALLER_THAN_NILKKA_MAX, error("poikanen.1.nilkka_min"));
			assertEquals(MerikotkaConst.NILKKA_MIN_MUST_BE_SMALLER_THAN_NILKKA_MAX, error("poikanen.1.nilkka_max"));
			assertEquals(MerikotkaConst.NILKKA_MIN_MUST_BE_SMALLER_THAN_NILKKA_MAX, error("poikanen.1.nilkka_min_2"));
			assertEquals(MerikotkaConst.NILKKA_MIN_MUST_BE_SMALLER_THAN_NILKKA_MAX, error("poikanen.1.nilkka_max_2"));
			
		}
		
		public void test___nilkka_min_must_be_smaller_than_nilkka_max3() throws Exception {
			params.getColumn("poikanen.1.nilkka_min").setValue("");
			params.getColumn("poikanen.1.nilkka_max").setValue("15");
			params.getColumn("poikanen.1.nilkka_min_2").setValue("15");
			params.getColumn("poikanen.1.nilkka_max_2").setValue("sdf");
			validator.validate();
			assertEquals(null, error("poikanen.1.nilkka_min"));
			assertEquals(null, error("poikanen.1.nilkka_max"));
			assertEquals(null, error("poikanen.1.nilkka_min_2"));
			assertEquals(_Validator.MUST_BE_DECIMAL, error("poikanen.1.nilkka_max_2"));
		}
		
		public void test___nilkka_min_must_be_smaller_than_nilkka_max4() throws Exception {
			params.getColumn("poikanen.1.nilkka_min").setValue("15,3");
			params.getColumn("poikanen.1.nilkka_max").setValue("16.6");
			validator.validate();
			assertEquals(null, error("poikanen.1.nilkka_min"));
			assertEquals(null, error("poikanen.1.nilkka_max"));
		}
		
		public void test___rauh_pvm__alku_loppu_can_be_in_the_future() throws Exception {
			params.getColumn("pesamuuttuva.rauh_aika_alku").setValue("1.1.2035");
			params.getColumn("pesamuuttuva.rauh_aika_loppu").setValue("2.1.2035");
			validator.validate();
			assertEquals(_Validator.CAN_NOT_BE_IN_THE_FUTURE, error("pesamuuttuva.rauh_aika_alku"));
			assertEquals(null, error("pesamuuttuva.rauh_aika_loppu"));
		}
		
		public void test___poikanen_muna_tiedot__pesimistulos_combinations_1() throws Exception {
			params.getColumn("pesatarkastus.pesimistulos").setValue("M");
			params.getColumn("poikanen.1.siipi_pituus").setValue("421");
			validator.validate();
			assertEquals(MerikotkaConst.INVALID_PESIMISTULOS_POIKASET, error("pesatarkastus.pesimistulos"));
			assertEquals(null, error("poikanen.1.siipi_pituus"));
		}
		
		public void test___poikanen_muna_tiedot__pesimistulos_combinations_2() throws Exception {
			params.getColumn("pesatarkastus.pesimistulos").setValue("P");
			params.getColumn("poikanen.1.siipi_pituus").setValue("421");
			validator.validate();
			assertEquals(null, error("pesatarkastus.pesimistulos"));
			assertEquals(null, error("poikanen.1.siipi_pituus"));
		}
		
		public void test___poikanen_muna_tiedot__pesimistulos_combinations_3() throws Exception {
			params.getColumn("pesatarkastus.pesimistulos").setValue("M");
			params.getColumn("poikanen.1.siipi_pituus").setValue("421");
			validator.validate();
			assertEquals(MerikotkaConst.INVALID_PESIMISTULOS_POIKASET, error("pesatarkastus.pesimistulos"));
			assertEquals(null, error("poikanen.1.siipi_pituus"));
		}
		
		public void test___poikanen_muna_tiedot__pesimistulos_combinations_4() throws Exception {
			params.getColumn("pesatarkastus.pesimistulos").setValue("M");
			params.getColumn("poikanen.1.munan_pituus").setValue("69");
			validator.validate();
			assertEquals(null, error("pesatarkastus.pesimistulos"));
			assertEquals(null, error("poikanen.1.munan_pituus"));
		}
		
		public void test___poikanen_muna_tiedot__pesimistulos_combinations_5() throws Exception {
			params.getColumn("pesatarkastus.pesimistulos").setValue("M");
			validator.validate();
			assertEquals(null, error("pesatarkastus.pesimistulos"));
			assertEquals(null, error("poikanen.1.kommentti"));
		}
		
		public void test___poikanen_muna_tiedot__pesimistulos_combinations_6() throws Exception {
			params.getColumn("pesatarkastus.pesimistulos").setValue("M");
			params.getColumn("poikanen.1.kommentti").setValue("asdasda");
			validator.validate();
			assertEquals(null, error("pesatarkastus.pesimistulos"));
			assertEquals(null, error("poikanen.1.kommentti"));
		}
		
		public void test___poikanen_muna_tiedot__pesimistulos_combinations_7() throws Exception {
			params.getColumn("pesatarkastus.pesimistulos").setValue("P");
			params.getColumn("poikanen.1.kommentti").setValue("asdasda");
			validator.validate();
			assertEquals(null, error("pesatarkastus.pesimistulos"));
			assertEquals(null, error("poikanen.1.kommentti"));
		}
		
		public void test___poikanen_muna_tiedot__pesimistulos_combinations_8() throws Exception {
			params.getColumn("pesatarkastus.pesimistulos").setValue("K");
			params.getColumn("poikanen.1.munan_pituus").setValue("69");
			validator.validate();
			assertEquals(MerikotkaConst.INVALID_PESIMISTULOS_MUNAT, error("pesatarkastus.pesimistulos"));
			assertEquals(null, error("poikanen.1.munan_pituus"));
		}
		
		public void test___pesa_tippunut__pesan_mittoja_ei_saa_ilmoittaa() throws Exception {
			params.getColumn("pesatarkastus.pesa_kunto").setValue("K"); // tippunut
			params.getColumn("pesatarkastus.pesa_korkeus").setValue("69");
			validator.validate();
			assertEquals("PESA_PUDONNUT_EI_SAA_ILMOITTAA_MITTAA", error("pesatarkastus.pesa_korkeus"));
		}
		
		public void test___reviirit_kaukana() throws Exception {
			params.getColumn("vuosi.reviirin_kunta").setValue(HELSIN);
			params.getColumn("vuosi.reviiri_id").setValue("1"); // Reviirille 1 on fake daossa yksi toinen pesä jonka koordinaatit ovat kaukana: 6670000:3390900 (leveys > 4km tästä pesästä)
			params.getColumn("vuosi.pesan_kunta").setValue(HELSIN);
			params.getColumn("pesavakio.pesanimi").setValue("Kaukana reviiristä");
			params.getColumn("pesavakio.koord_mittaus").setValue("G");
			params.getColumn("pesavakio.koord_tark").setValue("1");
			params.getColumn("pesamuuttuva.pesa_mit_pvm").setValue("1.1.2000");
			params.getColumn("pesamuuttuva.ymp_mit_pvm").setValue("1.1.2000");
			params.getColumn("pesatarkastus.tark_pvm").setValue("1.1.2000");
			params.getColumn("pesatarkastus.tark_vuosi").setValue("1999");
			params.getColumn("pesatarkastus.tark_pvm_tark").setValue("1");
			params.getColumn("pesatarkastus.tarkastaja1_id").setValue("100");
			params.getColumn("pesatarkastus.pesimistulos").setValue("A");
			params.getColumn("pesatarkastus.pesimist_tark").setValue("V");
			params.getColumn("pesatarkastus.nahdyt_merkit").setValue("A");
			params.getColumn("pesatarkastus.tark_tapa").setValue("1");
			params.getColumn("pesavakio.yht_leveys").setValue("6674423");
			params.getColumn("pesavakio.yht_pituus").setValue("3390958");
			params.getColumn("pesavakio.koord_tyyppi").setValue("Y");
			params.getColumn("pesavakio.helcom").setValue("K");
			params.getColumn("pesatarkastus.pesa_kunto").setValue("P");
			validator.validate();
			assertEquals("PESAT_YLI_4KM_TOISISTAAN", warning("vuosi.reviiri_id"));
		}
		
		public void test___reviirit_kaukana__eivat_ookkaan() throws Exception {
			params.getColumn("vuosi.reviirin_kunta").setValue(HELSIN);
			params.getColumn("vuosi.reviiri_id").setValue("2"); // Reviirille 2 on fake daossa yksi toinen pesä jonka koordinaatit ovat lähellä
			params.getColumn("vuosi.pesan_kunta").setValue(HELSIN);
			params.getColumn("pesavakio.pesanimi").setValue("Kaukana reviiristä");
			params.getColumn("pesavakio.koord_mittaus").setValue("G");
			params.getColumn("pesavakio.koord_tark").setValue("1");
			params.getColumn("pesamuuttuva.pesa_mit_pvm").setValue("1.1.2000");
			params.getColumn("pesamuuttuva.ymp_mit_pvm").setValue("1.1.2000");
			params.getColumn("pesatarkastus.tark_pvm").setValue("1.1.2000");
			params.getColumn("pesatarkastus.tark_vuosi").setValue("1999");
			params.getColumn("pesatarkastus.tark_pvm_tark").setValue("1");
			params.getColumn("pesatarkastus.tarkastaja1_id").setValue("100");
			params.getColumn("pesatarkastus.pesimistulos").setValue("A");
			params.getColumn("pesatarkastus.pesimist_tark").setValue("V");
			params.getColumn("pesatarkastus.nahdyt_merkit").setValue("A");
			params.getColumn("pesatarkastus.tark_tapa").setValue("1");
			params.getColumn("pesavakio.yht_leveys").setValue("6674423");
			params.getColumn("pesavakio.yht_pituus").setValue("3390958");
			params.getColumn("pesavakio.koord_tyyppi").setValue("Y");
			validator.validate();
			assertEquals(null, warning("vuosi.reviiri_id"));
		}
		
	}
	
}
