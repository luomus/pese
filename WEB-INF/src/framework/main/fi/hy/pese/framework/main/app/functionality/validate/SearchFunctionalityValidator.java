package fi.hy.pese.framework.main.app.functionality.validate;

import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._Data;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.main.general.data._Row;
import fi.hy.pese.framework.main.general.data._Table;

public class SearchFunctionalityValidator<T extends _Row> extends Validator<T> {

	public SearchFunctionalityValidator(_Request<T> request) {
		super(request);
	}

	@Override
	public void validate() throws Exception {
		checkAsSearchparameters(data.searchparameters());

		if (data.contains(Const.COORDINATE_SEARCH_RADIUS)) {
			checkCoordinateRadiusParameters(data);
		}

		if (printReportAction(data)) {
			if (!data.contains(Const.SELECTED_REPORTS)) {
				error(Const.SELECTED_REPORTS, _Validator.AT_LEAST_ONE_MUST_BE_SELECTED);
			}
		}

		if (data.contains("yyyy")) {
			checkValidYear("Vuosi", data.get("yyyy"));
		}

		if (data.contains("EkaTarkastusVuosi")) {
			String value = data.get("EkaTarkastusVuosi");
			if (checkAsNumericSearchRange("EkaTarkastusVuosi", value, new IntegerSearchParameterRangeValidator())) {
				if (value.startsWith("-")) {
					error("EkaTarkastusVuosi", _Validator.TOO_SMALL);
				} else if (value.contains("-")) {
					for (String vuosi : value.split("-")) {
						checkValidYear("EkaTarkastusVuosi", vuosi);
					}
				}
			}
		}
	}

	private boolean printReportAction(_Data<T> data) {
		return data.action().equals(Const.PRINT_REPORT);
	}

	private void checkCoordinateRadiusParameters(_Data<T> data) {
		if (data.searchparameters() instanceof _PesaDomainModelRow) {
			_PesaDomainModelRow searchParams = (_PesaDomainModelRow) data.searchparameters();
			checkInteger(Const.COORDINATE_SEARCH_RADIUS, data.get(Const.COORDINATE_SEARCH_RADIUS));

			_Table pesa = searchParams.getPesa();
			_Column yht_leveys = pesa.get("yht_leveys");
			_Column yht_pituus = pesa.get("yht_pituus");
			_Column eur_leveys = pesa.get("eur_leveys");
			_Column eur_pituus = pesa.get("eur_pituus");
			_Column ast_leveys = pesa.get("ast_leveys");
			_Column ast_pituus = pesa.get("ast_pituus");

			if (yht_leveys.hasValue() && yht_pituus.hasValue()) {
				if (eur_leveys.hasValue() || eur_pituus.hasValue() || ast_leveys.hasValue() || ast_pituus.hasValue()) {
					error(Const.COORDINATE_FIELDS, _Validator.COORDINATES_MUST_BE_GIVEN);
					return;
				}
				checkCoordinateIsInteger(yht_leveys);
				checkCoordinateIsInteger(yht_pituus);
			} else if (eur_leveys.hasValue() && eur_pituus.hasValue()) {
				if (yht_leveys.hasValue() || yht_pituus.hasValue() || ast_leveys.hasValue() || ast_pituus.hasValue()) {
					error(Const.COORDINATE_FIELDS, _Validator.COORDINATES_MUST_BE_GIVEN);
					return;
				}
				checkCoordinateIsInteger(eur_leveys);
				checkCoordinateIsInteger(eur_pituus);
			} else if (ast_leveys.hasValue() && ast_pituus.hasValue()) {
				if (yht_leveys.hasValue() || yht_pituus.hasValue() || eur_leveys.hasValue() || eur_pituus.hasValue()) {
					error(Const.COORDINATE_FIELDS, _Validator.COORDINATES_MUST_BE_GIVEN);
					return;
				}
				checkCoordinateIsInteger(ast_leveys);
				checkCoordinateIsInteger(ast_pituus);
				if (ast_leveys.getValue().length() != 6) {
					error(ast_leveys, _Validator.INVALID_VALUE);
				}
				if (ast_pituus.getValue().length() != 6) {
					error(ast_pituus, _Validator.INVALID_VALUE);
				}
			} else {
				error(Const.COORDINATE_FIELDS, _Validator.COORDINATES_MUST_BE_GIVEN);
			}
		} else {
			throw new UnsupportedOperationException("Not implemented for " + data.searchparameters().getClass().getName());
		}
	}

	private void checkCoordinateIsInteger(_Column c) {
		if (!isInteger(c.getValue())) {
			error(c, _Validator.COORDINATE_MUST_BE_INTEGER_IF_SEARCH_RADIUS_GIVEN);
		}
	}

}
