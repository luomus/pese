<!-- lahiympariston pesat --> 
<table class="infoContainer">
	<caption> Lähiympäristön&nbsp;pesät </caption>

    <tbody>

<#if ( errors?size > 0) >
<#-- 
      <tr>
        <td>
          	<#assign errorfields = errors?keys>
          	<#list errorfields as errorfield> <span class="error"> ${texts[errorfield]!errorfield}: ${texts[errors[errorfield]]!errors[errorfield]} </span> <br /> </#list>
        </td>
      </tr>
-->      
<#else>
	  <tr>
	    <th>Reviiri</th>
	    <th>Pesä</th>
        <th>Yht.koord.</th>
        <th></th>
	  </tr>
	  <#list results as r>
	    <#if data.nestid == r.pesa.pesaid>
          <tr class="highlightedRow">
        <#else>
          <tr>
        </#if>
        
          <td>${r.reviiri.reviirinimi} (${r.reviiri.reviiriid})</td>
          <td>
            <#if data.nestid != r.pesa.pesaid>
              <a href="${system.baseURL}?page=tarkastus&amp;action=fill_for_update&amp;updateparameters.tarkastus.tarkastusid=${r.tarkastus.tarkastusid}">
            </#if>
            <#if r.pesa.pesanimi != ""> 
              ${r.pesa.pesanimi} (${r.pesa.pesaid})
            <#else>
              ${r.pesa.pesaid}
            </#if>
            <#if data.nestid != r.pesa.pesaid>
              </a>
            </#if>
          </td>
          
          <td>${r.pesa.yht_leveys} <br />  ${r.pesa.yht_pituus}</td>
          <td><a target="_karttapaikka" href="https://asiointi.maanmittauslaitos.fi/karttapaikka/?share=customMarker&n=${r.pesa.eur_leveys}&amp;e=${r.pesa.eur_pituus}&amp;title=${r.pesa.pesanimi.getUrlEncodedValue()}(${r.pesa.pesaid})">Kartalle</a></td>
        </tr>
      </#list>
</#if>
	</tbody>
</table>
