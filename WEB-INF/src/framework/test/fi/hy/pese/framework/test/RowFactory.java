package fi.hy.pese.framework.test;

import fi.hy.pese.framework.main.general.data.PesaDomainModelDatabaseStructure;
import fi.hy.pese.framework.main.general.data.Row;
import fi.hy.pese.framework.main.general.data._Row;
import fi.hy.pese.framework.main.general.data._RowFactory;

public class RowFactory implements _RowFactory<_Row> {

	private final PesaDomainModelDatabaseStructure rowStructure;

	public RowFactory() {
		this.rowStructure = new PesaDomainModelDatabaseStructure();
	}

	public RowFactory(PesaDomainModelDatabaseStructure rowStructure) {
		this.rowStructure = rowStructure;
	}

	@Override
	public _Row newRow() {
		return new Row(rowStructure);
	}

	@Override
	public _Row newRow(String prefix) {
		return new Row(rowStructure, prefix);
	}

	@Override
	public String joins() {
		return null;
	}

	@Override
	public String orderby() {
		return null;
	}

}
