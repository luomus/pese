package fi.hy.pese.rengastus.main.app;

import java.sql.SQLException;

import fi.hy.pese.framework.main.db.oraclesql.Queries;
import fi.hy.pese.framework.main.general.data.DatabaseTableStructure;
import fi.luomus.commons.db.connectivity.TransactionConnection;
import fi.luomus.commons.tipuapi.TipuAPIClient;

public class RengastusDatabaseStructureCreator {

	public static RengastusDatabaseStructure loadFromDatabase(TransactionConnection con) throws SQLException {

		RengastusDatabaseStructure structure = new RengastusDatabaseStructure();

		// Taulut
		DatabaseTableStructure kontrolli = Queries.returnTableStructure("kontrolli", con);
		structure.setKontrolli(kontrolli);

		kontrolli.get("laji").setAssosiatedSelection(TipuAPIClient.SPECIES);
		kontrolli.get("rengastaja").setAssosiatedSelection(TipuAPIClient.RINGERS);
		kontrolli.get("maallikko").setAssosiatedSelection(TipuAPIClient.LAYMEN);
		kontrolli.get("kunta").setAssosiatedSelection(TipuAPIClient.MUNICIPALITIES);
		kontrolli.get("maa").setAssosiatedSelection(TipuAPIClient.COUNTRIES);
		kontrolli.get("euringprovinssi").setAssosiatedSelection(TipuAPIClient.EURING_PROVINCES);
		kontrolli.get("pyyntitapa").setAssosiatedSelection("codes_56");
		kontrolli.get("loytotapa").setAssosiatedSelection("codes_74");

		kontrolli.get("nimirengas").setToUppercaseColumn();
		kontrolli.get("jalkarengas").setToUppercaseColumn();
		kontrolli.get("vaihdetturengas").setToUppercaseColumn();

		kontrolli.customRowid("id");

		return structure;
	}

}
