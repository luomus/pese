package fi.hy.pese.merikotka.test;

import java.util.LinkedList;
import java.util.List;

import fi.hy.pese.framework.main.app.functionality.NestSorter;
import fi.hy.pese.framework.main.general.data.PesaDomainModelDatabaseStructure;
import fi.hy.pese.framework.main.general.data.PesaDomainModelRow;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.merikotka.main.app.MerikotkaDatabaseStructureCreator;
import fi.hy.pese.merikotka.main.functionality.MerikotkaSearchFunctionality.MerikotkaComparator;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class MerikotkaNestSorterComparatorTest {
	
	public static Test suite() {
		return new TestSuite(MerikotkaNestSorterComparatorTest.class.getDeclaredClasses());
	}
	
	public static class WhenSortingResultsUsingTheComparator extends TestCase {
		
		private final PesaDomainModelDatabaseStructure	structure	= MerikotkaDatabaseStructureCreator.hardCoded();
		private List<_PesaDomainModelRow>				results;
		private int						i;
		private NestSorter	sorter;
		
		@Override
		protected void setUp() throws Exception {
			results = new LinkedList<>();
			i = 0;
			sorter = new NestSorter(new MerikotkaComparator());
		}
		
		private _PesaDomainModelRow row(String... values) {
			_PesaDomainModelRow row = new PesaDomainModelRow(structure);
			int i = 0;
			for (String value : values) {
				switch (i++) {
					case 0:
						row.getVuosi().get("reviirin_kunta").setValue(value);
						break;
					case 1:
						row.getReviiri().get("reviirinimi").setValue(value);
						break;
					case 2:
						row.getReviiri().get("reviiri_id").setValue(value);
						break;
					case 3:
						row.getPesa().get("pesanimi").setValue(value);
						break;
					case 4:
						row.getPesa().get("pesa_id").setValue(value);
						break;
					case 5:
						row.getTarkastus().get("tark_vuosi").setValue(value);
						break;
				}
			}
			if (row.getPesa().getUniqueNumericIdColumn().hasValue()) {
				row.getPesa().setRowidValue(row.getPesa().getUniqueNumericIdColumn().getValue());
			} else {
				row.getPesa().setRowidValue(row.toString());
			}
			return row;
		}
		
		public void test_it_sorts_by__kunta() {
			results.add(row("HEL"));
			results.add(row("HEL"));
			results.add(row("VAN"));
			results.add(row("AAA"));
			results.add(row("HEL"));
			results = sorter.sort(results);
			assertEquals(res("AAA"), nextRow());
			assertEquals(res("HEL"), nextRow());
			assertEquals(res("HEL"), nextRow());
			assertEquals(res("HEL"), nextRow());
			assertEquals(res("VAN"), nextRow());
		}
		
		private String res(String... strings) {
			String value = "";
			for (String s : strings) {
				value += s;
			}
			return value;
		}
		
		private String res(_PesaDomainModelRow row) {
			String value = "";
			String kunta = row.getVuosi().get("reviirin_kunta").getValue();
			value += kunta;
			value += row.getReviiri().get("reviirinimi");
			value += row.getReviiri().get("reviiri_id");
			value += row.getPesa().get("pesanimi");
			value += row.getPesa().get("pesa_id");
			value += row.getTarkastus().get("tark_vuosi");
			return value;
		}
		
		public void test_it_sorts_by_suuralue__kunta___and__reviirinimi() {
			results.add(row("HEL", "Isosaari"));
			results.add(row("HEL", "Rev 4"));
			results.add(row("VAN", "Isosaari"));
			results.add(row("HEL", "Rev 2"));
			results.add(row("VAN", "Rev 3"));
			results = sorter.sort(results);
			assertEquals(res("HEL", "Isosaari"), nextRow());
			assertEquals(res("HEL", "Rev 2"), nextRow());
			assertEquals(res("HEL", "Rev 4"), nextRow());
			assertEquals(res("VAN", "Isosaari"), nextRow());
			assertEquals(res("VAN", "Rev 3"), nextRow());
		}
		
		private String nextRow() {
			return res(results.get(i++));
		}
		
		public void test_it_sorts_by_suuralue__kunta__reviirinimi__and__reviiir_id() {
			results.add(row("HEL", "Isosaari", "2"));
			results.add(row("HEL", "Isosaari", "1"));
			results.add(row("HEL", "Isosaari", "1"));
			results = sorter.sort(results);
			assertEquals(res("HEL", "Isosaari", "1"), nextRow());
			assertEquals(res("HEL", "Isosaari", "1"), nextRow());
			assertEquals(res("HEL", "Isosaari", "2"), nextRow());
		}
		
		public void test_it_sorts_by_suuralue__kunta__reviirinimi__and__pesanimi() {
			results.add(row("HEL", "Isosaari", "44", "Isosaari 3"));
			results.add(row("HEL", "Rev 4", "77", "Isosaari 2"));
			results.add(row("HEL", "Isosaari", "44", "Isosaari 1"));
			results = sorter.sort(results);
			assertEquals(res("HEL", "Isosaari", "44", "Isosaari 1"), nextRow());
			assertEquals(res("HEL", "Isosaari", "44", "Isosaari 3"), nextRow());
			assertEquals(res("HEL", "Rev 4", "77", "Isosaari 2"), nextRow());
		}
		
		public void test_it_sorts_by_suuralue__kunta__reviirinimi__and__pesa_id() {
			results.add(row("HEL", "Isosaari", "44", "", "6"));
			results.add(row("HEL", "Isosaari", "44", "", "2"));
			results.add(row("HEL", "Isosaari", "44", "", "3"));
			results = sorter.sort(results);
			assertEquals(res("HEL", "Isosaari", "44", "", "2"), nextRow());
			assertEquals(res("HEL", "Isosaari", "44", "", "3"), nextRow());
			assertEquals(res("HEL", "Isosaari", "44", "", "6"), nextRow());
		}
		
		public void test___keeps__tarkastus_of_same__pesa__togheter() {
			results.add(row("HEL", "Isosaari", "44", "Pesa 1", "1", "2008"));
			results.add(row("HEL", "Rev X", "77", "Pesa 1", "1", "2007"));
			results.add(row("HEL", "Rev X", "77", "Pesa 1", "1", "2006"));
			results.add(row("HEL", "Isosaari", "44", "Pesa 5", "5", "2008"));
			results.add(row("HEL", "Isosaari", "44", "Pesa 5", "5", "2007"));
			results = sorter.sort(results);
			assertEquals(res("HEL", "Isosaari", "44", "Pesa 1", "1", "2008"), nextRow());
			assertEquals(res("HEL", "Rev X", "77", "Pesa 1", "1", "2007"), nextRow());
			assertEquals(res("HEL", "Rev X", "77", "Pesa 1", "1", "2006"), nextRow());
			assertEquals(res("HEL", "Isosaari", "44", "Pesa 5", "5", "2008"), nextRow());
			assertEquals(res("HEL", "Isosaari", "44", "Pesa 5", "5", "2007"), nextRow());
		}
		
		public void test___complex_example() {
			sorter = new NestSorter(new MerikotkaComparator());
			
			results.add(row("HEL", "REV", "REVID", "PESA", "1", "2008"));
			results.add(row("HEL", "REV", "REVID", "PESA", "1", "2007"));
			results.add(row("HEL", "REV", "REVID", "PESA", "1", "2005"));
			results.add(row("BAA", "REV", "REVID", "PESA", "1", "2004"));
			results.add(row("BAA", "REV", "REVID", "PESA", "1", "2002"));
			
			results.add(row("AHV", "KOLMAS", "REVID", "PESA", "2", "1998"));
			results.add(row("AHV", "KOLMAS", "REVID", "PESA", "2", "1997"));
			
			results.add(row("HEL", "TOINEN", "REVID", "PESA", "3", "2008"));
			results.add(row("HEL", "TOINEN", "REVID", "PESA", "3", "2007"));
			
			results.add(row("UKK", "KOLMAS", "REVID", "PESA", "4", "2008"));
			results.add(row("AHV", "KOLMAS", "REVID", "PESA", "4", "2007"));
			
			results = sorter.sort(results);
			
			// Alla siis sorttautuu pesän uusimman tarkastuksen kunnan mukaan akkosjärjestykseen ja pitää saman pesän tarkastukset allekkain, vaikka reviirin kunta onkin muuttunut
			assertEquals(res("AHV", "KOLMAS", "REVID", "PESA", "2", "1998"), nextRow());
			assertEquals(res("AHV", "KOLMAS", "REVID", "PESA", "2", "1997"), nextRow());
			
			assertEquals(res("HEL", "REV", "REVID", "PESA", "1", "2008"), nextRow());
			assertEquals(res("HEL", "REV", "REVID", "PESA", "1", "2007"), nextRow());
			assertEquals(res("HEL", "REV", "REVID", "PESA", "1", "2005"), nextRow());
			assertEquals(res("BAA", "REV", "REVID", "PESA", "1", "2004"), nextRow());
			assertEquals(res("BAA", "REV", "REVID", "PESA", "1", "2002"), nextRow());
			
			assertEquals(res("HEL", "TOINEN", "REVID", "PESA", "3", "2008"), nextRow());
			assertEquals(res("HEL", "TOINEN", "REVID", "PESA", "3", "2007"), nextRow());
			
			assertEquals(res("UKK", "KOLMAS", "REVID", "PESA", "4", "2008"), nextRow());
			assertEquals(res("AHV", "KOLMAS", "REVID", "PESA", "4", "2007"), nextRow());
			
		}
		
	}
	
}
