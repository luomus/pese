package fi.hy.pese.merikotka.main.functionality.validation;

import java.sql.SQLException;

import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.app.functionality.validate.Validator;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;

public class MerikotkaUusiPesaValidator extends Validator<_PesaDomainModelRow> {

	public MerikotkaUusiPesaValidator(_Request<_PesaDomainModelRow> request) {
		super(request);
	}

	@Override
	public void validate() throws SQLException {
		if (data.action().equals(Const.CHECK)) {

			checkValidYear(data.updateparameters().getColumn("pesatarkastus.tark_vuosi"));

			_Column reviiri = data.updateparameters().getColumn("vuosi.reviiri_id");
			checkNotNull(reviiri);
			checkInteger(reviiri);
			if (noErrors(reviiri)) {
				checkReferenceValueExists(reviiri, dao);
			}

		}
	}

}
