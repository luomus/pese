package fi.hy.pese.framework.main.app.functionality.validate;

import java.util.Map;

public interface _Validator {

	int	SPECIAL_YEAR_VALUE_FOR_UNKNOWN_YEAR	= 999;

	String  TOO_SHORT = "value_too_short";
	String	TOO_SMALL										= "value_too_small";
	String	TOO_LARGE										= "value_too_large";
	String	DOES_NOT_EXIST										= "does_not_exist";
	String	CAN_NOT_BE_NULL										= "can_not_be_null";
	String	TOO_LONG											= "too_long";
	String	MUST_BE_INTEGER										= "must_be_integer";
	String	TOO_MANY_DECIMALS									= "too_many_decimals";
	String	MUST_BE_DECIMAL										= "must_be_decimal";
	String	INVALID_VALUE										= "invalid_value";
	String	INVALID_DATE										= "invalid_date";
	String	NOT_A_CALENDAR_DATE									= "not_a_calendar_date";
	String	LOGIN_INCORRECT										= "login_incorrect";
	String	SYSTEM_ERROR										= "system_error";
	String	NO_LONGER_EXISTS									= "no_longer_exists";
	String	ALREADY_IN_USE										= "already_in_use";
	String	INVALID_RANGE										= "invalid_range";
	String	INVALID_DATE_RANGE									= "invalid_date_range";
	String	INVALID_VALUE_LIST									= "invalid_value_list";
	String	INVALID_REFERENCE_VALUE								= "invalid_reference_value";
	String	CAN_NOT_DELETE_IS_BEING_USED						= "can_not_delete_is_being_used";
	String	CAN_NOT_INSERT_ALREADY_EXISTS						= "can_not_insert_already_exists";
	String	CAN_NOT_BE_IN_THE_FUTURE							= "can_not_be_in_the_future";
	String	COORDINATES_NOT_INSIDE_REGION						= "coordinates_not_inside_region";
	String	MUST_BE_NULL										= "must_be_null";
	String	CAN_NOT_CONTAIN_SPECIAL_CHARS						= "can_not_contain_special_chars";
	String	CAN_NOT_CONTAIN_EQUALS_SIGN							= "can_not_contain_equals_sign";
	String	CAN_NOT_CONTAIN_SEPARATOR							= "can_not_contain_separator";
	String	CAN_NOT_START_WITH_NEGATION							= "can_not_start_with_negation";
	String	COORDINATES_MUST_BE_GIVEN							= "coordinates_must_be_given";
	String	COORDINATE_MUST_BE_INTEGER_IF_SEARCH_RADIUS_GIVEN	= "coordinate_must_be_integer_if_search_radius_given";
	String	AT_LEAST_ONE_MUST_BE_SELECTED						= "at_least_one_must_be_selected";
	String	ASSOSIATED_SELECTION_DOES_NOT_CONTAIN_THE_VALUE		= "assosiated_selection_does_not_define_the_value";
	String	ARE_YOU_SURE	 									= "are_you_sure";
	String	ARE_YOU_SURE_VALUE_IS_NULL							= "are_you_sure_value_is_null";
	String	UNDEFINED_FIELD										= "undefined_field";
	String	MUST_BE_NULL_OR_ZERO								= "must_be_null_or_zero";
	String	MUST_NOT_BE_NULL_OR_ZERO							= "must_not_be_null_or_zero";
	String	MUST_BE_POSITIVE_NUMBER								= "must_be_positive_number";
	String  MUST_NOT_BE_NEGATIVE_NUMBER							= "must_not_be_negative_number";
	String	TOO_MANY_SELECTED									= "too_many_selected";
	String	INVALID_EMAIL										= "invalid_email";
	String	COORDINATE_ERROR									= "coordinate_error";

	double	MIN_YEAR = 1850;
	double  MAX_YEAR = 2099;
	int  	YEAR_WARNING_THRESHOLD = 15;

	int		YHT_LEVEYS_MIN										= 6610000;
	int		YHT_PITUUS_MIN										= 3060000;

	int		YHT_LEVEYS_MAX										= 7790000;
	int		YHT_PITUUS_MAX										= 3740000;

	int		EUR_LEVEYS_MIN										= 6610000;
	int		EUR_PITUUS_MIN										= 60000;

	int		EUR_LEVEYS_MAX										= 7790000;
	int		EUR_PITUUS_MAX										= 740000;

	int		AST_LEVEYS_MIN										= 593000;
	int		AST_PITUUS_MIN										= 192200;

	int		AST_LEVEYS_MAX										= 700200;
	int		AST_PITUUS_MAX										= 331700;

	double	WGS84_LEVEYS_MIN									= 59.39;
	double	WGS84_PITUUS_MIN									= 19.27;

	double	WGS84_LEVEYS_MAX									= 70.04;
	double	WGS84_PITUUS_MAX									= 33.30;

	double	WGS84_AST_LEVEYS_MIN								= 592357;
	double	WGS84_AST_PITUUS_MIN								= 191631;

	double	WGS84_AST_LEVEYS_MAX								= 700226;
	double	WGS84_AST_PITUUS_MAX								= 331834;

	int		YHT_LEVEYS_RUUTU_MIN = 661;
	int		YHT_PITUUS_RUUTU_MIN = 306;
	int		YHT_LEVEYS_RUUTU_MAX = 779;
	int		YHT_PITUUS_RUUTU_MAX = 374;

	void validate() throws Exception;

	boolean hasErrors();

	boolean hasWarnings();

	Map<String, String> errors();

	Map<String, String> warnings();

	boolean noErrors();

	boolean noWarnings();
}
