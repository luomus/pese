
<#include "header.ftl">

<#if (data.kirjekyyhky!"") == "yes">
  <h1>${texts["tarkastus_title_kirjekyyhky_"+system.action]}</h1>
<#else>
  <h1>${texts["tarkastus_title_"+system.action]}</h1>
</#if>

<#if system.action != "insert_first">
<table>
  <tr>
  
    <#if results?has_content>
    	<td>
        	<form action="${system.baseURL}" method="post">
        	<input type="hidden" name="page"   value="${system.page}" />
        	<input type="hidden" name="action" value="fill_for_update" />
           	Aikaisemmat pesintävuodet:
           	<select name="updateparameters.tarkastus.id">
              <#list results as i> 
                <#if i.tarkastus.id == updateparameters.tarkastus.id>
		          <option value="${i.tarkastus.id}" selected="selected">${i.tarkastus.vuosi} - ${i.tarkastus.pesiva_laji} - ${i.tarkastus.pesimistulos}</option>
		        <#else>
		          <option value="${i.tarkastus.id}" >${i.tarkastus.vuosi} - ${i.tarkastus.pesiva_laji} - ${i.tarkastus.pesimistulos}</option>
  		        </#if>
		      </#list>
           </select>
           <input type="submit" value="Tarkastele" />
        	</form>
    	</td>
    </#if>
    
    <#if system.action == "update">
    <td>
        <form action="${system.baseURL}" method="post">
        <input type="hidden" name="page" value="${system.page}" />
        <input type="hidden" name="action" value="fill_for_insert" />
        <input type="hidden" name="updateparameters.pesa.id" value="${updateparameters.pesa.id}" />
        
        Uusi tarkastus vuodelle:
        <input type="text" size="4" name="tark_vuosi" value="${data.tark_vuosi!""}" <@checkerror errors "tark_vuosi" /> /> 
        <input type="submit" value="Etene" />

        </form>
    </td>
    </#if>
    
    <#if updateparameters.pesa.id != "" && !data.kirjekyyhky??>
    	<td> 
      		<a target="_karttapaikka" href="https://asiointi.maanmittauslaitos.fi/karttapaikka/?share=customMarker&n=${updateparameters.pesa.eur_leveys}&amp;e=${updateparameters.pesa.eur_pituus}&amp;title=${updateparameters.pesa.nimi.getUrlEncodedValue()}(${updateparameters.pesa.id})">Lisää pesä kartalle</a> 
    	</td>
    </#if>
    
  </tr>
</table>
</#if>  
    
  


<@statustexts system texts />
<@errorlist errors texts />
<@warninglist warnings texts />

<#if lists.ristiriidat??>
  <br />
  <b>Pesät joille on onnistunut tarkastus tälle vuodelle: &nbsp; &nbsp;</b> 
  <#list lists.ristiriidat as pesaid>
    <a style="color:red" href="${system.baseURL}?page=tarkastus&amp;action=fill_for_update&amp;updateparameters.pesa.id=${pesaid}&amp;updateparameters.tarkastus.vuosi=${updateparameters.tarkastus.vuosi}">&nbsp;${pesaid}&nbsp;</a>
     &nbsp;&nbsp;
  </#list>
</#if>

<div id="tarkastuslomake">

<#if lists.log??>
   <table style="width: 50%">
     <caption>Päivitysloki</caption>
     <tr>
       <th>Aika</th>
       <th>Käyttäjä</th>
       <th>Toiminto</th>
     </tr>
     <#list lists.log as e>
       <tr>
         <td>${e.time}</td>
         <td> <#if selections['ringers'].containsOption(e.user)>${selections['ringers'][e.user]}<#else>${e.user}</#if></td>
         <td>${e.action}</td>
       </tr>
     </#list>
   </table>
   <br />
 </#if>
 
 <#if (data.kirjekyyhky!"") == "yes">
 <form action="${system.baseURL}" method="post" onsubmit="return false;" name="palautalomake">
   <input type="hidden" name="page" value="kirjekyyhky_management" />
   <input type="hidden" name="action" value="return_for_corrections" />
   <input type="hidden" name="id" value="${data.id!""}" />
   <table style="width: 50%">
     <caption>Palauta lomake korjattavaksi</caption>
     <tr>
       <td style="vertical-align: middle;"> Viesti </td>
       <td> <textarea name="return_for_corrections_message" rows="2" cols="50"></textarea> </td>
       <td  style="vertical-align: middle;"> <button onclick="document.palautalomake.submit()">Palauta</button> </td>
     </tr>
   </table>
   </form>
 </#if>
 
<form action="${system.baseURL}" method="post" onsubmit="return false;" name="tarkastusForm">
<input type="hidden" name="page" value="${system.page}" />
<input type="hidden" name="action" value="${system.action}" />
<input type="hidden" name="kirjekyyhky" value="${data.kirjekyyhky!""}" />
<input type="hidden" name="id" value="${data.id!""}" />
<input type="hidden" name="bypassed_warnings" id="bypassed_warnings" value="${data.bypassed_warnings!""}" /> 
<@inputHidden "updateparameters" updateparameters.pesa.rowid />
<@inputHidden "updateparameters" updateparameters.pesa.id />
<@inputHidden "updateparameters" updateparameters.tarkastus.rowid />
<@inputHidden "updateparameters" updateparameters.tarkastus.id />
<@inputHidden "updateparameters" updateparameters.getColumn("kaynti.1.rowid") />
<@inputHidden "updateparameters" updateparameters.getColumn("kaynti.2.rowid") />
<@inputHidden "updateparameters" updateparameters.getColumn("kaynti.3.rowid") />
<@inputHidden "updateparameters" updateparameters.getColumn("kaynti.4.rowid") />
<@inputHidden "updateparameters" updateparameters.tarkastus.ref_vanhaan_dataan />
<@inputHidden "updateparameters" updateparameters.tarkastus.oli_nipussa /> 
 
 <table class="noborders sameAsBackground" >
   <tr>
     <td style="width: 800px;">
        <h2>
        <!-- Toiminnon otsikko -->
        <#if system.action == "insert">
          <img src="${system.commonURL}/insert.gif" alt="Lisää" /> Uusi tarkastus: &nbsp;&nbsp; pesä: ${updateparameters.pesa.id} &nbsp;&nbsp; pesintävuosi: ${updateparameters.tarkastus.vuosi}
        </#if>
        <#if system.action == "update">
          <img src="${system.commonURL}/update.gif" alt="Muokkaa" /> Tarkastuksen muokkaus: &nbsp;&nbsp; pesä: ${updateparameters.pesa.id} &nbsp;&nbsp; pesintävuosi: ${updateparameters.tarkastus.vuosi}
        </#if>
        <#if system.action == "insert_first">
          <img src="${system.commonURL}/insert.gif" alt="Uusi" /> Uuden pesän lisäys
        </#if>
        </h2>
    </td> 
    <td style="vertical-align: middle;">
      <#if (data.kirjekyyhky!"") == "yes">
        <button onclick="document.tarkastusForm.submit()">${texts["tarkastus_submit_kirjekyyhky_"+system.action]}</button>
      <#else>
        <button onclick="document.tarkastusForm.submit()">${texts["tarkastus_submit_"+system.action]}</button>
      </#if>
    </td>
  </tr>
</table>

  

<h3> Perustiedot pesästä </h3>

<div style="float: left;">

<table>
  <tr>
    <th> Pesän ID </th>
    <th> Seuraava tarkastaja </th>
    <th> Esipainettu lomake (sähköisesti tai paperilla)</th>
  </tr>
  <tr>
    <td style="text-align: center;"> ${updateparameters.pesa.id} </td>
    <td> 
    	<@inputSelectCombo "updateparameters" updateparameters.pesa.seur_tarkastaja true false errors />  <@compare updateparameters.pesa.seur_tarkastaja />
    	<div id="lomake_vastaanottajat">
    	<span style="margin-top: 5px; font-weight: bold; display: inline-block;">Muut vastaanottajat:</span>
		<div class="henkilot">
			<#list lists["lomake_vastaanottajat"] as vastaanottaja>
				<input name="lomake_vastaanottaja" value="${vastaanottaja}"/>
			</#list>
		</div>
		<button onclick="addVastaanottaja(); return false;"> + Lisää</button>
		</div>
	</td> 
    <td> 
    	<select name="updateparameters.pesa.lomake" size="2" >
    		<option value="K" <#if updateparameters.pesa.lomake == "" || updateparameters.pesa.lomake == "K"> selected="selected" </#if> >Kyllä</option>
    		<option value="E" <#if updateparameters.pesa.lomake == "E"> selected="selected" </#if> >Ei</option>
    	</select>
    	<@compare updateparameters.pesa.lomake />
    </td>
  </tr>
</table>


  <br />
  <table>
    <tr>
      <th>REF</th>
      <th>Nippu</th>
      <th>Edellinen laji</th>
    </tr>
    <tr>
      <td>${updateparameters.tarkastus.ref_vanhaan_dataan}</td>
      <td>${updateparameters.tarkastus.oli_nipussa}</td>
      <td><@inputText "updateparameters" updateparameters.tarkastus.kommentti_edellinen_laji true errors /></td>
    </tr>
  </table>
 
 <br />


<table>
  <tr>
    <th> Kunta </th>
    <th> Kylä </th>
    <th> Pesän nimi </th>
  </tr>
  <tr>
    <td> 
      	<@inputSelectCombo "updateparameters" updateparameters.pesa.kunta true false errors /> <@compare updateparameters.pesa.kunta />
<#--      	<#assign column = updateparameters.kunta.ku_ympkesk> 
      	&nbsp; ${selections[column.assosiatedSelection][column.value]} -->  
    </td>
    <td> <@inputText "updateparameters" updateparameters.pesa.kyla true errors /> <@compare updateparameters.pesa.kyla /> </td>
    <td> <@inputText "updateparameters" updateparameters.pesa.nimi true errors /> <@compare updateparameters.pesa.nimi /> </td>
  </tr>
</table>

<br />
 
<table>
<caption>Koordinaatit</caption>
<tr>
<td>
<table>
  <tr>
    <th> </th>
    <th> Leveys </th>
    <th> Pituus </th>
  </tr>
  <tr>
    <th> <@radio "updateparameters" updateparameters.pesa.koord_ilm_tyyppi "Y" errors /> Yhtenäiskoordinaatit </th>
    <td> <@inputText "updateparameters" updateparameters.pesa.yht_leveys true errors /> <@compare updateparameters.pesa.yht_leveys /> </td>
    <td> <@inputText "updateparameters" updateparameters.pesa.yht_pituus true errors /> <@compare updateparameters.pesa.yht_pituus /> </td>
  </tr>
  <tr>
    <th> <@radio "updateparameters" updateparameters.pesa.koord_ilm_tyyppi "E" errors /> EUREF-koordinaatit </th>
    <td> <@inputText "updateparameters" updateparameters.pesa.eur_leveys true errors /> <@compare updateparameters.pesa.eur_leveys /></td>
    <td> <@inputText "updateparameters" updateparameters.pesa.eur_pituus true errors /> <@compare updateparameters.pesa.eur_pituus /></td>
  </tr>
  <tr>
    <th> <@radio "updateparameters" updateparameters.pesa.koord_ilm_tyyppi "A" errors /> Astekoordinaatit </th>
    <td> <@inputText "updateparameters" updateparameters.pesa.ast_leveys true errors /> <@compare updateparameters.pesa.ast_leveys /></td>
    <td> <@inputText "updateparameters" updateparameters.pesa.ast_pituus true errors /> <@compare updateparameters.pesa.ast_pituus /></td>
  </tr>
</table>
</td>
<td>
<table>
  <tr>
    <th> Mittaustapa / tarkkuus </th>
  </tr>
  <tr>
    <td> <@selection selections "updateparameters" updateparameters.pesa.koord_ilm_mittaustapa 2 false false false errors /> <@compare updateparameters.pesa.koord_ilm_mittaustapa /></td>
  </tr>
</table>
</td>
</tr>
</table>
  

<br />

<table>
  <tr>
    <th> Rauhoitustaulu vuonna ${updateparameters.tarkastus.vuosi}  </th>
    <th> Tietoja saa käyttää suojelutarkoituksiin </th>
  </tr>
  <tr>
    <td> <@selection selections "updateparameters" updateparameters.tarkastus.rauhoitustaulu 2 false true false errors /> <@compare updateparameters.tarkastus.rauhoitustaulu /></td>
    <td> <@selection selections "updateparameters" updateparameters.pesa.suojelu_kaytto 2 false true false errors /> <@compare updateparameters.pesa.suojelu_kaytto /></td>
  </tr>
</table>





<h3> Pesäpaikan ja pesän kuvaus </h3>
    
<table>
  <tr>
    <th> Rakennusvuosi </th>
    <th> Löytövuosi </th>
    <th> Rakentanut laji </th>
  </tr>
  <tr>
    <td> 
        <table>
          <tr> 
            <td>  <@inputText "updateparameters" updateparameters.pesa.rak_vuosi true errors />  <@compare updateparameters.pesa.rak_vuosi /> </td> 
            <td>  <@selection selections "updateparameters" updateparameters.pesa.rak_vuosi_tarkkuus 2 false true false errors /> <@compare updateparameters.pesa.rak_vuosi_tarkkuus /> </td>
          </tr>
        </table>
    </td>
    <td> <@inputText "updateparameters" updateparameters.pesa.loyt_vuosi true errors /> <@compare updateparameters.pesa.loyt_vuosi /></td>
    <td> <@inputSelectCombo "updateparameters" updateparameters.pesa.rak_laji true false errors /> <@compare updateparameters.pesa.rak_laji /> </td>
  </tr>
</table>


<table>
  <caption>  </caption>
  <tr>
    <td>
    		<table>
    		  <tr> <th> Maastotyyppi </th> </tr>
    		  <tr> 
    		    <td>
    		       <#assign maastoSelections = selections[updateparameters.tarkastus.maasto_1.assosiatedSelection].options> 
    		       <select name="maasto" <@checkerror errors "maasto" /> multiple="multiple" size="${maastoSelections?size+1}"> 
                     <option value=""></option>
                     <#list maastoSelections as option>
                       <#if updateparameters.tarkastus.maasto_1.value == option.value || updateparameters.tarkastus.maasto_2.value == option.value >
                         <option value="${option.value}" selected="selected"> ${option.value} - ${option.text} </option>
                       <#else>
                         <option value="${option.value}"> ${option.value} - ${option.text} </option>
                       </#if>  
                     </#list>
                  </select>
                  
    		    </td>
    		  </tr>
    		  <tr> <td> <@compare updateparameters.tarkastus.maasto_1 /> <@compare updateparameters.tarkastus.maasto_2 /> </td> </tr>
    		  <tr> <td> (voi valita kaksi arvoa) </td> </tr>
    		</table>
    		   
    </td>
    <td>
    		<table>
    		  <tr> <th> Lähiympäristön puusto </th> </tr>
    		  <tr> <td> <@selection selections "updateparameters" updateparameters.tarkastus.puusto 2 false true true errors /> <@compare updateparameters.tarkastus.puusto /> </td> </tr>
    		  <tr> <th> Puuston käsittelyaste </th> </tr>
    		  <tr> <td> <@selection selections "updateparameters" updateparameters.tarkastus.puusto_kasittelyaste 2 false true true errors /> <@compare updateparameters.tarkastus.puusto_kasittelyaste /></td> </tr>
    		</table>
    </td>
    <td>
    		<table>
    		  <tr> <th colspan="3"> Pesän tyyppi/alusta </th> </tr>
    		  <tr> <td colspan="3"> <@selection selections "updateparameters" updateparameters.tarkastus.pesa_sijainti 2 false true true errors /> <@compare updateparameters.tarkastus.pesa_sijainti /> </td> </tr>
    		</table>
    </td>
    <td>
    		<table>
    		  <tr> <th colspan="3"> Pesäpuun laji </th> </tr>
    		  <tr> <td colspan="3"> <@selection selections "updateparameters" updateparameters.pesa.puu_laji 2 false true true errors /> <@compare updateparameters.pesa.puu_laji /> </td> </tr>
    		  <tr> <th colspan="3"> Pesäpuun elävyys </th> </tr>
    		  <tr> <td colspan="3"> <@selection selections "updateparameters" updateparameters.tarkastus.puu_elavyys 2 false true true errors /> <@compare updateparameters.tarkastus.puu_elavyys /> </td> </tr>
    		</table>
    </td>
  </tr>    
</table>

<table>
  <tr>
    <th> Pesän etäisyys maasta </th>
    <th> Pesän etäisyys rungosta </th>
    <th> Puun korkeus </th>
  </tr>
  <tr>
    <td> <@inputText "updateparameters" updateparameters.tarkastus.pesa_korkeus true errors /> m <@selection selections "updateparameters" updateparameters.tarkastus.pesa_korkeus_tarkkuus 2 false true false errors /> <@compare updateparameters.tarkastus.pesa_korkeus /> <@compare updateparameters.tarkastus.pesa_korkeus_tarkkuus /> </td>
    <td> <@inputText "updateparameters" updateparameters.tarkastus.pesa_etaisyys_runko true errors /> cm <@selection selections "updateparameters" updateparameters.tarkastus.pesa_etaisyys_runko_tark 2 false true false errors /> <@compare updateparameters.tarkastus.pesa_etaisyys_runko /> <@compare updateparameters.tarkastus.pesa_etaisyys_runko_tark /> </td>
    <td> <@inputText "updateparameters" updateparameters.tarkastus.puu_korkeus true errors /> m <@selection selections "updateparameters" updateparameters.tarkastus.puu_korkeus_tarkkuus 2 false true false errors /> <@compare updateparameters.tarkastus.puu_korkeus /> <@compare updateparameters.tarkastus.puu_korkeus_tarkkuus /> </td>
  </tr>
</table>
    
<br />

<table>
  <tr>
    <th> Kommentit pesästä </th>
  </tr>
  <tr>
    <td> <@textarea "updateparameters" updateparameters.pesa.kommentti 3 100 errors /> <@compare updateparameters.pesa.kommentti />  </td>
  </tr>
</table>


</div>

<div style="white-space: nowrap;">
	<#if results?has_content>
    	<table class="infoContainer">
      	  <caption> Pesän historia </caption>
          <tr>
            <th>Vuosi</th>
            <th>Tarkastaja</th> 
            <th>Pesivä laji</th> 
            <th>Pesimistulos</th>
          </tr>
          <#list results as i>
            <tr>
              <td><a href="${system.baseURL}?page=tarkastus&amp;action=fill_for_update&amp;updateparameters.tarkastus.id=${i.tarkastus.id}"> ${i.tarkastus.vuosi} </a></td>
              <td>${selections[i.tarkastus.tarkastaja.assosiatedSelection][i.tarkastus.tarkastaja.value]}</td>  
              <td>${selections[i.tarkastus.pesiva_laji.assosiatedSelection][i.tarkastus.pesiva_laji.value]}</td> 
              <td>${i.tarkastus.pesimistulos}</td>
            </tr> 
          </#list>
    	</table>
	</#if>
	
	<br />
	
	<div style="display: inline-block;">
		<div id="lahiymparistonPesat"> Lataa... </div>
		
		<table class="infoContainer">
			<tr>
	    		<td colspan="4">
            		Säde:
            		<input type="text" name="CoordinateSearchRadius" value="${data.CoordinateSearchRadius!"500" }" size="7" tabindex="6666" /> m 
            		<button onclick="getLahiymparistonPesat();" tabindex="6667"> Hae </button> 
	         	</td>
	    	</tr>
		</table>
	</div>
	
</div>

<div  style="clear: both;"> </div>



<h3> Tarkastuskäyntien tiedot </h3>


  <table>
    <tr>
        <th> Pesinnän vuosi </th>
        <th> Pesä tarkastettu vuonna ${updateparameters.tarkastus.vuosi}? </th>
    	<th> Tarkastaja vuonna ${updateparameters.tarkastus.vuosi} </th>
    </tr>
    <tr>    	
        <td> <@inputText "updateparameters" updateparameters.tarkastus.vuosi true errors /> </td>
    	<td>
  			<select name="updateparameters.tarkastus.tarkastettu" <@checkerror errors "tarkastus.tarkastettu" /> size="2">
  			  <option value="K" <#if !updateparameters.tarkastus.tarkastettu.hasValue() || updateparameters.tarkastus.tarkastettu == "K"> selected="selected" </#if> > Kyllä </option>
  			  <option value="E" <#if updateparameters.tarkastus.tarkastettu == "E"> selected="selected" </#if> > Ei </option> 
  			</select> 
    	</td>
    	<td> <@inputSelectCombo "updateparameters" updateparameters.tarkastus.tarkastaja true false errors /> </td>
    </tr>
  </table>
  <br />
  <table>
    <tr>
       <th colspan="2"> Pesivä laji </th>
       <th> Tiedossa olevat vaihtopesät vuonna ${updateparameters.tarkastus.vuosi} </th>
     </tr>
      <tr>
        <td colspan="2"> <@inputSelectCombo "updateparameters" updateparameters.tarkastus.pesiva_laji true false errors /> </td>
        <td> <@textarea "updateparameters" updateparameters.tarkastus.kommentti_vaihtop 2 50 errors /> </td>
    </tr>
  </table>
  <br />
  <table>    
     <tr>
        <th> Pesinnän järjestysnumero </th>
        <th> Pesintöjä yhdistävä tunniste </th>
        <th> Uusintapesinnän perustelut </th>
     </tr>
     <tr>
        <td> <@inputText "updateparameters" updateparameters.tarkastus.pesinnan_jarjestysnumero true errors /> </td>
        <td> <@inputText "updateparameters" updateparameters.tarkastus.pesinnan_yhdistava_tunniste true errors /> </td>
        <td> <@textarea "updateparameters" updateparameters.tarkastus.kommentti_jarjestys 2 50 errors /> </td>
    </tr>
  </table>  
  
  <br />
  
  <table id="kayntitaulu">
    <tr>
        <th rowspan="2">#</th>
        <th rowspan="2"> Päivämäärä </th>
        <th rowspan="2"> Pesän kunto </th>
        <th rowspan="2"> Emojen <br /> lkm. </th>
        <th rowspan="2"> Munien <br /> lkm. </th>
        <th rowspan="2"> &nbsp; </th>
        <th colspan="2"> Pesäpoikasten lkm. </th>
		<th rowspan="2"> Lentop. <br /> lkm. </th>
        <th rowspan="2"> Vanhimman poikasen <ul><li>siiven pituus</li> <li>paino</li> </ul></th>
        <th rowspan="2"> Siiven <br />mittaustapa </th>
        
    </tr>
    <tr>
        <th> Elävät </th>
        <th> Kuolleet </th>
    </tr>
    
    <#list 1..4 as i>
        <#if i = 2 || i = 4>
          <tr class="odd">
         <#else>
           <tr>
         </#if>
              <td> ${i} </td>
              
              <td> 
                 <@date "updateparameters" updateparameters.getColumn("kaynti."+i+".pvm") errors /> <br />
                 <div style="margin-top: 2px;"> <@selection selections "updateparameters" updateparameters.getColumn("kaynti."+i+".pvm_tarkkuus") 1 false true true errors /> </div>  
              </td>
              
              <td class="pesakunto"> <@selection selections "updateparameters" updateparameters.getColumn("kaynti."+i+".pesa_kunto") 1 false true true errors /> </td>
              
              <td class="lkm_selection"> 
              	 <@inputText "updateparameters" updateparameters.getColumn("kaynti."+i+".aikuisia_lkm") true errors />
                 <@selection selections "updateparameters" updateparameters.getColumn("kaynti."+i+".aikuisia_lkm_tarkkuus") 1 false true true errors /> 
              </td>
              
              <td class="lkm_selection"> 
              	 <@inputText "updateparameters" updateparameters.getColumn("kaynti."+i+".munia_lkm") true errors />
                 <@selection selections "updateparameters" updateparameters.getColumn("kaynti."+i+".munia_lkm_tarkkuus") 1 false true true errors /> 
              </td>
              
              <td>  
                <table>
                  <tr>
                    <td> Hautova emo: </td>
                    <td><@selection selections "updateparameters" updateparameters.getColumn("kaynti."+i+".hautova_emo") 1 false true false errors /> </td>
                  </tr>
                  <tr>
                    <td> Siruja: </td>
                    <td> <@selection selections "updateparameters" updateparameters.getColumn("kaynti."+i+".siruja") 1 false true false errors /> </td>
                  </tr>
                </table>
              </td>
              
              <td class="lkm_selection"> 
              	 <@inputText "updateparameters" updateparameters.getColumn("kaynti."+i+".pesapoikasia_lkm") true errors />
                 <@selection selections "updateparameters" updateparameters.getColumn("kaynti."+i+".pesapoikasia_lkm_tarkkuus") 1 false true true errors /> 
              </td>
              
              <td class="lkm_selection"> 
              	 <@inputText "updateparameters" updateparameters.getColumn("kaynti."+i+".kuolleita_lkm") true errors />
                 <@selection selections "updateparameters" updateparameters.getColumn("kaynti."+i+".kuolleita_lkm_tarkkuus") 1 false true true errors /> 
              </td>
              
              <td class="lkm_selection"> 
              	 <@inputText "updateparameters" updateparameters.getColumn("kaynti."+i+".lentopoikasia_lkm") true errors />
                 <@selection selections "updateparameters" updateparameters.getColumn("kaynti."+i+".lentopoikasia_lkm_tarkkuus") 1 false true true errors /> 
              </td>
              
              <td class="max_poikanen"> 
                <@inputText "updateparameters" updateparameters.getColumn("kaynti."+i+".max_siipi") false errors />mm  <br />
                <@inputText "updateparameters" updateparameters.getColumn("kaynti."+i+".max_paino") false errors />g  
              </td>
              
              <td> <@selection selections "updateparameters" updateparameters.getColumn("kaynti."+i+".max_siipi_mittaustapa") 1 false true false errors /> </td>
        </tr>
        
              
              
              
        
    </#list>
  </table>
  
  <br /> 
  
   <table>
    <tr>
      <th> Kommentit käynneistä </th>
      <th> Poikasten tai aikuisten renkaat (tms.) </th>
    </tr>
    <tr>
      <td> <@textarea "updateparameters" updateparameters.tarkastus.kommentti_kaynnit 3 50 errors /> </td>
      <td> <@textarea "updateparameters" updateparameters.tarkastus.kommentti_renkaat 3 50 errors /> </td>
    </tr>
  </table>
  
  <br />
  
  <table>
    <tr>
      <th> Kuoriutumattomien munien lkm. <br /> <span class="ohje">(ilmoitetaan vain mikäli tiedetään tarkasti)</span> </th>
      <th> Aikuisia max. lkm. <br /> <span class="ohje">(ei tallenneta uusista lomakkeista)</span> </th>
    </tr>
    <tr>
      <td class="max_poikanen"> <@inputText "updateparameters" updateparameters.tarkastus.kuoriutumattomia_lkm false errors /> </td>
      <td class="max_poikanen"> <@inputText "updateparameters" updateparameters.tarkastus.aikuisia_max_lkm false errors /> </td>
    </tr>
  </table>
  
  <br />
  
  
<h3> Tulkinnat </h3>
  
  <table id="pesimistulostaulu">
    <tr>
      <th> Pesimistulos </th>
      <th> Epäonnistuminen </th>
      <th> Muu laji </th>
    </tr>
    <tr>
      <td> <@selection selections "updateparameters" updateparameters.tarkastus.pesimistulos 1 false true true errors /> </td> 
      <td> <@selection selections "updateparameters" updateparameters.tarkastus.epaonnistuminen 1 false true true errors /> </td>
      <td rowspan="2"> <@inputSelectCombo "updateparameters" updateparameters.tarkastus.muu_laji true false errors /> </td>
    </tr>
    <tr>
      <td> <@selection selections "updateparameters" updateparameters.tarkastus.pesimistulos_tarkkuus 1 false true true errors /> </td> 
      <td> <@selection selections "updateparameters" updateparameters.tarkastus.epaonnistuminen_tarkkuus 1 false true true errors /> </td>
    </tr>
    <tr>
      <th colspan="3"> Kommentti tarkastuksesta (ml. epäonnistumisen syy)</th>
    </tr>
    <tr>
      <td colspan="3"> <@textarea "updateparameters" updateparameters.tarkastus.kommentti_pesimistulos 3 100 errors /> </td>
    </tr>
  </table>
  

<h3> Saalistus ja näytteet </h3>
 
  <table>
    <tr>
      <th> Kommentteja saaliseläimistä </th>
      <th> Kommentteja saalistusalueista </th>
    </tr>
    <tr>
      <td> <@textarea "updateparameters" updateparameters.tarkastus.kommentti_saalis 3 50 errors /> </td>
      <td> <@textarea "updateparameters" updateparameters.tarkastus.kommentti_saalistusalue 3 50 errors /> </td>
    </tr>
  </table>
  
  <br /> 
  
  <table>
    <tr>
      <th> Munia </th>
      <th> Kuolleita poikasia </th>
      <th> Sulkia </th>
      <th> Saalisnäytteitä </th>
      <th> Kuvia </th>
      <th> &nbsp; </th>
    </tr>
    <tr>    
      <td> <@selection selections "updateparameters" updateparameters.tarkastus.nayte_munia 2 false true false errors /> </td>
      <td> <@selection selections "updateparameters" updateparameters.tarkastus.nayte_kuolleita 2 false true false errors /> </td>
      <td> <@selection selections "updateparameters" updateparameters.tarkastus.nayte_sulkia 2 false true false errors /> </td>
      <td> <@selection selections "updateparameters" updateparameters.tarkastus.nayte_saalis 2 false true false errors /> </td>
      <td> <@selection selections "updateparameters" updateparameters.tarkastus.kuvia 2 false true false errors /> </td>
      <td> <a href="#" onclick="eiNaytteita(); return false;" id="eiNaytteitaButton">&raquo; Ei näytteitä</a> </td>
    </tr>
  </table>
	 
   <br />
	 
   
      <#if (data.kirjekyyhky!"") == "yes">
        <button onclick="document.tarkastusForm.submit()">${texts["tarkastus_submit_kirjekyyhky_"+system.action]}</button>
      <#else>
        <button onclick="document.tarkastusForm.submit()">${texts["tarkastus_submit_"+system.action]}</button>
      </#if>
   
	 
  
</form>

</div>

	<script type="text/javascript">
	
	  function eiNaytteita() {
	  		$('#updateparameters\\.tarkastus\\.nayte_munia').val('E');
	  		$('#updateparameters\\.tarkastus\\.nayte_kuolleita').val('E');
	  		$('#updateparameters\\.tarkastus\\.nayte_sulkia').val('E');
	  		$('#updateparameters\\.tarkastus\\.nayte_saalis').val('E');
	  		$('#updateparameters\\.tarkastus\\.kuvia').val('E');
	  }
	  
	  var form = document.forms['tarkastusForm'];
	  var nestID = form['updateparameters.pesa.id'];
	  var yhtLeveys = form['updateparameters.pesa.yht_leveys'];
	  var yhtPituus = form['updateparameters.pesa.yht_pituus'];
	  var eurLeveys = form['updateparameters.pesa.eur_leveys'];
	  var eurPituus = form['updateparameters.pesa.eur_pituus'];
	  var radius = form['CoordinateSearchRadius'];
      getLahiymparistonPesat();
      
      
      var e = $('#updateparameters\\.pesa\\.seur_tarkastaja\\.selector'); 
	  e.change(function() {
	    var e = $('#updateparameters\\.pesa\\.seur_tarkastaja\\.selector');
	    var value = e.val().trim(); 
	    if (value == '') return;
	    var e = $('#updateparameters\\.tarkastus\\.tarkastaja\\.input');
	    if (e.val().trim() != '') return; 
        $('#updateparameters\\.tarkastus\\.tarkastaja\\.selector').val( value );
        $('#updateparameters\\.tarkastus\\.tarkastaja\\.input').val( value );
      });
	  var e = $('#updateparameters\\.pesa\\.seur_tarkastaja\\.input'); 
	  e.blur(function() {
	    var e = $('#updateparameters\\.pesa\\.seur_tarkastaja\\.input');
	    var value = e.val().trim(); 
	    if (value == '') return;
	    var e = $('#updateparameters\\.tarkastus\\.tarkastaja\\.input');
	    if (e.val().trim() != '') return; 
        $('#updateparameters\\.tarkastus\\.tarkastaja\\.selector').val( value );
        $('#updateparameters\\.tarkastus\\.tarkastaja\\.input').val( value );
      });
      
      var e = $('#updateparameters\\.pesa\\.rak_laji\\.selector'); 
	  e.change(function() {
	    var e = $('#updateparameters\\.pesa\\.rak_laji\\.selector'); 
	    var target = $('#updateparameters\\.tarkastus\\.pesiva_laji\\.selector');
	    if (target.val().trim() != '') return; 
        target.val( e.val() ).change();
      });
	  var e = $('#updateparameters\\.pesa\\.rak_laji\\.input'); 
	  e.blur(function() {
	    var e = $('#updateparameters\\.pesa\\.rak_laji\\.selector'); 
	    var target = $('#updateparameters\\.tarkastus\\.pesiva_laji\\.selector');
	    if (target.val().trim() != '') return; 
        target.val( e.val() ).change();
      });
      
      <#assign list = ["aikuisia", "munia", "kuolleita", "pesapoikasia", "lentopoikasia"]>
      <#list 1..4 as i>
        var e = $('#updateparameters\\.kaynti\\.${i}\\.pvm\\.dd');
        e.blur(function() { 
          if (   $('#updateparameters\\.kaynti\\.${i}\\.pvm\\.dd').val().trim() == ''    ) return;
	      var source = $('#updateparameters\\.tarkastus\\.vuosi');  
	      $('#updateparameters\\.kaynti\\.${i}\\.pvm\\.yyyy').val( source.val() );
          $('#updateparameters\\.kaynti\\.${i}\\.pvm_tarkkuus').val('1');
        });
        
        <#list list as type>
          var e = $('#updateparameters\\.kaynti\\.${i}\\.${type}_lkm');
          e.blur(function() {
            if (    $('#updateparameters\\.kaynti\\.${i}\\.${type}_lkm').val().trim() == ''   ) return;
            var tark = $('#updateparameters\\.kaynti\\.${i}\\.${type}_lkm_tarkkuus');
            if (  tark.val().trim() != '')  return; 
            tark.val('T');
          });
        </#list>
      </#list>
      
      <#if system.action == "insert_first">
        var e = $('#updateparameters\\.pesa\\.koord_ilm_mittaustapa');
        if (e.val() == null) e.val('K');
      </#if>
      
      var lomakeVastaanottajat = [<#list selections["ringers"].options as option>"${option.text?replace("\"","")} (${option.value})"<#if option_has_next>,</#if></#list>];
	
	</script>
	
<#include "footer.ftl">

