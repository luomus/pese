package fi.hy.pese.framework.main.app;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.hy.pese.framework.main.general.data.ParameterMap;
import fi.hy.pese.framework.main.general.data._Row;
import fi.luomus.commons.http.HttpStatus;
import fi.luomus.commons.kirjekyyhky.XmlToFormDataUtil;
import fi.luomus.commons.reporting.ErrorReporter;
import fi.luomus.commons.reporting.ErrorReporterViaEmail;
import fi.luomus.commons.session.SessionHandler;
import fi.luomus.commons.utils.Utils;

/**
 * The abstract base servlet for PeSe Framework Application's Kirjekyyhky Validation service.
 * The applications need to implement the factory() -method. The factory contains all the
 * application specific configuration and data templates, as defined in the _Factory interface.
 * The applications may also override other features of this class, or define their very own Servlet from scratch.
 * @see _ApplicationSpecificationFactory
 */
public abstract class KirjekyyhkyValidatorServlet<T extends _Row> extends HttpServlet {
	
	private static final long	serialVersionUID	= -8160730628169405593L;
	
	protected abstract _ApplicationSpecificationFactory<T> createFactory() throws Exception;
	
	protected Control<T>								control;
	protected _ApplicationSpecificationFactory<T>	factory;
	protected final SessionHandler fakeSessionHandler = new FakeSessionHandler();
	
	/**
	 * Called when the servlet is initialized; either at server startup or when the servlet is called for the first time,
	 * depending on server configuration.
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		try {
			factory = createFactory();
			control = new Control<>(factory);
		} catch (Exception e) {
			e.printStackTrace();
			try {
				ErrorReporter reporter = new ErrorReporterViaEmail("localhost", "", "", "eopiirai@cs.helsinki.fi", "eopiirai@cs.helsinki.fi", this.getClass().getName() + " error report");
				reporter.report(e);
			} catch (Exception errorReportinException) {
				errorReportinException.printStackTrace();
			}
			throw new ServletException("CAUSE: " + e.toString() + " " + e.getStackTrace()[0]);
		}
		System.out.println(this.getClass() + " INITIALIZED USING FACTORY: " + factory.getClass().toString());
	}
	
	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		redirectTo404(res);
	}
	
	private void redirectTo404(HttpServletResponse res) {
		res.setStatus(404);
		res.setHeader("Connection", "close");
	}
	
	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		process(req, res);
	}
	
	public void process(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		req.setCharacterEncoding("UTF-8");
		res.setContentType("application/xml; charset=utf-8");
		PrintWriter out = res.getWriter();
		
		StringBuilder formDataXML = readRequestBody(req);
		//System.out.println(formDataXML);
		
		if (formDataXML.length() < 1) {
			redirectTo404(res);
			return;
		}
		
		Map<String, String> params;
		try {
			params = XmlToFormDataUtil.readFormData(formDataXML.toString(), "notUsedSystemId").getData();
			Map<String, String> paramsCopy = new HashMap<>();
			for (Map.Entry<String, String> e : params.entrySet()) {
				paramsCopy.put("updateparameters." + e.getKey(), e.getValue());
			}
			params.putAll(paramsCopy);
		} catch (Exception e) {
			e.printStackTrace();
			factory.reportException("Parsing parameters: " + formDataXML.toString(), e);
			throw new ServletException(e);
		}
		//Utils.debug(params);
		
		String requestURI = Utils.getRequestURIAndQueryString(req);
		
		control.process(new ParameterMap(params), fakeSessionHandler, out, new HttpStatus(res), requestURI);
	}
	
	private StringBuilder readRequestBody(HttpServletRequest req) throws UnsupportedEncodingException, IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(req.getInputStream(), "UTF-8"));
		StringBuilder formDataXML = new StringBuilder();
		String s;
		while ((s = reader.readLine()) != null) {
			formDataXML.append(s);
		}
		return formDataXML;
	}
	
	public static class FakeSessionHandler implements SessionHandler {
		
		@Override
		public void put(String key, String value) {}
		
		@Override
		public String get(String key) {
			return "";
		}
		
		@Override
		public void remove(String key) {}
		
		@Override
		public String language() {
			return null;
		}
		
		@Override
		public void setLanguage(String language) {}
		
		@Override
		public boolean isAuthenticatedFor(String system) {
			return true;
		}
		
		@Override
		public void authenticateFor(String system) {}
		
		@Override
		public void removeAuthentication(String system) {}
		
		@Override
		public String userId() {
			return this.getClass().getName();
		}
		
		@Override
		public void setUserId(String userid) {}
		
		@Override
		public String userName() {
			return this.getClass().getName();
		}
		
		@Override
		public void setUserName(String username) {}
		
		@Override
		public String userType() {
			return this.getClass().getName();
		}
		
		@Override
		public void setUserType(String usertype) {}

		@Override
		public void setFlash(String message) {
		}

		@Override
		public String getFlash() {
			return null;
		}

		@Override
		public Object getObject(String key) {
			return null;
		}

		@Override
		public void setObject(String key, Object value) {
			
		}

		@Override
		public boolean hasSession() {
			return false;
		}

		@Override
		public void setFlashError(String message) {
			// Auto-generated method stub
			
		}

		@Override
		public String getFlashError() {
			// Auto-generated method stub
			return null;
		}

		@Override
		public void setFlashSuccess(String message) {
			// Auto-generated method stub
			
		}

		@Override
		public String getFlashSuccess() {
			// Auto-generated method stub
			return null;
		}

		@Override
		public void setTimeout(int minutes) {
			// Auto-generated method stub
			
		}

		@Override
		public void invalidate() {
			// Auto-generated method stub
			
		}
		
	}
	
}
