package fi.hy.pese.framework.main.db;

import java.sql.SQLException;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.general.ReportWriter;
import fi.hy.pese.framework.main.general.Utils;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._IndexedTablegroup;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.main.general.data._Table;
import fi.luomus.commons.containers.KeyValuePair;
import fi.luomus.commons.containers.Selection;
import fi.luomus.commons.tipuapi.TipuAPIClient;

public class ResultsToReportHandler extends BaseResultsHandler {
	
	private final _Request<_PesaDomainModelRow>			request;
	private final _PesaDomainModelRow				row;
	
	private final static String		SEARCHRESULTS_REPORT_FILENAME_PREFIX	= "searchresults";
	
	public ResultsToReportHandler(_Request<_PesaDomainModelRow> request) {
		this.request = request;
		this.row = request.data().newRow();
	}
	
	@Override
	public void process(_ResultSet rs) throws SQLException {
		ReportWriter<_PesaDomainModelRow> writer = new ResultsToReportWriter(request, SEARCHRESULTS_REPORT_FILENAME_PREFIX, rs);
		writer.produce();
	}
	
	private class ResultsToReportWriter extends ReportWriter<_PesaDomainModelRow> {
		
		private final _ResultSet	rs;
		
		public ResultsToReportWriter(_Request<_PesaDomainModelRow> request, String filenamePrefix, _ResultSet rs) {
			super(request, filenamePrefix);
			this.rs = rs;
		}
		
		@Override
		protected boolean produceSpecific() throws Exception {
			Collection<String> selectedFields = selectedFields();
			if (selectedFields.isEmpty()) { return false; }
			
			headerLine(additionalParams);
			newLine();
			newLine();
			columnDescriptions(selectedFields);
			newLine();
			newLine();
			
			ResultWriter resultWriter = new ResultWriter(request.dao(), selectedFields, request.data().selections());
			resultWriter.process(rs);
			
			if (!resultWriter.wroteRows) return false;
			
			newLine();
			
			return true;
		}
		
		private class ResultWriter extends RowCombiningResultHandler {
			
			private String						prev_line		= "";
			private StringBuilder				current_line	= null;
			private final Collection<String>	selectedFields;
			public boolean						wroteRows		= false;
			private final Map<String, Selection> selections;
			
			public ResultWriter(_DAO<_PesaDomainModelRow> dao, Collection<String> selectedFields, Map<String, Selection> selections) {
				super(dao);
				this.selectedFields = selectedFields;
				this.selections = selections;
			}
			
			@Override
			protected boolean handle(_PesaDomainModelRow row) {
				current_line = new StringBuilder();
				for (_Table t : row) {
					if (row.getGroups().keySet().contains(t.getName())) continue;
					for (_Column c : t) {
						if (selectedFields.contains(c.getFullname())) {
							current_line.append(c.getValue());
							current_line.append("|");
							if (ringerColumn(c)) {
								current_line.append(c.getSelectionDesc(selections));
								current_line.append("|");
							}
						}
					}
				}
				for (_IndexedTablegroup group : row.getGroups().values()) {
					if (group.getStaticCount() < 1) continue;
					for (int i = 1; i <= group.getStaticCount(); i++) {
						for (_Column c : group.get(i)) {
							if (selectedFields.contains(c.getFullname())) {
								current_line.append(c.getValue());
								current_line.append("|");
							}
						}
					}
				}
				if (!current_line.toString().equals(prev_line)) {
					write(current_line.toString().replace("\n", " ").replace("\r", ""));
					newLine();
					prev_line = current_line.toString();
					wroteRows = true;
				}
				return true;
			}
			
		}
		
		private void columnDescriptions(Collection<String> selectedFields) {
			for (_Table t : row) {
				columnDescriptions(selectedFields, t);
			}
			for (_IndexedTablegroup group : row.getGroups().values()) {
				if (group.getStaticCount() < 1) continue;
				for (int i = 1; i <= group.getStaticCount(); i++) {
					columnDescriptions(selectedFields, group.get(i));
				}
			}
		}
		
		private void columnDescriptions(Collection<String> selectedFields, _Table t) {
			for (_Column c : t) {
				if (selectedFields.contains(c.getFullname())) {
					writeShortnameOrFullname(c);
					separator();
					if (ringerColumn(c)) {
						write("nimi");
						separator();
					}
				}
			}
		}
		
		private Collection<String> selectedFields() {
			List<Object> fieldList = request.data().getLists().get(Const.SELECTED_FIELDS);
			
			Collection<String> selectedFields = new HashSet<>();
			if (fieldList == null) return selectedFields;
			
			String kayntiTablename = row.getKaynti().getName();
			int kayntiCount = row.getKaynnit().getStaticCount();
			
			String poikanenTablename = row.getPoikanen().getName();
			int poikanenCount = row.getPoikaset().getStaticCount();
			
			String aikuinenTablename = row.getAikuinen().getName();
			int aikuinenCount = row.getAikuiset().getStaticCount();
			
			String munaTablename = row.getMuna().getName();
			int munaCount = row.getMunat().getStaticCount();
			
			for (Object o : fieldList) {
				String field = o.toString();
				String tablename = Utils.tableName(field);
				if (tablename.equals(kayntiTablename)) {
					String columName = Utils.columnName(field);
					for (int i = 1; i <= kayntiCount; i++) {
						selectedFields.add(tablename + "." + i + "." + columName);
					}
				} else if (tablename.equals(poikanenTablename)) {
					String columName = Utils.columnName(field);
					for (int i = 1; i <= poikanenCount; i++) {
						selectedFields.add(tablename + "." + i + "." + columName);
					}
				} else if (tablename.equals(aikuinenTablename)) {
					String columName = Utils.columnName(field);
					for (int i = 1; i <= aikuinenCount; i++) {
						selectedFields.add(tablename + "." + i + "." + columName);
					}
				} else if (tablename.equals(munaTablename)) {
					String columName = Utils.columnName(field);
					for (int i = 1; i <= munaCount; i++) {
						selectedFields.add(tablename + "." + i + "." + columName);
					}
				} else {
					selectedFields.add(field);
				}
			}
			return selectedFields;
		}
		
	}
	
	private List<KeyValuePair> additionalParams = null;
	
	public void setAdditionalSearchParams(List<KeyValuePair> additionalParams) {
		this.additionalParams = additionalParams;
	}
	
	private static boolean ringerColumn(_Column c) {
		return c.hasAssosiatedSelection() && c.getAssosiatedSelection().equals(TipuAPIClient.RINGERS);
	}
	
}
