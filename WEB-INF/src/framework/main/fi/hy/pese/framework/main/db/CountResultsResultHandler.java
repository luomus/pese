package fi.hy.pese.framework.main.db;

import java.sql.SQLException;

/**
 * This result handler can be used to check if the search produced any results
 */
public class CountResultsResultHandler implements _ResultsHandler {

	public int numberOfRows = 0;

	@Override
	public void process(_ResultSet rs) throws SQLException {
		while (rs.next()) {
			numberOfRows++;
		}
	}

}
