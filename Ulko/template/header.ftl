<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
       "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<#include "macro.ftl">

<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
	<title>${texts["title_"+system.page]}</title>
	<link rel="stylesheet" href="${system.commonURL}/lintuvaara_shared_layout.css" type="text/css" media="screen, print" charset="utf-8" />
	<link rel="stylesheet" href="${system.commonURL}/pese.css?${system.last_update_timestamp}" type="text/css" />
	<link rel="stylesheet" href="${system.baseURL}/ulko.css?${system.last_update_timestamp}" type="text/css" />
    <link rel="stylesheet" href="${system.commonURL}/chosen.css" type="text/css" media="screen, print" charset="utf-8" />
	<script type="text/javascript" src="${system.commonURL}/jquery-latest.js?${system.last_update_timestamp}"></script>
	<script type="text/javascript" src="${system.commonURL}/jquery.tablesorter.min.js"></script> 
	<script type="text/javascript" src="${system.commonURL}/jquery.metadata.js"></script>
 	<script type="text/javascript" src="${system.commonURL}/chosen.jquery.js"></script>
  	<script type="text/javascript">
  	// <![CDATA[
		jQuery(document).ready(function(){
			$(".tablesorter").tablesorter({widgets: ['zebra']});
			jQuery(".chosen").chosen({ search_contains: true });
			$(".chosen.errorfield").parent().addClass("chosenErrorfield");
		});
	// ]]>
  	</script>
    <script type="text/javascript" src="${system.commonURL}/pese.js?${system.last_update_timestamp}"></script>
</head> 

<body>
  <div id="wrap">

       	<div id="header">
          <div id="mainlogo">
         		<a href="${data.lintuvaaraURL}"><img alt="Etusivu" src="https://rengastus.helsinki.fi/images/shared/luonnontieteellinen_keskusmuseo.gif?1336646640" /></a>
         		<#if system.StagingMode>     TESTIYMPÄRISTÖ </#if>
         		<#if system.DevelopmentMode> KEHITYSYMPÄRISTÖ </#if>
          </div>
          <div id="infoContainer">
              
              <div id="info">                  
                <ul id="infoTabs">
                	<li><a href="${system.baseURL}?page=logout">Kirjaudu ulos</a></li>
              		<li id="currentUserInfo">Tervetuloa, ${system.user_name!system.user_id}</li>  
              	</ul>
              </div>
              
          </div>
        </div>
	
    <div id="main" class="clearfix">
      <div id="menuContainer">
        <div id="menu">
        	
        	<img id="logo" src="${system.baseURL}/logo.png" alt="ULKOmaiset rengastukset" />
        	
        	<h3>Tapaukset</h3>
        	<ul id="tapaukset">
        		<li><a href="${system.baseURL}">Avaa uusi tapaus</a></li>
  				<li><a href="${system.baseURL}?tila=1">Löytö syötetty</a> <span class="count">(${data.pyynto_count_1})</span></li>
  				<li><a href="${system.baseURL}?tila=2">Odottaa rengastustietoja</a> <span class="count">(${data.pyynto_count_2})</span></li>
  				<li><a href="${system.baseURL}?tila=3">Rengastustiedot vastaanotettu</a> <span class="count">(${data.pyynto_count_3})</span></li>
  				<li><a href="${system.baseURL}?tila=4">Loppuunkäsitelty</a> <span class="count">(${data.pyynto_count_4})</span></li>
  				<li><a href="${system.baseURL}?roskiin=K">Roskakori</a> <span class="count">(${data.pyynto_count_roskakori})</span></li>
  			</ul>
  			
        	<h3>Haku</h3>
        	<ul>
				<li><a href="${system.baseURL}?page=search">Uusi haku</a></li>
			</ul>
			<#--
			<h3>Hallinta</h3>
			<ul>
    			<li><a href="${system.baseURL}?page=tablemanagement&amp;table=pyynto"> Pyynnöt </a></li>
      			<li><a href="${system.baseURL}?page=tablemanagement&amp;table=loyto"> Löydöt </a></li>
      			<li><a href="${system.baseURL}?page=tablemanagement&amp;table=rengastus"> Rengastukset </a></li>
    		</ul>
            -->
        </div>
      </div>

      <div id="contentContainer">

        <div id="content" class="page_content">
        

