package fi.hy.pese.framework.test.db;

import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import fi.hy.pese.framework.main.db.RowCombiningResultHandler;
import fi.hy.pese.framework.main.db._DAO;
import fi.hy.pese.framework.main.db._ResultSet;
import fi.hy.pese.framework.main.db._ResultsHandler;
import fi.hy.pese.framework.main.general.data.DatabaseTableStructure;
import fi.hy.pese.framework.main.general.data.PesaDomainModelDatabaseStructure;
import fi.hy.pese.framework.main.general.data.PesaDomainModelRow;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class RowCombiningTestWithKaynti {
	
	public static Test suite() {
		return new TestSuite(RowCombiningTestWithKaynti.class.getDeclaredClasses());
	}
	
	public static class WhenCombiningResultRowsIntoDatabaseStructureRows extends TestCase {
		
		private List<_PesaDomainModelRow>				resultRows;
		private _ResultsHandler			resultHandler;
		private final PesaDomainModelDatabaseStructure	structure	= buildStructure();
		private final _DAO<_PesaDomainModelRow>				fakeDAO		= new RowCombineTestFakeDAO();
		
		private PesaDomainModelDatabaseStructure buildStructure() {
			PesaDomainModelDatabaseStructure structure = new PesaDomainModelDatabaseStructure();
			structure.setTarkastus(simpleTable("tarkastus"));
			structure.setKaynti(simpleTable("kaynti"));
			structure.setPesa(simpleTable("pesa"));
			structure.setKaynnitCount(4);
			
			return structure;
		}
		
		private DatabaseTableStructure simpleTable(String name) {
			DatabaseTableStructure table = new DatabaseTableStructure(name);
			return table;
		}
		
		private class TestRowCombiner extends RowCombiningResultHandler {
			public TestRowCombiner(_DAO<_PesaDomainModelRow> dao) {
				super(dao);
			}
			
			@Override
			protected boolean handle(_PesaDomainModelRow row) {
				resultRows.add(row);
				return true;
			}
		}
		
		private class RowCombineTestFakeDAO extends DAOStub<_PesaDomainModelRow> {
			public RowCombineTestFakeDAO() {
			}
			
			@Override
			public _PesaDomainModelRow newRow() {
				return new PesaDomainModelRow(structure);
			}
		}
		
		@Override
		protected void setUp() throws Exception {
			resultRows = new ArrayList<>();
			resultHandler = new TestRowCombiner(fakeDAO);
		}
		
		public void test__processing_empty_resultset() throws SQLException {
			resultHandler.process(new _ResultSet() {
				@Override
				public boolean next() throws SQLException {
					return false;
				}
				
				@Override
				public String getString(String columnLabel) throws SQLException {
					return null;
				}
				
				@Override
				public String getString(int i) throws SQLException {
					return null;
				}
				
				@Override
				public Date getDate(String columnLabel) throws SQLException {
					return null;
				}
				
				@Override
				public Date getDate(int i) throws SQLException {
					return null;
				}
			});
			assertEquals(0, resultRows.size());
		}
		
		public void test__processing_single_row() throws Exception {
			resultHandler.process(new _ResultSet() {
				boolean	first	= true;
				
				@Override
				public boolean next() throws SQLException {
					if (first) {
						first = false;
						return true;
					}
					return false;
				}
				
				@Override
				public String getString(int i) throws SQLException {
					return "testvalue";
				}
				
				@Override
				public String getString(String columnLabel) throws SQLException {
					return null;
				}
				
				@Override
				public Date getDate(String columnLabel) throws SQLException {
					return null;
				}
				
				@Override
				public Date getDate(int i) throws SQLException {
					return null;
				}
			});
			assertEquals(1, resultRows.size());
			_PesaDomainModelRow row = resultRows.get(0);
			assertEquals("testvalue", row.getTarkastus().getRowidValue());
		}
		
		public void test__single_row_with_values() throws Exception {
			resultHandler.process(new _ResultSet() {
				boolean	first	= true;
				
				@Override
				public boolean next() throws SQLException {
					if (first) {
						first = false;
						return true;
					}
					return false;
				}
				
				@Override
				public String getString(int i) throws SQLException {
					switch (i) {
						case 1:
							return "tarkastus_rowid";
						case 2:
							return "kaynti_rowid";
						case 3:
							return "pesa_rowid";
						case 4:
							return "kunta_rowid";
					}
					throw new SQLException("end reached");
				}
				
				@Override
				public String getString(String columnLabel) throws SQLException {
					return null;
				}
				
				@Override
				public Date getDate(String columnLabel) throws SQLException {
					return null;
				}
				
				@Override
				public Date getDate(int i) throws SQLException {
					return null;
				}
			});
			assertEquals(1, resultRows.size());
			_PesaDomainModelRow row = resultRows.get(0);
			assertEquals("tarkastus_rowid", row.getTarkastus().getRowidValue());
			assertEquals(4, row.getKaynnit().getStaticCount());
			assertEquals("kaynti_rowid", row.getKaynnit().get(1).getRowidValue());
			assertEquals("pesa_rowid", row.getPesa().getRowidValue());
		}
		
		public void test__two_rows___no_kaynti() throws Exception {
			resultHandler.process(new _ResultSet() {
				private int	rownum	= 0;
				
				@Override
				public boolean next() throws SQLException {
					return (++rownum <= 2);
				}
				
				@Override
				public String getString(int i) throws SQLException {
					switch (i) {
						case 1:
							return "tarkastus_rowid_" + rownum;
						case 2:
							return "";
						case 3:
							return "pesa_rowid_" + rownum;
					}
					return "";
				}
				
				@Override
				public String getString(String columnLabel) throws SQLException {
					return null;
				}
				
				@Override
				public Date getDate(String columnLabel) throws SQLException {
					return null;
				}
				
				@Override
				public Date getDate(int i) throws SQLException {
					return null;
				}
			});
			assertEquals(2, resultRows.size());
			_PesaDomainModelRow row = resultRows.get(0);
			assertEquals("tarkastus_rowid_1", row.getTarkastus().getRowidValue());
			assertEquals("", row.getKaynnit().get(1).getRowidValue());
			assertEquals("pesa_rowid_1", row.getPesa().getRowidValue());
			row = resultRows.get(1);
			assertEquals("tarkastus_rowid_2", row.getTarkastus().getRowidValue());
			assertEquals("", row.getKaynnit().get(1).getRowidValue());
			assertEquals("pesa_rowid_2", row.getPesa().getRowidValue());
		}
		
		private class RowValues {
			private final List<Object[]>	rows;
			
			public RowValues() {
				this.rows = new ArrayList<>();
			}
			
			public void addRow(Object... values) {
				rows.add(values);
			}
			
			public String valueFor(int row, int col) {
				if (row > rows.size()) return "";
				Object[] values = rows.get(row - 1);
				if (col > values.length) return "";
				return values[col - 1].toString();
			}
		}
		
		// tark kaynti
		// 1 1
		// 1 2
		public void test__one_row_with_two_kaynti() throws Exception {
			resultHandler.process(new _ResultSet() {
				private int				rownum	= 0;
				private final RowValues	values	= init();
				
				private RowValues init() {
					RowValues values = new RowValues();
					values.addRow("tarkastus_1", "kaynti_1");
					values.addRow("tarkastus_1", "kaynti_2");
					return values;
				}
				
				@Override
				public boolean next() throws SQLException {
					return (++rownum <= 2);
				}
				
				@Override
				public String getString(int i) throws SQLException {
					return values.valueFor(rownum, i);
				}
				
				@Override
				public String getString(String columnLabel) throws SQLException {
					return null;
				}
				
				@Override
				public Date getDate(String columnLabel) throws SQLException {
					return null;
				}
				
				@Override
				public Date getDate(int i) throws SQLException {
					return null;
				}
			});
			assertEquals(1, resultRows.size());
			_PesaDomainModelRow row = resultRows.get(0);
			assertEquals("tarkastus_1", row.getTarkastus().getRowidValue());
			assertEquals("kaynti_1", row.getKaynnit().get(1).getRowidValue());
			assertEquals("kaynti_2", row.getKaynnit().get(2).getRowidValue());
		}
		
		// tark kaynti
		// 1 1
		// 1 2
		// 2 3
		// 2 4
		public void test__two_rows_with_two_kaynti() throws Exception {
			resultHandler.process(new _ResultSet() {
				private int				rownum	= 0;
				private final RowValues	values	= init();
				
				private RowValues init() {
					RowValues values = new RowValues();
					values.addRow("tarkastus_1", "kaynti_1");
					values.addRow("tarkastus_1", "kaynti_2");
					values.addRow("tarkastus_2", "kaynti_3");
					values.addRow("tarkastus_2", "kaynti_4");
					return values;
				}
				
				@Override
				public boolean next() throws SQLException {
					return (++rownum <= 4);
				}
				
				@Override
				public String getString(int i) throws SQLException {
					return values.valueFor(rownum, i);
				}
				
				@Override
				public String getString(String columnLabel) throws SQLException {
					return null;
				}
				
				@Override
				public Date getDate(String columnLabel) throws SQLException {
					return null;
				}
				
				@Override
				public Date getDate(int i) throws SQLException {
					return null;
				}
			});
			assertEquals(2, resultRows.size());
			_PesaDomainModelRow row = resultRows.get(0);
			assertEquals("tarkastus_1", row.getTarkastus().getRowidValue());
			assertEquals("kaynti_1", row.getKaynnit().get(1).getRowidValue());
			assertEquals("kaynti_2", row.getKaynnit().get(2).getRowidValue());
			row = resultRows.get(1);
			assertEquals("tarkastus_2", row.getTarkastus().getRowidValue());
			assertEquals("kaynti_3", row.getKaynnit().get(1).getRowidValue());
			assertEquals("kaynti_4", row.getKaynnit().get(2).getRowidValue());
		}
		
	}
}
