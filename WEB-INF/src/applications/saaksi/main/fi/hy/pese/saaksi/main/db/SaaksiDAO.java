package fi.hy.pese.saaksi.main.db;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Vector;

import fi.hy.pese.framework.main.db.RowExistsResultHandler;
import fi.hy.pese.framework.main.db._DAO;
import fi.hy.pese.framework.main.db._ResultSet;
import fi.hy.pese.framework.main.db._ResultsHandler;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.saaksi.main.app.SaaksiConst;
import fi.luomus.commons.utils.Utils;

public class SaaksiDAO {

	private static final String	DEAD_NESTS_SQL	= " SELECT pesaid FROM tarkastus WHERE epaonn = 'D'";

	public static Collection<String> deadNests(_DAO<_PesaDomainModelRow> dao) throws SQLException {
		DeadNestsQueryResultHandler resultHandler = new DeadNestsQueryResultHandler();
		dao.executeSearch(DEAD_NESTS_SQL, resultHandler);
		return resultHandler.deadNests();
	}

	private static class DeadNestsQueryResultHandler implements _ResultsHandler {
		private final Collection<String>	deadNests	= new HashSet<>();

		@Override
		public void process(_ResultSet rs) throws SQLException {
			while (rs.next()) {
				deadNests.add(rs.getString(1));
			}
		}

		public Collection<String> deadNests() {
			return deadNests;
		}
	}

	private static final String	SUCCESFUL_NESTINGS_IN_REVIIRI_SQL	= succesfulNestingsInReviiriSql();

	private static String succesfulNestingsInReviiriSql() {

		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT vuosi.pesaid                             ");
		sql.append(" FROM   vuosi, tarkastus                         ");
		sql.append(" WHERE  vuosi.pesaid = tarkastus.pesaid          ");
		sql.append(" AND    vuosi.reviiriid = ?                      ");
		sql.append(" AND    vuosi.pesaid != ?                        ");
		sql.append(" AND    vuosi.vuosi = tarkastus.vuosi            ");
		sql.append(" AND    tarkastus.vuosi = ?                      ");
		sql.append(" AND    pesimistulos in ( ");
		Utils.toCommaSeperatedStatement(sql, SaaksiConst.ONNISTUMISKOODIT, true);
		sql.append(" ) ");
		return sql.toString();
	}

	/**
	 * Returns other successful nestings of the given reviiri on the given year (conflicts). If none found, returns an empty collection.
	 * There can only be one successful nesting per one reviiri for a given year.
	 * @param reviiri_id
	 * @param vuosi
	 * @param ignoreThisPesaid this nest ID is not included to the conflict (the nest that we are doing update/insert for; if the nesting itself is successful, that is ok as long as there are no other successful nestings)
	 * @param dao
	 * @return
	 * @throws SQLException
	 */
	public static Collection<String> findOutIfThereAreOtherSuccessfulNestingsForReviiri(String reviiri_id, String vuosi, String ignoreThisPesaid, _DAO<_PesaDomainModelRow> dao) throws SQLException {
		if (ignoreThisPesaid == null || ignoreThisPesaid.length() < 1) {
			ignoreThisPesaid = "99999999";
		}
		ReviiriConflictsResultHandler resultHandler = new ReviiriConflictsResultHandler();
		dao.executeSearch(SUCCESFUL_NESTINGS_IN_REVIIRI_SQL, resultHandler, reviiri_id, ignoreThisPesaid, vuosi);
		return resultHandler.conflicts();
	}

	private static class ReviiriConflictsResultHandler implements _ResultsHandler {
		private final Collection<String>	conflicts	= new ArrayList<>(10);

		@Override
		public void process(_ResultSet rs) throws SQLException {
			while (rs.next()) {
				conflicts.add(rs.getString(1));
			}
		}

		public Collection<String> conflicts() {
			return conflicts;
		}
	}

	private static final String TARKASTUS_EXITS_SQL = " SELECT 1 FROM tarkastus WHERE pesaid = ? AND vuosi = ? ";

	/**
	 * Checks if pesa has tarkastus for a given year
	 * @param pesa_id
	 * @param tark_vuosi
	 * @return true if tarkastus exists, false if not
	 * @throws SQLException
	 */
	public static boolean checkTarkastusForTheYearExists(String pesa_id, String tark_vuosi, _DAO<_PesaDomainModelRow> dao) throws SQLException {
		RowExistsResultHandler resultHandler = new RowExistsResultHandler();
		dao.executeSearch(TARKASTUS_EXITS_SQL, resultHandler, pesa_id, tark_vuosi);
		return resultHandler.rowExists;
	}

	/**
	 * Checks if a reviiri with the given name already exists. If ignoredId is given, that reviiri is ignored.
	 * This is needed because we want reviiri names to be unique. Naturally when checking we want to ignore
	 * the reviiri in question itself.
	 * @param reviirinimi - ignores upercase
	 * @param ignoreId - can be null, if null checks if nimi exits at all
	 * @param dao
	 * @return true if (another) reviiri with same name exists
	 * @throws SQLException
	 */
	public static boolean reviirinimiExists(String reviirinimi, String ignoreId, _DAO<_PesaDomainModelRow> dao) throws SQLException {
		Vector<String> values = new Vector<>();
		String sql = " SELECT 1 FROM reviiri WHERE UPPER(reviirinimi) like ? ";
		values.add(reviirinimi.toUpperCase());
		if (ignoreId != null) {
			sql += " AND reviiri_id != ? ";
			values.add(ignoreId);
		}
		RowExistsResultHandler resultHandler = new RowExistsResultHandler();
		dao.executeSearch(sql, resultHandler, toArray(values));
		return resultHandler.rowExists;
	}

	private static String[] toArray(List<String> values) {
		return values.toArray(new String[values.size()]);
	}

	/**
	 * Returns the year nest has been marked dead, or null if it is still alive
	 * @param pesa_id
	 * @param dao
	 * @return year nest has been marked dead or null if still alive
	 * @throws SQLException
	 */
	public static Integer yearNestHasDied(String pesa_id, _DAO<_PesaDomainModelRow> dao) throws SQLException {
		String query = " SELECT MIN(vuosi) FROM tarkastus WHERE epaonn = 'D' AND pesaid = ? ";
		YearNestHasDiedResultHandler resultHandler = new YearNestHasDiedResultHandler();
		dao.executeSearch(query, resultHandler, pesa_id);
		return resultHandler.yearNestHasDied;
	}

	private static class YearNestHasDiedResultHandler implements _ResultsHandler {

		public Integer yearNestHasDied = null;

		@Override
		public void process(_ResultSet rs) throws SQLException {
			if (!rs.next()) {
				yearNestHasDied = null;
				return;
			}
			String year = rs.getString(1);
			if (year == null) {
				yearNestHasDied = null;
				return;
			}
			yearNestHasDied = Integer.valueOf(year);
		}

	}

}
