package fi.hy.pese.saaksi.main.functionality.reports;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.general.ReportWriter;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.luomus.commons.utils.DateUtils;

/**
 * Table of breeding counts by nest and territory
 */
public class SaaksiReport_poikasmaarat extends ReportWriter<_PesaDomainModelRow> {

	/**
A    Pesä asumaton.
X    Pesä jonkin toisen lajin asuma.
B    Pesä korjaamaton, aikuinen lintu/lintuja nähty pesäpaikalla.
C    Kesäpesä, rakennettu tuhoutuneen tilalle samana pesimiskautena.
Y    Asuttu pesä, ei tarkempia tietoja pesimistuloksesta.
D    Pesää rakennettu, jäänyt kesken.
E    Pesä rakennettu keväällä valmiiksi, pesään ei kuitenkaan ole munittu.
F    Pesä rakennettu keväällä valmiiksi, ehkä munittu.
K    Kaiken järjen mukaan munittu (selvä pesäkuoppa, mutta ei siruja), ei poikasia.
L    Luultavasti munittu, luultavasti ollut myös poikasia.
M    Varmasti munittu, mutta varmasti ei ole ollut poikasia (munansiruja, hautonut).
N    Varmasti munittu, ei tietoa poikasista (nietu).
O    Oletettavasti ollut poikasia, varmasti munittu.
P    Varmasti ollut poikasia, jotka kuitenkin menehtyneet ennen rengastuskäyntiä.
Q    Alle rengastusikäisiä poikasia, joiden selviämisestä lentoon ei tietoa.
R    Rengastusikäisiä poikasia, joiden selviämisestä lentoon ei tietoa.
T    Rengastusikäisiä poikasia, jotka kuitenkin tuhoutuneet ennen lentokykyisyyttä.
V    Poikaset päässeet varmasti lentoon.

Vanha (osin väärä) taulukko ja ohjeistus:
=========================================
                                                        P2/    P2/    P2/   P2/   P2/   P1/   PP/   PP/    PP/   PP/    PP/
Vuosi    P1    P2  REV1  REV2  REV3  REV4    MP    PP  REV1   REV2   REV3  REV4    MP    PP  REV1  REV2   REV3  REV4     MP
-----  ----  ----  ----  ----  ----  ----  ----  ----  ----   ----   ----  ----  ----  ----  ----  ----  ----  ----  ----
 1971   148   148   115   112   106   100    79    70   1.30   1.32  1.40  1.48  1.87  2.11  61.40 62.50  66.04  70.00  88.61
 1972   171   173   126   123   120   111   101    82   1.37   1.41  1.44  1.56  1.71  2.11  65.08 66.67  68.33  73.87  81.19

P1: poikasmäärä 1 (poikasmäärä 9 => 0, pesimistulos = R, V)
P2: poikasmäärä 2 (poikasmäärä 9 => 2, pesimistulos = R, V)
REV1: asutut reviirit 1 (pesimistulos = B..V)
REV2: asutut reviirit 2 (pesimistulos = B, D..V)
REV3: asutut reviirit 3 (pesimistulos = D..V)
REV4: asutut reviirit 4 (pesimistulos = F..V)
MP: munapesiä (pesimistulos = M..V)
PP: poikaspesiä (pesimistulos = R, V)

REV1..4 ja MP ollessa jakajana, mukaan ei oteta pesimistuloksia Y ja Q


Uusi ohjeistus:
===============
A X B C Y D E F K L M N O P Q R T V

(P1)_POIK_LKM_TARK                  (R V)
(PP_T)_PESA_LKM_JOISSA_P_LKM_TARK   (R V)
(P2)_POIK_LKM_ARVIO  				(9 tai puuttuva --> 2) (R V)
(MP)_VARMA_MUNAPESA_LKM				(M N O P Q R T V)
(PP)_VARMA_POIKASPESA 				(R V)

(REV1)_REV_ASUTTU 		   (B C Y D E F K L M N O P Q R T V)
(REV2)_REV_ASUTTU_EI_C_Y   (B     D E F K L M N O P Q R T V)
(REV3)_REV_RAKENNETTU      (      D E F K L M N O P Q R T V)
(REV4)_REV_EHKA_MUNITTU    (          F K L M N O P Q R T V)

REV1_EI_Y_Q 		   		(B C   D E F K L M N O P   R T V)
REV2_EI_Y_Q   				(B     D E F K L M N O P   R T V)
REV3_EI_Y_Q      			(      D E F K L M N O P   R T V)
REV4_EI_Y_Q    				(          F K L M N O P   R T V)

	 */

	private static final class YearSums {
		private int p1_poik_lkm_tark = 0;
		private int pp_t_pesa_lkm_joissa_p_lkm_tark = 0;
		private int p2_poik_lkm_arvio = 0;
		private int mp_varma_munapesa_lkm = 0;
		private int pp_varma_poikaspesa_lkm = 0;
		private final Set<Integer> rev1 = new HashSet<>();
		private final Set<Integer> rev2 = new HashSet<>();
		private final Set<Integer> rev3 = new HashSet<>();
		private final Set<Integer> rev4 = new HashSet<>();
		private final Set<Integer> rev1_jakaja = new HashSet<>();
		private final Set<Integer> rev2_jakaja = new HashSet<>();
		private final Set<Integer> rev3_jakaja = new HashSet<>();
		private final Set<Integer> rev4_jakaja = new HashSet<>();

		public void add(_PesaDomainModelRow row) {
			String pesimistulos = row.getTarkastus().get("pesimistulos").getValue();
			if (!given(pesimistulos)) return;

			if ("RV".contains(pesimistulos)) {
				this.pp_varma_poikaspesa_lkm++;

				int poik1 = lkm(row, "elavia");
				int poik2 = lkm(row, "lentopoik");
				int poik3 = lkm(row, "lop_lentopoik");
				int poik1_tark = poik1 == 9 ? 0 : poik1;
				int poik2_tark = poik2 == 9 ? 0 : poik2;
				int poik3_tark = poik3 == 9 ? 0 : poik3;

				int poikLkmTark = Math.max(Math.max(poik1_tark, poik2_tark), poik3_tark);
				int poikLkmArvio = 2;
				if (poikLkmTark > 0) {
					poikLkmArvio = poikLkmTark;
					this.pp_t_pesa_lkm_joissa_p_lkm_tark++;
					this.p1_poik_lkm_tark += poikLkmTark;
				}
				this.p2_poik_lkm_arvio += poikLkmArvio;
			}
			if ("MNOPQRTV".contains(pesimistulos)) {
				this.mp_varma_munapesa_lkm++;
			}

			int reviiriId = row.getReviiri().getUniqueNumericIdColumn().getIntegerValue();
			reviiriCount("B C Y D E F K L M N O P Q R T V", rev1, rev1_jakaja, pesimistulos, reviiriId);
			reviiriCount("B     D E F K L M N O P Q R T V", rev2, rev2_jakaja, pesimistulos, reviiriId);
			reviiriCount("      D E F K L M N O P Q R T V", rev3, rev3_jakaja, pesimistulos, reviiriId);
			reviiriCount("          F K L M N O P Q R T V", rev4, rev4_jakaja, pesimistulos, reviiriId);
		}

		private void reviiriCount(String allowed, Set<Integer> set, Set<Integer> set_jakaja, String pesimistulos, int reviiriId) {
			if (allowed.contains(pesimistulos)) {
				set.add(reviiriId);
				if (!"YQ".contains(pesimistulos)) {
					set_jakaja.add(reviiriId);
				}
			}
		}

		private int lkm(_PesaDomainModelRow row, String c) {
			return row.getTarkastus().get(c).hasValue() ? row.getTarkastus().get(c).getIntegerValue() : 0;
		}
	}

	public SaaksiReport_poikasmaarat(_Request<_PesaDomainModelRow> request, String filenamePrefix) {
		super(request, filenamePrefix);
	}

	@Override
	public boolean produceSpecific() {
		headerLine();
		newLine();
		newLine();
		columnDescriptions();
		rows();
		return true;
	}

	private void rows() {
		Map<Integer, YearSums> sums = new HashMap<>();
		for (_PesaDomainModelRow row : request.data().results()) {
			int year = row.getTarkastus().get("vuosi").getIntegerValue();
			if (!sums.containsKey(year)) {
				sums.put(year, new YearSums());
			}
			sums.get(year).add(row);
		}
		for (int year = 1971; year <= DateUtils.getCurrentYear(); year++) {
			if (!sums.containsKey(year)) continue;
			YearSums s = sums.get(year);

			write(year); separator();

			write(s.p1_poik_lkm_tark); separator();
			write(s.p2_poik_lkm_arvio); separator();
			write(s.mp_varma_munapesa_lkm); separator();
			write(s.pp_varma_poikaspesa_lkm); separator();
			write(s.pp_t_pesa_lkm_joissa_p_lkm_tark); separator();
			write(s.rev1.size()); separator();
			write(s.rev2.size()); separator();
			write(s.rev3.size()); separator();
			write(s.rev4.size()); separator();
			write(s.rev1_jakaja.size()); separator();
			write(s.rev2_jakaja.size()); separator();
			write(s.rev3_jakaja.size()); separator();
			write(s.rev4_jakaja.size()); separator();

			write((double) s.p2_poik_lkm_arvio / (double) s.rev1_jakaja.size()); separator();
			write((double) s.p2_poik_lkm_arvio / (double) s.rev2_jakaja.size()); separator();
			write((double) s.p2_poik_lkm_arvio / (double) s.rev3_jakaja.size()); separator();
			write((double) s.p2_poik_lkm_arvio / (double) s.rev4_jakaja.size()); separator();
			write((double) s.p2_poik_lkm_arvio / (double) s.mp_varma_munapesa_lkm); separator();

			write((double) s.p1_poik_lkm_tark / (double) s.pp_t_pesa_lkm_joissa_p_lkm_tark); separator();

			write((double) s.pp_varma_poikaspesa_lkm / (double) s.rev1_jakaja.size() * 100); separator();
			write((double) s.pp_varma_poikaspesa_lkm / (double) s.rev2_jakaja.size() * 100); separator();
			write((double) s.pp_varma_poikaspesa_lkm / (double) s.rev3_jakaja.size() * 100); separator();
			write((double) s.pp_varma_poikaspesa_lkm / (double) s.rev4_jakaja.size() * 100); separator();
			write((double) s.pp_varma_poikaspesa_lkm / (double) s.mp_varma_munapesa_lkm * 100); separator();
			newLine();
		}
	}

	private void columnDescriptions() {
		write("Koodien selitykset:"); newLine();
		newLine();
		write("P1|Poikasten tarkka lkm (ilmoitettu tarkasti)||||||(R, V)"); newLine();
		write("P2|Poikasten arvioitu lkm (puuttuva tieto tai 9 == 2)||||||(R, V)"); newLine();
		newLine();
		write("MP|Varmojen munapesien lkm||||||(M N O P Q R T V)"); newLine();
		write("PP|Varmojen poikaspesien lkm||||||(R V)"); newLine();
		write("PP_T|Pesien lkm jossa ilmoitettu poikasten lkm tarkasti||||||(R, V)"); newLine();
		newLine();
		write("REV1|Asuttu||||||(B C Y D E F K L M N O P Q R T V)"); newLine();
		write("REV2|Asuttu ei C Y||||||(B     D E F K L M N O P Q R T V)"); newLine();
		write("REV3|Rakennettu||||||(      D E F K L M N O P Q R T V)"); newLine();
		write("REV4|Ehkä munittu||||||(          F K L M N O P Q R T V)"); newLine();
		newLine();
		write("REV1_J|Kuten REV1 mutta ei Y Q||||||(B C   D E F K L M N O P   R T V)"); newLine();
		write("REV2_J|Kuten REV2 mutta ei Y Q||||||(B     D E F K L M N O P   R T V)"); newLine();
		write("REV3_J|Kuten REV3 mutta ei Y Q||||||(      D E F K L M N O P   R T V)"); newLine();
		write("REV4_J|Kuten REV4 mutta ei Y Q||||||(          F K L M N O P   R T V)"); newLine();

		newLine();
		newLine();

		separator(14);
		write("P2/"); separator();
		write("P2/"); separator();
		write("P2/"); separator();
		write("P2/"); separator();
		write("P2/"); separator();
		write("P1/"); separator();
		write("PP/"); separator();
		write("PP/"); separator();
		write("PP/"); separator();
		write("PP/"); separator();
		write("PP/"); separator();
		newLine();
		write("Vuosi"); separator();
		write("P1"); separator();
		write("P2"); separator();
		write("MP"); separator();
		write("PP"); separator();
		write("PP_T"); separator();
		write("REV1"); separator();
		write("REV2"); separator();
		write("REV3"); separator();
		write("REV4"); separator();
		write("REV1_J"); separator();
		write("REV2_J"); separator();
		write("REV3_J"); separator();
		write("REV4_J"); separator();

		write("REV1_J"); separator();
		write("REV2_J"); separator();
		write("REV3_J"); separator();
		write("REV4_J"); separator();
		write("MP"); separator();
		write("PP_T"); separator();
		write("REV1_J"); separator();
		write("REV2_J"); separator();
		write("REV3_J"); separator();
		write("REV4_J"); separator();
		write("MP"); separator();
		newLine();
	}

}
