package fi.hy.pese.peto.main.functionality;

import java.sql.SQLException;
import java.util.List;

import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.app.functionality.TablemanagementFunctionality;
import fi.hy.pese.framework.main.app.functionality.validate.TablemanagementValidator;
import fi.hy.pese.framework.main.app.functionality.validate._Validator;
import fi.hy.pese.framework.main.db.RowsToData_Single_Table_ResultHandler;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.main.general.data._Table;
import fi.hy.pese.peto.main.app.PetoConst;

public class SeurantaruutuManagementFunctionality extends TablemanagementFunctionality<_PesaDomainModelRow> {

	private static class SeurantaruutuManagementValidator extends TablemanagementValidator<_PesaDomainModelRow> {

		public SeurantaruutuManagementValidator(_Request<_PesaDomainModelRow> request) {
			super(request);
		}

		@Override
		public void validate() throws Exception {
			super.validate();
			if (data.action().equals(Const.INSERT) || data.action().equals(Const.UPDATE)) {
				validateInsertUpdate();
			} else if (data.action().equals(Const.SEARCH)) {
				_Column vuosi = data.searchparameters().getTableByName("seurantaruutu").get("vuosi");
				if (isNull(vuosi)) {
					error(vuosi, _Validator.CAN_NOT_BE_NULL);
				}
			} else if (data.action().equals(PetoConst.COPY_SEURANTARUUDUT)) {
				checkInteger("orig_year", data.get("orig_year"));
				checkInteger("dest_year", data.get("dest_year"));
				checkValidYear("orig_year", data.get("orig_year"));
				checkValidYear("dest_year", data.get("dest_year"));
			}
		}

		private void validateInsertUpdate() throws SQLException {
			_Column vuosi = data.updateparameters().getTableByName("seurantaruutu").get("vuosi");
			_Column leveys = data.updateparameters().getTableByName("seurantaruutu").get("kulma_yht_leveys");
			_Column pituus = data.updateparameters().getTableByName("seurantaruutu").get("kulma_yht_pituus");
			_Column koko = data.updateparameters().getTableByName("seurantaruutu").get("koko");

			checkValidYear(vuosi);
			checkRange(leveys, _Validator.YHT_LEVEYS_MIN, _Validator.YHT_LEVEYS_MAX);
			checkRange(pituus, _Validator.YHT_PITUUS_MIN, _Validator.YHT_PITUUS_MAX);
			checkRange(koko, 1000, 80000);

			if (noErrors(koko)) {
				if (koko.getIntegerValue() != 10000) {
					warning(koko, _Validator.ARE_YOU_SURE);
				}
			}

			if (hasErrors()) return;

			if (data.action().equals(Const.INSERT)) {
				checkDoesNotExists(vuosi, leveys, pituus);
			}
		}

		private void checkDoesNotExists(_Column vuosi, _Column leveys, _Column pituus) throws SQLException {
			_Table searchParams = dao.newTableByName("seurantaruutu");
			searchParams.get("vuosi").setValue(vuosi);
			searchParams.get("kulma_yht_leveys").setValue(leveys);
			searchParams.get("kulma_yht_pituus").setValue(pituus);
			boolean exists = dao.checkValuesExists(searchParams);
			if (exists) {
				warning("Petoruutu", "On jo olemassa");
			}
		}

	}

	public SeurantaruutuManagementFunctionality(_Request<_PesaDomainModelRow> request) {
		super(request);
	}


	@Override
	public void preProsessingHook() throws Exception {
		data.set(Const.TABLE, "seurantaruutu");
		super.preProsessingHook();
	}

	@Override
	public _Validator validator() {
		return new SeurantaruutuManagementValidator(request);
	}

	@Override
	public void afterSuccessfulValidation() throws Exception {
		String action = data.action();
		if (action.equals("")) return;

		String tablename = data.get(Const.TABLE);
		_Table table = data.updateparameters().getTableByName(tablename);
		_Table searchparams = data.searchparameters().getTableByName(tablename);

		if (action.equals(Const.UPDATE)) {
			super.update(table);
		} else if (action.equals(Const.DELETE)) {
			super.delete(table);
		} else if (action.equals(Const.INSERT)) {
			super.insert(table);
			searchparams.setRowidValue(table.getRowidValue());
			data.setAction(Const.SEARCH);
		} else if (action.equals(PetoConst.COPY_SEURANTARUUDUT)) {
			copy(searchparams);
		}

		dao.executeSearch(searchparams, new RowsToData_Single_Table_ResultHandler<>(data, tablename));
	}

	private void copy(_Table searchparams) throws SQLException {
		String origYear = data.get("orig_year");
		String destYear = data.get("dest_year");

		searchparams.get("vuosi").setValue(origYear);
		List<_Table> results = dao.returnTables(searchparams);
		for (_Table result : results) {
			dao.returnAndSetNextId(result);
			result.get("vuosi").setValue(destYear);
			dao.executeInsert(result);
		}

		data.setSuccessText(PetoConst.SUCCESS_COPY_SEURANTARUUDUT);
		searchparams.get("vuosi").setValue(destYear);
	}
}
