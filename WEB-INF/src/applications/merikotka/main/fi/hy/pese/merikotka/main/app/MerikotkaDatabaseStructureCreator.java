package fi.hy.pese.merikotka.main.app;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import fi.hy.pese.framework.main.app.functionality.validate._Validator;
import fi.hy.pese.framework.main.db.oraclesql.Queries;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data.DatabaseColumnStructure;
import fi.hy.pese.framework.main.general.data.DatabaseTableStructure;
import fi.hy.pese.framework.main.general.data.PesaDomainModelDatabaseStructure;
import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._Table;
import fi.luomus.commons.containers.Selection;
import fi.luomus.commons.db.connectivity.TransactionConnection;
import fi.luomus.commons.tipuapi.TipuAPIClient;

public class MerikotkaDatabaseStructureCreator {
	
	public static PesaDomainModelDatabaseStructure loadFromDatabase(TransactionConnection con) throws SQLException {
		
		PesaDomainModelDatabaseStructure structure = new PesaDomainModelDatabaseStructure();
		
		// Taulut
		DatabaseTableStructure pesa = Queries.returnTableStructure("pesavakio", con);
		DatabaseTableStructure olosuhde = Queries.returnTableStructure("pesamuuttuva", con);
		DatabaseTableStructure tarkastus = Queries.returnTableStructure("pesatarkastus", con);
		DatabaseTableStructure poikanen = Queries.returnTableStructure("poikanen", con);
		DatabaseTableStructure aikuinen = Queries.returnTableStructure("aikuinen", con);
		DatabaseTableStructure vuosi = Queries.returnTableStructure("vuosi", con);
		DatabaseTableStructure reviiri = Queries.returnTableStructure("reviiri", con);
		DatabaseTableStructure suuralue = Queries.returnTableStructure("suuralue", con);
		DatabaseTableStructure searches = Queries.returnTableStructure("saved_searches", con);
		DatabaseTableStructure userLoginInfo = Queries.returnTableStructure("kayttaja", con);
		DatabaseTableStructure muutosloki = Queries.returnTableStructure("muutosloki", con);
		DatabaseTableStructure lomakeVastaanottajat = Queries.returnTableStructure("lomake_vastaanottaja", con);
		
		DatabaseTableStructure aputaulu = new DatabaseTableStructure("aputaulu");
		aputaulu.addColumn("taulu", _Column.VARCHAR, 15);
		aputaulu.addColumn("attribuutti", _Column.VARCHAR, 20);
		aputaulu.addColumn("jarjestys", _Column.INTEGER, 3);
		aputaulu.addColumn("arvo", _Column.VARCHAR, 10);
		aputaulu.addColumn("selite", _Column.VARCHAR, 256);
		aputaulu.addColumn("ryhmittely", _Column.VARCHAR, 256);
		
		// N kpl sisältävien staattinen lukumäärä (listauksiin yms)
		structure.setPoikasetCount(4);
		structure.setAikuisetCount(3);
		
		
		// Taulut templateen
		structure.setPesa(pesa);
		structure.setOlosuhde(olosuhde);
		structure.setTarkastus(tarkastus);
		structure.setVuosi(vuosi);
		structure.setReviiri(reviiri);
		structure.setAputaulu(aputaulu);
		structure.setPoikanen(poikanen);
		structure.setAikuinen(aikuinen);
		structure.setSavedSearches(searches);
		structure.defineCustomTable(userLoginInfo);
		structure.defineCustomTable(suuralue);
		structure.setLogTable(muutosloki);
		structure.setLomakeVastaanottajat(lomakeVastaanottajat);
		structure.setTransactionEntry(Const.TRANSACTION_ENTRY_TABLE_STRUCTURE);
		
		// Avaimet
		pesa.get("pesa_id").setTypeToUniqueNumericIncreasingId();
		olosuhde.get("pesamuuttuva_id").setTypeToUniqueNumericIncreasingId();
		tarkastus.get("p_tarkastus_id").setTypeToUniqueNumericIncreasingId();
		poikanen.get("poikanen_id").setTypeToUniqueNumericIncreasingId();
		aikuinen.get("aikuinen_id").setTypeToUniqueNumericIncreasingId();
		vuosi.get("vuosi").setTypeToImmutablePartOfAKey();
		vuosi.get("pesa_id").setTypeToImmutablePartOfAKey();
		reviiri.get("reviiri_id").setTypeToUniqueNumericIncreasingId();
		suuralue.get("id").setTypeToImmutablePartOfAKey();
		aputaulu.get("taulu").setTypeToImmutablePartOfAKey();
		aputaulu.get("attribuutti").setTypeToImmutablePartOfAKey();
		aputaulu.get("arvo").setTypeToImmutablePartOfAKey();
		searches.get("id").setTypeToUniqueNumericIncreasingId();
		userLoginInfo.get("username").setTypeToImmutablePartOfAKey();
		muutosloki.get("id").setTypeToUniqueNumericIncreasingId();
		lomakeVastaanottajat.get("pesa").setTypeToImmutablePartOfAKey();
		lomakeVastaanottajat.get("vastaanottaja").setTypeToImmutablePartOfAKey();
		
		// Viiteavaimet... Ei kattava lista (toistaiseksi), vain ne joita tarvitaan
		tarkastus.get("pesa_id").references(pesa.getUniqueNumericIdColumn());
		vuosi.get("reviiri_id").references(reviiri.get("reviiri_id"));
		
		// Lisäyspäivämääräkentät
		pesa.get("kirj_pvm").setTypeToDateAddedColumn();
		olosuhde.get("kirj_pvm").setTypeToDateAddedColumn();
		tarkastus.get("kirj_pvm").setTypeToDateAddedColumn();
		poikanen.get("kirj_pvm").setTypeToDateAddedColumn();
		aikuinen.get("kirj_pvm").setTypeToDateAddedColumn();
		reviiri.get("kirj_pvm").setTypeToDateAddedColumn();
		vuosi.get("kirj_pvm").setTypeToDateAddedColumn();
		muutosloki.get("kirj_pvm").setTypeToDateAddedColumn();
		
		// Muokkauspäivämääräkentät
		pesa.get("muutos_pvm").setTypeToDateModifiedColumn();
		olosuhde.get("muutos_pvm").setTypeToDateModifiedColumn();
		tarkastus.get("muutos_pvm").setTypeToDateModifiedColumn();
		poikanen.get("muutos_pvm").setTypeToDateModifiedColumn();
		aikuinen.get("muutos_pvm").setTypeToDateModifiedColumn();
		reviiri.get("muutos_pvm").setTypeToDateModifiedColumn();
		vuosi.get("muutos_pvm").setTypeToDateModifiedColumn();
		suuralue.get("muutos_pvm").setTypeToDateModifiedColumn();
		
		muutosloki.get("kayttaja").setTypeToUserIdColumn();
		
		// Tauluhallintaoikeudet
		reviiri.setTablemanagementPermissions(_Table.TABLEMANAGEMENT_UPDATE_INSERT_DELETE);
		aputaulu.setTablemanagementPermissions(_Table.TABLEMANAGEMENT_UPDATE_INSERT);
		userLoginInfo.setTablemanagementPermissions(_Table.TABLEMANAGEMENT_UPDATE_INSERT_DELETE);
		suuralue.setTablemanagementPermissions(_Table.TABLEMANAGEMENT_UPDATE_INSERT);
		muutosloki.setTablemanagementPermissions(_Table.TABLEMANAGEMENT_INSERT_ONLY);
		
		// Liitokset selecteihin
		// Ensin aputaulussa olevan kentät automaattisesti
		Map<String, Selection> selections = Queries.returnSelectionsFromCodeTable(con, "aputaulu", "taulu", "attribuutti", "arvo", "selite", "ryhmittely", new String[] {"selite", "jarjestys", "arvo"});
		for (String name : selections.keySet()) {
			try {
				DatabaseColumnStructure c = structure.getColumn(name);
				c.setAssosiatedSelection(name);
			} catch (Exception e) { /* System.out.println(" Assosiation "+name + " invalid."); */
			}
			if (name.startsWith("poikanen")) {
				try {
					DatabaseColumnStructure c = structure.getColumn(name + "_2");
					c.setAssosiatedSelection(name);
				} catch (Exception e) {
				}
			}
		}
		// Loput liitokset manuaalisesti
		vuosi.get("pesan_kunta").setAssosiatedSelection(TipuAPIClient.MUNICIPALITIES);
		vuosi.get("reviirin_kunta").setAssosiatedSelection(TipuAPIClient.MUNICIPALITIES);
		vuosi.get("reviiri_id").setAssosiatedSelection(MerikotkaConst.REVIIRIT);
		aputaulu.get("taulu").setAssosiatedSelection(MerikotkaConst.TAULUT);
		pesa.get("seur_tarkastaja").setAssosiatedSelection(TipuAPIClient.RINGERS);
		tarkastus.get("tarkastaja1_id").setAssosiatedSelection(TipuAPIClient.RINGERS);
		tarkastus.get("tarkastaja2_id").setAssosiatedSelection(TipuAPIClient.RINGERS);
		tarkastus.get("mittaaja_id_1").setAssosiatedSelection(TipuAPIClient.RINGERS);
		tarkastus.get("mittaaja_id_2").setAssosiatedSelection(TipuAPIClient.RINGERS);
		suuralue.get("vastuuhenkilo").setAssosiatedSelection(TipuAPIClient.RINGERS);
		pesa.get("rak_laji").setAssosiatedSelection(TipuAPIClient.SPECIES);
		tarkastus.get("muulaji").setAssosiatedSelection(TipuAPIClient.SPECIES);
		tarkastus.get("muulaji_rinnakkainen").setAssosiatedSelection(TipuAPIClient.SPECIES);
		tarkastus.get("muulaji_vastarakki").setAssosiatedSelection(MerikotkaConst.YES_NO_SELECTION);
		tarkastus.get("kuvia_lintu").setAssosiatedSelection(MerikotkaConst.YES_NO_SELECTION);
		tarkastus.get("kuvia_pesa").setAssosiatedSelection(MerikotkaConst.YES_NO_SELECTION);
		tarkastus.get("kuvaaja_lintu").setAssosiatedSelection(TipuAPIClient.RINGERS);
		tarkastus.get("kuvaaja_pesa").setAssosiatedSelection(TipuAPIClient.RINGERS);
		olosuhde.get("autoyhteys").setAssosiatedSelection(MerikotkaConst.YES_NO_SELECTION);
		olosuhde.get("naturassa").setAssosiatedSelection(MerikotkaConst.YES_NO_SELECTION);
		tarkastus.get("nayte_s").setAssosiatedSelection(MerikotkaConst.YES_NO_SELECTION);
		tarkastus.get("nayte_i").setAssosiatedSelection(MerikotkaConst.YES_NO_SELECTION);
		tarkastus.get("nayte_o").setAssosiatedSelection(MerikotkaConst.YES_NO_SELECTION);
		poikanen.get("hoyhennayte").setAssosiatedSelection(MerikotkaConst.YES_NO_SELECTION);
		poikanen.get("verinayte").setAssosiatedSelection(MerikotkaConst.YES_NO_SELECTION);
		poikanen.get("hoyhennayte_2").setAssosiatedSelection(MerikotkaConst.YES_NO_SELECTION);
		poikanen.get("verinayte_2").setAssosiatedSelection(MerikotkaConst.YES_NO_SELECTION);
		tarkastus.get("n_rengas_o_vari").setAssosiatedSelection("pesatarkastus.rengas_vari");
		tarkastus.get("n_rengas_v_vari").setAssosiatedSelection("pesatarkastus.rengas_vari");
		tarkastus.get("k_rengas_o_vari").setAssosiatedSelection("pesatarkastus.rengas_vari");
		tarkastus.get("k_rengas_v_vari").setAssosiatedSelection("pesatarkastus.rengas_vari");
		poikanen.get("varit_oikea").setAssosiatedSelection("pesatarkastus.rengas_vari");
		poikanen.get("varit_vasen").setAssosiatedSelection("pesatarkastus.rengas_vari");
		aikuinen.get("varit_oikea").setAssosiatedSelection("pesatarkastus.rengas_vari");
		aikuinen.get("varit_vasen").setAssosiatedSelection("pesatarkastus.rengas_vari");
		aikuinen.get("sukupuoli").setAssosiatedSelection("poikanen.sukupuoli");
		
		// Kentät jotka eivät ole käytössä
		pesa.removeColumn("valokuva");
		pesa.removeColumn("tarkastaja1_id");
		pesa.removeColumn("tarkastaja2_id");
		pesa.removeColumn("tark_pvm");
		pesa.removeColumn("tark_pvm_tark");
		pesa.removeColumn("kuvia_pesa");
		pesa.removeColumn("kuvia_lintu");
		pesa.removeColumn("kuvaaja_id");
		pesa.removeColumn("kuvaaja_id_2");
		olosuhde.removeColumn("alku_pvm");
		olosuhde.removeColumn("loppu_pvm");
		olosuhde.removeColumn("et_maasta_tark");
		olosuhde.removeColumn("et_latvasta_tark");
		olosuhde.removeColumn("rauh_kommentti");
		olosuhde.removeColumn("tarkastaja1_id");
		olosuhde.removeColumn("tarkastaja2_id");
		olosuhde.removeColumn("et_lahipuu");
		olosuhde.removeColumn("tyvihal_tark");
		olosuhde.removeColumn("latvahal_tark");
		tarkastus.removeColumn("pesa_kunto_vanha");
		tarkastus.removeColumn("pesa_kommentti");
		tarkastus.removeColumn("uhat");
		tarkastus.removeColumn("et_talvitie");
		tarkastus.removeColumn("k_rengas_vasen");
		tarkastus.removeColumn("k_rengas_oikea");
		tarkastus.removeColumn("n_rengas_vasen");
		tarkastus.removeColumn("n_rengas_oikea");
		tarkastus.removeColumn("k_rengas_o_vari");
		tarkastus.removeColumn("k_rengas_v_vari");
		tarkastus.removeColumn("n_rengas_o_vari");
		tarkastus.removeColumn("n_rengas_v_vari");
		tarkastus.removeColumn("k_rengas");
		tarkastus.removeColumn("n_rengas");
		tarkastus.removeColumn("rengas");
		tarkastus.removeColumn("rengas_oikea");
		tarkastus.removeColumn("rengas_vasen");
		tarkastus.removeColumn("rengas_o_vari");
		tarkastus.removeColumn("rengas_v_vari");
		poikanen.removeColumn("dna_nayte");
		poikanen.removeColumn("dna_nayte_2");
		poikanen.removeColumn("mittaaja_id");
		poikanen.removeColumn("mittaus_pvm");
		poikanen.removeColumn("mittaaja_id_2");
		poikanen.removeColumn("mittaus_pvm_2");
		
		// Specially defined uppercase columns. All columns that have an assosiated selection (set above) are touppercase by default.
		poikanen.get("rengas_oikea").setToUppercaseColumn();
		poikanen.get("rengas_vasen").setToUppercaseColumn();
		aikuinen.get("rengas_oikea").setToUppercaseColumn();
		aikuinen.get("rengas_vasen").setToUppercaseColumn();
		
		// Misc
		userLoginInfo.get("password").setTypeToPasswordColumn();
		reviiri.get("vanha_reviirinro").setToOptionalColumn();
		reviiri.get("kommentti").setToOptionalColumn();
		aputaulu.get("ryhmittely").setToOptionalColumn();
		
		// Kirjekyyhky pakolliset
		vuosi.get("reviirin_kunta").setKirjekyyhkyRequired();
		vuosi.get("reviiri_id").setKirjekyyhkyRequired();
		vuosi.get("pesan_kunta").setKirjekyyhkyRequired();
		pesa.get("pesanimi").setKirjekyyhkyRequired();
		pesa.get("koord_mittaus").setKirjekyyhkyRequired();
		pesa.get("koord_tyyppi").setKirjekyyhkyRequired();
		pesa.get("koord_tark").setKirjekyyhkyRequired();
		olosuhde.get("pesa_mit_pvm").setKirjekyyhkyRequired();
		olosuhde.get("ymp_mit_pvm").setKirjekyyhkyRequired();
		tarkastus.get("tark_vuosi").setKirjekyyhkyRequired();
		tarkastus.get("tark_pvm").setKirjekyyhkyRequired();
		tarkastus.get("tark_pvm_tark").setKirjekyyhkyRequired();
		tarkastus.get("tarkastaja1_id").setKirjekyyhkyRequired();
		tarkastus.get("pesimistulos").setKirjekyyhkyRequired();
		tarkastus.get("pesimist_tark").setKirjekyyhkyRequired();
		tarkastus.get("nahdyt_merkit").setKirjekyyhkyRequired();
		tarkastus.get("tark_tapa").setKirjekyyhkyRequired();
		tarkastus.get("pesa_kunto").setKirjekyyhkyRequired();
		
		// Min/max values
		vuosi.get("vuosi").setValueLimits(_Validator.MIN_YEAR, _Validator.MAX_YEAR);
		pesa.get("loyt_vuosi").setValueLimits(_Validator.MIN_YEAR, _Validator.MAX_YEAR); // title="Löytövuosi"
		pesa.get("tuhoutumisvuosi").setValueLimits(_Validator.MIN_YEAR, _Validator.MAX_YEAR); // title="Tuhoutumisvuosi"
		pesa.get("rak_vuosi").setValueLimits(_Validator.MIN_YEAR, _Validator.MAX_YEAR); // title="Rakentamisvuosi"
		pesa.get("rak_vuosi_teko").setValueLimits(_Validator.MIN_YEAR, _Validator.MAX_YEAR); // title="Rakentamisvuosi"
		olosuhde.get("korkeus").setValueLimits(0, 50); // title="Puun korkeus (m)"
		olosuhde.get("tyviymparys").setValueLimits(30, 350); // title="ympärys (cm)"
		olosuhde.get("tyvihalkaisija").setValueLimits(6, 120); // title="halkaisija (cm)"
		olosuhde.get("latvaymparys").setValueLimits(6, 190); // title="ympärys (cm)"
		olosuhde.get("latvahalkaisija").setValueLimits(4, 60); // title="halkaisija (cm)"
		olosuhde.get("et_maasta").setValueLimits(0, 60); // title="yläpinnan etäisyys maasta (m)"
		olosuhde.get("et_latvasta").setValueLimits(0, 20); // title="yläpinnan etäisyys latvasta (m)"
		olosuhde.get("manty_lkm_p").setValueLimits(0, 80);
		olosuhde.get("manty_pit_p").setValueLimits(0, 80);
		olosuhde.get("manty_lkm_i").setValueLimits(0, 80);
		olosuhde.get("manty_pit_i").setValueLimits(0, 80);
		olosuhde.get("manty_lkm_l").setValueLimits(0, 80);
		olosuhde.get("manty_pit_l").setValueLimits(0, 80);
		olosuhde.get("manty_lkm_e").setValueLimits(0, 80);
		olosuhde.get("manty_pit_e").setValueLimits(0, 80);
		olosuhde.get("kuusi_lkm_p").setValueLimits(0, 80);
		olosuhde.get("kuusi_pit_p").setValueLimits(0, 80);
		olosuhde.get("kuusi_lkm_i").setValueLimits(0, 80);
		olosuhde.get("kuusi_pit_i").setValueLimits(0, 80);
		olosuhde.get("kuusi_lkm_l").setValueLimits(0, 80);
		olosuhde.get("kuusi_pit_l").setValueLimits(0, 80);
		olosuhde.get("kuusi_lkm_e").setValueLimits(0, 80);
		olosuhde.get("kuusi_pit_e").setValueLimits(0, 80);
		olosuhde.get("muu_lkm_p").setValueLimits(0, 80);
		olosuhde.get("muu_pit_p").setValueLimits(0, 80);
		olosuhde.get("muu_lkm_i").setValueLimits(0, 80);
		olosuhde.get("muu_pit_i").setValueLimits(0, 80);
		olosuhde.get("muu_lkm_l").setValueLimits(0, 80);
		olosuhde.get("muu_pit_l").setValueLimits(0, 80);
		olosuhde.get("muu_lkm_e").setValueLimits(0, 80);
		olosuhde.get("muu_pit_e").setValueLimits(0, 80);
		tarkastus.get("tark_vuosi").setValueLimits(_Validator.MIN_YEAR, _Validator.MAX_YEAR); // title="Pesintävuosi"
		tarkastus.get("munia_lkm").setValueLimits(0, 4); // title="Lopullinen munamäärä"
		tarkastus.get("kuoriutumattomia_lkm").setValueLimits(0, 4); // title="Kuoriutumattomat munat"
		tarkastus.get("kuolleita_lkm").setValueLimits(0, 4); // title="Kuolleet poikaset"
		tarkastus.get("reng_poik_lkm").setValueLimits(0, 4); // title="Rengastusikäiset poikaset"
		tarkastus.get("lentopoik_lkm").setValueLimits(0, 4); // title="Lentopoikaset"
		tarkastus.get("as_lkm_500").setValueLimits(0, 30); // title="500m säteellä"
		tarkastus.get("as_lkm_1000").setValueLimits(0, 200); // title="1000m säteellä"
		tarkastus.get("pesa_korkeus").setValueLimits(0, 450); // title="korkeus (cm)"
		tarkastus.get("pesa_halk_min").setValueLimits(0, 300); // title="pienin halkaisija (cm)"
		tarkastus.get("pesa_halk_max").setValueLimits(0, 400); // title="suurin halkaisija (cm)"
		tarkastus.get("tyop_tuntia").setValueLimits(0, 80); // title="Henkilätuntia (h)"
		tarkastus.get("tyop_auto_aj").setValueLimits(0, 2000); // title="Autolla ajoa (km)"
		tarkastus.get("tyop_vene_aj").setValueLimits(0, 250); // title="Veneellä ajoa (km)"
		poikanen.get("siipi_pituus").setValueLimits(30, 630);
		poikanen.get("siipi_pituus_2").setValueLimits(30, 630);
		poikanen.get("nilkka_max").setValueLimits(7.0, 22.0);
		poikanen.get("nilkka_min").setValueLimits(0.5, 21.0);
		poikanen.get("nilkka_max_2").setValueLimits(7.0, 22.0);
		poikanen.get("nilkka_min_2").setValueLimits(0.5, 21.0);
		poikanen.get("nokka_pituus").setValueLimits(18.0, 70.0);
		poikanen.get("nokka_pituus_2").setValueLimits(18.0, 70.0);
		poikanen.get("nokka_tyvi").setValueLimits(16.0, 42.0);
		poikanen.get("nokka_tyvi_2").setValueLimits(16.0, 42.0);
		poikanen.get("paino").setValueLimits(200, 6600);
		poikanen.get("paino_2").setValueLimits(200, 6600);
		poikanen.get("munan_pituus").setValueLimits(65.0, 85.0);
		poikanen.get("munan_leveys").setValueLimits(50.0, 70.0);
		
		List<String> orderOfPesaColumns = new ArrayList<>();
		List<String> orderOfOlosuhdeColumns = new ArrayList<>();
		List<String> orderOfTarkastusColumns = new ArrayList<>();
		List<String> orderOfPoikanenColumns = new ArrayList<>();
		List<String> orderOfAikuinenColumns = new ArrayList<>();
		
		orderOfPesaColumns.add("pesa_id");
		orderOfPesaColumns.add("pesanimi");
		orderOfPesaColumns.add("vanha_pesanro");
		orderOfPesaColumns.add("tarkka_sijainti");
		orderOfPesaColumns.add("seur_tarkastaja");
		orderOfPesaColumns.add("yht_leveys");
		orderOfPesaColumns.add("yht_pituus");
		orderOfPesaColumns.add("eur_leveys");
		orderOfPesaColumns.add("eur_pituus");
		orderOfPesaColumns.add("ast_leveys");
		orderOfPesaColumns.add("ast_pituus");
		orderOfPesaColumns.add("des_leveys");
		orderOfPesaColumns.add("des_pituus");
		orderOfPesaColumns.add("koord_mittaus");
		orderOfPesaColumns.add("koord_tyyppi");
		orderOfPesaColumns.add("koord_tark");
		orderOfPesaColumns.add("loyt_vuosi");
		orderOfPesaColumns.add("tuhoutumisvuosi");
		orderOfPesaColumns.add("rak_vuosi");
		orderOfPesaColumns.add("rak_vuosi_tark");
		orderOfPesaColumns.add("rak_vuosi_teko");
		orderOfPesaColumns.add("rak_vuosi_teko_tark");
		orderOfPesaColumns.add("rak_laji");
		orderOfPesaColumns.add("puulaji");
		orderOfPesaColumns.add("et_meri");
		orderOfPesaColumns.add("et_jarvi");
		orderOfPesaColumns.add("r_taulu_nro");
		orderOfPesaColumns.add("r_taulu_kieli");
		orderOfPesaColumns.add("r_taulu_pvm");
		orderOfPesaColumns.add("r_taulu_poisto_pvm");
		orderOfPesaColumns.add("kommentti");
		orderOfPesaColumns.add("uhat");
		orderOfPesaColumns.add("helcom");
		orderOfPesaColumns.add("kirj_pvm");
		orderOfPesaColumns.add("muutos_pvm");
		
		orderOfOlosuhdeColumns.add("havaintojen_vuosi");
		orderOfOlosuhdeColumns.add("pesa_mit_pvm");
		orderOfOlosuhdeColumns.add("ymp_mit_pvm");
		orderOfOlosuhdeColumns.add("sijainti");
		orderOfOlosuhdeColumns.add("elavyys");
		orderOfOlosuhdeColumns.add("korkeus");
		orderOfOlosuhdeColumns.add("korkeus_tark");
		orderOfOlosuhdeColumns.add("tyviymparys");
		orderOfOlosuhdeColumns.add("tyvihalkaisija");
		orderOfOlosuhdeColumns.add("latvaymparys");
		orderOfOlosuhdeColumns.add("latvahalkaisija");
		orderOfOlosuhdeColumns.add("et_maasta");
		orderOfOlosuhdeColumns.add("et_latvasta");
		orderOfOlosuhdeColumns.add("saari_tyyppi");
		orderOfOlosuhdeColumns.add("autoyhteys");
		orderOfOlosuhdeColumns.add("et_avosuo");
		orderOfOlosuhdeColumns.add("et_ilmajohto");
		orderOfOlosuhdeColumns.add("et_viljapelto");
		orderOfOlosuhdeColumns.add("et_avohakkuu");
		orderOfOlosuhdeColumns.add("et_siemenpuusto");
		orderOfOlosuhdeColumns.add("pesan_nakyvyys");
		orderOfOlosuhdeColumns.add("puusto");
		orderOfOlosuhdeColumns.add("puusto_kasittely");
		orderOfOlosuhdeColumns.add("puusto_ika");
		orderOfOlosuhdeColumns.add("maastotyyppi");
		orderOfOlosuhdeColumns.add("palsta_rauh_pvm");
		orderOfOlosuhdeColumns.add("rauh_aika_alku");
		orderOfOlosuhdeColumns.add("rauh_aika_loppu");
		orderOfOlosuhdeColumns.add("palsta_omistaja");
		orderOfOlosuhdeColumns.add("suojelualue");
		orderOfOlosuhdeColumns.add("omist_kommentti");
		orderOfOlosuhdeColumns.add("palsta_rauhoitus");
		orderOfOlosuhdeColumns.add("naturassa");
		orderOfOlosuhdeColumns.add("kirj_pvm");
		orderOfOlosuhdeColumns.add("muutos_pvm");
		orderOfOlosuhdeColumns.add("pesamuuttuva_id");
		orderOfOlosuhdeColumns.add("pesa_id");
		
		orderOfTarkastusColumns.add("tark_vuosi");
		orderOfTarkastusColumns.add("tarkastaja1_id");
		orderOfTarkastusColumns.add("tarkastaja2_id");
		orderOfTarkastusColumns.add("tark_pvm");
		orderOfTarkastusColumns.add("tark_pvm_tark");
		orderOfTarkastusColumns.add("tark_tunti");
		orderOfTarkastusColumns.add("tark_tapa");
		orderOfTarkastusColumns.add("pesimistulos");
		orderOfTarkastusColumns.add("pesimist_tark");
		orderOfTarkastusColumns.add("nahdyt_merkit");
		orderOfTarkastusColumns.add("epaonni_syy");
		orderOfTarkastusColumns.add("epaonni_tark");
		orderOfTarkastusColumns.add("pesa_kunto");
		orderOfTarkastusColumns.add("pesa_kunto_ihmisen_vaik");
		orderOfTarkastusColumns.add("pesa_kunto_aika");
		orderOfTarkastusColumns.add("pesa_merkit");
		orderOfTarkastusColumns.add("munia_lkm");
		orderOfTarkastusColumns.add("munia_pvm");
		orderOfTarkastusColumns.add("elavia_lkm");
		orderOfTarkastusColumns.add("kuoriutumattomia_lkm");
		orderOfTarkastusColumns.add("kuolleita_lkm");
		orderOfTarkastusColumns.add("reng_poik_lkm");
		orderOfTarkastusColumns.add("lentopoik_lkm");
		orderOfTarkastusColumns.add("kuoriutumispaiva");
		orderOfTarkastusColumns.add("muulaji");
		orderOfTarkastusColumns.add("muulaji_vastarakki");
		orderOfTarkastusColumns.add("muulaji_rinnakkainen");
		orderOfTarkastusColumns.add("aikuisia_lkm");
		orderOfTarkastusColumns.add("pesimist_kommentti");
		orderOfTarkastusColumns.add("rengas_kommentti");
		orderOfTarkastusColumns.add("kuollut_kommentti");
		orderOfTarkastusColumns.add("et_tie");
		orderOfTarkastusColumns.add("et_as");
		orderOfTarkastusColumns.add("et_kalaviljely");
		orderOfTarkastusColumns.add("et_moottorikelkka");
		orderOfTarkastusColumns.add("as_lkm_500");
		orderOfTarkastusColumns.add("as_lkm_1000");
		orderOfTarkastusColumns.add("pesa_korkeus");
		orderOfTarkastusColumns.add("pesa_halk_min");
		orderOfTarkastusColumns.add("pesa_halk_max");
		orderOfTarkastusColumns.add("nayte_i");
		orderOfTarkastusColumns.add("nayte_m");
		orderOfTarkastusColumns.add("nayte_s");
		orderOfTarkastusColumns.add("nayte_p");
		orderOfTarkastusColumns.add("nayte_a");
		orderOfTarkastusColumns.add("nayte_r");
		orderOfTarkastusColumns.add("nayte_o");
		orderOfTarkastusColumns.add("kuvia_lintu");
		orderOfTarkastusColumns.add("kuvia_pesa");
		orderOfTarkastusColumns.add("kuvaaja_lintu");
		orderOfTarkastusColumns.add("kuvaaja_pesa");
		orderOfTarkastusColumns.add("tyop_tuntia");
		orderOfTarkastusColumns.add("tyop_auto_aj");
		orderOfTarkastusColumns.add("tyop_vene_aj");
		orderOfTarkastusColumns.add("tyop_kommentti");
		orderOfTarkastusColumns.add("mittaus_pvm_1");
		orderOfTarkastusColumns.add("mittaaja_id_1");
		orderOfTarkastusColumns.add("mittaus_pvm_2");
		orderOfTarkastusColumns.add("mittaaja_id_2");
		orderOfTarkastusColumns.add("p_tarkastus_id");
		orderOfTarkastusColumns.add("pesa_id");
		orderOfTarkastusColumns.add("kirj_pvm");
		orderOfTarkastusColumns.add("muutos_pvm");
		
		orderOfPoikanenColumns.add("rengas_oikea");
		orderOfPoikanenColumns.add("varit_oikea");
		orderOfPoikanenColumns.add("rengas_vasen");
		orderOfPoikanenColumns.add("varit_vasen");
		orderOfPoikanenColumns.add("sukupuoli");
		orderOfPoikanenColumns.add("siipi_pituus");
		orderOfPoikanenColumns.add("siipi_pituus_m");
		orderOfPoikanenColumns.add("poikasen_ika");
		orderOfPoikanenColumns.add("siipi_pituus_2");
		orderOfPoikanenColumns.add("siipi_pituus_m_2");
		orderOfPoikanenColumns.add("poikasen_ika_2");
		orderOfPoikanenColumns.add("nilkka_max");
		orderOfPoikanenColumns.add("nilkka_min");
		orderOfPoikanenColumns.add("nilkka_max_2");
		orderOfPoikanenColumns.add("nilkka_min_2");
		orderOfPoikanenColumns.add("nokka_pituus");
		orderOfPoikanenColumns.add("nokka_tyvi");
		orderOfPoikanenColumns.add("nokka_pituus_2");
		orderOfPoikanenColumns.add("nokka_tyvi_2");
		orderOfPoikanenColumns.add("paino");
		orderOfPoikanenColumns.add("paino_2");
		orderOfPoikanenColumns.add("kupu");
		orderOfPoikanenColumns.add("kupu_2");
		orderOfPoikanenColumns.add("hoyhennayte");
		orderOfPoikanenColumns.add("hoyhennayte_2");
		orderOfPoikanenColumns.add("verinayte");
		orderOfPoikanenColumns.add("verinayte_2");
		orderOfPoikanenColumns.add("munan_pituus");
		orderOfPoikanenColumns.add("munan_leveys");
		orderOfPoikanenColumns.add("kommentti");
		orderOfPoikanenColumns.add("kommentti_2");
		orderOfPoikanenColumns.add("poikanen_id");
		orderOfPoikanenColumns.add("tarkastus_id");
		orderOfPoikanenColumns.add("kirj_pvm");
		orderOfPoikanenColumns.add("muutos_pvm");
		
		orderOfAikuinenColumns.add("sukupuoli");
		orderOfAikuinenColumns.add("rengas_oikea");
		orderOfAikuinenColumns.add("varit_oikea");
		orderOfAikuinenColumns.add("rengas_vasen");
		orderOfAikuinenColumns.add("varit_vasen");
		orderOfAikuinenColumns.add("aikuinen_id");
		orderOfAikuinenColumns.add("tarkastus_id");
		orderOfAikuinenColumns.add("kirj_pvm");
		orderOfAikuinenColumns.add("muutos_pvm");
		
		pesa.reorder(orderOfPesaColumns);
		olosuhde.reorder(orderOfOlosuhdeColumns);
		tarkastus.reorder(orderOfTarkastusColumns);
		poikanen.reorder(orderOfPoikanenColumns);
		aikuinen.reorder(orderOfAikuinenColumns);
		
		return structure;
	}
	
	// ---------------------------------- ---------------------------------- ---------------------------------- ---------------------------------- ----------------------------------
	
	public static PesaDomainModelDatabaseStructure hardCoded() {
		PesaDomainModelDatabaseStructure structure = new PesaDomainModelDatabaseStructure();
		
		// Taulut
		DatabaseTableStructure pesavakio = new DatabaseTableStructure("pesavakio");
		pesavakio.addColumn("pesa_id", 2, 7);
		pesavakio.addColumn("seur_tarkastaja", 2, 5);
		pesavakio.addColumn("kirj_pvm", 4);
		pesavakio.addColumn("muutos_pvm", 4);
		pesavakio.addColumn("pesanimi", 1, 30);
		pesavakio.addColumn("vanha_pesanro", 1, 35);
		pesavakio.addColumn("tarkka_sijainti", 1, 100);
		pesavakio.addColumn("koord_mittaus", 1, 1);
		pesavakio.addColumn("koord_tyyppi", 1, 1);
		pesavakio.addColumn("koord_tark", 1, 1);
		pesavakio.addColumn("yht_leveys", 2, 7);
		pesavakio.addColumn("yht_pituus", 2, 7);
		pesavakio.addColumn("ast_leveys", 2, 6);
		pesavakio.addColumn("ast_pituus", 2, 6);
		pesavakio.addColumn("des_leveys", 3, 10, 7);
		pesavakio.addColumn("des_pituus", 3, 10, 7);
		pesavakio.addColumn("et_meri", 2, 4);
		pesavakio.addColumn("et_jarvi", 2, 4);
		pesavakio.addColumn("rak_vuosi", 2, 4);
		pesavakio.addColumn("rak_vuosi_tark", 2, 1);
		pesavakio.addColumn("rak_laji", 1, 7);
		pesavakio.addColumn("loyt_vuosi", 2, 4);
		pesavakio.addColumn("tuhoutumisvuosi", 2, 4);
		pesavakio.addColumn("puulaji", 1, 1);
		pesavakio.addColumn("r_taulu_nro", 2, 5);
		pesavakio.addColumn("r_taulu_kieli", 1, 3);
		pesavakio.addColumn("r_taulu_pvm", 4);
		pesavakio.addColumn("r_taulu_poisto_pvm", 4);
		pesavakio.addColumn("kommentti", 1, 256);
		pesavakio.addColumn("rak_vuosi_teko", 2, 4);
		pesavakio.addColumn("rak_vuosi_teko_tark", 2, 1);
		pesavakio.addColumn("eur_leveys", 2, 7);
		pesavakio.addColumn("eur_pituus", 2, 7);
		pesavakio.addColumn("helcom", 1, 1);
		pesavakio.addColumn("uhat", 1, 256);
		DatabaseTableStructure pesamuuttuva = new DatabaseTableStructure("pesamuuttuva");
		pesamuuttuva.addColumn("pesamuuttuva_id", 2, 7);
		pesamuuttuva.addColumn("pesa_id", 2, 7);
		pesamuuttuva.addColumn("kirj_pvm", 4);
		pesamuuttuva.addColumn("muutos_pvm", 4);
		pesamuuttuva.addColumn("elavyys", 1, 1);
		pesamuuttuva.addColumn("korkeus_tark", 1, 1);
		pesamuuttuva.addColumn("korkeus", 2, 2);
		pesamuuttuva.addColumn("maastotyyppi", 1, 1);
		pesamuuttuva.addColumn("saari_tyyppi", 1, 1);
		pesamuuttuva.addColumn("puusto", 1, 1);
		pesamuuttuva.addColumn("puusto_kasittely", 1, 1);
		pesamuuttuva.addColumn("puusto_ika", 1, 1);
		pesamuuttuva.addColumn("et_avosuo", 2, 4);
		pesamuuttuva.addColumn("et_viljapelto", 2, 4);
		pesamuuttuva.addColumn("et_ilmajohto", 2, 4);
		pesamuuttuva.addColumn("et_avohakkuu", 2, 4);
		pesamuuttuva.addColumn("palsta_rauh_pvm", 4);
		pesamuuttuva.addColumn("rauh_aika_alku", 4);
		pesamuuttuva.addColumn("rauh_aika_loppu", 4);
		pesamuuttuva.addColumn("palsta_omistaja", 1, 1);
		pesamuuttuva.addColumn("suojelualue", 1, 100);
		pesamuuttuva.addColumn("omist_kommentti", 1, 256);
		pesamuuttuva.addColumn("palsta_rauhoitus", 1, 1);
		pesamuuttuva.addColumn("sijainti", 1, 1);
		pesamuuttuva.addColumn("pesa_mit_pvm", 4);
		pesamuuttuva.addColumn("ymp_mit_pvm", 4);
		pesamuuttuva.addColumn("autoyhteys", 1, 1);
		pesamuuttuva.addColumn("pesan_nakyvyys", 1, 1);
		pesamuuttuva.addColumn("et_maasta", 2, 2);
		pesamuuttuva.addColumn("et_latvasta", 2, 2);
		pesamuuttuva.addColumn("tyvihalkaisija", 2, 3);
		pesamuuttuva.addColumn("tyviymparys", 2, 3);
		pesamuuttuva.addColumn("latvahalkaisija", 2, 2);
		pesamuuttuva.addColumn("latvaymparys", 2, 3);
		pesamuuttuva.addColumn("manty_lkm_p", 2, 2);
		pesamuuttuva.addColumn("manty_pit_p", 2, 2);
		pesamuuttuva.addColumn("manty_lkm_i", 2, 2);
		pesamuuttuva.addColumn("manty_pit_i", 2, 2);
		pesamuuttuva.addColumn("manty_lkm_l", 2, 2);
		pesamuuttuva.addColumn("manty_pit_l", 2, 2);
		pesamuuttuva.addColumn("manty_lkm_e", 2, 2);
		pesamuuttuva.addColumn("manty_pit_e", 2, 2);
		pesamuuttuva.addColumn("kuusi_lkm_p", 2, 2);
		pesamuuttuva.addColumn("kuusi_pit_p", 2, 2);
		pesamuuttuva.addColumn("kuusi_lkm_i", 2, 2);
		pesamuuttuva.addColumn("kuusi_pit_i", 2, 2);
		pesamuuttuva.addColumn("kuusi_lkm_l", 2, 2);
		pesamuuttuva.addColumn("kuusi_pit_l", 2, 2);
		pesamuuttuva.addColumn("kuusi_lkm_e", 2, 2);
		pesamuuttuva.addColumn("kuusi_pit_e", 2, 2);
		pesamuuttuva.addColumn("muu_lkm_p", 2, 2);
		pesamuuttuva.addColumn("muu_pit_p", 2, 2);
		pesamuuttuva.addColumn("muu_lkm_i", 2, 2);
		pesamuuttuva.addColumn("muu_pit_i", 2, 2);
		pesamuuttuva.addColumn("muu_lkm_l", 2, 2);
		pesamuuttuva.addColumn("muu_pit_l", 2, 2);
		pesamuuttuva.addColumn("muu_lkm_e", 2, 2);
		pesamuuttuva.addColumn("muu_pit_e", 2, 2);
		pesamuuttuva.addColumn("naturassa", 1, 1);
		pesamuuttuva.addColumn("et_siemenpuusto", 2, 4);
		pesamuuttuva.addColumn("relaskoop_pvm", 4);
		pesamuuttuva.addColumn("havaintojen_vuosi", 2, 4);
		DatabaseTableStructure pesatarkastus = new DatabaseTableStructure("pesatarkastus");
		pesatarkastus.addColumn("p_tarkastus_id", 2, 7);
		pesatarkastus.addColumn("pesa_id", 2, 7);
		pesatarkastus.addColumn("tark_vuosi", 2, 4);
		pesatarkastus.addColumn("tarkastaja1_id", 2, 5);
		pesatarkastus.addColumn("tarkastaja2_id", 2, 5);
		pesatarkastus.addColumn("tark_pvm", 4);
		pesatarkastus.addColumn("kirj_pvm", 4);
		pesatarkastus.addColumn("muutos_pvm", 4);
		pesatarkastus.addColumn("tark_pvm_tark", 1, 1);
		pesatarkastus.addColumn("tark_tunti", 2, 2);
		pesatarkastus.addColumn("as_lkm_1000", 2, 3);
		pesatarkastus.addColumn("as_lkm_500", 2, 3);
		pesatarkastus.addColumn("nahdyt_merkit", 1, 1);
		pesatarkastus.addColumn("et_as", 2, 4);
		pesatarkastus.addColumn("et_tie", 2, 4);
		pesatarkastus.addColumn("et_kalaviljely", 2, 4);
		pesatarkastus.addColumn("et_moottorikelkka", 2, 4);
		pesatarkastus.addColumn("tark_tapa", 2, 1);
		pesatarkastus.addColumn("pesa_kunto", 1, 1);
		pesatarkastus.addColumn("pesa_kunto_aika", 1, 1);
		pesatarkastus.addColumn("pesa_kunto_ihmisen_vaik", 1, 1);
		pesatarkastus.addColumn("pesa_merkit", 1, 1);
		pesatarkastus.addColumn("aikuisia_lkm", 2, 1);
		pesatarkastus.addColumn("munia_lkm", 2, 1);
		pesatarkastus.addColumn("munia_pvm", 4);
		pesatarkastus.addColumn("kuoriutumattomia_lkm", 2, 1);
		pesatarkastus.addColumn("elavia_lkm", 2, 1);
		pesatarkastus.addColumn("kuolleita_lkm", 2, 1);
		pesatarkastus.addColumn("lentopoik_lkm", 2, 1);
		pesatarkastus.addColumn("reng_poik_lkm", 2, 1);
		pesatarkastus.addColumn("pesimistulos", 1, 1);
		pesatarkastus.addColumn("pesimist_tark", 1, 1);
		pesatarkastus.addColumn("epaonni_tark", 1, 1);
		pesatarkastus.addColumn("epaonni_syy", 1, 1);
		pesatarkastus.addColumn("muulaji", 1, 7);
		pesatarkastus.addColumn("muulaji_vastarakki", 1, 7);
		pesatarkastus.addColumn("muulaji_rinnakkainen", 1, 7);
		pesatarkastus.addColumn("pesa_korkeus", 2, 3);
		pesatarkastus.addColumn("pesa_halk_min", 2, 3);
		pesatarkastus.addColumn("pesa_halk_max", 2, 3);
		pesatarkastus.addColumn("uhat", 1, 256);
		pesatarkastus.addColumn("kuoriutumispaiva", 2, 3);
		pesatarkastus.addColumn("nayte_i", 1, 1);
		pesatarkastus.addColumn("nayte_m", 2, 1);
		pesatarkastus.addColumn("nayte_s", 1, 1);
		pesatarkastus.addColumn("nayte_p", 2, 1);
		pesatarkastus.addColumn("nayte_a", 2, 1);
		pesatarkastus.addColumn("nayte_r", 2, 1);
		pesatarkastus.addColumn("nayte_o", 1, 1);
		pesatarkastus.addColumn("pesa_kommentti", 1, 256);
		pesatarkastus.addColumn("pesimist_kommentti", 1, 256);
		pesatarkastus.addColumn("tyop_tuntia", 2, 3);
		pesatarkastus.addColumn("tyop_auto_aj", 2, 4);
		pesatarkastus.addColumn("tyop_vene_aj", 2, 3);
		pesatarkastus.addColumn("tyop_kommentti", 1, 256);
		pesatarkastus.addColumn("rengas_kommentti", 1, 256);
		pesatarkastus.addColumn("kuollut_kommentti", 1, 256);
		pesatarkastus.addColumn("mittaus_pvm_1", 4);
		pesatarkastus.addColumn("mittaaja_id_1", 2, 5);
		pesatarkastus.addColumn("mittaus_pvm_2", 4);
		pesatarkastus.addColumn("mittaaja_id_2", 2, 5);
		pesatarkastus.addColumn("kuvia_lintu", 1, 1);
		pesatarkastus.addColumn("kuvia_pesa", 1, 1);
		pesatarkastus.addColumn("kuvaaja_lintu", 2, 5);
		pesatarkastus.addColumn("kuvaaja_pesa", 2, 5);
		
		DatabaseTableStructure vuosi = new DatabaseTableStructure("vuosi");
		vuosi.addColumn("vuosi", 2, 4);
		vuosi.addColumn("pesa_id", 2, 7);
		vuosi.addColumn("reviiri_id", 2, 7);
		vuosi.addColumn("reviirin_kunta", 1, 6);
		vuosi.addColumn("pesan_kunta", 1, 6);
		vuosi.addColumn("kirj_pvm", 4);
		vuosi.addColumn("muutos_pvm", 4);
		DatabaseTableStructure reviiri = new DatabaseTableStructure("reviiri");
		reviiri.addColumn("reviiri_id", 2, 7);
		reviiri.addColumn("kirj_pvm", 4);
		reviiri.addColumn("muutos_pvm", 4);
		reviiri.addColumn("reviirinimi", 1, 25);
		reviiri.addColumn("vanha_reviirinro", 1, 35);
		reviiri.addColumn("kommentti", 1, 256);
		DatabaseTableStructure aputaulu = new DatabaseTableStructure("aputaulu");
		aputaulu.addColumn("taulu", 1, 15);
		aputaulu.addColumn("attribuutti", 1, 20);
		aputaulu.addColumn("jarjestys", 2, 3);
		aputaulu.addColumn("arvo", 1, 10);
		aputaulu.addColumn("selite", 1, 256);
		DatabaseTableStructure poikanen = new DatabaseTableStructure("poikanen");
		poikanen.addColumn("poikanen_id", 2, 7);
		poikanen.addColumn("tarkastus_id", 2, 7);
		poikanen.addColumn("mittaaja_id", 2, 5);
		poikanen.addColumn("mittaus_pvm", 4);
		poikanen.addColumn("kirj_pvm", 4);
		poikanen.addColumn("muutos_pvm", 4);
		poikanen.addColumn("rengas_oikea", 1, 9);
		poikanen.addColumn("rengas_vasen", 1, 9);
		poikanen.addColumn("varit_oikea", 1, 4);
		poikanen.addColumn("varit_vasen", 1, 4);
		poikanen.addColumn("sukupuoli", 1, 1);
		poikanen.addColumn("siipi_pituus", 2, 3);
		poikanen.addColumn("siipi_pituus_m", 1, 1);
		poikanen.addColumn("nilkka_min", 3, 4, 1);
		poikanen.addColumn("nilkka_max", 3, 4, 1);
		poikanen.addColumn("nokka_pituus", 3, 4, 1);
		poikanen.addColumn("nokka_tyvi", 3, 4, 1);
		poikanen.addColumn("paino", 2, 4);
		poikanen.addColumn("kupu", 2, 1);
		poikanen.addColumn("poikasen_ika", 2, 3);
		poikanen.addColumn("kommentti", 1, 256);
		poikanen.addColumn("mittaaja_id_2", 2, 5);
		poikanen.addColumn("mittaus_pvm_2", 4);
		poikanen.addColumn("siipi_pituus_2", 2, 3);
		poikanen.addColumn("siipi_pituus_m_2", 1, 1);
		poikanen.addColumn("nilkka_min_2", 3, 4, 1);
		poikanen.addColumn("nilkka_max_2", 3, 4, 1);
		poikanen.addColumn("nokka_pituus_2", 3, 4, 1);
		poikanen.addColumn("nokka_tyvi_2", 3, 4, 1);
		poikanen.addColumn("paino_2", 2, 4);
		poikanen.addColumn("kupu_2", 2, 1);
		poikanen.addColumn("poikasen_ika_2", 2, 3);
		poikanen.addColumn("kommentti_2", 1, 256);
		poikanen.addColumn("hoyhennayte", 1, 1);
		poikanen.addColumn("hoyhennayte_2", 1, 1);
		poikanen.addColumn("verinayte", 1, 1);
		poikanen.addColumn("verinayte_2", 1, 1);
		poikanen.addColumn("munan_pituus", 3, 6, 3);
		poikanen.addColumn("munan_leveys", 3, 6, 3);
		
		DatabaseTableStructure aikuinen = new DatabaseTableStructure("aikuinen");
		aikuinen.addColumn("aikuinen_id", 2, 7);
		aikuinen.addColumn("tarkastus_id", 2, 7);
		aikuinen.addColumn("kirj_pvm", 4);
		aikuinen.addColumn("muutos_pvm", 4);
		aikuinen.addColumn("rengas_oikea", 1, 9);
		aikuinen.addColumn("rengas_vasen", 1, 9);
		aikuinen.addColumn("varit_oikea", 1, 4);
		aikuinen.addColumn("varit_vasen", 1, 4);
		aikuinen.addColumn("sukupuoli", 1, 1);
		
		DatabaseTableStructure searches = new DatabaseTableStructure("saved_searches");
		searches.addColumn("id", _Column.INTEGER, 5);
		searches.addColumn("userid", _Column.VARCHAR, 25);
		searches.addColumn("last_used", _Column.DATE);
		searches.addColumn("name", _Column.VARCHAR, 150);
		searches.addColumn("description", _Column.VARCHAR, 500);
		searches.addColumn("selected_fields", _Column.VARCHAR, 2000);
		searches.addColumn("search_values", _Column.VARCHAR, 4000);
		searches.get("id").setTypeToUniqueNumericIncreasingId();
		structure.setSavedSearches(searches);
		
		DatabaseTableStructure userLoginInfo = new DatabaseTableStructure("kayttaja");
		userLoginInfo.addColumn("username", _Column.VARCHAR, 25);
		userLoginInfo.addColumn("password", _Column.VARCHAR, 256);
		userLoginInfo.get("username").setTypeToImmutablePartOfAKey();
		structure.defineCustomTable(userLoginInfo);
		
		// N kpl
		structure.setPoikasetCount(4);
		structure.setAikuisetCount(3);
		
		// Taulut templateen
		structure.setPesa(pesavakio);
		structure.setOlosuhde(pesamuuttuva);
		structure.setTarkastus(pesatarkastus);
		structure.setVuosi(vuosi);
		structure.setReviiri(reviiri);
		structure.setAputaulu(aputaulu);
		structure.setPoikanen(poikanen);
		structure.setAikuinen(aikuinen);
		
		// Avaimet
		pesavakio.get("pesa_id").setTypeToUniqueNumericIncreasingId();
		pesamuuttuva.get("pesamuuttuva_id").setTypeToUniqueNumericIncreasingId();
		pesatarkastus.get("p_tarkastus_id").setTypeToUniqueNumericIncreasingId();
		poikanen.get("poikanen_id").setTypeToUniqueNumericIncreasingId();
		aikuinen.get("aikuinen_id").setTypeToUniqueNumericIncreasingId();
		vuosi.get("vuosi").setTypeToImmutablePartOfAKey();
		vuosi.get("pesa_id").setTypeToImmutablePartOfAKey();
		reviiri.get("reviiri_id").setTypeToUniqueNumericIncreasingId();
		aputaulu.get("taulu").setTypeToImmutablePartOfAKey();
		aputaulu.get("attribuutti").setTypeToImmutablePartOfAKey();
		aputaulu.get("arvo").setTypeToImmutablePartOfAKey();
		
		// Viiteavaimet... Ei kattava lista (toistaiseksi), vain ne joita tarvitaan
		pesatarkastus.get("pesa_id").references(pesavakio.getUniqueNumericIdColumn());
		vuosi.get("reviiri_id").references(reviiri.get("reviiri_id"));
		
		// Lisäyspäivämääräkentät
		pesavakio.get("kirj_pvm").setTypeToDateAddedColumn();
		pesamuuttuva.get("kirj_pvm").setTypeToDateAddedColumn();
		pesatarkastus.get("kirj_pvm").setTypeToDateAddedColumn();
		poikanen.get("kirj_pvm").setTypeToDateAddedColumn();
		aikuinen.get("kirj_pvm").setTypeToDateAddedColumn();
		reviiri.get("kirj_pvm").setTypeToDateAddedColumn();
		vuosi.get("kirj_pvm").setTypeToDateAddedColumn();
		
		// Muokkauspäivämääräkentät
		pesavakio.get("muutos_pvm").setTypeToDateModifiedColumn();
		pesamuuttuva.get("muutos_pvm").setTypeToDateModifiedColumn();
		pesatarkastus.get("muutos_pvm").setTypeToDateModifiedColumn();
		poikanen.get("muutos_pvm").setTypeToDateModifiedColumn();
		aikuinen.get("muutos_pvm").setTypeToDateModifiedColumn();
		reviiri.get("muutos_pvm").setTypeToDateModifiedColumn();
		vuosi.get("muutos_pvm").setTypeToDateModifiedColumn();
		
		// Tauluhallintaoikeudet
		pesavakio.setTablemanagementPermissions(_Table.TABLEMANAGEMENT_NO_PERMISSIONS);
		pesamuuttuva.setTablemanagementPermissions(_Table.TABLEMANAGEMENT_NO_PERMISSIONS);
		pesatarkastus.setTablemanagementPermissions(_Table.TABLEMANAGEMENT_NO_PERMISSIONS);
		poikanen.setTablemanagementPermissions(_Table.TABLEMANAGEMENT_NO_PERMISSIONS);
		aikuinen.setTablemanagementPermissions(_Table.TABLEMANAGEMENT_NO_PERMISSIONS);
		vuosi.setTablemanagementPermissions(_Table.TABLEMANAGEMENT_NO_PERMISSIONS);
		reviiri.setTablemanagementPermissions(_Table.TABLEMANAGEMENT_UPDATE_INSERT_DELETE);
		aputaulu.setTablemanagementPermissions(_Table.TABLEMANAGEMENT_UPDATE_INSERT);
		userLoginInfo.setTablemanagementPermissions(_Table.TABLEMANAGEMENT_UPDATE_INSERT_DELETE);
		
		// Liitokset selecteihin
		pesavakio.get("seur_tarkastaja").setAssosiatedSelection(TipuAPIClient.RINGERS);
		pesavakio.get("koord_mittaus").setAssosiatedSelection("pesavakio.koord_mittaus");
		pesavakio.get("koord_tyyppi").setAssosiatedSelection("pesavakio.koord_tyyppi");
		pesavakio.get("koord_tark").setAssosiatedSelection("pesavakio.koord_tark");
		pesavakio.get("rak_vuosi_tark").setAssosiatedSelection("pesavakio.rak_vuosi_tark");
		pesavakio.get("rak_laji").setAssosiatedSelection(TipuAPIClient.SPECIES);
		pesavakio.get("puulaji").setAssosiatedSelection("pesavakio.puulaji");
		pesavakio.get("r_taulu_kieli").setAssosiatedSelection("pesavakio.r_taulu_kieli");
		pesatarkastus.get("kuvia_lintu").setAssosiatedSelection(MerikotkaConst.YES_NO_SELECTION);
		pesatarkastus.get("kuvia_pesa").setAssosiatedSelection(MerikotkaConst.YES_NO_SELECTION);
		pesatarkastus.get("kuvaaja_pesa").setAssosiatedSelection(TipuAPIClient.RINGERS);
		pesatarkastus.get("kuvaaja_lintu").setAssosiatedSelection(TipuAPIClient.RINGERS);
		pesavakio.get("rak_vuosi_teko_tark").setAssosiatedSelection("pesavakio.rak_vuosi_teko_tark");
		pesamuuttuva.get("elavyys").setAssosiatedSelection("pesamuuttuva.elavyys");
		pesamuuttuva.get("korkeus_tark").setAssosiatedSelection("pesamuuttuva.korkeus_tark");
		pesamuuttuva.get("maastotyyppi").setAssosiatedSelection("pesamuuttuva.maastotyyppi");
		pesamuuttuva.get("saari_tyyppi").setAssosiatedSelection("pesamuuttuva.saari_tyyppi");
		pesamuuttuva.get("puusto").setAssosiatedSelection("pesamuuttuva.puusto");
		pesamuuttuva.get("puusto_kasittely").setAssosiatedSelection("pesamuuttuva.puusto_kasittely");
		pesamuuttuva.get("puusto_ika").setAssosiatedSelection("pesamuuttuva.puusto_ika");
		pesamuuttuva.get("palsta_omistaja").setAssosiatedSelection("pesamuuttuva.palsta_omistaja");
		pesamuuttuva.get("palsta_rauhoitus").setAssosiatedSelection("pesamuuttuva.palsta_rauhoitus");
		pesamuuttuva.get("sijainti").setAssosiatedSelection("pesamuuttuva.sijainti");
		pesamuuttuva.get("autoyhteys").setAssosiatedSelection(MerikotkaConst.YES_NO_SELECTION);
		pesamuuttuva.get("pesan_nakyvyys").setAssosiatedSelection("pesamuuttuva.pesan_nakyvyys");
		pesamuuttuva.get("naturassa").setAssosiatedSelection(MerikotkaConst.YES_NO_SELECTION);
		pesatarkastus.get("tarkastaja1_id").setAssosiatedSelection(TipuAPIClient.RINGERS);
		pesatarkastus.get("tarkastaja2_id").setAssosiatedSelection(TipuAPIClient.RINGERS);
		pesatarkastus.get("tark_pvm_tark").setAssosiatedSelection("pesatarkastus.tark_pvm_tark");
		pesatarkastus.get("nahdyt_merkit").setAssosiatedSelection("pesatarkastus.nahdyt_merkit");
		pesatarkastus.get("tark_tapa").setAssosiatedSelection("pesatarkastus.tark_tapa");
		pesatarkastus.get("pesa_kunto").setAssosiatedSelection("pesatarkastus.pesa_kunto");
		pesatarkastus.get("pesa_kunto_ihmisen_vaik").setAssosiatedSelection("pesatarkastus.pesa_kunto_aika");
		pesatarkastus.get("pesa_kunto_aika").setAssosiatedSelection("pesatarkastus.pesa_kunto_ihmisen_vaik");
		pesatarkastus.get("pesa_merkit").setAssosiatedSelection("pesatarkastus.pesa_merkit");
		pesatarkastus.get("aikuisia_lkm").setAssosiatedSelection("pesatarkastus.aikuisia_lkm");
		pesatarkastus.get("elavia_lkm").setAssosiatedSelection("pesatarkastus.elavia_lkm");
		pesatarkastus.get("pesimistulos").setAssosiatedSelection("pesatarkastus.pesimistulos");
		pesatarkastus.get("pesimist_tark").setAssosiatedSelection("pesatarkastus.pesimist_tark");
		pesatarkastus.get("epaonni_tark").setAssosiatedSelection("pesatarkastus.epaonni_tark");
		pesatarkastus.get("epaonni_syy").setAssosiatedSelection("pesatarkastus.epaonni_syy");
		pesatarkastus.get("muulaji").setAssosiatedSelection(TipuAPIClient.SPECIES);
		pesatarkastus.get("muulaji_rinnakkainen").setAssosiatedSelection(TipuAPIClient.SPECIES);
		pesatarkastus.get("muulaji_vastarakki").setAssosiatedSelection(MerikotkaConst.YES_NO_SELECTION);
		pesatarkastus.get("nayte_i").setAssosiatedSelection(MerikotkaConst.YES_NO_SELECTION);
		pesatarkastus.get("nayte_s").setAssosiatedSelection(MerikotkaConst.YES_NO_SELECTION);
		pesatarkastus.get("nayte_o").setAssosiatedSelection(MerikotkaConst.YES_NO_SELECTION);
		pesatarkastus.get("mittaaja_id_1").setAssosiatedSelection(TipuAPIClient.RINGERS);
		pesatarkastus.get("mittaaja_id_2").setAssosiatedSelection(TipuAPIClient.RINGERS);
		vuosi.get("reviiri_id").setAssosiatedSelection("reviirit");
		vuosi.get("pesan_kunta").setAssosiatedSelection(TipuAPIClient.MUNICIPALITIES);
		vuosi.get("reviirin_kunta").setAssosiatedSelection(TipuAPIClient.MUNICIPALITIES);
		aputaulu.get("taulu").setAssosiatedSelection("taulut");
		poikanen.get("sukupuoli").setAssosiatedSelection("poikanen.sukupuoli");
		poikanen.get("siipi_pituus_m").setAssosiatedSelection("poikanen.siipi_pituus_m");
		poikanen.get("kupu").setAssosiatedSelection("poikanen.kupu");
		poikanen.get("siipi_pituus_m_2").setAssosiatedSelection("poikanen.siipi_pituus_m");
		poikanen.get("kupu_2").setAssosiatedSelection("poikanen.kupu");
		poikanen.get("hoyhennayte").setAssosiatedSelection(MerikotkaConst.YES_NO_SELECTION);
		poikanen.get("hoyhennayte_2").setAssosiatedSelection(MerikotkaConst.YES_NO_SELECTION);
		poikanen.get("verinayte").setAssosiatedSelection(MerikotkaConst.YES_NO_SELECTION);
		poikanen.get("verinayte_2").setAssosiatedSelection(MerikotkaConst.YES_NO_SELECTION);
		poikanen.get("varit_oikea").setAssosiatedSelection("pesatarkastus.rengas_vari");
		poikanen.get("varit_vasen").setAssosiatedSelection("pesatarkastus.rengas_vari");
		
		// Specially defined uppercase columns. All columns that have an assosiated selection (set above) are touppercase by default.
		poikanen.get("rengas_oikea").setToUppercaseColumn();
		poikanen.get("rengas_vasen").setToUppercaseColumn();
		aikuinen.get("rengas_oikea").setToUppercaseColumn();
		aikuinen.get("rengas_vasen").setToUppercaseColumn();
		
		// Misc
		userLoginInfo.get("password").setTypeToPasswordColumn();
		reviiri.get("vanha_reviirinro").setToOptionalColumn();
		
		// Min/max values
		vuosi.get("vuosi").setValueLimits(_Validator.MIN_YEAR, _Validator.MAX_YEAR);
		pesavakio.get("loyt_vuosi").setValueLimits(_Validator.MIN_YEAR, _Validator.MAX_YEAR); // title="Löytövuosi"
		pesavakio.get("tuhoutumisvuosi").setValueLimits(_Validator.MIN_YEAR, _Validator.MAX_YEAR); // title="Tuhoutumisvuosi"
		pesavakio.get("rak_vuosi").setValueLimits(_Validator.MIN_YEAR, _Validator.MAX_YEAR); // title="Rakentamisvuosi"
		pesavakio.get("rak_vuosi_teko").setValueLimits(_Validator.MIN_YEAR, _Validator.MAX_YEAR); // title="Rakentamisvuosi"
		pesamuuttuva.get("korkeus").setValueLimits(0, 50); // title="Puun korkeus (m)"
		pesamuuttuva.get("tyviymparys").setValueLimits(30, 350); // title="ympärys (cm)"
		pesamuuttuva.get("tyvihalkaisija").setValueLimits(6, 120); // title="halkaisija (cm)"
		pesamuuttuva.get("latvaymparys").setValueLimits(6, 190); // title="ympärys (cm)"
		pesamuuttuva.get("latvahalkaisija").setValueLimits(4, 60); // title="halkaisija (cm)"
		pesamuuttuva.get("et_maasta").setValueLimits(0, 60); // title="yläpinnan etäisyys maasta (m)"
		pesamuuttuva.get("et_latvasta").setValueLimits(0, 20); // title="yläpinnan etäisyys latvasta (m)"
		pesamuuttuva.get("manty_lkm_p").setValueLimits(0, 80);
		pesamuuttuva.get("manty_pit_p").setValueLimits(0, 80);
		pesamuuttuva.get("manty_lkm_i").setValueLimits(0, 80);
		pesamuuttuva.get("manty_pit_i").setValueLimits(0, 80);
		pesamuuttuva.get("manty_lkm_l").setValueLimits(0, 80);
		pesamuuttuva.get("manty_pit_l").setValueLimits(0, 80);
		pesamuuttuva.get("manty_lkm_e").setValueLimits(0, 80);
		pesamuuttuva.get("manty_pit_e").setValueLimits(0, 80);
		pesamuuttuva.get("kuusi_lkm_p").setValueLimits(0, 80);
		pesamuuttuva.get("kuusi_pit_p").setValueLimits(0, 80);
		pesamuuttuva.get("kuusi_lkm_i").setValueLimits(0, 80);
		pesamuuttuva.get("kuusi_pit_i").setValueLimits(0, 80);
		pesamuuttuva.get("kuusi_lkm_l").setValueLimits(0, 80);
		pesamuuttuva.get("kuusi_pit_l").setValueLimits(0, 80);
		pesamuuttuva.get("kuusi_lkm_e").setValueLimits(0, 80);
		pesamuuttuva.get("kuusi_pit_e").setValueLimits(0, 80);
		pesamuuttuva.get("muu_lkm_p").setValueLimits(0, 80);
		pesamuuttuva.get("muu_pit_p").setValueLimits(0, 80);
		pesamuuttuva.get("muu_lkm_i").setValueLimits(0, 80);
		pesamuuttuva.get("muu_pit_i").setValueLimits(0, 80);
		pesamuuttuva.get("muu_lkm_l").setValueLimits(0, 80);
		pesamuuttuva.get("muu_pit_l").setValueLimits(0, 80);
		pesamuuttuva.get("muu_lkm_e").setValueLimits(0, 80);
		pesamuuttuva.get("muu_pit_e").setValueLimits(0, 80);
		pesatarkastus.get("tark_vuosi").setValueLimits(_Validator.MIN_YEAR, _Validator.MAX_YEAR); // title="Pesintävuosi"
		pesatarkastus.get("munia_lkm").setValueLimits(0, 4); // title="Lopullinen munamäärä"
		pesatarkastus.get("kuoriutumattomia_lkm").setValueLimits(0, 4); // title="Kuoriutumattomat munat"
		pesatarkastus.get("kuolleita_lkm").setValueLimits(0, 4); // title="Kuolleet poikaset"
		pesatarkastus.get("reng_poik_lkm").setValueLimits(0, 4); // title="Rengastusikäiset poikaset"
		pesatarkastus.get("lentopoik_lkm").setValueLimits(0, 4); // title="Lentopoikaset"
		pesatarkastus.get("as_lkm_500").setValueLimits(0, 30); // title="500m säteellä"
		pesatarkastus.get("as_lkm_1000").setValueLimits(0, 200); // title="1000m säteellä"
		pesatarkastus.get("pesa_korkeus").setValueLimits(0, 450); // title="korkeus (cm)"
		pesatarkastus.get("pesa_halk_min").setValueLimits(0, 300); // title="pienin halkaisija (cm)"
		pesatarkastus.get("pesa_halk_max").setValueLimits(0, 400); // title="suurin halkaisija (cm)"
		pesatarkastus.get("tyop_tuntia").setValueLimits(0, 80); // title="Henkilätuntia (h)"
		pesatarkastus.get("tyop_auto_aj").setValueLimits(0, 2000); // title="Autolla ajoa (km)"
		pesatarkastus.get("tyop_vene_aj").setValueLimits(0, 250); // title="Veneellä ajoa (km)"
		poikanen.get("siipi_pituus").setValueLimits(30, 630);
		poikanen.get("siipi_pituus_2").setValueLimits(30, 630);
		poikanen.get("nilkka_max").setValueLimits(7.0, 22.0);
		poikanen.get("nilkka_min").setValueLimits(0.5, 21.0);
		poikanen.get("nilkka_max_2").setValueLimits(7.0, 22.0);
		poikanen.get("nilkka_min_2").setValueLimits(0.5, 21.0);
		poikanen.get("nokka_pituus").setValueLimits(18.0, 70.0);
		poikanen.get("nokka_pituus_2").setValueLimits(18.0, 70.0);
		poikanen.get("nokka_tyvi").setValueLimits(16.0, 42.0);
		poikanen.get("nokka_tyvi_2").setValueLimits(16.0, 42.0);
		poikanen.get("paino").setValueLimits(200, 6600);
		poikanen.get("paino_2").setValueLimits(200, 6600);
		poikanen.get("munan_pituus").setValueLimits(65.0, 85.0);
		poikanen.get("munan_leveys").setValueLimits(50.0, 70.0);
		
		return structure;
	}
	
	/*
	 * public void generateJavacodeFromDatabaseGeneratedDatabaseStructure() {
	 * _Row params = data.updateparameters();
	 * for (_Table t : params) {
	 * String tablename = t.getTablename();
	 * System.out.println("DatabaseTableStructure "+t.getTablename()+" = new DatabaseTableStructure(\""+t.getTablename()+"\");");
	 * Iterator<_Column> iterator = t.getColumns();
	 * while (iterator.hasNext()) {
	 * _Column c = iterator.next();
	 * if (c.getName().equals(Const.ROWID)) continue;
	 * if (c.getDatatype() == _Column.VARCHAR)
	 * System.out.println(tablename+".addColumn(\""+c.getName()+"\", "+c.getDatatype()+", "+c.getSize()+");");
	 * else if (c.getDatatype() == _Column.DATE)
	 * System.out.println(tablename+".addColumn(\""+c.getName()+"\", "+c.getDatatype()+");");
	 * else if (c.getDatatype() == _Column.INTEGER)
	 * System.out.println(tablename+".addColumn(\""+c.getName()+"\", "+c.getDatatype()+", "+c.getSize()+");");
	 * else if (c.getDatatype() == _Column.DECIMAL)
	 * System.out.println(tablename+".addColumn(\""+c.getName()+"\", "+c.getDatatype()+", "+c.getSize()+" , "+c.getDecimalSize()+");");
	 * }
	 * }
	 * for (_Table t : params) {
	 * String tablename = t.getTablename();
	 * Iterator<_Column> iterator = t.getColumns();
	 * while (iterator.hasNext()) {
	 * _Column c = iterator.next();
	 * if (!c.getAssosiatedSelection().equals("")) {
	 * System.out.println(tablename+".get(\""+c.getName()+"\").setAssosiatedSelection(\""+c.getAssosiatedSelection()+"\");");
	 * }
	 * }
	 * }
	 * }
	 */
}
