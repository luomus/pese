package fi.hy.pese.framework.test.db;

import java.io.InputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.Array;
import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.Date;
import java.sql.NClob;
import java.sql.ParameterMetaData;
import java.sql.PreparedStatement;
import java.sql.Ref;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.RowId;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.SQLXML;
import java.sql.Statement;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Map;

import fi.luomus.commons.db.connectivity.TransactionConnection;

public class ConnectionStub implements TransactionConnection {

	public ConnectionStub() {}

	private final ResultSet	resultsetStub	= new ResultSet() {


		@Override
		public <T> T unwrap(Class<T> iface) throws SQLException {

			return null;
		}


		@Override
		public boolean isWrapperFor(Class<?> iface) throws SQLException {

			return false;
		}


		@Override
		public boolean wasNull() throws SQLException {

			return false;
		}


		@Override
		public void updateTimestamp(String columnLabel, Timestamp x) throws SQLException {

		}


		@Override
		public void updateTimestamp(int columnIndex, Timestamp x) throws SQLException {

		}


		@Override
		public void updateTime(String columnLabel, Time x) throws SQLException {

		}


		@Override
		public void updateTime(int columnIndex, Time x) throws SQLException {

		}


		@Override
		public void updateString(String columnLabel, String x) throws SQLException {

		}


		@Override
		public void updateString(int columnIndex, String x) throws SQLException {

		}


		@Override
		public void updateShort(String columnLabel, short x) throws SQLException {

		}


		@Override
		public void updateShort(int columnIndex, short x) throws SQLException {

		}


		@Override
		public void updateSQLXML(String columnLabel, SQLXML xmlObject) throws SQLException {

		}


		@Override
		public void updateSQLXML(int columnIndex, SQLXML xmlObject) throws SQLException {

		}


		@Override
		public void updateRowId(String columnLabel, RowId x) throws SQLException {

		}


		@Override
		public void updateRowId(int columnIndex, RowId x) throws SQLException {

		}


		@Override
		public void updateRow() throws SQLException {

		}


		@Override
		public void updateRef(String columnLabel, Ref x) throws SQLException {

		}


		@Override
		public void updateRef(int columnIndex, Ref x) throws SQLException {

		}


		@Override
		public void updateObject(String columnLabel, Object x, int scaleOrLength) throws SQLException {

		}


		@Override
		public void updateObject(int columnIndex, Object x, int scaleOrLength) throws SQLException {

		}


		@Override
		public void updateObject(String columnLabel, Object x) throws SQLException {

		}


		@Override
		public void updateObject(int columnIndex, Object x) throws SQLException {

		}


		@Override
		public void updateNull(String columnLabel) throws SQLException {

		}


		@Override
		public void updateNull(int columnIndex) throws SQLException {

		}


		@Override
		public void updateNString(String columnLabel, String nString) throws SQLException {

		}


		@Override
		public void updateNString(int columnIndex, String nString) throws SQLException {

		}


		@Override
		public void updateNClob(String columnLabel, Reader reader, long length) throws SQLException {

		}


		@Override
		public void updateNClob(int columnIndex, Reader reader, long length) throws SQLException {

		}


		@Override
		public void updateNClob(String columnLabel, Reader reader) throws SQLException {

		}


		@Override
		public void updateNClob(int columnIndex, Reader reader) throws SQLException {

		}


		@Override
		public void updateNClob(String columnLabel, NClob nClob) throws SQLException {

		}


		@Override
		public void updateNClob(int columnIndex, NClob nClob) throws SQLException {

		}


		@Override
		public void updateNCharacterStream(String columnLabel, Reader reader, long length) throws SQLException {

		}


		@Override
		public void updateNCharacterStream(int columnIndex, Reader x, long length) throws SQLException {

		}


		@Override
		public void updateNCharacterStream(String columnLabel, Reader reader) throws SQLException {

		}


		@Override
		public void updateNCharacterStream(int columnIndex, Reader x) throws SQLException {

		}


		@Override
		public void updateLong(String columnLabel, long x) throws SQLException {

		}


		@Override
		public void updateLong(int columnIndex, long x) throws SQLException {

		}


		@Override
		public void updateInt(String columnLabel, int x) throws SQLException {

		}


		@Override
		public void updateInt(int columnIndex, int x) throws SQLException {

		}


		@Override
		public void updateFloat(String columnLabel, float x) throws SQLException {

		}


		@Override
		public void updateFloat(int columnIndex, float x) throws SQLException {

		}


		@Override
		public void updateDouble(String columnLabel, double x) throws SQLException {

		}


		@Override
		public void updateDouble(int columnIndex, double x) throws SQLException {

		}


		@Override
		public void updateDate(String columnLabel, Date x) throws SQLException {

		}


		@Override
		public void updateDate(int columnIndex, Date x) throws SQLException {

		}


		@Override
		public void updateClob(String columnLabel, Reader reader, long length) throws SQLException {

		}


		@Override
		public void updateClob(int columnIndex, Reader reader, long length) throws SQLException {

		}


		@Override
		public void updateClob(String columnLabel, Reader reader) throws SQLException {

		}


		@Override
		public void updateClob(int columnIndex, Reader reader) throws SQLException {

		}


		@Override
		public void updateClob(String columnLabel, Clob x) throws SQLException {

		}


		@Override
		public void updateClob(int columnIndex, Clob x) throws SQLException {

		}


		@Override
		public void updateCharacterStream(String columnLabel, Reader reader, long length) throws SQLException {

		}


		@Override
		public void updateCharacterStream(int columnIndex, Reader x, long length) throws SQLException {

		}


		@Override
		public void updateCharacterStream(String columnLabel, Reader reader, int length) throws SQLException {

		}


		@Override
		public void updateCharacterStream(int columnIndex, Reader x, int length) throws SQLException {

		}


		@Override
		public void updateCharacterStream(String columnLabel, Reader reader) throws SQLException {

		}


		@Override
		public void updateCharacterStream(int columnIndex, Reader x) throws SQLException {

		}


		@Override
		public void updateBytes(String columnLabel, byte[] x) throws SQLException {

		}


		@Override
		public void updateBytes(int columnIndex, byte[] x) throws SQLException {

		}


		@Override
		public void updateByte(String columnLabel, byte x) throws SQLException {

		}


		@Override
		public void updateByte(int columnIndex, byte x) throws SQLException {

		}


		@Override
		public void updateBoolean(String columnLabel, boolean x) throws SQLException {

		}


		@Override
		public void updateBoolean(int columnIndex, boolean x) throws SQLException {

		}


		@Override
		public void updateBlob(String columnLabel, InputStream inputStream, long length) throws SQLException {

		}


		@Override
		public void updateBlob(int columnIndex, InputStream inputStream, long length) throws SQLException {

		}


		@Override
		public void updateBlob(String columnLabel, InputStream inputStream) throws SQLException {

		}


		@Override
		public void updateBlob(int columnIndex, InputStream inputStream) throws SQLException {

		}


		@Override
		public void updateBlob(String columnLabel, Blob x) throws SQLException {

		}


		@Override
		public void updateBlob(int columnIndex, Blob x) throws SQLException {

		}


		@Override
		public void updateBinaryStream(String columnLabel, InputStream x, long length) throws SQLException {

		}


		@Override
		public void updateBinaryStream(int columnIndex, InputStream x, long length) throws SQLException {

		}


		@Override
		public void updateBinaryStream(String columnLabel, InputStream x, int length) throws SQLException {

		}


		@Override
		public void updateBinaryStream(int columnIndex, InputStream x, int length) throws SQLException {

		}


		@Override
		public void updateBinaryStream(String columnLabel, InputStream x) throws SQLException {

		}


		@Override
		public void updateBinaryStream(int columnIndex, InputStream x) throws SQLException {

		}


		@Override
		public void updateBigDecimal(String columnLabel, BigDecimal x) throws SQLException {

		}


		@Override
		public void updateBigDecimal(int columnIndex, BigDecimal x) throws SQLException {

		}


		@Override
		public void updateAsciiStream(String columnLabel, InputStream x, long length) throws SQLException {

		}


		@Override
		public void updateAsciiStream(int columnIndex, InputStream x, long length) throws SQLException {

		}


		@Override
		public void updateAsciiStream(String columnLabel, InputStream x, int length) throws SQLException {

		}


		@Override
		public void updateAsciiStream(int columnIndex, InputStream x, int length) throws SQLException {

		}


		@Override
		public void updateAsciiStream(String columnLabel, InputStream x) throws SQLException {

		}


		@Override
		public void updateAsciiStream(int columnIndex, InputStream x) throws SQLException {

		}


		@Override
		public void updateArray(String columnLabel, Array x) throws SQLException {

		}


		@Override
		public void updateArray(int columnIndex, Array x) throws SQLException {

		}


		@Override
		public void setFetchSize(int rows) throws SQLException {

		}


		@Override
		public void setFetchDirection(int direction) throws SQLException {

		}


		@Override
		public boolean rowUpdated() throws SQLException {

			return false;
		}


		@Override
		public boolean rowInserted() throws SQLException {

			return false;
		}


		@Override
		public boolean rowDeleted() throws SQLException {

			return false;
		}


		@Override
		public boolean relative(int rows) throws SQLException {

			return false;
		}


		@Override
		public void refreshRow() throws SQLException {

		}


		@Override
		public boolean previous() throws SQLException {

			return false;
		}

		private boolean	first	= true;


		@Override
		public boolean next() throws SQLException {
			if (first) {
				first = false;
				return true;
			}
			return false;
		}


		@Override
		public void moveToInsertRow() throws SQLException {

		}


		@Override
		public void moveToCurrentRow() throws SQLException {

		}


		@Override
		public boolean last() throws SQLException {

			return false;
		}


		@Override
		public boolean isLast() throws SQLException {

			return false;
		}


		@Override
		public boolean isFirst() throws SQLException {

			return false;
		}

		@Override
		public boolean isClosed() throws SQLException {

			return false;
		}

		@Override
		public boolean isBeforeFirst() throws SQLException {

			return false;
		}

		@Override
		public boolean isAfterLast() throws SQLException {

			return false;
		}

		@Override
		public void insertRow() throws SQLException {

		}

		@Override
		public SQLWarning getWarnings() throws SQLException {

			return null;
		}

		@Override
		public InputStream getUnicodeStream(String columnLabel) throws SQLException {

			return null;
		}

		@Override
		public InputStream getUnicodeStream(int columnIndex) throws SQLException {

			return null;
		}

		@Override
		public URL getURL(String columnLabel) throws SQLException {

			return null;
		}

		@Override
		public URL getURL(int columnIndex) throws SQLException {

			return null;
		}

		@Override
		public int getType() throws SQLException {

			return 0;
		}

		@Override
		public Timestamp getTimestamp(String columnLabel, Calendar cal) throws SQLException {

			return null;
		}

		@Override
		public Timestamp getTimestamp(int columnIndex, Calendar cal) throws SQLException {

			return null;
		}

		@Override
		public Timestamp getTimestamp(String columnLabel) throws SQLException {

			return null;
		}

		@Override
		public Timestamp getTimestamp(int columnIndex) throws SQLException {

			return null;
		}

		@Override
		public Time getTime(String columnLabel, Calendar cal) throws SQLException {

			return null;
		}

		@Override
		public Time getTime(int columnIndex, Calendar cal) throws SQLException {

			return null;
		}

		@Override
		public Time getTime(String columnLabel) throws SQLException {

			return null;
		}

		@Override
		public Time getTime(int columnIndex) throws SQLException {

			return null;
		}

		@Override
		public String getString(String columnLabel) throws SQLException {

			return null;
		}

		@Override
		public String getString(int columnIndex) throws SQLException {

			return null;
		}

		@Override
		public Statement getStatement() throws SQLException {

			return null;
		}

		@Override
		public short getShort(String columnLabel) throws SQLException {

			return 0;
		}

		@Override
		public short getShort(int columnIndex) throws SQLException {

			return 0;
		}

		@Override
		public SQLXML getSQLXML(String columnLabel) throws SQLException {

			return null;
		}

		@Override
		public SQLXML getSQLXML(int columnIndex) throws SQLException {

			return null;
		}

		@Override
		public RowId getRowId(String columnLabel) throws SQLException {

			return null;
		}

		@Override
		public RowId getRowId(int columnIndex) throws SQLException {

			return null;
		}

		@Override
		public int getRow() throws SQLException {

			return 0;
		}

		@Override
		public Ref getRef(String columnLabel) throws SQLException {

			return null;
		}

		@Override
		public Ref getRef(int columnIndex) throws SQLException {

			return null;
		}

		@Override
		public Object getObject(String columnLabel, Map<String, Class<?>> map) throws SQLException {

			return null;
		}

		@Override
		public Object getObject(int columnIndex, Map<String, Class<?>> map) throws SQLException {

			return null;
		}

		@Override
		public Object getObject(String columnLabel) throws SQLException {

			return null;
		}

		@Override
		public Object getObject(int columnIndex) throws SQLException {

			return null;
		}

		@Override
		public String getNString(String columnLabel) throws SQLException {

			return null;
		}

		@Override
		public String getNString(int columnIndex) throws SQLException {

			return null;
		}

		@Override
		public NClob getNClob(String columnLabel) throws SQLException {

			return null;
		}

		@Override
		public NClob getNClob(int columnIndex) throws SQLException {

			return null;
		}

		@Override
		public Reader getNCharacterStream(String columnLabel) throws SQLException {

			return null;
		}

		@Override
		public Reader getNCharacterStream(int columnIndex) throws SQLException {

			return null;
		}

		@Override
		public ResultSetMetaData getMetaData() throws SQLException {

			return null;
		}

		@Override
		public long getLong(String columnLabel) throws SQLException {

			return 0;
		}

		@Override
		public long getLong(int columnIndex) throws SQLException {

			return 0;
		}

		@Override
		public int getInt(String columnLabel) throws SQLException {

			return 0;
		}

		@Override
		public int getInt(int columnIndex) throws SQLException {

			return 0;
		}

		@Override
		public int getHoldability() throws SQLException {

			return 0;
		}

		@Override
		public float getFloat(String columnLabel) throws SQLException {

			return 0;
		}

		@Override
		public float getFloat(int columnIndex) throws SQLException {

			return 0;
		}

		@Override
		public int getFetchSize() throws SQLException {

			return 0;
		}

		@Override
		public int getFetchDirection() throws SQLException {

			return 0;
		}

		@Override
		public double getDouble(String columnLabel) throws SQLException {

			return 0;
		}

		@Override
		public double getDouble(int columnIndex) throws SQLException {

			return 0;
		}

		@Override
		public Date getDate(String columnLabel, Calendar cal) throws SQLException {

			return null;
		}

		@Override
		public Date getDate(int columnIndex, Calendar cal) throws SQLException {

			return null;
		}

		@Override
		public Date getDate(String columnLabel) throws SQLException {

			return null;
		}

		@Override
		public Date getDate(int columnIndex) throws SQLException {

			return null;
		}

		@Override
		public String getCursorName() throws SQLException {

			return null;
		}

		@Override
		public int getConcurrency() throws SQLException {

			return 0;
		}

		@Override
		public Clob getClob(String columnLabel) throws SQLException {

			return null;
		}

		@Override
		public Clob getClob(int columnIndex) throws SQLException {

			return null;
		}

		@Override
		public Reader getCharacterStream(String columnLabel) throws SQLException {

			return null;
		}

		@Override
		public Reader getCharacterStream(int columnIndex) throws SQLException {

			return null;
		}

		@Override
		public byte[] getBytes(String columnLabel) throws SQLException {

			return null;
		}

		@Override
		public byte[] getBytes(int columnIndex) throws SQLException {

			return null;
		}

		@Override
		public byte getByte(String columnLabel) throws SQLException {

			return 0;
		}

		@Override
		public byte getByte(int columnIndex) throws SQLException {

			return 0;
		}

		@Override
		public boolean getBoolean(String columnLabel) throws SQLException {

			return false;
		}

		@Override
		public boolean getBoolean(int columnIndex) throws SQLException {

			return false;
		}

		@Override
		public Blob getBlob(String columnLabel) throws SQLException {

			return null;
		}

		@Override
		public Blob getBlob(int columnIndex) throws SQLException {

			return null;
		}

		@Override
		public InputStream getBinaryStream(String columnLabel) throws SQLException {

			return null;
		}

		@Override
		public InputStream getBinaryStream(int columnIndex) throws SQLException {

			return null;
		}

		@Override
		public BigDecimal getBigDecimal(String columnLabel, int scale) throws SQLException {

			return null;
		}

		@Override
		public BigDecimal getBigDecimal(int columnIndex, int scale) throws SQLException {

			return null;
		}

		@Override
		public BigDecimal getBigDecimal(String columnLabel) throws SQLException {

			return null;
		}

		@Override
		public BigDecimal getBigDecimal(int columnIndex) throws SQLException {

			return null;
		}

		@Override
		public InputStream getAsciiStream(String columnLabel) throws SQLException {

			return null;
		}

		@Override
		public InputStream getAsciiStream(int columnIndex) throws SQLException {

			return null;
		}

		@Override
		public Array getArray(String columnLabel) throws SQLException {

			return null;
		}

		@Override
		public Array getArray(int columnIndex) throws SQLException {

			return null;
		}

		@Override
		public boolean first() throws SQLException {

			return false;
		}

		@Override
		public int findColumn(String columnLabel) throws SQLException {

			return 0;
		}

		@Override
		public void deleteRow() throws SQLException {

		}

		@Override
		public void close() throws SQLException {

		}

		@Override
		public void clearWarnings() throws SQLException {

		}

		@Override
		public void cancelRowUpdates() throws SQLException {

		}

		@Override
		public void beforeFirst() throws SQLException {

		}

		@Override
		public void afterLast() throws SQLException {

		}

		@Override
		public boolean absolute(int row) throws SQLException {

			return false;
		}


		@Override
		public <T> T getObject(int arg0, Class<T> arg1) throws SQLException {
			return null;
		}


		@Override
		public <T> T getObject(String arg0, Class<T> arg1) throws SQLException {
			return null;
		}

	};

	@Override
	public void release() {}

	@Override
	public PreparedStatement prepareStatement(String sql) throws SQLException {
		return new PreparedStatement() {

			@Override
			public <T> T unwrap(Class<T> arg0) throws SQLException {

				return null;
			}

			@Override
			public boolean isWrapperFor(Class<?> arg0) throws SQLException {

				return false;
			}

			@Override
			public void setQueryTimeout(int arg0) throws SQLException {

			}

			@Override
			public void setPoolable(boolean arg0) throws SQLException {

			}

			@Override
			public void setMaxRows(int arg0) throws SQLException {

			}

			@Override
			public void setMaxFieldSize(int arg0) throws SQLException {

			}

			@Override
			public void setFetchSize(int arg0) throws SQLException {

			}

			@Override
			public void setFetchDirection(int arg0) throws SQLException {

			}

			@Override
			public void setEscapeProcessing(boolean arg0) throws SQLException {

			}

			@Override
			public void setCursorName(String arg0) throws SQLException {

			}

			@Override
			public boolean isPoolable() throws SQLException {

				return false;
			}

			@Override
			public boolean isClosed() throws SQLException {

				return false;
			}

			@Override
			public SQLWarning getWarnings() throws SQLException {

				return null;
			}

			@Override
			public int getUpdateCount() throws SQLException {

				return 0;
			}

			@Override
			public int getResultSetType() throws SQLException {

				return 0;
			}

			@Override
			public int getResultSetHoldability() throws SQLException {

				return 0;
			}

			@Override
			public int getResultSetConcurrency() throws SQLException {

				return 0;
			}

			@Override
			public ResultSet getResultSet() throws SQLException {

				return null;
			}

			@Override
			public int getQueryTimeout() throws SQLException {

				return 0;
			}

			@Override
			public boolean getMoreResults(int arg0) throws SQLException {

				return false;
			}

			@Override
			public boolean getMoreResults() throws SQLException {

				return false;
			}

			@Override
			public int getMaxRows() throws SQLException {

				return 0;
			}

			@Override
			public int getMaxFieldSize() throws SQLException {

				return 0;
			}

			@Override
			public ResultSet getGeneratedKeys() throws SQLException {

				return null;
			}

			@Override
			public int getFetchSize() throws SQLException {

				return 0;
			}

			@Override
			public int getFetchDirection() throws SQLException {

				return 0;
			}

			@Override
			public Connection getConnection() throws SQLException {

				return null;
			}

			@Override
			public int executeUpdate(String arg0, String[] arg1) throws SQLException {

				return 0;
			}

			@Override
			public int executeUpdate(String arg0, int[] arg1) throws SQLException {

				return 0;
			}

			@Override
			public int executeUpdate(String arg0, int arg1) throws SQLException {

				return 0;
			}

			@Override
			public int executeUpdate(String arg0) throws SQLException {

				return 0;
			}

			@Override
			public ResultSet executeQuery(String arg0) throws SQLException {

				return resultsetStub;
			}

			@Override
			public int[] executeBatch() throws SQLException {

				return null;
			}

			@Override
			public boolean execute(String arg0, String[] arg1) throws SQLException {

				return false;
			}

			@Override
			public boolean execute(String arg0, int[] arg1) throws SQLException {

				return false;
			}

			@Override
			public boolean execute(String arg0, int arg1) throws SQLException {

				return false;
			}

			@Override
			public boolean execute(String arg0) throws SQLException {

				return false;
			}

			@Override
			public void close() throws SQLException {

			}

			@Override
			public void clearWarnings() throws SQLException {

			}

			@Override
			public void clearBatch() throws SQLException {

			}

			@Override
			public void cancel() throws SQLException {

			}

			@Override
			public void addBatch(String arg0) throws SQLException {

			}

			@Override
			public void setUnicodeStream(int arg0, InputStream arg1, int arg2) throws SQLException {

			}

			@Override
			public void setURL(int arg0, URL arg1) throws SQLException {

			}

			@Override
			public void setTimestamp(int arg0, Timestamp arg1, Calendar arg2) throws SQLException {

			}

			@Override
			public void setTimestamp(int arg0, Timestamp arg1) throws SQLException {

			}

			@Override
			public void setTime(int arg0, Time arg1, Calendar arg2) throws SQLException {

			}

			@Override
			public void setTime(int arg0, Time arg1) throws SQLException {

			}

			@Override
			public void setString(int arg0, String arg1) throws SQLException {

			}

			@Override
			public void setShort(int arg0, short arg1) throws SQLException {

			}

			@Override
			public void setSQLXML(int arg0, SQLXML arg1) throws SQLException {

			}

			@Override
			public void setRowId(int arg0, RowId arg1) throws SQLException {

			}

			@Override
			public void setRef(int arg0, Ref arg1) throws SQLException {

			}

			@Override
			public void setObject(int arg0, Object arg1, int arg2, int arg3) throws SQLException {

			}

			@Override
			public void setObject(int arg0, Object arg1, int arg2) throws SQLException {

			}

			@Override
			public void setObject(int arg0, Object arg1) throws SQLException {

			}

			@Override
			public void setNull(int arg0, int arg1, String arg2) throws SQLException {

			}

			@Override
			public void setNull(int arg0, int arg1) throws SQLException {

			}

			@Override
			public void setNString(int arg0, String arg1) throws SQLException {

			}

			@Override
			public void setNClob(int arg0, Reader arg1, long arg2) throws SQLException {

			}

			@Override
			public void setNClob(int arg0, Reader arg1) throws SQLException {

			}

			@Override
			public void setNClob(int arg0, NClob arg1) throws SQLException {

			}

			@Override
			public void setNCharacterStream(int arg0, Reader arg1, long arg2) throws SQLException {

			}

			@Override
			public void setNCharacterStream(int arg0, Reader arg1) throws SQLException {

			}

			@Override
			public void setLong(int arg0, long arg1) throws SQLException {

			}

			@Override
			public void setInt(int arg0, int arg1) throws SQLException {

			}

			@Override
			public void setFloat(int arg0, float arg1) throws SQLException {

			}

			@Override
			public void setDouble(int arg0, double arg1) throws SQLException {

			}

			@Override
			public void setDate(int arg0, Date arg1, Calendar arg2) throws SQLException {

			}

			@Override
			public void setDate(int arg0, Date arg1) throws SQLException {

			}

			@Override
			public void setClob(int arg0, Reader arg1, long arg2) throws SQLException {

			}

			@Override
			public void setClob(int arg0, Reader arg1) throws SQLException {

			}

			@Override
			public void setClob(int arg0, Clob arg1) throws SQLException {

			}

			@Override
			public void setCharacterStream(int arg0, Reader arg1, long arg2) throws SQLException {

			}

			@Override
			public void setCharacterStream(int arg0, Reader arg1, int arg2) throws SQLException {

			}

			@Override
			public void setCharacterStream(int arg0, Reader arg1) throws SQLException {

			}

			@Override
			public void setBytes(int arg0, byte[] arg1) throws SQLException {

			}

			@Override
			public void setByte(int arg0, byte arg1) throws SQLException {

			}

			@Override
			public void setBoolean(int arg0, boolean arg1) throws SQLException {

			}

			@Override
			public void setBlob(int arg0, InputStream arg1, long arg2) throws SQLException {

			}

			@Override
			public void setBlob(int arg0, InputStream arg1) throws SQLException {

			}

			@Override
			public void setBlob(int arg0, Blob arg1) throws SQLException {

			}

			@Override
			public void setBinaryStream(int arg0, InputStream arg1, long arg2) throws SQLException {

			}

			@Override
			public void setBinaryStream(int arg0, InputStream arg1, int arg2) throws SQLException {

			}

			@Override
			public void setBinaryStream(int arg0, InputStream arg1) throws SQLException {

			}

			@Override
			public void setBigDecimal(int arg0, BigDecimal arg1) throws SQLException {

			}

			@Override
			public void setAsciiStream(int arg0, InputStream arg1, long arg2) throws SQLException {

			}

			@Override
			public void setAsciiStream(int arg0, InputStream arg1, int arg2) throws SQLException {

			}

			@Override
			public void setAsciiStream(int arg0, InputStream arg1) throws SQLException {

			}

			@Override
			public void setArray(int arg0, Array arg1) throws SQLException {

			}

			@Override
			public ParameterMetaData getParameterMetaData() throws SQLException {

				return null;
			}

			@Override
			public ResultSetMetaData getMetaData() throws SQLException {

				return null;
			}

			@Override
			public int executeUpdate() throws SQLException {

				return 1;
			}

			@Override
			public ResultSet executeQuery() throws SQLException {

				return resultsetStub;
			}

			@Override
			public boolean execute() throws SQLException {

				return false;
			}

			@Override
			public void clearParameters() throws SQLException {

			}

			@Override
			public void addBatch() throws SQLException {

			}

			@Override
			public void closeOnCompletion() throws SQLException {

			}

			@Override
			public boolean isCloseOnCompletion() throws SQLException {
				return false;
			}

		};
	}

	@Override
	public void startTransaction() throws SQLException {}

	@Override
	public void commitTransaction() throws SQLException {}

	@Override
	public void rollbackTransaction() throws SQLException {}

	@Override
	public boolean isClosed() {
		return true;
	}

	@Override
	public CallableStatement prepareCall(String sql) throws SQLException {
		return null;
	}

	@Override
	public void close() throws Exception {
		// Auto-generated method stub
		
	}

}
