package fi.hy.pese.framework.main.app.functionality.validate;

public class NumericRange<T> {

	private final T minValue;
	private final T maxValue;

	public NumericRange(T minValue, T maxValue) {
		this.minValue = minValue;
		this.maxValue = maxValue;
	}

	public T getMinValue() {
		return minValue;
	}

	public T getMaxValue() {
		return maxValue;
	}
}
