package fi.hy.pese.framework.main.app;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import fi.hy.pese.framework.main.db._DAO;
import fi.hy.pese.framework.main.general._WarehouseRowGenerator;
import fi.hy.pese.framework.main.general.data.HistoryAction;
import fi.hy.pese.framework.main.general.data.ParameterMap;
import fi.hy.pese.framework.main.general.data._Data;
import fi.hy.pese.framework.main.general.data._Row;
import fi.hy.pese.framework.main.kirjekyyhky._KirjekyyhkyXMLGenerator;
import fi.hy.pese.framework.main.view._ExceptionViewer;
import fi.hy.pese.framework.main.view._Viewer;
import fi.luomus.commons.config.Config;
import fi.luomus.commons.containers.Selection;
import fi.luomus.commons.kirjekyyhky.KirjekyyhkyAPIClient;
import fi.luomus.commons.languagesupport.LocalizedTextsContainer;
import fi.luomus.commons.pdf.PDFWriter;
import fi.luomus.commons.reporting.ErrorReporter;
import fi.luomus.commons.tipuapi.TipuAPIClient;
import fi.luomus.commons.tipuapi.TipuApiResource;

/**
 * The factory contains all the application specific functionality, configuration and structures
 */
public interface _ApplicationSpecificationFactory<T extends _Row> {

	/**
	 * Returns a new data structure
	 * @param language
	 * @param params
	 * @return
	 * @throws Exception
	 */
	_Data<T> initData(String language, ParameterMap params) throws Exception;

	/**
	 * Returns application specific configuration values
	 * @see Config
	 * @return configuration
	 */
	Config config();

	/**
	 * Creates a NEW application specific dao connection, which must be released after use.
	 * @param userId The id of the user to whom the Dao is given (for logging purposes)
	 * @see _DAO
	 * @return dao
	 * @throws SQLException
	 */
	_DAO<T> dao(String userId) throws SQLException;

	/**
	 * Releseases dao connection.
	 * @param dao
	 * @throws SQLException
	 */
	void release(_DAO<T> dao);

	/**
	 * Returns the localization texts for this application
	 * @see LocalizedTextsContainer
	 * @return
	 */
	LocalizedTextsContainer uiTexts();

	/**
	 * Returns the functionality that this application has defined for the page, if any
	 * @see _Functionality
	 * @param page
	 * @param request request to be used
	 * @return functionality
	 * @throws UnsupportedOperationException if the application does not define a functionality for the page
	 */
	_Functionality<T> functionalityFor(String page, _Request<T> request) throws UnsupportedOperationException;

	/**
	 * Returns the Exception viewer which this application wants to be used to handle the exceptions that were caused during the initialization and execution of functionality
	 * @return exception viewer
	 */
	_ExceptionViewer exceptionViewer(_Request<T> request);

	/**
	 * Returns an application specific collection of selections using the DAO that is given
	 * @param dao
	 * @return selections
	 * @see Selection
	 * @throws Exception
	 */
	Map<String, Selection> selections(_DAO<T> dao) throws  Exception;

	/**
	 * Returns a resource from Tipu-API by it's name
	 * @param resourcename
	 * @return
	 * @
	 */
	TipuApiResource getTipuApiResource(String resourcename);

	/**
	 * Returns the Viewer that is to be used with this application
	 * @see _Viewer
	 * @return viewer
	 * @throws IOException if viewer configuration is incorrect
	 */
	_Viewer viewer() throws IOException;

	/**
	 * A convenience method for the developer to re-read the UITexts each page load, which is usually only done a the init stage of a servlet.
	 * The implementing application can override this method not to do anything, when in production.
	 * @throws Exception if there are configuration issues or possibly implementation specific problems, like malformed localization file
	 */
	void reloadUITexts() throws Exception;

	/**
	 * Returns the name of the page where to direct the user if needed, for example after a successful login
	 * @return frontpage
	 */
	String frontpage();

	/**
	 * Logs the exception
	 * @param originalRequestURI
	 * @param condition
	 */
	void reportException(String originalRequestURI, Throwable condition);

	/**
	 * Returns a new pdf writer
	 * @return
	 */
	PDFWriter pdfWriter();

	/**
	 * Returns a new pdf writer
	 * @param outputFolder
	 * @return
	 */
	PDFWriter pdfWriter(String outputFolder);

	/**
	 * Returns a new Kirjekyyhky form-data-xml generator
	 * @return
	 */
	_KirjekyyhkyXMLGenerator kirjekyyhkyFormDataXMLGenerator(_Request<T> request)  throws Exception;

	/**
	 * Returns a new Kirjekyyhky API
	 * @return
	 * @throws Exception
	 */
	KirjekyyhkyAPIClient kirjekyyhkyAPI() throws Exception;

	/**
	 * Returns a new TipuAPI instance
	 * @return
	 * @throws Exception
	 */
	TipuAPIClient tipuAPI() throws Exception;

	/**
	 * Tells if this application is using Kirjekyyhky -features
	 * @return
	 */
	boolean usingKirjekyyhky();

	/**
	 * Tells if this application is using TipuAPI
	 * @return
	 */
	boolean usingTipuApi();

	/**
	 * Loads count of incoming forms from Kirjekyyhky for this application
	 * @param request
	 * @throws Exception
	 */
	void loadKirjekyyhkyCount(_Request<T> request) throws Exception;

	/**
	 * Add application wide history action
	 * @param action
	 */
	void addHistoryAction(HistoryAction action);

	/**
	 * Get list of history actions
	 * @return
	 */
	List<HistoryAction> getHistoryActions();

	/**
	 * Saves current history actions to be loaded again after reboot/etc
	 */
	void saveHistoryActions();

	/**
	 * Load history actions
	 */
	void loadHistoryActions();

	/**
	 * Some application specific stuff that needs to be done just before calling the view and commiting transaction.
	 */
	void postProsessingHook(_Request<T> request);

	ErrorReporter getErrorReporter();

	/**
	 * Does this application support sending records to FinBIF data warehouse
	 * @return
	 */
	boolean isWarehouseSource();

	/**
	 * Get generator that transforms interal row to FinBIF data warehouse model
	 * @param dao
	 * @return
	 */
	_WarehouseRowGenerator getWarehouseRowGenerator(_DAO<T> dao);

}
