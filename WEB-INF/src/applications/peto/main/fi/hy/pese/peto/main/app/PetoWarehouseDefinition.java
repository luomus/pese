package fi.hy.pese.peto.main.app;

import java.util.HashMap;
import java.util.Map;

import fi.hy.pese.framework.main.general.WarehouseGenerationDefinitionBuilder;
import fi.hy.pese.framework.main.general.WarehouseRowGenerator.NestIsDead;
import fi.hy.pese.framework.main.general.WarehouseRowGenerator.NotInspected;
import fi.hy.pese.framework.main.general.WarehouseRowGenerator.ResolveAtlasCode;
import fi.hy.pese.framework.main.general.WarehouseRowGenerator.ResolveCoordinates;
import fi.hy.pese.framework.main.general.WarehouseRowGenerator.ResolveCounts;
import fi.hy.pese.framework.main.general.WarehouseRowGenerator.ResolveDateAccuracy;
import fi.hy.pese.framework.main.general.WarehouseRowGenerator.ResolveLocality.LocalityKylaNimi;
import fi.hy.pese.framework.main.general.WarehouseRowGenerator.ResolveNestCount;
import fi.hy.pese.framework.main.general.WarehouseRowGenerator.ResolveSecure;
import fi.hy.pese.framework.main.general.WarehouseRowGenerator.ResolveSpecies;
import fi.hy.pese.framework.main.general.WarehouseRowGenerator.WarehouseGenerationDefinition;
import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.main.general.data._Table;
import fi.laji.datawarehouse.etl.models.dw.Coordinates;
import fi.laji.datawarehouse.etl.models.dw.Coordinates.Type;
import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException;
import fi.luomus.commons.containers.rdf.Qname;

public class PetoWarehouseDefinition {
	
	private static WarehouseGenerationDefinition definition;
	
	public static WarehouseGenerationDefinition instance() {
		if (definition == null) {
			definition = init();
		}
		return definition;
	}
	
	public static WarehouseGenerationDefinition init() {
		return new WarehouseGenerationDefinitionBuilder()
				.sourceId(new Qname("KE.24"))
				.collectionId(new Qname("HR.49"))
				.nestIsDead(new NestIsDead() {
					@Override
					public boolean get(_PesaDomainModelRow row) {
						for (_Table kaynti : row.getKaynnit()) {
							if (PetoConst.DEAD_NEST_CODES.contains(kaynti.get("pesa_kunto").getValue())) return true;
						}
						return false;
					}
				})
				.notInspected(new NotInspected() {
					@Override
					public boolean get(_PesaDomainModelRow row) {
						return "E".equals(row.getColumn("tarkastus.tarkastettu").getValue());
					}
				})
				.yearColumnFullname("tarkastus.vuosi")
				.siteTypeColumnFullName("tarkastus.pesa_sijainti")
				.municipalityColumnFullName("pesa.kunta")
				.coordinates(new ResolveCoordinates() {
					@Override
					public Coordinates get(_Table pesa) throws NumberFormatException, IllegalArgumentException, DataValidationException {
						Coordinates c = new Coordinates(pesa.get("eur_leveys").getDoubleValue(), pesa.get("eur_pituus").getDoubleValue(), Type.EUREF);
						String koordTark = pesa.get("koord_ilm_mittaustapa").getValue();
						if ("K".equals(koordTark)) c.setAccuracyInMeters(100);
						else if ("X".equals(koordTark)) c.setAccuracyInMeters(1000);
						else c.setAccuracyInMeters(1);
						return c;
					}
				})
				.locality(new LocalityKylaNimi("kyla", "nimi"))
				.dateAccuracy(new ResolveDateAccuracy() {
					@Override
					public DateAccuracy get(_Table kaynti) {
						if (!kaynti.getName().equals("kaynti")) return null; // tarkastus table
						if (!kaynti.get("pvm").hasValue()) return null;
						String v = kaynti.get("pvm_tarkkuus").getValue();
						if ("1".equals(v) || v.isEmpty()) return DateAccuracy.ACCURATE;
						if ("2".equals(v)) return DateAccuracy.MONTH;
						return DateAccuracy.YEAR;
					}
				})
				.nestingSpecies(new ResolveSpecies() {
					@Override
					public String get(_PesaDomainModelRow row) {
						String pesiva = row.getTarkastus().get("pesiva_laji").getValue();
						String muuLaji = row.getTarkastus().get("muu_laji").getValue();
						String rakentanut = row.getPesa().get("rak_laji").getValue();
						String edellLaji = row.getTarkastus().get("kommentti_edellinen_laji").getValue();
						if (!pesiva.isEmpty()) return pesiva;
						if (!muuLaji.isEmpty()) return muuLaji;
						if (!rakentanut.isEmpty()) return rakentanut;
						if (!edellLaji.isEmpty()) return edellLaji;
						return null;
					}
				})
				.nestCount(new ResolveNestCount() {
					@Override
					public int get(_PesaDomainModelRow row) {
						if (!"A".equals(row.getColumn("tarkastus.pesimistulos").getValue())) return 1;
						for (_Table kaynti : row.getKaynnit()) {
							_Column kunto = kaynti.get("pesa_kunto");
							if (kunto.hasValue()) {
								if (kunto.getIntegerValue() <= 1) return 1; // puussa
							}
						}
						return 0;
					}
				})
				.atlasCode(new ResolveAtlasCode() {
					@Override
					public Qname get(_PesaDomainModelRow row) {
						String pesimistulos = row.getColumn("tarkastus.pesimistulos").getValue();
						String varmuus = row.getColumn("tarkastus.pesimistulos_tarkkuus").getValue();
						if ("X".equals(pesimistulos)) {
							// other species
							if ("VK".contains(varmuus)) return new Qname("MY.atlasCodeEnum7");
							return null;
						}
						return ATLAS_CODES.get(pesimistulos);
					}
				})
				.counts(new ResolveCounts() {
					@Override
					public Counts get(_Table kaynti) {
						Counts counts = new Counts();
						counts.adults = counts.value(kaynti.get("aikuisia_lkm"));
						counts.liveEggs = counts.value(kaynti.get("munia_lkm"));
						counts.livePullus = counts.value(kaynti.get("pesapoikasia_lkm"));
						counts.deadPullus = counts.value(kaynti.get("kuolleita_lkm"));
						counts.liveJuvenile = counts.value(kaynti.get("lentopoikasia_lkm"));
						return counts;
					}
				})
				//.isSuspecious(null)
				.shouldSecure(new ResolveSecure() {
					@Override
					public boolean get(_PesaDomainModelRow row, boolean isPublic) {
						return "E".equals(row.getColumn("pesa.suojelu_kaytto").getValue());
					}
				})
				.build();
	}
	
	private static final Map<String, Qname> ATLAS_CODES;
	static {
		ATLAS_CODES = new HashMap<>();
		ATLAS_CODES.put("B", new Qname("MY.atlasCodeEnum2"));
		ATLAS_CODES.put("D", new Qname("MY.atlasCodeEnum62"));
		ATLAS_CODES.put("E", new Qname("MY.atlasCodeEnum66"));
		ATLAS_CODES.put("F", new Qname("MY.atlasCodeEnum66"));
		ATLAS_CODES.put("K", new Qname("MY.atlasCodeEnum66"));
		ATLAS_CODES.put("L", new Qname("MY.atlasCodeEnum66"));
		ATLAS_CODES.put("M", new Qname("MY.atlasCodeEnum71"));
		ATLAS_CODES.put("N", new Qname("MY.atlasCodeEnum82"));
		ATLAS_CODES.put("O", new Qname("MY.atlasCodeEnum82"));
		ATLAS_CODES.put("P", new Qname("MY.atlasCodeEnum71"));
		ATLAS_CODES.put("Q", new Qname("MY.atlasCodeEnum82"));
		ATLAS_CODES.put("R", new Qname("MY.atlasCodeEnum82"));
		ATLAS_CODES.put("T", new Qname("MY.atlasCodeEnum71"));
		ATLAS_CODES.put("V", new Qname("MY.atlasCodeEnum82"));
		ATLAS_CODES.put("Y", new Qname("MY.atlasCodeEnum66"));
	}
	
}
