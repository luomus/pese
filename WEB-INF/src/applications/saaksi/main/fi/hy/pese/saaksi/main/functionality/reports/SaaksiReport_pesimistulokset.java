package fi.hy.pese.saaksi.main.functionality.reports;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.general.ReportWriter;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.Utils;

/**
 * Summary of nesting statuses by year
 */
public class SaaksiReport_pesimistulokset extends ReportWriter<_PesaDomainModelRow> {

	/**
	 * Example:

Pesimätulokset|1971|2013|
Vuosi|A|X|B|C|Y|...|R|V|*|
1971|61|0|6|....|0|
...
	 */

	public SaaksiReport_pesimistulokset(_Request<_PesaDomainModelRow> request, String filenamePrefix) {
		super(request, filenamePrefix);
	}

	@Override
	public boolean produceSpecific() {
		headerLine();
		newLine();
		newLine();
		columnDescriptions();
		rows();
		return true;
	}

	private void rows() {
		Map<Integer, Map<String, Integer>> pesimistulokset = new HashMap<>();

		for (_PesaDomainModelRow row : request.data().results()) {
			int year = row.getTarkastus().get("vuosi").getIntegerValue();
			String pesimistulos = row.getTarkastus().get("pesimistulos").getValue();
			if (!given(pesimistulos)) continue;
			if (!pesimistulokset.containsKey(year)) {
				pesimistulokset.put(year, new HashMap<String, Integer>());
			}
			Map<String, Integer> vuodenPesimistulokset = pesimistulokset.get(year);
			if (!vuodenPesimistulokset.containsKey(pesimistulos)) {
				vuodenPesimistulokset.put(pesimistulos, 1);
			} else {
				vuodenPesimistulokset.put(pesimistulos, vuodenPesimistulokset.get(pesimistulos) + 1);
			}
		}

		for (int year = 1971; year <= DateUtils.getCurrentYear(); year++) {
			if (!pesimistulokset.containsKey(year)) continue;
			write(year);
			separator();
			Map<String, Integer> vuodenPesimistulokset = pesimistulokset.get(year);
			for (String code : PESIMISTULOS_CODES) {
				if (!vuodenPesimistulokset.containsKey(code)) {
					write(0);
				} else {
					write(vuodenPesimistulokset.get(code));
				}
				separator();
			}
			newLine();
		}
	}

	private static final List<String> PESIMISTULOS_CODES = Utils.list("A", "X", "B", "C", "Y", "D", "E", "F", "K", "L", "M", "N", "O", "P", "Q", "T", "R", "V");

	private void columnDescriptions() {
		write("Pesimätulokset");
		separator();
		write("1971");
		separator();
		write(DateUtils.getCurrentYear());
		separator();
		newLine();

		write("Vuosi");
		separator();
		for (String code : PESIMISTULOS_CODES) {
			write(code);
			separator();
		}
		newLine();
	}


}
