package fi.hy.pese.saaksi.main.app;

import fi.hy.pese.framework.main.app._Functionality;
import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.main.view.RedirectTo500ExceptionViewer;
import fi.hy.pese.framework.main.view._ExceptionViewer;
import fi.hy.pese.saaksi.main.functionality.SaaksiKirjekyyhkyValidationServiceFunctionality;

public class SaaksiKirjekyyhkyValidationServiceFactory extends SaaksiFactory {

	public SaaksiKirjekyyhkyValidationServiceFactory(String configFileName) throws Exception {
		super(configFileName);
	}

	@Override
	public String frontpage() {
		return Const.LOGIN_PAGE;
	}

	@Override
	public _Functionality<_PesaDomainModelRow> functionalityFor(String page, _Request<_PesaDomainModelRow> request) {
		return new SaaksiKirjekyyhkyValidationServiceFunctionality(request);
	}

	@Override
	public _ExceptionViewer exceptionViewer(_Request<_PesaDomainModelRow> request) {
		return new RedirectTo500ExceptionViewer(request.redirecter());
	}

	@Override
	public void postProsessingHook(_Request<_PesaDomainModelRow> request) {
		// lets not load Kirjekyyhky count
	}

}
