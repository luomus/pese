package fi.hy.pese.framework.main.kirjekyyhky;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fi.hy.pese.framework.main.general.data.DatabaseColumnStructure;
import fi.hy.pese.framework.main.general.data.DatabaseTableStructure;
import fi.hy.pese.framework.main.general.data.Table;
import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._Row;
import fi.hy.pese.framework.main.general.data._Table;
import fi.luomus.commons.containers.Selection;
import fi.luomus.commons.containers.Selection.SelectionOption;
import fi.luomus.commons.utils.FileUtils;
import fi.luomus.commons.xml.Document;
import fi.luomus.commons.xml.Document.Node;
import fi.luomus.commons.xml.XMLWriter;

public class KirjekyyhkyXMLGeneratorUtility {

	public class StructureDefinition {

		public class ValidationServiceDefinition {
			public String URI;
			public String username;
			public String password;

			public boolean isDefined() {
				return URI != null;
			}
		}

		public class CoordinateFieldDefinition {
			public String leveys;
			public String pituus;
			public String tyyppi;
			public String mittaustapa;
			public final List<String> titleFields = new ArrayList<>();
			public boolean isDefined() {
				return leveys != null && pituus != null && tyyppi != null;
			}
			public void setTitleFields(String ... titles) {
				for (String title : titles) {
					titleFields.add(title);
				}
			}
		}

		public String typeID;
		public String title;
		public List<String> titleFields = new ArrayList<>();
		public ValidationServiceDefinition validation = new ValidationServiceDefinition();
		public String layout;
		public File emptyPDF;
		public File instructionsPDF;
		public _Row row;
		public Map<String, String> fieldTitles;
		public Map<String, Selection> selections;
		public Menutexts menutexts = new Menutexts();
		public CoordinateFieldDefinition	coordinateFields = new CoordinateFieldDefinition();

		public boolean hasMenutexts() {
			return menutexts.newForm != null || menutexts.printEmptyPdf != null || menutexts.printInstructions != null;
		}

	}

	public static class Menutexts {
		public String	newForm;
		public String 	printEmptyPdf;
		public String	printInstructions;
	}

	private static final String	CUSTOM	= "custom";

	private final StructureDefinition definition = new StructureDefinition();
	private final Set<String> ignoredColumns = new HashSet<>();
	private final List<String> tablesToInclude = new ArrayList<>();
	private final Set<String> noCodesForSelection = new HashSet<>();
	private final Set<String> ignoredSelections = new HashSet<>();
	private final Set<String>	userAsDefault = new HashSet<>();
	private final Map<String, String> tipuApiSelections = new HashMap<>();
	private final Set<String> immutableFields = new HashSet<>();
	private DatabaseTableStructure customTable = null;
	private final Set<String> requiredColumns = new HashSet<>();

	public void includeTable(String tableFullName) {
		tablesToInclude.add(tableFullName);
	}

	public StructureDefinition getDefinition() {
		return definition;
	}

	public String generateDataXML(Map<String, String> data, List<String> owners) throws Exception {
		Document document = new Document("form-data");
		Node rootNode = document.getRootNode();
		rootNode.addAttribute("type", definition.typeID);
		rootNode.addAttribute("prefilled", "yes");

		Node ownersNode = rootNode.addChildNode("owners");
		for (String owner : owners) {
			ownersNode.addChildNode("owner").addAttribute("type", "rengastaja").setContents(owner);
		}

		Node dataNode = rootNode.addChildNode("data");
		if (definition.row != null) {
			for (String tableName : tablesToInclude) {
				_Table table = definition.row.getTableByName(tableName);
				for (_Column column : table) {
					if (column.getFieldType() == _Column.DATE_ADDED_COLUMN) continue;
					if (column.getFieldType() == _Column.DATE_MODIFIED_COLUMN) continue;
					if (column.getFieldType() == _Column.ROWID) continue;
					if (ignoredColumns.contains(column.getFullname())) continue;
					if (data.containsKey(column.getFullname())) {
						dataNode.addChildNode(column.getFullname()).setContents(data.get(column.getFullname()));
					}
				}
			}

			if (customTable != null) {
				for (DatabaseColumnStructure column : customTable.getColumns()) {
					if (column.getFieldType() == _Column.ROWID) continue;
					if (data.containsKey(column.getName())) {
						dataNode.addChildNode(column.getName()).setContents(data.get(column.getName()));
					}
				}
			}
		}

		XMLWriter writer = new XMLWriter(document);
		return writer.generateXML();
	}

	public String generateStructureXML() throws Exception {
		Document document = new Document("form-structure");
		Node rootNode = document.getRootNode();

		rootNode.addAttribute("type", definition.typeID);

		if (definition.title != null) {
			rootNode.addChildNode("title").setContents(definition.title);
		}

		if (definition.hasMenutexts()) {
			Node menuTexts = rootNode.addChildNode("menu-texts");
			if (definition.menutexts.newForm != null) {
				menuTexts.addChildNode("new-form").setContents(definition.menutexts.newForm);
			}
			if (definition.menutexts.printEmptyPdf != null) {
				menuTexts.addChildNode("print-empty").setContents(definition.menutexts.printEmptyPdf);
			}
			if (definition.menutexts.printInstructions != null) {
				menuTexts.addChildNode("print-instructions").setContents(definition.menutexts.printInstructions);
			}
		}

		if (!definition.titleFields.isEmpty()) {
			Node titleFields = rootNode.addChildNode("title-fields");
			for (String s : definition.titleFields) {
				titleFields.addChildNode("title-field").setContents(s);
			}
		}

		if (definition.coordinateFields.isDefined()) {
			Node coordinateFields = rootNode.addChildNode("coordinate-fields");
			coordinateFields.addChildNode("y").setContents(definition.coordinateFields.leveys);
			coordinateFields.addChildNode("x").setContents(definition.coordinateFields.pituus);
			coordinateFields.addChildNode("type").setContents(definition.coordinateFields.tyyppi);
			coordinateFields.addChildNode("measurement-method").setContents(definition.coordinateFields.mittaustapa);
			if (!definition.coordinateFields.titleFields.isEmpty()) {
				Node titles = coordinateFields.addChildNode("title-fields");
				for (String field : definition.coordinateFields.titleFields) {
					titles.addChildNode("title").setContents(field);
				}
			}
		}

		if (definition.validation.isDefined()) {
			Node validation = rootNode.addChildNode("validation-service");
			validation.addChildNode("uri").setContents(definition.validation.URI);
			validation.addChildNode("username").setContents(definition.validation.username);
			validation.addChildNode("password").setContents(definition.validation.password);
		}

		if (definition.row != null) {
			Node fieldList = rootNode.addChildNode("field-list");
			for (String tableName : tablesToInclude) {
				_Table table = definition.row.getTableByName(tableName);
				addTo(fieldList, table);
			}
			if (customTable != null) {
				addTo(fieldList, new Table(customTable));
			}
		}

		if (definition.selections != null) {
			Node selectionList = rootNode.addChildNode("selections");
			for (Map.Entry<String, Selection> e : definition.selections.entrySet()) {
				Selection selection = e.getValue();
				if (shouldIgnore(selection)) continue;
				addSelectionToSelectionList(selection, selectionList);
			}
		}

		if (definition.layout != null) {
			Node layout = rootNode.addChildNode("layout");
			layout.contentIsXML(true);
			layout.setContents(definition.layout);
		}

		if (definition.emptyPDF != null) {
			Node emptyPdf = rootNode.addChildNode("empty-pdf");
			emptyPdf.addAttribute("filename", definition.emptyPDF.getName());
			emptyPdf.setCDATA(FileUtils.readContentsAsBase64(definition.emptyPDF));
		}

		if (definition.instructionsPDF != null) {
			Node instrPdf = rootNode.addChildNode("instructions-pdf");
			instrPdf.addAttribute("filename", definition.instructionsPDF.getName());
			instrPdf.setCDATA(FileUtils.readContentsAsBase64(definition.instructionsPDF));
		}

		XMLWriter generator = new XMLWriter(document);
		return generator.generateXML();
	}

	private void addSelectionToSelectionList(Selection selection, Node selectionList) {
		String selectionName = selection.getName();
		Node optionsListNode = selectionList.addChildNode("selection-options");
		optionsListNode.addAttribute("name", selectionName);
		addOptionsToOptionListNode(selection, optionsListNode);
	}

	private void addOptionsToOptionListNode(Selection selection, Node optionsListNode) {
		String prevOptGroup = "";
		Node addToThis = optionsListNode;
		for (SelectionOption o : selection) {
			if (!o.getGroup().equals(prevOptGroup)) {
				if (o.getGroup().length() > 0) {
					addToThis = optionsListNode.addChildNode("optgroup").addAttribute("label", o.getGroup());
				} else {
					addToThis = optionsListNode;
				}
				prevOptGroup = o.getGroup();
			}
			Node optionNode = addToThis.addChildNode("option").addAttribute("value", o.getValue());
			if (noCodesFor(selection)) {
				optionNode.setContents(o.getText());
			} else {
				optionNode.setContents(o.getValue() + " - " + o.getText());
			}
		}
		optionsListNode.addChildNode("option").addAttribute("value", "").setContents("(tyhjä arvo)");
		optionsListNode.addChildNode("option").addAttribute("value", "").setContents(" ");
	}

	private boolean shouldIgnore(Selection selection) {
		return ignoredSelections.contains(selection.getName().toLowerCase());
	}

	private boolean noCodesFor(Selection selection) {
		return noCodesForSelection.contains(selection.getName().toLowerCase());
	}

	private void addTo(Node fieldList, _Table table) {
		if (!table.isInitialized()) {
			throw new IllegalArgumentException("Table not initialized");
		}

		for (_Column column : table) {
			if (column.getFieldType() == _Column.DATE_ADDED_COLUMN) continue;
			if (column.getFieldType() == _Column.DATE_MODIFIED_COLUMN) continue;
			if (column.getFieldType() == _Column.ROWID) continue;
			if (ignoredColumns.contains(column.getFullname())) continue;
			addTo(fieldList, column);
		}
	}

	private void addTo(Node fieldList, _Column column) {
		String name = column.getFullname();
		if (column.getTablename().equals(CUSTOM)) {
			name = column.getName();
		}
		Node field = fieldList.addChildNode("field");
		field.addAttribute("name", name);

		if (column.getFieldType() == _Column.UNIQUE_NUMERIC_INCREASING_ID || isImmutableField(name)) {
			field.addAttribute("type", "immutable");
		} else if (tipuApiSelections.containsKey(column.getFullname())) {
			field.addAttribute("type", "selection");
			field.addAttribute("tipu-api-options", tipuApiSelections.get(column.getFullname()));
		} else if (column.hasAssosiatedSelection()) {
			field.addAttribute("type", "selection");
			field.addAttribute("selection-options", column.getAssosiatedSelection().toLowerCase());
		} else if (column.getDatatype() == _Column.VARCHAR) {
			field.addAttribute("type", "text");
			field.addAttribute("max-length", Integer.toString(column.getSize()));
		} else if (column.getDatatype() == _Column.INTEGER) {
			field.addAttribute("type", "number");
			setMinMaxValues(column, field);
		} else if (column.getDatatype() == _Column.DECIMAL) {
			field.addAttribute("type", "decimal");
			setMinMaxValues(column, field);
		} else if (column.getDatatype() == _Column.DATE) {
			field.addAttribute("type", "date");
		}
		if (requiredField(column)) {
			field.addAttribute("required", "yes");
		}
		if (definition.fieldTitles.containsKey(name)) {
			field.addAttribute("title", definition.fieldTitles.get(name));
		}
		if (userAsDefault.contains(name)) {
			field.addAttribute("user-as-default", "yes");
		}
	}

	private boolean requiredField(_Column column) {
		return column.kirjekyyhkyRequired() || requiredColumns.contains(column.getFullname());
	}

	private void setMinMaxValues(_Column column, Node field) {
		double minValue = column.getMinValue();
		double maxValue = column.getMaxValue();
		if (minValue != Double.NEGATIVE_INFINITY) {
			field.addAttribute("min-value", Double.toString(minValue));
		}
		if (maxValue != Double.POSITIVE_INFINITY) {
			field.addAttribute("max-value", Double.toString(maxValue));
		}
	}

	public void excludeColumn(String tablename, String columnName) {
		ignoredColumns.add(tablename+"."+columnName);
	}

	// Dangerous!  DatabaseTableStructure must not be modified!!!
	//	public void addCustomColumnToTable(String tableName, String columnName, int type, int size) {
	//		DatabaseTableStructure table = definition.databaseStructure.getTableStructureByName(tableName);
	//		table.addColumn(columnName, type, size);
	//	}

	public void addCustomColumn(String fieldName, int type, int size) {
		if (customTable == null) {
			customTable = new DatabaseTableStructure(CUSTOM);
		}
		customTable.addColumn(fieldName, type, size);
	}

	public void addCustomColumn(String fieldName, int type, String selectionOptions) {
		if (customTable == null) {
			customTable = new DatabaseTableStructure(CUSTOM);
		}
		customTable.addColumn(fieldName, type, 1);
		customTable.get(fieldName).setAssosiatedSelection(selectionOptions);
	}

	public void addCustomColumn(String fieldName, int type, int size, String selectionOptions) {
		if (customTable == null) {
			customTable = new DatabaseTableStructure(CUSTOM);
		}
		customTable.addColumn(fieldName, type, size);
		customTable.get(fieldName).setAssosiatedSelection(selectionOptions);
	}

	public void setCustomFieldRequired(String fieldname) {
		customTable.get(fieldname).setKirjekyyhkyRequired();
	}


	public void setRequired(String tablename, String fieldname) {
		requiredColumns.add(tablename + "." + fieldname);
	}

	public void noCodesForSelection(String selectionName) {
		noCodesForSelection.add(selectionName.toLowerCase());
	}

	public void excludeSelection(String selectionName) {
		ignoredSelections.add(selectionName.toLowerCase());
	}

	public void defineTipuApiSelection(String tablename, String fieldName, String tipuApiResourceName) {
		tipuApiSelections.put(tablename + "." + fieldName, tipuApiResourceName);
	}

	public void defineTipuApiSelection(String fieldName, String tipuApiResourceName) {
		defineTipuApiSelection(CUSTOM, fieldName, tipuApiResourceName);
	}

	public void userAsDefaultValue(String fieldName) {
		userAsDefault.add(fieldName);
	}

	public boolean isImmutableField(String columnFullname) {
		return immutableFields.contains(columnFullname);
	}

	public void setToImmutableField(String columnFullname) {
		immutableFields.add(columnFullname);
	}


}
