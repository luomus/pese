package fi.hy.pese.pesakortti.main.app;

import java.util.Map;

import fi.hy.pese.framework.main.app.ApiServiceFactory;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.luomus.commons.config.Config;
import fi.luomus.commons.languagesupport.LanguageFileReader;
import fi.luomus.commons.languagesupport.LocalizedTextsContainer;
import fi.luomus.commons.languagesupport.LocalizedTextsContainerImple;

public class PesakorttiApiFactory extends ApiServiceFactory<_PesaDomainModelRow> {

	public PesakorttiApiFactory(String configFileName) throws Exception {
		super(configFileName);
	}

	@Override
	public void loadUITexts() throws Exception {
		String folder = config().baseFolder() + config().get(Config.LANGUAGE_FILE_FOLDER);
		String prefix = config().get(Config.LANGUAGE_FILE_PREFIX);
		String[] languages = config().get(Config.SUPPORTED_LANGUAGES).split(",");
		LanguageFileReader r = new LanguageFileReader(folder, prefix, languages);
		LocalizedTextsContainer temp = r.readUITexts();

		_PesaDomainModelRow row = this.rowFactory.newRow();

		LocalizedTextsContainerImple uiTexts = new LocalizedTextsContainerImple(languages);

		for (String language : languages) {
			Map<String, String> texts = temp.getAllTexts(language);
			texts = temp.getAllTexts(language);

			PesakorttiFactory.handleNormalTexts(row, texts);
			for (Map.Entry<String, String> e : texts.entrySet()) {
				uiTexts.setText(e.getKey(), e.getValue(), language);
			}
		}
		this.uiTexts = uiTexts;
	}

}
