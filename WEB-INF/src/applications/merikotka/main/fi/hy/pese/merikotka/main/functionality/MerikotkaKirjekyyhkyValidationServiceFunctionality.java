package fi.hy.pese.merikotka.main.functionality;

import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.app.functionality.KirjekyyhkyValidationServiceFunctionality;
import fi.hy.pese.framework.main.app.functionality.validate._Validator;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.merikotka.main.app.MerikotkaConst;
import fi.hy.pese.merikotka.main.functionality.validation.MerikotkaTarkastusValidator;

public class MerikotkaKirjekyyhkyValidationServiceFunctionality extends KirjekyyhkyValidationServiceFunctionality<_PesaDomainModelRow> {

	private static final boolean	KIRJEKYYHKY_VALIDATIONS_ONLY	= true;

	public MerikotkaKirjekyyhkyValidationServiceFunctionality(_Request<_PesaDomainModelRow> request) {
		super(request);
	}

	@Override
	public _Validator validator() {
		return new MerikotkaTarkastusValidator(request, KIRJEKYYHKY_VALIDATIONS_ONLY);
	}

	@Override
	public void preProsessingHook() throws Exception {
		super.preProsessingHook();
		MerikotkaConst.kirjekyyhkyModelToData(data.getAll(), data.updateparameters());
	}

}
