
<#include "header.ftl">

<#-- <div class="kirjekyyhky_management"> -->

<h1>${texts.kirjekyyhky_management_title}</h1>

<h2>${texts.kirjekyyhky_management_incoming}</h2>

<@errorlist errors texts /> 
<@statustexts system texts />

<#if lists.forms??>
  <table  id="resultsTable" class="tablesorter">
    <thead>
    <tr>
      <th>Tyyppi</th>
      <th>Aika</th>
      <th>Kunta</th>
      <th>Kylä</th>
      <th>Pesä</th>
      <th>Omistajat</th>
      <th>Lähetyspvm</th>
      <th class="{sorter: false}"> &nbsp; </th>
    </tr>
    </thead>
    <tbody>
  		<#list lists.forms as form>
    		<tr>
    			<td> ${form.systemID?replace("pesakortti-yhdyskunta", "yhdyskunta")} </td>
      			<td> ${form.data['tarkastus.vuosi']!""} </td>
      			<td> <#if form.data['pesa.kunta']?has_content> ${selections['municipalities'][form.data['pesa.kunta']?upper_case]} </#if> </td>
      			<td> ${form.data['pesa.tarkempi_paikka']!""} </td>
      			<td> 
      				${form.data['pesa.nimi']!""} 
      			    <#if form.data['pesa.id']?has_content> (${form.data['pesa.id']}) <#else>  </#if>
				</td>
      			<td> <#list form.owners as owner> ${owner!""} <#if owner_has_next>,</#if> </#list> </td>
      			<td> ${form.lastUpdated} </td>
      		    <td> <a href="${system.baseURL}?page=${system.page}&amp;action=receive&amp;id=${form.id}&amp;mode=${form.systemID?replace("pesakortti-yhdyskunta", "yhdyskunta")}" class="button-link"> Aloita vastaanotto </a> </td>
    		</tr>
  		</#list>
    </tbody>
  </table>
<#else>
  Ei mitään vastaanotettavaa.
</#if>


<#-- </div> -->





<#include "footer.ftl">
