package fi.hy.pese.pesakortti.main.app;

import java.util.HashSet;
import java.util.Set;

import fi.hy.pese.framework.main.general.WarehouseGenerationDefinitionBuilder;
import fi.hy.pese.framework.main.general.WarehouseRowGenerator.NestIsDead;
import fi.hy.pese.framework.main.general.WarehouseRowGenerator.NotInspected;
import fi.hy.pese.framework.main.general.WarehouseRowGenerator.ResolveAdditionalNestingSpecies;
import fi.hy.pese.framework.main.general.WarehouseRowGenerator.ResolveAtlasCode;
import fi.hy.pese.framework.main.general.WarehouseRowGenerator.ResolveCoordinates;
import fi.hy.pese.framework.main.general.WarehouseRowGenerator.ResolveCounts;
import fi.hy.pese.framework.main.general.WarehouseRowGenerator.ResolveDateAccuracy;
import fi.hy.pese.framework.main.general.WarehouseRowGenerator.ResolveLocality.LocalityKylaNimi;
import fi.hy.pese.framework.main.general.WarehouseRowGenerator.ResolveNestCount;
import fi.hy.pese.framework.main.general.WarehouseRowGenerator.ResolveSecure;
import fi.hy.pese.framework.main.general.WarehouseRowGenerator.ResolveSpecies;
import fi.hy.pese.framework.main.general.WarehouseRowGenerator.ResolveSuspecious;
import fi.hy.pese.framework.main.general.WarehouseRowGenerator.WarehouseGenerationDefinition;
import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.main.general.data._Table;
import fi.laji.datawarehouse.etl.models.dw.Coordinates;
import fi.laji.datawarehouse.etl.models.dw.Coordinates.Type;
import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.Utils;

public class PesakorttiWarehouseDefinition {

	private static WarehouseGenerationDefinition definition;

	public static WarehouseGenerationDefinition instance() {
		if (definition == null) {
			definition = init();
		}
		return definition;
	}

	public static WarehouseGenerationDefinition init() {
		return new WarehouseGenerationDefinitionBuilder()
				.sourceId(new Qname("KE.23"))
				.collectionId(new Qname("HR.64"))
				.nestIsDead(new NestIsDead() {
					@Override
					public boolean get(_PesaDomainModelRow row) {
						return false;
					}
				})
				.notInspected(new NotInspected() {
					@Override
					public boolean get(_PesaDomainModelRow row) {
						return false;
					}
				})
				.yearColumnFullname("tarkastus.vuosi")
				.siteTypeColumnFullName("pesa.pesa_sijainti")
				.municipalityColumnFullName("pesa.kunta")
				.coordinates(new ResolveCoordinates() {
					@Override
					public Coordinates get(_Table pesa) throws NumberFormatException, IllegalArgumentException, DataValidationException {
						_Column lev = pesa.get("eur_leveys");
						_Column pit = pesa.get("eur_pituus");
						if (!lev.hasValue() || !pit.hasValue()) return null;
						Coordinates c = new Coordinates(lev.getDoubleValue(), pit.getDoubleValue(), Type.EUREF);
						String koordTark = pesa.get("koord_tarkkuus").getValue();
						if (!koordTark.isEmpty()) {
							c.setAccuracyInMeters(Integer.valueOf(koordTark));
						} else {
							c.setAccuracyInMeters(1000);
						}
						return c;
					}
				})
				.locality(new LocalityKylaNimi("tarkempi_paikka", "nimi"))
				.dateAccuracy(new ResolveDateAccuracy() {
					@Override
					public DateAccuracy get(_Table kaynti) {
						if (!kaynti.getName().equals("kaynti")) return DateAccuracy.YEAR;
						String v = kaynti.get("pvm_tarkkuus").getValue();
						if ("1".equals(v) || v.isEmpty()) return DateAccuracy.ACCURATE;
						if ("2".equals(v)) return DateAccuracy.MONTH;
						if ("3".equals(v)) return DateAccuracy.YEAR;
						return DateAccuracy.UKNOWN;
					}
				})
				.nestingSpecies(new ResolveSpecies() {
					@Override
					public String get(_PesaDomainModelRow row) {
						return row.getTarkastus().get("pesiva_laji").getValue();
					}
				})
				.nestCount(new ResolveNestCount() {
					@Override
					public int get(_PesaDomainModelRow row) {
						return 1;
					}
				})
				.atlasCode(new ResolveAtlasCode() {
					@Override
					public Qname get(_PesaDomainModelRow row) {
						String pesimistulos = "|" + row.getColumn("tarkastus.pesimistulos").getValue() + "|";
						boolean hasIka = false;
						Set<String> vaiheet = new HashSet<>();
						for (_Table kaynti : row.getKaynnit()) {
							_Column ikaP = kaynti.get("ika_poikaset");
							_Column ikaM = kaynti.get("ika_munat");
							if (ikaP.hasValue()) {
								if (ikaP.getIntegerValue() > 0 && ikaP.getIntegerValue() < 40) hasIka = true;
							}
							if (ikaM.hasValue()) {
								if (ikaM.getIntegerValue() > 0 && ikaM.getIntegerValue() < 40) hasIka = true;
							}
							String vaihe = kaynti.get("pesinnan_vaihe").getValue();
							if (!vaihe.isEmpty()) {
								vaiheet.add(vaihe);
								vaiheet.add(""+vaihe.charAt(0));
							}
						}
						boolean onlyR9 = vaiheet.equals(Utils.set("R9"));

						if ("|3|4|5|".contains(pesimistulos)) return code(82);
						if ("|6|7|8|9|10|11|".contains(pesimistulos)) {
							if (hasIka) return code(82);
							if (contains(vaiheet, "M0", "M1", "M2", "M3", "M4", "M5", "M6", "M7", "P0", "P1", "P2", "P3", "P4", "P5", "P6", "P7")) return code(82);
							if (contains(vaiheet, "M9")) return code(75);
							if (contains(vaiheet, "P8", "P9")) return code(73);
						}
						if ("|7|".equals(pesimistulos)) {
							if (!onlyR9) return code(74);
						}
						if ("|6|".equals(pesimistulos)) {
							if (!onlyR9) return code(73);
						}
						if ("|8|9|10|11|".contains(pesimistulos)) {
							if (!onlyR9) return code(71);
						}
						if ("|12|13|14|15|16|17|18|19|20|21|22|23|24|25|26|".contains(pesimistulos)) {
							if (hasIka) return code(71);
							if (contains(vaiheet, "T0", "T1", "T2", "J0")) return code(71);
							if (vaiheet.contains("M")) return code(71);
							if (vaiheet.contains("P")) return code(71);
						}
						if ("|16|17|18|19|20|".contains(pesimistulos)) {
							if (contains(vaiheet, "R0", "R1", "R2", "R3", "R4", "R5", "E1", "E2")) return code(71);
						}
						if ("|12|13|14|15|21|22|23|25|26|".contains(pesimistulos)) {
							if (vaiheet.contains("R5")) return code(66);
							if (vaiheet.contains("E")) return code(66);
							if (contains(vaiheet, "R1", "R2", "R3", "R4")) return code(62);
							if (vaiheet.contains("R0")) return code(61);
						}
						if ("|1|2|".contains(pesimistulos)) {
							if (vaiheet.contains("T")) return code(71);
							if (vaiheet.contains("M8")) return code(71);
							if (hasIka) return code(82);
							if (contains(vaiheet, "M0", "M1", "M2", "M3", "M4", "M5", "M6", "M7", "P1", "P2", "P3", "P4", "P5", "P6", "P7", "P8", "P9")) code(82);
							if (contains(vaiheet, "J1", "J2")) return code(82);
							if (vaiheet.contains("M9")) return code(75);
							if (vaiheet.contains("P0")) return code(72);
							if (vaiheet.contains("R5")) return code(66);
							if (contains(vaiheet, "R1", "R2", "R3", "R4")) return code(62);
							if (vaiheet.contains("R=")) return code(61);
							if (vaiheet.contains("J0")) return code(7);
						}

						// the following are fail-safes; should be covered by the above logic
						if (hasIka) return code(82);
						if (vaiheet.contains("J")) return code(82);
						if (vaiheet.contains("P")) return code(74);
						if (vaiheet.contains("M")) return code(71);
						if (contains(vaiheet, "T1", "T2")) return code(71);
						if (contains(vaiheet, "T")) return code(66);
						if (contains(vaiheet, "R1", "R2", "R3", "R4", "R5")) return code(66);
						if (vaiheet.contains("R0")) return code(61);
						if (vaiheet.contains("E")) return code(2);
						return null;
					}
					private Qname code(int i) {
						return new Qname("MY.atlasCodeEnum"+i);
					}
					private boolean contains(Set<String> set, String... values) {
						for (String value : values) {
							if (set.stream().anyMatch(element -> element.contains(value))) {
								return true;
							}
						}
						return false;
					}
				})
				.additionalNestingSpecies(new ResolveAdditionalNestingSpecies() {
					@Override
					public Set<String> get(_Table tarkastus) {
						Set<String> species = new HashSet<>();
						String yhdyskunta = tarkastus.get("yhdyskunta_laji").getValue();
						String pesiva = tarkastus.get("pesiva_laji").getValue();
						if (!yhdyskunta.equals(pesiva)) {
							species.add(yhdyskunta);
						}
						return species;
					}
				})
				.counts(new ResolveCounts() {
					@Override
					public Counts get(_Table kaynti) {
						Counts counts = new Counts();
						counts.liveEggs = counts.value(kaynti.get("lkm_munat"));
						counts.deadEggs = counts.value(kaynti.get("lkm_munat_kuolleet"));
						counts.livePullus = counts.value(kaynti.get("lkm_poikaset"));
						counts.deadPullus = counts.value(kaynti.get("lkm_poikaset_kuolleet"));
						if (counts.noCounts()) {
							String vaihe = kaynti.get("pesinnan_vaihe").getValue();
							if ("E1".equals(vaihe)) counts.adults = 1;
							if ("E2".equals(vaihe)) counts.adults = 2;
							if ("M9".equals(vaihe)) counts.adults = 1;
							if ("P0".equals(vaihe)) counts.adults = 1;
						}
						return counts;
					}
				})
				.isSuspecious(new ResolveSuspecious() {
					@Override
					public boolean get(_PesaDomainModelRow row) {
						return "K".equals(row.getColumn("tarkastus.epailyttava").getValue());
					}
				})
				.shouldSecure(new ResolveSecure() {
					@Override
					public boolean get(_PesaDomainModelRow row, boolean isPublic) {
						if ("E".equals(row.getColumn("pesa.suojelu_kaytto").getValue())) return true;
						if (isPublic) {
							if ("K".equals(row.getColumn("tarkastus.suojaus").getValue())) {
								int year = row.getColumn("tarkastus.vuosi").getIntegerValue();
								int currentYear = DateUtils.getCurrentYear();
								if (currentYear - year <= 5) return true;
							}
						}
						return false;
					}
				})
				.build();
	}

}
