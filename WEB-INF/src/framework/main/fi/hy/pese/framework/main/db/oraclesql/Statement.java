package fi.hy.pese.framework.main.db.oraclesql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._Table;
import fi.luomus.commons.db.connectivity.TransactionConnection;
import fi.luomus.commons.utils.Utils;

public abstract class Statement {

	protected final TransactionConnection		con;
	protected final List<Object>	values;
	private PreparedStatement		preparedStatement;

	public Statement(TransactionConnection con) {
		this.con = con;
		this.values = new LinkedList<>();
	}

	protected abstract String constructQuery();

	public void close() throws SQLException {
		preparedStatement.close();
	}

	private PreparedStatement prepareStatement() throws SQLException {
		String query = constructQuery();
		//		System.out.println(query.toString());
		//		System.out.println(values.toString());
		preparedStatement = con.prepareStatement(query.toString());
		int i = 1;
		for (Object o : values) {
			if (o instanceof Double) {
				preparedStatement.setDouble(i++, (Double) o);
			} else if (o instanceof Date) {
				preparedStatement.setDate(i++, new java.sql.Date(((Date) o).getTime()));
			}
			else {
				preparedStatement.setString(i++, o.toString());
			}
		}
		return preparedStatement;
	}

	public int execute() throws SQLException {
		return prepareStatement().executeUpdate();
	}

	public ResultSet executeQuery() throws SQLException {
		return prepareStatement().executeQuery();
	}

	protected boolean hasNumericId(_Table table) {
		return table.getUniqueNumericIdColumn().getValue().length() > 0;
	}

	protected boolean hasRowId(_Table table) {
		return table.getRowidValue().length() > 0;
	}

	protected boolean hasValueSet(_Column c) {
		return c.getValue().length() > 0;
	}

	protected boolean isDate(_Column column) {
		return column.getDatatype() == _Column.DATE;
	}

	protected boolean isDateAddedColumn(_Column c) {
		return c.getFieldType() == _Column.DATE_ADDED_COLUMN;
	}

	protected boolean isDateModifiedColumn(_Column c) {
		return c.getFieldType() == _Column.DATE_MODIFIED_COLUMN;
	}

	protected boolean isDifferent(String text1, String text2) {
		return !text2.equals(text1);
	}

	protected boolean isNull(String[] parameters) {
		return parameters == null || parameters.length < 1;
	}

	protected boolean isNumericIdColumn(_Column c) {
		return c.getFieldType() == _Column.UNIQUE_NUMERIC_INCREASING_ID;
	}

	protected boolean isPartOfTheKey(_Column c) {
		return c.getFieldType() == _Column.IMMUTABLE_PART_OF_A_KEY;
	}

	protected boolean isRowIdColumn(_Column c) {
		return c.getName().equals(Const.ROWID);
	}

	protected void removeLastComma(StringBuilder query) {
		query.setCharAt(query.length() - 1, ' ');
	}

	protected boolean isIntegerDatatypeColumn(_Column c) {
		return c.getDatatype() == _Column.INTEGER;
	}

	protected boolean isDecimalDatatypeColumn(_Column c) {
		return c.getDatatype() == _Column.DECIMAL;
	}

	protected boolean isVarcharDatatypeColumn(_Column c) {
		return c.getDatatype() == _Column.VARCHAR;
	}

	protected void toAndSeperatedStatement(StringBuilder query, List<String> list) {
		Utils.toAndSeperatedStatement(query, list);
	}

	protected void toCommaSeperatedStatement(StringBuilder query, List<String> list) {
		Utils.toCommaSeperatedStatement(query, list);
	}

	protected boolean isNull(String value) {
		return value.length() < 1;
	}

	protected String databasenameFor(_Column c) {
		return c.getDatabaseTablename() + "." + c.getName();
	}

	protected boolean hasAssosiatedSelection(_Column c) {
		return c.getAssosiatedSelection().length() > 0;
	}
}
