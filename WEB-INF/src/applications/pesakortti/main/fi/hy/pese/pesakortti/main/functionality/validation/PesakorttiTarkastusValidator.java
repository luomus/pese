package fi.hy.pese.pesakortti.main.functionality.validation;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.app.functionality.validate.TarkastusValidator;
import fi.hy.pese.framework.main.app.functionality.validate._Validator;
import fi.hy.pese.framework.main.general.PesimisenBiologisetRajatUtil.Rajat;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.main.general.data._Table;
import fi.hy.pese.pesakortti.main.app.PesakorttiConst;
import fi.hy.pese.peto.main.app.PetoConst;
import fi.luomus.commons.utils.CoordinateConverter;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.Utils;

public class PesakorttiTarkastusValidator extends TarkastusValidator {

	private static final Collection<String>	EI_SAA_OLLA_LUKUMAARIA_PESINNAN_VAIHEET	= Utils.collection("E1", "E2", "M0", "M9", "P0");
	private static final int	TUNTURI_THRESHOLD_DEGREES	= 658100;
	private static final int	TUNTURI_THRESHOLD_METRIC	= 7300000;
	private static final String	NESTBOX	= "18";
	private static final String	KIVENKOLO_MAAKOLO	= "19";
	private static final int	COLONY_SIZE_WARNING_THRESHOLD	= 1000;
	private static final String	PESINNAN_VAIHE_ASUMATON	= "R9";
	private static final String	YES	= "K";
	private static final int	WARNING_THRESHOLD_FOR_KOORD_TARKKUUS_FOR_MITTAUSTAPA_K	= 99;
	private static final String	KARTTA	= YES;
	private static final int	WARNING_THRESHOLD_FOR_KOORD_TARKKUUS_FOR_MITTAUSTAPA_M	= 10000;
	private static final String	MUU	= "X";
	private static final String	ZERO			= "0";
	private static final Collection<String> TUNTURI_BIOTOOPIT = Utils.collection("40", "41", "42", "43");
	private static final Collection<String>	PETOLINNUT = initPetolinnut();
	private static final Map<Vaihe, Integer> PESINNAN_VAIHE_ORDER_MAP = initNestingStageMap();
	private static final String[] PESYEKOKO_COLUMNS = {"lkm_munat", "lkm_munat_kuolleet", "lkm_poikaset", "lkm_poikaset_kuolleet"};

	private static Collection<String> initPetolinnut() {
		Collection<String> petolinnut = new HashSet<>();
		petolinnut.addAll(PetoConst.ALLOWED_NESTING_SPECIES);
		petolinnut.add("PANHAL");
		petolinnut.add("HALALB");
		return petolinnut;
	}

	private static Map<Vaihe, Integer> initNestingStageMap() {
		Map<Vaihe, Integer> map = new HashMap<>();
		map.put(Vaihe.RAKENTAMISVAIHE, 0);
		map.put(Vaihe.EMOVAIHE, 1);
		map.put(Vaihe.MUNAVAIHE, 2);
		map.put(Vaihe.POIKASVAIHE, 3);
		map.put(Vaihe.JALKIVAIHE, 4);
		return map;
	}

	protected final _PesaDomainModelRow params;
	protected final _Table pesa;
	protected final _Table tarkastus;
	private final Map<Integer, _Table> ponttoalueet;
	private final Map<String, Rajat> rajat;
	private boolean kirjekyyhkyValidationsOnly;

	public PesakorttiTarkastusValidator(_Request<_PesaDomainModelRow> request, Map<Integer, _Table> ponttoalueet, Map<String, Rajat> rajat) {
		super(request);
		this.params = request.data().updateparameters();
		this.pesa = params.getPesa();
		this.tarkastus = params.getTarkastus();
		this.ponttoalueet = ponttoalueet;
		this.rajat = rajat;
		this.kirjekyyhkyValidationsOnly = false;
	}

	public PesakorttiTarkastusValidator(_Request<_PesaDomainModelRow> request, boolean kirjekyyhkyValidationsOnly, Map<Integer, _Table> ponttoalueet, Map<String, Rajat> rajat) {
		super(request);
		this.params = request.data().updateparameters();
		this.pesa = params.getPesa();
		this.tarkastus = params.getTarkastus();
		this.ponttoalueet = ponttoalueet;
		this.rajat = rajat;
		this.kirjekyyhkyValidationsOnly = kirjekyyhkyValidationsOnly;
	}

	@Override
	public void validate() throws Exception {
		if (kirjekyyhkyValidationsOnly) {
			validateSendingTheForm();
			copyAndRemoveError("pesa.yht_leveys", "pesa.leveys");
			copyAndRemoveError("pesa.yht_pituus", "pesa.pituus");
			copyAndRemoveError("pesa.eur_leveys", "pesa.leveys");
			copyAndRemoveError("pesa.eur_pituus", "pesa.pituus");
			copyAndRemoveError("pesa.ast_leveys", "pesa.leveys");
			copyAndRemoveError("pesa.ast_pituus", "pesa.pituus");
			for (_Table t : params.getKaynnit()) {
				copyAndRemoveError(t.get("ika_munat").getFullname(), t.getFullname() + ".ika");
				copyAndRemoveError(t.get("ika_poikaset").getFullname(), t.getFullname() + ".ika");
			}
		} else {
			super.validate();
			if (hasErrors()) return;
			String action = data.action();
			if (action.equals(Const.FILL_FOR_INSERT)) {
				String tark_vuosi = data.get("tark_vuosi");
				checkValidYear("tark_vuosi", tark_vuosi);
				if (hasErrors()) return;
				//checkNestIsNotDead();
			} else if (action.equals(Const.UPDATE) || action.equals(Const.INSERT) || action.equals(Const.INSERT_FIRST)) {
				validateSendingTheForm();
			}
		}
	}

	private void validateSendingTheForm() throws Exception {
		checkIds();
		if (hasErrors()) return;

		checkAsUpdateparameters(params);
		checkReferenceValuesOfNonNullColumns(params, dao);

		checkNotNull(tarkastus.get("vuosi"));
		checkValidYear(tarkastus.get("vuosi"));

		if (!data.action().equals(Const.UPDATE)) {
			warnYearRange(tarkastus.get("vuosi"));
		}

		if (kirjekyyhkyValidationsOnly) {
			checkNotNull(pesa.get("tarkempi_paikka"));
			checkNotNull(tarkastus.get("tarkastaja"));
			checkNotNull(tarkastus.get("pesimistulos"));
		} else {
			warningIfNull(tarkastus.get("tarkastaja"));
			checkNotNull(tarkastus.get("uusintapesinta_jarjestysnro"));
			checkNotNull(pesa.get("lomake"));
			checkLomake_seurTarkastaja();
		}
		checkNotNull(pesa.get("kunta"));
		checkCoordinates();
		checkKoordTarkkuus_koordMittaustapa();
		checkPesanSijaintiAndTreeNestMeasurements();

		checkTreeVsNestHeight();
		checkPonttoTutkimus();

		if (noErrors(tarkastus.get("vuosi"))) {
			checkKaynnit();
		}

		checkPesinnanBiologisetRajat();
		checkUusintapesinta();
		checkBiotopeVsArea();
		checkColonyInfo();
		checkTunturi();
		checkMunaLeveysPituus();

		if (kirjekyyhkyValidationsOnly) {
			warningIfPetolintu();
		}
	}

	private void warningIfPetolintu() {
		_Column pesiva_laji = tarkastus.get("pesiva_laji");
		if (notNullNoErrors(pesiva_laji)) {
			if (isPetolintu(pesiva_laji)) {
				warning(pesiva_laji, PesakorttiConst.EI_TARKOITUS_ILMOITTAA_PETOLINTUJA);
			}
		}
	}

	private boolean isPetolintu(_Column pesiva_laji) {
		return PETOLINNUT.contains(pesiva_laji.getValue());
	}

	private void checkMunaLeveysPituus() {
		for (_Table muna : params.getMunat().values()) {
			if (!muna.hasValues()) continue;
			_Column pituus = muna.get("pituus");
			_Column leveys = muna.get("leveys");
			if (notNullNoErrors(pituus)) {
				checkNotNull(leveys);
			} else if (notNullNoErrors(leveys)) {
				checkNotNull(pituus);
			}
		}
	}

	private void checkTunturi() {
		_Column biotooppi = tarkastus.get("pesa_biotooppi");
		_Column koordIlmTyyppi = pesa.get("koord_ilm_tyyppi");
		if (notNullNoErrors(biotooppi) && notNullNoErrors(koordIlmTyyppi)) {
			if (tunturilla(biotooppi)) {
				int threshold = -1;
				_Column lev = null;
				switch(koordIlmTyyppi.getValue().charAt(0)) {
					case 'Y': threshold = TUNTURI_THRESHOLD_METRIC; lev = pesa.get("yht_leveys"); break;
					case 'E': threshold = TUNTURI_THRESHOLD_METRIC; lev = pesa.get("eur_leveys"); break;
					case 'A': threshold = TUNTURI_THRESHOLD_DEGREES;  lev = pesa.get("ast_leveys");
				}
				if (lev == null) throw new UnsupportedOperationException();
				if (noErrors(lev)) {
					if (lev.getIntegerValue() < threshold) {
						warning(biotooppi, PesakorttiConst.TUNTURI_BIOTOOPPI_LIIAN_ETELASSA);
					}
				}
			}
		}
	}

	private boolean tunturilla(_Column biotooppi) {
		return TUNTURI_BIOTOOPIT.contains(biotooppi.getValue());
	}

	private static enum Vaihe { RAKENTAMISVAIHE, EMOVAIHE, MUNAVAIHE, POIKASVAIHE, JALKIVAIHE }

	private void checkKaynnit() throws ParseException {
		Date prevKayntiDate = null;
		Vaihe vaihe = Vaihe.RAKENTAMISVAIHE;
		int i = 0;
		Map<String, Integer> pvmCountMap = new HashMap<>();
		for (_Table kaynti : params.getKaynnit().values()) {
			_Column pvm = kaynti.get("pvm");
			checkValueAndAccuracyPair(pvm, kaynti.get("pvm_tarkkuus"), true);
			if (!kaynti.hasValuesIgnoreMetadata("tarkastus")) continue;
			i++;
			prevKayntiDate = checkPvmKlo(prevKayntiDate, kaynti);
			checkPesinnanVaiheAndIkaCombination(kaynti);
			vaihe = checkOrderAndCounts(vaihe, kaynti);
			warnIfMoreThanThreeKayntiForSameDate(pvmCountMap, pvm);
		}
		if (i == 0) {
			error("kaynti.1.pvm", PesakorttiConst.AINAKIN_YKSI_KAYNTI_ILMOITETTAVA);
		} else {
			checkPesivaLaji();
		}
	}

	private void warnIfMoreThanThreeKayntiForSameDate(Map<String, Integer> pvmCountMap, _Column pvm) {
		if (pvm.hasValue()) {
			if (!pvmCountMap.containsKey(pvm.getValue())) {
				setTo1(pvmCountMap, pvm);
			} else {
				addBy1(pvmCountMap, pvm);
			}
			if (pvmCountMap.get(pvm.getValue()) >= 3) {
				warning(pvm, PesakorttiConst.OLETKO_VARMA_ETTEI_KYSE_OLE_YHDYSKUNTAILMOITUKSESTA);
			}
		}
	}

	private void addBy1(Map<String, Integer> pvmCountMap, _Column pvm) {
		pvmCountMap.put(pvm.getValue(), pvmCountMap.get(pvm.getValue()) + 1);
	}

	private void setTo1(Map<String, Integer> pvmCountMap, _Column pvm) {
		pvmCountMap.put(pvm.getValue(), 1);
	}

	private Vaihe checkOrderAndCounts(Vaihe edellinenVaihe, _Table kaynti) {
		_Column pesinnan_vaihe = kaynti.get("pesinnan_vaihe");
		_Column ika_munat = kaynti.get("ika_munat");
		_Column ika_poikaset = kaynti.get("ika_poikaset");

		if (hasErrors(pesinnan_vaihe, ika_munat, ika_poikaset)) return edellinenVaihe;

		Vaihe vaihe = null;
		if (ika_munat.hasValue()) {
			vaihe = Vaihe.MUNAVAIHE;
		} else if (ika_poikaset.hasValue()) {
			vaihe = Vaihe.POIKASVAIHE;
		} else if (notNullNoErrors(pesinnan_vaihe)){
			vaihe = toVaihe(pesinnan_vaihe, vaihe);
		}
		if (vaihe == null) {
			throw new IllegalStateException("No pesinnän vaihe for kaynti " + kaynti.getFullname() + " : " + pesinnan_vaihe.getValue());
		}

		if (vaihe == Vaihe.EMOVAIHE) {
			// saa esiintyä missä välissä vain
		} else if ((pesinnan_vaihe.getValue().equals("P8") || pesinnan_vaihe.getValue().equals("P9")) && edellinenVaihe == Vaihe.JALKIVAIHE) {
			// jälkitarkistusta saa seurata P8 tai P9
		}
		else {
			if (order(edellinenVaihe) > order(vaihe)) {
				warning(pesinnan_vaihe, PesakorttiConst.PESINTA_ETENEE_VAARAAN_SUUNTAAN);
			}
		}

		_Column lkm_munat = kaynti.get("lkm_munat");
		_Column lkm_munat_kuolleet = kaynti.get("lkm_munat_kuolleet");
		_Column lkm_poikaset = kaynti.get("lkm_poikaset");
		_Column lkm_poikaset_kuolleet = kaynti.get("lkm_poikaset_kuolleet");

		checkIsPositiveOrZeroIfHasValue(lkm_munat, lkm_munat_kuolleet, lkm_poikaset, lkm_poikaset_kuolleet);

		switch (vaihe) {
			case RAKENTAMISVAIHE: checkIsNullOrZero(lkm_munat, lkm_munat_kuolleet, lkm_poikaset, lkm_poikaset_kuolleet); break;
			case EMOVAIHE: checkIsNullOrZero(lkm_munat, lkm_munat_kuolleet, lkm_poikaset, lkm_poikaset_kuolleet); break;
			case MUNAVAIHE: checkIsNullOrZero(lkm_poikaset, lkm_poikaset_kuolleet); break;
			case POIKASVAIHE:  break;
			case JALKIVAIHE: checkIsNullOrZero(lkm_munat, lkm_poikaset); break;
			default: break;
		}

		if (eiSaaOllaLukumääriä(pesinnan_vaihe)) {
			checkIsNullOrZero(lkm_munat, lkm_munat_kuolleet, lkm_poikaset, lkm_poikaset_kuolleet);
		}

		if (order(edellinenVaihe) > order(vaihe)) {
			return edellinenVaihe;
		}
		return vaihe;
	}

	private boolean eiSaaOllaLukumääriä(_Column pesinnan_vaihe) {
		return EI_SAA_OLLA_LUKUMAARIA_PESINNAN_VAIHEET.contains(pesinnan_vaihe.getValue());
	}

	private Vaihe toVaihe(_Column pesinnan_vaihe, Vaihe oldVaihe) {
		char ekaMerkki = pesinnan_vaihe.getValue().charAt(0);
		switch (ekaMerkki) {
			case 'R': return Vaihe.RAKENTAMISVAIHE;
			case 'E': return Vaihe.EMOVAIHE;
			case 'M': return Vaihe.MUNAVAIHE;
			case 'P': return Vaihe.POIKASVAIHE;
			case 'T': return Vaihe.JALKIVAIHE;
			case 'J': return Vaihe.JALKIVAIHE;
		}
		return oldVaihe;
	}

	private Integer order(Vaihe vaihe) {
		return PESINNAN_VAIHE_ORDER_MAP.get(vaihe);
	}

	private void checkPesinnanVaiheAndIkaCombination(_Table kaynti) {
		_Column pesinnan_vaihe = kaynti.get("pesinnan_vaihe");
		_Column ika_munat = kaynti.get("ika_munat");
		_Column ika_poikaset = kaynti.get("ika_poikaset");
		if (isNull(pesinnan_vaihe) && isNull(ika_munat) && isNull(ika_poikaset)) {
			if (kirjekyyhkyValidationsOnly) {
				error(pesinnan_vaihe, PesakorttiConst.JOKO_PESINNAN_VAIHE_TAI_MUNAN_TAI_POIKASEN_IKA_ILMOITETTAVA);
			} else {
				error(pesinnan_vaihe, CAN_NOT_BE_NULL);
			}
		} else {
			if (!kirjekyyhkyValidationsOnly) {
				if (!isNull(ika_munat)) {
					checkIsNull(pesinnan_vaihe);
					checkIsNull(ika_poikaset);
				} else if (!isNull(ika_poikaset)) {
					checkIsNull(pesinnan_vaihe);
					checkIsNull(ika_munat);
				} else if (!isNull(pesinnan_vaihe)) {
					checkIsNull(ika_munat);
					checkIsNull(ika_poikaset);
				}
			}
		}
		if (!isNull(ika_munat)) {
			checkIsNotNullOrZero(kaynti.get("lkm_munat"));
		}
		if (!isNull(ika_poikaset)) {
			checkIsNotNullOrZero(kaynti.get("lkm_poikaset"));
		}
	}

	private Date checkPvmKlo(Date prevKayntiDate, _Table kaynti) throws ParseException {
		_Column pvm = kaynti.get("pvm");
		_Column tunnit = kaynti.get("klo_h");
		_Column minuutit = kaynti.get("klo_min");

		checkNotNull(pvm);

		if (minuutit.hasValue()) {
			checkNotNull(tunnit);
		}

		if (noErrors(pvm)) {
			Integer tarkVuosi = tarkastus.get("vuosi").getIntegerValue();
			Integer kayntiVuosi = year(pvm);
			if (kayntiVuosi < tarkVuosi -1 || kayntiVuosi > tarkVuosi) {
				error (pvm, PesakorttiConst.ALL_KAYNTI_MUST_BE_FROM_TARK_VUOSI_OR_ONE_YEAR_BEFORE);
			}
		}
		prevKayntiDate = checkOrder(prevKayntiDate, pvm, tunnit, minuutit);
		return prevKayntiDate;
	}

	private Date checkOrder(Date prevKaynti, _Column pvm, _Column tunnit, _Column minuutit) throws ParseException {
		if (hasErrors(pvm)) return prevKaynti;

		Date date = DateUtils.convertToDate(pvm.getValue());
		if (notNullNoErrors(tunnit)) {
			date = new Date(date.getTime() + tunnit.getIntegerValue() * 60 * 60 * 1000);
		}
		if (notNullNoErrors(minuutit)) {
			date = new Date(date.getTime() + minuutit.getIntegerValue() * 60 * 1000);
		}

		if (prevKaynti == null) {
			prevKaynti = date;
		} else {
			if (date.before(prevKaynti)) {
				error (pvm, PesakorttiConst.INVALID_KAYNTI_ORDER);
			} else {
				prevKaynti = date;
			}
		}
		return prevKaynti;
	}

	private Integer year(_Column pvm) {
		return Integer.valueOf(pvm.getDateValue().getYear());
	}

	private void checkPesinnanBiologisetRajat() {
		_Column pesivaLaji = tarkastus.get("pesiva_laji");
		if (isNullOrHasErrors(pesivaLaji)) return;

		Rajat lajinRajat = rajat.get(pesivaLaji.getValue());
		if (lajinRajat == null) {
			lajinRajat = rajat.get(poistaAlalaji(pesivaLaji));
		}
		if (lajinRajat == null) {
			error(pesivaLaji, PesakorttiConst.EI_RAJA_ARVOA);
			return;
		}

		int maxCount = countMax();
		if (maxCount > lajinRajat.maxPesyekoko) {
			warning(PesakorttiConst.COUNTS, PesakorttiConst.MAX_PESYEKOKO_YLITTYNYT);
		}

		int prevDayOfYear = -1;
		int prevPesyeKoko = -1;
		for (_Table kaynti : params.getKaynnit()) {
			if (isNullOrHasErrors(kaynti.get("pvm"))) continue;
			int dayofYear = DateUtils.getDayOfYear(kaynti.get("pvm").getDateValue());
			List<_Column> pesykokoColumns = kaynti.getColumns("lkm_munat", "lkm_munat_kuolleet", "lkm_poikaset", "lkm_poikaset_kuolleet");
			int pesyekoko = countPesyekoko(pesykokoColumns);
			if (pesyekoko <= 0) continue;
			if (notFirstRow(prevDayOfYear, prevPesyeKoko)) {
				int paiviaKayntienValilla = dayofYear - prevDayOfYear;
				double pesyekoonKasvu = pesyekoko - prevPesyeKoko;
				double maxMunaaPaivassa = 1 / lajinRajat.maxMunintaVali;
				double maxTassaAjassa = maxMunaaPaivassa * paiviaKayntienValilla;
				if (paiviaKayntienValilla == 0) {
					maxTassaAjassa = maxMunaaPaivassa;
				}
				if (pesyekoonKasvu > maxTassaAjassa) {
					for (_Column c : pesykokoColumns) {
						if (c.hasValue()) {
							warning(c, PesakorttiConst.MAX_PESINTANOPEUS_YLITTYNYT);
						}
					}
				}
			}
			if (prevDayOfYear != dayofYear || prevPesyeKoko == -1) {
				if (pesyekoko > prevPesyeKoko) {
					prevPesyeKoko = pesyekoko;
				}
			}
			prevDayOfYear = dayofYear;
			if (prevDayOfYear >= 365) prevDayOfYear = 1;
		}
	}

	private String poistaAlalaji(_Column pesivaLaji) {
		String laji = pesivaLaji.getValue();
		if (laji.length() > 6) {
			return laji.substring(0, 6);
		}
		return laji;
	}

	private boolean notFirstRow(int prevDayOfYear, int prevPesyeKoko) {
		return prevDayOfYear != -1 && prevPesyeKoko != -1;
	}

	private int countMax() {
		int max = 0;
		for (_Table kaynti : params.getKaynnit()) {
			List<_Column> pesykokoColumns = kaynti.getColumns(PESYEKOKO_COLUMNS);
			int thisMax = countPesyekoko(pesykokoColumns);
			if (thisMax > max) max = thisMax;
		}
		return max;
	}

	private int countPesyekoko(List<_Column> pesykokoColumns) {
		int koko = 0;
		for (_Column c : pesykokoColumns) {
			if (notNullNoErrors(c)) {
				koko += c.getIntegerValue();
			}
		}
		if (koko == 0) return -1;
		return koko;
	}

	private void checkColonyInfo() {
		_Column yhdyskunnassa = tarkastus.get("yhdyskunnassa");
		_Column laji = tarkastus.get("yhdyskunta_laji");
		_Column koko = tarkastus.get("yhdyskunta_koko");

		if (yhdyskunnassa.getValue().equals("K")) {
			if (notNullNoErrors(koko)) {
				if (koko.getIntegerValue() > COLONY_SIZE_WARNING_THRESHOLD) {
					warning(koko, ARE_YOU_SURE);
				}
			}
		} else {
			if (!isNull(laji)) {
				error(laji, PesakorttiConst.COLONY_INFO_CAN_NOT_BE_GIVEN_IF_NOT_REPORTED_TO_BE_IN_COLONY);
			}
			if (!isNull(koko)) {
				error(koko, PesakorttiConst.COLONY_INFO_CAN_NOT_BE_GIVEN_IF_NOT_REPORTED_TO_BE_IN_COLONY);
			}
		}

	}

	private void checkBiotopeVsArea() {
		_Column biot = tarkastus.get("pesa_biotooppi");
		warningIfNull(biot);
		_Column biotAla = tarkastus.get("pesa_biotooppi_ala");
		if (isNull(biot)) {
			if (!isNull(biotAla)) {
				warning(biotAla, PesakorttiConst.BIOTOPE_AREA_CAN_T_BE_GIVEN_IF_BIOTOPE_NOT_GIVEN);
			}
		}
	}

	private void checkTreeVsNestHeight() {
		_Column puu_korkeus = tarkastus.get("puu_korkeus");
		_Column pesa_korkeus = tarkastus.get("pesa_korkeus");

		if (notNullNoErrors(puu_korkeus) && notNullNoErrors(pesa_korkeus)) {
			if (Math.floor(pesa_korkeus.getDoubleValue()) > puu_korkeus.getDoubleValue()) {
				error(pesa_korkeus, PesakorttiConst.PESA_CAN_NO_BE_HIGHER_THAN_TREE_HEIGHT);
				error(puu_korkeus, PesakorttiConst.PESA_CAN_NO_BE_HIGHER_THAN_TREE_HEIGHT);
			}
		}
	}


	//	private boolean muniaIlmoitettu() {
	//		for (_Table kaynti : params.getKaynnit().values()) {
	//			if (notNullNoErrors(kaynti.get("lkm_munat"))) {
	//				return true;
	//			}
	//			if (notNullNoErrors(kaynti.get("lkm_munat_kuolleet"))) {
	//				return true;
	//			}
	//		}
	//		return false;
	//	}

	private void checkUusintapesinta() throws SQLException {
		_Column vuosi = tarkastus.get("vuosi");
		_Column pesiva_laji = tarkastus.get("pesiva_laji");
		_Column jarjnro = tarkastus.get("uusintapesinta_jarjestysnro");
		_Column tunniste = tarkastus.get("uusintapesinta_tunniste");
		_Column perustelut = tarkastus.get("uusintapesinta_perustelut");

		if (isNull(jarjnro) || jarjnro.getValue().equals(ZERO)) {
			checkIsNull(tunniste);
			checkIsNull(perustelut);
			return;
		}

		checkNotNull(tunniste);

		if (notNullNoErrors(jarjnro) && notNullNoErrors(pesiva_laji) && notNullNoErrors(vuosi)) {
			int nro = jarjnro.getIntegerValue();
			boolean exists = checkIfAlreadyExists(vuosi, pesiva_laji, tunniste, nro);
			if (exists && !data.action().equals(Const.UPDATE)) {
				error(jarjnro, PesakorttiConst.NESTING_SEQUENCE_USED_MORE_THAN_ONCE);
				error(tunniste, PesakorttiConst.NESTING_SEQUENCE_USED_MORE_THAN_ONCE);
			} else if (nro > 1) {
				checkMinusOneMatches(vuosi, pesiva_laji, jarjnro, tunniste, nro);
			}
		}
	}

	private boolean checkIfAlreadyExists(_Column vuosi, _Column pesiva_laji, _Column tunniste, int nro) throws SQLException {
		_Table searchparams = dao.newRow().getTarkastus();
		searchparams.get("vuosi").setValue(vuosi);
		searchparams.get("pesiva_laji").setValue(pesiva_laji);
		searchparams.get("uusintapesinta_jarjestysnro").setValue(nro);
		searchparams.get("uusintapesinta_tunniste").setValue(tunniste);
		boolean exists = dao.checkValuesExists(searchparams);
		return exists;
	}

	private void checkMinusOneMatches(_Column vuosi, _Column pesiva_laji, _Column jarjnro, _Column tunniste, int nro) throws SQLException {
		_Table searchparams = dao.newRow().getTarkastus();
		searchparams.get("vuosi").setValue(vuosi);
		searchparams.get("pesiva_laji").setValue(pesiva_laji);
		searchparams.get("uusintapesinta_jarjestysnro").setValue(nro - 1);
		searchparams.get("uusintapesinta_tunniste").setValue(tunniste);
		boolean exists = dao.checkValuesExists(searchparams);
		if (!exists) {
			error(jarjnro, PesakorttiConst.MUST_HAVE_NESTING_SEQUENCE_MINUS_ONE);
			error(tunniste, PesakorttiConst.MUST_HAVE_NESTING_SEQUENCE_MINUS_ONE);
		}
	}

	private void checkPesivaLaji() {
		_Column pesiva_laji = tarkastus.get("pesiva_laji");
		boolean asuttu = false;
		for (_Table kaynti : params.getKaynnit().values()) {
			String vaihe = kaynti.get("pesinnan_vaihe").getValue();
			String lkmt = kaynti.get("lkm_munat").getValue() + kaynti.get("lkm_munat_kuolleet").getValue() + kaynti.get("lkm_poikaset").getValue() + kaynti.get("lkm_poikaset_kuolleet").getValue();
			String iat = kaynti.get("ika_munat").getValue() + kaynti.get("ika_poikaset").getValue();
			if (!isNull(lkmt) || !isNull(iat) || (!isNull(vaihe) && !vaihe.equals(PESINNAN_VAIHE_ASUMATON))) {
				asuttu = true;
				break;
			}
		}
		if (!asuttu && !isNull(pesiva_laji)) {
			error(pesiva_laji, PesakorttiConst.PESIVA_LAJI_MUST_BE_EMPTY_WITH_THIS_NESTING_STAGE);
		} else {
			if (asuttu && isNull(pesiva_laji)) {
				error(pesiva_laji, PesakorttiConst.PESIVA_LAJI_REQUIRED_WITH_THIS_NESTING_STAGE);
			}
		}
	}

	private void checkPonttoTutkimus() {
		_Column ponttoalue_nro = pesa.get("ponttoalue_nro");
		_Column pontto_nro = pesa.get("pontto_nro");
		_Column pontto_ripustus = tarkastus.get("pontto_ripustus");

		checkPair(ponttoalue_nro, pontto_nro, PesakorttiConst.PONTTOALUE_PONTTO_MUST_BOTH_BE_GIVEN_OR_NEITHER);
		if (notNullNoErrors(ponttoalue_nro)) {
			if (!ponttoalueet.containsKey( ponttoalue_nro.getIntegerValue() )) {
				error(ponttoalue_nro, DOES_NOT_EXIST);
			}
		}
		_Column kunta = pesa.get("kunta");
		if (notNullNoErrors(ponttoalue_nro) && notNullNoErrors(kunta)) {
			try {
				_Table ponttoalue = dao.returnTableByKeys("ponttoalue", ponttoalue_nro.getValue());
				if (!ponttoalue.get("kunta").getValue().equals(kunta.getValue())) {
					warning(kunta, PesakorttiConst.PONTTOALUE_KUNTA_DOES_NOT_MATCH_WITH_PESA_KUNTA);
				}
			} catch (SQLException e) {
				error(ponttoalue_nro, DOES_NOT_EXIST);
			}
		}

		if (notNullNoErrors(pontto_nro) || notNullNoErrors(pontto_nro) || notNullNoErrors(pontto_ripustus)) {
			_Column pesa_sij = pesa.get("pesa_sijainti");
			if (!pesa_sij.getValue().equals(NESTBOX)) {
				error(pesa_sij, PesakorttiConst.PONTON_TIETOJA_ANNETTU_MUTTA_PESA_SIJ_EI_PONTTO);
				if (pontto_nro.hasValue()) {
					error(pontto_nro, PesakorttiConst.PONTON_TIETOJA_ANNETTU_MUTTA_PESA_SIJ_EI_PONTTO);
				}
				if (ponttoalue_nro.hasValue()) {
					error(ponttoalue_nro, PesakorttiConst.PONTON_TIETOJA_ANNETTU_MUTTA_PESA_SIJ_EI_PONTTO);
				}
				if (pontto_ripustus.hasValue()) {
					error(pontto_ripustus, PesakorttiConst.PONTON_TIETOJA_ANNETTU_MUTTA_PESA_SIJ_EI_PONTTO);
				}
			}
		}

		if (notNullNoErrors(pontto_ripustus)) {
			checkValidYear(pontto_ripustus);
		}
		_Column tarkV = tarkastus.get("vuosi");
		if (notNullNoErrors(pontto_ripustus) && notNullNoErrors(tarkV)) {
			if (pontto_ripustus.getIntegerValue() > tarkV.getIntegerValue()) {
				error(pontto_ripustus, PesakorttiConst.PONTON_RIPUSTUS_EI_VOI_OLLA_ENNEN_TARK_VUOTTA);
			}
		}
	}

	private void checkPesanSijaintiAndTreeNestMeasurements() {
		_Column pesa_sij = pesa.get("pesa_sijainti");
		warningIfNull(pesa_sij);
		if (pesa_sij.getValue().equals("0")) {
			warning(pesa_sij, ARE_YOU_SURE_VALUE_IS_NULL);
		}
		if (isNull(pesa_sij)) {
			return;
		}

		PesasijaintiValidatorSpec spec = PesakorttiConst.PESAN_SIJAINTI_SPECS.get(pesa_sij.getValue());
		if (spec == null) throw new IllegalStateException("No spec defined for pesa sijainti: " +pesa_sij.getValue());

		_Column pesa_kork = tarkastus.get("pesa_korkeus");
		_Column puu_kork = tarkastus.get("puu_korkeus");
		_Column pesa_etaisyys_runko = tarkastus.get("pesa_etaisyys_runko");

		if (notNullNoErrors(pesa_kork)) {
			if (ignoreNestHeight(pesa_sij, tarkastus.get("pesiva_laji"))) {

			} else {
				checkRange(pesa_kork, spec.allowedNestHeightRange);
				if (getError(pesa_kork).equals(TOO_LARGE) && noErrors(pesa_sij)) {
					error(pesa_sij, PesakorttiConst.NEST_TOO_HIGH_FOR_THIS_TYPE_OF_NEST);
					error(pesa_kork, PesakorttiConst.NEST_TOO_HIGH_FOR_THIS_TYPE_OF_NEST);
				}
				if (warningNestHeightDefined(spec) && noErrors(pesa_kork)) {
					checkRange(pesa_kork, spec.noWarningsNestHeightRange);
					if (hasErrors(pesa_kork)) {
						removeError(pesa_kork);
						warning(pesa_kork, ARE_YOU_SURE);
					}
				}
			}
		}
		if (!spec.inTree) {
			if (!isNull(puu_kork)) {
				error(puu_kork, PesakorttiConst.TREE_INFO_CAN_NOT_BE_GIVEN_FOR_THIS_TYPE_OF_NEST);
			}
			if (!isNull(pesa_etaisyys_runko)) {
				error(pesa_etaisyys_runko,PesakorttiConst.TREE_INFO_CAN_NOT_BE_GIVEN_FOR_THIS_TYPE_OF_NEST);
			}
		} else {
			if (notNullNoErrors(puu_kork)) {
				checkRange(puu_kork, spec.allowedTreeHeightRange);
				if (getError(puu_kork).equals(TOO_LARGE) && noErrors(pesa_sij)) {
					error(pesa_sij, PesakorttiConst.TREE_TOO_HIGH_FOR_THIS_TYPE_OF_NEST);
					error(puu_kork, PesakorttiConst.TREE_TOO_HIGH_FOR_THIS_TYPE_OF_NEST);
				}
				if (warningTreeHeightDefined(spec) && noErrors(puu_kork)) {
					checkRange(puu_kork, spec.noWarningsTreeHeightRange);
					if (hasErrors(puu_kork)) {
						removeError(puu_kork);
						warning(puu_kork, ARE_YOU_SURE);
					}
				}
			}
			if (notNullNoErrors(pesa_etaisyys_runko)) {
				checkRange(pesa_etaisyys_runko, spec.allowedNestDistanceFromTreeRange);
				if (warningTreeDistanceFromTreeDefined(spec) && noErrors(pesa_etaisyys_runko)) {
					checkRange(pesa_etaisyys_runko, spec.noWarningsNestDistanceFromTreeRange);
					if (hasErrors(pesa_etaisyys_runko)) {
						removeError(pesa_etaisyys_runko);
						warning(pesa_etaisyys_runko, ARE_YOU_SURE);
					}
				}
			}
		}
	}

	private boolean ignoreNestHeight(_Column pesa_sij, _Column pesiva_laji) {
		return pesiva_laji.getValue().equals("RIPRIP") && pesa_sij.getValue().equals(KIVENKOLO_MAAKOLO);
	}

	private boolean warningNestHeightDefined(PesasijaintiValidatorSpec spec) {
		return spec.noWarningsNestHeightRange != null;
	}

	private boolean warningTreeHeightDefined(PesasijaintiValidatorSpec spec) {
		return spec.noWarningsTreeHeightRange != null;
	}

	private boolean warningTreeDistanceFromTreeDefined(PesasijaintiValidatorSpec spec) {
		return spec.noWarningsNestDistanceFromTreeRange != null;
	}

	private void checkLomake_seurTarkastaja() {
		if (pesa.get("lomake").getValue().equals(YES)) {
			checkNotNull(pesa.get("seur_tarkastaja"));
		}
	}

	private void checkKoordTarkkuus_koordMittaustapa() {
		if (kirjekyyhkyValidationsOnly) return;
		_Column tarkkuus = pesa.get("koord_tarkkuus");
		_Column mittaustapa = pesa.get("koord_mittaustapa");
		if (notNullNoErrors(tarkkuus)) {
			checkRange(tarkkuus, 1, PesakorttiConst.MAX_KOORD_TARKKUUS);
			if (notNullNoErrors(mittaustapa)) {
				if (tarkkuus.getIntegerValue() > WARNING_THRESHOLD_FOR_KOORD_TARKKUUS_FOR_MITTAUSTAPA_M && !mittaustapa.getValue().equals(MUU)) {
					warning(mittaustapa, _Validator.ARE_YOU_SURE);
				}
				if (tarkkuus.getIntegerValue() < WARNING_THRESHOLD_FOR_KOORD_TARKKUUS_FOR_MITTAUSTAPA_M && mittaustapa.getValue().equals(MUU)) {
					warning(mittaustapa, _Validator.ARE_YOU_SURE);
				}
				if (mittaustapa.getValue().equals(KARTTA)) {
					if (tarkkuus.getIntegerValue() < WARNING_THRESHOLD_FOR_KOORD_TARKKUUS_FOR_MITTAUSTAPA_K) {
						warning(mittaustapa, _Validator.ARE_YOU_SURE);
					}
				}
			}
		}
	}

	private void checkIds() {
		String action = data.action();
		if (!action.equals(Const.INSERT_FIRST)) {
			if (!kirjekyyhkyValidationsOnly) {
				checkNotNull(pesa.getUniqueNumericIdColumn());
			}
		}
		if (action.equals(Const.UPDATE)) {
			checkNotNull(tarkastus.getUniqueNumericIdColumn());
		}
	}

	private void checkCoordinates() throws Exception {
		_Column koord_tyyppi = params.getColumn("pesa.koord_ilm_tyyppi");
		if (kirjekyyhkyValidationsOnly) {
			checkNotNull(koord_tyyppi);
		} else {
			if (isNull(koord_tyyppi) && !otherCoordValuesGiven()) {
				return;
			}
			if (!isNull(koord_tyyppi) && !otherCoordValuesGiven()) {
				koord_tyyppi.clearValue();
				return;
			}
			if (otherCoordValuesGiven()) {
				checkNotNull(pesa.get("koord_ilm_tyyppi"));
				warningIfNull(pesa.get("koord_mittaustapa"));
				checkNotNull(pesa.get("koord_tarkkuus"));
			}
		}

		_Column leveys = null, pituus = null;
		int leveys_min, leveys_max, pituus_min, pituus_max;
		CoordinateConverter converter = new CoordinateConverter();
		boolean okToCheckInsideReagion = false;

		if (koord_tyyppi.getValue().equals("Y")) {
			leveys = params.getColumn("pesa.yht_leveys");
			pituus = params.getColumn("pesa.yht_pituus");
			leveys_min = _Validator.YHT_LEVEYS_MIN;
			leveys_max = _Validator.YHT_LEVEYS_MAX;
			pituus_min = _Validator.YHT_PITUUS_MIN;
			pituus_max = _Validator.YHT_PITUUS_MAX;
			checkCoordinateRanges(leveys, pituus, leveys_min, leveys_max, pituus_min, pituus_max);
			if (noErrors(leveys) && noErrors(pituus)) {
				converter.setYht_leveys(leveys.getValue());
				converter.setYht_pituus(pituus.getValue());
				okToCheckInsideReagion = true;
			}
		} else if (koord_tyyppi.getValue().equals("E")) {
			leveys = params.getColumn("pesa.eur_leveys");
			pituus = params.getColumn("pesa.eur_pituus");
			leveys_min = _Validator.EUR_LEVEYS_MIN;
			leveys_max = _Validator.EUR_LEVEYS_MAX;
			pituus_min = _Validator.EUR_PITUUS_MIN;
			pituus_max = _Validator.EUR_PITUUS_MAX;
			checkCoordinateRanges(leveys, pituus, leveys_min, leveys_max, pituus_min, pituus_max);
			if (noErrors(leveys) && noErrors(pituus)) {
				converter.setEur_leveys(leveys.getValue());
				converter.setEur_pituus(pituus.getValue());
				okToCheckInsideReagion = true;
			}
		} else if (koord_tyyppi.getValue().equals("A")) {
			leveys = params.getColumn("pesa.ast_leveys");
			pituus = params.getColumn("pesa.ast_pituus");
			leveys_min = _Validator.AST_LEVEYS_MIN;
			leveys_max = _Validator.AST_LEVEYS_MAX;
			pituus_min = _Validator.AST_PITUUS_MIN;
			pituus_max = _Validator.AST_PITUUS_MAX;
			checkCoordinateRanges(leveys, pituus, leveys_min, leveys_max, pituus_min, pituus_max);
			if (noErrors(leveys) && noErrors(pituus)) {
				converter.setAst_leveys(leveys.getValue());
				converter.setAst_pituus(pituus.getValue());
				okToCheckInsideReagion = true;
			}
		}

		if (okToCheckInsideReagion && noErrors("pesa.kunta")) {
			checkInsideReagion(params.getColumn("pesa.kunta").getValue(), leveys, pituus, converter);
		}
	}

	private boolean otherCoordValuesGiven() {
		return !isNull(
				params.getColumn("pesa.eur_pituus"), params.getColumn("pesa.eur_leveys"),
				params.getColumn("pesa.yht_pituus"), params.getColumn("pesa.yht_leveys"),
				params.getColumn("pesa.ast_pituus"), params.getColumn("pesa.ast_leveys"),
				params.getColumn("pesa.koord_tarkkuus"), params.getColumn("pesa.koord_mittaustapa")
				);
	}

	public void setKirjekyyhkyValidation(boolean value) {
		this.kirjekyyhkyValidationsOnly = value;
	}

}
