package fi.hy.pese.ulko.main.functionality;

import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.app.functionality.BaseFunctionality;
import fi.hy.pese.framework.main.app.functionality.validate.SearchFunctionalityValidator;
import fi.hy.pese.framework.main.app.functionality.validate._Validator;
import fi.hy.pese.framework.main.db._ResultsHandler;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.ulko.main.app._UlkoDomainModelRow;
import fi.hy.pese.ulko.main.db.UlkoRowsToDataResultsHandler;
import fi.luomus.commons.config.Config;

public class UlkoSearchFunctionality extends BaseFunctionality<_UlkoDomainModelRow> {
	
	public UlkoSearchFunctionality(_Request<_UlkoDomainModelRow> request) {
		super(request);
		try {
			if (request.config().defines(Config.MAX_RESULT_COUNT)) {
				Integer.valueOf(request.config().get(Config.MAX_RESULT_COUNT));
			} else {
			}
		} catch (Exception e) {
		}
	}
	
	@Override
	public _Validator validator() {
		return new SearchFunctionalityValidator<>(request);
	}
	
	@Override
	public void afterSuccessfulValidation() throws Exception {
		String action = data.action();
		if (action.equals(Const.SEARCH)) {
			performStandardSearchForSearchparameters();
		}
	}
	
	private void performStandardSearchForSearchparameters() throws Exception {
		_ResultsHandler handler = new UlkoRowsToDataResultsHandler(data);
		dao.executeSearch(data.searchparameters(), handler);
	}
	
	@Override
	public void afterFailedValidation() throws Exception {
		data.setAction("");
	}
	
}
