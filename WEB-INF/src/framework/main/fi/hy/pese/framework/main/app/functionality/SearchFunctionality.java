package fi.hy.pese.framework.main.app.functionality;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.app.functionality.validate.SearchFunctionalityValidator;
import fi.hy.pese.framework.main.app.functionality.validate._Validator;
import fi.hy.pese.framework.main.db.ResultsToReportHandler;
import fi.hy.pese.framework.main.db.RowsToDataResultHandler;
import fi.hy.pese.framework.main.db._ResultsHandler;
import fi.hy.pese.framework.main.general.StringWrapper;
import fi.hy.pese.framework.main.general._PDFDataMapGenerator;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.consts.Success;
import fi.hy.pese.framework.main.general.data.UsedSlots;
import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.main.general.data._ResultSorter;
import fi.hy.pese.framework.main.general.data._Table;
import fi.hy.pese.framework.main.kirjekyyhky._KirjekyyhkyXMLGenerator;
import fi.luomus.commons.config.Config;
import fi.luomus.commons.containers.KeyValuePair;
import fi.luomus.commons.kirjekyyhky.KirjekyyhkyAPIClient;
import fi.luomus.commons.pdf.PDFWriter;
import fi.luomus.commons.utils.CoordinateConverter;
import fi.luomus.commons.utils.FileCompresser;

public abstract class SearchFunctionality extends BaseFunctionality<_PesaDomainModelRow> {
	
	private static final int	UNLIMITED	= -1;
	private static final int	DEFAULT_MAX_RESULT_COUNT	= UNLIMITED;
	private int					maxResultCountToPutToData;
	
	public SearchFunctionality(_Request<_PesaDomainModelRow> request) {
		super(request);
		try {
			if (request.config().defines(Config.MAX_RESULT_COUNT)) {
				maxResultCountToPutToData = Integer.valueOf(request.config().get(Config.MAX_RESULT_COUNT));
			} else {
				maxResultCountToPutToData = DEFAULT_MAX_RESULT_COUNT;
			}
		} catch (Exception e) {
			maxResultCountToPutToData = DEFAULT_MAX_RESULT_COUNT;
		}
	}
	
	/**
	 * Application specific order of results after processing full tarkastus search
	 * @param results
	 * @return
	 * @throws Exception
	 */
	protected abstract _ResultSorter<_PesaDomainModelRow> sorter() throws Exception;
	
	/**
	 * Application specific order in which PDFs (esitäytetty lomake) are printed.
	 * By default the same as sorter(), but can be overriden by the application.
	 * @return
	 * @throws Exception
	 */
	protected _ResultSorter<_PesaDomainModelRow> pdfPrintingSorter() throws Exception {
		return sorter();
	}
	
	/**
	 * Can be overriden by the implementing application, if shown in UI of the application.
	 * Should return a number that represents the "output" of the nesting  (number of produced youngs,etc).
	 */
	public String nestingOutput(@SuppressWarnings("unused") _Table tarkastus) {
		return "maxPoikascount() not defined";
	}
	
	/**
	 * Should be overriden by the implementing application if the application wants to use print_pdf feature
	 */
	public _PDFDataMapGenerator pdfDataMapGenerator() {
		return null;
	}
	
	/**
	 * Returns query that results of nests that are considered "dead" by the implementing application
	 * @return
	 * @throws SQLException
	 */
	public abstract String deadNestsQuery();
	
	/**
	 * Returns query that results of nests that are considered "alive" by the implementing application
	 * @return
	 * @throws SQLException
	 */
	public abstract String liveNestsQuery();
	
	/**
	 * Application specific handling for this report
	 * @param reportName
	 */
	protected abstract void applicationPrintReport(String reportName);
	
	@Override
	public _Validator validator() {
		return new SearchFunctionalityValidator<>(request);
	}
	
	@Override
	public void preProsessingHook() throws Exception {
		//		if (!data.page().equals(Const.REPORTS_PAGE)) {
		//			data.setPage(Const.SEARCH_PAGE);
		//		}
		
		if (data.contains(Const.PRINT_TO_TEXTFILE)) {
			data.setAction(Const.PRINT_TO_TEXTFILE);
		}
		
		if (!data.contains(Const.COORDINATE_SEARCH_RADIUS)) {
			setDefaultCoordinateRadius();
		}
		
		if (!data.contains(Const.NEST_CONDITION)) {
			setDefaultNestCondition();
		}
		
		if (data.contains(Const.SELECTED_FIELDS)) {
			generateSelectedFieldsList();
		}
		
		if (!data.contains(Const.SEARCH_VALUES)) {
			generateSearchvaluesList();
		}
		
		if (data.action().equals(Const.EXECUTE_SAVED_SEARH)) {
			setSavedSearchParameters();
			data.setAction(Const.SEARCH);
			updateLastUsedValueOfSavedSearch();
		}
		
		if (data.page().equals(Const.REPORTS_PAGE) && data.contains(Const.PRINT_PDF)) {
			data.setAction(Const.PRINT_PDF);
		}
		
		if (data.page().equals(Const.REPORTS_PAGE) && data.contains(Const.PRINT_PDF_FULL)) {
			data.setAction(Const.PRINT_PDF);
			data.set(Const.PRINT_PDF_FULL, Const.YES);
		}
		
		if (data.page().equals(Const.REPORTS_PAGE) && !data.contains(Const.SELECTED_REPORTS)) {
			data.set(Const.SELECTED_REPORTS, "");
		}
	}
	
	private void updateLastUsedValueOfSavedSearch() throws SQLException {
		dao.executeUpdate(" UPDATE saved_searches SET last_used = SYSDATE WHERE id = ? ", data.get("id"));
	}
	
	private void setDefaultCoordinateRadius() {
		data.set(Const.COORDINATE_SEARCH_RADIUS, "");
	}
	
	private void setDefaultNestCondition() {
		if (data.page().equals(Const.REPORTS_PAGE)) {
			data.set(Const.NEST_CONDITION, Const.NEST_CONDITION__ONLY_LIVE_NESTS);
		} else {
			data.set(Const.NEST_CONDITION, Const.NEST_CONDITION__ALL);
		}
	}
	
	private void setSavedSearchParameters() {
		String[] searchValues = data.get(Const.SEARCH_VALUES).split(Const.SEPARATOR);
		for (String searchValue : searchValues) {
			String[] s = searchValue.split("=");
			if (s.length != 2) continue;
			if (data.searchparameters().containsColumn(s[0])) {
				data.searchparameters().getColumn(s[0]).setValue(s[1]);
			} else {
				data.set(s[0], s[1]);
			}
		}
	}
	
	private void generateSearchvaluesList() {
		StringBuilder searchValues = new StringBuilder();
		for (_Table t : data.searchparameters()) {
			if (!t.hasValues()) continue;
			for (_Column c : t) {
				if (c.hasValue()) {
					searchValue(searchValues, c.getFullname(), c.getValue());
				}
			}
		}
		if (data.contains(Const.COORDINATE_SEARCH_RADIUS)) {
			searchValue(searchValues, Const.COORDINATE_SEARCH_RADIUS, data.get(Const.COORDINATE_SEARCH_RADIUS));
		}
		if (data.contains(Const.NEST_CONDITION)) {
			searchValue(searchValues, Const.NEST_CONDITION, data.get(Const.NEST_CONDITION));
		}
		if (data.contains(Const.SUURALUE)) {
			searchValue(searchValues, Const.SUURALUE, data.get(Const.SUURALUE));
		}
		if (data.contains(Const.YMPARISTOKESKUS)) {
			searchValue(searchValues, Const.YMPARISTOKESKUS, data.get(Const.YMPARISTOKESKUS));
		}
		fi.hy.pese.framework.main.general.Utils.removeLastSeparator(searchValues);
		data.set(Const.SEARCH_VALUES, searchValues.toString().trim());
	}
	
	private void searchValue(StringBuilder searchValues, String field, String value) {
		searchValues.append(field).append("=").append(value).append(Const.SEPARATOR);
	}
	
	private void generateSelectedFieldsList() {
		String pesaidColumnName = dao.newRow().getPesa().getUniqueNumericIdColumn().getFullname();
		boolean pesaidColumnIncluded = false;
		String[] selectedFields = data.get(Const.SELECTED_FIELDS).split(",");
		for (String s : selectedFields) {
			s = s.trim();
			if (s.length() < 1) continue;
			data.addToList(Const.SELECTED_FIELDS, new StringWrapper(s));
			if (s.equalsIgnoreCase(pesaidColumnName)) {
				pesaidColumnIncluded = true;
			}
		}
		if (!pesaidColumnIncluded) {
			data.addToList(Const.SELECTED_FIELDS, new StringWrapper(pesaidColumnName));
		}
	}
	
	@Override
	public void afterSuccessfulValidation() throws Exception {
		String action = data.action();
		if (action.equals(Const.SEARCH)) {
			performStandardSearchForSearchparameters(maxResultCountToPutToData);
		} else if (action.equals(Const.PRINT_TO_TEXTFILE)) {
			if (data.getLists().get(Const.SELECTED_FIELDS) == null) return;
			printToTextfile();
		} else if (action.equals(Const.SAVE_SEARCH)) {
			saveSearch();
		} else if (action.equals(Const.DELETE_SAVED_SEARCH)) {
			deleteSearch();
		} else if (action.equals(Const.PRINT_PDF)) {
			printPdfs();
		} else if (action.equals(Const.PRINT_REPORT)) {
			printReports();
		}
	}
	
	private void performStandardSearchForSearchparameters(int maxResultCountToPutToData) throws Exception {
		performStandardSearchForSearchparameters(maxResultCountToPutToData, sorter());
	}
	
	private void performStandardSearchForSearchparameters(int maxResultCountToPutToData, _ResultSorter<_PesaDomainModelRow> sorter) throws Exception {
		_ResultsHandler handler = new RowsToDataResultHandler(data, dao, maxResultCountToPutToData);
		searchAndSort(handler, sorter);
		addCountsToData();
	}
	
	private void addCountsToData() {
		int rowcount = 0;
		int pesacount = 0;
		String prev_pesaid = "";
		UsedSlots maxResultTypes = data.getCountOfUsedSlots();
		for (_PesaDomainModelRow row : data.results()) {
			String pesa_id = row.getPesa().getUniqueNumericIdColumn().getValue();
			if (!pesa_id.equals(prev_pesaid)) {
				pesacount++;
				prev_pesaid = pesa_id;
			}
			String nestingOutput = nestingOutput(row.getTarkastus());
			String tarkastusid = row.getTarkastus().getUniqueNumericIdColumn().getValue();
			data.set(Const.MAX_POIKAS_COUNT_FOR_TARKASTUS_ + tarkastusid, nestingOutput);
			rowcount++;
			
			addKayntiCount(maxResultTypes, row);
			addAikuinenCount(maxResultTypes, row);
			addPoikanenCount(maxResultTypes, row);
			addMunaCount(maxResultTypes, row);
		}
		data.set(Const.ROW_COUNT, Integer.toString(rowcount));
		data.set(Const.ITEM_COUNT, Integer.toString(pesacount));
	}
	
	private void addKayntiCount(UsedSlots maxResultTypes, _PesaDomainModelRow row) {
		int usedKayntiCount = count(row.getKaynnit().values());
		if (maxResultTypes.getUsedKaynnit() == Integer.MAX_VALUE || maxResultTypes.getUsedKaynnit() < usedKayntiCount) {
			maxResultTypes.setUsedKaynnit(usedKayntiCount);
		}
	}
	private void addAikuinenCount(UsedSlots maxResultTypes, _PesaDomainModelRow row) {
		int usedAikuinenCount = count(row.getAikuiset().values());
		if (maxResultTypes.getUsedAikuiset() == Integer.MAX_VALUE || maxResultTypes.getUsedAikuiset() < usedAikuinenCount) {
			maxResultTypes.setUsedAikuiset(usedAikuinenCount);
		}
	}
	private void addPoikanenCount(UsedSlots maxResultTypes, _PesaDomainModelRow row) {
		int usedPoikanenCount = count(row.getPoikaset().values());
		if (maxResultTypes.getUsedPoikaset() == Integer.MAX_VALUE || maxResultTypes.getUsedPoikaset() < usedPoikanenCount) {
			maxResultTypes.setUsedPoikaset(usedPoikanenCount);
		}
	}
	private void addMunaCount(UsedSlots maxResultTypes, _PesaDomainModelRow row) {
		int usedMunaCount = count(row.getMunat().values());
		if (maxResultTypes.getUsedMunat() == Integer.MAX_VALUE || maxResultTypes.getUsedMunat() < usedMunaCount) {
			maxResultTypes.setUsedMunat(usedMunaCount);
		}
	}
	
	private int count(Collection<_Table> tables) {
		int count = 0;
		for (_Table t : tables) {
			if (!t.hasValues()) break;
			count++;
		}
		return count;
	}
	
	/**
	 * Does a "full" database row search with data.searchparameters with additional queries such as coordinate radius search and
	 * live/dead nests. Sorts data.results afterwards using the provided sorter.
	 * @param resultHandler used to handle the results
	 * @param sorter used to sort data.results after the search is done; can be null in which case no sorting is done
	 *            (for example if result handler does not put anything to data.results
	 * @throws Exception
	 */
	public void searchAndSort(_ResultsHandler resultHandler, _ResultSorter<_PesaDomainModelRow> sorter) throws Exception {
		List<String> additionalQueries = new ArrayList<>();
		addNestConditionQuery(additionalQueries);
		additionalSearchConditionsHook(additionalQueries);
		CoordinateRestorer coordinateRestorer = addCoordinateRadiusSearchQueryAndGiveCoordinateRestorer(additionalQueries);
		dao.executeSearch(data.searchparameters(), resultHandler, additionalQueries.toArray(new String[additionalQueries.size()]));
		if (coordinateRestorer != null) {
			coordinateRestorer.restore();
		}
		if (sorter != null) {
			data.setResults(sorter.sort(data.results()));
		}
	}
	
	/**
	 * Applications can add their own custom search condition queries by overriding this method. Add a valid SQL where -statement
	 * to the list (the contents of the list will be catenaded together like this: <stmn1> AND <stmt2> AND ...).
	 * @param additionalQueries
	 */
	protected void additionalSearchConditionsHook(List<String> additionalQueries) { }
	
	/**
	 * Does a "full" database row search with data.searchparameters with additional queries such as coordinate radius search and
	 * live/dead nests.
	 * @param resultHandler used to handle the results
	 * @throws Exception
	 */
	public void search(_ResultsHandler resultHandler) throws Exception {
		searchAndSort(resultHandler, null);
	}
	
	private void addNestConditionQuery(List<String> additionalQueries) {
		String nestCondition = data.get(Const.NEST_CONDITION);
		if (nestCondition.equals(Const.NEST_CONDITION__ALL)) return;
		if (nestCondition.length() < 1) return;
		
		if (nestCondition.equals(Const.NEST_CONDITION__ONLY_DESTROYED_NESTS)) {
			additionalQueries.add(deadNestsQuery());
		} else if (nestCondition.equals(Const.NEST_CONDITION__ONLY_LIVE_NESTS)) {
			additionalQueries.add(liveNestsQuery());
		}
	}
	
	/**
	 * Used to temporarily store and restore coordinates given as parameter after coordinate radius search
	 */
	private class CoordinateRestorer {
		private final _Column	leveys;
		private final _Column	pituus;
		private final String	leveys_original_value;
		private final String	pituus_original_value;
		
		public CoordinateRestorer(_Column leveys, _Column pituus) {
			this.leveys = leveys;
			this.pituus = pituus;
			this.leveys_original_value = leveys.getValue();
			this.pituus_original_value = pituus.getValue();
			this.leveys.setValue("");
			this.pituus.setValue("");
		}
		
		public void restore() {
			this.leveys.setValue(leveys_original_value);
			this.pituus.setValue(pituus_original_value);
		}
	}
	
	private CoordinateRestorer addCoordinateRadiusSearchQueryAndGiveCoordinateRestorer(List<String> additionalQueries) throws Exception {
		if (!coordinateSearchRadiusGiven()) return null;
		
		_Table pesa = data.searchparameters().getPesa();
		_Column yht_leveys = pesa.get("yht_leveys");
		_Column yht_pituus = pesa.get("yht_pituus");
		_Column eur_leveys = pesa.get("eur_leveys");
		_Column eur_pituus = pesa.get("eur_pituus");
		_Column ast_leveys = pesa.get("ast_leveys");
		_Column ast_pituus = pesa.get("ast_pituus");
		
		_Column leveys = null;
		_Column pituus = null;
		
		if (yht_leveys.hasValue() && yht_pituus.hasValue()) {
			leveys = yht_leveys;
			pituus = yht_pituus;
		} else if (eur_leveys.hasValue() && eur_pituus.hasValue()) {
			leveys = eur_leveys;
			pituus = eur_pituus;
		} else if (ast_leveys.hasValue() && ast_pituus.hasValue()) {
			leveys = ast_leveys;
			pituus = ast_pituus;
			CoordinateConverter converter = new CoordinateConverter();
			converter.setAst_leveys(ast_leveys.getValue());
			converter.setAst_pituus(ast_pituus.getValue());
			converter.convert();
			
			StringBuilder statement = new StringBuilder();
			statement.append("SQRT( ");
			statement.append("POWER(").append(converter.getYht_leveys()).append(" - ").append(yht_leveys.getFullname()).append(", 2) + ");
			statement.append("POWER(").append(converter.getYht_pituus()).append(" - ").append(yht_pituus.getFullname()).append(", 2) ");
			statement.append(") <= ").append(data.get(Const.COORDINATE_SEARCH_RADIUS));
			additionalQueries.add(statement.toString());
			
			return new CoordinateRestorer(leveys, pituus);
		}
		else
			throw new Exception("Coordinate radius given but no coordinates. Validator should make sure this doesn't happen.");
		
		StringBuilder statement = new StringBuilder();
		statement.append("SQRT( ");
		statement.append("POWER(").append(leveys.getValue()).append(" - ").append(leveys.getFullname()).append(", 2) + ");
		statement.append("POWER(").append(pituus.getValue()).append(" - ").append(pituus.getFullname()).append(", 2) ");
		statement.append(") <= ").append(data.get(Const.COORDINATE_SEARCH_RADIUS));
		additionalQueries.add(statement.toString());
		
		return new CoordinateRestorer(leveys, pituus);
	}
	
	protected void printReports() throws Exception {
		performStandardSearchForSearchparameters(UNLIMITED);
		for (String reportName : data.get(Const.SELECTED_REPORTS).split("\\|")) {
			reportName = reportName.trim();
			if (reportName.length() < 1) continue;
			printReport(reportName);
		}
		data.clearResults();
	}
	
	private void printReport(String reportName) {
		applicationPrintReport(reportName);
	}
	
	private void printToTextfile() throws FileNotFoundException, Exception {
		ResultsToReportHandler handler = new ResultsToReportHandler(request);
		List<KeyValuePair> additionalParams = new ArrayList<>();
		for (String paramPair : data.get(Const.SEARCH_VALUES).split(Const.SEPARATOR)) {
			String[] paramPairValues = paramPair.split("=");
			additionalParams.add(new KeyValuePair(paramPairValues[0], paramPairValues[1]));
		}
		handler.setAdditionalSearchParams(additionalParams);
		search(handler);
		data.searchparameters().getPesa().getUniqueNumericIdColumn().setValue(""); // TODO tätä ei pitäisi tehdä.. haavin sisältö pitäisi välittää muulla tavoin
	}
	
	private boolean coordinateSearchRadiusGiven() {
		return !data.get(Const.COORDINATE_SEARCH_RADIUS).equals("");
	}
	
	private void printPdfs() throws Exception {
		List<String> ids = generateSortedListOfIdsToBePrinted();
		if (ids.size() < 1) {
			data.addToNoticeTexts(Const.NO_RESULTS);
			return;
		}
		PDFWriter pdfWriter = request.pdfWriter();
		printIndividualPdfsAndMaybeSendToKirjekyyhky(ids, pdfWriter);
		createCollectionPdfAndZipFile(pdfWriter);
		data.addToProducedFiles(pdfWriter.producedFiles());
		data.addToNoticeTexts(pdfWriter.noticeTexts());
	}
	
	private void createCollectionPdfAndZipFile(PDFWriter pdfWriter) throws IOException {
		if (pdfWriter.producedFiles().isEmpty()) return;
		
		if (!data.contains(Const.COLLECTION_FILENAME)) return;
		String collectionFilename = data.get(Const.COLLECTION_FILENAME);
		
		pdfWriter.createCollection(collectionFilename);
		
		File zipFile = new File(config.pdfFolder() + File.separator + collectionFilename + ".zip");
		if (zipFile.exists()) {
			zipFile.delete();
		}
		FileCompresser compresser = null;
		try {
			compresser = new FileCompresser(zipFile);
			for (String filename : pdfWriter.producedFiles()) {
				try {
					compresser.addToZip(new File(config.pdfFolder() + File.separator + filename));
				} catch (FileNotFoundException e) {	}
			}
		} finally {
			if (compresser != null) compresser.close();
		}
		data.producedFiles().add(zipFile.getName());
	}
	
	private void printIndividualPdfsAndMaybeSendToKirjekyyhky(List<String> ids, PDFWriter pdfWriter) throws Exception {
		String year = data.get(Const.YEAR);
		_KirjekyyhkyXMLGenerator xmlGenerator = null;
		KirjekyyhkyAPIClient api = null;
		if (Const.YES.equals(data.get(Const.GENERATE_KIRJEKYYHKY_FORM_DATA_XML))) {
			xmlGenerator = request.kirjekyyhkyFormDataXMLGenerator();
			api = request.kirjekyyhkyAPI();
		}
		try {
			for (String id : ids) {
				try {
					Map<String, String> dataInMap = null;
					if (printingFullPDFs()) {
						dataInMap = pdfDataMapGenerator().tarkastusDataFor(id);
					} else {
						dataInMap = pdfDataMapGenerator().tarkastusDataFor(id, year);
					}
					String filename = dataInMap.get(Const.FILENAME);
					File pdfFile = pdfWriter.writePdf(Const.TARKASTUS_PAGE, dataInMap, filename);
					if (xmlGenerator != null && api != null) {
						File xmlFile = xmlGenerator.writeFormDataXML(dataInMap, filename);
						if (xmlFile != null) {
							api.send(xmlFile, pdfFile);
						}
					}
				} catch (Exception e) {
					data.addToNoticeTexts("ERROR: " + e.getMessage() + " : " + e.getStackTrace()[0]);
					e.printStackTrace();
				}
			}
		} finally {
			if (api != null) {
				api.close();
			}
		}
	}
	
	private boolean printingFullPDFs() {
		return Const.YES.equals(data.get(Const.PRINT_PDF_FULL));
	}
	
	private List<String> generateSortedListOfIdsToBePrinted() throws Exception {
		performStandardSearchForSearchparameters(UNLIMITED, pdfPrintingSorter());
		
		List<String> ids = new ArrayList<>();
		if (printingFullPDFs()) {
			for (_PesaDomainModelRow r : data.results()) {
				ids.add(r.getTarkastus().getUniqueNumericIdColumn().getValue());
			}
		} else {
			String prev_id = "";
			for (_PesaDomainModelRow r : data.results()) {
				String id = r.getPesa().getUniqueNumericIdColumn().getValue();
				if (id.equals(prev_id)) continue;
				ids.add(id);
				prev_id = id;
			}
		}
		data.clearResults();
		return ids;
	}
	
	private void deleteSearch() throws SQLException {
		dao.executeUpdate(" DELETE FROM saved_searches WHERE id = ? ", data.get("id"));
		data.setSuccessText(Success.DELETE);
	}
	
	private void saveSearch() throws SQLException {
		_Table searchToBeSaved = dao.newTableByName("saved_searches");
		searchToBeSaved.getUniqueNumericIdColumn().setValue(dao.returnAndSetNextId(searchToBeSaved));
		searchToBeSaved.get("userid").setValue(data.userId());
		searchToBeSaved.get("last_used").setValue(Const.SYSDATE);
		searchToBeSaved.get("name").setValue(data.get(Const.SEARCH_NAME));
		searchToBeSaved.get("description").setValue(data.get(Const.SEARCH_DESCRIPTION));
		searchToBeSaved.get("selected_fields").setValue(data.get(Const.SELECTED_FIELDS));
		searchToBeSaved.get("search_values").setValue(data.get(Const.SEARCH_VALUES));
		dao.executeInsert(searchToBeSaved);
		data.setSuccessText(Success.SAVED);
	}
	
	@Override
	public void afterFailedValidation() throws Exception {
		data.setAction("");
	}
	
	@Override
	public void postProsessingHook() throws Exception {
		if (!data.page().equals(Const.SEARCH_PAGE)) return;
		dao.addSavedSearchesListingToData(data);
		if (data.getLists().containsKey(Const.SELECTED_FIELDS)) {
			generateResultFieldsListToData();
		} else {
			data.getLists().put(Const.RESULT_COLUMNS, new ArrayList<>());
		}
	}
	
	private void generateResultFieldsListToData() {
		_PesaDomainModelRow structure = request.data().newRow();
		_Table kaynti = structure.getKaynti();
		_Table poikanen = structure.getPoikanen();
		_Table aikuinen = structure.getAikuinen();
		_Table muna = structure.getMuna();
		
		List<String> otherFields = new ArrayList<>();
		List<String> kayntiFields = new ArrayList<>();
		List<String> poikanenFields = new ArrayList<>();
		List<String> aikuinenFields = new ArrayList<>();
		List<String> munaFields = new ArrayList<>();
		
		UsedSlots usedSlots = data.getCountOfUsedSlots();
		
		for (Object o : data.getLists().get(Const.SELECTED_FIELDS)) {
			String field = o.toString();
			String columnName = columnPart(field);
			if (field.startsWith(kaynti.getName() + ".")) {
				kayntiFields.add(columnName);
			} else if (field.startsWith(poikanen.getName() + ".")){
				poikanenFields.add(columnName);
			} else if (field.startsWith(aikuinen.getName() + ".")){
				aikuinenFields.add(columnName);
			} else if (field.startsWith(muna.getName() + ".")){
				munaFields.add(columnName);
			} else {
				otherFields.add(field);
			}
		}
		
		
		
		for (String field : otherFields) {
			data.addToList(Const.RESULT_COLUMNS, field);
		}
		
		addToList(kayntiFields, structure.getKaynti(), usedSlots.getUsedKaynnit(), structure.getKaynnit().getStaticCount());
		addToList(poikanenFields, structure.getPoikanen(), usedSlots.getUsedPoikaset(), structure.getPoikaset().getStaticCount());
		addToList(aikuinenFields, structure.getAikuinen(), usedSlots.getUsedAikuiset(), structure.getAikuiset().getStaticCount());
		addToList(munaFields, structure.getMuna(), usedSlots.getUsedMunat(), structure.getMunat().getStaticCount());
	}
	
	private void addToList(List<String> fields, _Table table, int usedCount, int staticCount) {
		if (!table.isInitialized()) return;
		if (fields.isEmpty()) return;
		if (usedCount == Integer.MAX_VALUE) usedCount = staticCount;
		if (usedCount == 0) return;
		for (int count = 1; count <= usedCount; count++) {
			for (String field : fields) {
				data.addToList(Const.RESULT_COLUMNS, table.getName() + "." + count + "." + field);
			}
		}
	}
	
	private String columnPart(String field) {
		return field.substring(field.lastIndexOf(".") +1 );
	}
	
}
