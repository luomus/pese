package fi.hy.pese.saaksi.main.functionality.validator;

import java.sql.SQLException;
import java.util.Collection;
import java.util.HashSet;

import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.app.functionality.validate.PesimistulosValidationDefinition;
import fi.hy.pese.framework.main.app.functionality.validate.PesimistulosValidationDefinition.Havainto;
import fi.hy.pese.framework.main.app.functionality.validate.TarkastusValidator;
import fi.hy.pese.framework.main.app.functionality.validate._Validator;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.main.general.data._Row;
import fi.hy.pese.framework.main.general.data._Table;
import fi.hy.pese.saaksi.main.app.SaaksiConst;
import fi.hy.pese.saaksi.main.app.SaaksiConst.KoordTyyppi;
import fi.hy.pese.saaksi.main.db.SaaksiDAO;
import fi.luomus.commons.utils.CoordinateConverter;
import fi.luomus.commons.utils.Utils;

public class SaaksiTarkastusValidator extends TarkastusValidator {

	private static final String	PESIMISTULOS_LENTOPOIKASPESA	= "V";
	private static final String	PESAA_EI_TARKASTETTU_LAINKAAN	= "J";
	private static final boolean	ACCURACY_REQUIRED	= true;
	private static final Collection<String> PESA_VOI_SIJAITA_PUUSSA = Utils.collection("P", "B", "K", "L", "T", "C", "O");
	private final boolean kirjekyyhkyValidations;
	private final _PesaDomainModelRow params;

	public SaaksiTarkastusValidator(_Request<_PesaDomainModelRow> request) {
		this(request, false);
	}

	public SaaksiTarkastusValidator(_Request<_PesaDomainModelRow> request, boolean kirjekyyhkyValidationserviceValidationsOnly) {
		super(request);
		this.kirjekyyhkyValidations = kirjekyyhkyValidationserviceValidationsOnly;
		this.params = request.data().updateparameters();
	}

	@Override
	public void validate() throws Exception {
		if (kirjekyyhkyValidations) {
			validateSendingTheForm();
			copyAndRemoveError("pesa.yht_leveys", "koord_leveys");
			copyAndRemoveError("pesa.yht_pituus", "koord_pituus");
			copyAndRemoveError("pesa.eur_leveys", "koord_leveys");
			copyAndRemoveError("pesa.eur_pituus", "koord_pituus");
			copyAndRemoveError("pesa.ast_leveys", "koord_leveys");
			copyAndRemoveError("pesa.ast_pituus", "koord_pituus");
		} else {
			super.validate();
			if (hasErrors()) return;
			String action = data.action();
			if (action.equals(Const.FILL_FOR_INSERT)) {
				checkTarkastusVuosiValid_NoTarkastusForTheYear("tark_vuosi", data.get("tark_vuosi"));
				if (hasErrors()) return;
				checkNestIsNotDead("tark_vuosi", data.get("tark_vuosi"));
			} else if (action.equals(Const.UPDATE) || action.equals(Const.INSERT) || action.equals(Const.INSERT_FIRST)) {
				validateSendingTheForm();
			}
		}
	}

	private void checkNestIsNotDead(String tarkVuosiFieldName, String tark_vuosi) throws SQLException {
		if (hasErrors()) return;
		String pesa_id = data.updateparameters().getPesa().getUniqueNumericIdColumn().getValue();
		if (isDead(pesa_id)) {
			Integer yearNestMarkedDead = SaaksiDAO.yearNestHasDied(pesa_id, dao);
			if (Integer.valueOf(tark_vuosi) > yearNestMarkedDead) {
				error(tarkVuosiFieldName, SaaksiConst.PESA_IS_DEAD_CAN_NOT_INSERT);
			}
		}
	}

	private boolean isDead(String pesa_id) throws SQLException {
		Collection<String> deadNests = SaaksiDAO.deadNests(dao);
		return deadNests.contains(pesa_id);
	}

	private void checkTarkastusVuosiValid_NoTarkastusForTheYear(String tarkVuosiFieldName, String tark_vuosi) throws SQLException {
		if (!checkValidYear(tarkVuosiFieldName, tark_vuosi)) return;
		if (noErrors(tarkVuosiFieldName) && noErrors("pesa.pesaid") && doingInsert()) {
			String pesa_id = data.updateparameters().getColumn("pesa.pesaid").getValue();
			boolean tarkastusAlreadyExists = SaaksiDAO.checkTarkastusForTheYearExists(pesa_id, tark_vuosi, dao);
			if (tarkastusAlreadyExists) {
				error(tarkVuosiFieldName, CAN_NOT_INSERT_ALREADY_EXISTS);
			}
		}
	}

	private boolean doingInsert() {
		return (data.action().equals(Const.INSERT) || data.action().equals(Const.FILL_FOR_INSERT));
	}

	private void validateSendingTheForm() throws Exception {
		_PesaDomainModelRow params = data.updateparameters();
		String action = data.action();

		_Column pesaId = params.getColumn("pesa.pesaid");
		_Column vuosi = params.getColumn("tarkastus.vuosi");

		if (!action.equals(Const.INSERT_FIRST) && !kirjekyyhkyValidations) {
			checkNotNull(pesaId);
		}
		if (action.equals(Const.UPDATE)) {
			checkNotNull(params.getColumn("tarkastus.tarkastusid"));
		}

		checkAsUpdateparameters(params);
		checkReferenceValuesOfNonNullColumns(params, dao);

		if (!kirjekyyhkyValidations) {
			checkTarkastusVuosiValid_NoTarkastusForTheYear("tarkastus.vuosi", vuosi.getValue());
			checkNestIsNotDead("tarkastus.vuosi", vuosi.getValue());
		}

		warningIfNull(params.getColumn("pesa.pesanimi"));
		checkNotNull(params.getColumn("pesa.kunta"));
		checkNotNull(params.getColumn("pesa.koord_tyyppi"));
		checkNotNull(params.getColumn("pesa.lomake"));
		checkNotNull(params.getColumn("pesa.seur_rengastaja"));
		checkNotNull(params.getColumn("olosuhde.pesan_sijainti"));
		warningIfNull(params.getColumn("olosuhde.maastotyyppi"));
		warningIfNull(params.getColumn("olosuhde.puusto"));
		checkNotNull(params.getColumn("tarkastus.rengastaja"));
		checkNotNull(params.getColumn("tarkastus.vuosi"));
		checkNotNull(params.getColumn("tarkastus.tark_pvm"));
		checkNotNull(params.getColumn("tarkastus.tark_pvm_tark"));
		if (kirjekyyhkyValidations) {
			checkNotNull(params.getColumn("olosuhde.rauhoitustaulu"));
			checkNotNull(params.getColumn("pesa.koord_mittaustapa"));
		} else {
			checkNotNull(params.getColumn("vuosi.reviiriid"));
		}

		checkCoordinates(params);

		if (!kirjekyyhkyValidations) {
			_Column reviiri = request.data().updateparameters().getVuosi().get("reviiriid");
			if (request.data().contains("updateparameters.reviiri.reviirinimi") && notNullNoErrors(reviiri)) {
				String ilmoitettuReviirinimi = request.data().get("updateparameters.reviiri.reviirinimi");
				String nykyinenReviiriID = reviiri.getValue();
				String nykyinenReviirinimi = null;
				try {
					nykyinenReviirinimi = dao.returnTableByKeys("reviiri", nykyinenReviiriID).get("reviirinimi").getValue();
				} catch (SQLException e) {
					error(reviiri, INVALID_VALUE);
				}
				if (!ilmoitettuReviirinimi.equalsIgnoreCase(nykyinenReviirinimi)) {
					warning(params.getColumn("vuosi.reviiriid"), SaaksiConst.ILMOITETTU_REVIIRIN_NIMI_JA_REVIIRI_EIVAT_TASMAA);
				}
			}
		}

		_Column pesaSijainti = params.getOlosuhde().get("pesan_sijainti");
		_Column puulaji = params.getPesa().get("pesap_laji");
		_Column puuElavyys = params.getOlosuhde().get("pesap_elavyys");
		_Column latvaHalk = params.getOlosuhde().get("pesap_latvahalk");
		_Column latvaYmp = params.getOlosuhde().get("pesap_latvaymp");
		_Column tyviHalk = params.getOlosuhde().get("pesap_tyvihalk");
		_Column tyviYmp = params.getOlosuhde().get("pesap_tyviymp");

		checkValidYear(vuosi);
		_Column tarkPvm = params.getTarkastus().get("tark_pvm");
		checkTark_vuosiTheSameOrOneYearBeforeTarkPvm(vuosi, tarkPvm);

		if (notNullNoErrors(pesaSijainti)) {
			if (!PESA_VOI_SIJAITA_PUUSSA.contains(pesaSijainti.getValue())) {
				checkIsNull(SaaksiConst.TAMAN_TYYPPINEN_PESA_EI_VOI_SIJAITA_PUUSSA, puulaji, puuElavyys, tyviHalk, tyviYmp, latvaHalk, latvaYmp);
			}
		}
		checkValueAndAccuracyPair(latvaHalk, params.getOlosuhde().get("pesap_latvah_tark"), ACCURACY_REQUIRED);
		checkValueAndAccuracyPair(latvaYmp, params.getOlosuhde().get("pesap_latvay_tark"), ACCURACY_REQUIRED);
		checkValueAndAccuracyPair(tyviHalk, params.getOlosuhde().get("pesap_tyvih_tark"), ACCURACY_REQUIRED);
		checkValueAndAccuracyPair(tyviYmp, params.getOlosuhde().get("pesap_tyviy_tark"), ACCURACY_REQUIRED);

		if (kirjekyyhkyValidations && !isNull(params.getOlosuhde().get("puusto"))) {
			warningIfNull(params.getColumn("olosuhde.puust_kasaste"));
			warningIfNull(params.getColumn("olosuhde.puust_tiheys"));
		}

		_Column pesimistulos = params.getTarkastus().get("pesimistulos");
		if (SaaksiConst.ONNISTUMISKOODIT.contains(pesimistulos.getValue()) && !kirjekyyhkyValidations) {
			String reviiri_id = params.getColumn("vuosi.reviiriid").getValue();
			Collection<String> conflicts = SaaksiDAO.findOutIfThereAreOtherSuccessfulNestingsForReviiri(reviiri_id, vuosi.getValue(), pesaId.getValue(), dao);
			if (!conflicts.isEmpty()) {
				error(pesimistulos, SaaksiConst.REVIIRI_ALREADY_HAS_SUCCESSFUL_NESTING);
				for (String conflict : conflicts) {
					data.addToList(Const.RISTIRIIDAT, conflict);
				}
			}
		}

		if (kirjekyyhkyValidations) {
			crossCheckWithKaynnit(pesimistulos);
		} else {
			lkmVsPesimistulos(params, pesimistulos);
		}

		_Column pesimistulosTarkkuus = params.getTarkastus().get("pesimist_tark");
		_Column kunto = params.getTarkastus().get("kunto");
		_Column vaihtoPesanEtsinta = params.getTarkastus().get("vaihtop_laiminl");
		_Column epaonnistuminen = params.getTarkastus().get("epaonn");
		_Column epaonnistumisenTarkkuus = params.getTarkastus().get("epaonn_tark");

		if (!vaihtoPesanEtsinta.getValue().equals(PESAA_EI_TARKASTETTU_LAINKAAN)) {
			checkNotNull(pesimistulos, pesimistulosTarkkuus, kunto);
		} else {
			checkIsNull(pesimistulos, pesimistulosTarkkuus, kunto, epaonnistuminen, epaonnistumisenTarkkuus);
			checkIsNullOrZero(params.getColumn("tarkastus.aikuisia"), params.getColumn("tarkastus.elavia"), params.getColumn("tarkastus.kuolleita"), params.getColumn("tarkastus.lentopoik"));
			for (_Table t : params.getPoikaset()) {
				checkIsNull(t.get("rengas"), t.get("siipi"), t.get("paino"));
			}
		}

		_Column kiipeysTietdotJne = params.getTarkastus().get("tarkkuus");
		if (SaaksiConst.ONNISTUMISKOODIT.contains(pesimistulos.getValue())) {
			if (kirjekyyhkyValidations) {
				checkNotNull(kiipeysTietdotJne);
			}
		}

		checkValueAndAccuracyPair(epaonnistuminen, epaonnistumisenTarkkuus, ACCURACY_REQUIRED);

		muulaji(params, pesimistulos);

		if (kirjekyyhkyValidations) {
			if (notNullNoErrors(pesimistulos) && pesimistulos.getValue().equals("T")) {
				warning(pesimistulos, _Validator.ARE_YOU_SURE);
			}
		}
	}

	private void muulaji(_PesaDomainModelRow params, _Column pesimistulos) {
		_Column muulaji = params.getColumn("tarkastus.muulaji");
		if (notNullNoErrors(muulaji)) {
			if (muulaji.getValue().equals("PANHAL")) {
				error(muulaji, SaaksiConst.MUULAJI_CAN_NOT_BE_PANHAL);
			}
		}
		if (!pesimistulos.getValue().equals("X")) {
			checkIsNull(muulaji);
		} else {
			checkNotNull(muulaji);
		}
	}

	private void crossCheckWithKaynnit(_Column pesimistulos) {
		if (notNullNoErrors(pesimistulos)) {
			Collection<Havainto> esiintyvatHavainnot = selvitaEsiintyvatHavainnot();
			PesimistulosValidationDefinition validationRules = SaaksiConst.PESIMISTULOS_VALIDATION_DEFINITIONS.get(pesimistulos.getValue());
			for (Havainto havainto : esiintyvatHavainnot) {
				if (!validationRules.sallitut().contains(havainto)) {
					warning(havainto.toString(), SaaksiConst.PESIMISTULOS_JA_HAVAINTO_RISTIRIIDASSA);
				}
			}
			boolean meetsRequirements = false;
			for (Havainto vaadittu : validationRules.vaaditut()) {
				if (esiintyvatHavainnot.contains(vaadittu)) {
					meetsRequirements = true;
					break;
				}
			}
			if (validationRules.vaaditut().isEmpty()) {
				meetsRequirements = true;
			}
			if (!meetsRequirements) {
				warning(pesimistulos, SaaksiConst.EI_TARPEEKSI_HAVAINTOJA_PESIMISTULOKSELLE + pesimistulos.getValue());
			}
		}
	}

	private Collection<Havainto> selvitaEsiintyvatHavainnot() {
		Collection<Havainto> esiintyvatHavainnot = new HashSet<>();
		for (_Table kaynti : params.getKaynnit().values()) {
			if (!kaynti.hasValues()) continue;
			if (notNullNotZero(kaynti.get("lentopoik"))) {
				esiintyvatHavainnot.add(Havainto.LENTOP);
			}
			if (notNullNotZero(kaynti.get("elavia"))) {
				esiintyvatHavainnot.add(Havainto.PESAP);
			}
			if (notNullNotZero(kaynti.get("kuolleita"))) {
				esiintyvatHavainnot.add(Havainto.KUOLLEITA);
			}
			if (notNullNotZero(kaynti.get("munia"))) {
				esiintyvatHavainnot.add(Havainto.MUNIA);
			}
			if (notNullNotZero(kaynti.get("aikuisia"))) {
				esiintyvatHavainnot.add(Havainto.EMOJA);
			}
		}
		return esiintyvatHavainnot;
	}

	private boolean notNullNotZero(_Column c) {
		if (c.getValue().equals("0")) return false;
		return c.hasValue();
	}

	private void lkmVsPesimistulos(_PesaDomainModelRow params, _Column pesimistulos) {
		_Column aik_aikuisia_lkm = params.getColumn("tarkastus.aik_aikuisia");
		_Column aikuisia_lkm = params.getColumn("tarkastus.aikuisia");
		_Column aik_munia_lkm = params.getColumn("tarkastus.aik_munia");
		_Column munia_lkm = params.getColumn("tarkastus.munia");
		_Column aik_elavia_lkm = params.getColumn("tarkastus.aik_elavia");
		_Column elavia_lkm = params.getColumn("tarkastus.elavia");
		_Column lentopoik_lkm = params.getColumn("tarkastus.lentopoik");
		_Column lop_lentopoik_lkm = params.getColumn("tarkastus.lop_lentopoik");
		_Column kuolleita_lkm = params.getColumn("tarkastus.kuolleita");
		_Column lop_kuolleita_lkm = params.getColumn("tarkastus.lop_kuolleita");

		if (SaaksiConst.PESIMISTULOS_POIKANEN_NOT_ALLOWED.contains(pesimistulos.getValue())) {
			if (poikanenValuesGiven(params)) {
				error(pesimistulos, SaaksiConst.INVALID_PESIMISTULOS_POIKASET);
			}
			checkIsNullOrZero(aik_elavia_lkm, SaaksiConst.INVALID_PESIMISTULOS_POIKASET);
			checkIsNullOrZero(elavia_lkm, SaaksiConst.INVALID_PESIMISTULOS_POIKASET);
			checkIsNullOrZero(lentopoik_lkm, SaaksiConst.INVALID_PESIMISTULOS_POIKASET);
			checkIsNullOrZero(lop_lentopoik_lkm, SaaksiConst.INVALID_PESIMISTULOS_POIKASET);
			checkIsNullOrZero(kuolleita_lkm, SaaksiConst.INVALID_PESIMISTULOS_POIKASET);
			checkIsNullOrZero(lop_kuolleita_lkm, SaaksiConst.INVALID_PESIMISTULOS_POIKASET);
		}
		if (SaaksiConst.PESIMISTULOS_MUNA_NOT_ALLOWED.contains(pesimistulos.getValue())) {
			checkIsNullOrZero(aik_munia_lkm, SaaksiConst.INVALID_PESIMISTULOS_MUNAT);
			checkIsNullOrZero(munia_lkm, SaaksiConst.INVALID_PESIMISTULOS_MUNAT);
		}
		if (!PESIMISTULOS_LENTOPOIKASPESA.equals(pesimistulos.getValue())) {
			checkIsNullOrZero(lentopoik_lkm, SaaksiConst.INVALID_PESIMISTULOS_LENTOPOIKASET);
			checkIsNullOrZero(lop_lentopoik_lkm, SaaksiConst.INVALID_PESIMISTULOS_LENTOPOIKASET);
		}
		Collection<Havainto> esiintyvatHavainnot = selvitaEsiintyvatHavainnot();
		if (esiintyvatHavainnot.contains(Havainto.EMOJA)) {
			if (isNullOrZero(aikuisia_lkm) && isNullOrZero(aik_aikuisia_lkm)) {
				error(aikuisia_lkm, SaaksiConst.TIIVISTETYT_LKMT_VIRHE);
			}
		}
		if (esiintyvatHavainnot.contains(Havainto.MUNIA)) {
			if (isNullOrZero(munia_lkm) && isNullOrZero(aik_munia_lkm)) {
				error(munia_lkm, SaaksiConst.TIIVISTETYT_LKMT_VIRHE);
			}
		}
		if (esiintyvatHavainnot.contains(Havainto.PESAP)) {
			if (isNullOrZero(elavia_lkm) && isNullOrZero(aik_elavia_lkm)) {
				error(elavia_lkm, SaaksiConst.TIIVISTETYT_LKMT_VIRHE);
			}
		}
		if (esiintyvatHavainnot.contains(Havainto.LENTOP)) {
			if (isNullOrZero(lentopoik_lkm) && isNullOrZero(lop_lentopoik_lkm)) {
				error(lentopoik_lkm, SaaksiConst.TIIVISTETYT_LKMT_VIRHE);
			}
		}
		if (esiintyvatHavainnot.contains(Havainto.KUOLLEITA)) {
			if (isNullOrZero(kuolleita_lkm) && isNullOrZero(lop_kuolleita_lkm)) {
				error(kuolleita_lkm, SaaksiConst.TIIVISTETYT_LKMT_VIRHE);
			}
		}
	}

	private boolean poikanenValuesGiven(_PesaDomainModelRow params) {
		for (_Table poikanen : params.getPoikaset().values()) {
			if (poikanen.hasValuesIgnoreMetadata("tarkastusid")) {
				return true;
			}
		}
		return false;
	}

	private void checkTark_vuosiTheSameOrOneYearBeforeTarkPvm(_Column vuosi, _Column tarkPvm) {
		if (notNullNoErrors(vuosi) && notNullNoErrors(tarkPvm)) {
			int tarkVuosi = Integer.valueOf(tarkPvm.getDateValue().getYear());
			if (vuosi.getIntegerValue() < tarkVuosi - 1 || vuosi.getIntegerValue() > tarkVuosi) {
				error(tarkPvm, SaaksiConst.INVALID_TARK_VUOSI_TARK_PVM_YYYY);
			}
		}
	}

	private void checkCoordinates(_Row params) throws Exception {
		_Column tyyppiC = params.getColumn("pesa.koord_tyyppi");
		if (!notNullNoErrors(tyyppiC)) return;
		KoordTyyppi koord_tyyppi = SaaksiConst.KOORD_TYYPPI_MAPPING.get(tyyppiC.getValue());
		_Column leveys = null, pituus = null;
		int leveys_min, leveys_max, pituus_min, pituus_max;
		CoordinateConverter converter = new CoordinateConverter();
		boolean okToCheckInsideReagion = false;

		if (koord_tyyppi == KoordTyyppi.YHT) {
			leveys = params.getColumn("pesa.yht_leveys");
			pituus = params.getColumn("pesa.yht_pituus");
			leveys_min = _Validator.YHT_LEVEYS_MIN;
			leveys_max = _Validator.YHT_LEVEYS_MAX;
			pituus_min = _Validator.YHT_PITUUS_MIN;
			pituus_max = _Validator.YHT_PITUUS_MAX;
			checkCoordinateRanges(leveys, pituus, leveys_min, leveys_max, pituus_min, pituus_max);
			if (noErrors(leveys) && noErrors(pituus)) {
				converter.setYht_leveys(leveys.getValue());
				converter.setYht_pituus(pituus.getValue());
				okToCheckInsideReagion = true;
			}
		} else if (koord_tyyppi == KoordTyyppi.EUR) {
			leveys = params.getColumn("pesa.eur_leveys");
			pituus = params.getColumn("pesa.eur_pituus");
			leveys_min = _Validator.EUR_LEVEYS_MIN;
			leveys_max = _Validator.EUR_LEVEYS_MAX;
			pituus_min = _Validator.EUR_PITUUS_MIN;
			pituus_max = _Validator.EUR_PITUUS_MAX;
			checkCoordinateRanges(leveys, pituus, leveys_min, leveys_max, pituus_min, pituus_max);
			if (noErrors(leveys) && noErrors(pituus)) {
				converter.setEur_leveys(leveys.getValue());
				converter.setEur_pituus(pituus.getValue());
				okToCheckInsideReagion = true;
			}
		} else if (koord_tyyppi == KoordTyyppi.AST) {
			leveys = params.getColumn("pesa.ast_leveys");
			pituus = params.getColumn("pesa.ast_pituus");
			leveys_min = _Validator.AST_LEVEYS_MIN;
			leveys_max = _Validator.AST_LEVEYS_MAX;
			pituus_min = _Validator.AST_PITUUS_MIN;
			pituus_max = _Validator.AST_PITUUS_MAX;
			checkCoordinateRanges(leveys, pituus, leveys_min, leveys_max, pituus_min, pituus_max);
			if (noErrors(leveys) && noErrors(pituus)) {
				converter.setAst_leveys(leveys.getValue());
				converter.setAst_pituus(pituus.getValue());
				okToCheckInsideReagion = true;
			}
		} else {
			error("pesa.koord_tyyppi", _Validator.INVALID_VALUE);
		}

		if (okToCheckInsideReagion && noErrors("pesa.kunta")) {
			checkInsideReagion(params.getColumn("pesa.kunta").getValue(), leveys, pituus, converter);
		}
	}

}
