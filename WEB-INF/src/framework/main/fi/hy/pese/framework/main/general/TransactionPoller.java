package fi.hy.pese.framework.main.general;

import java.sql.SQLException;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.util.EntityUtils;

import fi.hy.pese.framework.main.app._ApplicationSpecificationFactory;
import fi.hy.pese.framework.main.db._DAO;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data._Row;
import fi.hy.pese.framework.main.general.data._Table;
import fi.laji.datawarehouse.etl.models.dw.DwRoot;
import fi.luomus.commons.http.HttpClientService;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.utils.URIBuilder;

public class TransactionPoller<T extends _Row> implements Runnable {
	
	private static final long SHORT_DELAY = 10;
	private static final long LONG_DELAY = 1000*60*2;
	private volatile boolean run = true;
	private final _ApplicationSpecificationFactory<T> application;
	private final String etlURL;
	private final String etlApiKey;
	
	public TransactionPoller(_ApplicationSpecificationFactory<T> application) {
		this.application = application;
		this.etlURL = application.config().get(Const.ETL_URI);
		this.etlApiKey = application.config().get(Const.ETL_API_KEY);
	}
	
	@Override
	public void run() {
		markAllUnattempted();
		System.out.println("Transaction handler for " + application.config().systemId() + " is running");
		while (run) {
			long delay = doWork();
			sleep(delay);
		}
		System.out.println("Transaction handler for " + application.config().systemId() + " has been stopped!");
	}
	
	private void markAllUnattempted() {
		_DAO<T> dao = null;
		try {
			dao = application.dao(TransactionPoller.class.getName());
			dao.markAllWarehousingUnattempted();
		} catch (Exception e) {
			application.getErrorReporter().report("Marking all unattempted", e);
		} finally {
			if (dao != null) application.release(dao);
		}
	}
	
	private long doWork() {
		_DAO<T> dao = null;
		try {
			dao = application.dao(TransactionPoller.class.getSimpleName()+"_"+application.config().systemId());
			return handleTransactions(dao);
		} catch (Exception e) {
			application.getErrorReporter().report("Transaction poller", e);
			return LONG_DELAY;
		} finally {
			if (dao != null) application.release(dao);
		}
	}
	
	private long handleTransactions(_DAO<T> dao) throws SQLException {
		_Table searchParams = dao.newRow().getTransactionEntry();
		searchParams.get("attempted").setValue(0);
		
		List<_Table> results = getTransactionEntries(dao, searchParams);
		if (!results.isEmpty()) {
			return handleUnattemptedTransactions(dao, results);
		}
		searchParams.get("attempted").setValue(1);
		results = getTransactionEntries(dao, searchParams);
		if (!results.isEmpty()) {
			return handleReattemptedTransactions(dao, results);
		}
		return LONG_DELAY;
	}
	
	private long handleReattemptedTransactions(_DAO<T> dao, List<_Table> results) {
		try {
			handleTransactions(results, dao);
			return SHORT_DELAY;
		} catch (Exception e) {
			return LONG_DELAY;
		}
	}
	
	private long handleUnattemptedTransactions(_DAO<T> dao, List<_Table> results) {
		try {
			handleTransactions(results, dao);
		} catch (Exception e) {}
		return SHORT_DELAY;
	}
	
	private List<_Table> getTransactionEntries(_DAO<T> dao, _Table searchParams) throws SQLException {
		List<_Table> results = dao.returnTables(searchParams, 10, "created DESC");
		return results;
	}
	
	private void handleTransactions(List<_Table> transactions, _DAO<T> dao) throws Exception {
		HttpClientService client = null;
		try {
			client = new HttpClientService();
			for (_Table transaction : transactions) {
				handleTransaction(transaction, dao, client);
			}
		} finally {
			if (client != null) client.close();
		}
	}
	
	private void handleTransaction(_Table transaction, _DAO<T> dao, HttpClientService client) throws Exception {
		int rowId = transaction.get("row_id").getIntegerValue();
		try {
			sendToWarehouse(rowId, dao, client);
			dao.markWarehousingSuccess(transaction);
		} catch (Exception e) {
			application.getErrorReporter().report("Handling transaction for row " + rowId, e);
			dao.markWarehousingAttempted(transaction);
			throw e;
		}
	}
	
	private void sendToWarehouse(int rowId, _DAO<T> dao, HttpClientService client) throws Exception {
		DwRoot root = application.getWarehouseRowGenerator(dao).generate(rowId);
		JSONObject json = root.toJSON();
		sendToWarehouse(json, client);
	}
	
	private void sendToWarehouse(JSONObject json, HttpClientService client) throws Exception {
		CloseableHttpResponse response = null;
		try {
			response = sendToWarehouse(client, json);
			handleFail(response);
		} finally {
			if (response != null) try { response.close(); } catch (Exception e) {}
		}
	}
	
	private void handleFail(CloseableHttpResponse response) throws Exception {
		if (response.getStatusLine().getStatusCode() != 200) {
			String errorMessage = EntityUtils.toString(response.getEntity(), "UTF-8");
			throw new Exception("POST to warehouse failed with message: " + errorMessage);
		}
	}
	
	private CloseableHttpResponse sendToWarehouse(HttpClientService client, JSONObject json) throws Exception {
		URIBuilder uri = new URIBuilder(etlURL).addParameter("access_token", etlApiKey);
		HttpPost request = new HttpPost(uri.getURI());
		request.setHeader("content-type", "application/json");
		HttpEntity entity = new ByteArrayEntity(json.reveal().toString().getBytes("UTF-8"));
		request.setEntity(entity);
		return client.execute(request);
	}
	
	private void sleep(long delay) {
		try {
			Thread.sleep(delay);
		} catch (InterruptedException e) {}
	}
	
	public void stop() {
		run = false;
	}
	
}
