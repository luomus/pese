
<#include "header.ftl">

<h1>${texts[data.table+"_management_title"]}</h1>

<#if system.action != "insert" && system.action != "insert_page">
<#------------------------------------ SEARCH UPDATE DELETE ---------------------------------------------->

<#----------------- link to add new entry page ------------------------------>

  <p align="right"> 
  <#if data.tablemanagement_permissions == TABLEMANAGEMENT_UPDATE_INSERT || data.tablemanagement_permissions == TABLEMANAGEMENT_UPDATE_INSERT_DELETE || data.tablemanagement_permissions == TABLEMANAGEMENT_INSERT_ONLY>
      <a href="${system.baseURL}?page=${system.page}&amp;action=insert_page&amp;table=${data.table}">
        &raquo; ${texts[data.table+"_management_add_link"]}
      </a>
  </#if>
  </p> 

<#----------------- --------------------------- ------------------------------>

<h2>${texts.management_search}</h2>

<#if system.action == "search">
  <@errorlist errors texts /> 
</#if>

<br />

<form action="${system.baseURL}" method="post" name="search">
<input type="hidden" name="page"   value="${system.page}" />
<input type="hidden" name="action" value="search" />
<input type="hidden" name="table"  value="${data.table}" />

<table>
  <thead>
  <tr>
    <#----------------- column descriptions ------------------------------>
    <#list searchparameters.getTableByName(data.table).columns as column>
      <#if column.name != "rowid" && column.name != "id">
        <th>${texts[ column.fullname ]!column.fullname}</th>
      </#if>
	</#list>
	<#------------------------------------------------------------------->
  </tr>
  </thead>
  <tbody>
  <tr>
    <#-------------------- search form columns --------------------------------->
    <#list searchparameters.getTableByName(data.table).columns as column> 
      
      <#if column.name != "rowid" && column.name != "id">
      
      <td>
        <#if column.inputType == "TEXT">
          <@inputText "searchparameters" column true errors />  
        </#if>
        <#if column.inputType == "TEXTAREA">
          <@inputText "searchparameters" column false errors />  
        </#if>
        <#if column.inputType == "SELECT"> <@selection selections "searchparameters" column 1 false true false errors /> </#if>
        <#if column.inputType == "DATE">
          <@date "searchparameters" column errors /> 
        </#if>
      </td>
      
      </#if>
	</#list>
	<#-------------------------------------------------------------------->
  </tr>
  </tbody>
</table>

<input type="submit" value="${texts.search}" />  
<input type="submit" value="${texts.clear}"  onclick="document.clear.submit(); return false; " /> 

</form>

<#----------------- clear button ------------------------------>
  <form action="${system.baseURL}" method="get" name="clear">
  <input type="hidden" name="page"  value="${system.page}" />
  <input type="hidden" name="table" value="${data.table}" />
  </form>
<#----------------- ------------ ------------------------------>



<br />
<br />
<#if system.action == "search">${results?size} ${texts.management_results_count}</#if>
<br />

<#if system.action != "search"><@errorlist errors texts /> <@warninglist warnings texts /> </#if>
<@statustexts system texts />

<br />

<#----------------------------------- results -------------------------------------------->
<#if (results?size > 0) >  
  
  <table > 
    <thead>
    <tr>
    <#----------------- column descriptions ------------------------------>
      <#list updateparameters.getTableByName(data.table).columns as column>
        <#if column.name != "rowid"  && column.name != "id">
          <th <#if column.datatype == INTEGER || column.datatype == DECIMAL>class="{sorter: 'digit'}"</#if>  >${texts[ column.fullname ]!column.fullname}</th>
        </#if>
	  </#list>
    <#------------------------------------------------------------------->
    </tr>
    </thead>
    
    <tbody>
    <#list results as result>
      <tr>
      <form action="${system.baseURL}" method="post">
      <input type="hidden" name="page"   value="${system.page}" />
      <input type="hidden" name="table"  value="${data.table}" />
      <input type="hidden" name="bypassed_warnings" id="bypassed_warnings" value="${data.bypassed_warnings!""}" /> 
      <#---------------------- attach searchparameters to request ---------------------> 
        <#list searchparameters.getTableByName(data.table).columns as column>
           <@inputHidden "searchparameters" column />
	    </#list>
	  <#---------------------- --------------------------------------- ---------------->
	           
      <#list result.getTableByName(data.table).columns as column>
        <#if column.name == "rowid" || column.name == "id"> <@inputHidden "updateparameters" column /> 
        <#else>
            <td>
              <#if column.modifiability == "NO" || column.tablename="muutosloki"> ${column.value}  <@inputHidden "updateparameters" column />
              <#else>
                <#if column.inputType == "TEXT">     <@inputText "updateparameters" column true errors /> </#if>
                <#if column.inputType == "TEXTAREA"> <@textarea "updateparameters" column 2 40 errors />   </#if>
                <#if column.inputType == "SELECT">   <@selection selections "updateparameters" column 1 false true false errors /> </#if>
                <#if column.inputType == "DATE">     <@date "updateparameters" column errors /> </#if> 
                <#if column.inputType == "PASSWORD"> <@inputPassword "updateparameters" column errors /> </#if>
              </#if>
            </td>
        </#if>
	  </#list>
	   
	   <#if data.tablemanagement_permissions !=  TABLEMANAGEMENT_INSERT_ONLY>	
	     <td><input type="submit" name="update" value="${texts.update}" /> </td>
	   </#if>
	   <#if data.tablemanagement_permissions == TABLEMANAGEMENT_UPDATE_INSERT_DELETE> <td><input type="submit" name="delete" value="${texts.delete}" /> </td> </#if>
       
      </form>
      </tr>
      
    </#list>
    </tbody>
  </table>
</#if>

<br />
<br />

<form action="${system.baseURL}" method="get" name="copy">
  <input type="hidden" name="page"  value="${system.page}" />
  <input type="hidden" name="table" value="${data.table}" />
  <input type="hidden" name="action" value="copy_seurantaruudut" />

  
<table>
  <caption> Kopioi ruututiedot </caption>
  <tr>
    <td> Kopioi kaikki seurantaruudut vuodelta: </td>
    <td> <input name="orig_year" type="text" size="4" value="${data.orig_year!""}" <@checkerror errors "orig_year" /> /> </td>
    <td> vuodelle: </td>
    <td> <input name="dest_year" type="text" size="4" value="${data.dest_year!""}" <@checkerror errors "dest_year" /> /> </td>
    <td> <button onclick="if (confirm('Oletko varma?')) { document.copy.submit(); return false; }"> Kopioi </button> </td>
  </tr>
</table>

</form>

<#------------------------------------ ---------------- -------------------------------------------------->




<#else>

<#------------------------------------      INSERT     --------------------------------------------------->

<br />
<h2>${texts[data.table+"_management_add_link"]}</h2>
<br />

<@errorlist errors texts />
<@warninglist warnings texts />

<form action="${system.baseURL}" method="post">
<input type="hidden" name="page"   value="${system.page}" />
<input type="hidden" name="action" value="insert" />
<input type="hidden" name="table"  value="${data.table}" />
<input type="hidden" name="bypassed_warnings" id="bypassed_warnings" value="${data.bypassed_warnings!""}" /> 

<table>
  <tr>
    <#----------------- column descriptions ------------------------------>
    <#list updateparameters.getTableByName(data.table).columns as column>
      <#if column.fieldType == NORMAL_FIELD || column.fieldType == IMMUTABLE_KEY || column.fieldType == PASSWORD>
        <th>${texts[ column.fullname ]!column.fullname}</th>
      </#if>
	</#list>
	<#------------------------------------------------------------------->
  </tr> 
  <tr>
  <#list updateparameters.getTableByName(data.table).columns as column>
    <#if column.fieldType == NORMAL_FIELD || column.fieldType == IMMUTABLE_KEY || column.fieldType == PASSWORD>
      <td>
        <#if column.inputType == "TEXT">     <@inputText "updateparameters" column true errors /> </#if>
        <#if column.inputType == "TEXTAREA"> <@textarea "updateparameters" column 2 40 errors />   </#if>
        <#if column.inputType == "SELECT">   <@selection selections "updateparameters" column 1 false true false errors /> </#if>
        <#if column.inputType == "DATE">     <@date "updateparameters" column errors /> </#if> 
        <#if column.inputType == "PASSWORD"> <@inputPassword "updateparameters" column errors /> </#if>
      </td>
    </#if>
  </#list>
  </tr>
</table>
  
  <br />
  
  <input type="submit" value="${texts.insert}" />
</form>

<script type="text/javascript">
	 var e = $('#updateparameters\\.seurantaruutu\\.koko');
	 if (e.val().trim() != '') { } 
	 else { e.val('10000'); } 
</script>

<br /><br />

<form action="${system.baseURL}" method="post">
<input type="hidden" name="page"   value="${system.page}" />
<input type="hidden" name="table"  value="${data.table}" />
<input type="submit" value="${texts.cancel}" />
</form>

<#------------------------------------ ---------------- -------------------------------------------------->
</#if>


<#include "footer.ftl">
