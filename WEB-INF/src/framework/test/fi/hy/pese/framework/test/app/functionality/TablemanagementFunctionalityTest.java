package fi.hy.pese.framework.test.app.functionality;

import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import fi.hy.pese.framework.main.app.BaseFactoryForApplicationSpecificationFactory;
import fi.hy.pese.framework.main.app.FunctionalityExecutor;
import fi.hy.pese.framework.main.app.Request;
import fi.hy.pese.framework.main.app.RequestImpleData;
import fi.hy.pese.framework.main.app._Functionality;
import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.app.functionality.TablemanagementFunctionality;
import fi.hy.pese.framework.main.app.functionality.validate._Validator;
import fi.hy.pese.framework.main.db._DAO;
import fi.hy.pese.framework.main.db._ResultsHandler;
import fi.hy.pese.framework.main.db.oraclesql.OracleSQL_DAO;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.consts.Success;
import fi.hy.pese.framework.main.general.data.Column;
import fi.hy.pese.framework.main.general.data.Data;
import fi.hy.pese.framework.main.general.data.DatabaseTableStructure;
import fi.hy.pese.framework.main.general.data.PesaDomainModelDatabaseStructure;
import fi.hy.pese.framework.main.general.data.PesaDomainModelRowFactory;
import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._Data;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.main.general.data._ReferenceKey;
import fi.hy.pese.framework.main.general.data._RowFactory;
import fi.hy.pese.framework.main.general.data._Table;
import fi.luomus.commons.containers.Selection;
import fi.luomus.commons.containers.SelectionImple;
import fi.luomus.commons.containers.SelectionOptionImple;
import fi.luomus.commons.utils.Utils;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class TablemanagementFunctionalityTest {
	
	// NOTE: Some of the functionality is tested via TestApp (testapp.test.UsingTheFrameworkToImplementTablemanagement)
	
	public static Test suite() {
		return new TestSuite(TablemanagementFunctionalityTest.class.getDeclaredClasses());
	}
	
	public static class UsingTablemanagement extends TestCase {
		
		private class TablemanagementFunctionalityTestFactory extends BaseFactoryForApplicationSpecificationFactory<_PesaDomainModelRow> {
			
			public TablemanagementFunctionalityTestFactory(String configFileName) throws FileNotFoundException {
				super(configFileName);
			}
			
			@Override
			public _DAO<_PesaDomainModelRow> dao(String userId) {
				return new TablemanagementFunctionalityTestFakeDAO(data);
			}
			
			@Override
			public _Functionality<_PesaDomainModelRow> functionalityFor(String page, _Request<_PesaDomainModelRow> request) throws UnsupportedOperationException {
				return new TablemanagementFunctionality<>(request);
			}
			
			@Override
			protected Map<String, Selection> fetchSelections(_DAO<_PesaDomainModelRow> dao) throws Exception {
				return null;
			}
		}
		
		private class TablemanagementFunctionalityTestFakeDAO extends OracleSQL_DAO<_PesaDomainModelRow> {
			private final _Data<_PesaDomainModelRow>	resultdata;
			
			public TablemanagementFunctionalityTestFakeDAO(_Data<_PesaDomainModelRow> data) {
				super(null, null);
				this.resultdata = data;
			}
			
			private boolean	tintti_1_update_called		= false;
			private boolean	tintti_1_has_been_deleted	= false;
			
			@Override
			public boolean checkReferenceValueExists(String value, _ReferenceKey ref) {
				if (value.equals("HALALB")) return true;
				return false;
			}
			
			@Override
			public String returnValueByRowid(String table, String valueColumn, String rowid) throws SQLException {
				if (rowid.equals("HALALBrivi"))
					return "HALALB";
				return "Foooo";
			}
			
			@Override
			public void executeSearch(_Table searchparameters, _ResultsHandler resultHandler) {
				
				if (searchparameters.getName().equals("laji")) {
					if (searchparameters.get("id").getValue().equals("HALIB")) {
						_PesaDomainModelRow resultrow = resultdata.newResultRow();
						resultrow.getKaynti().get("nimi").setValue("Merikotka");
					} else {
						_PesaDomainModelRow resultrow = resultdata.newResultRow();
						if (!tintti_1_has_been_deleted) {
							if (!tintti_1_update_called)
								resultrow.getKaynti().get("nimi").setValue("Tintti 1");
							else
								resultrow.getKaynti().get("nimi").setValue("Tintti ykkänen");
							resultrow.getKaynti().setRowidValue("AAAA");
							resultrow = resultdata.newResultRow();
						}
						resultrow.getKaynti().get("nimi").setValue("Tintti 2");
						resultrow.getKaynti().setRowidValue("AAAB");
					}
				} else {
					if (searchparameters.getRowidValue().equals("ADDBD")) {
						_PesaDomainModelRow resultrow = resultdata.newResultRow();
						resultrow.getReviiri().get("nimi").setValue("Uusi reviiri");
					}
				}
			}
			
			@Override
			public boolean checkRowIDMatchesKeys(_Table table) {
				if (table.getRowidValue().equals("CCCA") && table.getUniqueNumericIdColumn().getValue().equals("1")) return true;
				if (table.getName().equals("olosuhde")) return true;
				if (table.getRowidValue().equals("AAAA") && table.get("id").getValue().equals("TINT_1")) return true;
				return false;
			}
			
			@Override
			public void executeUpdate(_Table table) {
				tintti_1_update_called = true;
			}
			
			@Override
			public void executeDelete(_Table table) {
				tintti_1_has_been_deleted = true;
			}
			
			@Override
			public void executeInsert(_Table table) {
				table.setRowidValue("ADDBD");
			}
			
			@Override
			public boolean checkRowExists(_Table table) {
				if (table.getRowidValue().equals("HALALBrivi")) return true;
				if (table.getRowidValue().equals("CCCA")) return true;
				if (table.getRowidValue().equals("AAAA")) return true;
				if (table.getName().equals("laji") && table.get("id").getValue().equals("HALIB")) return true;
				return false;
			}
			
			@Override
			public void setSelectionsUpdateTime() {
				
			}
			
			@Override
			public String returnAndSetNextId(_Table table) {
				return "";
			}
		}
		
		private _Data<_PesaDomainModelRow>			data;
		private _Functionality<_PesaDomainModelRow>	tablemanagementFunctionality;
		
		@Override
		protected void setUp() throws Exception {
			TablemanagementFunctionalityTestFactory factory = new TablemanagementFunctionalityTestFactory(null);
			initTestData(factory);
			_Request<_PesaDomainModelRow> request = new Request<>(new RequestImpleData<_PesaDomainModelRow>().setData(data).setDao(factory.dao("userid_for_db_log")).setApplication(factory));
			tablemanagementFunctionality = factory.functionalityFor(Const.TARKASTUS_PAGE, request);
			data.setPage(Const.TABLEMANAGEMENT_PAGE);
			data.setUserId("admin_ep");
			data.setUserName("Esko Piirainen");
		}
		
		private void initTestData(TablemanagementFunctionalityTestFactory factory) {
			PesaDomainModelDatabaseStructure tablestemplate = new PesaDomainModelDatabaseStructure();
			DatabaseTableStructure laji = new DatabaseTableStructure("laji");
			laji.addColumn("id", Column.VARCHAR, 6);
			laji.addColumn("nimi", Column.VARCHAR, 20);
			laji.addColumn("kommentti", Column.VARCHAR, 250);
			laji.addColumn("minimipaino", Column.DECIMAL, 5, 2);
			laji.addColumn("kotikunta", Column.VARCHAR, 6);
			laji.addColumn("userid", _Column.VARCHAR, 20);
			laji.get("id").setTypeToImmutablePartOfAKey();
			laji.get("minimipaino").setToOptionalColumn();
			laji.get("userid").setTypeToUserIdColumn();
			laji.setTablemanagementPermissions(_Table.TABLEMANAGEMENT_UPDATE_INSERT_DELETE);
			tablestemplate.setKaynti(laji);
			
			DatabaseTableStructure reviiri = new DatabaseTableStructure("reviiri");
			reviiri.addColumn("reviiriid", _Column.INTEGER, 5);
			reviiri.addColumn("nimi", _Column.VARCHAR, 25);
			reviiri.addColumn("kirj_pvm", _Column.DATE);
			reviiri.addColumn("muutos_pvm", _Column.DATE);
			reviiri.addColumn("loyto_pvm", _Column.DATE);
			reviiri.get("reviiriid").setTypeToUniqueNumericIncreasingId();
			reviiri.get("kirj_pvm").setTypeToDateAddedColumn();
			reviiri.get("muutos_pvm").setTypeToDateModifiedColumn();
			reviiri.setTablemanagementPermissions(_Table.TABLEMANAGEMENT_UPDATE_INSERT_DELETE);
			tablestemplate.setReviiri(reviiri);
			
			DatabaseTableStructure pesa = new DatabaseTableStructure("no_permissions");
			tablestemplate.setPesa(pesa);
			
			DatabaseTableStructure aputaulu = new DatabaseTableStructure("update_only");
			aputaulu.setTablemanagementPermissions(_Table.TABLEMANAGEMENT_UPDATE_ONLY);
			tablestemplate.setAputaulu(aputaulu);
			
			DatabaseTableStructure muna = new DatabaseTableStructure("update_insert");
			muna.setTablemanagementPermissions(_Table.TABLEMANAGEMENT_UPDATE_INSERT);
			tablestemplate.setMuna(muna);
			
			DatabaseTableStructure olosuhde = new DatabaseTableStructure("olosuhde");
			olosuhde.setTablemanagementPermissions(_Table.TABLEMANAGEMENT_UPDATE_INSERT_DELETE);
			olosuhde.addColumn("asuva_laji", _Column.VARCHAR, 6);
			olosuhde.get("asuva_laji").references(laji.get("id"));
			tablestemplate.setOlosuhde(olosuhde);
			_RowFactory<_PesaDomainModelRow> rowFactory = new PesaDomainModelRowFactory(tablestemplate, "", "");
			factory.setRowFactory(rowFactory);
			data = new Data<>(rowFactory);
			
			SelectionImple kotikunnat = new SelectionImple("kotikunta");
			kotikunnat.addOption(new SelectionOptionImple("hel", "Helsinki"));
			kotikunnat.addOption(new SelectionOptionImple("van", "Vantaa"));
			Map<String, Selection> selections = new HashMap<>();
			selections.put("kotikunta", kotikunnat);
			data.setSelections(selections);
		}
		
		@Override
		protected void tearDown() {
			// Normally the DAO that was received from factory should aways be released (and rolled back),
			// but we are using a fake dao (not sql connection) here with these test
		}
		
		public void test_it_throws_exception_if_no_table_given() {
			try {
				FunctionalityExecutor.execute(tablemanagementFunctionality);
				fail("Should throw exception");
			} catch (Exception e) {
			}
		}
		
		public void test_it_throws_exception_if_given_table_not_found_from_tablestemplate() throws Exception {
			try {
				data.set(Const.TABLE, "foo");
				FunctionalityExecutor.execute(tablemanagementFunctionality);
				fail("Should throw exception");
			} catch (IllegalArgumentException e) {
				assertTrue("tells wrong parameter name also", e.toString().contains("foo"));
			}
		}
		
		public void test_it_adds_a_result_to_data() throws Exception {
			data.set(Const.TABLE, "laji");
			data.searchparameters().getKaynti().get("id").setValue("HALIB");
			data.setAction(Const.SEARCH);
			
			FunctionalityExecutor.execute(tablemanagementFunctionality);
			
			assertEquals(1, data.results().size());
			assertEquals("Merikotka", data.results().get(0).getKaynti().get("nimi").getValue());
			
			checkNoErrors();
		}
		
		private void checkNoErrors() {
			if (!data.errors().isEmpty()) {
				Utils.debug(data.errors());
			}
			assertTrue("Should not have errors", data.errors().isEmpty());
		}
		
		public void test_it_adds_multiple_resultrows_to_data() throws Exception {
			data.set(Const.TABLE, "laji");
			data.searchparameters().getKaynti().get("id").setValue("BB");
			data.setAction(Const.SEARCH);
			
			FunctionalityExecutor.execute(tablemanagementFunctionality);
			
			assertEquals(2, data.results().size());
			assertEquals("Tintti 1", data.results().get(0).getKaynti().get("nimi").getValue());
			assertEquals("Tintti 2", data.results().get(1).getKaynti().get("nimi").getValue());
		}
		
		public void test_it_doesnt_add_results_if_no_action_set() throws Exception {
			data.set(Const.TABLE, "laji");
			FunctionalityExecutor.execute(tablemanagementFunctionality);
			assertEquals(0, data.results().size());
		}
		
		public void test_setting_tablemanagement_permissions_for_tables() {
			DatabaseTableStructure pesa = new DatabaseTableStructure("pesa");
			assertEquals(_Table.TABLEMANAGEMENT_NO_PERMISSIONS, pesa.getTablemanagementPermissions());
			pesa.setTablemanagementPermissions(_Table.TABLEMANAGEMENT_UPDATE_ONLY);
			assertEquals(_Table.TABLEMANAGEMENT_UPDATE_ONLY, pesa.getTablemanagementPermissions());
			pesa.setTablemanagementPermissions(_Table.TABLEMANAGEMENT_UPDATE_INSERT);
			assertEquals(_Table.TABLEMANAGEMENT_UPDATE_INSERT, pesa.getTablemanagementPermissions());
			pesa.setTablemanagementPermissions(_Table.TABLEMANAGEMENT_UPDATE_INSERT_DELETE);
			assertEquals(_Table.TABLEMANAGEMENT_UPDATE_INSERT_DELETE, pesa.getTablemanagementPermissions());
		}
		
		public void test_it_throws_exception_if_trying_to_manage_table_that_has_no_permissions() throws Exception {
			data.set(Const.TABLE, "no_permissions");
			try {
				FunctionalityExecutor.execute(tablemanagementFunctionality);
				fail("Should throw exception");
			} catch (UnsupportedOperationException e) {
				assertTrue("Mentions the name of the table", e.toString().contains("no_permissions"));
			}
		}
		
		public void test_it_throws_exception_for_all_ops_if_no_permissions() throws Exception {
			data.set(Const.TABLE, "no_permissions");
			data.setAction(Const.UPDATE);
			try {
				FunctionalityExecutor.execute(tablemanagementFunctionality);
				fail("Should throw exception");
			} catch (UnsupportedOperationException e) {
			}
			
			data.setAction(Const.INSERT);
			try {
				FunctionalityExecutor.execute(tablemanagementFunctionality);
				fail("Should throw exception");
			} catch (UnsupportedOperationException e) {
			}
			
			data.setAction(Const.DELETE);
			try {
				FunctionalityExecutor.execute(tablemanagementFunctionality);
				fail("Should throw exception");
			} catch (UnsupportedOperationException e) {
			}
		}
		
		public void test_permissions___update_only() throws Exception {
			data.set(Const.TABLE, "update_only");
			data.setAction(Const.UPDATE);
			try {
				FunctionalityExecutor.execute(tablemanagementFunctionality);
			} catch (UnsupportedOperationException e) {
				fail("Should allow update");
			}
			
			data.setAction(Const.INSERT);
			try {
				FunctionalityExecutor.execute(tablemanagementFunctionality);
				fail("Should throw exception");
			} catch (UnsupportedOperationException e) {
			}
			
			data.setAction(Const.DELETE);
			try {
				FunctionalityExecutor.execute(tablemanagementFunctionality);
				fail("Should throw exception");
			} catch (UnsupportedOperationException e) {
			}
		}
		
		public void test_permissions__update_insert() throws Exception {
			data.set(Const.TABLE, "update_insert");
			data.setAction(Const.UPDATE);
			try {
				FunctionalityExecutor.execute(tablemanagementFunctionality);
			} catch (UnsupportedOperationException e) {
				fail("Should allow update");
			}
			
			data.setAction(Const.INSERT);
			try {
				FunctionalityExecutor.execute(tablemanagementFunctionality);
			} catch (UnsupportedOperationException e) {
				fail("Should allow insert " + e);
			}
			
			data.setAction(Const.DELETE);
			try {
				FunctionalityExecutor.execute(tablemanagementFunctionality);
				fail("Should throw exception");
			} catch (UnsupportedOperationException e) {
			}
		}
		
		public void test_permissions___update_insert_delete() throws Exception {
			data.set(Const.TABLE, "reviiri");
			data.setAction(Const.UPDATE);
			try {
				FunctionalityExecutor.execute(tablemanagementFunctionality);
			} catch (UnsupportedOperationException e) {
				fail("Should allow update");
			}
			
			data.setAction(Const.INSERT);
			try {
				FunctionalityExecutor.execute(tablemanagementFunctionality);
			} catch (UnsupportedOperationException e) {
				fail("Should allow insert");
			}
			
			data.setAction(Const.DELETE);
			try {
				FunctionalityExecutor.execute(tablemanagementFunctionality);
			} catch (UnsupportedOperationException e) {
				fail("Should allow delete");
			}
		}
		
		public void test_it_fetches_rowids() throws Exception {
			data.set(Const.TABLE, "laji");
			data.searchparameters().getKaynti().get("id").setValue("BB");
			data.setAction(Const.SEARCH);
			
			FunctionalityExecutor.execute(tablemanagementFunctionality);
			
			assertEquals(2, data.results().size());
			assertEquals("Tintti 1", data.results().get(0).getKaynti().get("nimi").getValue());
			assertEquals("Tintti 2", data.results().get(1).getKaynti().get("nimi").getValue());
			
			assertEquals("AAAA", data.results().get(0).getKaynti().getRowidValue());
			assertEquals("AAAB", data.results().get(1).getKaynti().getRowidValue());
			
		}
		
		public void test_calling_update_with_no_updateparameters() throws Exception {
			data.set(Const.TABLE, "reviiri");
			data.setAction(Const.UPDATE);
			FunctionalityExecutor.execute(tablemanagementFunctionality);
			
			checkHasErrors();
			assertEquals(_Validator.SYSTEM_ERROR, data.errors().get("reviiri"));
		}
		
		private void checkHasErrors() {
			assertFalse("Should have errors", data.errors().isEmpty());
		}
		
		/*
		 * Table reviiri = new Table("reviiri");
		 * reviiri.addColumn("reviiriid", _Column.INTEGER, 5);
		 * reviiri.addColumn("nimi", _Column.VARCHAR, 25);
		 * reviiri.addColumn("kirj_pvm", _Column.DATE);
		 * reviiri.addColumn("muutos_pvm", _Column.DATE);
		 * reviiri.addColumn("loyto_pvm", _Column.DATE);
		 */
		public void test_calling_update_with_correct_updateparameters() throws Exception {
			data.set(Const.TABLE, "reviiri");
			data.setAction(Const.UPDATE);
			data.updateparameters().getReviiri().setRowidValue("CCCA");
			data.updateparameters().getReviiri().get("reviiriid").setValue("1");
			data.updateparameters().getReviiri().get("nimi").setValue("Reviiri 1");
			data.updateparameters().getReviiri().get("loyto_pvm").setValue("1.1.2009");
			
			FunctionalityExecutor.execute(tablemanagementFunctionality);
			
			checkNoErrors();
		}
		
		public void test_calling_update_with_non_matching_rowid_and_numeric_id() throws Exception {
			data.set(Const.TABLE, "reviiri");
			data.setAction(Const.UPDATE);
			data.updateparameters().getReviiri().setRowidValue("CCCA");
			data.updateparameters().getReviiri().get("reviiriid").setValue("252");
			data.updateparameters().getReviiri().get("nimi").setValue("Reviiri 1");
			
			FunctionalityExecutor.execute(tablemanagementFunctionality);
			
			checkHasErrors();
			assertEquals(_Validator.NO_LONGER_EXISTS, data.errors().get("reviiri"));
		}
		
		public void test_it_validates_table_values___date_added_date_modified_are_not_required() throws Exception {
			data.set(Const.TABLE, "reviiri");
			data.setAction(Const.UPDATE);
			data.updateparameters().getReviiri().setRowidValue("CCCA");
			data.updateparameters().getReviiri().get("reviiriid").setValue("1");
			data.updateparameters().getReviiri().get("nimi").setValue("Reviiri --------------tämä on liian pitkä-------------------");
			
			FunctionalityExecutor.execute(tablemanagementFunctionality);
			
			checkHasErrors();
			assertEquals(_Validator.TOO_LONG, data.errors().get("reviiri.nimi"));
			assertEquals(_Validator.CAN_NOT_BE_NULL, data.errors().get("reviiri.loyto_pvm"));
		}
		
		/*
		 * Table laji = new Table("laji");
		 * laji.addColumn("id", Column.VARCHAR, 6);
		 * laji.addColumn("nimi", Column.VARCHAR, 20);
		 * laji.addColumn("kommentti", Column.VARCHAR, 250);
		 * laji.addColumn("minimipaino", Column.DECIMAL, 3, 2);
		 * laji.addColumn("kotikunta", Column.VARCHAR, 6);
		 * laji.column("id").setTypeToImmutablePartOfAKey();
		 * laji.setTablemanagementPermissions(_Table.TABLEMANAGEMENT_UPDATE_INSERT_DELETE);
		 * tablestemplate.setKaynti(laji);
		 */
		
		public void test_it_sets_userid_to__userid_column() throws Exception {
			data.set(Const.TABLE, "laji");
			data.setAction(Const.INSERT);
			data.updateparameters().getKaynti().get("id").setValue("TINT_1");
			data.updateparameters().getKaynti().get("nimi").setValue("Tintti ykkänen");
			data.updateparameters().getKaynti().get("kommentti").setValue("asd");
			data.updateparameters().getKaynti().get("minimipaino").setValue("0");
			data.updateparameters().getKaynti().get("kotikunta").setValue("asd");
			FunctionalityExecutor.execute(tablemanagementFunctionality);
			checkNoErrors();
			assertEquals("Esko Piirainen", data.updateparameters().getKaynti().get("userid").getValue());
		}
		
		public void test_calling_update_with_matching_rowid_and_immutablevarcharpartkey() throws Exception {
			data.set(Const.TABLE, "laji");
			data.setAction(Const.UPDATE);
			data.updateparameters().getKaynti().setRowidValue("AAAA");
			data.updateparameters().getKaynti().get("id").setValue("TINT_1");
			data.updateparameters().getKaynti().get("nimi").setValue("Tintti ykkänen");
			data.updateparameters().getKaynti().get("kommentti").setValue("asd");
			data.updateparameters().getKaynti().get("minimipaino").setValue("0");
			data.updateparameters().getKaynti().get("kotikunta").setValue("asd");
			
			FunctionalityExecutor.execute(tablemanagementFunctionality);
			
			checkNoErrors();
		}
		
		public void test_calling_update_with_non_matching_rowid_and_immutablevarcharpartkey() throws Exception {
			data.set(Const.TABLE, "laji");
			data.setAction(Const.UPDATE);
			data.updateparameters().getKaynti().setRowidValue("foo");
			data.updateparameters().getKaynti().get("id").setValue("TINT_1");
			data.updateparameters().getKaynti().get("nimi").setValue("Tintti ykkänen");
			
			FunctionalityExecutor.execute(tablemanagementFunctionality);
			
			checkHasErrors();
			assertEquals(_Validator.NO_LONGER_EXISTS, data.errors().get("laji"));
		}
		
		public void test_if_validator_finds_errors_it_returns_with_same_searchparameters_but_one_resultrow_and_resultrow_contains_the_row_that_was_being_updated() throws Exception {
			data.set(Const.TABLE, "laji");
			data.searchparameters().getKaynti().get("nimi").setValue("tintti");
			data.setAction(Const.UPDATE);
			data.updateparameters().getKaynti().setRowidValue("AAAA");
			data.updateparameters().getKaynti().get("id").setValue("TINT_1");
			data.updateparameters().getKaynti().get("nimi").setValue("Tintti ykkänen ----------tämä on liian pitkä ------");
			
			FunctionalityExecutor.execute(tablemanagementFunctionality);
			
			assertEquals(_Validator.TOO_LONG, data.errors().get("laji.nimi"));
			assertEquals("tintti", data.searchparameters().getKaynti().get("nimi").getValue());
			assertEquals("", data.searchparameters().getKaynti().get("id").getValue());
			assertEquals(1, data.results().size());
			_PesaDomainModelRow result = data.results().get(0);
			assertEquals("AAAA", result.getKaynti().getRowidValue());
			assertEquals("TINT_1", result.getKaynti().get("id").getValue());
			assertEquals("Tintti ykkänen ----------tämä on liian pitkä ------", result.getKaynti().get("nimi").getValue());
		}
		
		/*
		 * _Tables resultrow = resultdata.newResultRow();
		 * if (!tintti_1_update_called)
		 * resultrow.laji().column("nimi").setValue("Tintti 1");
		 * else
		 * resultrow.laji().column("nimi").setValue("Tintti ykkänen");
		 * resultrow.laji().setRowid("AAAA");
		 * resultrow = resultdata.newResultRow();
		 * resultrow.laji().column("nimi").setValue("Tintti 2");
		 * resultrow.laji().setRowid("AAAB");
		 */
		public void test_if_validator_doesnt_find_errors_it_calls_update_and_does_search() throws Exception {
			data.set(Const.TABLE, "laji");
			data.searchparameters().getKaynti().get("nimi").setValue("tintti");
			data.setAction(Const.UPDATE);
			data.updateparameters().getKaynti().setRowidValue("AAAA");
			data.updateparameters().getKaynti().get("id").setValue("TINT_1");
			data.updateparameters().getKaynti().get("nimi").setValue("Tintti ykkänen");
			data.updateparameters().getKaynti().get("kommentti").setValue("asd");
			data.updateparameters().getKaynti().get("minimipaino").setValue("0");
			data.updateparameters().getKaynti().get("kotikunta").setValue("asd");
			
			FunctionalityExecutor.execute(tablemanagementFunctionality);
			
			checkNoErrors();
			assertEquals(2, data.results().size());
			assertEquals("tintti", data.searchparameters().getKaynti().get("nimi").getValue());
			assertEquals("", data.searchparameters().getKaynti().get("id").getValue());
			_PesaDomainModelRow result = data.results().get(0);
			assertEquals("Tintti ykkänen", result.getKaynti().get("nimi").getValue());
			result = data.results().get(1);
			assertEquals("Tintti 2", result.getKaynti().get("nimi").getValue());
		}
		
		public void test_calling_delete_with_no_updateparameters() throws Exception {
			data.set(Const.TABLE, "reviiri");
			data.setAction(Const.DELETE);
			FunctionalityExecutor.execute(tablemanagementFunctionality);
			
			checkHasErrors();
			assertEquals(_Validator.SYSTEM_ERROR, data.errors().get("reviiri"));
		}
		
		public void test_calling_delete_with_existing_rowid() throws Exception {
			data.set(Const.TABLE, "reviiri");
			data.setAction(Const.DELETE);
			data.updateparameters().getReviiri().setRowidValue("CCCA");
			
			FunctionalityExecutor.execute(tablemanagementFunctionality);
			
			checkNoErrors();
		}
		
		public void test_calling_delete_with_nonexisting_rowid() throws Exception {
			data.set(Const.TABLE, "reviiri");
			data.setAction(Const.DELETE);
			data.updateparameters().getReviiri().setRowidValue("FOOO");
			
			FunctionalityExecutor.execute(tablemanagementFunctionality);
			
			checkHasErrors();
			assertEquals(_Validator.NO_LONGER_EXISTS, data.errors().get("reviiri.rowid"));
		}
		
		public void test_if_validator_finds_errors_when_trying_to_delete_it_returns_search_results_as_if_doing_a_search() throws Exception {
			data.set(Const.TABLE, "laji");
			data.searchparameters().getKaynti().get("nimi").setValue("tintti");
			data.setAction(Const.DELETE);
			data.updateparameters().getKaynti().setRowidValue("FOOO");
			
			FunctionalityExecutor.execute(tablemanagementFunctionality);
			
			assertEquals(_Validator.NO_LONGER_EXISTS, data.errors().get("laji.rowid"));
			assertEquals("tintti", data.searchparameters().getKaynti().get("nimi").getValue());
			assertEquals("", data.searchparameters().getKaynti().get("id").getValue());
			assertEquals(2, data.results().size());
			_PesaDomainModelRow result = data.results().get(0);
			assertEquals("AAAA", result.getKaynti().getRowidValue());
			assertEquals("Tintti 1", result.getKaynti().get("nimi").getValue());
			result = data.results().get(1);
			assertEquals("AAAB", result.getKaynti().getRowidValue());
			assertEquals("Tintti 2", result.getKaynti().get("nimi").getValue());
		}
		
		public void test_if_validator_doesnt_find_errors_it_calls_delete_does_search_with_original_searchvalues() throws Exception {
			data.set(Const.TABLE, "laji");
			data.searchparameters().getKaynti().get("nimi").setValue("tintti");
			data.setAction(Const.DELETE);
			data.updateparameters().getKaynti().setRowidValue("AAAA");
			
			FunctionalityExecutor.execute(tablemanagementFunctionality);
			
			checkNoErrors();
			assertEquals(1, data.results().size());
			assertEquals("tintti", data.searchparameters().getKaynti().get("nimi").getValue());
			assertEquals("", data.searchparameters().getKaynti().get("id").getValue());
			_PesaDomainModelRow result = data.results().get(0);
			assertEquals("Tintti 2", result.getKaynti().get("nimi").getValue());
		}
		
		public void test_calling_insert_with_no_updateparameters____numericid_not_required___added_modified_dates_not_required() throws Exception {
			data.set(Const.TABLE, "reviiri");
			data.setAction(Const.INSERT);
			FunctionalityExecutor.execute(tablemanagementFunctionality);
			
			checkHasErrors();
			assertEquals(_Validator.CAN_NOT_BE_NULL, data.errors().get("reviiri.nimi"));
			assertEquals(_Validator.CAN_NOT_BE_NULL, data.errors().get("reviiri.loyto_pvm"));
			assertEquals(null, data.errors().get("reviiri.reviiriid"));
			assertEquals(null, data.errors().get("reviiri.kirj_pvm"));
			assertEquals(null, data.errors().get("reviiri.muutos_pvm"));
		}
		
		public void test_calling_insert_with_no_updateparameters____uniquekeys_are_required_to_be_given() throws Exception {
			data.set(Const.TABLE, "laji");
			data.setAction(Const.INSERT);
			FunctionalityExecutor.execute(tablemanagementFunctionality);
			
			checkHasErrors();
			assertEquals(_Validator.CAN_NOT_BE_NULL, data.errors().get("laji.id"));
			assertEquals(_Validator.CAN_NOT_BE_NULL, data.errors().get("laji.nimi"));
			assertEquals(_Validator.CAN_NOT_BE_NULL, data.errors().get("laji.kommentti"));
			assertEquals(null, data.errors().get("laji.minimipaino"));
			assertEquals(_Validator.CAN_NOT_BE_NULL, data.errors().get("laji.kotikunta"));
			assertEquals(null, data.errors().get("laji.kirj_pvm"));
			assertEquals(null, data.errors().get("laji.muutos_pvm"));
		}
		
		public void test_calling_insert_with_correct_updateparameters() throws Exception {
			data.set(Const.TABLE, "reviiri");
			data.setAction(Const.INSERT);
			data.updateparameters().getReviiri().get("nimi").setValue("Uusi reviiri");
			data.updateparameters().getReviiri().get("loyto_pvm").setValue("1.1.2009");
			
			FunctionalityExecutor.execute(tablemanagementFunctionality);
			
			checkNoErrors();
		}
		
		/*
		 * Table laji = new Table("laji");
		 * laji.addColumn("id", Column.VARCHAR, 6);
		 * laji.addColumn("nimi", Column.VARCHAR, 20);
		 * laji.addColumn("kommentti", Column.VARCHAR, 250);
		 * laji.addColumn("minimipaino", Column.DECIMAL, 3, 2);
		 * laji.addColumn("kotikunta", Column.VARCHAR, 6);
		 * laji.column("id").setTypeToImmutablePartOfAKey();
		 * laji.setTablemanagementPermissions(_Table.TABLEMANAGEMENT_UPDATE_INSERT_DELETE);
		 * tablestemplate.setKaynti(laji);
		 */
		public void test_calling_insert_with_correct_updateparameters2() throws Exception {
			data.set(Const.TABLE, "laji");
			data.setAction(Const.INSERT);
			data.updateparameters().getKaynti().get("id").setValue("UUSLA");
			data.updateparameters().getKaynti().get("nimi").setValue("Uusi laji");
			data.updateparameters().getKaynti().get("kommentti").setValue("Tämä on uuden lajin kommentti.");
			data.updateparameters().getKaynti().get("minimipaino").setValue("25,0");
			data.updateparameters().getKaynti().get("kotikunta").setValue("VANTAA");
			
			FunctionalityExecutor.execute(tablemanagementFunctionality);
			for (Map.Entry<String, String> e : data.errors().entrySet()) {
				System.err.println(e.getKey() + ":" + e.getValue()); // Prints something only if there are errors -> test fails
			}
			
			checkNoErrors();
		}
		
		public void test_it_doesnt_accept_already_existing__immutablekeycombination() throws Exception {
			data.set(Const.TABLE, "laji");
			data.setAction(Const.INSERT);
			data.updateparameters().getKaynti().get("id").setValue("HALIB");
			
			FunctionalityExecutor.execute(tablemanagementFunctionality);
			
			assertEquals(_Validator.ALREADY_IN_USE, data.errors().get("laji.id"));
		}
		
		public void test_calling_insert_with_rowid() throws Exception {
			data.set(Const.TABLE, "reviiri");
			data.setAction(Const.INSERT);
			data.updateparameters().getReviiri().setRowidValue("CCCA");
			
			FunctionalityExecutor.execute(tablemanagementFunctionality);
			
			checkHasErrors();
			assertEquals(_Validator.SYSTEM_ERROR, data.errors().get("reviiri"));
		}
		
		public void test_calling_insert_with_the_id_field_that_is_numeric_id() throws Exception {
			data.set(Const.TABLE, "reviiri");
			data.setAction(Const.INSERT);
			data.updateparameters().getReviiri().get("reviiriid").setValue("125");
			
			FunctionalityExecutor.execute(tablemanagementFunctionality);
			
			checkHasErrors();
			assertEquals(_Validator.SYSTEM_ERROR, data.errors().get("reviiri"));
		}
		
		public void test_it_validates_insert_values() throws Exception {
			data.set(Const.TABLE, "reviiri");
			data.setAction(Const.INSERT);
			data.updateparameters().getReviiri().get("nimi").setValue("Uusi reviiri -----------------tamaonliianpitka-----------------");
			data.updateparameters().getReviiri().get("loyto_pvm").setValue("1.1.200");
			
			FunctionalityExecutor.execute(tablemanagementFunctionality);
			
			checkHasErrors();
			assertEquals(2, data.errors().size());
			assertEquals(_Validator.TOO_LONG, data.errors().get("reviiri.nimi"));
			assertEquals(_Validator.INVALID_DATE, data.errors().get("reviiri.loyto_pvm"));
		}
		
		public void test_if_validator_finds_errors_it_returns_with_same_updateparameters___no_searchparameters_no_results_action_is_insert() throws Exception {
			data.set(Const.TABLE, "laji");
			data.setAction(Const.INSERT);
			data.updateparameters().getKaynti().get("id").setValue("");
			data.updateparameters().getKaynti().get("nimi").setValue("Kayntinimi");
			
			FunctionalityExecutor.execute(tablemanagementFunctionality);
			
			checkHasErrors();
			assertEquals(_Validator.CAN_NOT_BE_NULL, data.errors().get("laji.id"));
			assertEquals(0, data.results().size());
			assertEquals("", data.updateparameters().getKaynti().get("id").getValue());
			assertEquals("Kayntinimi", data.updateparameters().getKaynti().get("nimi").getValue());
		}
		
		public void test_if_validator_doesnt_find_errors_it_calls_insert_and_does_search_____and_sets_success_text() throws Exception {
			data.set(Const.TABLE, "reviiri");
			data.setAction(Const.INSERT);
			data.updateparameters().getReviiri().get("nimi").setValue("Uusi reviiri");
			data.updateparameters().getReviiri().get("loyto_pvm").setValue("1.1.2009");
			
			FunctionalityExecutor.execute(tablemanagementFunctionality);
			
			checkNoErrors();
			assertEquals("", data.searchparameters().getReviiri().get("reviiriid").getValue());
			assertEquals("", data.searchparameters().getReviiri().get("nimi").getValue());
			assertEquals(1, data.results().size());
			_PesaDomainModelRow result = data.results().get(0);
			assertEquals("Uusi reviiri", result.getReviiri().get("nimi").getValue());
			assertEquals(Success.INSERT, data.successText());
		}
		
		public void test_it_sets_success_text_after_a_successful_update() throws Exception {
			data.set(Const.TABLE, "reviiri");
			data.setAction(Const.UPDATE);
			data.updateparameters().getReviiri().setRowidValue("CCCA");
			data.updateparameters().getReviiri().get("reviiriid").setValue("1");
			data.updateparameters().getReviiri().get("nimi").setValue("Reviiri 1");
			data.updateparameters().getReviiri().get("loyto_pvm").setValue("2.2.1990");
			
			FunctionalityExecutor.execute(tablemanagementFunctionality);
			
			checkNoErrors();
			assertEquals(Success.UPDATE, data.successText());
		}
		
		public void test_it_sets_success_text_after_a_successful_delete() throws Exception {
			data.set(Const.TABLE, "laji");
			data.searchparameters().getKaynti().get("nimi").setValue("tintti");
			data.setAction(Const.DELETE);
			data.updateparameters().getKaynti().setRowidValue("AAAA");
			
			FunctionalityExecutor.execute(tablemanagementFunctionality);
			
			checkNoErrors();
			assertEquals(Success.DELETE, data.successText());
		}
		
		public void test_it_adds_tablemanagement_permissions__to_data() throws Exception {
			data.set(Const.TABLE, "laji");
			FunctionalityExecutor.execute(tablemanagementFunctionality);
			assertEquals("" + _Table.TABLEMANAGEMENT_UPDATE_INSERT_DELETE, data.get(Const.TABLEMANAGEMENT_PERMISSIONS));
		}
		
		public void test__it_passes_a_valid_reference_when_updating() throws Exception {
			data.set(Const.TABLE, "olosuhde");
			data.setAction(Const.UPDATE);
			data.updateparameters().getOlosuhde().setRowidValue("AAAA");
			data.updateparameters().getOlosuhde().get("asuva_laji").setValue("HALALB");
			FunctionalityExecutor.execute(tablemanagementFunctionality);
			checkNoErrors();
		}
		
		public void test__it_passes_a_valid_reference_when_inserting() throws Exception {
			data.set(Const.TABLE, "olosuhde");
			data.setAction(Const.INSERT);
			data.updateparameters().getOlosuhde().get("asuva_laji").setValue("HALALB");
			FunctionalityExecutor.execute(tablemanagementFunctionality);
			checkNoErrors();
		}
		
		public void test__it_fails_nonexisting__reference_value_when_updating() throws Exception {
			data.set(Const.TABLE, "olosuhde");
			data.setAction(Const.UPDATE);
			data.updateparameters().getOlosuhde().setRowidValue("AAAA");
			data.updateparameters().getOlosuhde().get("asuva_laji").setValue("FOOFOO");
			FunctionalityExecutor.execute(tablemanagementFunctionality);
			assertEquals(_Validator.INVALID_REFERENCE_VALUE, data.errors().get("olosuhde.asuva_laji"));
		}
		
		public void test__it_fails_nonexisting__reference_value_when_inserting() throws Exception {
			data.set(Const.TABLE, "olosuhde");
			data.setAction(Const.INSERT);
			data.updateparameters().getOlosuhde().get("asuva_laji").setValue("FOOFOO");
			FunctionalityExecutor.execute(tablemanagementFunctionality);
			assertEquals(_Validator.INVALID_REFERENCE_VALUE, data.errors().get("olosuhde.asuva_laji"));
		}
		
		public void test__it_allows_to_delete_a_row_which_reference_value_is_not_being_used() throws Exception {
			data.set(Const.TABLE, "laji");
			data.setAction(Const.DELETE);
			data.updateparameters().getKaynti().setRowidValue("AAAA");
			FunctionalityExecutor.execute(tablemanagementFunctionality);
			checkNoErrors();
		}
		
		public void test__it_does_not_allow_to_delete_a_row_which_reference_value_is_being_used() throws Exception {
			data.set(Const.TABLE, "laji");
			data.setAction(Const.DELETE);
			data.updateparameters().getKaynti().setRowidValue("HALALBrivi");
			FunctionalityExecutor.execute(tablemanagementFunctionality);
			assertEquals(1, data.errors().size());
			assertEquals(_Validator.CAN_NOT_DELETE_IS_BEING_USED, data.errors().get("laji"));
		}
		
	}
	
	public static class ApplicationExtendsBasicValidator extends TestCase {
		
		private class FrameworkBasicFunctionalityValidator {
			protected String	theString;
			
			public FrameworkBasicFunctionalityValidator() {
				theString = "at first";
			}
			
			public void validate() {
				theString = "after validation ";
			}
			
			@Override
			public String toString() {
				return theString;
			}
		}
		
		private class BasicFunctionalityValidatorExtendedByTheApplicationToProvideApplicationSpecificValidations extends FrameworkBasicFunctionalityValidator {
			@Override
			public void validate() {
				super.validate();
				theString = theString + " even more validation ";
			}
		}
		
		public void test____this_is_how_applications_will_extend_basic_validations_if_needed() {
			FrameworkBasicFunctionalityValidator validator = new FrameworkBasicFunctionalityValidator();
			assertEquals("at first", validator.toString());
			validator.validate();
			assertEquals("after validation ", validator.toString());
			validator = new BasicFunctionalityValidatorExtendedByTheApplicationToProvideApplicationSpecificValidations();
			assertEquals("at first", validator.toString());
			validator.validate();
			assertEquals("after validation  even more validation ", validator.toString());
		}
		
	}
	
}
