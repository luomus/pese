<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html 
     PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
     "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<#include "macro.ftl">

<#assign ROW_ID = 0>
<#assign UNIQUE_KEY = 1>
<#assign IMMUTABLE_KEY = 2>
<#assign NORMAL_FIELD = 3>
<#assign DATE_ADDED = 4>
<#assign DATE_MODIFIED = 5>
<#assign PASSWORD = 6>
<#assign USER_ID = 7>

<#assign TABLEMANAGEMENT_NO_PERMISSIONS = "0">
<#assign TABLEMANAGEMENT_UPDATE_ONLY = "1">
<#assign TABLEMANAGEMENT_UPDATE_INSERT = "2">
<#assign TABLEMANAGEMENT_UPDATE_INSERT_DELETE = "3">
<#assign TABLEMANAGEMENT_INSERT_ONLY = "4">
	
<#assign  VARCHAR = 1>
<#assign  INTEGER = 2>
<#assign  DECIMAL = 3>
<#assign  DATE = 4>


<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
     
	<title>${texts["title_"+system.page]}</title>
	
	<link rel="stylesheet" href="${system.commonURL}/lintuvaara_shared_layout.css?${system.last_update_timestamp}" type="text/css" />
	<link rel="stylesheet" href="${system.commonURL}/lintuvaara_small_header.css?${system.last_update_timestamp}" type="text/css" />
	<link rel="stylesheet" href="${system.commonURL}/pese_peto.css?${system.last_update_timestamp}" type="text/css" />
	
    <script type="text/javascript" src="${system.commonURL}/jquery-latest.js?${system.last_update_timestamp}"></script>
	<script type="text/javascript" src="${system.commonURL}/jquery-ui/js/jquery-ui-1.10.4.min.js?${system.last_update_timestamp}"></script>
	<link rel="stylesheet" href="${system.commonURL}/jquery-ui/css/jquery-ui-1.10.4.min.css?${system.last_update_timestamp}" type="text/css" />
	
	<script type="text/javascript" src="${system.commonURL}/pese.js?${system.last_update_timestamp}"></script>
	<link rel="stylesheet" href="${system.commonURL}/pese_lomakevastaanottajat.css?${system.last_update_timestamp}" type="text/css" />
	
	<script type="text/javascript" src="${system.commonURL}/jquery.tablesorter.min.js?${system.last_update_timestamp}"></script> 
	<script type="text/javascript" src="${system.commonURL}/jquery.metadata.js?${system.last_update_timestamp}"></script>
    <script type="text/javascript"> 
    	jQuery(document).ready(function(){
    		$("#resultsTable").tablesorter();
    	}); 
    </script>
    
    <link rel="stylesheet" href="${system.commonURL}/chosen.css?${system.last_update_timestamp}" type="text/css" media="screen, print" charset="utf-8" />
 	<script type="text/javascript" src="${system.commonURL}/chosen.jquery.js?${system.last_update_timestamp}"></script>
  	<script type="text/javascript">
		jQuery(document).ready(function(){
			jQuery(".chosen").chosen({ search_contains: true });
		});
  	</script>
    
</head> 


<body>
    
  <div id="header">
    <div id="mainlogo"> 
        <img src="${system.commonURL}/img/lintuvaara.gif" alt="Lintuvaaraan" />  
          <#if system.StagingMode>     <span style="color: red; font-weight: bold; font-size: 200%">TESTIYMPÄRISTÖ</span></#if>
          <#if system.DevelopmentMode> <span style="color: red; font-weight: bold; font-size: 200%">KEHITYSYMPÄRISTÖ</span></#if>
    </div>
        
    <div id="lintuvaaraLink"> 
        <a href="${data.lintuvaaraURL}"> LINTUVAARA </a> 
    </div>
    
    <div id="infoContainer">
      
      <div id="info">                  
          <ul id="infoTabs">
              <li><a href="${system.baseURL}?page=logout">Kirjaudu ulos</a></li>
              <li id="currentUserInfo">Tervetuloa, ${system.user_name!system.user_id}</li>
          </ul>
      </div>
    </div>
  </div>

<table class="main">
  <tr>
    <td class="main_menu">
    
      <a href="${system.baseURL}?page=search">  ${texts.menu_search}  </a>
      <a href="${system.baseURL}?page=kirjekyyhky_management">ETEINEN (${data.kirjekyyhky_count})</a>
      <a href="${system.baseURL}?page=tarkastus"> Syöttö </a> 
      <a href="${system.baseURL}?page=reports">  ${texts.menu_report}  </a>
      <a href="${system.baseURL}?page=filemanagement">  ${texts.menu_filemanagement}  </a>  
      <a href="${system.baseURL}?page=seurantaruutumanagement">  ${texts.menu_aputaulut}  </a>
       
    </td>
  </tr>
  <tr>
    <td class="sub_menu">
    
      <#if system.page == "search">
        <div> <a href="${system.baseURL}?page=search"> Uusi haku </a>  </div> 
        <div> <a href="#"> Tallennetut haut </a>  </div> 
      </#if>
      
      <form action="${system.baseURL}" method="post">
      <input type="hidden" name="page" value="tarkastus" />
      <input type="hidden" name="action" value="fill_for_insert" />
      
      <#if system.page == "tarkastus" || system.page == "pesinnat" || (system.page == "tablemanagement" && data.table == "muutosloki")>
        <div> <a href="${system.baseURL}?page=tarkastus"> Uusi pesä </a> </div> 
        <div> 
          Uusi tarkastus pesä ID:lle <input type="text" name="updateparameters.pesa.id" size="6" />
          ja vuodelle  <input type="text" name="tark_vuosi" value="${system.currentYear}" size="4" />
          <input type="submit" value="Etene" /> 
        </div> 
        <div> <a href="${system.baseURL}?page=pesinnat"> Pesien ja tarkastusten hallinta </a>  </div> 
        <div> <a href="${system.baseURL}?page=tablemanagement&amp;table=muutosloki&amp;action=search"> Muutosloki </a> </div>
      </#if>
      
      </form>
      
      <#if system.page == "reports">  </#if>
      
      <#if system.page == "filemanagement">
        <div> <a href="${system.baseURL}?page=filemanagement&amp;folder=report"> Raportit </a>  </div> 
        <div> <a href="${system.baseURL}?page=filemanagement&amp;folder=pdf"> Tarkastuslomakkeet </a>  </div> 
      </#if>
      
      <#if (system.page == "tablemanagement" && data.table != "muutosloki") || system.page == "seurantaruutumanagement">
      		<div> <a href="${system.baseURL}?page=seurantaruutumanagement"> Seurantaruudut </a>  </div>
      		<div> <a href="${system.baseURL}?page=tablemanagement&amp;table=aputaulu"> Muuttujat </a>  </div>
      </#if>
      
    </td>
  </tr>
  <tr>
    <td class="page_content">



<div id="historyActions" class="hidden">
  <#if history_actions?has_content>
    <a href="#"> Sulje  <span class="icon">X</span> </a> 
    <table>
      <caption> Viimeksi käsitellyt pesät</caption>
      <tr>
        <th> Aika </th>
        <th> Käyttäjä </th>
        <th> Toiminto </th>
        <th colspan="2"> Pesä </th>
        <th> Tarkastus </th>
      </tr>
    <#list history_actions as action>
      <tr>
        <td> ${action.date} </td>
        <td> ${action.user} </td>
        <td> ${texts["history_actionlist_text_" + action.action]} </td>
        <td style="text-align: center;"> ${action.row.pesa.id} </td>
        <td> ${action.row.pesa.kyla}, ${ selections["municipalities"][action.row.pesa.kunta] }  </td>
        <td> <a href="${system.baseURL}?page=tarkastus&amp;action=fill_for_update&amp;updateparameters.tarkastus.id=${action.row.tarkastus.id}" class="button-link"> ${action.row.tarkastus.vuosi}: ${action.row.tarkastus.pesiva_laji} ${action.row.tarkastus.pesimistulos} </a>  </td>
      </tr>
    </#list>
    </table>
  <#else>
    <a href="#"> Sulje  <span class="icon">X</span> </a> <br />
    Ei mitään näytettävää. (Ehkä järjestelmä on juuri käynnistetty uudelleen) 
  </#if>
</div>

<div id="historyActions_show">
  <a href="#"> Viimeksi käsitellyt pesät <span class="icon">+</span> </a>
</div>

<script type="text/javascript">
    $("#historyActions").hide();
    $("#historyActions_show").click(function () {
      $(this).hide();
      $("#historyActions").show('fast');
      return true;
    });
    $("#historyActions").click(function () {
      $(this).hide();
      $("#historyActions_show").show('fast');
      return true;
    });
</script>


