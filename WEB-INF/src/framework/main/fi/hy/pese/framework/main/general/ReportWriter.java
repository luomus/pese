package fi.hy.pese.framework.main.general;

import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._Row;
import fi.hy.pese.framework.main.general.data._Table;
import fi.luomus.commons.containers.KeyValuePair;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.Utils;

/**
 * Report writer provides tools for applications to write reports to a text file.
 * Anyone using this class must implement the abstract produceSpecific() -method.
 */
public abstract class ReportWriter<T extends _Row> {

	protected final _Request<T>	request;
	protected final String		filenamePrefix;
	protected PrintWriter		writer;

	public ReportWriter(_Request<T> request, String filenamePrefix) {
		this.request = request;
		this.filenamePrefix = filenamePrefix;
	}

	protected abstract boolean produceSpecific() throws Exception;

	/**
	 * Calls the abstract produceSpecific() method, which returns a boolean.
	 * If produceSpecific() returns true, the file is added to data.procudedfiles,
	 * but if it returns false the file is deleted and not added to data.producedfiles.
	 * If produceSpecific() throws an exception the file is deleted, and not added to data.producedfiles,
	 * and exception message is added to data.noticetexts along with the name of the report
	 * that produced the exception.
	 */
	public void produce() {
		File file = null;
		try {
			file = file();
			file.getParentFile().mkdirs();
			this.writer = new PrintWriter(file, request.config().characterEncoding());
			boolean success = produceSpecific();
			if (success) {
				request.data().addToProducedFiles(file.getName());
			} else {
				if (writer != null) writer.close();
				file.delete();
			}
		} catch (Exception e) {
			e.printStackTrace();
			request.data().addToNoticeTexts(filenamePrefix + ": " + e.toString() + " " + e.getStackTrace()[0]);
			deleteIfExists(file);
		} finally {
			if (writer != null) writer.close();
		}
	}

	private File file() {
		String pathname = request.config().reportFolder();
		String userid = "";
		if (request.session() != null) {
			userid = request.session().userId() + "_";
		}
		String filename = filenamePrefix + "_" + userid + DateUtils.getFilenameDatetime() + ".txt";
		return new File(pathname + File.separator + filename);
	}

	private void deleteIfExists(File file) {
		if (file != null && file.exists() && !file.isDirectory()) {
			file.delete();
		}
	}

	/**
	 * Writes a string
	 * @param text
	 */
	protected void write(String text) {
		if (text != null) writer.write(text);
	}

	protected void write(Integer integer) {
		write(Integer.toString(integer));
	}

	protected void write(Double d) {
		try {
			write(Double.toString(Utils.round(d, 2)));
		} catch (NumberFormatException e) {
			write("err");
		}
	}

	/**
	 * Writes a separator ( '|' )
	 */
	protected void separator() {
		writer.write("|");
	}

	/**
	 * Writers several separators, for example with parameter 4 writes '||||'
	 * @param count
	 */
	protected void separator(int count) {
		for (int i = 0; i < count; i++) {
			separator();
		}
	}

	/**
	 * Writes a new line
	 */
	protected void newLine() {
		writer.println();
	}

	/**
	 * Current date in format dd.MM.yyyy
	 * @return
	 */
	protected String currentDate() {
		return DateUtils.getCurrentDateTime("dd.MM.yyyy");
	}


	/**
	 * AJETTU: |6.6.2010||PARAMETREILLA: |kentta:|arvo||kentta2:|arvo|...
	 * where "AJETTU" is Const.RUN_DATE_TEXT and "PARAMETEREILLA" is Const.RUN_PARAMETERS_TEXT from UITexts
	 */
	protected void headerLine() {
		this.headerLine(null);
	}

	/**
	 * AJETTU: |6.6.2010||PARAMETREILLA: |kentta:|arvo||kentta2:|arvo|...
	 * where "AJETTU" is Const.RUN_DATE_TEXT and "PARAMETEREILLA" is Const.RUN_PARAMETERS_TEXT from UITexts, and
	 * @param includeParams true = will print the parameters, false = will not print parameters
	 * @param additionalParams a list of additional params to include in the header file (additional params are params that are not in searchparameters-row)
	 */
	protected void headerLine(List<KeyValuePair> additionalParams) {
		write(request.data().uiTexts().get(Const.RUN_DATE_TEXT));
		write(": ");
		separator(4);
		write(request.data().uiTexts().get(Const.RUN_PARAMETERS_TEXT));
		write(": ");
		for (_Table t : request.data().searchparameters()) {
			if (!t.hasValues()) continue;
			for (_Column c : t) {
				if (c.hasValue()) {
					writeShortnameOrFullname(c);
					write(": ");
					write(c.getValue().replace('|', ','));
					write("; ");
				}
			}
		}
		if (additionalParams != null) {
			for (KeyValuePair pair : additionalParams) {
				writeDescOrKey(pair.getKey());
				write(": ");
				write(pair.getValue().replace('|', ','));
				write("; ");
			}
		}
		newLine();
		write(currentDate());
	}

	protected void writeDescOrKey(String key) {
		String text = request.data().uiTexts().get(key);
		if (text == null) {
			write(key);
		} else {
			write(text);
		}
	}

	/**
	 * Writes the short name from language file for this column if it has been given.
	 * Else writes the full name.
	 * @param c
	 */
	protected void writeShortnameOrFullname(_Column c) {
		String shortname = shortNameFromUiTexts(c.getFullname());
		if (shortname != null) {
			write(shortname);
		} else {
			write(c.getFullname());
		}
	}

	/**
	 * Returns a shortname for a column from the language file
	 * @param fieldName
	 * @return
	 */
	protected String shortNameFromUiTexts(String fieldName) {
		String text = request.data().uiTexts().get(Const.PREFIX_FOR_SHORT_COLUMN_NAME + fieldName);
		if (text == null) return fieldName;
		return text;
	}

	protected void writeHeaders(String[] fieldnames) {
		for (String field : fieldnames) {
			String text = shortNameFromUiTexts(field);
			write(text);
			separator();
		}
	}

	/**
	 * Takes the value that is in yearcolumn and retuns a list of years.
	 * If value of the column is "2008", retuns list with ony entry, "2008".
	 * If value is "2004-2007" returns list with values 2004, 2005, 2006, 2007
	 * If value is 2000|2006 returns list with values 2000 and 2006.
	 * If column is null or value is "", returns an empty list.
	 * This function expects that the contents of the year column have been validated,
	 * for example if a range is given, the first range value is not larger than the second value,
	 * and that all values are valid integers.
	 * @param yearColumn
	 * @return
	 */
	protected List<Integer> yearsToPrint(_Column yearColumn) {
		List<Integer> years = new ArrayList<>();
		if (yearColumn == null) return years;

		String yearValue = yearColumn.getValue();
		if (yearValue.equals("")) return years;

		if (yearValue.contains("-")) {
			String[] split = yearValue.split("-");
			Integer start_year = new Integer(split[0]);
			Integer end_year = new Integer(split[1]);
			while (end_year >= start_year) {
				years.add(end_year);
				end_year--;
			}
		} else {
			for (String value : yearColumn.getValueList()) {
				years.add(new Integer(value));
			}
		}
		Collections.sort(years, Collections.reverseOrder());
		return years;
	}

	protected static boolean given(String s) {
		return s != null && s.length() > 0;
	}
}
