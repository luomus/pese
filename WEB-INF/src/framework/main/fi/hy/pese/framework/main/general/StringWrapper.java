package fi.hy.pese.framework.main.general;

/**
 * Wraps a String to "JavaBean" object.
 */
public class StringWrapper {

	private final String	s;

	/**
	 * Wraps a String to "JavaBean" object.
	 */
	public StringWrapper(String s) {
		this.s = s;
	}

	@Override
	public String toString() {
		return s;
	}
}
