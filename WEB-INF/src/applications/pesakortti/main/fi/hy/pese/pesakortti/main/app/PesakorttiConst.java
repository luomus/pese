package fi.hy.pese.pesakortti.main.app;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import fi.hy.pese.framework.main.app.functionality.validate.NumericRange;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.main.general.data._Table;
import fi.hy.pese.pesakortti.main.functionality.validation.PesasijaintiValidatorSpec;
import fi.luomus.commons.utils.CoordinateConverter;
import fi.luomus.commons.utils.Utils;

public class PesakorttiConst {

	public static final Object	YHDYSKUNTA_PAGE	= "yhdyskunta";
	public static final int	MAX_KOORD_TARKKUUS	= 1000000;
	public static final Collection<String>	TARKASTUS_FIELDS_TO_BE_PREFILLED_FOR_INSERTS	= Utils.collection(
			"pesa_korkeus", "puu_korkeus", "pesa_etaisyys_runko", "pesa_biotooppi", "pesa_biotooppi_ala", "pesa_suhde_reuna", "pesa_etaisyys_reuna", "pesa_ymp_biotooppi",
			"yhdyskunnassa", "yhdyskunta_laji"
			);
	public static final String		IKAKOODIT_AIKUINEN								= "ikakoodit_aikuinen";
	public static final String		IKAKOODIT_POIKANEN								= "ikakoodit_poikanen";
	public static final String		TAULUT											= "taulut";
	public static final String		APUTAULU										= "aputaulu";
	public static final String		K_E												= "k_e";

	public static final String		JOINS											= joins();

	private static String joins() {
		StringBuilder joins = new StringBuilder();
		joins.append("       pesa.id        =  tarkastus.pesa         ");
		joins.append(" AND   tarkastus.id   =  kaynti.tarkastus(+)    ");
		joins.append(" AND   tarkastus.id   =  poikanen.tarkastus(+)  ");
		joins.append(" AND   tarkastus.id   =  aikuinen.tarkastus(+)  ");
		joins.append(" AND   tarkastus.id   =  muna.tarkastus(+)      ");
		return joins.toString();
	}

	public static final String	ORDER_BY	= orderBy();

	private static String orderBy() {
		return " pesa.kunta, pesa.id, tarkastus.vuosi DESC, tarkastus.id, kaynti.pvm, kaynti.klo_h, kaynti.klo_min, kaynti.id, poikanen.id, aikuinen.id, muna.id ";
	}

	// public static final Collection<String> PESAN_KUNTO_DEAD = Utils.collection("U", "D", "E", "F", "R", "G", "H", "I");
	// public static final Collection<String> PESAN_KUNTO_ALIVE = Utils.collection("P", "O", "K", "T", "A", "J");
	// public static final Collection<String> PESIMISTULOS_SUCCESS = Utils.collection("P", "R", "L");
	// public static final Collection<String> PESIMISTULOS_IN_RISING_SUCCESS_ORDER = Utils.collection("U", "A", "K", "M", "P", "R", "L", "1", "2", "3", "7", "8", "9");

	public static final String	INVALID_TARK_VUOSI_TARK_PVM_YYYY	= "INVALID_TARK_VUOSI_TARK_PVM_YYYY";
	public static final String	INVALID_RAK_VUOSI					= "INVALID_RAK_VUOSI";
	public static final String	INVALID_LOYT_VUOSI					= "INVALID_LOYT_VUOSI";
	public static final String  INVALID_RAK_TAI_LOYT_VUOSI = "INVALID_RAK_TAI_LOYT_VUOSI";
	public static final String	PESA_IS_DEAD_CAN_NOT_INSERT			= "PESA_IS_DEAD_CAN_NOT_INSERT";
	public static final String	INVALID_REPORT_YEARS				= "INVALID_REPORT_YEARS";
	public static final String	NOT_INSPECTED_BUT_VALUES_GIVEN		= "NOT_INSPECTED_BUT_VALUES_GIVEN";
	public static final String  NEST_STATE_IS_DEAD_BUT_NOT_MARKED_DEAD  = "NEST_STATE_IS_DEAD_BUT_NOT_MARKED_DEAD";
	public static final String  ALL_KAYNTI_MUST_BE_FROM_TARK_VUOSI_OR_ONE_YEAR_BEFORE = "ALL_KAYNTI_MUST_BE_FROM_TARK_VUOSI_OR_ONE_YEAR_BEFORE";
	public static final String  INVALID_TARK_VUOSI_KAYNTI_VUOSI = "INVALID_TARK_VUOSI_KAYNTI_VUOSI";
	public static final String  INVALID_KAYNTI_VUOSI_LOYT_VUOSI = "INVALID_KAYNTI_VUOSI_LOYT_VUOSI";
	public static final String  INVALID_KAYNTI_VUOSI_RAK_VUOSI = "INVALID_KAYNTI_VUOSI_RAK_VUOSI";
	public static final String  PESIVA_LAJI_REQUIRED_WITH_THIS_NESTING_STAGE = "PESIVA_LAJI_REQUIRED_WITH_THIS_PESIMISTULOS";
	public static final String  PESIVA_LAJI_MUST_BE_EMPTY_WITH_THIS_NESTING_STAGE = "PESIVA_LAJI_MUST_BE_EMPTY_WITH_THIS_PESIMISTULOS";
	public static final String  PESA_CAN_NO_BE_HIGHER_THAN_TREE_HEIGHT = "PESA_CAN_NO_BE_HIGHER_THAN_TREE_HEIGHT";
	public static final String  PUU_LAJI_CAN_NOT_BE_GIVEN_FOR_THIS_TYPE_OF_NEST = "PUU_LAJI_CAN_NOT_BE_GIVEN_FOR_THIS_TYPE_OF_NEST";
	public static final String  RAK_LAJI_CAN_NOT_BE_GIVEN_FOR_THIS_TYPE_OF_NEST = "RAK_LAJI_CAN_NOT_BE_GIVEN_FOR_THIS_TYPE_OF_NEST";
	public static final String  THIS_NEST_STATUS_CODE_CAN_BE_USED_ONLY_FOR_NESTS_THAT_ARE_IN_TREE = "THIS_NEST_STATUS_CODE_CAN_BE_USED_ONLY_FOR_NESTS_THAT_ARE_IN_TREE";
	public static final String  THIS_NEST_STATUS_CODE_CAN_BE_USED_ONLY_FOR_NESTS_THAT_ARE_ON_GROUND = "THIS_NEST_STATUS_CODE_CAN_BE_USED_ONLY_FOR_NESTS_THAT_ARE_ON_GROUND";
	public static final String  MUST_HAVE_NESTING_SEQUENCE_MINUS_ONE = "MUST_HAVE_NESTING_SEQUENCE_MINUS_ONE";
	public static final String  NESTING_SEQUENCE_USED_MORE_THAN_ONCE = "NESTING_SEQUENCE_USED_MORE_THAN_ONCE";
	public static final String	NEST_TOO_HIGH_FOR_THIS_TYPE_OF_NEST	= "NEST_TOO_HIGH_FOR_THIS_TYPE_OF_NEST";
	public static final String	TREE_INFO_CAN_NOT_BE_GIVEN_FOR_THIS_TYPE_OF_NEST	= "TREE_INFO_CAN_NOT_BE_GIVEN_FOR_THIS_TYPE_OF_NEST";
	public static final String	TREE_TOO_HIGH_FOR_THIS_TYPE_OF_NEST	= "TREE_TOO_HIGH_FOR_THIS_TYPE_OF_NEST";
	public static final String	PONTTOALUE_PONTTO_MUST_BOTH_BE_GIVEN_OR_NEITHER	= "PONTTOALUE_PONTTO_MUST_BOTH_BE_GIVEN_OR_NEITHER";
	public static final String	PONTTOALUE_KUNTA_DOES_NOT_MATCH_WITH_PESA_KUNTA	= "PONTTOALUE_KUNTA_DOES_NOT_MATCH_WITH_PESA_KUNTA";
	public static final String	MUNINNAN_TIETOJA_ANNETTU_VAIKKEI_MUNIA_RAPORTOITU	= "MUNINNAN_TIETOJA_ANNETTU_VAIKKEI_MUNIA_RAPORTOITU";
	public static final String	BIOTOPE_AREA_CAN_T_BE_GIVEN_IF_BIOTOPE_NOT_GIVEN	= "BIOTOPE_AREA_CAN_T_BE_GIVEN_IF_BIOTOPE_NOT_GIVEN";
	public static final String	COLONY_INFO_CAN_NOT_BE_GIVEN_IF_NOT_REPORTED_TO_BE_IN_COLONY	= "COLONY_INFO_CAN_NOT_BE_GIVEN_IF_NOT_REPORTED_TO_BE_IN_COLONY";
	public static final String	COUNTS	= "COUNTS";
	public static final String	TUNTURI_BIOTOOPPI_LIIAN_ETELASSA	= "TUNTURI_BIOTOOPPI_LIIAN_ETELASSA";
	public static final String	INVALID_KAYNTI_ORDER	= "INVALID_KAYNTI_ORDER";

	public static void removeTarkastusValuesButKeepSpecific(_Table tarkastus) {
		Map<String, String> values = specificValuesToMap(tarkastus);
		tarkastus.clearAllValues();
		restoreSpecificValues(tarkastus, values);
	}

	private static void restoreSpecificValues(_Table tarkastus, Map<String, String> values) {
		for (String f : TARKASTUS_FIELDS_TO_BE_PREFILLED_FOR_INSERTS) {
			tarkastus.get(f).setValue(values.get(f));
		}
	}

	private static Map<String, String> specificValuesToMap(_Table tarkastus) {
		Map<String, String> values = new HashMap<>();
		for (String f : TARKASTUS_FIELDS_TO_BE_PREFILLED_FOR_INSERTS) {
			values.put(f, tarkastus.get(f).getValue());
		}
		return values;
	}

	// public static boolean firstPesimistulosBetterThanSecond(String thisPesimistulos, String bestPesimistulos) {
	// if (bestPesimistulos.equals("")) return true;
	// if (thisPesimistulos.equals("")) return false;
	// if (!PESIMISTULOS_IN_RISING_SUCCESS_ORDER.contains(thisPesimistulos)) throw new IllegalStateException("Pesimistulos code: '"+thisPesimistulos+"' not found");
	// if (!PESIMISTULOS_IN_RISING_SUCCESS_ORDER.contains(bestPesimistulos)) throw new IllegalStateException("Pesimistulos code: '"+bestPesimistulos+"' not found");
	// int thisIndex = 0;
	// int bestIndex = 0;
	// for (String code : PESIMISTULOS_IN_RISING_SUCCESS_ORDER) {
	// if (code.equals(thisPesimistulos)) break;
	// thisIndex++;
	// }
	// for (String code : PESIMISTULOS_IN_RISING_SUCCESS_ORDER) {
	// if (code.equals(bestPesimistulos)) break;
	// bestIndex++;
	// }
	// return thisIndex > bestIndex;
	// }

	public static final PesasijaintiValidatorSpec BUILDING_AND_UNKNOWNS = initUnknown();

	private static PesasijaintiValidatorSpec initUnknown() {
		PesasijaintiValidatorSpec spec = new PesasijaintiValidatorSpec();
		spec.allowedNestHeightRange = new NumericRange<>(0.0, 250.0);
		spec.noWarningsNestHeightRange = new NumericRange<>(0.0, 40.0);
		return spec;
	}

	public static final PesasijaintiValidatorSpec GROUNDLEVEL = initGroundlevel();

	private static PesasijaintiValidatorSpec initGroundlevel() {
		PesasijaintiValidatorSpec spec = new PesasijaintiValidatorSpec();
		spec.allowedNestHeightRange = new NumericRange<>(0.0, 1.5);
		spec.noWarningsNestHeightRange = new NumericRange<>(0.0, 0.5);
		return spec;
	}

	public static final PesasijaintiValidatorSpec HILLTOP = initHilltop();

	private static PesasijaintiValidatorSpec initHilltop() {
		PesasijaintiValidatorSpec spec = new PesasijaintiValidatorSpec();
		spec.allowedNestHeightRange = new NumericRange<>(0.0, 200.0);
		spec.noWarningsNestHeightRange = new NumericRange<>(0.0, 30.0);
		return spec;
	}

	public static final PesasijaintiValidatorSpec CLOSE_TO_GROUND = initCloseToGround();

	private static PesasijaintiValidatorSpec initCloseToGround() {
		PesasijaintiValidatorSpec spec = new PesasijaintiValidatorSpec();
		spec.allowedNestHeightRange = new NumericRange<>(0.0, 2.0);
		return spec;
	}

	public static final PesasijaintiValidatorSpec FELLED_TREE = initFelledTree();

	private static PesasijaintiValidatorSpec initFelledTree() {
		PesasijaintiValidatorSpec spec = new PesasijaintiValidatorSpec();
		spec.noWarningsNestHeightRange = new NumericRange<>(0.0, 6.0);
		spec.inTree = true;
		return spec;
	}

	public static final PesasijaintiValidatorSpec SOMEWHAT_ABOVE_GROUND = initSomewhatAboveGround();

	private static PesasijaintiValidatorSpec initSomewhatAboveGround() {
		PesasijaintiValidatorSpec spec = new PesasijaintiValidatorSpec();
		spec.allowedNestHeightRange = new NumericRange<>(0.0, 3.0);
		spec.allowedNestDistanceFromTreeRange = null;
		spec.allowedTreeHeightRange = null;
		return spec;
	}

	public static final PesasijaintiValidatorSpec SMALL_TREE_ETC = initSmalltree();

	private static PesasijaintiValidatorSpec initSmalltree() {
		PesasijaintiValidatorSpec spec = new PesasijaintiValidatorSpec();
		spec.allowedNestHeightRange = new NumericRange<>(0.0, 5.0);
		spec.allowedNestDistanceFromTreeRange = new NumericRange<>(0.0, 2.0);
		spec.allowedTreeHeightRange = new NumericRange<>(0, 5);
		spec.noWarningsNestDistanceFromTreeRange = new NumericRange<>(0.0, 1.0);
		spec.inTree = true;
		return spec;
	}

	public static final PesasijaintiValidatorSpec JUNIPER = initJuniper();

	private static PesasijaintiValidatorSpec initJuniper() {
		PesasijaintiValidatorSpec spec = new PesasijaintiValidatorSpec();
		spec.allowedNestHeightRange = new NumericRange<>(0.0, 6.0);
		spec.allowedNestDistanceFromTreeRange = new NumericRange<>(0.0, 1.0);
		spec.allowedTreeHeightRange = new NumericRange<>(0, 11);
		spec.noWarningsTreeHeightRange = new NumericRange<>(0, 5);
		spec.inTree = true;
		return spec;
	}

	public static final PesasijaintiValidatorSpec TREE = initTree();

	private static PesasijaintiValidatorSpec initTree() {
		PesasijaintiValidatorSpec spec = new PesasijaintiValidatorSpec();
		spec.allowedNestHeightRange = new NumericRange<>(0.0, 45.0);
		spec.allowedNestDistanceFromTreeRange = new NumericRange<>(0.0, 6.0);
		spec.allowedTreeHeightRange = new NumericRange<>(0, 45);
		spec.noWarningsNestHeightRange = new NumericRange<>(0.0, 35.0);
		spec.noWarningsNestDistanceFromTreeRange = new NumericRange<>(0.0, 4.0);
		spec.noWarningsTreeHeightRange = new NumericRange<>(0, 35);
		spec.inTree = true;
		return spec;
	}

	public static final Map<String, PesasijaintiValidatorSpec> PESAN_SIJAINTI_SPECS = initPesansijaintiSpecs();


	private static HashMap<String, PesasijaintiValidatorSpec> initPesansijaintiSpecs() {
		HashMap<String, PesasijaintiValidatorSpec> map = new HashMap<>();
		map.put(null, BUILDING_AND_UNKNOWNS);
		map.put("",   BUILDING_AND_UNKNOWNS);
		map.put("0",  BUILDING_AND_UNKNOWNS);
		map.put("1",  CLOSE_TO_GROUND);
		map.put("2",  CLOSE_TO_GROUND);
		map.put("3",  CLOSE_TO_GROUND);
		map.put("4",  CLOSE_TO_GROUND);
		map.put("5",  SMALL_TREE_ETC);
		map.put("6",  SMALL_TREE_ETC);
		map.put("7",  SMALL_TREE_ETC);
		map.put("8",  SMALL_TREE_ETC);
		map.put("9",  JUNIPER);
		map.put("10", FELLED_TREE);
		map.put("11", SOMEWHAT_ABOVE_GROUND);
		map.put("12", SOMEWHAT_ABOVE_GROUND);
		map.put("13", TREE);
		map.put("14", TREE);
		map.put("15", TREE);
		map.put("16", TREE);
		map.put("17", TREE);
		map.put("18", TREE);
		map.put("19", GROUNDLEVEL);
		map.put("20", HILLTOP);
		map.put("21", BUILDING_AND_UNKNOWNS);
		map.put("22", BUILDING_AND_UNKNOWNS);
		map.put("23", BUILDING_AND_UNKNOWNS);
		map.put("24", CLOSE_TO_GROUND);
		map.put("25", CLOSE_TO_GROUND);
		map.put("26", TREE);
		map.put("27", CLOSE_TO_GROUND);
		return map;
	}

	public static final Collection<String> SAA_OLLA_ELAVIA_MUNIA = Utils.collection("M1", "M2", "M3", "M4", "M5", "M6", "M7", "P1", "P2", "P3", "P4");
	public static final Collection<String> SAA_OLLA_KUOLLEITA_MUNIA = Utils.collection("M1", "M2", "M3", "M4", "M5", "M6", "M7", "M8", "P1", "P2", "P3", "P4", "P5", "P6", "P7", "P8", "P9", "T1", "T2", "J1", "J2");
	public static final Collection<String> SAA_OLLA_ELAVIA_POIKASIA = Utils.collection("P1", "P2", "P3", "P4", "P5", "P6", "P7", "P8", "P9", "J1");
	public static final Collection<String> SAA_OLLA_KUOLLEITA_POIKASIA = Utils.collection("P1", "P2", "P3", "P4", "P5", "P6", "P7", "P8", "P9", "T2", "J1", "J2");
	public static final Collection<String> EI_SAA_OLLA_AIKUISIA = Utils.collection("R9", "R0");

	public static final Map<String, Collection<String>> SALLITUT_PESIMISTULOS_VIIMEINEN_PESINNAN_VAIHE_KOMBINAATIOT = initSallitutPesimistulosViimeinenPesinnanVaiheKombinaatiot();

	private static Map<String, Collection<String>> initSallitutPesimistulosViimeinenPesinnanVaiheKombinaatiot() {
		Map<String, Collection<String>> map = new HashMap<>();
		map.put("3", Utils.collection("P6"));
		map.put("4", Utils.collection("P7"));
		map.put("5", Utils.collection("P6"));
		map.put("6", Utils.collection("P8", "P9", "J1"));
		map.put("7", Utils.collection("R5", "E1", "E2"));
		map.put("8", Utils.collection("J0"));
		map.put("9", Utils.collection("J0"));
		map.put("10", Utils.collection("J0"));
		return map;
	}

	public static final Map<String, Collection<String>> SALLITUT_EPAONNISTUMINEN_VIIMEINEN_PESINNAN_VAIHE_KOMBINAATIOT = initSallitutEpaonnistuminenViimeinenPesinnanVaiheKombinaatiot();
	public static final String	PONTON_TIETOJA_ANNETTU_MUTTA_PESA_SIJ_EI_PONTTO	= "PONTON_TIETOJA_ANNETTU_MUTTA_PESA_SIJ_EI_PONTTO";
	public static final String	MAX_PESYEKOKO_YLITTYNYT	= "MAX_PESYEKOKO_YLITTYNYT";
	public static final String	PONTON_RIPUSTUS_EI_VOI_OLLA_ENNEN_TARK_VUOTTA	= "PONTON_RIPUSTUS_EI_VOI_OLLA_ENNEN_TARK_VUOTTA";
	public static final String	EPATAVALLINEN_POIKASTEN_MUNIEN_LKM_VAIHTELU	= "EPATAVALLINEN_POIKASTEN_MUNIEN_LKM_VAIHTELU";
	public static final String	PESINTA_ETENEE_VAARAAN_SUUNTAAN	= "PESINTA_ETENEE_VAARAAN_SUUNTAAN";
	public static final String	EI_RAJA_ARVOA	= "EI_RAJA_ARVOA";
	public static final String	MAX_PESINTANOPEUS_YLITTYNYT	= "MAX_PESINTANOPEUS_YLITTYNYT";
	public static final String	AINAKIN_YKSI_KAYNTI_ILMOITETTAVA	= "AINAKIN_YKSI_KAYNTI_ILMOITETTAVA";
	public static final String	OLETKO_VARMA_ETTEI_KYSE_OLE_YHDYSKUNTAILMOITUKSESTA	= "OLETKO_VARMA_ETTEI_KYSE_OLE_YHDYSKUNTAILMOITUKSESTA";
	public static final String	JOKO_PESINNAN_VAIHE_TAI_MUNAN_TAI_POIKASEN_IKA_ILMOITETTAVA	= "JOKO_PESINNAN_VAIHE_TAI_MUNAN_TAI_POIKASEN_IKA_ILMOITETTAVA";
	public static final String	EI_TARKOITUS_ILMOITTAA_PETOLINTUJA	= "EI_TARKOITUS_ILMOITTAA_PETOLINTUJA";

	private static Map<String, Collection<String>> initSallitutEpaonnistuminenViimeinenPesinnanVaiheKombinaatiot() {
		Map<String, Collection<String>> map = new HashMap<>();
		map.put("12", Utils.collection("R9", "R0", "R1", "R2", "R3", "R4", "R5", "E1", "E2"));
		map.put("13", Utils.collection("R9", "R0", "R1", "R2", "R3", "R4", "R5", "E1", "E2", "T0"));
		map.put("14", Utils.collection("T0", "T1", "T2"));
		map.put("26", Utils.collection("T0", "T1", "T2"));
		map.put("15", Utils.collection("T0", "T1", "J2"));
		map.put("16", Utils.collection("T0", "T1", "T2"));
		map.put("17", Utils.collection(""));
		map.put("18", Utils.collection(""));
		map.put("19", Utils.collection(""));
		map.put("20", Utils.collection(""));
		map.put("21", Utils.collection(""));
		map.put("22", Utils.collection(""));
		map.put("23", Utils.collection(""));
		map.put("24", Utils.collection(""));
		map.put("25", Utils.collection(""));
		return map;
	}

	public static void kirjekyyhkyModelToData(Map<String, String> datamap,  _PesaDomainModelRow kirjekyyhkyparameters) {
		String leveys = datamap.get(Const.UPDATEPARAMETERS + ".pesa.leveys");
		String pituus = datamap.get(Const.UPDATEPARAMETERS + ".pesa.pituus");

		String koord_tyyppi = kirjekyyhkyparameters.getPesa().get("koord_ilm_tyyppi").getValue();
		_Column levCol = null;
		_Column pitCol = null;
		if (koord_tyyppi.equals("Y")) {
			levCol = kirjekyyhkyparameters.getPesa().get("yht_leveys");
			pitCol = kirjekyyhkyparameters.getPesa().get("yht_pituus");
		} else if (koord_tyyppi.equals("E")) {
			levCol = kirjekyyhkyparameters.getPesa().get("eur_leveys");
			pitCol = kirjekyyhkyparameters.getPesa().get("eur_pituus");
		} if (levCol != null && pitCol != null) {
			convertCoordinates(kirjekyyhkyparameters, leveys, pituus, koord_tyyppi);
			levCol.setValue(leveys);
			pitCol.setValue(pituus);
		}

		boolean poikasvaihe = false;
		for (_Table kaynti : kirjekyyhkyparameters.getKaynnit()) {
			_Column lkm_poikaset = kaynti.get("lkm_poikaset");
			_Column vaihe = kaynti.get("pesinnan_vaihe");

			if (vaihe.getValue().startsWith("P")) {
				poikasvaihe = true;
				break;
			}
			if (lkm_poikaset.hasValue()) {
				poikasvaihe = true;
				break;
			}
		}

		for (Map.Entry<String, String> e : datamap.entrySet()) {
			String key = e.getKey();
			if (key.startsWith(Const.UPDATEPARAMETERS+".kaynti.") && key.endsWith(".ika")) {
				String value = e.getValue();
				if (value.length() < 1) continue;
				int i = Integer.valueOf(key.split("\\.")[2]);

				if (poikasvaihe) {
					kirjekyyhkyparameters.getColumn("kaynti."+i+".ika_poikaset").setValue(value);
				} else {
					kirjekyyhkyparameters.getColumn("kaynti."+i+".ika_munat").setValue(value);
				}
			}
		}
	}

	private static void convertCoordinates(_PesaDomainModelRow kirjekyyhkyparameters, String leveys, String pituus, String koord_tyyppi) {
		try {
			CoordinateConverter converter = new CoordinateConverter();
			if (koord_tyyppi.equals("Y")) {
				converter.setYht_leveys(leveys);
				converter.setYht_pituus(pituus);
			} else if (koord_tyyppi.equals("E")) {
				converter.setEur_leveys(leveys);
				converter.setEur_pituus(pituus);
			}
			converter.convert();
			kirjekyyhkyparameters.getPesa().get("yht_leveys").setValue(converter.getYht_leveys().toString());
			kirjekyyhkyparameters.getPesa().get("yht_pituus").setValue(converter.getYht_pituus().toString());
			kirjekyyhkyparameters.getPesa().get("eur_leveys").setValue(converter.getEur_leveys().toString());
			kirjekyyhkyparameters.getPesa().get("eur_pituus").setValue(converter.getEur_pituus().toString());
		} catch (Exception e) {
			// Something wrong with coordinates... we do nothing, validation will pick this up later.
		}
	}
}
