package fi.hy.pese.framework.main.app;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fi.hy.pese.framework.main.general.TransactionPoller;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data.ParameterMap;
import fi.hy.pese.framework.main.general.data._Row;
import fi.luomus.commons.config.Config;
import fi.luomus.commons.http.HttpStatus;
import fi.luomus.commons.session.SessionHandler;
import fi.luomus.commons.session.SessionHandlerImple;
import fi.luomus.commons.utils.LogUtils;
import fi.luomus.commons.utils.Utils;

/**
 * The abstract base servlet for Pese Framework application.
 * The applications need to implement the factory() -method. The factory contains all the
 * application specific configuration and data templates, as defined in the _Factory interface.
 * The applications may also override other features of this class, or define their very own Servlet from scratch.
 * @see _ApplicationSpecificationFactory
 */
public abstract class PeSeServlet<T extends _Row> extends HttpServlet {

	private static final int	SESSION_TIMEOUT					= 3 * 60 * 60;
	private static final long	serialVersionUID				= -2696093641528425611L;
	private static final int	DEFAULT_SIZE_FOR_PARAMETER_MAP	= 50;

	protected abstract _ApplicationSpecificationFactory<T> createFactory() throws Exception;

	private Control<T>								control;
	private _ApplicationSpecificationFactory<T>		application;
	private TransactionPoller<T> 					transactionPoller;
	private Thread 									pollerThread;

	/**
	 * Called when the servlet is initialized; either at server startup or when the servlet is called for the first time,
	 * depending on server configuration.
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		try {
			application = createFactory();
			control = new Control<>(application);
			application.loadHistoryActions();
			if (application.isWarehouseSource()) {
				startTransactionPoller();
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new ServletException("CAUSE: " + e.toString() + " " + e.getStackTrace()[0]);
		}
		System.out.println(this.getClass() + " INITIALIZED USING FACTORY: " + application.getClass().toString());
	}

	private void startTransactionPoller() {
		transactionPoller = new TransactionPoller<>(application);
		pollerThread = new Thread(transactionPoller);
		pollerThread.start();
	}

	/**
	 * Called at server shutdown (etc)
	 */
	@Override
	public void destroy() {
		if (transactionPoller != null) {
			try {
				transactionPoller.stop();
				pollerThread.interrupt();
			} catch (Exception e) {
				application.getErrorReporter().report(e);
			}
		}
		try {
			application.saveHistoryActions();
		} catch (Exception e) {
			application.getErrorReporter().report(e);
		}

		super.destroy();
		System.out.println(this.getClass() + " CLOSED DOWN ");
	}

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		process(req, res);
	}

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		process(req, res);
	}

	/**
	 * Processes a request. Builds parameters, wraps session, and response and gives them to Control for execution
	 * The default functionality replaces some characters with their HTML entities. It also writes the non-empty parameters
	 * to log, if so defined in the application configuration.
	 * @see Control
	 * @see Config
	 * @param req request
	 * @param res response
	 * @throws IOException
	 */
	@SuppressWarnings("rawtypes")
	public void process(HttpServletRequest req, HttpServletResponse res) throws IOException {
		req.setCharacterEncoding("UTF-8");
		res.setContentType("text/html; charset=utf-8");
		PrintWriter out = res.getWriter();
		HttpSession ses = req.getSession();
		ses.setMaxInactiveInterval(SESSION_TIMEOUT);

		SessionHandler sessionHandler = new SessionHandlerImple(ses, application.config().systemId());
		HashMap<String, String> params = new HashMap<>(DEFAULT_SIZE_FOR_PARAMETER_MAP);
		StringBuilder logText = new StringBuilder();

		Enumeration parameterNames = req.getParameterNames();
		while (parameterNames.hasMoreElements()) {
			String key = (String) parameterNames.nextElement();
			String[] values = req.getParameterValues(key);
			String value = "";
			for (String v : values) {
				if (v.length() > 0) value += v.trim() + "|";
			}
			value = Utils.removeLastChar(value);
			if (value.length() > 0) {
				params.put(key, value);
				addParameterForLogging(logText, key, value);
			}
		}

		writeParametersToLog(sessionHandler.userId(), logText);

		String requestURI = Utils.getRequestURIAndQueryString(req);

		control.process(new ParameterMap(params), sessionHandler, out, new HttpStatus(res), requestURI);
	}

	// Implementing servlet may override
	protected void addParameterForLogging(StringBuilder parametersForLog, String key, String value) {
		if (Const.LOGIN_PASSWORD.equals(key)) value = "-password-";
		parametersForLog.append("\"").append(key).append("\"=\"").append(removeLinebreaks(value)).append("\" ");
	}

	private String removeLinebreaks(String value) {
		value = value.replace("\r", " ");
		value = value.replace("\n", " ");
		return value;
	}

	// Implementing servlet may override
	protected void writeParametersToLog(String userid, StringBuilder parametersForLog) {
		if (application.config().parameterLogging()) {
			String filePrefix = "";
			try {
				filePrefix = application.config().get(Config.PARAMETER_LOG_FILE_PREFIX);
			} catch (Exception e) {
			}
			if (userid == null || userid.length() < 1)
				LogUtils.write(parametersForLog.toString(), application.config().logFolder(), filePrefix);
			else
				LogUtils.write(parametersForLog.toString(), userid, application.config().logFolder(), filePrefix);
			if (application.config().developmentMode()) {
				System.out.println(parametersForLog.toString());
			}
		}
	}

}
