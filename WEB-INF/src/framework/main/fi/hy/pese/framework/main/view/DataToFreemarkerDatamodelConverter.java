package fi.hy.pese.framework.main.view;

import java.util.HashMap;
import java.util.Map;

import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data._Data;
import fi.hy.pese.framework.main.general.data._Row;
import fi.luomus.commons.config.Config;
import fi.luomus.commons.utils.DateUtils;

public class DataToFreemarkerDatamodelConverter {

	public static Map<Object, Object> convert(_Request<? extends _Row> request) {
		Map<Object, Object> datamodel = convert(request.data());
		datamodel.put(Const.HISTORY_ACTIONS, request.getHistoryActions());
		return datamodel;
	}

	public static Map<Object, Object> convert(_Data<? extends _Row> data) {
		Map<Object, Object> root = new HashMap<>();

		// url, page, action, success, failure, notice, language
		Map<Object, Object> system = new HashMap<>();
		system.put(Const.BASE_URL, data.baseURL());
		system.put(Const.COMMON_URL, data.commonURL());
		system.put(Const.PAGE, data.page());
		system.put(Const.ACTION, data.action());
		system.put(Const.SUCCESS, data.successText());
		system.put(Const.FAILURE, data.failureText());
		system.put(Const.NOTICES, data.noticeTexts());
		system.put(Const.LANGUAGE, data.language());
		system.put(Const.USER_ID, data.userId());
		system.put(Const.USER_NAME, data.userName());
		system.put(Const.USER_TYPE, data.userType());
		system.put(Const.CURRENT_YEAR, data.currentYear());
		system.put(Const.TIMESTAMP, Long.toString(DateUtils.getCurrentEpoch()));
		system.put(Const.LAST_UPDATE_TIMESTAMP, data.lastUpdateTimestamp());
		system.put(Config.DEVELOPMENT_MODE, data.developmentMode());
		system.put(Config.STAGING_MODE, data.stagingMode());
		root.put(Const.SYSTEM, system);

		// Misc texts
		root.put(Const.DATA, data.getAll()); // TODO ei tarpeen? Voisi kutsua dataa sellaisenaan...

		// Misc lists
		root.put(Const.LISTS, data.getLists());

		root.put(Const.SEARCHPARAMETERS, data.searchparameters());
		root.put(Const.UPDATEPARAMETERS, data.updateparameters());
		root.put(Const.COMPARISONPARAMETERS, data.comparisonparameters());
		root.put(Const.RESULTS, data.results());

		// Selections
		root.put(Const.SELECTION_OPTIONS, data.selections());

		// Errortexts
		if (data.errors() != null) {
			root.put(Const.ERRORS, data.errors());
		} else {
			root.put(Const.ERRORS, new HashMap<>());
		}

		// Warnings
		if (data.warnings() != null) {
			root.put(Const.WARNINGS, data.warnings());
		} else {
			root.put(Const.WARNINGS, new HashMap<>());
		}

		// Produced files
		root.put(Const.FILES, data.producedFiles());

		// UItexts
		root.put(Const.TEXTS, data.uiTexts());

		root.put(Const.XML_OUTPUT, data.getXMLOutput());

		return root;
	}

}
