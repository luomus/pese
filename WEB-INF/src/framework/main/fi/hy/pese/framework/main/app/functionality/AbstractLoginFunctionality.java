package fi.hy.pese.framework.main.app.functionality;

import fi.hy.pese.framework.main.app.FunctionalityExecutor;
import fi.hy.pese.framework.main.app._Functionality;
import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.app.functionality.validate._Validator;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data._Row;

public abstract class AbstractLoginFunctionality<T extends _Row> extends BaseFunctionality<T> {

	public AbstractLoginFunctionality(_Request<T> request) {
		super(request);
	}

	@Override
	public abstract _Validator validator();

	@Override
	public void afterSuccessfulValidation() throws Exception {
		if (data.action().equals(Const.CHECK_LOGIN)) {
			String userid = data.get(Const.LOGIN_USERNAME).toLowerCase();
			request.session().authenticateFor(request.config().systemId());
			request.session().setUserId(userid);
			request.session().setUserName(userid);
			data.setUserId(userid);
			data.setUserName(userid);
			data.setPage(request.frontpage());
			data.setAction("");
			_Functionality<T> frontpageFunctionality = request.functionalityFor(request.frontpage());
			FunctionalityExecutor.execute(frontpageFunctionality);
		}
	}

	@Override
	public void afterFailedValidation() throws Exception {
		request.data().setPage(Const.LOGIN_PAGE);
	}

}
