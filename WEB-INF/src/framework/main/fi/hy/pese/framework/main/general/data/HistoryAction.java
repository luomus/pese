package fi.hy.pese.framework.main.general.data;

import java.io.Serializable;

import fi.luomus.commons.utils.DateUtils;

public class HistoryAction implements Serializable {

	private static final long	serialVersionUID	= 4470893736034267550L;
	private final String date;
	private final _Row row;
	private final String action;
	private final String user;
	private final String id;

	public HistoryAction(String id, _Row row, String action, String user) {
		this.row = row;
		this.action = action;
		this.date = DateUtils.getCurrentDateTime("dd.MM.yyyy HH:mm:ss");
		this.user = user;
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public _Row getRow() {
		return row;
	}

	public String getAction() {
		return action;
	}

	public String getDate() {
		return date;
	}

	public String getUser() {
		return user;
	}

}
