package fi.hy.pese.saaksi.main.app;

import java.io.File;
import java.util.Map;

import fi.hy.pese.framework.main.app.BaseFactoryForApplicationSpecificationFactory;
import fi.hy.pese.framework.main.app._Functionality;
import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.app.functionality.CoordinateRadiusSearcherFunctionality;
import fi.hy.pese.framework.main.app.functionality.FilemanagementFunctionality;
import fi.hy.pese.framework.main.app.functionality.FilemanagementFunctionality.FileListGeneratorImple;
import fi.hy.pese.framework.main.app.functionality.LintuvaaraLoginFunctionality;
import fi.hy.pese.framework.main.app.functionality.LoginFunctionalityWithSingleUser;
import fi.hy.pese.framework.main.app.functionality.LogoutFunctionality;
import fi.hy.pese.framework.main.app.functionality.PesintamanagementFunctionality;
import fi.hy.pese.framework.main.app.functionality.TablemanagementFunctionality;
import fi.hy.pese.framework.main.db._DAO;
import fi.hy.pese.framework.main.general.WarehouseRowGenerator;
import fi.hy.pese.framework.main.general._WarehouseRowGenerator;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data.ParameterMap;
import fi.hy.pese.framework.main.general.data._Data;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.main.kirjekyyhky._KirjekyyhkyXMLGenerator;
import fi.hy.pese.saaksi.main.functionality.SaaksiKirjekyyhkyManagementFunctionality;
import fi.hy.pese.saaksi.main.functionality.SaaksiKirjekyyhkyXMLGenerator;
import fi.hy.pese.saaksi.main.functionality.SaaksiSearchFunctionality;
import fi.hy.pese.saaksi.main.functionality.SaaksiTarkastusFunctionality;
import fi.luomus.commons.config.Config;
import fi.luomus.commons.containers.Selection;
import fi.luomus.commons.containers.SelectionImple;
import fi.luomus.commons.containers.SelectionOptionImple;
import fi.luomus.commons.tipuapi.TipuAPIClient;
import fi.luomus.commons.utils.AESDecryptor;

public class SaaksiFactory extends BaseFactoryForApplicationSpecificationFactory<_PesaDomainModelRow> {

	public SaaksiFactory(String configFileName) throws Exception {
		super(configFileName);
	}

	@Override
	public _Data<_PesaDomainModelRow> initData(String language, ParameterMap params) throws Exception {
		_Data<_PesaDomainModelRow> data = super.initData(language, params);
		data.set(Const.LINTUVAARA_URL, config().get(Config.LINTUVAARA_URL));
		return data;
	}

	@Override
	public String frontpage() {
		return Const.SEARCH_PAGE; // Warning: frontpage must be defined bellow (functionalityFor()) or an infinite loop will happen
	}

	private AESDecryptor	decryptor	= null;

	private AESDecryptor getAESDecryptor() {
		if (decryptor == null) {
			try {
				decryptor = new AESDecryptor(config().get(Config.LINTUVAARA_PUBLIC_RSA_KEY));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return decryptor;
	}

	@Override
	public _Functionality<_PesaDomainModelRow> functionalityFor(String page, _Request<_PesaDomainModelRow> request) {
		if (page.equals(Const.LOGIN_PAGE)) {
			if (config().developmentMode()) {
				return new LoginFunctionalityWithSingleUser<>(request, "z", "z");
			}
			return new LintuvaaraLoginFunctionality<>(request, getAESDecryptor(), getErrorReporter());
		} else if (page.equals(Const.TABLEMANAGEMENT_PAGE))
			return new TablemanagementFunctionality<>(request);
		else if (page.equals(Const.SEARCH_PAGE))
			return new SaaksiSearchFunctionality(request);
		else if (page.equals(Const.TARKASTUS_PAGE))
			return new SaaksiTarkastusFunctionality(request);
		else if (page.equals(Const.REPORTS_PAGE)) return new SaaksiSearchFunctionality(request);
		else if (page.equals(Const.FILEMANAGEMENT_PAGE)) {
			return new FilemanagementFunctionality<>(request, new FileListGeneratorImple(request.config().pdfFolder(), request.config().reportFolder()));
		} else if (page.equals(Const.COORDINATE_RADIUS_SEARCH_PAGE)) {
			return new CoordinateRadiusSearcherFunctionality(request, new SaaksiSearchFunctionality(request));
		} else if (page.equals(Const.KIRJEKYYHKY_MANAGEMENT_PAGE)) {
			return new SaaksiKirjekyyhkyManagementFunctionality(request);
		}
		else if (page.equals(Const.PESINTAMANAGEMENT_PAGE))
			return new PesintamanagementFunctionality(request);
		else if (page.equals(Const.LOGOUT_PAGE))
			return new LogoutFunctionality<>(request);
		else if (page.equals("luo")) {
			try {
				SaaksiKirjekyyhkyXMLGenerator generator = new SaaksiKirjekyyhkyXMLGenerator(request);
				@SuppressWarnings("unused")
				File file = generator.writeStructureXML();
				//						KirjekyyhkyAPIClient api = null;
				//						try {
				//							api = this.kirjekyyhkyAPI();
				//							api.sendFormStructure(file, null);
				//						} finally {
				//							if (api != null) api.close();
				//						}
			} catch (Exception e) {
				e.printStackTrace();
			}
			request.data().setPage(Const.FILEMANAGEMENT_PAGE);
			return functionalityFor(Const.FILEMANAGEMENT_PAGE, request);
		}
		else {
			request.data().setPage(frontpage());
			return functionalityFor(frontpage(), request);
		}
	}

	@Override
	public Map<String, Selection> fetchSelections(_DAO<_PesaDomainModelRow> dao) throws Exception {
		Map<String, Selection> selections = dao.returnCodeSelections("aputiedot", "taulu", "kentta", "arvo", "selite_suomi", "ryhmittely", new String[] {"jarjestys"});
		selections.put(SaaksiConst.TAULUT, dao.returnSelections("aputiedot", "taulu", "taulu", new String[] {"taulu"}));
		selections.put(SaaksiConst.REVIIRIT, dao.returnSelections("reviiri", "reviiriid", "reviirinimi", new String[] {"reviirinimi"}));

		TipuAPIClient tipuAPI = null;
		try {
			tipuAPI = this.tipuAPI();
			selections.put(TipuAPIClient.RINGERS, tipuAPI.getAsSelection(TipuAPIClient.RINGERS, 2));
			selections.put(TipuAPIClient.MUNICIPALITIES, tipuAPI.getAsSelection(TipuAPIClient.MUNICIPALITIES, 1));
			selections.put(TipuAPIClient.SPECIES, tipuAPI.getAsSelection(TipuAPIClient.SPECIES, 1));
			selections.put(TipuAPIClient.ELY_CENTRES, tipuAPI.getAsSelection(TipuAPIClient.ELY_CENTRES, 1));
		} finally {
			if (tipuAPI != null) tipuAPI.close();
		}

		SelectionImple k_e = new SelectionImple(SaaksiConst.K_E); // TODO should come from lang file
		k_e.addOption(new SelectionOptionImple("K", "Kyllä"));
		k_e.addOption(new SelectionOptionImple("E", "Ei"));
		selections.put(SaaksiConst.K_E, k_e);

		return selections;
	}

	@Override
	public _KirjekyyhkyXMLGenerator kirjekyyhkyFormDataXMLGenerator(_Request<_PesaDomainModelRow> request) throws Exception {
		return new SaaksiKirjekyyhkyXMLGenerator(request);
	}

	@Override
	public _WarehouseRowGenerator getWarehouseRowGenerator(_DAO<_PesaDomainModelRow> dao) {
		return new WarehouseRowGenerator(SaaksiWarehouseDefinition.instance(), dao, this);
	}

}
