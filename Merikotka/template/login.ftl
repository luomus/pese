<#include "macro.ftl">

<!DOCTYPE html 
  PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="${system.language}">

<head> 
	<title>${texts["title_"+system.page]}</title>
	<link rel="stylesheet" href="${system.baseURL}/style.css" type="text/css" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head> 

<body>

<#--         
<h1 align="center">${texts.login_halisystem}</h1>

<p align="center"> 
	<img src="Merikotka/img/halalb.jpg" border="3" alt="" />   
</p>
-->

<br /><br />

<div align="center"> <@errorlist errors texts /> </div>

<form action="${system.baseURL}" method="post">
<input type="hidden"   name="action" value="check_login" />

	
	
	<table border="0" align="center">		
		<tr>
			<td colspan="2">
				<#if system.StagingMode>     <span style="color: red; font-weigth: bold; font-size: 200%">TESTIYMPÄRISTÖ</span></#if>
				<#if system.DevelopmentMode> <span style="color: red; font-weigth: bold; font-size: 200%">KEHITYSYMPÄRISTÖ</span></#if>
				<br />
			</td>
		</tr>
		<tr>
			<td>${texts.login_userid}</td>		
			<td><input style="width: 130px;" type="text" name="login_username" value="${data.login_username!""}" <@checkerror errors "login_fields" />   /></td>			
		</tr>
		<tr>
			<td>${texts.login_password}</td>			
			<td><input style="width: 130px;" type="password" name="login_password" value="" <@checkerror errors "login_fields" />   /></td>			
		</tr>
		<tr>
			<td></td>
			<td><input type="submit" value="${texts.submit_login}" /></td>			
		</tr>
	</table>
	
  
</form>

<#--
<table align="center">
	<tr>
		<td>
			<a href="${haliURL}?language=fi">Suomeksi</a>
		</td>
		<td>
			<a href="${haliURL}?language=sv">Svenska</a>
		</td>
		<td>
			<a href="${haliURL}?language=en">English</a>
		</td>
	</tr>
</table>
-->

</body>
</html>
