package fi.hy.pese.framework.main.view;

import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;

public class FreemarkerTemplateViewerExceptionHandler implements TemplateExceptionHandler {

	@Override
	public void handleTemplateException(TemplateException te, freemarker.core.Environment env, java.io.Writer out) throws TemplateException {

		try {
			System.err.println(te.getMessage());
			out.write("LOMAKEVIRHE: " + te.getMessage());
		} catch (Exception e) {
		}
		// catch (Throwable t) { }
	}

}
