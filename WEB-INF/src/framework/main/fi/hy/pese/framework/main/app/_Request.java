package fi.hy.pese.framework.main.app;

import java.io.PrintWriter;
import java.util.List;

import fi.hy.pese.framework.main.db._DAO;
import fi.hy.pese.framework.main.general.data.HistoryAction;
import fi.hy.pese.framework.main.general.data._Data;
import fi.hy.pese.framework.main.general.data._Row;
import fi.hy.pese.framework.main.kirjekyyhky._KirjekyyhkyXMLGenerator;
import fi.luomus.commons.config.Config;
import fi.luomus.commons.http.HttpStatus;
import fi.luomus.commons.kirjekyyhky.KirjekyyhkyAPIClient;
import fi.luomus.commons.pdf.PDFWriter;
import fi.luomus.commons.reporting.ErrorReporter;
import fi.luomus.commons.session.SessionHandler;
import fi.luomus.commons.tipuapi.TipuApiResource;

public interface _Request<T extends _Row> {
	
	/**
	 * The data of this request
	 * @return
	 */
	_Data<T> data();
	
	/**
	 * Session of this request
	 * @return
	 */
	SessionHandler session();
	
	/**
	 * DAO connection for this request. Should not be closed by any functionality using this DAO: Control will close the connection.
	 * Control sets this dao to transaction mode and will be commit it after the call, or rolls back if an exception occurs.
	 * @return
	 */
	_DAO<T> dao();
	
	/**
	 * Frontpage defined for a specific application
	 * @return
	 */
	String frontpage();
	
	/**
	 * Returns a new functionality that is given all the data anda parameters of this request
	 * @param page
	 * @return
	 */
	_Functionality<T> functionalityFor(String page);
	
	/**
	 * Redirecter to redirect this request to another URI
	 * @return
	 */
	HttpStatus redirecter();
	
	/**
	 * Configuration of a specific application
	 * @return
	 */
	Config config();
	
	/**
	 * Returns a new PDFWriter
	 * @return
	 */
	PDFWriter pdfWriter();
	
	/**
	 * Returns a new PDFWriter
	 * @param outputFolder
	 * @return
	 */
	PDFWriter pdfWriter(String outputFolder);
	
	/**
	 * Returns a new Kirjekyyhky Form-data-XML generator for a specific application
	 * @return
	 */
	_KirjekyyhkyXMLGenerator kirjekyyhkyFormDataXMLGenerator() throws Exception;
	
	/**
	 * Returns a new Kirjekyyhky Form-data-XML sender for a specific application
	 * @return
	 * @throws Exception
	 */
	KirjekyyhkyAPIClient kirjekyyhkyAPI() throws Exception;
	
	/**
	 * Printwriter for this request
	 * @return
	 */
	PrintWriter out();
	
	/**
	 * Adds a history action entry
	 * @param action
	 */
	void addHistoryAction(HistoryAction action);
	
	/**
	 * Get a list of actions (should not be altered)
	 * @return
	 */
	List<HistoryAction> getHistoryActions();
	
	/**
	 * Get a TipuApiResource by it's name
	 * @param resourcename
	 * @return
	 * @
	 */
	TipuApiResource getTipuApiResource(String resourcename);
	
	/**
	 * Full uri (  http(s)://.....?and_params=if_any  )
	 * @return
	 */
	String getOriginalRequestURI();
	
	ErrorReporter getErrorReporter();
	
}
