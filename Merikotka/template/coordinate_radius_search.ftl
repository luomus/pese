<!-- lahiympariston pesat --> 

<#assign ROWS_PER_PAGE = 15>

<#if ( errors?size > 0) >
<#-- 
      <tr>
        <td>
          	<#assign errorfields = errors?keys>
          	<#list errorfields as errorfield> <span class="error"> ${texts[errorfield]!errorfield}: ${texts[errors[errorfield]]!errors[errorfield]} </span> <br /> </#list>
        </td>
      </tr>
-->      
<#else>
	  
	<#assign page = 0>
	  
	<#list results as r>
		<#if ((r_index) % ROWS_PER_PAGE) == 0>
	    	<#assign page = page + 1>
	        <table id="lahiymparistonPesat_${page}" class="infoContainer <#if page != 1>hidden</#if>">
	        <caption> Lähiympäristön&nbsp;pesät </caption>
            <tbody>
	    	<tr>
	    		<th>Pesä</th>
	    		<th>Kylä</th>
	    		<th>Reviiri</th>
        		<th>Yht.koord.</th>
        		<th> &nbsp; </th>
	   		</tr>
	    </#if>
	    
	    <#if data.nestid == r.pesa.pesa_id> <tr class="highlightedRow"> <#else> <tr> </#if>
          <td> 
            <#if data.nestid != r.pesa.pesa_id> 
            	<a href="${system.baseURL}?page=tarkastus&amp;action=fill_for_update&amp;updateparameters.pesavakio.pesa_id=${r.pesa.pesa_id}">
            		<#if r.pesa.pesanimi != ""> ${r.pesa.pesanimi} (${r.pesa.pesa_id}) <#else> ${r.pesa.pesa_id} </#if>
            	</a>
            <#else>
            	<#if r.pesa.pesanimi != ""> ${r.pesa.pesanimi} (${r.pesa.pesa_id}) <#else> ${r.pesa.pesa_id} </#if>
            </#if>
          </td>
          <td>${r.pesa.tarkka_sijainti}</td>
          <td> ${ selections["reviirit"][r.vuosi.reviiri_id] }</td>
          <td>${r.pesa.yht_leveys} <br />  ${r.pesa.yht_pituus}</td>
          <td><a target="_karttapaikka" href="https://asiointi.maanmittauslaitos.fi/karttapaikka/?share=customMarker&n=${r.pesa.eur_leveys}&e=${r.pesa.eur_pituus}&title=${r.pesa.pesanimi.getUrlEncodedValue()}(${r.pesa.pesa_id})">Kartalle</a></td>
        </tr>

	     <#if (r_index+1) % ROWS_PER_PAGE == 0 || !r_has_next>
   				<tr>
          			<th colspan="7" style="text-align: left;"> Löytyi yhteensä ${results?size} kappaletta. &nbsp; &nbsp;
            		<#if page != 1> 
              			<a href="javascript:void(0)" onclick="showPrevInfocontainerPage(${page})">&laquo; Edelliset</a> 
            		</#if>
            		<#if r_has_next>
            			<a href="javascript:void(0)" onclick="showNextInfocontainerPage(${page})">Seuraavat &raquo;</a>
            		</#if>
            		</th>
        		</tr>
       		</tbody>
			</table>
		</#if>
		
      </#list>
				

	   
</#if>
