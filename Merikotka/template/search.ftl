<#assign ROW_ID = 0>
<#assign UNIQUE_KEY = 1>
<#assign IMMUTABLE_KEY = 2>
<#assign NORMAL_FIELD = 3>
<#assign DATE_ADDED = 4>
<#assign DATE_MODIFIED = 5>

<#assign  VARCHAR = 1>
<#assign  INTEGER = 2>
<#assign  DECIMAL = 3>
<#assign  DATE = 4>
	
<#include "navi.ftl">
<#include "macro.ftl">

<#if lists.selectedFields??>
  <#assign resultcolumns = lists.selectedFields>
<#else>
  <#assign resultcolumns = [ "vuosi.reviirin_kunta", "reviiri.reviirinimi", "reviiri.reviiri_id", 
                             "pesavakio.pesanimi", "pesavakio.pesa_id", "pesatarkastus.tark_vuosi", 
                             "pesatarkastus.tarkastaja1_id", "pesatarkastus.pesimistulos" ]> 
</#if>

<div class="search">

<table width="100%">
<tr valign="top">
<td class="sameAsBackground" style="vertical-align: top;">
  <h1>${texts.search_title}</h1>
</td>
<td class="sameAsBackground" align="right">

<#-- -------------------------- SAVED SEARCHES ------------------------------------------------------------------------------------------------------ -->
<div class="saved_searches" align="right">

<#-- --------- listing all saved searches --------- -->
<#if lists.savedSearches??>
<table>
  <tr>
    <th> <a href="javascript:void(0);" onclick="document.getElementById('select_search_container').className = ''; this.blur();">
           Tallennetut haut <img src="${system.baseURL}/img/expand.gif" alt="" />
         </a>
    </th>
  </tr>
  <tr id="select_search_container" class="hidden">
    <td>
      <table>
        <tr>
          <th>Käyttäjä</th> <th>Haku</th> <th></th> <th>Viimeksi käytetty</th> <th></th>
        </tr>
        <#list lists.savedSearches as search>
          <tr>
            <td> ${search.userid} </td>
            <td> <b>${search.name}</b> <br /> <span class="savedSearchesDescription">${search.description}</span> </td>
            <td>
                  <form action="${system.baseURL}" method="post">
                  <input type="hidden" name="page"   value="${system.page}" />
                  <input type="hidden" name="action" value="execute_saved_search" />
                  <input type="hidden" name="search_type" value="advancedSearch" />
                  <input type="hidden" name="selectedFields" value="${search.selected_fields}" />
                  <input type="hidden" name="searchValues" value="${search.search_values}" />
                  <input type="hidden" name="id" value="${search.id}" />
                  <input type="submit" value="Hae" /> 
                  </form>
            </td>
            <td> ${search.last_used} </td> 
            <td> 
                  <form action="${system.baseURL}" method="post" name="delete_search_${search.id}">
                  <input type="hidden" name="page"   value="${system.page}" />
                  <input type="hidden" name="action" value="delete_saved_search" />
                  <input type="hidden" name="id" value="${search.id}" />
                  <a href="#" onclick="if (confirm('Haluatko varmasti poistaa tämän haun?')) document.delete_search_${search.id}.submit();">Poista</a>
                  </form>
            </td>
          </tr>
        </#list>
      </table>
      <input type="submit" value="Peruuta" onclick="document.getElementById('select_search_container').className = 'hidden'; return false;" />
    </td>
  </tr>
</table>
</#if>

<#-- --------- form to save a search --------- -->
<#if (results?size > 0)>
<table>
  <tr>
    <th> <a href="javascript:void(0);" onclick="document.getElementById('save_search_form_container').className = ''; this.blur();">
           Tallenna tämä haku <img src="${system.baseURL}/img/expand.gif" alt="" />
         </a>
    </th>
  </tr>
  <tr id="save_search_form_container" class="hidden">
    <td>
      <form action="${system.baseURL}" method="post" name="save_search" >
      <input type="hidden" name="page"   value="${system.page}" />
      <input type="hidden" name="action" value="save_search" />
      <input type="hidden" name="selectedFields" value="<#list resultcolumns as c>${c}<#if c_has_next>,</#if></#list>" />
      <input type="hidden" name="searchValues" value="${data.searchValues}" />
      <table>
        <tr>
          <td>Nimi:</td>
          <td><input type="text" name="searchName" size="40" /></td>
        </tr>
        <tr> 
          <td>Kuvaus:</td>
          <td><input type="text" name="searchDescription" size="80" /></td>
        </tr>
        <tr>
          <td><input type="submit" value="Tallenna" /></td>
          <td><input type="submit" value="Peruuta" onclick="document.getElementById('save_search_form_container').className = 'hidden'; return false;" /></td>
        </tr>
      </table>
      </form>
    </td>
  </tr>
</table>
</#if>
</div>
<#-- -------------------------- --------- ------------------------------------------------------------------------------------------------------ -->
</td>
</tr>
</table>

<@statustexts system texts />
<@errorlist errors texts />

<#if system.action == "search">
  <#if data.row_count == "0"> 
    Haku ei tuottanut tuloksia 
  <#else> 
    ${texts.search_result_pesa_count} ${data.item_count} &nbsp; &nbsp; Tarkastuksia: ${data.row_count} <br /> <br />
  </#if>
</#if>

<#-- --------------------------------------------------------------------------------------------------------------------------------------------- -->
<#-- -------------------------- RESULTS ---------------------------------------------------------------------------------------------------------- -->
<#if (results?size > 0)>
  
  
  <table id="resultsTable" class="tablesorter"> 
    <thead> 
      <tr> 
        <th class="{sorter: false} haavi"> Haavi </th>
        <th class="{sorter: false} "> &nbsp; </th>
        <#-- ---------- result field descriptions ----------------------- -->
          <#list lists.resultColumns as colName> <#assign column = searchparameters.getColumn(colName)>
              <th <#if column.datatype == INTEGER || column.datatype == DECIMAL>class="{sorter: 'digit'}"</#if>  >${texts[colName]!colName}</th>
          </#list>
          
          <th>${texts.search_max_poikaset}</th>
          <#-- ----------------------- show all / show less  link ----------------------- -->  
          <th align="right" class="{sorter: false}"> <a id="showAll" href="javascript:void(0);" onclick=" changeVisibilityOfAll('<#list results as i>${i.pesa.pesa_id},</#list>', '${texts.search_show_all}',  '${texts.search_hide_all}', '${texts.search_show_more}', '${texts.search_show_less}'   ); this.blur(); "> ${texts.search_show_all} </a> </th>
          <th  class="{sorter: false}"></th>
          <th  class="{sorter: false}"></th>
          <#-- -------------------------- ----------------------- ----------------------- -->
  
        <#-- ----------------------- ----------------------- ------------ -->
      </tr>
    </thead>
    
    <tbody>
    
    <#assign prevResult = "">
    <#list results as result>  
      <#assign id = result.pesa.pesa_id><#if id == prevResult> <#assign i = i + 1><tr class="hidden" id="result_row_${id}_${i}"><td>&nbsp;</td><#else> <#assign i = 0><tr> <td class="haavi"> <input type="checkbox" name="nest_entry_${id}" onchange="toNet(${id});" /> </td> </#if>
      <#-- <td> <form action="${system.baseURL}" method="post"><input type="hidden" name="page"   value="tarkastus" /> <input type="hidden" name="action" value="fill_for_update" /> <input type="hidden" name="updateparameters.pesatarkastus.p_tarkastus_id" value="${result.tarkastus.p_tarkastus_id.value}" /> <input type="submit" value="${result.pesa.pesa_id.value} - ${result.tarkastus.tark_vuosi.value}&nbsp;" /> </form> </td> -->
      <td> <a class="button-link" href="${system.baseURL}?page=tarkastus&amp;action=fill_for_update&amp;updateparameters.pesatarkastus.p_tarkastus_id=${result.tarkastus.p_tarkastus_id.value}">${result.pesa.pesa_id.value}</a> </td> 
      <#list lists.resultColumns as colName><#assign column = result.getColumn(colName)> <#if column.inputType == "SELECT"> <td>${ column.value } - ${ selections[column.assosiatedSelection][ column.value ] }</td> <#else> <td>${column.value}</td> </#if> </#list>
      <td>${data["max_poikas_count_for_tarkastus_"+result.tarkastus.p_tarkastus_id]!""}</td>
      <td> <#if id != prevResult> <a id="visibility_link_${id}" href="javascript:void(0);" class="show_more" onclick=" changeVisibility( ${id}, '${texts.search_show_more}', '${texts.search_show_less}' ); blur(); return false; "> ${texts.search_show_more} </a> </#if> </td>
      <td> <#if id != prevResult> <p style="margin-bottom: 3px;"><a target="_karttapaikka" href="https://asiointi.maanmittauslaitos.fi/karttapaikka/?share=customMarker&n=${result.pesa.eur_leveys}&e=${result.pesa.eur_pituus}&title=${result.pesa.pesanimi.getUrlEncodedValue()}(${result.pesa.pesa_id})">kartalle</a></p> ${result.pesa.yht_leveys} ${result.pesa.yht_pituus} </#if>  </td>
      <td> <#if id != prevResult> Uusi tarkastus vuodelle: <form action="${system.baseURL}" method="post"> <input type="hidden" name="page"   value="tarkastus" /> <input type="hidden" name="action" value="fill_for_insert" /> <input type="hidden" name="updateparameters.pesavakio.pesa_id" value="${result.pesa.pesa_id}" /> <input type="text" size="4" name="tark_vuosi" value="${data.tark_vuosi!""}" <@checkerror errors "tark_vuosi" /> /> <input type="submit" value="Etene" /> </form> </#if>  </td>
      </tr><#assign prevResult = result.pesa.pesa_id>
    </#list>
     
    </tbody>
  </table>
  
  <#if data.item_count != "1">
   <a href="javascript:selectAll();" style="font-size: 10px;">&raquo; lisää kaikki pesät haaviin</a>
   <br />
  </#if>
  
  <#if data.search_type??>
  <br />
  <b> Suorita toiminto haavissa oleville pesille: </b>
  <table class="haavi">
    <tr>
      <td> Tulosta vastaukset: </td>
      <td> 
            <#if data.search_type == "quickSearch"> 
              <button onclick="if (!netIsEmpty()) {getIdsFromNetTo(document.getElementsByName('searchparameters.pesavakio.pesa_id')[1]); document.getElementById('print_to_textfile_quick').value='1'; document.search.submit();} else alert('Haavi on tyhjä');">
                Tulosta vastaukset tekstitiedostoon
              </button> 
            <#else>
              <button onclick="if (!netIsEmpty()) {generateAdvancedSearch(); getIdsFromNetTo(document.getElementsByName('searchparameters.pesavakio.pesa_id')[2]); document.getElementById('print_to_textfile_advanced').value='1'; document.advancedSearch.submit();} else alert('Haavi on tyhjä');">
                Tulosta vastaukset tekstitiedostoon
              </button>
            </#if>
      </td>
    </tr>
    <tr>
      <td>Esitäytetty lomake:</td>
      <td>
          <form action="${system.baseURL}" method="post" name="print_pdf">
          <input type="hidden" name="page"   value="${system.page}" />
          <input type="hidden" name="action" value="print_pdf" />
          <input type="hidden" name="searchparameters.pesavakio.pesa_id" value="" id="idsToPrint" /> 
          
          Vuodelle: <input type="text" size="4" name="yyyy" value="${system.currentYear}" /> 
          
          <input type="submit" onclick="if (!netIsEmpty()) {getIdsFromNetTo(document.getElementById('idsToPrint')); } else { alert('Haavi on tyhjä'); return false;}" value="Tulosta" />
          
          Kokoelman tiedostonimi:
          <input type="text" size="20" name="collection_filename" value="" />.pdf   &nbsp; &nbsp;     
                     
          (kokoelmaa ei luoda, jos tiedostonimeä ei anneta)
          </form>
      </td>
    </tr> 
  </table>
  </#if>
  
  <br /><br /><br />
  
</#if>
<#-- -------------------------- result listing ends ---------------------------------------------------------------------------------------------- -->
<#-- --------------------------------------------------------------------------------------------------------------------------------------------- -->

<#list files as file>
  <#if system.action == "print_pdf">
    <span class="file"> <a target="_new" href="${system.baseURL}?page=filemanagement&amp;action=show&amp;folder=pdf&amp;filename=${file}">${file}</a> </span> <br />
  <#else>
    <span class="file"> <a target="_new" href="${system.baseURL}?page=filemanagement&amp;action=show&amp;folder=report&amp;filename=${file}">${file}</a> </span> <br />
  </#if>
</#list>


<#-- --------------------------------------------------------------------------------------------------------------------------------------------- -->
<#-- -------------------------- QUICK SEARCH  ---------------------------------------------------------------------------------------------------- -->

<div id="quickSearch" <#if (data.search_type!"") == "advancedSearch">class="hidden"</#if> >

<h2>${texts.search_quick}</h2>

<form action="${system.baseURL}" method="post" name="search" onsubmit="return confirmSearch(document.search, '${texts.search_confirm_search_all}');" >
<input type="hidden" name="page"   value="${system.page}" />
<input type="hidden" name="action" value="search" />
<input type="hidden" name="search_type" value="quickSearch" />


<input type="hidden" name="selectedFields" value=" 
<#list resultcolumns as c>
  ${c},
</#list>
" />

<input type="hidden" name="print_to_textfile" value="" id="print_to_textfile_quick" />

<table style="min-width: 770px;">
  <tr>
    <td>${texts.search_quick_year}: </td>
    <td> <@inputText "searchparameters" searchparameters.tarkastus.tark_vuosi false errors /> </td> 
    <td colspan="2"> </td>
    <td rowspan="3">
      <select name="NestCondition" size="3">
        <option value="0" <#if (data.NestCondition == "0") > selected="selected" </#if> >Pesintäkuntoiset</option>
        <option value="1" <#if (data.NestCondition == "1") > selected="selected" </#if> >Tuhoutuneet</option>
        <option value="2" <#if (data.NestCondition == "2") > selected="selected" </#if> >Kaikki</option>
      </select> 
    </td>
  </tr>
  <tr>
    <td>${texts.search_quick_pesa}: </td>
    <td>${texts.search_quick_name}:       <@inputText "searchparameters" searchparameters.pesa.pesanimi false errors /> </td>
    <td>${texts.search_quick_id}:         <@inputText "searchparameters" searchparameters.pesa.pesa_id true errors  /> </td>
    <td>${texts.search_quick_pesa_kunta}: <@selection selections "searchparameters" searchparameters.vuosi.pesan_kunta 1 false true false errors /> </td>
  </tr>
  <tr>
    <td>${texts.search_quick_reviiri}: </td>
    <td>${texts.search_quick_name}:          <@inputText "searchparameters" searchparameters.reviiri.reviirinimi false errors /> </td>
    <td>${texts.search_quick_id}:            <@inputText "searchparameters" searchparameters.reviiri.reviiri_id  true errors /> </td>
    <td>${texts.search_quick_reviiri_kunta}: <@selection selections "searchparameters" searchparameters.vuosi.reviirin_kunta 1 false true false errors /> </td>
  </tr>
</table>
<table style="min-width: 770px;">
  <tr>
    <td>${texts.search_quick_rev_kunta_suuralue}: </td>
    <td> 
    	<select name="Suuralue">
    	  <option value=""></option>
    	  <#list selections["suuralueet"].options as s>
    	    <option value="${s.value}" <#if ( (data.Suuralue!"") == s.value) > selected="selected" </#if> > ${s.text} </option>
    	  </#list>
    	</select>
    </td>
    <td>${texts.search_quick_rev_kunta_ympkesk}: </td>
    <td>
      <select name="Ymparistokeskus">
          <option value=""></option>
    	  <#list selections["ely-centres"].options as s>
    	    <option value="${s.value}" <#if ( (data.Ymparistokeskus!"") == s.value) > selected="selected" </#if> > ${s.text} </option>
    	  </#list>
    	</select>
    </td>
  </tr>
</table>

<table style="min-width: 770px;">
  <tr>
    <td rowspan="2">${texts.search_quick_coordinates}: </td>
    <td>${texts.search_quick_yht}: </td>
	<td>${texts.search_quick_lat}: <@inputText "searchparameters" searchparameters.pesa.yht_leveys false errors /> </td>
	<td>${texts.search_quick_lon}: <@inputText "searchparameters" searchparameters.pesa.yht_pituus false errors /> </td>
	
	<td rowspan="2">${texts.search_quick_radius}:</td>
    <td rowspan="2">
      <@coordinateSelection errors "quick" />
    </td>
  </tr>
  <tr> 
    <td>${texts.search_quick_eur}: </td>
	<td>${texts.search_quick_lat}: <@inputText "searchparameters" searchparameters.pesa.eur_leveys false errors /> </td>
	<td>${texts.search_quick_lon}: <@inputText "searchparameters" searchparameters.pesa.eur_pituus false errors /> </td>
  </tr>
</table>


<input type="submit" value="${texts.search}"  /> 
<input type="submit" value="${texts.clear}"  onclick="document.clear.submit(); return false;" />

</form>

<#----------------- clear button ------------------------------>
  <form action="${system.baseURL}" method="get" name="clear">
  <input type="hidden" name="page"  value="${system.page}" />
  </form>
  
<#----------------- ------------ ------------------------------>


<br />
   

</div>

<#-- -------------------------- quick search ends  ----------------------------------------------------------------------------------------------- -->
<#-- --------------------------------------------------------------------------------------------------------------------------------------------- -->

<br /><br />

<#if (data.showAdvancedSearch!"false") == "false">
	<a href="${system.baseURL}?showAdvancedSearch=true">Näytä laaja haku</a>
<#else>
<#-- --------------------------------------------------------------------------------------------------------------------------------------------- -->
<#-- -------------------------- ADVANCED SEARCH -------------------------------------------------------------------------------------------------- -->

<div id="advancedSearch" <#if (data.search_type!"") == "quickSearch">class="hidden"</#if> >

<h2>${texts.search_advanced}</h2>

<form action="javascript:generateAdvancedSearch();" name="fieldSelections">
<table>
  <thead>
    <tr>
      <th>${texts.search_pesa}</th>
      <th>${texts.search_tarkastus}</th>
      <th>${texts.search_olosuhde}</th>
      <th>${texts.search_poikanen}</th>
      <th>${texts.search_aikuinen}</th>
      <th>${texts.search_vuosi}</th>
    </tr>
  </thead>
  
  <tbody>
    <tr>
      <td> <@fieldSelection searchparameters.pesa "pesa" texts /> </td>
      <td> <@fieldSelection searchparameters.tarkastus "tarkastus" texts /> </td>
      <td> <@fieldSelection searchparameters.olosuhde "olosuhde" texts /> </td>
      <td> <@fieldSelection searchparameters.poikanen "poikanen" texts /> </td>
      <td> <@fieldSelection searchparameters.aikuinen "aikuinen" texts /> </td>
      <td> <@fieldSelection searchparameters.vuosi "vuosi" texts /> </td>
    </tr>
  </tbody>
</table>
</form>


<br />


<form action="${system.baseURL}" method="post" name="advancedSearch">
<input type="hidden" name="page"   value="${system.page}" />
<input type="hidden" name="action" value="search" />
<input type="hidden" name="search_type" value="advancedSearch" />
<input type="hidden" name="selectedFields" id="selectedFields" value="" />
<input type="hidden" name="print_to_textfile" value="" id="print_to_textfile_advanced" />
<input type="hidden" name="showAdvancedSearch" value="true" />

<table>
  <tr>
    
    <td style="vertical-align: top;">
     
      <table id="advanced_search_table" <#if (data.search_type!"") != "advancedSearch"> </#if> >
        <thead>
          <tr>
            <th>${texts.search_pesa}</th>
            <th>${texts.search_tarkastus}</th>
            <th>${texts.search_olosuhde}</th>
            <th>${texts.search_poikanen}</th>
            <th>${texts.search_aikuinen}</th>
            <th>${texts.search_vuosi}</th>
          </tr>
        </thead>
        
        <tbody>
          <tr>
            <td valign="top"> <@searchFields searchparameters.pesa texts /> </td>
            <td valign="top"> <@searchFields searchparameters.tarkastus texts /> </td>
            <td valign="top"> <@searchFields searchparameters.olosuhde texts /> </td>
            <td valign="top"> <@searchFields searchparameters.poikanen texts /> </td>
            <td valign="top"> <@searchFields searchparameters.aikuinen texts /> </td>
            <td valign="top"> <@searchFields searchparameters.vuosi texts /> </td>
          </tr>
        </tbody>
      </table>
      
    </td>
    
    <td style="vertical-align: top;">
      
      <br /> 
      
      <select name="NestCondition" size="3">
        <option value="0" <#if (data.NestCondition == "0") > selected="selected" </#if> >Pesintäkuntoiset</option>
        <option value="1" <#if (data.NestCondition == "1") > selected="selected" </#if> >Tuhoutuneet</option>
        <option value="2" <#if (data.NestCondition == "2") > selected="selected" </#if> >Kaikki</option>
      </select>
      
      <table>
        <tr>
          <td>Koordinaattihaun säde: </td>
        </tr>
        <tr>
          <td>  
            <@coordinateSelection errors "advanced" />
          </td>
        </tr>
        <tr>
<td>${texts.search_quick_rev_kunta_suuralue}: </td>
</tr><tr>
    <td> 
    	<select name="Suuralue">
    	  <option value=""></option>
    	  <#list selections["suuralueet"].options as s>
    	    <option value="${s.value}" <#if ( (data.Suuralue!"") == s.value) > selected="selected" </#if> > ${s.text} </option>
    	  </#list>
    	</select>
    </td>
    </tr><tr>
    <td>${texts.search_quick_rev_kunta_ympkesk}: </td>
    </tr><tr>
    <td>
      <select name="Ymparistokeskus">
          <option value=""></option>
    	  <#list selections["ely-centres"].options as s>
    	    <option value="${s.value}" <#if ( (data.Ymparistokeskus!"") == s.value) > selected="selected" </#if> > ${s.text} </option>
    	  </#list>
    	</select>
    </td>
            </tr>
      </table>   
    
    </td>
    
  </tr>
</table>

<input type="submit" value="${texts.search}" onclick=" generateAdvancedSearch(); if (confirmSearch(document.advancedSearch, '${texts.search_confirm_search_all}')) document.advancedSearch.submit(); else return false;" />
<input type="submit" value="${texts.clear}"  onclick="document.clear.submit(); return false; " />   
<input type="submit" name="print_to_textfile" value="Tulosta vastaukset tekstitiedostoon" onclick=" generateAdvancedSearch(); if (confirmSearch(document.advancedSearch, 'Oletko varma että haluat tulostaa kaiken?')) document.advancedSearch.submit(); else return false;" />

</form>

<br />

</div>

<#-- ------------------------- advanced search ends  --------------------------------------------------------------------------------------------- -->
<#-- --------------------------------------------------------------------------------------------------------------------------------------------- -->
</#if>

</div>

<br /><br /><br /><br /><br /><br />


</body>
</html>

