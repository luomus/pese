package fi.hy.pese.framework.test.app.functionality;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fi.hy.pese.framework.main.app.BaseFactoryForApplicationSpecificationFactory;
import fi.hy.pese.framework.main.app.FunctionalityExecutor;
import fi.hy.pese.framework.main.app.Request;
import fi.hy.pese.framework.main.app.RequestImpleData;
import fi.hy.pese.framework.main.app._Functionality;
import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.app.functionality.BaseFunctionality;
import fi.hy.pese.framework.main.app.functionality.KirjekyyhkyManagementFunctionality;
import fi.hy.pese.framework.main.app.functionality.validate.Validator;
import fi.hy.pese.framework.main.app.functionality.validate._Validator;
import fi.hy.pese.framework.main.db._DAO;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.consts.Kirjekyyhky;
import fi.hy.pese.framework.main.general.data.Data;
import fi.hy.pese.framework.main.general.data.PesaDomainModelRowFactory;
import fi.hy.pese.framework.main.general.data._Data;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.merikotka.main.app.MerikotkaDatabaseStructureCreator;
import fi.luomus.commons.containers.Selection;
import fi.luomus.commons.kirjekyyhky.FormData;
import fi.luomus.commons.kirjekyyhky.KirjekyyhkyAPIClient;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class KirjekyyhkyManagementFunctionalityTests {

	public static Test suite() {
		return new TestSuite(KirjekyyhkyManagementFunctionalityTests.class.getDeclaredClasses());
	}

	public static class WhenReceivingAForm extends TestCase {


		private static class KirjekyyhkyManagementTestsFactory extends BaseFactoryForApplicationSpecificationFactory<_PesaDomainModelRow> {
			public KirjekyyhkyManagementTestsFactory() throws FileNotFoundException {
				super();
			}
			@Override
			public _Functionality<_PesaDomainModelRow> functionalityFor(String page, _Request<_PesaDomainModelRow> request) throws UnsupportedOperationException {
				if (page.equals(Const.TARKASTUS_PAGE)) {
					return new KirjekyyhkyManagementTestsFakeTarkastusFunctionality(request);
				}
				throw new UnsupportedOperationException("not implemented in this test");
			}
			@Override
			public KirjekyyhkyAPIClient kirjekyyhkyAPI() {
				return new KirjekyyhkyManagementTestsFakeKirjekyyhkyApi();
			}
			@Override
			protected Map<String, Selection> fetchSelections(_DAO<_PesaDomainModelRow> dao) throws Exception {
				return null;
			}
		}

		private static class KirjekyyhkyManagementTestsFakeKirjekyyhkyApi implements KirjekyyhkyAPIClient {

			public KirjekyyhkyManagementTestsFakeKirjekyyhkyApi() {
				kirjekyyhkyApiOpen = true;
			}

			private final Set<String> returnedForms = new HashSet<>();

			@Override
			public void send(File xml, File pdf) throws Exception {
				throw new UnsupportedOperationException("Not implemented in this test");
			}

			@Override
			public Integer getCount() throws Exception {
				return 1;
			}

			@Override
			public List<FormData> getForms() throws Exception {
				List<FormData> forms = new ArrayList<>();
				if (!returnedForms.contains("1")) {
					forms.add(new FormData("1", "mysystemid"));
				}
				return forms;
			}

			@Override
			public FormData get(String id) throws Exception {
				if (id.equals("1")) {
					Map<String, String> formData = new HashMap<>();
					return new FormData("1", "mysystemid").setData(formData);
				}
				if (id.equals("2")) {
					Map<String, String> formData = new HashMap<>();
					formData.put("pesavakio.pesa_id", "xxx");
					return new FormData("2", "mysystemid").setData(formData);
				}
				return null;
			}

			@Override
			public void receive(String id) throws Exception {
				receivedForms.add(id);
			}

			@Override
			public void close() {
				kirjekyyhkyApiOpen = false;
			}

			@Override
			public void sendForCorrections(String id, String message) {
				returnedForms.add(id);
			}

			@Override
			public void sendFormStructure(File xlm, File js) throws Exception {
			}

		}

		private static class KirjekyyhkyManagementTestsFakeTarkastusFunctionality extends BaseFunctionality<_PesaDomainModelRow> {

			public KirjekyyhkyManagementTestsFakeTarkastusFunctionality(_Request<_PesaDomainModelRow> request) {
				super(request);
			}

			@Override
			public _Validator validator() {
				return new KirjekyyhkyManagementTestsTarkastusFunctionalityFakeValidator(request);
			}

		}

		private static class KirjekyyhkyManagementTestsTarkastusFunctionalityFakeValidator extends Validator<_PesaDomainModelRow> {

			public KirjekyyhkyManagementTestsTarkastusFunctionalityFakeValidator(_Request<_PesaDomainModelRow> request) {
				super(request);
			}

			@Override
			public void validate() throws Exception {

			}

		}

		private static class TestKirjekyyhkyManagementFunctionality extends KirjekyyhkyManagementFunctionality {
			public TestKirjekyyhkyManagementFunctionality(_Request<_PesaDomainModelRow> request) {
				super(request);
			}
			@Override
			protected void applicationSpecificConversions(Map<String, String> formData, _PesaDomainModelRow kirjekyyhkyparameters) {

			}
			@Override
			protected void applicationSpecificReturnedEmptyProcedure(_PesaDomainModelRow kirjekyyhkyParameters) {

			}
			@Override
			protected void applicationSpecificComparisonDataOperations(_PesaDomainModelRow kirjekyyhkyParameters, _PesaDomainModelRow comparisonParameters) {

			}

		}

		private KirjekyyhkyManagementFunctionality functionality;
		private _Data<_PesaDomainModelRow> data;
		private static boolean kirjekyyhkyApiOpen;
		private static Set<String> receivedForms;

		@Override
		public void setUp() throws Exception {
			RequestImpleData<_PesaDomainModelRow> reqData = new RequestImpleData<>();
			reqData.setApplication(new KirjekyyhkyManagementTestsFactory());
			data = new Data<>(new PesaDomainModelRowFactory(MerikotkaDatabaseStructureCreator.hardCoded(), "", ""));
			reqData.setData(data);
			Request<_PesaDomainModelRow> req = new Request<>(reqData);
			functionality = new TestKirjekyyhkyManagementFunctionality(req);
			receivedForms = new HashSet<>();
		}

		public void test___when_no_action__lists_forms() throws Exception {
			data.setAction("");
			FunctionalityExecutor.execute(functionality);
			assertTrue("Data has list of forms and one form in the list", data.getLists().get(Kirjekyyhky.FORMS).size() == 1);
			Object o = data.getLists().get(Kirjekyyhky.FORMS).get(0);
			assertTrue(o instanceof FormData);
			FormData form = (FormData) o;
			assertEquals("1", form.getId());
			assertFalse(kirjekyyhkyApiOpen);
		}

		public void test___when_starting_to_receive_a_form__for_a_non_existing_form() throws Exception {
			try {
				data.setAction(Kirjekyyhky.RECEIVE);
				FunctionalityExecutor.execute(functionality);
				fail("Exception should have been thrown");
			} catch (NullPointerException e) {
				// OK
			}
		}

		public void test___when_starting_to_receive_a_form_it__hands_control_over_to_tarkastus_functionality____form_is_not_prefilled() throws Exception {
			data.setAction(Kirjekyyhky.RECEIVE);
			data.set(Kirjekyyhky.ID, "1");
			FunctionalityExecutor.execute(functionality);
			assertTrue("Form has not been received", !receivedForms.contains("1"));
			assertEquals(Const.FILL_FOR_INSERT_FIRST, data.action());
			assertEquals("1", data.get(Kirjekyyhky.ID));
			assertEquals(Const.TARKASTUS_PAGE, data.page());
			assertEquals(Const.YES, data.get(Kirjekyyhky.KIRJEKYYHKY));
		}

		public void test___when_starting_to_receive_a_form_it__hands_control_over_to_tarkastus_functionality____form_is_prefilled() throws Exception {
			data.setAction(Kirjekyyhky.RECEIVE);
			data.set(Kirjekyyhky.ID, "2");
			FunctionalityExecutor.execute(functionality);
			assertTrue("Form has not been received", !receivedForms.contains("1"));
			assertEquals(Const.FILL_FOR_INSERT, data.action());
			assertEquals("2", data.get(Kirjekyyhky.ID));
			assertEquals(Const.TARKASTUS_PAGE, data.page());
			assertEquals(Const.YES, data.get(Kirjekyyhky.KIRJEKYYHKY));
		}

		public void test___returning_form_for_corrections() throws Exception {
			data.setAction(Kirjekyyhky.RETURN_FOR_CORRECTIONS);
			data.set(Kirjekyyhky.ID, "1");
			data.set(Kirjekyyhky.RETURN_FOR_CORRECTIONS_MESSAGE, "message");
			FunctionalityExecutor.execute(functionality);
			assertTrue("Form has not been received", !receivedForms.contains("1"));
			assertEquals(null, data.getLists().get(Kirjekyyhky.FORMS));
			assertFalse(kirjekyyhkyApiOpen);
		}

	}
}
