package fi.hy.pese.framework.main.general.consts;

import fi.hy.pese.framework.main.general.data.DatabaseTableStructure;
import fi.hy.pese.framework.main.general.data._Column;

public class Const {
	
	// Database related
	public static final String	SEQUENCE_PREFIX							= "SEQ_";
	public static final String	SYSDATE									= "insert_sysdate_as_value_here";
	public static final String	VALUE_IS_NULL							= "NULL";
	public static final String	VALUE_IS_NOT							= "!";
	
	// Data structure
	public static final String	DATA									= "data";
	public static final String	SYSTEM									= "system";
	public static final String	LISTS									= "lists";
	public static final String	SEARCHPARAMETERS						= "searchparameters";
	public static final String	UPDATEPARAMETERS						= "updateparameters";
	public static final String	COMPARISONPARAMETERS					= "comparisonparameters";
	public static final String	RESULTS									= "results";
	public static final String	XML_OUTPUT								= "xml_output";
	public static final String	SELECTION_OPTIONS						= "selections";
	public static final String	OPTIONS_VALUE							= "value";
	public static final String	OPTIONS_TEXT							= "text";
	public static final String	ERRORS									= "errors";
	public static final Object	WARNINGS								= "warnings";
	public static final String	BASE_URL								= "baseURL";
	public static final String	COMMON_URL								= "commonURL";
	public static final String	LINTUVAARA_URL							= "lintuvaaraURL";
	public static final String	ACTION									= "action";
	public static final String	SUCCESS									= "success";
	public static final String	FAILURE									= "failure";
	public static final String	NOTICES									= "notices";
	public static final String	FILES									= "files";
	public static final String	TEXTS									= "texts";
	public static final String  HISTORY_ACTIONS							= "history_actions";
	public static final String	PAGE									= "page";
	public static final String	LANGUAGE								= "language";
	public static final String	YEAR									= "yyyy";
	public static final String	MONTH									= "mm";
	public static final String	DAY										= "dd";
	public static final String	ROWID									= "rowid";
	public static final String	USER_ID									= "user_id";
	public static final String	USER_NAME								= "user_name";
	public static final String	USER_TYPE								= "user_type";
	public static final String	CURRENT_YEAR							= "currentYear";
	public static final String  TIMESTAMP								= "timestamp";
	public static final String  LAST_UPDATE_TIMESTAMP					= "last_update_timestamp";
	public static final String	BYPASSED_WARNINGS						= "bypassed_warnings";
	public static final String	RESULT_COLUMNS							= "resultColumns";
	public static final String	URI										= "URI";
	
	// Misc
	public static final String	FILENAME								= "filename";
	public static final String	COLLECTION_FILENAME						= "collection_filename";
	public static final String	SEPARATOR								= ";;";
	public static final String	NO_RESULTS								= "no_results";
	public static final String  MOVE_TO_NEST_ID                         = "move_to_nest_id";
	public static final String  NEW_NEST_ID                             = "new_nest_id";
	public static final String  YES 									= "yes";
	public static final String	K_E	= "k_e";
	
	// Page / functionality names
	public static final String	LOGIN_PAGE								= "login";
	public static final String	TABLEMANAGEMENT_PAGE					= "tablemanagement";
	public static final String	SEARCH_PAGE								= "search";
	public static final String	TARKASTUS_PAGE							= "tarkastus";
	public static final String	INSERT_FIRST_PAGE						= "insert_first";
	public static final String	REPORTS_PAGE							= "reports";
	public static final String  FILEMANAGEMENT_PAGE						= "filemanagement";
	public static final String	LOGOUT_PAGE								= "logout";
	public static final String	PESINTAMANAGEMENT_PAGE					= "pesinnat";
	public static final String  COORDINATE_RADIUS_SEARCH_PAGE			= "coordinate_radius_search";
	public static final String	XML_OUTPUT_PAGE							= "xml_out";
	public static final String  KIRJEKYYHKY_MANAGEMENT_PAGE			    = "kirjekyyhky_management";
	public static final String	MAIN_PAGE								= "main_page";
	public static final String	OUTPUT_ALREADY_PRINTED_TO_REQUEST_OUT_PAGE	= "OUTPUT_ALREADY_PRINTED_TO_REQUEST_OUT_PAGE";
	
	// Actions
	public static final String	CHECK_LOGIN								= "check_login";
	public static final String	SEARCH									= "search";
	public static final String	UPDATE									= "update";
	public static final String	INSERT									= "insert";
	public static final String	DELETE									= "delete";
	public static final String	CHECK									= "check";
	public static final String	PRINT_TO_TEXTFILE						= "print_to_textfile";
	public static final String	PRINT_PDF								= "print_pdf";
	public static final String	PRINT_PDF_FULL							= "print_pdf_full";
	public static final String	PRINT_REPORT							= "print_report";
	public static final String 	SHOW									= "show";
	public static final String 	COMPRESS_AND_SHOW						= "compress_and_show";
	public static final String	SELECT_NEST								= "select_nest";
	public static final String	DELETE_NEST								= "delete_nest";
	public static final String	DELETE_TARKASTUKSET						= "delete_tarkastukset";
	public static final String	MOVE_TARKASTUKSET						= "move_tarkastukset";
	public static final String	MOVE_TARKASTUKSET_TO_NEW				= "move_tarkastukset_to_new";
	
	
	// Login fields (special as not tied to any table / error not tied to single field)
	public static final String	LOGIN_USERNAME							= "login_username";
	public static final String	LOGIN_PASSWORD							= "login_password";
	public static final String	LOGIN_FIELDS							= "login_fields";
	
	// Tablemanagement
	public static final String	TABLE									= "table";
	public static final String	INSERT_PAGE								= "insert_page";
	public static final String	TABLEMANAGEMENT_PERMISSIONS				= "tablemanagement_permissions";
	
	// Search
	public static final String	COORDINATE_SEARCH_RADIUS				= "CoordinateSearchRadius";
	public static final String	COORDINATE_FIELDS						= "coordinate_fields";
	public static final String	SELECTED_FIELDS							= "selectedFields";
	public static final String	SEARCH_VALUES							= "searchValues";
	public static final String	ROW_COUNT								= "row_count";
	public static final String	ITEM_COUNT								= "item_count";
	public static final String	MAX_POIKAS_COUNT_FOR_TARKASTUS_			= "max_poikas_count_for_tarkastus_";
	public static final String	PREFIX_FOR_SHORT_COLUMN_NAME			= "short.";
	public static final String	SAVED_SEARCHES							= "savedSearches";
	public static final String	SAVE_SEARCH								= "save_search";
	public static final String	SEARCH_NAME								= "searchName";
	public static final String	SEARCH_DESCRIPTION						= "searchDescription";
	public static final String	EXECUTE_SAVED_SEARH						= "execute_saved_search";
	public static final String	DELETE_SAVED_SEARCH						= "delete_saved_search";
	public static final String	NEST_CONDITION							= "NestCondition";
	public static final String	NEST_CONDITION__ONLY_LIVE_NESTS			= "0";
	public static final String	NEST_CONDITION__ONLY_DESTROYED_NESTS	= "1";
	public static final String	NEST_CONDITION__ALL						= "2";
	public static final String  SUURALUE                                = "Suuralue";
	public static final String  YMPARISTOKESKUS                         = "Ymparistokeskus";
	
	// Kirjekyyhky
	public static final String  GENERATE_KIRJEKYYHKY_FORM_DATA_XML 		= "GenerateKirjekyyhkyFormDataXML";
	
	
	// Reports
	public static final String	SELECTED_REPORTS						= "report";
	public static final String	RUN_DATE_TEXT							= "run_date_text";
	public static final String	RUN_PARAMETERS_TEXT						= "run_parameters_text";
	
	// Tarkastus
	public static final String	FILL_FOR_UPDATE							= "fill_for_update";
	public static final String	FILL_FOR_INSERT							= "fill_for_insert";
	public static final String	FILL_FOR_INSERT_FIRST					= "fill_for_insert_first";
	public static final String	INSERT_FIRST							= "insert_first";
	public static final String	EDELLISET_TARKASTUKSET					= "edelliset_tarkastukset";
	public static final String	RISTIRIIDAT								= "ristiriidat";
	
	public static final DatabaseTableStructure TRANSACTION_ENTRY_TABLE_STRUCTURE = initTransactionEntryTable();
	
	private static DatabaseTableStructure initTransactionEntryTable() {
		DatabaseTableStructure table = new DatabaseTableStructure("transaction");
		table.addColumn("transaction_id",_Column.INTEGER, 22);
		table.addColumn("row_id", _Column.INTEGER, 22);
		table.addColumn("attempted",_Column.INTEGER, 22);
		table.addColumn("created", _Column.DATE);
		
		table.get("transaction_id").setTypeToUniqueNumericIncreasingId();
		table.get("created").setTypeToDateAddedColumn();
		
		return table;
	}
	
	public static final String ETL_URI = "ETL_URI";
	public static final String ETL_API_KEY = "ETL_APIKey";
	
}
