package fi.hy.pese.pesakortti.main.app;

import fi.hy.pese.framework.main.app.PeSeServlet;
import fi.hy.pese.framework.main.app._ApplicationSpecificationFactory;
import fi.hy.pese.framework.main.general.data.PesaDomainModelDatabaseStructure;
import fi.hy.pese.framework.main.general.data.PesaDomainModelRowFactory;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.luomus.commons.db.connectivity.PreparedStatementStoringAndClosingTransactionConnection;
import fi.luomus.commons.db.connectivity.TransactionConnection;

public class PesakorttiServlet extends PeSeServlet<_PesaDomainModelRow> {

	static final long	serialVersionUID	= 2525L;

	@Override
	protected _ApplicationSpecificationFactory<_PesaDomainModelRow> createFactory() throws Exception {
		PesakorttiFactory factory = new PesakorttiFactory("pese_pesakortti.config");

		TransactionConnection con = new PreparedStatementStoringAndClosingTransactionConnection(factory.config().connectionDescription());
		try {
			PesaDomainModelDatabaseStructure structure = PesakorttiDatabaseStructureCreator.loadFromDatabase(con);
			factory.setRowFactory(new PesaDomainModelRowFactory(structure, PesakorttiConst.JOINS, PesakorttiConst.ORDER_BY));
		} catch (Exception e) {
			throw new Exception(e);
		} finally {
			con.release();
		}

		factory.loadUITexts();
		return factory;
	}

}
