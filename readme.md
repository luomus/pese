Pesäseurantajärjestelmä-framework (PeSe)
=======================================

Ja sillä toteuteutetut hallintajärjestelmät

* Merikotkatutkimus (Haliaeetus)
* Petopesätutkimus (Petopesä)
* Sääksitutkimus (Pandion)
* Pesäkorttitutkimus (Pesäkortti)
* Ulkomaisten rengaslöytöjen käsittely (ULKO)
* API linnustonseurannan tulospalvelulle rengastajan omien tietojen selailuun

