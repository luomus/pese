package fi.hy.pese.peto.main.functionality;

import java.sql.SQLException;

import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.app.functionality.TarkastusFunctionality;
import fi.hy.pese.framework.main.app.functionality.validate._Validator;
import fi.hy.pese.framework.main.general.PesimisenBiologisetRajatUtil;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.main.general.data._Table;
import fi.hy.pese.peto.main.app.PetoConst;
import fi.hy.pese.peto.main.functionality.validation.PetoTarkastusValidator;
import fi.luomus.commons.utils.CoordinateConverter;

public class PetoTarkastusFunctionality extends TarkastusFunctionality {
	
	public PetoTarkastusFunctionality(_Request<_PesaDomainModelRow> request) {
		super(request);
	}
	
	@Override
	public _Validator validator() throws Exception {
		return new PetoTarkastusValidator(request, PesimisenBiologisetRajatUtil.getInstance(request.config()));
	}
	
	@Override
	public void applicationSpecificDatamanipulation() throws Exception {
		String action = data.action();
		if (action.equals(Const.FILL_FOR_INSERT)) {
			selectWhatResultToUseForThisTarkYear();
			PetoConst.removeTarkastusValuesButKeepSpecific(data.updateparameters());
			moveTarkVuosiFromDataToUpdateparameters();
		} else if (action.equals(Const.INSERT_FIRST) || action.equals(Const.INSERT) || action.equals(Const.UPDATE)) {
			preInsertUpdate();
		}
	}
	
	private void moveTarkVuosiFromDataToUpdateparameters() {
		_Column tark_vuosi = data.updateparameters().getTarkastus().get("vuosi");
		tark_vuosi.setValue(data.get("tark_vuosi"));
	}
	
	private void selectWhatResultToUseForThisTarkYear() throws Exception {
		if (data.results().size() < 1) throw new Exception("No results");
		int tark_vuosi = Integer.valueOf(data.get("tark_vuosi"));
		_PesaDomainModelRow useAsBase = null;
		String edellinenLaji = null;
		for (_PesaDomainModelRow row : data.results()) {
			useAsBase = row;
			_Column pesivaLaji = row.getTarkastus().get("pesiva_laji");
			if (pesivaLaji.hasValue()) edellinenLaji = pesivaLaji.getValue();
			int row_tark_vuosi = Integer.valueOf(row.getTarkastus().get("vuosi").getValue());
			if (tark_vuosi > row_tark_vuosi) break;
		}
		data.setUpdateparameters(data.copyOf(useAsBase));
		if (edellinenLaji != null) {
			data.updateparameters().getTarkastus().get("kommentti_edellinen_laji").setValue(edellinenLaji);
		}
	}
	
	private void preInsertUpdate() throws Exception, SQLException {
		convertCoordinates();
		setIds();
	}
	
	private void setIds() throws SQLException {
		String action = data.action();
		_PesaDomainModelRow params = data.updateparameters();
		String pesa_id = null;
		String tarkastus_id = null;
		if (action.equals(Const.INSERT_FIRST)) {
			pesa_id = dao.returnAndSetNextId(params.getPesa());
			tarkastus_id = dao.returnAndSetNextId(params.getTarkastus());
		} else if (action.equals(Const.INSERT)) {
			pesa_id = params.getPesa().getUniqueNumericIdColumn().getValue();
			tarkastus_id = dao.returnAndSetNextId(params.getTarkastus());
		} else if (action.equals(Const.UPDATE)) {
			pesa_id = params.getPesa().getUniqueNumericIdColumn().getValue();
			tarkastus_id = params.getTarkastus().getUniqueNumericIdColumn().getValue();
		}
		params.getTarkastus().get("pesa").setValue(pesa_id);
		
		for (_Table kaynti : params.getKaynnit().values()) {
			if (kaynti.hasValues()) {
				kaynti.get("tarkastus").setValue(tarkastus_id);
			}
		}
	}
	
	private void convertCoordinates() throws Exception {
		_Table pesa = data.updateparameters().getPesa();
		CoordinateConverter converter = new CoordinateConverter();
		switch (pesa.get("koord_ilm_tyyppi").getValue().charAt(0)) {
			case 'Y':
				converter.setYht_leveys(pesa.get("yht_leveys").getValue());
				converter.setYht_pituus(pesa.get("yht_pituus").getValue());
				break;
			case 'E':
				converter.setEur_leveys(pesa.get("eur_leveys").getValue());
				converter.setEur_pituus(pesa.get("eur_pituus").getValue());
				break;
			case 'A':
				converter.setAst_leveys(pesa.get("ast_leveys").getValue());
				converter.setAst_pituus(pesa.get("ast_pituus").getValue());
				break;
		}
		converter.convert();
		pesa.get("yht_leveys").setValue(converter.getYht_leveys().toString());
		pesa.get("yht_pituus").setValue(converter.getYht_pituus().toString());
		pesa.get("eur_leveys").setValue(converter.getEur_leveys().toString());
		pesa.get("eur_pituus").setValue(converter.getEur_pituus().toString());
		pesa.get("ast_leveys").setValue(converter.getAst_leveys().toString());
		pesa.get("ast_pituus").setValue(converter.getAst_pituus().toString());
		pesa.get("des_leveys").setValue(converter.getDes_leveys().toString());
		pesa.get("des_pituus").setValue(converter.getDes_pituus().toString());
	}
	
	@Override
	protected void doInsertFirst() throws Exception {
		_PesaDomainModelRow params = data.updateparameters();
		dao.executeInsert(params.getPesa());
		dao.executeInsert(params.getTarkastus());
		insertKaynnit(params);
	}
	
	@Override
	protected void doInsert() throws Exception {
		_PesaDomainModelRow params = data.updateparameters();
		updateIfChanges(params.getPesa());
		dao.executeInsert(params.getTarkastus());
		insertKaynnit(params);
	}
	
	@Override
	protected void doUpdate() throws Exception {
		_PesaDomainModelRow params = data.updateparameters();
		updateIfChanges(params.getPesa());
		updateIfChanges(params.getTarkastus());
		updateKaynnit(params);
	}
	
	@Override
	protected boolean prevOlosuhdeIsForThisYear(_Table currentTarkastus, _Table prevOlosuhde) {
		throw new UnsupportedOperationException("Olosuhde table not used by peto-app");
	}
	
	
}
