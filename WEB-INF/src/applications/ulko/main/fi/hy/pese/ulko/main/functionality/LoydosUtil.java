package fi.hy.pese.ulko.main.functionality;

import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._Data;
import fi.hy.pese.ulko.main.app._UlkoDomainModelRow;
import fi.luomus.commons.config.Config;
import fi.luomus.commons.utils.URIBuilder;

public class LoydosUtil {

	private final _Data<_UlkoDomainModelRow> data;
	private final String loydosBaseURL;
	private final String username;
	private final String password;

	public LoydosUtil(_Request<_UlkoDomainModelRow> request) {
		this.data = request.data();
		Config config = request.config();
		this.loydosBaseURL = config.get("Loydos_uri");
		this.username = config.get("Loydos_username");
		this.password = config.get("Loydos_password");
	}

	public void setLoydosValuesToData() {
		_Column loydosDocumentID = data.updateparameters().getPyynto().get("loydosid");
		if (loydosDocumentID.hasValue()) {
			String loydosLinkURL = loydosLinkURL(loydosDocumentID);
			data.set("loydos_link_url", loydosLinkURL);
			addKuvaIds(loydosDocumentID);
		}
	}

	private void addKuvaIds(_Column loydosDocumentID) {
		_Column loydosKuvaId = data.updateparameters().getPyynto().get("loydos_kuvaid");
		if (loydosKuvaId.hasValue()) {
			for (String kuvaId : loydosKuvaId.getValue().split(";")) {
				URIBuilder loydosKuvaLinkURL = new URIBuilder(loydosBaseURL + "/auth");
				loydosKuvaLinkURL.addParameter("username", username);
				loydosKuvaLinkURL.addParameter("password", password);
				loydosKuvaLinkURL.addParameter("next", "admin/api/submissions/rengasloyto/"+loydosDocumentID.getValue()+"/attachments/" + kuvaId);
				data.addToList("loydos_kuva_links", loydosKuvaLinkURL.toString());
			}
		}
	}

	private String loydosLinkURL(_Column loydosDocumentID) {
		URIBuilder loydosLinkURL = new URIBuilder(loydosBaseURL + "/auth");
		loydosLinkURL.addParameter("username", username);
		loydosLinkURL.addParameter("password", password);
		loydosLinkURL.addParameter("next", "admin/submissions/rengasloyto/#" + loydosDocumentID.getValue());
		return loydosLinkURL.toString();
	}

}
