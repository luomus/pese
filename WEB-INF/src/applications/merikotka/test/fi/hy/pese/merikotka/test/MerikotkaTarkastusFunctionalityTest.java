package fi.hy.pese.merikotka.test;

import java.sql.Date;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import fi.hy.pese.framework.main.app.FunctionalityExecutor;
import fi.hy.pese.framework.main.app.Request;
import fi.hy.pese.framework.main.app.RequestImpleData;
import fi.hy.pese.framework.main.app._Functionality;
import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.app.functionality.validate._Validator;
import fi.hy.pese.framework.main.db._DAO;
import fi.hy.pese.framework.main.db._ResultSet;
import fi.hy.pese.framework.main.db._ResultsHandler;
import fi.hy.pese.framework.main.db.oraclesql.OracleSQL_DAO;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.consts.Success;
import fi.hy.pese.framework.main.general.data.Data;
import fi.hy.pese.framework.main.general.data.PesaDomainModelDatabaseStructure;
import fi.hy.pese.framework.main.general.data.PesaDomainModelRow;
import fi.hy.pese.framework.main.general.data.PesaDomainModelRowFactory;
import fi.hy.pese.framework.main.general.data.Table;
import fi.hy.pese.framework.main.general.data._Data;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.main.general.data._RowFactory;
import fi.hy.pese.framework.main.general.data._Table;
import fi.hy.pese.framework.test.TestConfig;
import fi.hy.pese.framework.test.app.functionality.FakeHttpRedirected;
import fi.hy.pese.framework.test.db.DAOStub;
import fi.hy.pese.merikotka.main.app.MerikotkaConst;
import fi.hy.pese.merikotka.main.app.MerikotkaDatabaseStructureCreator;
import fi.hy.pese.merikotka.main.app.MerikotkaFactory;
import fi.hy.pese.merikotka.main.functionality.MerikotkaTarkastusFunctionality;
import fi.luomus.commons.containers.DateValue;
import fi.luomus.commons.containers.Selection;
import fi.luomus.commons.tipuapi.TipuAPIClient;
import fi.luomus.commons.tipuapi.TipuApiResource;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.Utils;
import fi.luomus.commons.xml.Document.Node;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class MerikotkaTarkastusFunctionalityTest {

	private static final String	HELSINKI	= "60";

	public static Test suite() {
		return new TestSuite(MerikotkaTarkastusFunctionalityTest.class.getDeclaredClasses());
	}

	public static class PoikanenAges extends TestCase {

		public void test___selecting_max_siipi() {
			String p1 = "136";
			String p2 = "187";
			String p3 = "";
			assertEquals("187", Utils.maxOf(p1, p2, p3));
		}

		public void test___koiras_ika() {
			assertEquals("0", MerikotkaConst.poikanenIka("0", MerikotkaConst.KOIRAS));
			assertEquals("0", MerikotkaConst.poikanenIka("24", MerikotkaConst.KOIRAS));
			assertEquals("10", MerikotkaConst.poikanenIka("25", MerikotkaConst.KOIRAS));
			assertEquals("10", MerikotkaConst.poikanenIka("26", MerikotkaConst.KOIRAS));
			assertEquals("34", MerikotkaConst.poikanenIka("250", MerikotkaConst.KOIRAS));
			assertEquals("65", MerikotkaConst.poikanenIka("530", MerikotkaConst.KOIRAS));
			assertEquals("69", MerikotkaConst.poikanenIka("575", MerikotkaConst.KOIRAS));
			assertEquals("70", MerikotkaConst.poikanenIka("576", MerikotkaConst.KOIRAS));
			assertEquals("70", MerikotkaConst.poikanenIka("585", MerikotkaConst.KOIRAS));
			assertEquals("70", MerikotkaConst.poikanenIka("586", MerikotkaConst.KOIRAS));
		}

		public void test__naaras_ika() {
			assertEquals("0", MerikotkaConst.poikanenIka("0", MerikotkaConst.NAARAS));
			assertEquals("0", MerikotkaConst.poikanenIka("1", MerikotkaConst.NAARAS));
			assertEquals("0", MerikotkaConst.poikanenIka("51", MerikotkaConst.NAARAS));
			assertEquals("10", MerikotkaConst.poikanenIka("52", MerikotkaConst.NAARAS));
			assertEquals("30", MerikotkaConst.poikanenIka("225", MerikotkaConst.NAARAS));
			assertEquals("63", MerikotkaConst.poikanenIka("500", MerikotkaConst.NAARAS));
			assertEquals("70", MerikotkaConst.poikanenIka("560", MerikotkaConst.NAARAS));
			assertEquals("70", MerikotkaConst.poikanenIka("567", MerikotkaConst.NAARAS));
			assertEquals("70", MerikotkaConst.poikanenIka("800", MerikotkaConst.NAARAS));
		}

		public void test___luultavasti_naaras_tai_luultavasti_koiras() {
			assertEquals("0", MerikotkaConst.poikanenIka("0", MerikotkaConst.LUULTAVASTI_KOIRAS));
			assertEquals("0", MerikotkaConst.poikanenIka("1", MerikotkaConst.LUULTAVASTI_NAARAS));
			assertEquals("0", MerikotkaConst.poikanenIka("39", MerikotkaConst.LUULTAVASTI_KOIRAS));
			assertEquals("10", MerikotkaConst.poikanenIka("40", MerikotkaConst.LUULTAVASTI_KOIRAS));
			assertEquals("30", MerikotkaConst.poikanenIka("225", MerikotkaConst.LUULTAVASTI_KOIRAS));
			assertEquals("62", MerikotkaConst.poikanenIka("500", MerikotkaConst.LUULTAVASTI_NAARAS));
			assertEquals("62", MerikotkaConst.poikanenIka("500", "foo"));
			assertEquals("62", MerikotkaConst.poikanenIka("500", ""));
			assertEquals("69", MerikotkaConst.poikanenIka("570", MerikotkaConst.LUULTAVASTI_NAARAS));
			assertEquals("70", MerikotkaConst.poikanenIka("571", MerikotkaConst.LUULTAVASTI_NAARAS));
		}

		public void test___kuoriutumispaiva() {
			assertEquals("", MerikotkaConst.kuoriutumispaiva(date(""), ""));
			assertEquals("", MerikotkaConst.kuoriutumispaiva(date(""), "10"));
			assertEquals("", MerikotkaConst.kuoriutumispaiva(date("1.3."), "10"));
			assertEquals("", MerikotkaConst.kuoriutumispaiva(date("1.3.2000"), ""));
			assertEquals("", MerikotkaConst.kuoriutumispaiva(date(null), (String[]) null));
			assertEquals("", MerikotkaConst.kuoriutumispaiva(date(null), "10"));
			assertEquals("", MerikotkaConst.kuoriutumispaiva(date("1.3.2009"), (String[]) null));
			assertEquals("-10", MerikotkaConst.kuoriutumispaiva(date("1.3.2000"), "10"));
			assertEquals("-10", MerikotkaConst.kuoriutumispaiva(date("1.3.2000"), "10", "5"));
			assertEquals("", MerikotkaConst.kuoriutumispaiva(date("1.3.2000"), "a"));
			assertEquals("1", MerikotkaConst.kuoriutumispaiva(date("1.4.2000"), "30"));
			assertEquals("21", MerikotkaConst.kuoriutumispaiva(date("1.4.2000"), "10"));
			assertEquals("-15", MerikotkaConst.kuoriutumispaiva(date("20.4.2000"), "65"));
		}

		private DateValue date(String string) {
			return DateUtils.convertToDateValue(string);
		}

	}

	public static class ApplicationSpecificDataManipulation extends TestCase {

		public class FakeDAO extends OracleSQL_DAO<_PesaDomainModelRow> {

			public FakeDAO() {
				super(null, null);
			}

			@Override
			public String returnAndSetNextId(_Table table) throws SQLException {
				String value = "";
				if (table.getName().equals("pesavakio"))
					value = "100";
				else
					value = "700";

				table.getUniqueNumericIdColumn().setValue(value);
				return value;
			}

			@Override
			public int executeUpdate(String sql, String... values) {
				return 1;
			}

			@Override
			public void executeSearch(_Table parameters, _ResultsHandler resultHandler, String... orderBy) {}

			@Override
			public _PesaDomainModelRow newRow() {
				return new PesaDomainModelRow(structure);
			}
		}

		private MerikotkaTarkastusFunctionality				functionality;
		private _Data<_PesaDomainModelRow>					data;
		private _PesaDomainModelRow							params;
		private _DAO<_PesaDomainModelRow>					dao;
		private PesaDomainModelDatabaseStructure structure = null;

		@Override
		protected void setUp() {
			if (structure == null) {
				structure = MerikotkaDatabaseStructureCreator.hardCoded();
			}
			dao = new FakeDAO();
			data = new Data<>(new PesaDomainModelRowFactory(structure, MerikotkaConst.JOINS, MerikotkaConst.ORDER_BY));
			functionality = new MerikotkaTarkastusFunctionality(new Request<>(new RequestImpleData<_PesaDomainModelRow>().setData(data).setDao(dao)));
			params = data.updateparameters();
			params.getPesa().get("koord_tyyppi").setValue("Y");
			params.getPesa().get("yht_leveys").setValue("" + (_Validator.YHT_LEVEYS_MAX - 100));
			params.getPesa().get("yht_pituus").setValue("" + (_Validator.YHT_PITUUS_MAX - 100));
		}

		public void test__fill_for_insert__moves_tark_vuosi_from_data_to_updateparameters() throws Exception {
			data.setAction(Const.FILL_FOR_INSERT);
			data.set("tark_vuosi", "2009");
			_PesaDomainModelRow r = data.newResultRow();
			r.getTarkastus().get("tark_vuosi").setValue("2009");
			functionality.applicationSpecificDatamanipulation();
			assertEquals("2009", data.updateparameters().getColumn("pesatarkastus.tark_vuosi").getValue());
		}

		public void test___insert_first___gets_new_pesaid_sets_it_to_other_fields() throws Exception {
			data.setAction(Const.INSERT_FIRST);
			functionality.applicationSpecificDatamanipulation();
			assertEquals("100", params.getPesa().get("pesa_id").getValue());
			assertEquals("100", params.getVuosi().get("pesa_id").getValue());
			assertEquals("100", params.getOlosuhde().get("pesa_id").getValue());
			assertEquals("100", params.getTarkastus().get("pesa_id").getValue());
		}

		public void test__insert__uses_existing_pesaid() throws Exception {
			data.setAction(Const.INSERT);
			params.getPesa().get("pesa_id").setValue("55");
			functionality.applicationSpecificDatamanipulation();
			assertEquals("55", params.getPesa().get("pesa_id").getValue());
			assertEquals("55", params.getVuosi().get("pesa_id").getValue());
			assertEquals("55", params.getOlosuhde().get("pesa_id").getValue());
			assertEquals("55", params.getTarkastus().get("pesa_id").getValue());
		}

		public void test__update__uses_existing_pesaid() throws Exception {
			data.setAction(Const.UPDATE);
			params.getPesa().get("pesa_id").setValue("55");
			functionality.applicationSpecificDatamanipulation();
			assertEquals("55", params.getPesa().get("pesa_id").getValue());
			assertEquals("55", params.getVuosi().get("pesa_id").getValue());
			assertEquals("55", params.getOlosuhde().get("pesa_id").getValue());
			assertEquals("55", params.getTarkastus().get("pesa_id").getValue());
		}

		public void test___insert_first___gets_new_tarkastusid_sets_it_to_poikanen_but_only_if_poikanen_has_some_other_data() throws Exception {
			data.setAction(Const.INSERT_FIRST);
			params.getPoikaset().get(1).get("rengas_oikea").setValue("asd");
			params.getAikuiset().get(1).get("rengas_oikea").setValue("asd");
			functionality.applicationSpecificDatamanipulation();
			assertEquals("700", params.getTarkastus().get("p_tarkastus_id").getValue());
			assertEquals("700", params.getPoikaset().get(1).get("tarkastus_id").getValue());
			assertEquals("700", params.getAikuiset().get(1).get("tarkastus_id").getValue());
			assertEquals("", params.getPoikaset().get(2).get("tarkastus_id").getValue());
			assertEquals("", params.getAikuiset().get(2).get("tarkastus_id").getValue());
		}

		public void test___insert___gets_new_tarkastusid_sets_it_to_poikanen_but_only_if_poikanen_has_some_other_data() throws Exception {
			data.setAction(Const.INSERT);
			params.getPoikaset().get(1).get("rengas_oikea").setValue("asd");
			functionality.applicationSpecificDatamanipulation();
			assertEquals("700", params.getTarkastus().get("p_tarkastus_id").getValue());
			assertEquals("700", params.getPoikaset().get(1).get("tarkastus_id").getValue());
			assertEquals("", params.getPoikaset().get(2).get("tarkastus_id").getValue());
		}

		public void test___update___uses_existing_tarkastusid_sets_it_to_poikanen_but_only_if_poikanen_has_some_other_data() throws Exception {
			data.setAction(Const.UPDATE);
			params.getTarkastus().get("p_tarkastus_id").setValue("678");
			params.getPoikaset().get(1).get("rengas_oikea").setValue("asd");
			functionality.applicationSpecificDatamanipulation();
			assertEquals("678", params.getTarkastus().get("p_tarkastus_id").getValue());
			assertEquals("678", params.getPoikaset().get(1).get("tarkastus_id").getValue());
			assertEquals("", params.getPoikaset().get(2).get("tarkastus_id").getValue());
		}

		public void test__sets_tark_vuosi_to_olosuhde_vuosi() throws Exception {
			data.setAction(Const.INSERT);
			params.getTarkastus().get("tark_vuosi").setValue("2009");
			functionality.applicationSpecificDatamanipulation();
			assertEquals("2009", params.getVuosi().get("vuosi").getValue());
			assertEquals("2009", params.getOlosuhde().get("havaintojen_vuosi").getValue());
		}

		public void test__it_calculates_and_sets__poikanen_ages_and__hatching_day_() throws Exception {
			data.setAction(Const.INSERT);
			params.getColumn("poikanen.1.siipi_pituus").setValue("335");
			params.getColumn("poikanen.1.sukupuoli").setValue(MerikotkaConst.NAARAS);
			params.getTarkastus().get("mittaus_pvm_1").setValue("1.3.2000");
			functionality.applicationSpecificDatamanipulation();
			assertEquals("43", params.getColumn("poikanen.1.poikasen_ika").getValue());
			assertEquals("", params.getColumn("poikanen.2.poikasen_ika").getValue());
			assertEquals("-43", params.getTarkastus().get("kuoriutumispaiva").getValue());
		}

		public void test__it_calculates_and_sets__poikanen_ages_and__hatching_day_2() throws Exception {
			data.setAction(Const.INSERT);
			params.getColumn("poikanen.1.siipi_pituus").setValue("335");
			params.getColumn("poikanen.1.sukupuoli").setValue(MerikotkaConst.NAARAS);
			params.getColumn("poikanen.2.siipi_pituus").setValue("400");
			params.getColumn("poikanen.2.sukupuoli").setValue(MerikotkaConst.NAARAS);
			params.getTarkastus().get("mittaus_pvm_1").setValue("1.3.2000");
			params.getTarkastus().get("mittaus_pvm_2").setValue("11.3.2000");
			functionality.applicationSpecificDatamanipulation();
			assertEquals("43", params.getColumn("poikanen.1.poikasen_ika").getValue());
			assertEquals("51", params.getColumn("poikanen.2.poikasen_ika").getValue());
			assertEquals("-51", params.getTarkastus().get("kuoriutumispaiva").getValue());
		}

		public void test__it_calculates_and_sets__poikanen_ages_and__hatching_day_3() throws Exception {
			data.setAction(Const.INSERT);
			params.getColumn("poikanen.1.siipi_pituus").setValue("335");
			params.getColumn("poikanen.1.sukupuoli").setValue(MerikotkaConst.NAARAS);
			params.getColumn("poikanen.2.siipi_pituus").setValue("400");
			params.getColumn("poikanen.2.siipi_pituus_2").setValue("450");
			params.getColumn("poikanen.2.sukupuoli").setValue(MerikotkaConst.NAARAS);
			params.getTarkastus().get("mittaus_pvm_1").setValue("1.3.2000");
			params.getTarkastus().get("mittaus_pvm_2").setValue("11.3.2000");
			functionality.applicationSpecificDatamanipulation();
			assertEquals("43", params.getColumn("poikanen.1.poikasen_ika").getValue());
			assertEquals("51", params.getColumn("poikanen.2.poikasen_ika").getValue());
			assertEquals("57", params.getColumn("poikanen.2.poikasen_ika_2").getValue());
			assertEquals("-47", params.getTarkastus().get("kuoriutumispaiva").getValue());
		}

	}

	public static class FillingTheForm extends TestCase {

		private static final String	DEAD_NEST_ID	= "67";
		private static final int YEAR_NEST_MARKED_DEAD = 2010;
		private final static TipuApiResource kunnat = initMunicipalities();

		private static TipuApiResource initMunicipalities() {
			TipuApiResource kunnatResource = new TipuApiResource();
			Node helsinki = new Node("municipality");
			helsinki.addChildNode("id").setContents("HELSIN");
			helsinki.addChildNode("merikotka-suuralue").setContents("A");
			kunnatResource.add(helsinki);
			return kunnatResource;
		}

		private class TarkastusFunctionalityTestFactory extends MerikotkaFactory {
			public TarkastusFunctionalityTestFactory(String configFile) throws Exception {
				super(configFile);
			}

			@Override
			public Map<String, Selection> fetchSelections(_DAO<_PesaDomainModelRow> dao)  {
				return null;
			}
			@Override
			public TipuApiResource getTipuApiResource(String resourcename)  {
				if (resourcename.equals(TipuAPIClient.MUNICIPALITIES)) {
					return kunnat;
				}
				throw new IllegalArgumentException("Not implemeted for resource " + resourcename);
			}

			@Override
			public _DAO<_PesaDomainModelRow> dao(String userId) {
				return new TarkastusFunctionalityFakeDAO(data);
			}
		}

		private class TarkastusFunctionalityFakeDAO extends OracleSQL_DAO<_PesaDomainModelRow> {
			private final _Data<_PesaDomainModelRow>	resultdata;
			public TarkastusFunctionalityFakeDAO(_Data<_PesaDomainModelRow> resultdata) {
				super(null, new PesaDomainModelRowFactory(databaseStructure, "", ""));
				this.resultdata = resultdata;
			}

			@Override
			public _Table returnTableByKeys(String tablename, String ... keys) throws SQLException {
				if (tablename.equals("pesatarkastus")) {
					if (keys[0].equals("1")) {
						_Table t = this.newTableByName(tablename);
						t.getUniqueNumericIdColumn().setValue("1");
						t.get("pesa_id").setValue("1");
						return t;
					}
					if (keys[0].equals("100")) {
						_Table t = this.newTableByName(tablename);
						t.getUniqueNumericIdColumn().setValue("100");
						t.get("pesa_id").setValue("1");
						return t;
					}
				}
				throw new SQLException("Not defined");
			}

			@Override
			public void executeSearch(_PesaDomainModelRow parameters, _ResultsHandler handler, String... additional) throws SQLException {
				if (parameters.getTarkastus().getUniqueNumericIdColumn().getValue().equals("1")) {
					_PesaDomainModelRow result = resultdata.newResultRow();
					result.getColumn("pesavakio.pesanimi").setValue("Pesa 1");
					result.getColumn("pesatarkastus.tark_vuosi").setValue("2009");
					result.getColumn("pesatarkastus.p_tarkastus_id").setValue("1");
				} else if (parameters.getPesa().getUniqueNumericIdColumn().getValue().equals("1")) {
					_PesaDomainModelRow result = resultdata.newResultRow();
					result.getColumn("pesavakio.pesanimi").setValue("Pesa 1");
					result.getColumn("pesatarkastus.p_tarkastus_id").setValue("5");
					result.getColumn("pesatarkastus.as_lkm_1000").setValue("10");
					result.getColumn("pesatarkastus.tark_vuosi").setValue("2008");
					result.getColumn("pesamuuttuva.pesa_mit_pvm").setValue("1.1.2008");
					result = resultdata.newResultRow();
					result.getColumn("pesavakio.pesanimi").setValue("Pesa 1");
					result.getColumn("pesatarkastus.p_tarkastus_id").setValue("1");
					result.getColumn("pesatarkastus.tark_vuosi").setValue("2005");
					result.getColumn("pesamuuttuva.pesa_mit_pvm").setValue("1.1.2005");
				} else if (parameters.getPesa().getUniqueNumericIdColumn().getValue().equals(DEAD_NEST_ID)) {
					_PesaDomainModelRow result = resultdata.newResultRow();
					result.getColumn("pesavakio.pesanimi").setValue("Kuollut pesä");
					result.getColumn("pesatarkastus.tark_vuosi").setValue("2009");
				}
			}

			@Override
			public void executeSearch(_Table parameters, _ResultsHandler handler, String... orderBy) throws SQLException {
				if (parameters.getName().equals("pesatarkastus")) {
					return;
				}
				handler.process(new _ResultSet() {
					boolean	first	= true;

					@Override
					public boolean next() throws SQLException {
						if (first) {
							first = false;
							return true;
						}
						return false;
					}

					@Override
					public String getString(String columnLabel) throws SQLException {
						return HELSINKI;
					}

					@Override
					public String getString(int i) throws SQLException {
						return HELSINKI;
					}

					@Override
					public Date getDate(String columnLabel) throws SQLException {
						return null;
					}

					@Override
					public Date getDate(int i) throws SQLException {
						return null;
					}
				});
			}

			@Override
			public boolean checkValuesExists(_Table table) throws SQLException {
				if (table.get("vuosi").getValue().equals("2008")) return true;
				if (table.get("pesa_id").getValue().equals("1")) return false; //MerikotkaDao: tarkastusAlreadyExistsForYear..
				if (table.get("pesa_id").getValue().equals(DEAD_NEST_ID)) return false; //MerikotkaDao: tarkastusAlreadyExistsForYear..
				throw new SQLException("not defined in this fake dao");
			}

			@Override
			public boolean checkRowExists(_Table table) throws SQLException {
				if (table.getFullname().equals("pesatarkastus")) {
					if (table.getUniqueNumericIdColumn().getValue().equals("1")) {
						return true;
					}
					if (table.getUniqueNumericIdColumn().getValue().equals("5")) {
						return true;
					}
					return false;
				}
				if (table.getFullname().equals("pesavakio")) {
					if (table.getUniqueNumericIdColumn().getValue().equals("1")) {
						return true;
					}
					if (table.getUniqueNumericIdColumn().getValue().equals(DEAD_NEST_ID)) {
						return true;
					}
					return false;
				}
				throw new SQLException("not defined in this fake dao");
			}

			@Override
			public void executeSearch(String sql, _ResultsHandler resultHandler, String... values) throws SQLException {
				if (Utils.removeWhitespace(sql).equals(Utils.removeWhitespace(" SELECT DISTINCT pesa_id FROM pesatarkastus WHERE pesa_kunto IN (  'Y' , 'U' , 'D'  ) "))) {
					resultHandler.process(new _ResultSet() {
						int	count	= 0;
						@Override
						public boolean next() throws SQLException {
							count++;
							if (count <= 1) {
								return true;
							}
							return false;
						}
						@Override
						public String getString(String columnLabel) throws SQLException {
							return null;
						}
						@Override
						public String getString(int i) throws SQLException {
							return DEAD_NEST_ID;
						}
						@Override
						public Date getDate(String columnLabel) throws SQLException {
							return null;
						}
						@Override
						public Date getDate(int i) throws SQLException {
							return null;
						}
					});
					return;
				}
				if (Utils.removeWhitespace(sql).equals(Utils.removeWhitespace(" SELECT MIN(tark_vuosi) FROM pesatarkastus WHERE pesa_kunto IN (  'Y' , 'U' , 'D'  )  AND pesa_id = ? "))) {
					resultHandler.process(new _ResultSet() {
						int	count	= 0;
						@Override
						public boolean next() throws SQLException {
							count++;
							if (count <= 1) {
								return true;
							}
							return false;
						}
						@Override
						public String getString(String columnLabel) throws SQLException {
							return null;
						}
						@Override
						public String getString(int i) throws SQLException {
							return String.valueOf(YEAR_NEST_MARKED_DEAD);
						}
						@Override
						public Date getDate(String columnLabel) throws SQLException {
							return null;
						}
						@Override
						public Date getDate(int i) throws SQLException {
							return null;
						}
					});
					return;
				}
				throw new SQLException("No such query defined in this fake DAO: " + sql);
			}
		}

		private _Data<_PesaDomainModelRow>				data;
		private _Functionality<_PesaDomainModelRow>		tarkastusFunctionality;
		private PesaDomainModelDatabaseStructure	databaseStructure;
		private FakeHttpRedirected redirecter;

		@Override
		protected void setUp() throws Exception {
			TarkastusFunctionalityTestFactory factory = new TarkastusFunctionalityTestFactory(TestConfig.getConfigFile("pese_merikotka.config"));
			databaseStructure = MerikotkaDatabaseStructureCreator.hardCoded();
			_RowFactory<_PesaDomainModelRow> rowFactory = new PesaDomainModelRowFactory(databaseStructure, "", "");
			factory.setRowFactory(rowFactory);
			data = new Data<>(rowFactory);
			redirecter = new FakeHttpRedirected();
			_Request<_PesaDomainModelRow> request =
					new Request<>(
							new RequestImpleData<_PesaDomainModelRow>().
							setData(data).
							setDao(factory.dao("userid")).
							setRedirecter(redirecter).
							setApplication(factory));
			tarkastusFunctionality = factory.functionalityFor(Const.TARKASTUS_PAGE, request);
		}

		public void test__giving_nonexisting__params_causes_validation_error() throws Exception {
			data.setPage(Const.TARKASTUS_PAGE);
			data.setAction(Const.FILL_FOR_UPDATE);
			data.updateparameters().getTarkastus().getUniqueNumericIdColumn().setValue("asdsa");
			FunctionalityExecutor.execute(tarkastusFunctionality);
			assertFalse("has not been redirected to 404", redirecter.isHttpStatusCodeSet());
			assertEquals(_Validator.MUST_BE_INTEGER, data.errors().get("pesatarkastus.p_tarkastus_id"));
		}

		public void test__fill_for_update__when_given_tarkastusid_it_fills_data_to_updateparameters() throws Exception {
			data.setPage(Const.TARKASTUS_PAGE);
			data.setAction(Const.FILL_FOR_UPDATE);
			data.updateparameters().getTarkastus().getUniqueNumericIdColumn().setValue("1");
			FunctionalityExecutor.execute(tarkastusFunctionality);
			assertEquals("Pesa 1", data.updateparameters().getPesa().get("pesanimi").getValue());
			assertFalse("has NOT been redirected to 404", redirecter.isHttpStatusCodeSet());
		}

		public void test____fill_for_update__doesnt_accept_updateparameters_that_dont_produce_results() throws Exception {
			data.setPage(Const.TARKASTUS_PAGE);
			data.setAction(Const.FILL_FOR_UPDATE);
			data.updateparameters().getTarkastus().getUniqueNumericIdColumn().setValue("99999");
			FunctionalityExecutor.execute(tarkastusFunctionality);
			assertEquals(_Validator.DOES_NOT_EXIST, data.errors().get("pesatarkastus.p_tarkastus_id"));
		}

		public void test____fill_for_update__if_given_pesaid__fills_latest_tarkastus_info() throws Exception {
			data.setPage(Const.TARKASTUS_PAGE);
			data.setAction(Const.FILL_FOR_UPDATE);
			data.updateparameters().getPesa().getUniqueNumericIdColumn().setValue("1");
			data.updateparameters().getTarkastus().getUniqueNumericIdColumn().setValue("5");
			FunctionalityExecutor.execute(tarkastusFunctionality);
			assertEquals("Pesa 1", data.updateparameters().getPesa().get("pesanimi").getValue());
			assertEquals("5", data.updateparameters().getTarkastus().get("p_tarkastus_id").getValue());
		}

		public void test___fill_for_update___after_filling_sets_action_to_update() throws Exception {
			data.setPage(Const.TARKASTUS_PAGE);
			data.setAction(Const.FILL_FOR_UPDATE);
			data.updateparameters().getPesa().getUniqueNumericIdColumn().setValue("1");
			data.updateparameters().getTarkastus().getUniqueNumericIdColumn().setValue("5");
			FunctionalityExecutor.execute(tarkastusFunctionality);
			assertEquals(Const.UPDATE, data.action());
		}

		public void test__fill_for_insert____calls_application_specific_validation() throws Exception {
			data.setPage(Const.TARKASTUS_PAGE);
			data.setAction(Const.FILL_FOR_INSERT);
			data.updateparameters().getPesa().getUniqueNumericIdColumn().setValue("1");
			FunctionalityExecutor.execute(tarkastusFunctionality);
			assertTrue("should have errors", data.errors().size() > 0);
			assertEquals(_Validator.CAN_NOT_BE_NULL, data.errors().get("tark_vuosi"));
		}

		public void test__fill_for_insert____calls_application_specific_validation2() throws Exception {
			data.setPage(Const.TARKASTUS_PAGE);
			data.setAction(Const.FILL_FOR_INSERT);
			data.set("tark_vuosi", "a");
			data.updateparameters().getPesa().getUniqueNumericIdColumn().setValue("1");
			FunctionalityExecutor.execute(tarkastusFunctionality);
			assertTrue("should have errors", data.errors().size() > 0);
			assertEquals(_Validator.MUST_BE_INTEGER, data.errors().get("tark_vuosi"));
		}

		public void test__fill_for_insert____recirects_to_frontpage_if_errors_with_initial_params() throws Exception {
			data.setPage(Const.TARKASTUS_PAGE);
			data.setAction(Const.FILL_FOR_INSERT);
			FunctionalityExecutor.execute(tarkastusFunctionality);
			assertTrue("should have errors", data.errors().size() > 0);
			assertEquals(_Validator.DOES_NOT_EXIST, data.errors().get("pesavakio.pesa_id"));
			assertEquals(Const.SEARCH_PAGE, data.page());
			assertEquals("", data.action());
		}

		public void test__fill_for_insert__after_successful_validation_fills_previous_insepection_to_data() throws Exception {
			data.setPage(Const.TARKASTUS_PAGE);
			data.setAction(Const.FILL_FOR_INSERT);
			data.updateparameters().getColumn("pesavakio.pesa_id").setValue("1");
			data.set("tark_vuosi", "2009");
			FunctionalityExecutor.execute(tarkastusFunctionality);
			assertFalse("should not have errors", data.errors().size() > 0);
			assertEquals("Pesa 1", data.updateparameters().getColumn("pesavakio.pesanimi").getValue());
		}

		public void test__fill_for_instert__after_succ_validation_sets_action_to_insert() throws Exception {
			data.setPage(Const.TARKASTUS_PAGE);
			data.setAction(Const.FILL_FOR_INSERT);
			data.updateparameters().getColumn("pesavakio.pesa_id").setValue("1");
			data.set("tark_vuosi", "2009");
			FunctionalityExecutor.execute(tarkastusFunctionality);
			assertFalse("no errors", data.errors().size() > 0);
			assertEquals(Const.INSERT, data.action());
		}

		public void test__fill_for_insert____removes_those_values_that_are_not_wanted_to_be_prefilled() throws Exception {
			data.setPage(Const.TARKASTUS_PAGE);
			data.setAction(Const.FILL_FOR_INSERT);
			data.updateparameters().getColumn("pesavakio.pesa_id").setValue("1");
			data.set("tark_vuosi", "2009");
			FunctionalityExecutor.execute(tarkastusFunctionality);
			assertFalse("no errors", data.errors().size() > 0);
			assertEquals("", data.updateparameters().getColumn("pesatarkastus.p_tarkastus_id").getValue());
			assertEquals("", data.updateparameters().getColumn("pesatarkastus.pesa_id").getValue());
			assertEquals("10", data.updateparameters().getColumn("pesatarkastus.as_lkm_1000").getValue());
			assertEquals("", data.updateparameters().getColumn("pesatarkastus.pesimistulos").getValue());
		}

		public void test__fill_for_insert__validation_error_if_tarkastus_already_exists_for_the_year() throws Exception {
			data.setPage(Const.TARKASTUS_PAGE);
			data.setAction(Const.FILL_FOR_INSERT);
			data.updateparameters().getColumn("pesavakio.pesa_id").setValue("1");
			data.set("tark_vuosi", "2008");
			FunctionalityExecutor.execute(tarkastusFunctionality);
			assertEquals(_Validator.CAN_NOT_INSERT_ALREADY_EXISTS, data.errors().get("tark_vuosi"));
		}

		public void test___fill_for_insert__fails_years_that_are_in_the_future() throws Exception {
			data.setPage(Const.TARKASTUS_PAGE);
			data.setAction(Const.FILL_FOR_INSERT);
			data.updateparameters().getColumn("pesavakio.pesa_id").setValue("1");
			data.set("tark_vuosi", "3000");
			FunctionalityExecutor.execute(tarkastusFunctionality);
			assertEquals(_Validator.CAN_NOT_BE_IN_THE_FUTURE, data.errors().get("tark_vuosi"));
		}

		public void test___fill_for_insert_first___fetches_reviiri_and_pesa__kunnat_based_on_given_reviiriid() throws Exception {
			data.setPage(Const.TARKASTUS_PAGE);
			data.setAction(Const.FILL_FOR_INSERT_FIRST);
			data.updateparameters().getColumn("vuosi.reviiri_id").setValue("1");
			data.updateparameters().getColumn("pesatarkastus.tark_vuosi").setValue("2010");
			FunctionalityExecutor.execute(tarkastusFunctionality);
			assertEquals(HELSINKI, data.updateparameters().getColumn("vuosi.pesan_kunta").getValue());
			assertEquals(HELSINKI, data.updateparameters().getColumn("vuosi.reviirin_kunta").getValue());
		}

		public void test___fill_for_insert___doesnt_let_add_tarkastus_for_a_dead_nest__but_allows_update_on_the_same_year() throws Exception {
			data.setPage(Const.TARKASTUS_PAGE);
			data.setAction(Const.FILL_FOR_INSERT);
			data.updateparameters().getColumn("pesavakio.pesa_id").setValue(DEAD_NEST_ID);
			data.set("tark_vuosi", String.valueOf(YEAR_NEST_MARKED_DEAD));
			FunctionalityExecutor.execute(tarkastusFunctionality);
			assertEquals(null, data.errors().get("tark_vuosi"));
		}

		public void test___fill_for_insert___doesnt_let_add_tarkastus_for_a_dead_nest() throws Exception {
			data.setPage(Const.TARKASTUS_PAGE);
			data.setAction(Const.FILL_FOR_INSERT);
			data.updateparameters().getColumn("pesavakio.pesa_id").setValue(DEAD_NEST_ID);
			data.set("tark_vuosi", String.valueOf(YEAR_NEST_MARKED_DEAD + 1));
			FunctionalityExecutor.execute(tarkastusFunctionality);
			assertEquals(MerikotkaConst.PESA_IS_DEAD_CAN_NOT_INSERT, data.errors().get("tark_vuosi"));
		}

		public void test___fill_for_insert___doesnt_let_add_tarkastus_for_a_dead_nest____allows_past_inserts() throws Exception {
			data.setPage(Const.TARKASTUS_PAGE);
			data.setAction(Const.FILL_FOR_INSERT);
			data.updateparameters().getColumn("pesavakio.pesa_id").setValue(DEAD_NEST_ID);
			data.set("tark_vuosi", "1970");
			FunctionalityExecutor.execute(tarkastusFunctionality);
			assertEquals(null, data.errors().get("tark_vuosi"));
		}

		public void test___fill_for_insert___which_inspection_to_choose_as_base() throws Exception {
			data.setPage(Const.TARKASTUS_PAGE);
			data.setAction(Const.FILL_FOR_INSERT);
			data.updateparameters().getColumn("pesavakio.pesa_id").setValue("1");
			data.set("tark_vuosi", "2010");
			FunctionalityExecutor.execute(tarkastusFunctionality);
			assertEquals("1.1.2008", data.updateparameters().getOlosuhde().get("pesa_mit_pvm").getValue());
		}

		public void test___fill_for_insert___which_inspection_to_choose_as_base2() throws Exception {
			data.setPage(Const.TARKASTUS_PAGE);
			data.setAction(Const.FILL_FOR_INSERT);
			data.updateparameters().getColumn("pesavakio.pesa_id").setValue("1");
			data.set("tark_vuosi", "2007");
			FunctionalityExecutor.execute(tarkastusFunctionality);
			assertEquals("1.1.2005", data.updateparameters().getOlosuhde().get("pesa_mit_pvm").getValue());
		}

		public void test___fill_for_insert___which_inspection_to_choose_as_base3() throws Exception {
			data.setPage(Const.TARKASTUS_PAGE);
			data.setAction(Const.FILL_FOR_INSERT);
			data.updateparameters().getColumn("pesavakio.pesa_id").setValue("1");
			data.set("tark_vuosi", "2000");
			FunctionalityExecutor.execute(tarkastusFunctionality);
			assertEquals("1.1.2005", data.updateparameters().getOlosuhde().get("pesa_mit_pvm").getValue());
		}
	}

	public static class TarkastusLogicTest extends TestCase {

		private static final String	INSERT_PESA			= "insert_pesa";
		private static final String	INSERT_OLOSUHDE		= "insert_olosuhde";
		private static final String	INSERT_TARKASTUS	= "insert_tarkastus";
		private static final String	INSERT_VUOSI		= "insert_vuosi";
		private static final String	INSERT_P1			= "insert_p1";
		private static final String	INSERT_P2			= "insert_p2";
		private static final String	INSERT_P3			= "insert_p3";
		private static final String	INSERT_P4			= "insert_p4";
		private static final String	INSERT_A1			= "insert_a1";
		private static final String	INSERT_A2			= "insert_a2";
		private static final String	INSERT_A3			= "insert_a3";
		private static final String	UPDATE_PESA			= "update_pesa";
		private static final String	UPDATE_OLOSUHDE		= "update_olosuhde";
		private static final String	UPDATE_TARKASTUS	= "update_tarkastus";
		private static final String	UPDATE_VUOSI		= "update_vuosi";
		private static final String	UPDATE_P1			= "update_p1";
		private static final String	UPDATE_P2			= "update_p2";
		private static final String	UPDATE_P3			= "update_p3";
		private static final String	UPDATE_P4			= "update_p4";
		private static final String	UPDATE_A1			= "update_a1";
		private static final String	UPDATE_A2			= "update_a2";
		private static final String	UPDATE_A3			= "update_a3";
		private static final String	DELETE_PESA			= "delete_pesa";
		private static final String	DELETE_OLOSUHDE		= "delete_olosuhde";
		private static final String	DELETE_TARKASTUS	= "delete_tarkastus";
		private static final String	DELETE_VUOSI		= "delete_vuosi";
		private static final String	DELETE_P1			= "delete_p1";
		private static final String	DELETE_P2			= "delete_p2";
		private static final String	DELETE_P3			= "delete_p3";
		private static final String	DELETE_P4			= "delete_p4";
		private static final String	DELETE_A1			= "delete_a1";
		private static final String	DELETE_A2			= "delete_a2";
		private static final String	DELETE_A3			= "delete_a3";

		private class TarkastusLogicTestDAO extends DAOStub<_PesaDomainModelRow> {
			public final Map<String, Boolean>	performedOperations;
			private final PesaDomainModelDatabaseStructure		structure;

			public TarkastusLogicTestDAO(PesaDomainModelDatabaseStructure structure) {
				this.structure = structure;
				performedOperations = new HashMap<>();
				performedOperations.put(INSERT_PESA, false);
				performedOperations.put(INSERT_OLOSUHDE, false);
				performedOperations.put(INSERT_TARKASTUS, false);
				performedOperations.put(INSERT_VUOSI, false);
				performedOperations.put(INSERT_P1, false);
				performedOperations.put(INSERT_P2, false);
				performedOperations.put(INSERT_P3, false);
				performedOperations.put(INSERT_P4, false);
				performedOperations.put(INSERT_A1, false);
				performedOperations.put(INSERT_A2, false);
				performedOperations.put(INSERT_A3, false);

				performedOperations.put(UPDATE_PESA, false);
				performedOperations.put(UPDATE_OLOSUHDE, false);
				performedOperations.put(UPDATE_TARKASTUS, false);
				performedOperations.put(UPDATE_VUOSI, false);
				performedOperations.put(UPDATE_P1, false);
				performedOperations.put(UPDATE_P2, false);
				performedOperations.put(UPDATE_P3, false);
				performedOperations.put(UPDATE_P4, false);
				performedOperations.put(UPDATE_A1, false);
				performedOperations.put(UPDATE_A2, false);
				performedOperations.put(UPDATE_A3, false);

				performedOperations.put(DELETE_PESA, false);
				performedOperations.put(DELETE_OLOSUHDE, false);
				performedOperations.put(DELETE_TARKASTUS, false);
				performedOperations.put(DELETE_VUOSI, false);
				performedOperations.put(DELETE_P1, false);
				performedOperations.put(DELETE_P2, false);
				performedOperations.put(DELETE_P3, false);
				performedOperations.put(DELETE_P4, false);
				performedOperations.put(DELETE_A1, false);
				performedOperations.put(DELETE_A2, false);
				performedOperations.put(DELETE_A3, false);
			}

			@Override
			public void executeInsert(_Table table) {
				if (table.getFullname().equals("pesavakio")) {
					performedOperations.put(INSERT_PESA, true);
					return;
				}
				if (table.getFullname().equals("pesamuuttuva")) {
					performedOperations.put(INSERT_OLOSUHDE, true);
					return;
				}
				if (table.getFullname().equals("pesatarkastus")) {
					performedOperations.put(INSERT_TARKASTUS, true);
					return;
				}
				if (table.getFullname().equals("vuosi")) {
					performedOperations.put(INSERT_VUOSI, true);
					return;
				}
				if (table.getFullname().equals("poikanen.1")) {
					performedOperations.put(INSERT_P1, true);
					return;
				}
				if (table.getFullname().equals("poikanen.2")) {
					performedOperations.put(INSERT_P2, true);
					return;
				}
				if (table.getFullname().equals("poikanen.3")) {
					performedOperations.put(INSERT_P3, true);
					return;
				}
				if (table.getFullname().equals("poikanen.4")) {
					performedOperations.put(INSERT_P4, true);
					return;
				}
				if (table.getFullname().equals("aikuinen.1")) {
					performedOperations.put(INSERT_A1, true);
					return;
				}
				if (table.getFullname().equals("aikuinen.2")) {
					performedOperations.put(INSERT_A2, true);
					return;
				}
				if (table.getFullname().equals("aikuinen.3")) {
					performedOperations.put(INSERT_A3, true);
					return;
				}
			}

			@Override
			public void executeUpdate(_Table table) {
				if (table.getFullname().equals("pesavakio")) {
					performedOperations.put(UPDATE_PESA, true);
					return;
				}
				if (table.getFullname().equals("pesamuuttuva")) {
					performedOperations.put(UPDATE_OLOSUHDE, true);
					return;
				}
				if (table.getFullname().equals("pesatarkastus")) {
					performedOperations.put(UPDATE_TARKASTUS, true);
					return;
				}
				if (table.getFullname().equals("vuosi")) {
					performedOperations.put(UPDATE_VUOSI, true);
					return;
				}
				if (table.getFullname().equals("poikanen.1")) {
					performedOperations.put(UPDATE_P1, true);
					return;
				}
				if (table.getFullname().equals("poikanen.2")) {
					performedOperations.put(UPDATE_P2, true);
					return;
				}
				if (table.getFullname().equals("poikanen.3")) {
					performedOperations.put(UPDATE_P3, true);
					return;
				}
				if (table.getFullname().equals("poikanen.4")) {
					performedOperations.put(UPDATE_P4, true);
					return;
				}
				if (table.getFullname().equals("aikuinen.1")) {
					performedOperations.put(UPDATE_A1, true);
					return;
				}
				if (table.getFullname().equals("aikuinen.2")) {
					performedOperations.put(UPDATE_A2, true);
					return;
				}
				if (table.getFullname().equals("aikuinen.3")) {
					performedOperations.put(UPDATE_A3, true);
					return;
				}
			}

			@Override
			public void executeDelete(_Table table) {
				if (table.getFullname().equals("pesavakio")) {
					performedOperations.put(DELETE_PESA, true);
					return;
				}
				if (table.getFullname().equals("pesamuuttuva")) {
					performedOperations.put(DELETE_OLOSUHDE, true);
					return;
				}
				if (table.getFullname().equals("pesatarkastus")) {
					performedOperations.put(DELETE_TARKASTUS, true);
					return;
				}
				if (table.getFullname().equals("vuosi")) {
					performedOperations.put(DELETE_VUOSI, true);
					return;
				}
				if (table.getFullname().equals("poikanen.1")) {
					performedOperations.put(DELETE_P1, true);
					return;
				}
				if (table.getFullname().equals("poikanen.2")) {
					performedOperations.put(DELETE_P2, true);
					return;
				}
				if (table.getFullname().equals("poikanen.3")) {
					performedOperations.put(DELETE_P3, true);
					return;
				}
				if (table.getFullname().equals("poikanen.4")) {
					performedOperations.put(DELETE_P4, true);
					return;
				}
				if (table.getFullname().equals("aikuinen.1")) {
					performedOperations.put(DELETE_A1, true);
					return;
				}
				if (table.getFullname().equals("aikuinen.2")) {
					performedOperations.put(DELETE_A2, true);
					return;
				}
				if (table.getFullname().equals("aikuinen.3")) {
					performedOperations.put(DELETE_A3, true);
					return;
				}
			}

			@Override
			public _Table newTableByName(String tablename) {
				return new Table(structure.getTableStructureByName(tablename));
			}

			@Override
			public _PesaDomainModelRow newRow() {
				return new PesaDomainModelRow(structure);
			}

			@Override
			public _Table returnTableByRowid(String tablename, String rowid) throws SQLException {
				_Table table = this.newTableByName(tablename);
				if (table.getName().equals("pesavakio")) {
					table.get("pesanimi").setValue("");
					table.get("koord_tyyppi").setValue("Y");
					table.get("yht_leveys").setValue("" + (_Validator.YHT_LEVEYS_MAX - 100));
					table.get("yht_pituus").setValue("" + (_Validator.YHT_PITUUS_MAX - 100));
					table.get("ast_leveys").setValue("700426");
					table.get("ast_pituus").setValue("331851");
					table.get("des_leveys").setValue("70,0741209423392");
					table.get("des_pituus").setValue("33,314205616868");
					table.get("eur_leveys").setValue("7786650");
					table.get("eur_pituus").setValue("739631");
				} else if (table.getName().equals("pesamuuttuva")) {
					table.get("puusto_ika").setValue("");
					table.get("havaintojen_vuosi").setValue("2008");
					table.get("pesa_id").setValue("1");
				} else if (table.getName().equals("pesatarkastus")) {
					table.get("pesimistulos").setValue("");
					table.get("tark_vuosi").setValue("2008");
					table.get("pesa_id").setValue("1");
				} else if (table.getName().equals("poikanen")) {
					if (rowid.equals("1")) {
						table.get("rengas_oikea").setValue("000");
						table.get("tarkastus_id").setValue("1");
					} else {
						table.get("tarkastus_id").setValue("1");
					}
				} else if (table.getName().equals("aikuinen")) {
					if (rowid.equals("1")) {
						table.get("rengas_oikea").setValue("000");
						table.get("tarkastus_id").setValue("1");
					} else {
						table.get("tarkastus_id").setValue("1");
					}
				}
				return table;
			}

			@Override
			public _Table returnTableByKeys(String tablename, String... keys) throws SQLException {
				if (keys[0].equals("100")) {
					_Table t = this.newTableByName(tablename);
					t.getUniqueNumericIdColumn().setValue("100");
				}
				_Table table = this.newTableByName(tablename);
				if (table.getName().equals("pesavakio")) {
					table.get("pesanimi").setValue("");
					table.get("koord_tyyppi").setValue("Y");
					table.get("yht_leveys").setValue("" + (_Validator.YHT_LEVEYS_MAX - 100));
					table.get("yht_pituus").setValue("" + (_Validator.YHT_PITUUS_MAX - 100));
					table.get("ast_leveys").setValue("700427");
					table.get("ast_pituus").setValue("331851");
					table.get("des_leveys").setValue("70,0741209423392");
					table.get("des_pituus").setValue("33,314205616868");
					table.get("eur_leveys").setValue("7786650");
					table.get("eur_pituus").setValue("739631");
				} else if (table.getName().equals("pesamuuttuva")) {
					table.get("puusto_ika").setValue("");
					table.get("havaintojen_vuosi").setValue("2008");
				} else if (table.getName().equals("pesatarkastus")) {
					table.get("pesimistulos").setValue("");
					table.get("tark_vuosi").setValue("2008");
					table.get("pesa_id").setValue("1");
				} else if (table.getName().equals("poikanen")) {
					if (keys[0].equals("1")) {
						table.get("rengas_oikea").setValue("000");
						table.get("tarkastus_id").setValue("1");
					} else {
						table.get("tarkastus_id").setValue("1");
					}
				} else if (table.getName().equals("aikuinen")) {
					if (keys[0].equals("1")) {
						table.get("rengas_oikea").setValue("000");
						table.get("tarkastus_id").setValue("1");
					} else {
						table.get("tarkastus_id").setValue("1");
					}
				}
				return table;
			}

			@Override
			public String returnValueByRowid(String table, String valueColumn, String rowid) {
				return "1";
			}

			@Override
			public boolean checkRowExists(_Table table) throws SQLException {
				return true;
			}

			@Override
			public void executeSearch(_PesaDomainModelRow parameters, _ResultsHandler resultHandler, String... additional) {
				data.newResultRow(data.updateparameters());
			}

			@Override
			public String returnAndSetNextId(_Table table) throws SQLException {
				return "100";
			}

			@Override
			public void executeSearch(_Table parameters, _ResultsHandler resultHandler, String... orderBy) {

			}
		}

		private _Data<_PesaDomainModelRow>							data;
		private _Request<_PesaDomainModelRow>						request;
		private MerikotkaTarkastusFunctionality	functionality;
		private TarkastusLogicTestDAO			testDao;
		private _PesaDomainModelRow							params;

		@Override
		protected void setUp() throws Exception {
			MerikotkaFactory factory = new MerikotkaFactory(TestConfig.getConfigFile("pese_merikotka.config"));
			factory.loadUITexts();
			PesaDomainModelDatabaseStructure structure =  MerikotkaDatabaseStructureCreator.hardCoded();
			_RowFactory<_PesaDomainModelRow> rowFactory = new PesaDomainModelRowFactory(structure, "", "");
			testDao = new TarkastusLogicTestDAO(structure);
			data = new Data<>(rowFactory);
			RequestImpleData<_PesaDomainModelRow> reqData = new RequestImpleData<>();
			reqData.setData(data).setDao(testDao).setApplication(factory);
			request = new Request<>(reqData);
			functionality = new MerikotkaTarkastusFunctionality(request);
			params = data.updateparameters();
			params.getPesa().get("koord_tyyppi").setValue("Y");
			params.getPesa().get("yht_leveys").setValue("" + (_Validator.YHT_LEVEYS_MAX - 100));
			params.getPesa().get("yht_pituus").setValue("" + (_Validator.YHT_PITUUS_MAX - 100));
			params.getTarkastus().getUniqueNumericIdColumn().setValue("1");
			params.getPesa().getUniqueNumericIdColumn().setValue("1");
			params.getColumn("pesamuuttuva.rowid").setValue("aaa");
			params.getColumn("pesavakio.rowid").setValue("aaa");
			params.getColumn("pesatarkastus.rowid").setValue("aaa");
			params.getColumn("vuosi.rowid").setValue("aaa");
		}

		@Override
		protected void tearDown() throws Exception {}

		private void assertGivenOperationsAreCalledRestAreNotCalled(String... shouldBeTrueValues) {
			Set<String> shouldBeFalseValues = new HashSet<>();
			shouldBeFalseValues.add(INSERT_PESA);
			shouldBeFalseValues.add(INSERT_OLOSUHDE);
			shouldBeFalseValues.add(INSERT_TARKASTUS);
			shouldBeFalseValues.add(INSERT_VUOSI);
			shouldBeFalseValues.add(INSERT_P1);
			shouldBeFalseValues.add(INSERT_P2);
			shouldBeFalseValues.add(INSERT_P3);
			shouldBeFalseValues.add(INSERT_P4);
			shouldBeFalseValues.add(INSERT_A1);
			shouldBeFalseValues.add(INSERT_A2);
			shouldBeFalseValues.add(INSERT_A3);
			shouldBeFalseValues.add(UPDATE_PESA);
			shouldBeFalseValues.add(UPDATE_OLOSUHDE);
			shouldBeFalseValues.add(UPDATE_TARKASTUS);
			shouldBeFalseValues.add(UPDATE_VUOSI);
			shouldBeFalseValues.add(UPDATE_P1);
			shouldBeFalseValues.add(UPDATE_P2);
			shouldBeFalseValues.add(UPDATE_P3);
			shouldBeFalseValues.add(UPDATE_P4);
			shouldBeFalseValues.add(UPDATE_A1);
			shouldBeFalseValues.add(UPDATE_A2);
			shouldBeFalseValues.add(UPDATE_A3);
			shouldBeFalseValues.add(DELETE_PESA);
			shouldBeFalseValues.add(DELETE_OLOSUHDE);
			shouldBeFalseValues.add(DELETE_TARKASTUS);
			shouldBeFalseValues.add(DELETE_VUOSI);
			shouldBeFalseValues.add(DELETE_P1);
			shouldBeFalseValues.add(DELETE_P2);
			shouldBeFalseValues.add(DELETE_P3);
			shouldBeFalseValues.add(DELETE_P4);
			shouldBeFalseValues.add(DELETE_A1);
			shouldBeFalseValues.add(DELETE_A2);
			shouldBeFalseValues.add(DELETE_A3);
			for (String s : shouldBeTrueValues) {
				shouldBeFalseValues.remove(s);
				assertEquals(s + " should be called", Boolean.TRUE, testDao.performedOperations.get(s));
			}
			for (String s : shouldBeFalseValues) {
				assertEquals(s + " should not have been called", Boolean.FALSE, testDao.performedOperations.get(s));
			}
		}

		public void test___insert_first__it_sets_success_text() throws Exception {
			data.updateparameters().getPesa().getUniqueNumericIdColumn().setValue("1");
			data.setAction(Const.INSERT_FIRST);
			functionality.afterSuccessfulValidation();
			assertEquals(Success.TARKASTUS_INSERT_FIRST, data.successText());
		}

		public void test___insert_first__it_tries_to_insert_the_tables() throws Exception {
			data.setAction(Const.INSERT_FIRST);
			functionality.afterSuccessfulValidation();
			assertGivenOperationsAreCalledRestAreNotCalled(INSERT_PESA, INSERT_OLOSUHDE, INSERT_TARKASTUS, INSERT_VUOSI);
		}

		public void test___insert_first__it_tries_to_insert_those_poikanen_tables_which_have_data() throws Exception {
			data.setAction(Const.INSERT_FIRST);
			params.getColumn("poikanen.1.rengas_oikea").setValue("aaa");
			params.getColumn("poikanen.2.rengas_oikea").setValue("aaa");
			functionality.afterSuccessfulValidation();
			assertGivenOperationsAreCalledRestAreNotCalled(INSERT_PESA, INSERT_OLOSUHDE, INSERT_TARKASTUS, INSERT_VUOSI, INSERT_P1, INSERT_P2);
		}

		public void test___insert_first__it_tries_to_insert_those_aikuinen_tables_which_have_data() throws Exception {
			data.setAction(Const.INSERT_FIRST);
			params.getColumn("aikuinen.1.rengas_oikea").setValue("aaa");
			params.getColumn("aikuinen.2.rengas_oikea").setValue("aaa");
			functionality.afterSuccessfulValidation();
			assertGivenOperationsAreCalledRestAreNotCalled(INSERT_PESA, INSERT_OLOSUHDE, INSERT_TARKASTUS, INSERT_VUOSI, INSERT_A1, INSERT_A2);
		}

		public void test___insert___it_sets_success_text() throws Exception {
			data.setAction(Const.INSERT);
			functionality.afterSuccessfulValidation();
			assertEquals(Success.TARKASTUS_INSERT, data.successText());
		}

		public void test___insert______no_changes_to_pesa_olosuhde() throws Exception {
			data.setAction(Const.INSERT);
			params.getColumn("poikanen.1.rengas_oikea").setValue("aaa");
			params.getColumn("aikuinen.1.rengas_oikea").setValue("aaa");
			params.getColumn("pesamuuttuva.havaintojen_vuosi").setValue("2008");
			params.getColumn("pesatarkastus.tark_vuosi").setValue("2009");
			functionality.afterSuccessfulValidation();
			assertGivenOperationsAreCalledRestAreNotCalled(INSERT_TARKASTUS, INSERT_VUOSI, INSERT_P1, INSERT_A1);
		}

		public void test___insert______there_are_changes_to_pesa() throws Exception {
			data.setAction(Const.INSERT);
			params.getColumn("pesavakio.pesanimi").setValue("uusi nimi");
			params.getColumn("pesamuuttuva.havaintojen_vuosi").setValue("2008");
			params.getColumn("pesatarkastus.tark_vuosi").setValue("2009");
			functionality.afterSuccessfulValidation();
			assertGivenOperationsAreCalledRestAreNotCalled(UPDATE_PESA, INSERT_TARKASTUS, INSERT_VUOSI);
		}

		public void test___insert___there_are_changes_to_olosuhde() throws Exception {
			data.setAction(Const.INSERT);
			params.getColumn("pesamuuttuva.puusto_ika").setValue("10");
			params.getColumn("pesamuuttuva.havaintojen_vuosi").setValue("2008");
			params.getColumn("pesatarkastus.tark_vuosi").setValue("2009");
			functionality.afterSuccessfulValidation();
			assertGivenOperationsAreCalledRestAreNotCalled(INSERT_OLOSUHDE, INSERT_TARKASTUS, INSERT_VUOSI);
		}

		public void test___update___it_sets_success_text() throws Exception {
			data.setAction(Const.UPDATE);
			params.getTarkastus().get("p_tarkastus_id").setValue("1");
			functionality.afterSuccessfulValidation();
			assertEquals(Success.TARKASTUS_UPDATE, data.successText());
		}

		public void test___update___there_are_no_changes_at_all() throws Exception {
			data.setAction(Const.UPDATE);
			params.getColumn("pesamuuttuva.havaintojen_vuosi").setValue("2008");
			params.getColumn("pesatarkastus.tark_vuosi").setValue("2008");
			params.getTarkastus().get("p_tarkastus_id").setValue("1");
			params.getTarkastus().get("pesa_id").setValue("1");
			functionality.afterSuccessfulValidation();
			assertGivenOperationsAreCalledRestAreNotCalled();
		}

		public void test___update___there_are_changes_to_pesa() throws Exception {
			data.setAction(Const.UPDATE);
			params.getColumn("pesavakio.pesanimi").setValue("uusi nimi");
			params.getColumn("pesamuuttuva.havaintojen_vuosi").setValue("2008");
			params.getColumn("pesatarkastus.tark_vuosi").setValue("2008");
			params.getTarkastus().get("p_tarkastus_id").setValue("1");
			functionality.afterSuccessfulValidation();
			assertGivenOperationsAreCalledRestAreNotCalled(UPDATE_PESA);
		}

		public void test___update___there_are_changes_to_olosuhde_and_row_exists_for_tark_year() throws Exception {
			data.setAction(Const.UPDATE);
			params.getColumn("pesamuuttuva.puusto_ika").setValue("10");
			params.getColumn("pesamuuttuva.havaintojen_vuosi").setValue("2008");
			params.getColumn("pesatarkastus.tark_vuosi").setValue("2008");
			params.getTarkastus().get("p_tarkastus_id").setValue("1");
			functionality.afterSuccessfulValidation();
			assertGivenOperationsAreCalledRestAreNotCalled(UPDATE_OLOSUHDE);
		}

		public void test___update___there_are_changes_to_olosuhde_but_no_row_for_tark_year() throws Exception {
			data.setAction(Const.UPDATE);
			params.getColumn("pesamuuttuva.puusto_ika").setValue("10");
			params.getColumn("pesatarkastus.tark_vuosi").setValue("2009");
			params.getTarkastus().get("p_tarkastus_id").setValue("1");
			functionality.afterSuccessfulValidation();
			assertGivenOperationsAreCalledRestAreNotCalled(INSERT_OLOSUHDE, UPDATE_TARKASTUS);
		}

		public void test___update___there_are_changes_to_tarkastus() throws Exception {
			data.setAction(Const.UPDATE);
			params.getColumn("pesamuuttuva.havaintojen_vuosi").setValue("2008");
			params.getColumn("pesatarkastus.tark_vuosi").setValue("2008");
			params.getColumn("pesatarkastus.pesimistulos").setValue("R");
			params.getTarkastus().get("p_tarkastus_id").setValue("1");
			functionality.afterSuccessfulValidation();
			assertGivenOperationsAreCalledRestAreNotCalled(UPDATE_TARKASTUS);
		}

		public void test___update__poikaset___matches_original_values___nop() throws Exception {
			data.setAction(Const.UPDATE);
			params.getColumn("pesamuuttuva.havaintojen_vuosi").setValue("2008");
			params.getColumn("pesatarkastus.tark_vuosi").setValue("2008");
			params.getTarkastus().get("p_tarkastus_id").setValue("1");
			params.getColumn("poikanen.1.rowid").setValue("1");
			params.getColumn("poikanen.1.rengas_oikea").setValue("000");
			params.getColumn("poikanen.2.rowid").setValue("2");
			functionality.afterSuccessfulValidation();
			assertGivenOperationsAreCalledRestAreNotCalled();
		}

		public void test___update__poikaset___changes_compared_to_original_values___update() throws Exception {
			data.setAction(Const.UPDATE);
			params.getColumn("pesamuuttuva.havaintojen_vuosi").setValue("2008");
			params.getColumn("pesatarkastus.tark_vuosi").setValue("2008");
			params.getTarkastus().get("p_tarkastus_id").setValue("1");
			params.getColumn("poikanen.1.rowid").setValue("1");
			params.getColumn("poikanen.1.rengas_oikea").setValue("000");
			params.getColumn("poikanen.2.rowid").setValue("2");
			params.getColumn("poikanen.2.rengas_oikea").setValue("123");
			functionality.afterSuccessfulValidation();
			assertGivenOperationsAreCalledRestAreNotCalled(UPDATE_P2);
		}

		public void test___update__poikaset___new_row_given____insert() throws Exception {
			data.setAction(Const.UPDATE);
			params.getColumn("pesamuuttuva.havaintojen_vuosi").setValue("2008");
			params.getColumn("pesatarkastus.tark_vuosi").setValue("2008");
			params.getTarkastus().get("p_tarkastus_id").setValue("1");
			params.getColumn("poikanen.1.rowid").setValue("1");
			params.getColumn("poikanen.1.rengas_oikea").setValue("000");
			params.getColumn("poikanen.2.rowid").setValue("2");
			params.getColumn("poikanen.3.rengas_oikea").setValue("456");
			functionality.afterSuccessfulValidation();
			assertGivenOperationsAreCalledRestAreNotCalled(INSERT_P3);
		}

		public void test___update__aikuiset___new_row_given____insert() throws Exception {
			data.setAction(Const.UPDATE);
			params.getColumn("pesamuuttuva.havaintojen_vuosi").setValue("2008");
			params.getColumn("pesatarkastus.tark_vuosi").setValue("2008");
			params.getTarkastus().get("p_tarkastus_id").setValue("1");
			params.getColumn("aikuinen.1.rowid").setValue("1");
			params.getColumn("aikuinen.1.rengas_oikea").setValue("000");
			params.getColumn("aikuinen.2.rowid").setValue("2");
			params.getColumn("aikuinen.3.rengas_oikea").setValue("456");
			functionality.afterSuccessfulValidation();
			assertGivenOperationsAreCalledRestAreNotCalled(INSERT_A3);
		}

	}

}
