package fi.hy.pese.saaksi.main.app;

import fi.hy.pese.framework.main.app.ApiServiceFactory;
import fi.hy.pese.framework.main.app.ApiServlet;
import fi.hy.pese.framework.main.app._ApplicationSpecificationFactory;
import fi.hy.pese.framework.main.general.data.PesaDomainModelDatabaseStructure;
import fi.hy.pese.framework.main.general.data.PesaDomainModelRowFactory;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.luomus.commons.db.connectivity.PreparedStatementStoringAndClosingTransactionConnection;
import fi.luomus.commons.db.connectivity.TransactionConnection;

public class SaaksiApiServlet extends ApiServlet<_PesaDomainModelRow> {
	
	private static final long	serialVersionUID	= -1898038058767299318L;
	
	@Override
	protected _ApplicationSpecificationFactory<_PesaDomainModelRow> createFactory() throws Exception {
		ApiServiceFactory<_PesaDomainModelRow> factory = new ApiServiceFactory<>("pese_saaksi.config");
		
		TransactionConnection con = new PreparedStatementStoringAndClosingTransactionConnection(factory.config().connectionDescription());
		try {
			PesaDomainModelDatabaseStructure structure = SaaksiDatabaseStructureCreator.loadFromDatabase(con);
			factory.setRowFactory(new PesaDomainModelRowFactory(structure, SaaksiConst.JOINS, SaaksiConst.ORDER_BY));
		} finally {
			con.release();
		}
		
		factory.loadUITexts();
		
		return factory;
	}
	
}
