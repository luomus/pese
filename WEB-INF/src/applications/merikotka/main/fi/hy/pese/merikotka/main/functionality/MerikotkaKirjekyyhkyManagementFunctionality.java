package fi.hy.pese.merikotka.main.functionality;

import java.util.List;
import java.util.Map;

import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.app.functionality.KirjekyyhkyManagementFunctionality;
import fi.hy.pese.framework.main.general.consts.Kirjekyyhky;
import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.main.general.data._Table;
import fi.hy.pese.merikotka.main.app.MerikotkaConst;
import fi.luomus.commons.kirjekyyhky.FormData;
import fi.luomus.commons.kirjekyyhky.KirjekyyhkyAPIClient;
import fi.luomus.commons.utils.Utils;

public class MerikotkaKirjekyyhkyManagementFunctionality extends KirjekyyhkyManagementFunctionality {

	public MerikotkaKirjekyyhkyManagementFunctionality(_Request<_PesaDomainModelRow> request) {
		super(request);
	}

	@Override
	protected void applicationSpecificConversions(Map<String, String> formData, _PesaDomainModelRow kirjekyyhkyparameters) {
		MerikotkaConst.kirjekyyhkyModelToData(formData, kirjekyyhkyparameters);
		data.set("tark_vuosi", kirjekyyhkyparameters.getTarkastus().get("tark_vuosi").getValue());
		for (String field : MerikotkaConst.ETAISYYS_FIELDS) {
			_Column c = kirjekyyhkyparameters.getColumn(field);
			String value = Utils.removeWhitespace(c.getValue());
			value = value.replace("&gt;", ">");
			if (value.equals(">1000")) {
				c.setValue("9999");
			}
		}
	}

	@Override
	protected void applicationSpecificReturnedEmptyProcedure(_PesaDomainModelRow kirjekyyhkyParameters) {
		_Table tark = kirjekyyhkyParameters.getTarkastus();
		tark.get("pesimistulos").setValue("U");
		tark.get("pesimist_tark").setValue("U");
		tark.get("nahdyt_merkit").setValue("U");
		tark.get("pesa_kunto").setValue("J");
		tark.get("tark_tapa").setValue("8");
		tark.get("tark_pvm").setValue("1.1." + tark.get("tark_vuosi").getValue());
		tark.get("tark_pvm_tark").setValue("4");
	}

	@Override
	protected void getListOfForms(KirjekyyhkyAPIClient api) throws Exception {
		super.getListOfForms(api);
		if (!data.getLists().containsKey(Kirjekyyhky.FORMS)) return;
		for (Object o : data.getLists().get(Kirjekyyhky.FORMS)) {
			if (o instanceof FormData) {
				FormData formData = (FormData) o;
				String pesaid = formData.getData().get("pesavakio.pesa_id");
				if (given(pesaid)) {
					addReviiriid(pesaid, formData);
				}
			}
		}
	}

	private void addReviiriid(String pesaid, FormData formData) throws Exception {
		_Table searchparams = dao.newTableByName("vuosi");
		searchparams.get("pesa_id").setValue(pesaid);
		List<_Table> results = dao.returnTables(searchparams, "vuosi DESC");
		if (results.size() > 0) {
			String reviiriid = results.get(0).get("reviiri_id").getValue();
			formData.getData().put("reviiri_id", reviiriid);
			String reviirinimi = dao.returnTableByKeys("reviiri", reviiriid).get("reviirinimi").getValue();
			if (!reviirinimi.equals(formData.getData().get("reviirinimi"))) {
				formData.getData().put("reviirinimi", "Uusi: " + formData.getData().get("reviirinimi") + ", Vanha: " + reviirinimi);
			}
		}
	}

	private boolean given(String reviirinimi) {
		return reviirinimi != null && reviirinimi.length() > 0;
	}

	@Override
	protected void applicationSpecificComparisonDataOperations(_PesaDomainModelRow kirjekyyhkyParameters, _PesaDomainModelRow comparisonParameters) {
		kirjekyyhkyParameters.getPesa().get("helcom").setValue(comparisonParameters.getPesa().get("helcom").getValue());
	}

}
