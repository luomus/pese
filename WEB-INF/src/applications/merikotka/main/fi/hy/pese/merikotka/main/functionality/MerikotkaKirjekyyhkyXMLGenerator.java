package fi.hy.pese.merikotka.main.functionality;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.db.SingleValueResultHandler;
import fi.hy.pese.framework.main.db._DAO;
import fi.hy.pese.framework.main.general.Utils;
import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.main.general.data._Row;
import fi.hy.pese.framework.main.general.data._Table;
import fi.hy.pese.framework.main.kirjekyyhky.KirjekyyhkyXMLGeneratorUtility;
import fi.hy.pese.framework.main.kirjekyyhky.KirjekyyhkyXMLGeneratorUtility.StructureDefinition;
import fi.hy.pese.framework.main.kirjekyyhky._KirjekyyhkyXMLGenerator;
import fi.hy.pese.merikotka.main.app.MerikotkaConst;
import fi.luomus.commons.config.Config;
import fi.luomus.commons.tipuapi.TipuAPIClient;
import fi.luomus.commons.utils.FileUtils;

public class MerikotkaKirjekyyhkyXMLGenerator implements _KirjekyyhkyXMLGenerator {

	private static final int	AIKUISET_COUNT	= 3;
	private static final int	POIKASET_COUNT	= 3;
	private static final String[]	AIKUINEN_COLUMNS_TO_EXCLUDE	= { "aikuinen_id", "tarkastus_id" };
	private static final String[]	POIKANEN_COLUMNS_TO_EXCLUDE	= { "poikanen_id", "tarkastus_id", "poikasen_ika", "poikasen_ika_2" };
	private final _Request<_PesaDomainModelRow> request;
	private final Config config;
	private final _DAO<_PesaDomainModelRow> dao;
	private final KirjekyyhkyXMLGeneratorUtility generator;
	private final List<String> producedDataXMLFiles = new LinkedList<>();

	public MerikotkaKirjekyyhkyXMLGenerator(_Request<_PesaDomainModelRow> request) throws Exception {
		this.request = request;
		this.config = request.config();
		this.dao = request.dao();
		this.generator = initGenerator();
	}

	@Override
	public File writeFormDataXML(Map<String, String> data, String filename) throws IllegalArgumentException {
		filename += ".xml";
		List<String> owners = new ArrayList<>();

		if (data.containsKey("pesa.seur_tarkastaja")) {
			owners.add(data.get("pesa.seur_tarkastaja"));
		}

		String vastuuhenkilo = getSuuralueVastuuhenkilo(data);
		if (!owners.contains(vastuuhenkilo)) {
			owners.add(vastuuhenkilo);
		}

		for (String lomakeVastaanottaja : getLomakeVastaanottajat(data)) {
			if (!owners.contains(lomakeVastaanottaja)) {
				owners.add(lomakeVastaanottaja);
			}
		}

		Map<String, String> xmlData = new HashMap<>();
		xmlData.put("koord_leveys", data.get("pesa.koord_leveys"));
		xmlData.put("koord_pituus", data.get("pesa.koord_pituus"));
		xmlData.put("reviirinimi", data.get("reviiri.reviirinimi"));
		xmlData.put("kunta", data.get("vuosi.reviirin_kunta"));
		for (Entry<String, String> e : data.entrySet()) {
			if (replace("pesa.", "pesavakio.", xmlData, e)) continue;
			if (replace("olosuhde.", "pesamuuttuva.", xmlData, e)) continue;
			if (replace("tarkastus.", "pesatarkastus.", xmlData, e)) continue;
		}

		xmlData.put("pesatarkastus.tark_vuosi", data.get("tark_vuosi"));
		xmlData.put("pesatarkastus.tarkastaja1_id", owners.get(0));
		//xmlData.put("pesatarkastus.tarkastaja2_id", owners.get(1));
		xmlData.put("pesatarkastus.tarkastaja2_id", "");

		File file = new File(config.kirjekyyhkyFolder(), filename);
		try {
			String xml = generator.generateDataXML(xmlData, owners);
			FileUtils.writeToFile(file, xml);
			producedDataXMLFiles.add(filename);
		} catch (Exception e) {
			e.printStackTrace();
			request.data().addToNoticeTexts("VIRHE: " + e.getMessage() + " " + e.getStackTrace()[0]);
			return null;
		}
		return file;
	}

	private List<String> getLomakeVastaanottajat(Map<String, String> data) {
		String pesaid = data.get("pesa.pesa_id");
		_Table searchParams = dao.newRow().getLomakeVastaanottajat();
		searchParams.get("pesa").setValue(pesaid);
		List<String> vastaanottajat = new ArrayList<>();
		try {
			for (_Table t : dao.returnTables(searchParams)) {
				vastaanottajat.add(t.get("vastaanottaja").getValue());
			}
		} catch (SQLException e) {
			throw new RuntimeException("Lomake vastaanottajat pesälle " + pesaid);
		}
		return vastaanottajat;
	}

	private String getSuuralueVastuuhenkilo(Map<String, String> data) {
		String kunta = data.get("vuosi.pesan_kunta");
		String suurAlue = request.getTipuApiResource(TipuAPIClient.MUNICIPALITIES).getById(kunta).getNode("merikotka-suuralue").getContents();
		SingleValueResultHandler resultHandler = new SingleValueResultHandler();
		try {
			dao.executeSearch("SELECT vastuuhenkilo FROM suuralue WHERE ID = ?", resultHandler, suurAlue);
			return resultHandler.value;
		} catch (SQLException e) {
			throw new RuntimeException("VIRHE HAETTAESSA SUURALUEEN '" + suurAlue + "' VASTUUHENKILÖÄ", e);
		}
	}

	private boolean replace(String originalTableName, String newTableName, Map<String, String> xmlData, Entry<String, String> e) {
		if (e.getKey().startsWith(originalTableName)) {
			String key = newTableName + e.getKey().substring(originalTableName.length());
			xmlData.put(key, e.getValue());
			return true;
		}
		return false;
	}

	@Override
	public List<String> producedFormDataXMLFiles() {
		return producedDataXMLFiles;
	}

	public void writeStructureXML() throws Exception {
		String xml = generator.generateStructureXML();
		File file = new File(config.reportFolder(), "merikotka.xml");
		try {
			FileUtils.writeToFile(file, xml, request.config().characterEncoding());
		} catch (Exception e) {
			request.data().addToNoticeTexts("VIRHE: " + e.getMessage() + " " + e.getStackTrace()[0]);
		}
	}

	private KirjekyyhkyXMLGeneratorUtility initGenerator() throws FileNotFoundException, IOException {
		KirjekyyhkyXMLGeneratorUtility generator = new KirjekyyhkyXMLGeneratorUtility();
		StructureDefinition definition = generator.getDefinition();

		definition.typeID = "merikotka";
		definition.title = "Merikotkatutkimus";

		definition.menutexts.newForm = "Ilmoita uusi pesä";
		definition.menutexts.printEmptyPdf = "Tulosta tyhjä lomake";
		definition.menutexts.printInstructions = "Tulosta täyttöohje";

		definition.titleFields.add("pesatarkastus.tark_vuosi");
		definition.titleFields.add("kunta");
		definition.titleFields.add("reviirinimi");
		definition.titleFields.add("pesavakio.pesanimi");
		definition.titleFields.add("pesavakio.pesa_id");
		definition.titleFields.add("pesatarkastus.tarkastaja1_id");

		definition.validation.URI = config.get("ValidatonServiceURL");
		definition.validation.username = config.get("ValidatonServiceUsername");
		definition.validation.password = config.get("ValidatonServicePassword");

		definition.coordinateFields.leveys = "koord_leveys";
		definition.coordinateFields.pituus = "koord_pituus";
		definition.coordinateFields.tyyppi = "pesavakio.koord_tyyppi";
		definition.coordinateFields.mittaustapa = "pesavakio.koord_mittaus";
		definition.coordinateFields.setTitleFields("reviirinimi", "pesavakio.pesanimi", "pesavakio.pesa_id");

		_Row row = dao.newRow();
		definition.row = row;
		generator.excludeColumn("pesavakio", "yht_leveys");
		generator.excludeColumn("pesavakio", "yht_pituus");
		generator.excludeColumn("pesavakio", "ast_leveys");
		generator.excludeColumn("pesavakio", "ast_pituus");
		generator.excludeColumn("pesavakio", "des_leveys");
		generator.excludeColumn("pesavakio", "des_pituus");
		generator.excludeColumn("pesavakio", "eur_leveys");
		generator.excludeColumn("pesavakio", "eur_pituus");
		generator.excludeColumn("pesavakio", "tuhoutumisvuosi");
		generator.excludeColumn("pesavakio", "helcom");
		generator.excludeColumn("pesamuuttuva", "pesamuuttuva_id");
		generator.excludeColumn("pesamuuttuva", "pesa_id");
		generator.excludeColumn("pesamuuttuva", "havaintojen_vuosi");
		generator.excludeColumn("pesatarkastus", "p_tarkastus_id");
		generator.excludeColumn("pesatarkastus", "pesa_id");
		generator.excludeColumn("pesatarkastus", "kuoriutumispaiva");

		for (int i = 1; i <= POIKASET_COUNT; i++) {
			for (String c : POIKANEN_COLUMNS_TO_EXCLUDE) {
				generator.excludeColumn("poikanen."+i, c);
			}
		}

		for (int i = 1; i <= AIKUISET_COUNT; i++) {
			for (String c : AIKUINEN_COLUMNS_TO_EXCLUDE) {
				generator.excludeColumn("aikuinen."+i, c);
			}
		}

		generator.addCustomColumn("koord_leveys", _Column.INTEGER, 7);
		generator.addCustomColumn("koord_pituus", _Column.INTEGER, 7);
		generator.addCustomColumn("reviirinimi", _Column.VARCHAR, 25);
		generator.addCustomColumn("kunta", _Column.VARCHAR, TipuAPIClient.MUNICIPALITIES);
		generator.addCustomColumn("oliko_reviiri_uusi", _Column.VARCHAR, MerikotkaConst.YES_NO_SELECTION);
		generator.addCustomColumn("uusi_reviiri_pesat", _Column.VARCHAR, 250);

		for (String field : MerikotkaConst.ETAISYYS_FIELDS) {
			String table = Utils.splitColumnFullname(field)[0];
			String column = Utils.splitColumnFullname(field)[1];
			generator.excludeColumn(table, column);
			generator.addCustomColumn(field, _Column.VARCHAR, 5);
		}

		generator.includeTable("pesavakio");
		generator.includeTable("pesamuuttuva");
		generator.includeTable("pesatarkastus");
		generator.includeTable("poikanen.1");
		generator.includeTable("poikanen.2");
		generator.includeTable("poikanen.3");
		generator.includeTable("aikuinen.1");
		generator.includeTable("aikuinen.2");
		generator.includeTable("aikuinen.3");

		definition.selections = request.data().selections();

		generator.excludeSelection(MerikotkaConst.REVIIRIT);
		generator.excludeSelection(TipuAPIClient.SPECIES);
		generator.excludeSelection(TipuAPIClient.MUNICIPALITIES);
		generator.excludeSelection(TipuAPIClient.RINGERS);
		generator.excludeSelection(MerikotkaConst.TAULUT);
		generator.defineTipuApiSelection("pesavakio", "seur_tarkastaja", TipuAPIClient.RINGERS);
		generator.defineTipuApiSelection("pesatarkastus", "tarkastaja1_id", TipuAPIClient.RINGERS);
		generator.defineTipuApiSelection("pesatarkastus", "tarkastaja2_id", TipuAPIClient.RINGERS);
		generator.defineTipuApiSelection("pesatarkastus", "kuvaaja_lintu", TipuAPIClient.RINGERS);
		generator.defineTipuApiSelection("pesatarkastus", "kuvaaja_pesa", TipuAPIClient.RINGERS);
		generator.defineTipuApiSelection("pesatarkastus", "mittaaja_id_1", TipuAPIClient.RINGERS);
		generator.defineTipuApiSelection("pesatarkastus", "mittaaja_id_2", TipuAPIClient.RINGERS);
		generator.defineTipuApiSelection("kunta", TipuAPIClient.MUNICIPALITIES);
		generator.defineTipuApiSelection("pesavakio", "rak_laji", TipuAPIClient.SPECIES);
		generator.defineTipuApiSelection("pesatarkastus", "muulaji", TipuAPIClient.SPECIES);
		generator.defineTipuApiSelection("pesatarkastus", "muulaji_rinnakkainen", TipuAPIClient.SPECIES);
		generator.noCodesForSelection(MerikotkaConst.YES_NO_SELECTION);

		generator.userAsDefaultValue("pesavakio.seur_tarkastaja");
		generator.userAsDefaultValue("pesatarkastus.tarkastaja1_id");
		// generator.userAsDefaultValue("pesatarkastus.mittaaja_id_1");

		generator.setCustomFieldRequired("kunta");
		generator.setCustomFieldRequired("koord_leveys");
		generator.setCustomFieldRequired("koord_pituus");

		Map<String, String> fieldTitles = new HashMap<>();
		for (Entry<String, String> e : request.data().uiTexts().entrySet()) {
			if (e.getKey().startsWith("tarkastuslomake.")) {
				// if (e.getKey().startsWith("tarkastuslomake.aikuinen.")) continue;
				fieldTitles.put(e.getKey().substring("tarkastuslomake.".length()), e.getValue());
			} else {
				if (!fieldTitles.containsKey(e.getKey())) {
					fieldTitles.put(e.getKey(), e.getValue());
				}
			}
		}
		definition.fieldTitles = fieldTitles;

		definition.layout = FileUtils.readContents(new File(config.templateFolder(), "layout.xml"));

		definition.emptyPDF = new File(config.templateFolder(), "merikotkatutkimus-uusi-pesailmoitus.pdf");
		definition.instructionsPDF = new File(config.templateFolder(), "merikotkatutkimus-ohjeet.pdf");
		return generator;
	}

}
