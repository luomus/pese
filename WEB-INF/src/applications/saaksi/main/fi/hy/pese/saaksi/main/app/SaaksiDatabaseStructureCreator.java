package fi.hy.pese.saaksi.main.app;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import fi.hy.pese.framework.main.db.oraclesql.Queries;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data.DatabaseColumnStructure;
import fi.hy.pese.framework.main.general.data.DatabaseTableStructure;
import fi.hy.pese.framework.main.general.data.PesaDomainModelDatabaseStructure;
import fi.hy.pese.framework.main.general.data._Table;
import fi.luomus.commons.containers.Selection;
import fi.luomus.commons.db.connectivity.TransactionConnection;
import fi.luomus.commons.tipuapi.TipuAPIClient;

public class SaaksiDatabaseStructureCreator {

	public static PesaDomainModelDatabaseStructure loadFromDatabase(TransactionConnection con) throws SQLException {

		PesaDomainModelDatabaseStructure structure = new PesaDomainModelDatabaseStructure();

		// Taulut
		DatabaseTableStructure pesa = Queries.returnTableStructure("pesa", con);
		DatabaseTableStructure olosuhde = Queries.returnTableStructure("olosuhde", con);
		DatabaseTableStructure tarkastus = Queries.returnTableStructure("tarkastus", con);
		DatabaseTableStructure kaynti = Queries.returnTableStructure("kaynti", con);
		DatabaseTableStructure poikanen = Queries.returnTableStructure("poikanen", con);
		DatabaseTableStructure vuosi = Queries.returnTableStructure("vuosi", con);
		DatabaseTableStructure reviiri = Queries.returnTableStructure("reviiri", con);
		//DatabaseTableStructure searches = Queries.returnTableStructure("saved_searches", con);
		DatabaseTableStructure aputaulu = Queries.returnTableStructure("aputiedot", con);
		DatabaseTableStructure kalanviljelylaitos = Queries.returnTableStructure("kalanviljelylaitos", con);
		DatabaseTableStructure muutosloki = Queries.returnTableStructure("muutosloki", con);
		DatabaseTableStructure lomakeVastaanottajat = Queries.returnTableStructure("lomake_vastaanottaja", con);

		// N kpl sisältävien staattinen lukumäärä (listauksiin yms)
		structure.setKaynnitCount(4);
		structure.setPoikasetCount(4);

		// Taulut templateen
		structure.setPesa(pesa);
		structure.setOlosuhde(olosuhde);
		structure.setTarkastus(tarkastus);
		structure.setKaynti(kaynti);
		structure.setPoikanen(poikanen);
		structure.setVuosi(vuosi);
		structure.setReviiri(reviiri);
		//structure.setSavedSearches(searches);
		structure.setAputaulu(aputaulu);
		structure.defineCustomTable(kalanviljelylaitos);
		structure.setLogTable(muutosloki);
		structure.setLomakeVastaanottajat(lomakeVastaanottajat);
		structure.setTransactionEntry(Const.TRANSACTION_ENTRY_TABLE_STRUCTURE);

		// Avaimet
		pesa.get("pesaid").setTypeToUniqueNumericIncreasingId();
		olosuhde.get("olosuhdeid").setTypeToUniqueNumericIncreasingId();
		tarkastus.get("tarkastusid").setTypeToUniqueNumericIncreasingId();
		kaynti.get("id").setTypeToUniqueNumericIncreasingId();
		poikanen.get("poikanenid").setTypeToUniqueNumericIncreasingId();
		vuosi.get("pesaid").setTypeToImmutablePartOfAKey();
		vuosi.get("vuosi").setTypeToImmutablePartOfAKey();
		reviiri.get("reviiriid").setTypeToUniqueNumericIncreasingId();
		aputaulu.get("taulu").setTypeToImmutablePartOfAKey();
		aputaulu.get("kentta").setTypeToImmutablePartOfAKey();
		aputaulu.get("arvo").setTypeToImmutablePartOfAKey();
		kalanviljelylaitos.get("id").setTypeToUniqueNumericIncreasingId();
		//searches.get("id").setTypeToUniqueNumericIncreasingId();
		muutosloki.get("id").setTypeToUniqueNumericIncreasingId();
		lomakeVastaanottajat.get("pesa").setTypeToImmutablePartOfAKey();
		lomakeVastaanottajat.get("vastaanottaja").setTypeToImmutablePartOfAKey();

		kaynti.get("kirj_pvm").setTypeToDateAddedColumn();
		kaynti.get("muutos_pvm").setTypeToDateModifiedColumn();
		kalanviljelylaitos.get("kirj_pvm").setTypeToDateAddedColumn();
		kalanviljelylaitos.get("muutos_pvm").setTypeToDateModifiedColumn();
		muutosloki.get("kirj_pvm").setTypeToDateAddedColumn();


		// Viiteavaimet... Ei kattava lista (toistaiseksi), vain ne joita tarvitaan
		vuosi.get("reviiriid").references(reviiri.getUniqueNumericIdColumn());
		tarkastus.get("pesaid").references(pesa.getUniqueNumericIdColumn());
		kaynti.get("tarkastus").references(tarkastus.getUniqueNumericIdColumn());

		// Sekalaiset
		muutosloki.get("kayttaja").setTypeToUserIdColumn();

		// Tauluhallintaoikeudet
		aputaulu.setTablemanagementPermissions(_Table.TABLEMANAGEMENT_UPDATE_INSERT_DELETE);
		reviiri.setTablemanagementPermissions(_Table.TABLEMANAGEMENT_UPDATE_INSERT_DELETE);
		kalanviljelylaitos.setTablemanagementPermissions(_Table.TABLEMANAGEMENT_UPDATE_INSERT_DELETE);
		muutosloki.setTablemanagementPermissions(_Table.TABLEMANAGEMENT_INSERT_ONLY);

		// Kentät jotka eivät ole käytössä
		reviiri.removeColumn("numero");
		reviiri.removeColumn("luokka");
		reviiri.removeColumn("virhe");
		vuosi.removeColumn("pesapuu");
		vuosi.removeColumn("kommentti");
		pesa.removeColumn("luokka");
		pesa.removeColumn("virhe");
		pesa.removeColumn("koord_tarkkuus");
		pesa.removeColumn("km_pituus");
		pesa.removeColumn("km_leveys");
		olosuhde.removeColumn("luokka");
		olosuhde.removeColumn("virhe");
		tarkastus.removeColumn("luokka");
		tarkastus.removeColumn("virhe");
		tarkastus.removeColumn("fenologia");
		tarkastus.removeColumn("valokuva");
		tarkastus.removeColumn("myrkky");
		tarkastus.removeColumn("saalistus");
		tarkastus.removeColumn("historia");
		poikanen.removeColumn("luokka");
		poikanen.removeColumn("virhe");
		poikanen.removeColumn("siipi_mittaus_menet");
		olosuhde.removeColumn("i_maastotyyppi");
		olosuhde.removeColumn("i_puusto");
		olosuhde.removeColumn("i_puust_kasaste");
		olosuhde.removeColumn("i_puust_tiheys");
		olosuhde.removeColumn("i_pesan_sijainti");
		olosuhde.removeColumn("i_pesap_elavyys");
		olosuhde.removeColumn("i_pesap_korkeus");
		olosuhde.removeColumn("i_pesap_tyvihalk");
		olosuhde.removeColumn("i_pesap_tyviymp");
		olosuhde.removeColumn("i_pesap_latvahalk");
		olosuhde.removeColumn("i_pesap_latvaymp");
		olosuhde.removeColumn("i_rauhoitustaulu");

		// Validointisääntöjä
		aputaulu.get("selite_ruotsi").setToOptionalColumn();
		aputaulu.get("ryhmittely").setToOptionalColumn();
		reviiri.get("kommentti").setToOptionalColumn();

		// Liitokset selecteihin
		// Ensin aputaulussa olevan kentät automaattisesti
		Map<String, Selection> selections = Queries.returnSelectionsFromCodeTable(con, "aputiedot", "taulu", "kentta", "arvo", "selite_suomi", "ryhmittely", new String[] {"jarjestys"});
		for (String name : selections.keySet()) {
			try {
				DatabaseColumnStructure column = structure.getColumn(name);
				column.setAssosiatedSelection(name);
			} catch (Exception e) {
				// System.out.println(" Assosiation "+name + " does not match a field.");
			}
		}
		// Loput liitokset manuaalisesti
		reviiri.get("hallintolaani").setAssosiatedSelection("pesa.laani");
		reviiri.get("hallintokunta").setAssosiatedSelection(TipuAPIClient.MUNICIPALITIES);
		pesa.get("seur_rengastaja").setAssosiatedSelection(TipuAPIClient.RINGERS);
		pesa.get("kunta").setAssosiatedSelection(TipuAPIClient.MUNICIPALITIES);
		tarkastus.get("rengastaja").setAssosiatedSelection(TipuAPIClient.RINGERS);
		tarkastus.get("muulaji").setAssosiatedSelection(TipuAPIClient.SPECIES);
		tarkastus.get("nayte_munia").setAssosiatedSelection(SaaksiConst.K_E);
		tarkastus.get("nayte_kuolleita").setAssosiatedSelection(SaaksiConst.K_E);
		tarkastus.get("nayte_sulkia").setAssosiatedSelection(SaaksiConst.K_E);
		kaynti.get("aikuisia").setAssosiatedSelection("tarkastus.aikuisia");
		kaynti.get("munia").setAssosiatedSelection("tarkastus.munia");
		kaynti.get("elavia").setAssosiatedSelection("tarkastus.elavia");
		kaynti.get("kuolleita").setAssosiatedSelection("tarkastus.kuolleita");
		kaynti.get("lentopoik").setAssosiatedSelection("tarkastus.lentopoik");
		aputaulu.get("taulu").setAssosiatedSelection(SaaksiConst.TAULUT);
		pesa.get("lomake").setAssosiatedSelection(SaaksiConst.K_E);
		olosuhde.get("rauhoitustaulu").setAssosiatedSelection(SaaksiConst.K_E);
		vuosi.get("reviiriid").setAssosiatedSelection(SaaksiConst.REVIIRIT);

		// Kirjekyyhky pakolliset
		pesa.get("kunta").setKirjekyyhkyRequired();
		pesa.get("koord_tyyppi").setKirjekyyhkyRequired();
		pesa.get("lomake").setKirjekyyhkyRequired();
		pesa.get("seur_rengastaja").setKirjekyyhkyRequired();
		olosuhde.get("pesan_sijainti").setKirjekyyhkyRequired();
		tarkastus.get("rengastaja").setKirjekyyhkyRequired();
		tarkastus.get("vuosi").setKirjekyyhkyRequired();
		tarkastus.get("tark_pvm").setKirjekyyhkyRequired();
		tarkastus.get("tark_pvm_tark").setKirjekyyhkyRequired();
		olosuhde.get("rauhoitustaulu").setKirjekyyhkyRequired();
		pesa.get("koord_mittaustapa").setKirjekyyhkyRequired();
		tarkastus.get("pesimistulos").setKirjekyyhkyRequired();
		tarkastus.get("pesimist_tark").setKirjekyyhkyRequired();
		tarkastus.get("kunto").setKirjekyyhkyRequired();

		// Muokataan kenttien järjestyksiä
		List<String> orderOfAputauluColumns = new ArrayList<>();
		orderOfAputauluColumns.add("taulu");
		orderOfAputauluColumns.add("kentta");
		orderOfAputauluColumns.add("jarjestys");
		orderOfAputauluColumns.add("arvo");
		orderOfAputauluColumns.add("selite_suomi");
		orderOfAputauluColumns.add("selite_ruotsi");
		aputaulu.reorder(orderOfAputauluColumns);


		List<String> orderOfPesaColumns = new ArrayList<>();
		orderOfPesaColumns.add("pesaid");
		orderOfPesaColumns.add("pesanimi");
		orderOfPesaColumns.add("laani");
		orderOfPesaColumns.add("kunta");
		orderOfPesaColumns.add("kyla");
		orderOfPesaColumns.add("yht_leveys");
		orderOfPesaColumns.add("yht_pituus");
		orderOfPesaColumns.add("eur_leveys");
		orderOfPesaColumns.add("eur_pituus");
		orderOfPesaColumns.add("ast_leveys");
		orderOfPesaColumns.add("ast_pituus");
		orderOfPesaColumns.add("des_leveys");
		orderOfPesaColumns.add("des_pituus");
		orderOfPesaColumns.add("koord_tyyppi");
		orderOfPesaColumns.add("koord_mittaustapa");
		orderOfPesaColumns.add("pesap_laji");
		orderOfPesaColumns.add("vanha_pesaid");
		orderOfPesaColumns.add("seur_rengastaja");
		orderOfPesaColumns.add("kommentti");
		pesa.reorder(orderOfPesaColumns);

		List<String> orderOfOlosuhdeColumns = new ArrayList<>();
		for (DatabaseColumnStructure c : olosuhde.getColumns()) {
			String name = c.getName();
			if (!name.equals("olosuhdeid") && !name.equals("pesaid")) {
				orderOfOlosuhdeColumns.add(name);
			}
		}
		olosuhde.reorder(orderOfOlosuhdeColumns);

		List<String> orderOfTarkastusColumns = new ArrayList<>();
		orderOfTarkastusColumns.add("vuosi");
		orderOfTarkastusColumns.add("tark_pvm");
		orderOfTarkastusColumns.add("tark_pvm_tark");
		orderOfTarkastusColumns.add("rengastaja");
		orderOfTarkastusColumns.add("pesimistulos");
		orderOfTarkastusColumns.add("pesimist_tark");
		orderOfTarkastusColumns.add("epaonn");
		orderOfTarkastusColumns.add("epaonn_tark");
		orderOfTarkastusColumns.add("kunto");
		orderOfTarkastusColumns.add("vaihtop_laiminl");
		orderOfTarkastusColumns.add("aik_aikuisia");
		orderOfTarkastusColumns.add("aikuisia");
		orderOfTarkastusColumns.add("aik_munia");
		orderOfTarkastusColumns.add("munia");
		orderOfTarkastusColumns.add("aik_elavia");
		orderOfTarkastusColumns.add("elavia");
		orderOfTarkastusColumns.add("kuolleita");
		orderOfTarkastusColumns.add("lop_kuolleita");
		orderOfTarkastusColumns.add("lentopoik");
		orderOfTarkastusColumns.add("lop_lentopoik");
		orderOfTarkastusColumns.add("tarkkuus");
		orderOfTarkastusColumns.add("muulaji");
		//orderOfTarkastusColumns.add("myrkky");
		orderOfTarkastusColumns.add("nayte_munia");
		orderOfTarkastusColumns.add("nayte_kuolleita");
		orderOfTarkastusColumns.add("nayte_sulkia");
		orderOfTarkastusColumns.add("kommentti");
		tarkastus.reorder(orderOfTarkastusColumns);

		List<String> orderOfPoikanenColumns = new ArrayList<>();
		orderOfPoikanenColumns.add("rengas");
		orderOfPoikanenColumns.add("siipi");
		orderOfPoikanenColumns.add("paino");
		orderOfPoikanenColumns.add("pvm");
		orderOfPoikanenColumns.add("kommentti");
		poikanen.reorder(orderOfPoikanenColumns);

		List<String> orderOfKayntiColumns = new ArrayList<>();
		orderOfKayntiColumns.add("pvm");
		orderOfKayntiColumns.add("aikuisia");
		orderOfKayntiColumns.add("munia");
		orderOfKayntiColumns.add("elavia");
		orderOfKayntiColumns.add("kuolleita");
		orderOfKayntiColumns.add("lentopoik");
		orderOfKayntiColumns.add("kommentti");
		kaynti.reorder(orderOfKayntiColumns);

		//		for (DatabaseTableStructure t : structure) {
		//			for (DatabaseColumnStructure c : t.getColumns()) {
		//				System.out.println(c.getFullname() + " = ");
		//			}
		//		}
		//		for (DatabaseTableStructure t : structure) {
		//			for (DatabaseColumnStructure c : t.getColumns()) {
		//				System.out.println("short."+c.getFullname() + " = ");
		//			}
		//		}

		return structure;
	}

	// ---------------------------------- ---------------------------------- ---------------------------------- ---------------------------------- ----------------------------------

	public static PesaDomainModelDatabaseStructure hardCoded() {
		PesaDomainModelDatabaseStructure structure = new PesaDomainModelDatabaseStructure();

		// TODO
		return structure;
	}

	/*
	 * public void generateJavacodeFromDatabaseGeneratedDatabaseStructure() {
	 * _Row params = data.updateparameters();
	 * for (_Table t : params) {
	 * String tablename = t.getTablename();
	 * System.out.println("DatabaseTableStructure "+t.getTablename()+" = new DatabaseTableStructure(\""+t.getTablename()+"\");");
	 * Iterator<_Column> iterator = t.getColumns();
	 * while (iterator.hasNext()) {
	 * _Column c = iterator.next();
	 * if (c.getName().equals(Const.ROWID)) continue;
	 * if (c.getDatatype() == _Column.VARCHAR)
	 * System.out.println(tablename+".addColumn(\""+c.getName()+"\", "+c.getDatatype()+", "+c.getSize()+");");
	 * else if (c.getDatatype() == _Column.DATE)
	 * System.out.println(tablename+".addColumn(\""+c.getName()+"\", "+c.getDatatype()+");");
	 * else if (c.getDatatype() == _Column.INTEGER)
	 * System.out.println(tablename+".addColumn(\""+c.getName()+"\", "+c.getDatatype()+", "+c.getSize()+");");
	 * else if (c.getDatatype() == _Column.DECIMAL)
	 * System.out.println(tablename+".addColumn(\""+c.getName()+"\", "+c.getDatatype()+", "+c.getSize()+" , "+c.getDecimalSize()+");");
	 * }
	 * }
	 * for (_Table t : params) {
	 * String tablename = t.getTablename();
	 * Iterator<_Column> iterator = t.getColumns();
	 * while (iterator.hasNext()) {
	 * _Column c = iterator.next();
	 * if (!c.getAssosiatedSelection().equals("")) {
	 * System.out.println(tablename+".get(\""+c.getName()+"\").setAssosiatedSelection(\""+c.getAssosiatedSelection()+"\");");
	 * }
	 * }
	 * }
	 * }
	 */
}
