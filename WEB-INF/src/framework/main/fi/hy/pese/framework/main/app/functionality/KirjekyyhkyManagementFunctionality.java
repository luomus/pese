package fi.hy.pese.framework.main.app.functionality;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import fi.hy.pese.framework.main.app.FunctionalityExecutor;
import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.app.functionality.validate.Validator;
import fi.hy.pese.framework.main.app.functionality.validate._Validator;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.consts.Kirjekyyhky;
import fi.hy.pese.framework.main.general.data.ParameterMap;
import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.main.general.data._Table;
import fi.luomus.commons.containers.Selection;
import fi.luomus.commons.kirjekyyhky.FormData;
import fi.luomus.commons.kirjekyyhky.KirjekyyhkyAPIClient;
import fi.luomus.commons.tipuapi.TipuAPIClient;

public abstract class KirjekyyhkyManagementFunctionality extends BaseFunctionality<_PesaDomainModelRow> {

	private static final String	LOMAKE_VASTAANOTTAJAT	= "lomake_vastaanottajat";
	
	protected abstract void applicationSpecificConversions(Map<String, String> formData, _PesaDomainModelRow kirjekyyhkyparameters);

	protected abstract void  applicationSpecificReturnedEmptyProcedure(_PesaDomainModelRow kirjekyyhkyParameters);
	
	protected abstract void applicationSpecificComparisonDataOperations(_PesaDomainModelRow kirjekyyhkyParameters, _PesaDomainModelRow comparisonParameters);

	public KirjekyyhkyManagementFunctionality(_Request<_PesaDomainModelRow> request) {
		super(request);
	}

	@Override
	public _Validator validator() throws Exception {
		return new KirjekyyhkymanagementValidator(request);
	}

	public static class KirjekyyhkymanagementValidator extends Validator<_PesaDomainModelRow> {

		public KirjekyyhkymanagementValidator(_Request<_PesaDomainModelRow> request) {
			super(request);
		}

		@Override
		public void validate() throws Exception {
			if (data.action().equals("receive_to_other_nest")) {
				receiveToOtherNest();
			}
		}

		private void receiveToOtherNest() throws SQLException {
			_Table nestTable = data.newRow().getPesa();
			_Column nestIdColumn = nestTable.getUniqueNumericIdColumn();
			nestIdColumn.setValue(data.get("nestID"));
			checkNotNull(nestIdColumn);
			checkInteger(nestIdColumn);
			if (noErrors(nestIdColumn)) {
				if (!dao.checkValuesExists(nestTable)) {
					error(nestIdColumn, _Validator.DOES_NOT_EXIST);
				}
			}
		}
	}

	@Override
	public void afterFailedValidation() throws Exception {
		KirjekyyhkyAPIClient api = request.kirjekyyhkyAPI();
		try {
			getListOfForms(api);
		} finally {
			if (api != null) api.close();
		}
	}

	@Override
	public void afterSuccessfulValidation() throws Exception {
		KirjekyyhkyAPIClient api = request.kirjekyyhkyAPI();
		String id = data.get(Kirjekyyhky.ID);
		try {
			if (noAction()) {
				getListOfForms(api);
			} else if (data.action().equals(Kirjekyyhky.RECEIVE)) {
				startFormReceivingProcess(api, id);
			} else if (data.action().equals(Kirjekyyhky.RETURN_FOR_CORRECTIONS)) {
				returnForm(api, id);
				getListOfForms(api);
			} else if (data.action().equals("receive_to_other_nest")) {
				startFormReceivingProcess(api, id, data.get("nestID"));
			}
		} finally {
			if (api != null) api.close();
		}
	}

	private void returnForm(KirjekyyhkyAPIClient api, String id) throws Exception {
		String message = data.get(Kirjekyyhky.RETURN_FOR_CORRECTIONS_MESSAGE);
		message += " -" + data.userName();
		api.sendForCorrections(id, message);
	}

	private void startFormReceivingProcess(KirjekyyhkyAPIClient api, String id) throws Exception {
		startFormReceivingProcess(api, id, null);
	}

	private void startFormReceivingProcess(KirjekyyhkyAPIClient api, String id, String otherNestId) throws Exception {
		FormData form = api.get(id);
		Map<String, String> formData = new HashMap<>();
		for (Map.Entry<String, String> e : form.getData().entrySet()) {
			formData.put(Const.UPDATEPARAMETERS + "." + e.getKey(), e.getValue());
		}
		for (String ringerId : form.getOwners()) {
			String vastaanottaja = ringerId;
			Selection ringers = data.selections().get(TipuAPIClient.RINGERS);
			if (ringers.containsOption(vastaanottaja)) {
				vastaanottaja = ringers.get(vastaanottaja) + " (" + vastaanottaja + ")";
			}
			data.addToList(LOMAKE_VASTAANOTTAJAT, vastaanottaja);
		}
		for (FormData.LogEntry e : form.getLogEntries()) {
			data.addToList("log", e);
		}
		for (String message : form.getMessages()) {
			data.addToList("message", message);
		}
		ParameterMap params = new ParameterMap(formData);
		_PesaDomainModelRow kirjekyyhkyParameters = data.newRow(Const.UPDATEPARAMETERS);
		params.convertTabledata(kirjekyyhkyParameters);

		applicationSpecificConversions(formData, kirjekyyhkyParameters);
		if (form.returnedEmpty()) {
			applicationSpecificReturnedEmptyProcedure(kirjekyyhkyParameters);
		}

		for (Map.Entry<String, String> e : params.remainingParams().entrySet()) {
			request.data().set(e.getKey(), e.getValue());
		}

		_Column pesaid = kirjekyyhkyParameters.getPesa().getUniqueNumericIdColumn();
		if (otherNestId != null) {
			pesaid.setValue(otherNestId);
		}
		_PesaDomainModelRow comparisonParameters = loadCurrentDatabaseValues(pesaid);

		applicationSpecificComparisonDataOperations(kirjekyyhkyParameters, comparisonParameters);
		
		data.setComparisonparameters(comparisonParameters);
		data.setUpdateparameters(kirjekyyhkyParameters);

		copyRowIdsFromCurrentDatabaseValuesToNewUpdateparameters();

		data.setPage(Const.TARKASTUS_PAGE);
		data.set(Kirjekyyhky.KIRJEKYYHKY, Const.YES);
	}
	
	private void copyRowIdsFromCurrentDatabaseValuesToNewUpdateparameters() {
		data.updateparameters().getPesa().setRowidValue(data.comparisonparameters().getPesa().getRowidValue());
		if (data.updateparameters().getOlosuhde().isInitialized()) {
			data.updateparameters().getOlosuhde().setRowidValue(data.comparisonparameters().getOlosuhde().getRowidValue());
		}
		if (data.updateparameters().getVuosi().isInitialized()) {
			data.updateparameters().getVuosi().setRowidValue(data.comparisonparameters().getVuosi().getRowidValue());
		}
	}

	private _PesaDomainModelRow loadCurrentDatabaseValues(_Column pesaid) throws Exception {
		if (pesaid.hasValue()) {
			data.setAction(Const.FILL_FOR_INSERT);
			data.updateparameters().getPesa().getUniqueNumericIdColumn().setValue(pesaid);
		} else {
			data.setAction(Const.FILL_FOR_INSERT_FIRST);
		}

		data.setPage(Const.TARKASTUS_PAGE);
		data.set(Kirjekyyhky.KIRJEKYYHKY, Const.YES);
		FunctionalityExecutor.execute(request.functionalityFor(Const.TARKASTUS_PAGE));
		return data.updateparameters();
	}

	protected void getListOfForms(KirjekyyhkyAPIClient api) throws Exception {
		try {
			for (FormData form : api.getForms()) {
				data.addToList(Kirjekyyhky.FORMS, form);
			}
		} catch (IllegalStateException e) {
			data.addToNoticeTexts("Loading list of forms has failed");
		}
	}

	private boolean noAction() {
		return data.action().equals("");
	}

}
