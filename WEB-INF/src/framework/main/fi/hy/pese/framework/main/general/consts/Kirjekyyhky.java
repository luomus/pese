package fi.hy.pese.framework.main.general.consts;

public class Kirjekyyhky {

	public static final String	FORMS	= "forms";
	public static final String  KIRJEKYYHKY_COUNT						= "kirjekyyhky_count";
	public static final String	KIRJEKYYHKY = "kirjekyyhky";
	public static final String FIRST_SHOW = "first_show";
	public static final String	ID	= "id";
	public static final String 	SHOW									= "show";
	public static final String 	RECEIVE									= "receive";
	public static final String	RETURN_FOR_CORRECTIONS	= "return_for_corrections";
	public static final String	RETURN_FOR_CORRECTIONS_MESSAGE	= "return_for_corrections_message";

}
