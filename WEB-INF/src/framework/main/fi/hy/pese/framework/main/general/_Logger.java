package fi.hy.pese.framework.main.general;

import fi.hy.pese.framework.main.general.data._Table;

public interface _Logger {

	void commitMessages();

	void log(String message);

	void logInsert(_Table table);

	void logUpdate(_Table oldValues, _Table newValues);

	void logDelete(_Table table);

}
