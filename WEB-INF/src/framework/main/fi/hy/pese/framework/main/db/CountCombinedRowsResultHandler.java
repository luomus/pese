package fi.hy.pese.framework.main.db;

import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;

/**
 * This result handler can be used to check if the search produced any results
 */
public class CountCombinedRowsResultHandler extends RowCombiningResultHandler {

	public int numberOfRows = 0;

	public CountCombinedRowsResultHandler(_DAO<_PesaDomainModelRow> dao) {
		super(dao);
	}

	@Override
	protected boolean handle(_PesaDomainModelRow row) {
		numberOfRows++;
		return true;
	}

}
