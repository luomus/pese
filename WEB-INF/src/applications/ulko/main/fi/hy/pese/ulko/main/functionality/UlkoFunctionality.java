package fi.hy.pese.ulko.main.functionality;

import java.io.File;
import java.sql.SQLException;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;

import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.app.functionality.BaseFunctionality;
import fi.hy.pese.framework.main.app.functionality.FilemanagementFunctionality;
import fi.hy.pese.framework.main.app.functionality.validate.EmptyValidator;
import fi.hy.pese.framework.main.app.functionality.validate._Validator;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.consts.Success;
import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._Table;
import fi.hy.pese.ulko.main.app._UlkoDomainModelRow;
import fi.hy.pese.ulko.main.db.UlkoRowsToDataResultsHandler;
import fi.luomus.commons.config.Config;
import fi.luomus.commons.http.HttpClientService;
import fi.luomus.commons.tipuapi.TipuAPIClient;
import fi.luomus.commons.tipuapi.TipuAPIClientImple;
import fi.luomus.commons.utils.EmailUtil;
import fi.luomus.commons.utils.EmailUtil.Receivers;
import fi.luomus.commons.utils.URIBuilder;
import fi.luomus.commons.utils.Utils;
import fi.luomus.commons.xml.Document.Node;

public class UlkoFunctionality extends BaseFunctionality<_UlkoDomainModelRow> {
	
	private static final String	FROM_ADDRESS	= "ring@luomus.fi";
	
	private static final String	RECOVERY_EMAIL_SUBJECT	= "Recovery of a ringed bird";
	private static final String	LOYTO_EMAIL_SUBJECT	= "Rengastustiedot Ringmärkningsuppgifter";
	
	private static final String	RECOVERY_EMAIL_MESSAGE	= "" +
			"We have the pleasure of sending you the recovery details of a bird with one of your rings. " +
			"Would you please send the ringing details as soon as possible. \n\n" +
			"Please see the attached PDF-document. \n\n" +
			"If this is not your ring, please return the complete letter. \n" +
			"Thank you for your collaboration! ";
	
	private static final String LOYTO_EMAIL_MESSAGE = "" +
			"Kiitämme Teitä ilmoittamastanne rengastetun linnun löydöstä. Linnun rengastus- ja löytötiedot " +
			"on esitetty oheisessa PDF-liitetiedostossa. \n";
	
	private static final boolean KANSILEHTI = true;
	private static final boolean ILMAN_KANSILEHTEA = false;
	
	private final _Table pyynto;
	private final _Table loyto;
	private final _Table rengastus;
	
	public UlkoFunctionality(_Request<_UlkoDomainModelRow> request) {
		super(request);
		pyynto = data.updateparameters().getPyynto();
		loyto = data.updateparameters().getLoyto();
		rengastus = data.updateparameters().getRengastus();
	}
	
	@Override
	public void preProsessingHook() throws Exception {
		if (data.contains("id")) {
			_UlkoDomainModelRow searchParams = dao.newRow();
			searchParams.getPyynto().getUniqueNumericIdColumn().setValue(data.get("id"));
			dao.executeSearch(searchParams, new UlkoRowsToDataResultsHandler(data));
			data.setUpdateparameters(data.results().get(0));
		}
		else if (showListing()) {
			addListing();
		} else {
			// Tyhjä lomake
			_Column euringPaikka = data.updateparameters().getLoyto().get("euring_paikka");
			if (!euringPaikka.hasValue()) {
				euringPaikka.setValue("SF--");
			}
		}
	}
	
	@Override
	public _Validator validator() throws Exception {
		if (data.updateparameters().getPyynto().hasValues() || data.action().equals(Const.UPDATE)) {
			return new UlkoValidator(request);
		}
		return new EmptyValidator();
		
	}
	
	@Override
	public void afterSuccessfulValidation() throws Exception {
		if (data.action().equals(Const.UPDATE)) {
			executeInsertOrUpdate();
		} else if (data.action().equals("esikatselu_rengastustietopyynto")) {
			File kirje = writeRecoveryLetter(ILMAN_KANSILEHTEA);
			redirectToPreviewOrShowErrorPage(kirje);
		} else if (data.action().equals("laheta_rengastustietopyynto")) {
			sendRecoveryLetter();
		} else if (data.action().equals("esikatselu_loytokirje")) {
			File kirje = writeLoytokirje("loytaja", ILMAN_KANSILEHTEA);
			redirectToPreviewOrShowErrorPage(kirje);
		} else if (data.action().equals("laheta_loytokirje")) {
			sendLoytokirje();
		}
		else if (data.action().equals("lahetetty")) {
			showSendOrPrintMessages();
		}
	}
	
	private void sendLoytokirje() throws Exception {
		String loytajaEmail = getEmail("loytaja");
		String ilmoittajaEmail = getEmail("ilmoittaja");
		File kirje = null;
		File kirje2 = null;
		boolean somethingSent = false;
		boolean somethingNotSent = false;
		if (given(loytajaEmail)) {
			kirje = writeLoytokirje("loytaja", ILMAN_KANSILEHTEA);
			sendEmail(loytajaEmail, LOYTO_EMAIL_SUBJECT, LOYTO_EMAIL_MESSAGE, kirje);
			somethingSent = true;
		} else {
			kirje = writeLoytokirje("loytaja", KANSILEHTI);
			somethingNotSent = true;
		}
		if (ilmoittajaValuesGiven()) {
			if (given(ilmoittajaEmail)) {
				kirje2 = writeLoytokirje("ilmoittaja", ILMAN_KANSILEHTEA);
				sendEmail(ilmoittajaEmail, LOYTO_EMAIL_SUBJECT, LOYTO_EMAIL_MESSAGE, kirje2);
				somethingSent = true;
			} else {
				kirje2 = writeLoytokirje("ilmoittaja", KANSILEHTI);
				somethingNotSent = true;
			}
		}
		redirectTo(somethingSent, somethingNotSent, loytajaEmail+"|"+ilmoittajaEmail, kirje, kirje2);
		
		_Table pyynto = data.updateparameters().getPyynto();
		pyynto.get("tila").setValue("4");
		pyynto.get("loytokirje_lahetetty").setValue(Const.SYSDATE);
		dao.executeUpdate(pyynto);
	}
	
	private void sendRecoveryLetter() throws Exception {
		String schemeEmail = getSchemeEmail();
		String ccEmail = data.updateparameters().getPyynto().get("cc_rengastustietopyynto_email").getValue();
		File kirje = null;
		boolean somethingSent = false;
		boolean somethingNotSent = false;
		if (given(schemeEmail) || given(ccEmail)) {
			kirje = writeRecoveryLetter(false);
			String subject = RECOVERY_EMAIL_SUBJECT;
			subject += " " + data.updateparameters().getLoyto().get("rengas").getValue();
			subject += " (ref. " + data.updateparameters().getPyynto().getUniqueNumericIdColumn().getValue() + ")";
			sendEmail(schemeEmail, ccEmail, subject, RECOVERY_EMAIL_MESSAGE, kirje);
			somethingSent = true;
		} else {
			kirje = writeRecoveryLetter(true);
			somethingNotSent = true;
		}
		_Table pyynto = data.updateparameters().getPyynto();
		pyynto.get("tila").setValue("2");
		pyynto.get("rengaspyynto_lahetetty").setValue(Const.SYSDATE);
		dao.executeUpdate(pyynto);
		redirectTo(somethingSent, somethingNotSent, schemeEmail+"|"+ccEmail, kirje);
	}
	
	private void showSendOrPrintMessages() {
		String sent = data.get("sent");
		String notSent = data.get("notSent");
		if (given(sent) && !given(notSent)) {
			data.setSuccessText("email_sent");
		}
		if (given(notSent) && !given(sent)) {
			data.setFailureText("no_email");
		}
		if (given(sent) && given(notSent)) {
			data.setSuccessText("email_sent_to_other");
			data.setFailureText("email_not_sent_to_other");
		}
		for (String fileName : Utils.valuelist(data.get("file"))) {
			data.producedFiles().add(fileName);
		}
		for (String email : Utils.valuelist(data.get("emails"))) {
			if (given(email))
				data.addToList("emails", email);
		}
	}
	
	private void sendEmail(String email, String subject, String message, File kirje) throws Exception {
		sendEmail(email, null, subject, message, kirje);
	}
	
	private void sendEmail(String email, String ccEmail, String subject, String message, File kirje) throws Exception {
		if (config.developmentMode()) return;
		
		EmailUtil emailUtil = new EmailUtil(config.get(Config.ERROR_REPORTING_SMTP_HOST));
		
		if (config.stagingMode()) {
			Receivers receivers = new Receivers();
			receivers.addReceiver("eopiirai@cs.helsinki.fi");
			receivers.addCC("eopiirai@cs.helsinki.fi");
			emailUtil.send(receivers, FROM_ADDRESS, subject, message, kirje);
		} else {
			Receivers receivers = new Receivers();
			if (given(email)) {
				receivers.addReceiver(email);
			}
			if (given(ccEmail)) {
				receivers.addReceiver(ccEmail);
			}
			receivers.addCC(FROM_ADDRESS);
			emailUtil.send(receivers, FROM_ADDRESS, subject, message, kirje);
		}
	}
	
	private void redirectTo(boolean somethingSent, boolean somethingNotSent, String emails, File ... kirjeet) {
		URIBuilder uri = new URIBuilder(config.baseURL())
				.addParameter("id", data.updateparameters().getPyynto().getUniqueNumericIdColumn().getValue())
				.addParameter(Const.ACTION, "lahetetty");
		for (File kirje : kirjeet) {
			if (kirje != null)
				uri.addParameter("file", kirje.getName());
		}
		if (somethingSent) uri.addParameter("sent", "true");
		if (somethingNotSent) uri.addParameter("notSent", "true");
		uri.addParameter("emails", emails);
		request.redirecter().redirectTo(uri.toString());
	}
	
	private String getSchemeEmail() {
		String scheme = data.updateparameters().getLoyto().get("scheme").getValue();
		Node schemeInfo = request.getTipuApiResource(TipuAPIClient.SCHEMES).getById(scheme);
		String email = schemeInfo.getNode("email").getContents();
		return email;
	}
	
	private String getEmail(String henkilotyyppi){
		return data.updateparameters().getPyynto().get(henkilotyyppi+"_email").getValue();
	}
	
	private boolean ilmoittajaValuesGiven()  {
		for (_Column c : data.updateparameters().getPyynto()) {
			if (!c.getName().startsWith("ilmoittaja_")) continue;
			if (c.hasValue()) return true;
		}
		return false;
	}
	
	private boolean given(String email) {
		return email != null && email.length() > 0;
	}
	
	private void redirectToPreviewOrShowErrorPage(File kirje) {
		if (kirje == null) {
			request.data().setPage("errorpage");
		} else {
			URIBuilder uri = new URIBuilder(config.baseURL())
					.addParameter(Const.PAGE, Const.FILEMANAGEMENT_PAGE)
					.addParameter(Const.ACTION, Const.SHOW)
					.addParameter(FilemanagementFunctionality.FOLDER, FilemanagementFunctionality.PDF_FOLDER)
					.addParameter(Const.FILENAME, kirje.getName());
			request.redirecter().redirectTo(uri.toString());
		}
	}
	
	private File writeRecoveryLetter(boolean kansilehti) throws Exception {
		_Table pyynto = data.updateparameters().getPyynto();
		_Table loyto = data.updateparameters().getLoyto();
		return new RecoveryLetterUtil(pyynto, loyto, request).write(kansilehti);
	}
	
	private File writeLoytokirje(String kenelle, boolean kansilehti) throws Exception {
		_Table pyynto = data.updateparameters().getPyynto();
		_Table loyto = data.updateparameters().getLoyto();
		_Table rengastus = data.updateparameters().getRengastus();
		return new LoytokirjeUtil(pyynto, loyto, rengastus, request).setVastaanottaja(kenelle).write(kansilehti);
	}
	
	private boolean showListing() {
		return data.contains("tila") || data.contains("roskiin");
	}
	
	private void addListing() throws SQLException {
		_UlkoDomainModelRow searchParams = dao.newRow();
		if (data.contains("roskiin")) {
			searchParams.getPyynto().get("roskiin").setValue("K");
		} else {
			searchParams.getPyynto().get("tila").setValue(data.get("tila"));
			searchParams.getPyynto().get("roskiin").setValue("E");
		}
		dao.executeSearch(searchParams, new UlkoRowsToDataResultsHandler(data));
	}
	
	private void executeInsertOrUpdate() throws Exception {
		boolean update = pyynto.getRowidValue().length() > 0;
		
		_Column pyynnonTila = pyynto.get("tila");
		if (!pyynnonTila.hasValue()) {
			pyynto.get("tila").setValue("1");
		}
		
		convertCoordinates(loyto, config);
		convertCoordinates(rengastus, config);
		
		if (!pyynto.get("roskiin").hasValue()) {
			pyynto.get("roskiin").setValue("E");
		}
		
		insertOrUpdate(loyto);
		pyynto.get("loyto").setValue(loyto.getUniqueNumericIdColumn().getValue());
		
		if (rengastus.hasValuesIgnoreMetadata()) {
			if (pyynnonTila.getValue().equals("2")) {
				pyynnonTila.setValue("3");
			}
			insertOrUpdate(rengastus);
			pyynto.get("rengastus").setValue(rengastus.getUniqueNumericIdColumn().getValue());
		}
		
		insertOrUpdate(pyynto);
		
		if (update) {
			request.session().setFlash(Success.UPDATE);
		} else {
			request.session().setFlash(Success.INSERT);
		}
		
		if (data.contains("fromloydos")) {
			markReceived(data.updateparameters().getPyynto().get("loydosid").getValue());
		}
		
		request.redirecter().redirectTo(config.baseURL()+"?id="+pyynto.getUniqueNumericIdColumn().getValue());
	}
	
	private void markReceived(String loydosid) throws Exception {
		HttpClientService client = null;
		CloseableHttpResponse response = null;
		HttpEntity requestEntity = new StringEntity("true", ContentType.create("text/json", "utf-8"));
		try {
			String uri = config.get("Loydos_uri") + "/admin/api/submissions/rengasloyto/" + loydosid + "/received";
			client = new HttpClientService(uri, config.get("Loydos_username"), config.get("Loydos_password"));
			HttpPost request = new HttpPost(uri);
			request.setEntity(requestEntity);
			response = client.execute(request);
			if (response.getStatusLine().getStatusCode() != 200) {
				this.request.getErrorReporter().report("Failed to receive from Löydös: " + uri + ". Status code was " +  response + ".");
			}
		} finally {
			EntityUtils.consume(requestEntity);
			if (response != null) response.close();
			if (client != null) client.close();
		}
	}
	
	public static void convertCoordinates(_Table table, Config config) throws Exception {
		String koordIlmTyyppi = table.get("koord_ilm_tyyppi").getValue();
		String lev = "";
		String pit = "";
		if ("E".equals(koordIlmTyyppi)) {
			koordIlmTyyppi = "euref";
			lev = table.get("eur_leveys").getValue();
			pit = table.get("eur_pituus").getValue();
		} else if ("Y".equals(koordIlmTyyppi)) {
			koordIlmTyyppi = "ykj";
			lev = table.get("yht_leveys").getValue();
			pit = table.get("yht_pituus").getValue();
		} else if ("W".equals(koordIlmTyyppi)) {
			koordIlmTyyppi = "wgs84";
			lev = table.get("wgs84_leveys").getValue();
			pit = table.get("wgs84_pituus").getValue();
		} else if ("WA".equals(koordIlmTyyppi)) {
			koordIlmTyyppi = "wgs84-degrees";
			lev = table.get("wgs84_ast_leveys").getValue();
			pit = table.get("wgs84_ast_pituus").getValue();
		} else {
			return;
		}
		URIBuilder uri = new URIBuilder("coordinate-conversion-service")
				.addParameter("lat", lev.replace(",", "."))
				.addParameter("lon", pit.replace(",", "."))
				.addParameter("type", koordIlmTyyppi);
		
		TipuAPIClient api = null;
		try  {
			api = new TipuAPIClientImple(config);
			Node response = api.getAsDocument(uri.toString()).getRootNode();
			//			<conversion-response>
			//				<euref lat="6663841" lon="333229" aka="euref"/>
			//				<ykj lat="6666639" lon="3333331" aka="ykj"/>
			//				<wgs84 lat="60.077640218789" lon="24.002369855155" aka="etrs89-decimal"/>
			//				<wgs84-degrees lat="600440" lon="240009" aka="etrs89-degrees" note="format: ddmmss"/>
			//			</conversion-response>
			if (table.get("euring_paikka").getValue().startsWith("SF")) {
				table.get("yht_leveys").setValue(response.getNode("ykj").getAttribute("lat"));
				table.get("yht_pituus").setValue(response.getNode("ykj").getAttribute("lon"));
				table.get("eur_leveys").setValue(response.getNode("euref").getAttribute("lat"));
				table.get("eur_pituus").setValue(response.getNode("euref").getAttribute("lon"));
			} else {
				table.get("yht_leveys").clearValue();
				table.get("yht_pituus").clearValue();
				table.get("eur_leveys").clearValue();
				table.get("eur_pituus").clearValue();
			}
			table.get("wgs84_leveys").setValue(response.getNode("wgs84").getAttribute("lat"));
			table.get("wgs84_pituus").setValue(response.getNode("wgs84").getAttribute("lon"));
			table.get("wgs84_ast_leveys").setValue(response.getNode("wgs84-degrees").getAttribute("lat"));
			table.get("wgs84_ast_pituus").setValue(response.getNode("wgs84-degrees").getAttribute("lon"));
		} finally {
			if (api != null) api.close();
		}
	}
	
	private void insertOrUpdate(_Table table) throws SQLException {
		if (alreadyExists(table)) {
			dao.executeUpdate(table);
		} else {
			dao.returnAndSetNextId(table);
			dao.executeInsert(table);
		}
	}
	
	
	private boolean alreadyExists(_Table table) {
		return table.getRowidValue().length() > 0;
	}
	
	@Override
	public void postProsessingHook() throws Exception {
		if (data.page().equals("errorpage")) return;
		if (showListing()) {
			data.setPage("main_list");
		} else {
			data.setPage("main_form");
		}
		new LoydosUtil(request).setLoydosValuesToData();
	}
	
}
