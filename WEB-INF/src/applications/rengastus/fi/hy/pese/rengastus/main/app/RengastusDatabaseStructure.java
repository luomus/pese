package fi.hy.pese.rengastus.main.app;


import fi.hy.pese.framework.main.general.data.DatabaseStructure;
import fi.hy.pese.framework.main.general.data.DatabaseTableStructure;
import fi.hy.pese.framework.main.general.data._Table;

public class RengastusDatabaseStructure extends DatabaseStructure {

	private DatabaseTableStructure kontrolli;

	public RengastusDatabaseStructure() {
		super();
		kontrolli = new DatabaseTableStructure(_Table.UNINITIALIZED_TABLE);
	}

	public DatabaseTableStructure getKontrolli() {
		return kontrolli;
	}

	public void setKontrolli(DatabaseTableStructure t) {
		this.kontrolli = t;
		toTableStructuremap(t);
	}

}
