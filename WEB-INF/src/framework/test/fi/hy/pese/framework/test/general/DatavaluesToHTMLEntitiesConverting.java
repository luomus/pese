package fi.hy.pese.framework.test.general;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import fi.hy.pese.framework.main.general.data.Data;
import fi.hy.pese.framework.main.general.data.DatabaseTableStructure;
import fi.hy.pese.framework.main.general.data.PesaDomainModelDatabaseStructure;
import fi.hy.pese.framework.main.general.data.PesaDomainModelRowFactory;
import fi.hy.pese.framework.main.general.data.Row;
import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._Data;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.main.general.data._Row;
import fi.hy.pese.framework.test.RowFactory;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class DatavaluesToHTMLEntitiesConverting {

	public static Test suite() {
		return new TestSuite(DatavaluesToHTMLEntitiesConverting.class.getDeclaredClasses());
	}

	public static class ConvertingAllTablesValuesInDataToHTMLEntities extends TestCase {

		private _Data<_PesaDomainModelRow>	data;

		@Override
		protected void setUp() {
			PesaDomainModelDatabaseStructure template = new PesaDomainModelDatabaseStructure();
			DatabaseTableStructure pesa = new DatabaseTableStructure("pesa");
			pesa.addColumn("kommentti", _Column.VARCHAR, 250);
			DatabaseTableStructure poikanen = new DatabaseTableStructure("poikanen");
			poikanen.addColumn("mitta", _Column.DECIMAL, 5, 2);
			template.setPoikanen(poikanen);
			template.setPesa(pesa);
			data = new Data<>(new PesaDomainModelRowFactory(template, "", ""));
		}

		public void test_it_converts_searchparameters_html_entities() {
			data.searchparameters().getColumn("pesa.kommentti").setValue("Tämä on 'uusi' pesa");
			data.convertValuesToHTMLEntities();
			assertEquals("Tämä on &#39;uusi&#39; pesa", data.searchparameters().getColumn("pesa.kommentti").getValue());
		}

		public void test_it_converts_searchparameters_html_entities2() {
			data.searchparameters().getColumn("pesa.kommentti").setValue("' \" < > &");
			data.convertValuesToHTMLEntities();
			assertEquals("&#39; &quot; &lt; &gt; &amp;", data.searchparameters().getColumn("pesa.kommentti").getValue());
		}

		public void test_it_converts_updateparameters_html_entities() {
			data.updateparameters().getColumn("pesa.kommentti").setValue("Tämä on 'uusi' pesa");
			data.convertValuesToHTMLEntities();
			assertEquals("Tämä on &#39;uusi&#39; pesa", data.updateparameters().getColumn("pesa.kommentti").getValue());
		}

		public void test_it_converts_html_entities_from_results() {
			_Row result = data.newResultRow();
			result.getColumn("pesa.kommentti").setValue("mitta > 1000");
			result = data.newResultRow();
			result.getColumn("pesa.kommentti").setValue("400 < mitta > 500 & musta nokka");

			data.convertValuesToHTMLEntities();
			assertEquals("mitta &gt; 1000", data.results().get(0).getColumn("pesa.kommentti").getValue());
			assertEquals("400 &lt; mitta &gt; 500 &amp; musta nokka", data.results().get(1).getColumn("pesa.kommentti").getValue());
		}

		public void test_it_converts_poikanen_columns___also_decimals() {
			_Row result = data.newResultRow();
			result.getColumn("poikanen.1.mitta").setValue(">500");
			data.convertValuesToHTMLEntities();
			assertEquals("&gt;500", data.results().get(0).getColumn("poikanen.1.mitta").getValue());
		}

	}

	public static class ConvertingRestOfTheValuesInDataToHTMLEntities extends TestCase {

		private Data<_Row>	data;

		@Override
		protected void setUp() {
			data = new Data<>(new RowFactory());
		}

		public void test__success_and_failure_texts() {
			data.setFailureText(" < ");
			data.setSuccessText(" < ");

			data.convertValuesToHTMLEntities();

			assertEquals(" &lt; ", data.failureText());
			assertEquals(" &lt; ", data.successText());
		}

		public void test__notice_texts() {
			List<String> notices = new ArrayList<>();
			notices.add(" < ");
			notices.add(" > ");
			data.addToNoticeTexts(notices);

			data.convertValuesToHTMLEntities();

			assertEquals(" &lt; ", data.noticeTexts().get(0));
			assertEquals(" &gt; ", data.noticeTexts().get(1));
		}

		public void test__procuded_files() {

			List<String> files = new LinkedList<>();
			files.add(" 53_ruskea&musta.txt");
			files.add(" 54_\"saari\".txt");
			data.addToProducedFiles(files);

			data.convertValuesToHTMLEntities();

			assertEquals(" 53_ruskea&amp;musta.txt", data.producedFiles().get(0));
			assertEquals(" 54_&quot;saari&quot;.txt", data.producedFiles().get(1));
		}

		public void test__uitexts() {
			Map<String, String> uiTexts = new HashMap<>();
			uiTexts.put("title_search", "Haku & Syättä");
			uiTexts.put("error.must_be_larger", "Arvon oltava > 5");
			data.setUITexts(uiTexts);

			data.convertValuesToHTMLEntities();

			assertEquals("Haku &amp; Syättä", uiTexts.get("title_search"));
			assertEquals("Arvon oltava &gt; 5", uiTexts.get("error.must_be_larger"));
		}

		public void test__misc_values() {
			data.set("and", "&");
			data.convertValuesToHTMLEntities();
			assertEquals("&amp;", data.get("and"));
		}

		public void test_misc_lists__stringlist() {
			data.addToList("merkit", "<");
			data.addToList("merkit", ">");
			data.convertValuesToHTMLEntities();
			assertEquals("&lt;", data.getLists().get("merkit").get(0));
			assertEquals("&gt;", data.getLists().get("merkit").get(1));
		}

		public void test_misc_lists__map_string_string() {
			/*
			 * Map<String, String> m1 = new HashMap<String, String>();
			 * m1.put("tyyppi", "'suisto'");
			 * m1.put("etaisyys", "> 500");
			 * data.addToList("maastot", m1);
			 * Map<String, String> m2 = new HashMap<String, String>();
			 * m2.put("tyyppi", "a");
			 * m2.put("etaisyys", "b & c");
			 * data.addToList("maastot", m2);
			 * data.convertValuesToHTMLEntities();
			 * assertEquals("&apos;suisto&apos;", m1.get("tyyppi"));
			 */
			// T O D O .....
		}

		public void test_misc_lists___tables() {
			PesaDomainModelDatabaseStructure rowStructure = new PesaDomainModelDatabaseStructure();
			rowStructure.setKaynti(new DatabaseTableStructure("kaynti"));
			rowStructure.getKaynti().addColumn("kommentti", _Column.VARCHAR, 20);

			_Row row = new Row(rowStructure);

			row.getColumn("kaynti.kommentti").setValue("Uusi 'kommentti'");
			data.addToList("lista", row);

			data.convertValuesToHTMLEntities();

			_Row after = (_Row) data.getLists().get("lista").get(0);
			assertEquals("Uusi &#39;kommentti&#39;", after.getColumn("kaynti.kommentti").getValue());
		}

	}

}
