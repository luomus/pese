package fi.hy.pese.pesakortti.main.x_batch_processing;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Collection;

import fi.hy.pese.framework.main.app._ApplicationSpecificationFactory;
import fi.hy.pese.framework.main.db.RowCombiningResultHandler;
import fi.hy.pese.framework.main.db._DAO;
import fi.hy.pese.framework.main.general.data.PesaDomainModelDatabaseStructure;
import fi.hy.pese.framework.main.general.data.PesaDomainModelRowFactory;
import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._IndexedTablegroup;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.main.general.data._Table;
import fi.hy.pese.framework.test.TestConfig;
import fi.hy.pese.pesakortti.main.app.PesakorttiConst;
import fi.hy.pese.pesakortti.main.app.PesakorttiDatabaseStructureCreator;
import fi.hy.pese.pesakortti.main.app.PesakorttiFactory;
import fi.luomus.commons.db.connectivity.PreparedStatementStoringAndClosingTransactionConnection;
import fi.luomus.commons.db.connectivity.TransactionConnection;
import fi.luomus.commons.utils.FileUtils;
import fi.luomus.commons.utils.Utils;


public class Dumppi {

	private static final Collection<String> TAULUT = Utils.collection("pesa", "tarkastus", "kaynti", "aikuinen", "poikanen", "muna");

	public static void main(String[] args) throws Exception {
		_ApplicationSpecificationFactory<_PesaDomainModelRow> factory = createFactory();
		_DAO<_PesaDomainModelRow> dao = null;
		try {
			dao = factory.dao(Dumppi.class.getCanonicalName());
			createDump(dao);
		} finally {
			factory.release(dao);
		}
		System.out.println("done");
	}

	private static void createDump(_DAO<_PesaDomainModelRow> dao) throws SQLException, IOException {
		File outputFile = new File("C:/x_pesakortti_dump.txt");
		_PesaDomainModelRow searchParams = dao.newRow();
		searchParams.getTarkastus().get("vuosi").setValue("2000");

		writeHeader(searchParams, outputFile);
		dao.executeSearch(searchParams, new ResultsToFileHandler(dao, outputFile));
	}

	private static class ResultsToFileHandler extends RowCombiningResultHandler {

		private final File outputFile;

		public ResultsToFileHandler(_DAO<_PesaDomainModelRow> dao, File outputFile) {
			super(dao);
			this.outputFile = outputFile;
		}

		@Override
		protected boolean handle(_PesaDomainModelRow row) {
			try {
				write(row, outputFile);
				System.out.println(vanhaKortti(row));
			} catch (IOException e) {
				e.printStackTrace();
				return false;
			}
			return true;
		}

	}


	private static void writeHeader(_PesaDomainModelRow row, File file) throws IOException {
		StringBuilder line = new StringBuilder();
		for (_Table t : row) {
			if (!TAULUT.contains(t.getName())) continue;
			if (row.getGroups().keySet().contains(t.getName())) continue;
			for (_Column c : t) {
				appendColumnHeader(line, c);
			}
		}
		for (_IndexedTablegroup group : row.getGroups().values()) {
			if (group.getStaticCount() < 1) continue;
			for (int i = 1; i <= group.getStaticCount(); i++) {
				for (_Column c : group.get(i)) {
					appendColumnHeader(line, c);
				}
			}
		}
		line.append("\n");
		FileUtils.writeToFile(file, line.toString(), false);
	}

	private static String prev_line = "";

	private static void write(_PesaDomainModelRow row, File file) throws IOException {
		StringBuilder current_line = new StringBuilder();
		for (_Table t : row) {
			if (!TAULUT.contains(t.getName())) continue;
			if (row.getGroups().keySet().contains(t.getName())) continue;
			for (_Column c : t) {
				appendColumn(current_line, c);
			}
		}
		for (_IndexedTablegroup group : row.getGroups().values()) {
			if (group.getStaticCount() < 1) continue;
			for (int i = 1; i <= group.getStaticCount(); i++) {
				for (_Column c : group.get(i)) {
					appendColumn(current_line, c);
				}
			}
		}
		current_line.append("\n");
		if (!current_line.toString().equals(prev_line)) {
			FileUtils.writeToFile(file, current_line.toString(), true);
			prev_line = current_line.toString();
		}
	}

	private static void appendColumn(StringBuilder line, _Column c) {
		if (c.isRowIdColumn()) return;
		line.append(c.getValue());
		line.append("|");
	}

	private static void appendColumnHeader(StringBuilder line, _Column c) {
		if (c.isRowIdColumn()) return;
		line.append(c.getFullname());
		line.append("|");
	}

	private static Integer vanhaKortti(_PesaDomainModelRow row) {
		return row.getTarkastus().get("vanha_kortti").getIntegerValue();
	}

	private static _ApplicationSpecificationFactory<_PesaDomainModelRow> createFactory() throws Exception {
		PesakorttiFactory factory = new PesakorttiFactory(TestConfig.getConfigFile("pese_pesakortti.config"));

		TransactionConnection con = new PreparedStatementStoringAndClosingTransactionConnection(factory.config().connectionDescription());
		try {
			PesaDomainModelDatabaseStructure structure = PesakorttiDatabaseStructureCreator.loadFromDatabase(con);
			factory.setRowFactory(new PesaDomainModelRowFactory(structure, PesakorttiConst.JOINS, " tarkastus.vanha_kortti, kaynti.pvm, kaynti.klo_h, kaynti.klo_min, kaynti.id, poikanen.id, aikuinen.id, muna.id "));
		} catch (Exception e) {
			throw new Exception(e);
		} finally {
			con.release();
		}

		factory.loadUITexts();
		return factory;
	}

}
