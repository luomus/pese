package fi.hy.pese.merikotka.main.app;

import fi.hy.pese.framework.main.app.PeSeServlet;
import fi.hy.pese.framework.main.app._ApplicationSpecificationFactory;
import fi.hy.pese.framework.main.general.data.PesaDomainModelDatabaseStructure;
import fi.hy.pese.framework.main.general.data.PesaDomainModelRowFactory;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.main.general.data._RowFactory;
import fi.luomus.commons.db.connectivity.PreparedStatementStoringAndClosingTransactionConnection;
import fi.luomus.commons.db.connectivity.TransactionConnection;

public class MerikotkaServlet extends PeSeServlet<_PesaDomainModelRow> {

	static final long	serialVersionUID	= 2525435333L;

	@Override
	protected _ApplicationSpecificationFactory<_PesaDomainModelRow> createFactory() throws Exception {
		MerikotkaFactory factory = new MerikotkaFactory("pese_merikotka.config");
		factory.loadUITexts();

		TransactionConnection con = new PreparedStatementStoringAndClosingTransactionConnection(factory.config().connectionDescription());
		try {
			PesaDomainModelDatabaseStructure structure = MerikotkaDatabaseStructureCreator.loadFromDatabase(con);
			_RowFactory<_PesaDomainModelRow> rowFactory = new PesaDomainModelRowFactory(structure, MerikotkaConst.JOINS, MerikotkaConst.ORDER_BY);
			factory.setRowFactory(rowFactory);
		} catch (Exception e) {
			con.release();
			throw new Exception(e);
		}
		con.release();
		return factory;
	}

}
