package fi.hy.pese.ulko.main.app;

import java.sql.SQLException;

import fi.hy.pese.framework.main.db.oraclesql.Queries;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data.DatabaseTableStructure;
import fi.luomus.commons.db.connectivity.TransactionConnection;
import fi.luomus.commons.tipuapi.TipuAPIClient;

public class UlkoDatabaseStructureCreator {

	public static UlkoDatabaseStructure loadFromDatabase(TransactionConnection con) throws SQLException {

		UlkoDatabaseStructure structure = new UlkoDatabaseStructure();

		DatabaseTableStructure pyynto = Queries.returnTableStructure("pyynto", con);
		DatabaseTableStructure loyto = Queries.returnTableStructure("tapaaminen", "loyto", con);
		DatabaseTableStructure rengastus = Queries.returnTableStructure("tapaaminen", "rengastus", con);

		structure.setPyynto(pyynto);
		structure.setLoyto(loyto);
		structure.setRengastus(rengastus);

		pyynto.get("id").setTypeToUniqueNumericIncreasingId();
		pyynto.get("kirj_pvm").setTypeToDateAddedColumn();
		pyynto.get("muutos_pvm").setTypeToDateModifiedColumn();
		pyynto.get("tila").setAssosiatedSelection("pyynto.tila");
		pyynto.get("roskiin").setAssosiatedSelection(Const.K_E);
		pyynto.get("loytaja_renro").setAssosiatedSelection(TipuAPIClient.RINGERS);
		pyynto.get("ilmoittaja_renro").setAssosiatedSelection(TipuAPIClient.RINGERS);
		setLoytoRengastusCommon(loyto);
		setLoytoRengastusCommon(rengastus);

		rengastus.get("linnun_tila").setAssosiatedSelection("codes_22");
		// loyto.get("kunta").setAssosiatedSelection(TipuAPIClient.MUNICIPALITIES);

		return structure;
	}

	private static void setLoytoRengastusCommon(DatabaseTableStructure table) {
		table.get("id").setTypeToUniqueNumericIncreasingId();
		table.get("kirj_pvm").setTypeToDateAddedColumn();
		table.get("muutos_pvm").setTypeToDateModifiedColumn();

		table.get("scheme").setAssosiatedSelection(TipuAPIClient.SCHEMES);

		table.get("rengas").setToUppercaseColumn();
		table.get("rengas_varmennettu").setAssosiatedSelection(Const.K_E);

		table.get("laji").setAssosiatedSelection(TipuAPIClient.SPECIES);

		table.get("aika_tarkkuus").setAssosiatedSelection("codes_67");

		table.get("euring_paikka").setAssosiatedSelection(TipuAPIClient.EURING_PROVINCES);
		table.get("koord_ilm_tyyppi").setAssosiatedSelection("codes_601");
		table.get("paikka_tarkkuus").setAssosiatedSelection("codes_68");

		table.get("linnun_tila").setAssosiatedSelection("codes_70");
		table.get("loytotapa").setAssosiatedSelection("kontrolli.tapa");
		table.get("loytotapa_tarkkuus").setAssosiatedSelection(Const.K_E);

		table.get("sukupuoli").setAssosiatedSelection("codes_9");
		table.get("ika").setAssosiatedSelection("codes_11");
		table.get("rasva").setAssosiatedSelection("codes_62");

		//		List<String> order = new ArrayList<String>();
		//		order.add("id");
		//		order.add("scheme");
		//		order.add("rengas");
		//		order.add("rengas_selite");
		//		order.add("rengas_varmennettu");
		//		order.add("pvm");
		//		order.add("klo");
		//		order.add("aika_tarkkuus");
		//		order.add("euring_paikka");
		//		order.add("kunta");
		//		order.add("tarkempi_paikka");
		//		order.add("koord_ilm_tyyppi");
		//		order.add("yht_leveys");
		//		order.add("yht_pituus");
		//		order.add("eur_leveys");
		//		order.add("eur_pituus");
		//		order.add("wgs84_leveys");
		//		order.add("wgs84_pituus");
		//		order.add("paikka_tarkkuus");
		//		order.add("laji");
		//		order.add("sukupuoli");
		//		order.add("ika");
		//		order.add("paino");
		//		order.add("siipi");
		//		order.add("rasva");
		//		order.add("linnun_tila");
		//		order.add("loytotapa");
		//		order.add("loytotapa_tarkkuus");
		//		order.add("kommentti");
		//		order.add("kirj_pvm");
		//		order.add("muutos_pvm");
		//		table.reorder(order);
	}

}
