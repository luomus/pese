package fi.hy.pese.framework.test.general;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

import fi.hy.pese.framework.main.general.Logger;
import fi.hy.pese.framework.main.general._Logger;
import fi.hy.pese.framework.main.general.data.DatabaseTableStructure;
import fi.hy.pese.framework.main.general.data.Table;
import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._Table;
import fi.hy.pese.framework.test.TestConfig;
import fi.luomus.commons.config.Config;
import fi.luomus.commons.utils.DateUtils;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class LoggerImplementationTest {

	public static Test suite() {
		return new TestSuite(LoggerImplementationTest.class.getDeclaredClasses());
	}

	public static class LogWriting extends TestCase {

		private static final String	COL_2_VALUE		= "COL_2_V";
		private static final String	COL_1_VALUE		= "COL_1_V";
		private static final String	COL_2_NAME		= "test_column_2";
		private static final String	COL_1_NAME		= "test_column_1";
		private static final String	TEST_TABLE_NAME	= "test_table";
		private static final String	USERID			= "userid";
		private Config				config;
		private _Logger				logger;
		private String				logFileContents;
		private String				expected;

		@Override
		protected void setUp() throws Exception {
			config = TestConfig.getConfig();
			logger = new Logger(USERID, config.baseFolder(), config.get(Config.DATABASE_LOG_FILE_PREFIX));
			logFileContents = null;
		}

		private _Table createTable() {
			DatabaseTableStructure tableStructure = new DatabaseTableStructure(TEST_TABLE_NAME);
			tableStructure.addColumn(COL_1_NAME, _Column.VARCHAR, 20);
			tableStructure.addColumn(COL_2_NAME, _Column.VARCHAR, 20);
			tableStructure.get(COL_1_NAME).setTypeToImmutablePartOfAKey();
			_Table table = new Table(tableStructure);
			table.setRowidValue("ROWID_V");
			table.get(COL_1_NAME).setValue(COL_1_VALUE);
			table.get(COL_2_NAME).setValue(COL_2_VALUE);
			return table;
		}

		@Override
		protected void tearDown() throws Exception {
			File logfile = logFile();
			logfile.delete();
		}

		private File logFile() {
			return new File(config.baseFolder() + File.separator + config.get(Config.DATABASE_LOG_FILE_PREFIX) + DateUtils.getCurrentDateTime("yyyy-MM") + ".txt");
		}

		public void test__writing_to_log() throws Exception {
			logger.log("test");
			logger.commitMessages();
			expected = "] [userid] [test]\n";
			assertTrue(readContents().contains(expected));
		}

		private String readContents() throws FileNotFoundException, IOException {
			if (logFileContents != null) return logFileContents;
			File logfile = logFile();
			BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(logfile)));
			StringBuilder contents = new StringBuilder();
			String row;
			while ((row = reader.readLine()) != null) {
				contents.append(row).append("\n");
			}
			reader.close();
			logFileContents = contents.toString();
			return logFileContents;
		}

		public void test__doesnt_write_anything_before_commit() throws Exception {
			logger.log("test");
			// logger.commitMessages();
			try {
				readContents();
				fail("File should not have been written");
			} catch (FileNotFoundException e) {
			}
		}

		public void test__two_lines() throws Exception {
			logger.log("line 1");
			logger.log("line 2");
			logger.commitMessages();
			logger.log("line 3");
			expected = "] [userid] [line 1, line 2]\n";
			assertTrue(readContents().contains(expected));
		}

		public void test__two_messages() throws Exception {
			logger.log("line 1");
			logger.log("line 2");
			logger.commitMessages();
			logger.log("line 3");
			logger.commitMessages();
			assertTrue(readContents().contains("[userid] [line 1, line 2]\n"));
			assertTrue(readContents().contains("[userid] [line 3]\n"));
		}

		public void test__delete_log() throws Exception {
			logger.logDelete(createTable());
			logger.commitMessages();
			expected = "[userid] [DELETES ROW FROM test_table, test_column_1=COL_1_V, test_column_2=COL_2_V]\n";
			assertTrue(readContents().contains(expected));
		}

		public void test__insert_log() throws Exception {
			logger.logInsert(createTable());
			logger.commitMessages();
			expected = "[userid] [INSERTS ROW TO test_table, test_column_1=COL_1_V, test_column_2=COL_2_V]\n";
			assertTrue(readContents().contains(expected));
		}

		public void test__update_log_____only_changed_values____includes_keys() throws Exception {
			_Table newValues = createTable();
			_Table oldValues = createTable();
			newValues.get(COL_2_NAME).setValue("new value");
			logger.logUpdate(oldValues, newValues);
			logger.commitMessages();
			expected = "[userid] [UPDATES ROW OF test_table (COL_1_V), test_column_2: COL_2_V => new value]\n";
			assertTrue(readContents().contains(expected));
		}

	}

}
