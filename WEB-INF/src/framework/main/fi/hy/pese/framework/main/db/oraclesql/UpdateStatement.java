package fi.hy.pese.framework.main.db.oraclesql;

import java.util.LinkedList;
import java.util.List;

import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._Table;
import fi.luomus.commons.db.connectivity.TransactionConnection;
import fi.luomus.commons.utils.DateUtils;

public class UpdateStatement extends Statement {

	private String				table	= "";
	private final List<String>	updateStatementList;
	private final List<String>	whereStatementList;

	public UpdateStatement(TransactionConnection con) {
		super(con);
		updateStatementList = new LinkedList<>();
		whereStatementList = new LinkedList<>();
	}

	@Override
	protected String constructQuery() {
		StringBuilder query = new StringBuilder(" UPDATE ");
		query.append(table).append(" SET ");
		toCommaSeperatedStatement(query, updateStatementList);
		query.append(" WHERE ");
		toAndSeperatedStatement(query, whereStatementList);
		return query.toString();
	}

	public void updateByRowId(_Table table) {
		this.table = table.getName();
		for (_Column c : table) {
			if (isRowIdColumn(c)) continue;
			if (isNumericIdColumn(c)) continue;
			if (isPartOfTheKey(c)) continue;
			if (isDateAddedColumn(c)) continue;
			if (isDateModifiedColumn(c) || c.getValue().equals(Const.SYSDATE)) {
				updateStatementList.add(c.getName() + " = SYSDATE ");
			} else {
				updateStatementList.add(c.getName() + " = ? ");
				if (c.hasValue() && c.getDatatype() == _Column.DECIMAL) {
					values.add(new Double(c.getDoubleValue()));
				} else if (c.hasValue() && c.getDatatype() == _Column.DATE) {
					values.add(DateUtils.convertToDate(c.getDateValue()));
				} else {
					values.add(c.getValue());
				}
			}
		}
		whereStatementList.add(" rowid = ? ");
		values.add(table.getRowidValue());
	}

}
