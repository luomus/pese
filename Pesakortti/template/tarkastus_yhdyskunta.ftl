
<h3> Tarkastustiedot </h3>

<table class="osio paaosio">
	<tr>
		<td>
			<fieldset>
			<legend>Pesinnän vuosi</legend>
			<@inputText "updateparameters" updateparameters.tarkastus.vuosi true errors /> 
			</fieldset>
		</td>
		<td>
			<fieldset>
			<legend>Tarkastaja</legend>
			<@inputSelectCombo "updateparameters" updateparameters.tarkastus.tarkastaja true false errors />
			</fieldset>
		</td>
		<td>
			<fieldset>    
    		<legend>Tietojen suojaus 5v. eteenpäin</legend>
    		<select name="updateparameters.tarkastus.suojaus" size="1" >
    			<option value="K" <#if updateparameters.tarkastus.suojaus == "K"> selected="selected" </#if> >Kyllä</option>
    			<option value="E" <#if updateparameters.tarkastus.suojaus == "" || updateparameters.tarkastus.suojaus == "E"> selected="selected" </#if> >Ei</option>
    		</select>
			</fieldset>
		</td>
	</tr>
</table>  	

<h3>Yhdyskunnan perustiedot</h3>

<table class="osio paaosio">
	<tr>
		<td>
			<fieldset>
			<legend>Kunta</legend>
			<@inputSelectCombo "updateparameters" updateparameters.pesa.kunta true false errors />
			</fieldset>
		</td>
		<td>
			<fieldset>
			<legend>Tarkempi paikka</legend>
			<@inputText "updateparameters" updateparameters.pesa.tarkempi_paikka true errors />
			</fieldset>
		</td>
	</tr>
</table>


<table class="osio paaosio">
	<caption>Yhdyskunnan keskipisteen koordinaatit</caption>
	<tr>
  		<td>
  			<table id="koordinaatit">
  				<tr>
    				<td> </td>
    				<td> Pohjois- </td>
    				<td> Itä- </td>
  				</tr>
  				<tr>
    				<td> <@radio "updateparameters" updateparameters.pesa.koord_ilm_tyyppi "Y" errors /> Yhtenäiskoordinaatit </td>
    				<td> <@inputText "updateparameters" updateparameters.pesa.yht_leveys true errors /> </td>
    				<td> <@inputText "updateparameters" updateparameters.pesa.yht_pituus true errors /> </td>
  				</tr>
  				<tr>
    				<td> <@radio "updateparameters" updateparameters.pesa.koord_ilm_tyyppi "E" errors /> EUREF-koordinaatit </td>
    				<td> <@inputText "updateparameters" updateparameters.pesa.eur_leveys true errors /> </td>
    				<td> <@inputText "updateparameters" updateparameters.pesa.eur_pituus true errors /> </td>
  				</tr>
  			</table>
  		</td>
  		<td>
			<table>
				<tr>
					<td>
						<fieldset>
						<legend>Mittaustapa</legend>
						<@selection selections "updateparameters" updateparameters.pesa.koord_mittaustapa 2 false false false errors /> 
						</fieldset>
					</td>
					<td>
						<fieldset>    
    					<legend>Tarkkuus</legend>
    					<@inputText "updateparameters" updateparameters.pesa.koord_tarkkuus true errors /> m
						</fieldset>
					</td>
					<td>
						<fieldset>    
    					<legend>Luovutus</legend>
    					<@selection selections "updateparameters" updateparameters.pesa.suojelu_kaytto 2 false false false errors />
						</fieldset>
					</td>
				</tr>
			</table>
  		</td>
  	</tr>
</table>

<h3> Yhdyskunnan paikan ja pesien kuvaus </h3>

<table class="osio paaosio">
	<tr>
		<td>
			<fieldset id="pesa_sijainti_muuta_fieldset" <@checkwarning "pesa.pesa_sijainti" /> >
			<legend <@checkwarning "pesa.pesa_sijainti" /> >Pesien sijainti</legend>
			<a href="#" class="muuta" id="pesan_sijainti_content">
				<#if updateparameters.pesa.pesa_sijainti == ""> 
					(tyhjä) 
				<#else> 
					<#assign column = updateparameters.pesa.pesa_sijainti> 
      	        	${selections[column.assosiatedSelection][column.value]}    
				</#if>
			 </a> 
			</fieldset>
		</td>
		<td rowspan="2">
			<fieldset id="reunavaikutus">
			<legend>Reunavaikutus</legend>
			<label>Suhde reunaan:</label><br />
			<@selection selections "updateparameters" updateparameters.tarkastus.pesa_suhde_reuna 1 false true false errors /><br />
			<label>Etäisyys reunasta/reunaan:</label><br />
			<@selection selections "updateparameters" updateparameters.tarkastus.pesa_etaisyys_reuna 1 false true false errors />
			</fieldset>
		</td>
		<td >
			<fieldset id="biotooppi_muuta_fieldset" <@checkwarning "tarkastus.pesa_biotooppi" /> >
			<legend <@checkwarning "tarkastus.pesa_biotooppi" /> >Varsinainen pesimäbiotooppi</legend>
			<a href="#" class="muuta" id="biotooppi_content">
				<#if updateparameters.tarkastus.pesa_biotooppi == ""> 
					(tyhjä) 
				<#else> 
					<#assign column = updateparameters.tarkastus.pesa_biotooppi> 
      	        	${selections[column.assosiatedSelection][column.value]}    
				</#if>
			</a> 
			</fieldset>
		</td>
	</tr>
	<tr>
		<td>
			<fieldset>
			<legend>Ympäröivä biotooppi</legend>
			<@selection selections "updateparameters" updateparameters.tarkastus.pesa_ymp_biotooppi 1 false true false errors />
			</fieldset>
		</td>
		<td>
			<fieldset>
			<legend>Varsinaisen biotoopin ala</legend>
			<@selection selections "updateparameters" updateparameters.tarkastus.pesa_biotooppi_ala 1 false true false errors />
			</fieldset>
		</td>
	</tr>
</table>


<table class="osio paaosio">
	<tr>
		<td>
			<fieldset>
			<legend>Kommentteja paikasta / yhdyskunnasta</legend>
			<@textarea "updateparameters" updateparameters.pesa.kommentti 3 80 errors /> 
			</fieldset>
		</td>
	</tr>
</table>





<h3> Yhdyskunnan tiedot </h3>

<table class="osio paaosio">
	<tr>
		<td>
			<fieldset>
			<legend>Yhdyskunnan laji</legend>
			<@inputSelectCombo "updateparameters" updateparameters.tarkastus.pesiva_laji true false errors /> 
			</fieldset>
		</td>
		<td>
			<fieldset>
			<legend>Yhdyskunnan koko</legend>
			<@inputText "updateparameters" updateparameters.tarkastus.yhdyskunta_koko true errors /> 
			</fieldset>
		</td>
	</tr>
</table>

<table class="osio" id="kayntitaulu">
	<caption>Pesät</caption>
	<thead>
	<tr>
	    <td colspan="6"> Päivämäärä: 
                 <@date "updateparameters" updateparameters.getColumn("kaynti.1.pvm") errors /> &nbsp;&nbsp;
                 <@selection selections "updateparameters" updateparameters.getColumn("kaynti.1.pvm_tarkkuus") 1 false true false errors />  
        </td>
       <td colspan="1"> Kellonaika 
                 <@inputText "updateparameters" updateparameters.getColumn("kaynti.1.klo_h") true errors />
                 . 
                 <@inputText "updateparameters" updateparameters.getColumn("kaynti.1.klo_min") true errors />
        </td>
    </tr>
	<tr>
		<th rowspan="2"> # </th>
		<th colspan="2"> Munien lkm </th>
		<th colspan="2"> Poikasten lkm </th>
		<th rowspan="2"> Pesinnän vaihe </th>
		<th rowspan="2"> Kommentteja </th>
	</tr>
	<tr>
		<th> Elävät </th>
		<th> Kuolleet </th>
		<th> Elävät </th>
		<th> Kuolleet </th>
	</tr>
	</thead>
	<tbody>
	<#assign lastRowToShow = 20>
	
	<#list 1..250 as i>
	  <#if updateparameters.getTableByName("kaynti."+i).hasValues()>
	    <#assign lastRowToShow = i>
	  </#if>
	</#list>
	
	<#if lastRowToShow lt 20>
		<#assign lastRowToShow = 20>
	</#if>
	
	<#list 1..50 as i>
		<#if i % 2 == 0> <tr class="odd" id="kaynti_${i}"> <#else> <tr id="kaynti_${i}"> </#if>
		
		<#if i != 1>
			<input type="hidden" class="hiddenYear" id="updateparameters.kaynti.${i}.pvm.yyyy" name="updateparameters.kaynti.${i}.pvm.yyyy" value="${updateparameters.getColumn("kaynti."+i+".pvm").dateValue.yyyy}" />
		</#if>
		
    	<td> ${i} </td>
        <td> <@inputText "updateparameters" updateparameters.getColumn("kaynti."+i+".lkm_munat") true errors /> </td>
        <td> <@inputText "updateparameters" updateparameters.getColumn("kaynti."+i+".lkm_munat_kuolleet") true errors /> </td>
        <td> <@inputText "updateparameters" updateparameters.getColumn("kaynti."+i+".lkm_poikaset") true errors /> </td>
        <td> <@inputText "updateparameters" updateparameters.getColumn("kaynti."+i+".lkm_poikaset_kuolleet") true errors /> </td>
        <td> 
        	<input type="text" name="PesinnanVaihe.${i}" value="${data["PesinnanVaihe."+i]!""}" size="3"
        	  <@checkerror errors "kaynti."+i+".pesinnan_vaihe" /> 
        	  <@checkerror errors "kaynti."+i+".ika_munat" />
        	  <@checkerror errors "kaynti."+i+".ika_poikaset" />
        	/>
        	<#if updateparameters.getColumn("kaynti."+i+".ika_munat")!?length gt 0>Munan ikä</#if>
        	<#if updateparameters.getColumn("kaynti."+i+".ika_poikaset")!?length gt 0>Poikasen ikä</#if>
        </td>
        <td> <@textarea "updateparameters"  updateparameters.getColumn("kaynti."+i+".kommentti") 1 60 errors />  </td>
	    </tr>
	</#list>
	</tbody>
</table>

<script type="text/javascript">

$(function() {
	
    var e = $('#updateparameters\\.pesa\\.suojelu_kaytto'); 
	if (e.val() == undefined) e.val('K');
	 
	// Jokaisella rivillä jolla on sisältyöä täytyy olla jokin varsinaisen tietokannan kentän arvo, jotta "pesinnän vaihe" kenttään syötetty ei-varsinainen
	// tietokannan kentän arvo välittyy formin mukana updateparametersiin. Siksi pistetään täysin käyttämättä jätetty vuosiarvo jokaiselle riville, jolla
	// on jotain sisältöä.
	var paivitaPiilotettuDummyMuttuja = function() {
		var year = $('#updateparameters\\.kaynti\\.1\\.pvm\\.yyyy').val();
		$("#kayntitaulu").find("tbody").find("tr").not(':first').each(function() {
			var hiddenYear = $(this).find(".hiddenYear").first();
			hiddenYear.val('');
			if (hasValues(this)) {
				hiddenYear.val(year);
			}
		});
	};
	
	$('#updateparameters\\.kaynti\\.1\\.pvm\\.yyyy').change(paivitaPiilotettuDummyMuttuja);
	paivitaPiilotettuDummyMuttuja();
	
	function hasValues(tr) {
		var hadValues = false;
		$(tr).find(":input").each(function() {
			if ($(this).val().length > 0) {
				hadValues = true;
				return;
			}
		});
		return hadValues;
	}
});	
</script>

<br />
	 