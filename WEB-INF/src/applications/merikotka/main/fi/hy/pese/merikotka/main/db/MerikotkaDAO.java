package fi.hy.pese.merikotka.main.db;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Vector;

import fi.hy.pese.framework.main.db.RowExistsResultHandler;
import fi.hy.pese.framework.main.db._DAO;
import fi.hy.pese.framework.main.db._ResultSet;
import fi.hy.pese.framework.main.db._ResultsHandler;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.main.general.data._Table;
import fi.hy.pese.merikotka.main.app.MerikotkaConst;
import fi.luomus.commons.utils.Utils;

public class MerikotkaDAO {

	private static final String	SUCCESFUL_NESTINGS_IN_REVIIRI_SQL	= succesfulNestingsInReviiriSql();

	private static String succesfulNestingsInReviiriSql() {
		StringBuilder successCodesSql = new StringBuilder();
		for (String code : MerikotkaConst.PESIMISTULOS_SUCCESS) {
			successCodesSql.append("'").append(code).append("'").append(",");
		}
		Utils.removeLastChar(successCodesSql);
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT vuosi.pesa_id                              ");
		sql.append(" FROM   vuosi, pesatarkastus                       ");
		sql.append(" WHERE  reviiri_id = ?                             ");
		sql.append(" AND    vuosi =  ?                                 ");
		sql.append(" AND    vuosi.pesa_id != ?                         ");
		sql.append(" AND    vuosi.pesa_id = pesatarkastus.pesa_id      ");
		sql.append(" AND    vuosi.vuosi   = pesatarkastus.tark_vuosi   ");
		sql.append(" AND    pesimistulos in (" + successCodesSql + ")  ");
		return sql.toString();
	}

	/**
	 * Returns a collection of nest IDs that are considered "dead"
	 * @param dao
	 * @return
	 * @throws SQLException
	 */
	public static Collection<String> deadNests(_DAO<_PesaDomainModelRow> dao) throws SQLException {
		StringBuilder query = new StringBuilder();
		query.append(" SELECT DISTINCT pesa_id FROM pesatarkastus WHERE pesa_kunto IN ( ");
		Utils.toCommaSeperatedStatement(query, MerikotkaConst.PESAN_KUNTO__DEAD, true);
		query.append(" ) ");

		DeadNestsQueryResultHandler resultHandler = new DeadNestsQueryResultHandler();
		dao.executeSearch(query.toString(), resultHandler);
		return resultHandler.deadNests();
	}

	private static class DeadNestsQueryResultHandler implements _ResultsHandler {
		private final Collection<String>	deadNests	= new HashSet<>();

		@Override
		public void process(_ResultSet rs) throws SQLException {
			while (rs.next()) {
				deadNests.add(rs.getString(1));
			}
		}

		public Collection<String> deadNests() {
			return deadNests;
		}
	}

	/**
	 * Returns the year nest has been marked dead, or null if it is still alive
	 * @param pesa_id
	 * @param dao
	 * @return year nest has been marked dead or null if still alive
	 * @throws SQLException
	 */
	public static Integer yearNestHasDied(String pesa_id, _DAO<_PesaDomainModelRow> dao) throws SQLException {
		StringBuilder query = new StringBuilder();
		query.append(" SELECT MIN(tark_vuosi) FROM pesatarkastus WHERE pesa_kunto IN ( ");
		Utils.toCommaSeperatedStatement(query, MerikotkaConst.PESAN_KUNTO__DEAD, true);
		query.append(" ) ").append(" AND pesa_id = ? ");
		YearNestHasDiedResultHandler resultHandler = new YearNestHasDiedResultHandler();
		dao.executeSearch(query.toString(), resultHandler, pesa_id);
		return resultHandler.yearNestHasDied;
	}

	private static class YearNestHasDiedResultHandler implements _ResultsHandler {

		public Integer yearNestHasDied = null;

		@Override
		public void process(_ResultSet rs) throws SQLException {
			if (!rs.next()) {
				yearNestHasDied = null;
				return;
			}
			String year = rs.getString(1);
			if (year == null) {
				yearNestHasDied = null;
				return;
			}
			yearNestHasDied = Integer.valueOf(year);
		}

	}

	/**
	 * Returns other successful nestings of the given reviiri on the given year (conflicts). If none found, returns an empty collection.
	 * There can only be one successful nesting per one reviiri for a given year.
	 * @param reviiri_id
	 * @param vuosi
	 * @param ignoreThisPesaid this nest ID is not included to the conflict (the nest that we are doing update/insert for; if the nesting itself is successful, that is ok as long as there are no other successful nestings)
	 * @param dao
	 * @return
	 * @throws SQLException
	 */
	public static Collection<String> findOutIfThereAreOtherSuccessfulNestingsForReviiri(String reviiri_id, String vuosi, String ignoreThisPesaid, _DAO<_PesaDomainModelRow> dao) throws SQLException {
		if (!given(ignoreThisPesaid)) {
			ignoreThisPesaid = "-1";
		}
		ReviiriConflictsResultHandler resultHandler = new ReviiriConflictsResultHandler();
		dao.executeSearch(SUCCESFUL_NESTINGS_IN_REVIIRI_SQL, resultHandler, reviiri_id, vuosi, ignoreThisPesaid);
		return resultHandler.conflicts();
	}

	private static boolean given(String s) {
		return s != null && s.length() > 0;
	}

	private static class ReviiriConflictsResultHandler implements _ResultsHandler {
		private final Collection<String>	conflicts	= new ArrayList<>(10);

		@Override
		public void process(_ResultSet rs) throws SQLException {
			while (rs.next()) {
				conflicts.add(rs.getString(1));
			}
		}

		public Collection<String> conflicts() {
			return conflicts;
		}
	}

	/**
	 * Updates reviiri kunta for all reviiri-vuosi-pesa -rows for a given reviiri on a given year
	 * NOTE: This is needed because of a fault with the original database design: it allows reviiri two belong to
	 * more than one kunta per year, for example:
	 * reviiri_id | pesa_id | vuosi | r_kunta
	 * 1|1|2000|HELSIN
	 * 1|2|2000|VANTAA
	 * @param reviiri_id
	 * @param vuosi
	 * @param r_kunta_id
	 * @param dao
	 * @throws SQLException
	 */
	public static void updateReviiriKunnatForVuosi(String reviiri_id, String vuosi, String r_kunta_id, _DAO<_PesaDomainModelRow> dao) throws SQLException {
		String sql = " UPDATE vuosi SET reviirin_kunta = ?, muutos_pvm = SYSDATE WHERE reviiri_id = ? AND vuosi= ? ";
		dao.executeUpdate(sql, r_kunta_id, reviiri_id, vuosi);
	}

	/**
	 * Returns the newest kunta_id that has been given to reviiri
	 * @param reviiri_id
	 * @return
	 * @throws SQLException
	 */
	public static String returnKuntaForReviiri(String reviiri_id, _DAO<_PesaDomainModelRow> dao) throws SQLException {
		_Table vuosi = dao.newRow().getVuosi();
		vuosi.get("reviiri_id").setValue(reviiri_id);
		ReviiriSearchResult resultHandler = new ReviiriSearchResult();
		dao.executeSearch(vuosi, resultHandler, "vuosi DESC");
		return resultHandler.kunta();
	}

	private static class ReviiriSearchResult implements _ResultsHandler {
		private String	kunta	= "";

		@Override
		public void process(_ResultSet rs) throws SQLException {
			if (!rs.next()) return;
			kunta = rs.getString("reviirin_kunta");
		}

		public String kunta() {
			return kunta;
		}
	}

	/**
	 * Checks if pesa has tarkastus for a given year
	 * @param pesa_id
	 * @param tark_vuosi
	 * @return true if tarkastus exists, false if not
	 * @throws SQLException
	 */
	public static boolean checkTarkastusForTheYearExists(String pesa_id, String tark_vuosi, _DAO<_PesaDomainModelRow> dao) throws SQLException {
		_Table vuosi = dao.newRow().getVuosi();
		vuosi.get("pesa_id").setValue(pesa_id);
		vuosi.get("vuosi").setValue(tark_vuosi);
		return dao.checkValuesExists(vuosi);
	}

	/**
	 * Checks if a reviiri with the given name already exists. If ignoredId is given, that reviiri is ignored.
	 * This is needed because we want reviiri names to be unique. Naturally when checking we want to ignore
	 * the reviiri in question itself.
	 * @param reviirinimi
	 * @param ignoreId
	 * @param dao
	 * @return true if (another) reviiri with same name exists
	 * @throws SQLException
	 */
	public static boolean reviirinimiExists(String reviirinimi, String ignoreId, _DAO<_PesaDomainModelRow> dao) throws SQLException {
		Vector<String> values = new Vector<>();
		String sql = " SELECT 1 FROM reviiri WHERE UPPER(reviirinimi) like ? ";
		values.add(reviirinimi.toUpperCase());
		if (ignoreId != null) {
			sql += " AND reviiri_id != ? ";
			values.add(ignoreId);
		}
		RowExistsResultHandler resultHandler = new RowExistsResultHandler();
		dao.executeSearch(sql, resultHandler, toArray(values));
		return resultHandler.rowExists;
	}

	private static String[] toArray(List<String> values) {
		return values.toArray(new String[values.size()]);
	}

}
