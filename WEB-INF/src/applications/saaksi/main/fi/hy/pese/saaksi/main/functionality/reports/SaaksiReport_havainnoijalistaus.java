package fi.hy.pese.saaksi.main.functionality.reports;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.general.ReportWriter;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.main.general.data._Row;
import fi.luomus.commons.tipuapi.TipuAPIClient;
import fi.luomus.commons.tipuapi.TipuApiResource;
import fi.luomus.commons.xml.Document.Node;

/**
 * Lists all people who are marked as "seur_tarkastaja" for nests that match the search criteria.
 */
public class SaaksiReport_havainnoijalistaus extends ReportWriter<_PesaDomainModelRow> {

	private static final String[]	FIELDS	= {
	                             	      	   "tarkastaja.numero", "tarkastaja.sukunimi", "tarkastaja.etunimi"
	};

	public SaaksiReport_havainnoijalistaus(_Request<_PesaDomainModelRow> request, String filenamePrefix) {
		super(request, filenamePrefix);
	}

	@Override
	public boolean produceSpecific() {
		headerLine();
		newLine();
		newLine();
		columnDescriptions();
		newLine();
		newLine();
		rows();
		return true;
	}

	private void rows() {
		Collection<Integer> havainnoijat = new HashSet<>();

		for (_Row row : request.data().results()) {
			if (row.getColumn("pesa.seur_rengastaja").hasValue()) {
				havainnoijat.add(row.getColumn("pesa.seur_rengastaja").getIntegerValue());
			}
		}

		List<Integer> sortedHavainnoijat = new LinkedList<>();
		sortedHavainnoijat.addAll(havainnoijat);
		Collections.sort(sortedHavainnoijat);

		for (Integer havainnoija : sortedHavainnoijat) {
			TipuApiResource ringers = request.getTipuApiResource(TipuAPIClient.RINGERS);
			if (!ringers.containsUnitById(Integer.toString(havainnoija))) {
				request.data().addToNoticeTexts("Havainoijaa " + havainnoija + " ei löytynyt Tipu-kannasta");
				continue;
			}
			Node ringer = ringers.getById(Integer.toString(havainnoija));
			write(havainnoija); separator();
			write(ringer.getNode("lastname").getContents()); separator();
			write(ringer.getNode("firstname").getContents()); separator();
			newLine();
		}
	}

	private void columnDescriptions() {
		for (String field : FIELDS) {
			write(shortNameFromUiTexts(field));
			separator();
		}
	}

}
