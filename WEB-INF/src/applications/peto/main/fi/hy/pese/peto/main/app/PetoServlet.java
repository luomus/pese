package fi.hy.pese.peto.main.app;

import fi.hy.pese.framework.main.app.PeSeServlet;
import fi.hy.pese.framework.main.app._ApplicationSpecificationFactory;
import fi.hy.pese.framework.main.general.data.PesaDomainModelDatabaseStructure;
import fi.hy.pese.framework.main.general.data.PesaDomainModelRowFactory;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.luomus.commons.db.connectivity.PreparedStatementStoringAndClosingTransactionConnection;
import fi.luomus.commons.db.connectivity.TransactionConnection;

public class PetoServlet extends PeSeServlet<_PesaDomainModelRow> {

	static final long	serialVersionUID	= 2525L;

	@Override
	protected _ApplicationSpecificationFactory<_PesaDomainModelRow> createFactory() throws Exception {
		PetoFactory factory = new PetoFactory("pese_peto.config");
		factory.loadUITexts();

		TransactionConnection con = null;
		try {
			con = new PreparedStatementStoringAndClosingTransactionConnection(factory.config().connectionDescription());
			PesaDomainModelDatabaseStructure structure = PetoDatabaseStructureCreator.loadFromDatabase(con);
			factory.setRowFactory(new PesaDomainModelRowFactory(structure, PetoConst.JOINS, PetoConst.ORDER_BY));
		} catch (Exception e) {
			throw new Exception(e);
		} finally {
			if (con != null) con.release();
		}
		return factory;
	}

}
