package fi.hy.pese.framework.main.general;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import fi.hy.pese.framework.main.app._ApplicationSpecificationFactory;
import fi.hy.pese.framework.main.db._DAO;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data._Row;
import fi.luomus.commons.containers.Selection;
import fi.luomus.commons.containers.Selection.SelectionOption;
import fi.luomus.commons.containers.SelectionImple;
import fi.luomus.commons.containers.SelectionOptionImple;
import fi.luomus.commons.tipuapi.TipuAPIClient;
import fi.luomus.commons.utils.SingleObjectCache;
import fi.luomus.commons.xml.Document.Node;

public class SelectionsLoader<T extends _Row> implements SingleObjectCache.CacheLoader<Map<String, Selection>> {
	private _ApplicationSpecificationFactory<T> factory;
	
	public SelectionsLoader(_ApplicationSpecificationFactory<T> factory) {
		this.factory = factory;
	}
	
	@Override
	public Map<String, Selection> load() {
		_DAO<T> dao = null;
		TipuAPIClient tipuAPI = null;
		Map<String, Selection> selections = new HashMap<>();
		try {
			dao = factory.dao(SelectionsLoader.class.getSimpleName());
			tipuAPI = factory.tipuAPI();
			try {
				selections = dao.returnCodeSelections("aputaulu", "taulu", "kentta", "arvo", "selite", "ryhmittely", new String[] {"jarjestys", "selite"});
			} catch (SQLException e) {
				try {
					selections = dao.returnCodeSelections("aputiedot", "taulu", "kentta", "arvo", "selite_suomi", "ryhmittely", new String[] {"jarjestys"});
				} catch (SQLException e2) {}
			}
			selections.put(TipuAPIClient.RINGERS, tipuAPI.getAsSelection(TipuAPIClient.RINGERS, 2));
			selections.put(TipuAPIClient.LAYMEN, tipuAPI.getAsSelection(TipuAPIClient.LAYMEN, 1));
			selections.put(TipuAPIClient.MUNICIPALITIES, tipuAPI.getAsSelection(TipuAPIClient.MUNICIPALITIES, 1));
			selections.put(TipuAPIClient.SPECIES, tipuAPI.getAsSelection(TipuAPIClient.SPECIES, 1));
			selections.put(TipuAPIClient.ELY_CENTRES, tipuAPI.getAsSelection(TipuAPIClient.ELY_CENTRES, 1));
			selections.put(TipuAPIClient.SCHEMES, tipuAPI.getAsSelection(TipuAPIClient.SCHEMES, 1));
			selections.put(TipuAPIClient.COUNTRIES, tipuAPI.getAsSelection(TipuAPIClient.COUNTRIES, 1));
			selections.put(TipuAPIClient.EURING_PROVINCES, tipuAPI.getAsSelection(TipuAPIClient.EURING_PROVINCES, 2, 1));
			rengastusCodesTo(selections, tipuAPI);

		} catch (Exception e) {
			throw new RuntimeException("loading selections", e);
		} finally {
			if (dao != null) factory.release(dao);
			if (tipuAPI != null) tipuAPI.close();
		}

		SelectionImple k_e = new SelectionImple(Const.K_E);
		k_e.addOption(new SelectionOptionImple("K", "Kyllä"));
		k_e.addOption(new SelectionOptionImple("E", "Ei"));
		selections.put(k_e.getName(), k_e);

		return selections;
	}

	private void rengastusCodesTo(Map<String, Selection> selections, TipuAPIClient tipuAPI) throws Exception {
		for (Node n : tipuAPI.getAsDocument(TipuAPIClient.CODES).getRootNode()) {
			String variable = TipuAPIClient.CODES +"_" + n.getNode("id").getContents();
			String code = n.getNode("code").getContents();
			String desc = n.getNode("desc").getContents();
			if (!selections.containsKey(variable)) {
				selections.put(variable, new SelectionImple(variable));
			}
			SelectionImple selection = (SelectionImple) selections.get(variable);
			SelectionOptionImple option = new SelectionOptionImple(code, desc);
			for (Node descNode : n.getChildNodes("desc")) {
				option.setText(descNode.getAttribute("lang"), descNode.getContents());
			}
			selection.addOption(option);
		}
		SelectionImple s = new SelectionImple("kontrolli.tapa");
		for (SelectionOption o : selections.get(TipuAPIClient.CODES + "_" + 20)) {
			s.addOption(o);
		}
		for (SelectionOption o : selections.get(TipuAPIClient.CODES + "_" + 74)) {
			s.addOption(o);
		}
		selections.put("kontrolli.tapa", s);
	}
	
}
