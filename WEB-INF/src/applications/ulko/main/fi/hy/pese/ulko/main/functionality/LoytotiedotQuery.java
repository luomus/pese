package fi.hy.pese.ulko.main.functionality;

import java.util.HashMap;
import java.util.Map;

import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.app.functionality.BaseFunctionality;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.ulko.main.app._UlkoDomainModelRow;
import fi.luomus.commons.containers.DateValue;
import fi.luomus.commons.tipuapi.TipuAPIClient;
import fi.luomus.commons.tipuapi.TipuApiResource;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.Utils;
import fi.luomus.commons.xml.Document.Node;
import fi.luomus.commons.xml.XMLWriter;

public class LoytotiedotQuery extends BaseFunctionality<_UlkoDomainModelRow> {

	private final Node values = new Node("data");

	public LoytotiedotQuery(_Request<_UlkoDomainModelRow> request) {
		super(request);
	}

	@Override
	public void afterSuccessfulValidation() throws Exception {
		process();
		String xml = new XMLWriter(values).generateXML();
		data.setXMLOutput(xml);
		data.setPage(Const.XML_OUTPUT_PAGE);
	}

	protected void process() {
		Map<String, String> lines = new HashMap<>();
		for (String line : data.get("data").split("\n")) {
			line = line.trim();
			if (line.length() < 1) continue;
			if (!line.contains(":")) continue;
			String field = line.split(":")[0];
			String value = line.replace(field+":", "");
			lines.put(field, value);
		}

		set("loyto.rengas_selite", lines.get("ring"));
		try {
			DateValue date = DateUtils.convertToDateValue(lines.get("date"));
			set("loyto.pvm.dd", date.getDay());
			set("loyto.pvm.mm", date.getMonth());
			set("loyto.pvm.yyyy", date.getYear());
		} catch (Exception e) {
			set("loyto.kommentti", lines.get("date"));
		}
		if ("Suomi".equals(lines.get("country"))) {
			set("loyto.maa", "SF");
		}

		String kunta = lines.get("municipality");
		if (given(kunta)) {
			kunta = Utils.removeWhitespace(kunta).toLowerCase();
			TipuApiResource municipalities = request.getTipuApiResource(TipuAPIClient.MUNICIPALITIES);
			Node municipality = getMunicipality(kunta, municipalities);
			if (municipality != null) {
				set("loyto.kunta", municipality.getNode("id").getContents());
				Node euringPlace = getEuringPlace(municipality, request.getTipuApiResource(TipuAPIClient.EURING_PROVINCES));
				if (euringPlace != null) {
					set("loyto.euring_paikka", euringPlace.getNode("id").getContents());
				}
			}
		}
		set("loyto.tarkempi_paikka", lines.get("locality"));
		set("loyto.kommentti", lines.get("coord"));
		set("loyto.kommentti", lines.get("status"));
		set("loyto.kommentti", lines.get("status_specified"));
		set("loyto.kommentti", lines.get("cause"));
		String laji = lines.get("species");
		if (given(laji)) {
			laji = Utils.removeWhitespace(laji).toLowerCase();
			TipuApiResource species = request.getTipuApiResource(TipuAPIClient.SPECIES);
			for (Node n : species) {
				String name = n.getNode("name").getContents();
				if (Utils.removeWhitespace(name.toLowerCase()).contains(laji)) {
					set("loyto.laji", n.getNode("id").getContents());
					break;
				}
			}
		}
		String sukupuoli = lines.get("sex");
		if ("koiras".equals(sukupuoli)) {
			set("loyto.sukupuoli", "K");
		} else if ("naaras".equals(sukupuoli)) {
			set("loyto.sukupuoli", "N");
		}
		set("pyynto.loytaja_nimi", lines.get("name"));
		set("pyynto.loytaja_osoite", lines.get("address"));
		set("pyynto.loytaja_email", lines.get("email"));
		set("pyynto.loytaja_puhelin", lines.get("phone"));
		set("pyynto.kommentti", lines.get("info_finder"));
	}

	protected Node getMunicipality(String kunta, TipuApiResource municipalities) {
		for (Node municipality : municipalities) {
			String name = municipality.getNode("name").getContents();
			if (Utils.removeWhitespace(name.toLowerCase()).contains(kunta)) {
				return municipality;
			}
		}
		return null;
	}

	protected static Node getEuringPlace(Node municipality, TipuApiResource euringPlaces) {
		String laaniteksti = Utils.removeWhitespace(municipality.getNode("old-county").getAttribute("name").toLowerCase().replace("finland", ""));
		for (Node euringPlace : euringPlaces) {
			String province = Utils.removeWhitespace(euringPlace.getNode("province").getContents().toLowerCase());
			if (laaniteksti.contains(province)) {
				return euringPlace;
			}
		}
		return null;
	}

	protected boolean given(String string) {
		return string != null && string.trim().length() > 0;
	}

	protected void set(String field, String value) {
		if (!given(value)) return;
		field = Const.UPDATEPARAMETERS+"."+field;
		if (values.hasChildNodes(field)) {
			Node n = values.getNode(field);
			n.setContents(n.getContents() + " \n " + value);
		} else {
			values.addChildNode(field).setContents(value);
		}
	}



}
