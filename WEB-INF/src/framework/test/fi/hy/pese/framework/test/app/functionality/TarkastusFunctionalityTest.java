package fi.hy.pese.framework.test.app.functionality;

import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.Map;

import fi.hy.pese.framework.main.app.BaseFactoryForApplicationSpecificationFactory;
import fi.hy.pese.framework.main.app.FunctionalityExecutor;
import fi.hy.pese.framework.main.app.Request;
import fi.hy.pese.framework.main.app.RequestImpleData;
import fi.hy.pese.framework.main.app._Functionality;
import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.app.functionality.TarkastusFunctionality;
import fi.hy.pese.framework.main.app.functionality.validate.Validator;
import fi.hy.pese.framework.main.app.functionality.validate._Validator;
import fi.hy.pese.framework.main.db._DAO;
import fi.hy.pese.framework.main.db._ResultsHandler;
import fi.hy.pese.framework.main.db.oraclesql.OracleSQL_DAO;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data.Data;
import fi.hy.pese.framework.main.general.data.DatabaseTableStructure;
import fi.hy.pese.framework.main.general.data.PesaDomainModelDatabaseStructure;
import fi.hy.pese.framework.main.general.data.PesaDomainModelRow;
import fi.hy.pese.framework.main.general.data.PesaDomainModelRowFactory;
import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._Data;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.main.general.data._RowFactory;
import fi.hy.pese.framework.main.general.data._Table;
import fi.hy.pese.framework.test.TestConfig;
import fi.luomus.commons.config.Config;
import fi.luomus.commons.containers.Selection;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class TarkastusFunctionalityTest {

	public static Test suite() {
		return new TestSuite(TarkastusFunctionalityTest.class.getDeclaredClasses());
	}

	public static class FillingTheFormForUpdate extends TestCase {

		private class TarkastusFunctionalityTestFactory extends BaseFactoryForApplicationSpecificationFactory<_PesaDomainModelRow> {
			public TarkastusFunctionalityTestFactory(String configFileName) throws FileNotFoundException {
				super(configFileName);
			}

			@Override
			public _Functionality<_PesaDomainModelRow> functionalityFor(String page, _Request<_PesaDomainModelRow> request) {
				return new TarkastusFunctionality(request) {

					@Override
					public void applicationSpecificDatamanipulation() {
						data.setUpdateparameters(data.results().get(0));
					}

					@Override
					protected boolean prevOlosuhdeIsForThisYear(_Table currentTarkastus, _Table prevOlosuhde) {
						return false;
					}

					@Override
					protected void doInsert() throws Exception {}

					@Override
					protected void doInsertFirst() throws Exception {}

					@Override
					protected void doUpdate() throws Exception {}

				};
			}

			@Override
			public _DAO<_PesaDomainModelRow> dao(String userId) {
				return new TarkastusFunctionalityFakeDAO();
			}

			@Override
			public Config config() {
				try {
					return TestConfig.getConfig();
				} catch (Exception e) {
					fail();
					e.printStackTrace();
				}
				return null;
			}

			@Override
			protected Map<String, Selection> fetchSelections(_DAO<_PesaDomainModelRow> dao) throws Exception {
				return null;
			}
		}

		private class TarkastusFunctionalityFakeDAO extends OracleSQL_DAO<_PesaDomainModelRow> {

			public TarkastusFunctionalityFakeDAO() {
				super(null, rowFactory);
			}

			@Override
			public _Table returnTableByKeys(String tablename, String ... keys) throws SQLException {
				if (tablename.equals("tarkastus")) {
					if (keys[0].equals("1")) {
						_Table t = this.newTableByName(tablename);
						t.getUniqueNumericIdColumn().setValue("1");
						t.get("pesaid").setValue("1");
						return t;
					}
				}
				throw new SQLException("not defined");
			}

			@Override
			public void executeSearch(_PesaDomainModelRow parameters, _ResultsHandler handler, String... additional) throws SQLException {
				_Data<_PesaDomainModelRow> resultdata = data;
				if (parameters.getTarkastus().getUniqueNumericIdColumn().getValue().equals("1")) {
					_PesaDomainModelRow result = resultdata.newResultRow();
					result.getColumn("pesa.nimi").setValue("Pesa 1");
				} else if (parameters.getPesa().getUniqueNumericIdColumn().getValue().equals("1")) {
					_PesaDomainModelRow result = resultdata.newResultRow();
					result.getColumn("pesa.nimi").setValue("Pesa 1");
					result.getColumn("tarkastus.tarkastusid").setValue("5");
					result = resultdata.newResultRow();
					result.getColumn("pesa.nimi").setValue("Pesa 1");
					result.getColumn("tarkastus.tarkastusid").setValue("1");
				}
			}

			@Override
			public void executeSearch(_Table parameters, _ResultsHandler handler, String... orderBy) throws SQLException {

			}

			@Override
			public boolean checkRowExists(_Table table) {
				return true;
			}

		}

		private _Data<_PesaDomainModelRow>				data;
		private _Functionality<_PesaDomainModelRow>		tarkastusFunctionality;
		private PesaDomainModelDatabaseStructure	structure;
		private _RowFactory<_PesaDomainModelRow> rowFactory;

		@Override
		protected void setUp() throws Exception {

			DatabaseTableStructure tarkastus = new DatabaseTableStructure("tarkastus");
			tarkastus.addColumn("tarkastusid", _Column.INTEGER, 5);
			tarkastus.addColumn("pesaid", _Column.INTEGER, 5);
			tarkastus.get("tarkastusid").setTypeToUniqueNumericIncreasingId();

			DatabaseTableStructure pesa = new DatabaseTableStructure("pesa");
			pesa.addColumn("pesaid", _Column.INTEGER, 5);
			pesa.addColumn("nimi", _Column.VARCHAR, 25);
			pesa.get("pesaid").setTypeToUniqueNumericIncreasingId();

			tarkastus.get("pesaid").references(pesa.getUniqueNumericIdColumn());

			structure = new PesaDomainModelDatabaseStructure();
			structure.setTarkastus(tarkastus);
			structure.setPesa(pesa);

			rowFactory = new PesaDomainModelRowFactory(structure, "", "");
			data = new Data<>(rowFactory);

			TarkastusFunctionalityTestFactory factory = new TarkastusFunctionalityTestFactory(null);
			// factory.loadDatabaseStructure(structure);
			RequestImpleData<_PesaDomainModelRow> reqData = new RequestImpleData<>();
			reqData.setData(data).setDao(factory.dao("userid")).setApplication(factory);
			_Request<_PesaDomainModelRow> request = new Request<>(reqData);
			data.setPage(Const.TARKASTUS_PAGE);
			data.setAction(Const.FILL_FOR_UPDATE);
			tarkastusFunctionality = factory.functionalityFor(Const.TARKASTUS_PAGE, request);
		}

		public void test__fill_for_update__when_given_tarkastusid_it_fills_data_to_updateparameters() throws Exception {
			data.updateparameters().getTarkastus().getUniqueNumericIdColumn().setValue("1");
			FunctionalityExecutor.execute(tarkastusFunctionality);
			assertEquals("Pesa 1", data.updateparameters().getPesa().get("nimi").getValue());
		}

		public void test____fill_for_update__doesnt_accept_updateparameters_that_dont_produce_results() throws Exception {
			data.updateparameters().getTarkastus().getUniqueNumericIdColumn().setValue("99999");
			try {
				FunctionalityExecutor.execute(tarkastusFunctionality);
				fail("Should throw exception");
			} catch (SQLException e) {
			}
		}

		public void test____fill_for_update__if_given_pesaid__fills_latest_tarkastus_info() throws Exception {
			data.updateparameters().getPesa().getUniqueNumericIdColumn().setValue("1");
			FunctionalityExecutor.execute(tarkastusFunctionality);
			assertEquals("Pesa 1", data.updateparameters().getPesa().get("nimi").getValue());
			assertEquals("5", data.updateparameters().getTarkastus().get("tarkastusid").getValue());
		}

		public void test___fill_for_update___after_filling_sets_action_to_update() throws Exception {
			data.updateparameters().getPesa().getUniqueNumericIdColumn().setValue("1");
			FunctionalityExecutor.execute(tarkastusFunctionality);
			assertEquals(Const.UPDATE, data.action());
		}

	}

	public static class FillingTheFormForInsert extends TestCase {

		private class TarkastusFunctionalityTestValidator extends Validator<_PesaDomainModelRow> {
			public TarkastusFunctionalityTestValidator(_Request<_PesaDomainModelRow> request) {
				super(request);
			}

			@Override
			public void validate() throws Exception {
				if (!data.updateparameters().getColumn("pesa.pesaid").getValue().equals("1")) error("test", "testerror");
			}
		}

		private class TarkastusFunctionalityTestFactory extends BaseFactoryForApplicationSpecificationFactory<_PesaDomainModelRow>  {
			public TarkastusFunctionalityTestFactory(String configFileName) throws FileNotFoundException {
				super(configFileName);
			}

			@Override
			public _Functionality<_PesaDomainModelRow> functionalityFor(String page, _Request<_PesaDomainModelRow> request) {
				return new TarkastusFunctionality(request) {
					@Override
					public _Validator validator() {
						return new TarkastusFunctionalityTestValidator(request);
					}

					@Override
					public void afterFailedValidation() throws Exception {
						_Data<_PesaDomainModelRow> data = request.data();
						data.setPage(Const.SEARCH_PAGE);
						data.setAction(Const.SEARCH);
					}

					@Override
					public void applicationSpecificDatamanipulation() {
						data.setUpdateparameters(data.results().get(0));
						data.updateparameters().getTarkastus().clearAllValues();
					}

					@Override
					protected boolean prevOlosuhdeIsForThisYear(_Table currentTarkastus, _Table prevOlosuhde) {
						return false;
					}

					@Override
					protected void doInsert() throws Exception {}

					@Override
					protected void doInsertFirst() throws Exception {}

					@Override
					protected void doUpdate() throws Exception {}

				};
			}

			@Override
			public _DAO<_PesaDomainModelRow> dao(String userId) {
				return new TarkastusFunctionalityFakeDAO();
			}

			@Override
			public Config config() {
				try {
					return TestConfig.getConfig();
				} catch (Exception e) {
					fail();
					e.printStackTrace();
				}
				return null;
			}

			@Override
			protected Map<String, Selection> fetchSelections(_DAO<_PesaDomainModelRow> dao) throws Exception {
				return null;
			}
		}

		private class TarkastusFunctionalityFakeDAO extends OracleSQL_DAO<_PesaDomainModelRow> {
			public TarkastusFunctionalityFakeDAO() {
				super(null, null);
			}

			@Override
			public void executeSearch(_PesaDomainModelRow parameters, _ResultsHandler handler, String... additional) throws SQLException {
				_PesaDomainModelRow resultrow = data.newResultRow();
				resultrow.getColumn("pesa.nimi").setValue("testipesa");
				resultrow.getColumn("tarkastus.tarkastusid").setValue("24");
				resultrow.getColumn("tarkastus.pesaid").setValue("1");
			}

			@Override
			public void executeSearch(_Table parameters, _ResultsHandler handler, String... orderBy) throws SQLException {}

			@Override
			public _PesaDomainModelRow newRow() {
				_PesaDomainModelRow row = new PesaDomainModelRow(structure);
				return row;
			}
		}

		private _Data<_PesaDomainModelRow>				data;
		private _Functionality<_PesaDomainModelRow>		tarkastusFunctionality;
		private PesaDomainModelDatabaseStructure	structure;

		@Override
		protected void setUp() throws Exception {

			DatabaseTableStructure tarkastus = new DatabaseTableStructure("tarkastus");
			tarkastus.addColumn("tarkastusid", _Column.INTEGER, 5);
			tarkastus.addColumn("pesaid", _Column.INTEGER, 5);
			tarkastus.get("tarkastusid").setTypeToUniqueNumericIncreasingId();

			DatabaseTableStructure pesa = new DatabaseTableStructure("pesa");
			pesa.addColumn("pesaid", _Column.INTEGER, 5);
			pesa.addColumn("nimi", _Column.VARCHAR, 25);
			pesa.get("pesaid").setTypeToUniqueNumericIncreasingId();

			structure = new PesaDomainModelDatabaseStructure();
			structure.setTarkastus(tarkastus);
			structure.setPesa(pesa);
			_RowFactory<_PesaDomainModelRow> rowF = new PesaDomainModelRowFactory(structure, "", "");
			data = new Data<>(rowF);

			TarkastusFunctionalityTestFactory factory = new TarkastusFunctionalityTestFactory(null);
			factory.setRowFactory(rowF);
			RequestImpleData<_PesaDomainModelRow> reqData = new RequestImpleData<>();
			reqData.setData(data).setDao(factory.dao("userid")).setApplication(factory);
			_Request<_PesaDomainModelRow> request = new Request<>(reqData);
			data.setPage(Const.TARKASTUS_PAGE);
			data.setAction(Const.FILL_FOR_INSERT);
			tarkastusFunctionality = factory.functionalityFor(Const.TARKASTUS_PAGE, request);
		}

		public void test__fill_for_insert____calls_application_specific_validation() throws Exception {
			FunctionalityExecutor.execute(tarkastusFunctionality);
			assertTrue("should have errors", data.errors().size() > 0);
			assertEquals("testerror", data.errors().get("test"));
		}

		public void test__fill_for_insert__after_failed_validation_executes_application_specific_functionality() throws Exception {
			FunctionalityExecutor.execute(tarkastusFunctionality);
			assertTrue("should have errors", data.errors().size() > 0);
			assertEquals(Const.SEARCH_PAGE, data.page());
			assertEquals(Const.SEARCH, data.action());
		}

		public void test__fill_for_insert__after_successful_validation_fills_previous_insepection_to_data() throws Exception {
			data.updateparameters().getColumn("pesa.pesaid").setValue("1");
			FunctionalityExecutor.execute(tarkastusFunctionality);
			assertFalse("no errors", data.errors().size() > 0);
			assertEquals("testipesa", data.updateparameters().getColumn("pesa.nimi").getValue());
		}

		public void test__fill_for_instert__after_succ_validation_sets_action_to_insert() throws Exception {
			data.updateparameters().getColumn("pesa.pesaid").setValue("1");
			FunctionalityExecutor.execute(tarkastusFunctionality);
			assertFalse("no errors", data.errors().size() > 0);
			assertEquals("testipesa", data.updateparameters().getColumn("pesa.nimi").getValue());
			assertEquals(Const.INSERT, data.action());
		}

		public void test__fills_for_insert____removes_those_values_that_are_not_wanted_to_be_prefilled() throws Exception {
			data.updateparameters().getColumn("pesa.pesaid").setValue("1");
			FunctionalityExecutor.execute(tarkastusFunctionality);
			assertFalse("no errors", data.errors().size() > 0);
			assertEquals("", data.updateparameters().getColumn("tarkastus.tarkastusid").getValue());
			assertEquals("", data.updateparameters().getColumn("tarkastus.pesaid").getValue());
		}
	}
}
