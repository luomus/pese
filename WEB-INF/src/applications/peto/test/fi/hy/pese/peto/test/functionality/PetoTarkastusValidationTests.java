package fi.hy.pese.peto.test.functionality;

import java.util.HashMap;
import java.util.Map;

import fi.hy.pese.framework.main.app.Request;
import fi.hy.pese.framework.main.app.RequestImpleData;
import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.app.functionality.validate._Validator;
import fi.hy.pese.framework.main.db._DAO;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data.Data;
import fi.hy.pese.framework.main.general.data.PesaDomainModelDatabaseStructure;
import fi.hy.pese.framework.main.general.data.PesaDomainModelRowFactory;
import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._Data;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.test.db.DAOStub;
import fi.hy.pese.peto.main.app.PetoConst;
import fi.hy.pese.peto.main.app.PetoDatabaseStructureCreator;
import fi.hy.pese.peto.main.app.PetoFactory;
import fi.hy.pese.peto.main.functionality.validation.PetoTarkastusValidator;
import fi.luomus.commons.containers.Selection;
import fi.luomus.commons.containers.SelectionImple;
import fi.luomus.commons.containers.SelectionOptionImple;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class PetoTarkastusValidationTests {

	public static Test suite() {
		return new TestSuite(PetoTarkastusValidationTests.class.getDeclaredClasses());
	}

	public static class WhenSendingTheForm extends TestCase {

		public class FakeDAO<T extends _PesaDomainModelRow> extends DAOStub<T> {

			public FakeDAO() {}

		}

		private _Data<_PesaDomainModelRow>				data;
		private _Validator			validator;
		private _PesaDomainModelRow				params;
		private _DAO<_PesaDomainModelRow>				dao;
		private final PesaDomainModelDatabaseStructure	databaseStructure = PetoDatabaseStructureCreator.hardCoded();
		private _Request<_PesaDomainModelRow>		request;
		private final Map<String,Selection> selections = setSelections();

		private static Map<String, Selection> setSelections() {
			Map<String, Selection> selections = new HashMap<>();

			SelectionImple k_e = new SelectionImple(PetoConst.K_E);
			k_e.addOption(new SelectionOptionImple("K", "Kyllä"));
			k_e.addOption(new SelectionOptionImple("E", "Ei"));
			selections.put(k_e.getName(), k_e);

			newSelection(selections, "pesa.puu_laji", "K", "M");
			newSelection(selections, "tarkastus.pesa_sijainti", "K");
			newSelection(selections, "tarkastus.puu_elavyys", "E", "O", "K");
			return selections;
		}

		private static void newSelection(Map<String, Selection> selections, String name, String ... values) {
			SelectionImple selection = new SelectionImple(name);
			for (String value : values) {
				selection.addOption(new SelectionOptionImple(value, value));
			}
			selections.put(selection.getName(), selection);
		}

		@Override
		protected void setUp() throws Exception {
			data = new Data<>(new PesaDomainModelRowFactory(databaseStructure, PetoConst.JOINS, PetoConst.ORDER_BY));
			data.setPage(Const.TARKASTUS_PAGE);
			data.setAction(Const.INSERT_FIRST);
			data.setSelections(selections);
			dao = new FakeDAO<>();
			PetoFactory factory = new PetoFactory(null);
			factory.setRowFactory(new PesaDomainModelRowFactory(databaseStructure, PetoConst.JOINS, PetoConst.ORDER_BY));
			RequestImpleData<_PesaDomainModelRow> reqData = new RequestImpleData<_PesaDomainModelRow>().setData(data).setDao(dao);
			request = new Request<>(reqData);
			validator = new PetoTarkastusValidator(request, null);
			params = data.updateparameters();
			params.getTarkastus().get("tarkastettu").setValue("K");
		}

		@Override
		protected void tearDown() throws Exception {}

		public void test__() throws Exception {
			validator.validate();
		}

		public void test__loyt_vuosi_tark_vuosi() throws Exception {
			column("tarkastus.vuosi").setValue("2000");
			column("pesa.loyt_vuosi").setValue("2000");
			validator.validate();
			assertNull(error("tarkastus.vuosi"));
			assertNull(error("pesa.loyt_vuosi"));

			validator = new PetoTarkastusValidator(request, null);
			column("tarkastus.vuosi").setValue("2010");
			column("pesa.loyt_vuosi").setValue("2000");
			validator.validate();
			assertNull(error("tarkastus.vuosi"));
			assertNull(error("pesa.loyt_vuosi"));

			validator = new PetoTarkastusValidator(request, null);
			column("tarkastus.vuosi").setValue("1999");
			column("pesa.loyt_vuosi").setValue("2000");
			validator.validate();
			assertNull(error("tarkastus.vuosi"));
			assertNull(error("pesa.loyt_vuosi"));

			validator = new PetoTarkastusValidator(request, null);
			column("tarkastus.vuosi").setValue("1998");
			column("pesa.loyt_vuosi").setValue("2000");
			validator.validate();
			assertEquals(null, warning("tarkastus.vuosi"));
			assertEquals(PetoConst.INVALID_LOYT_VUOSI, warning("pesa.loyt_vuosi"));
		}

		public void test__luonnokolo_puussa() throws Exception {
			column("pesa.puu_laji").setValue("M");
			column("tarkastus.pesa_sijainti").setValue("K");
			column("tarkastus.puu_elavyys").setValue("E");
			validator.validate();
			assertEquals(null, error("pesa.puu_laji"));
			assertEquals(null, error("tarkastus.pesa_sijainti"));
			assertEquals(null, error("tarkastus.puu_elavyys"));
		}

		public void test__luonnokolo_puussa_2() throws Exception {
			column("pesa.puu_laji").setValue("");
			column("tarkastus.pesa_sijainti").setValue("K");
			column("tarkastus.puu_elavyys").setValue("E");
			validator.validate();
			assertEquals(PetoConst.PUULAJI_ILMOITETTAVA_JOS_ELAVYYS_ILMOITETTU, error("pesa.puu_laji"));
			assertEquals(null, error("tarkastus.pesa_sijainti"));
			assertEquals(null, error("tarkastus.puu_elavyys"));
		}

		private String warning(String column) {
			return validator.warnings().get(column);
		}

		private String error(String column) {
			return validator.errors().get(column);
		}

		private _Column column(String column) {
			return params.getColumn(column);
		}

	}

}
