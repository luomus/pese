package fi.hy.pese.framework.main.general.data;

/**
 * Database reference. Column C in table T is referenced by ReferenceKey {'referringTable', 'referringTableColumn'}
 */
public interface _ReferenceKey {

	/**
	 * Name of the database table referred
	 * @return
	 */
	String table();

	/**
	 * Name of the database column referred
	 * @return
	 */
	String column();

	/**
	 * Checks if names of given column match with this reference key
	 * @param c database column
	 * @return true if names match, false if not
	 */
	boolean equals(_Column c);

	/**
	 * Checks if given string matches the table and column name of this reference key
	 * @param s string to be matched
	 * @return true if names match, false if not
	 */
	boolean equals(String s);
}
