
   
        </div>

      </div>
    </div>
  </div>
  
  <div id="footer">
      <a href="http://www.helsinki.fi/yliopisto/">
        <img src="https://rengastus.helsinki.fi/images/shared/hy-logo3kielta.gif" id="footerLogo" alt="Helsingin yliopisto (logo)" />
      </a>
      <p>
      	<a href="http://www.fmnh.helsinki.fi/elainmuseo/rengastus/index.htm">Luonnontieteellisen keskusmuseon rengastustoimisto</a>
      	|
      	<a href="http://www.fmnh.helsinki.fi/elainmuseo/rengastus/tietosuojaseloste.htm">Tietosuojaseloste</a>
      	|
      	<a href="http://www.fmnh.helsinki.fi/info/palaute/yleinen.asp">Palaute</a>
      	|
      	<a href="http://www.fmnh.helsinki.fi/elainmuseo/yhteystiedot/rengastustoimisto.htm">Yhteystiedot</a>
      </p>
	
      <p>
      	&copy; 2009 Helsingin yliopisto 
      </p>
	
    </div>
		
</body>
</html>
    

