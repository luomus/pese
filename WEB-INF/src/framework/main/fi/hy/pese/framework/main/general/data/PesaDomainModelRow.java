package fi.hy.pese.framework.main.general.data;

public class PesaDomainModelRow extends Row implements _PesaDomainModelRow {

	private static final long	serialVersionUID	= 7810292514029270765L;
	private final Table					pesa;
	private final Table					olosuhde;
	private final Table					tarkastus;
	private final Table					kaynti;
	private final IndexedTablegroup		kaynnit;
	private final Table					vuosi;
	private final Table					reviiri;
	private final Table					poikanen;
	private final IndexedTablegroup		poikaset;
	private final Table					aikuinen;
	private final IndexedTablegroup		aikuiset;
	private final Table					muna;
	private final IndexedTablegroup		munat;

	public PesaDomainModelRow(PesaDomainModelDatabaseStructure rowStructure) {
		this(rowStructure, null);
	}

	public PesaDomainModelRow(PesaDomainModelDatabaseStructure rowStructure, String prefix) {
		super(rowStructure, prefix);

		pesa = new Table(rowStructure.getPesa());
		olosuhde = new Table(rowStructure.getOlosuhde());
		tarkastus = new Table(rowStructure.getTarkastus());
		kaynti = new Table(rowStructure.getKaynti());
		vuosi = new Table(rowStructure.getVuosi());
		reviiri = new Table(rowStructure.getReviiri());
		poikanen = new Table(rowStructure.getPoikanen());
		aikuinen = new Table(rowStructure.getAikuinen());
		muna = new Table(rowStructure.getMuna());

		kaynnit = new IndexedTablegroup(rowStructure.getKaynti(), rowStructure.getKaynnitCount());
		poikaset = new IndexedTablegroup(rowStructure.getPoikanen(), rowStructure.getPoikasetCount());
		aikuiset = new IndexedTablegroup(rowStructure.getAikuinen(), rowStructure.getAikuisetCount());
		munat = new IndexedTablegroup(rowStructure.getMuna(), rowStructure.getMunatCount());

		addGroupLink(kaynti, kaynnit);
		addGroupLink(poikanen, poikaset);
		addGroupLink(aikuinen, aikuiset);
		addGroupLink(muna, munat);

		defineBasetable(pesa);
		defineBasetable(olosuhde);
		defineBasetable(tarkastus);
		defineBasetable(vuosi);
		defineBasetable(reviiri);
		defineBasetable(kaynti);
		defineBasetable(poikanen);
		defineBasetable(aikuinen);
		defineBasetable(muna);

		constructBasetables(rowStructure);
	}

	@Override
	public Table getPesa() {
		return pesa;
	}

	@Override
	public Table getOlosuhde() {
		return olosuhde;
	}

	@Override
	public Table getTarkastus() {
		return tarkastus;
	}

	@Override
	public Table getKaynti() {
		return kaynti;
	}

	@Override
	public Table getVuosi() {
		return vuosi;
	}

	@Override
	public Table getReviiri() {
		return reviiri;
	}

	@Override
	public Table getPoikanen() {
		return poikanen;
	}

	@Override
	public Table getMuna() {
		return muna;
	}

	@Override
	public Table getAikuinen() {
		return aikuinen;
	}

	@Override
	public _IndexedTablegroup getKaynnit() {
		return kaynnit;
	}

	@Override
	public _IndexedTablegroup getPoikaset() {
		return poikaset;
	}

	@Override
	public _IndexedTablegroup getAikuiset() {
		return aikuiset;
	}

	@Override
	public _IndexedTablegroup getMunat() {
		return munat;
	}

}
