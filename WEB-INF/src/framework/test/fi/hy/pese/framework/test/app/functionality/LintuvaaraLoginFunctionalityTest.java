package fi.hy.pese.framework.test.app.functionality;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fi.hy.pese.framework.main.app.BaseFactoryForApplicationSpecificationFactory;
import fi.hy.pese.framework.main.app.Control;
import fi.hy.pese.framework.main.app._Functionality;
import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.app.functionality.LintuvaaraLoginFunctionality;
import fi.hy.pese.framework.main.db._DAO;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data.Data;
import fi.hy.pese.framework.main.general.data.HistoryAction;
import fi.hy.pese.framework.main.general.data.ParameterMap;
import fi.hy.pese.framework.main.general.data._Data;
import fi.hy.pese.framework.main.general.data._Row;
import fi.hy.pese.framework.main.kirjekyyhky._KirjekyyhkyXMLGenerator;
import fi.hy.pese.framework.main.view._ExceptionViewer;
import fi.hy.pese.framework.main.view._Viewer;
import fi.hy.pese.framework.test.RowFactory;
import fi.hy.pese.framework.test.db.DAOStub;
import fi.luomus.commons.config.Config;
import fi.luomus.commons.containers.Selection;
import fi.luomus.commons.db.connectivity.ConnectionDescription;
import fi.luomus.commons.kirjekyyhky.KirjekyyhkyAPIClient;
import fi.luomus.commons.languagesupport.LocalizedTextsContainer;
import fi.luomus.commons.pdf.PDFWriter;
import fi.luomus.commons.reporting.ErrorReporter;
import fi.luomus.commons.session.SessionHandler;
import fi.luomus.commons.tipuapi.TipuAPIClient;
import fi.luomus.commons.tipuapi.TipuApiResource;
import fi.luomus.commons.utils.AESDecryptor;
import fi.luomus.commons.utils.AESDecryptor.AESDecryptorInitializationException;
import fi.luomus.commons.utils.Utils;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class LintuvaaraLoginFunctionalityTest {

	public static Test suite() {
		return new TestSuite(LintuvaaraLoginFunctionalityTest.class.getDeclaredClasses());
	}

	/**
	 * See documentation for different cases, 1-6
	 */
	public static class WhenExecutingLintuvaaraLoginFunctionality extends TestCase {

		private static final String	LINTUVAARA_IHKU_TESTVERSION_KEY	= "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDsBtrkGf4cQzZazUvKGgiVOOPC"
				+ "nM0tdzvVrmXUlDL4e+bNJs8uA+IsddoQKrYxJOQyDCw3SEv6aVXAzDKW1ypAbpoQ"
				+ "7jl3CIylqWasujW50zWb9Ki4QFDPz0Mhz2jTwyzLyCon08yBvQhmbKoZcLTVL+l8" + "7BY4umyhc8zaDL5FJwIDAQAB";
		private static final String	SOMEPAGE						= "somepage";
		private static final String	THIS_APPLICATIONS_SYSTEM_ID		= "petoseuranta";
		private static final String	THIS_APPLICATIONS_BASE_URL		= "https://this.apps.URL/Servl";
		private static final String	THIS_APPLICATIONS_FRONTPAGE		= "FrontpageForThisApplication";
		private static final String	LINTUVAARA_URL					= "https://lintuvaara.URL";

		private boolean				developementMode;
		private boolean				giveWrongSystemId;

		private Control<_Row>		control;
		private FakeHttpRedirected	redirecter;
		private OutputStream		out;
		private FakeErrorReporter	errorReporter;
		private SessionHandler		session;

		private class TestFactory extends BaseFactoryForApplicationSpecificationFactory<_Row> {
			public TestFactory() throws FileNotFoundException {
				super();
			}

			@Override
			public _Viewer viewer() throws IOException {
				return new _Viewer() {

					@Override
					public void viewFor(_Request<? extends _Row> request) throws Exception {
						PrintWriter out = request.out();
						out.write("Page output for page: " + request.data().page());
						out.close();
					}
				};
			}

			@Override
			public LocalizedTextsContainer uiTexts() {
				return new LocalizedTextsContainer() {
					@Override
					public String getText(String text, String language) throws IllegalArgumentException {
						return null;
					}

					@Override
					public Map<String, String> getAllTexts(String language) throws IllegalArgumentException {
						return new HashMap<>();
					}
				};
			}

			@Override
			public Map<String, Selection> selections(_DAO<_Row> dao) throws SQLException {
				return null;
			}

			@Override
			public void reloadUITexts() throws Exception {}

			@Override
			public PDFWriter pdfWriter() {
				return null;
			}

			@Override
			public _Functionality<_Row> functionalityFor(String page, _Request<_Row> request) throws UnsupportedOperationException {
				AESDecryptor decryptor = null;
				try {
					decryptor = new AESDecryptor(LINTUVAARA_IHKU_TESTVERSION_KEY);
				} catch (AESDecryptorInitializationException e) {
					e.printStackTrace();
				}
				if (page.equals(Const.LOGIN_PAGE)) { return new LintuvaaraLoginFunctionality<>(request, decryptor, errorReporter); }
				if (page.equals(THIS_APPLICATIONS_FRONTPAGE)) { return new EmptyFunctionality<>(); }
				if (page.equals(SOMEPAGE)) { return new EmptyFunctionality<>(); }
				throw new UnsupportedOperationException();
			}

			@Override
			public String frontpage() {
				return THIS_APPLICATIONS_FRONTPAGE;
			}

			@Override
			public _ExceptionViewer exceptionViewer(_Request<_Row> request) {
				return new _ExceptionViewer() {
					@Override
					public void view(Throwable condition) {
						condition.printStackTrace();
						fail("Caused exception: " + condition.getMessage());
					}
				};
			}


			@Override
			public _DAO<_Row> dao(String userId) throws SQLException {
				return new DAOStub<>();
			}

			@Override
			public Config config() {
				return new Config() {
					@Override
					public String templateFolder() throws UnsupportedOperationException {
						return null;
					}

					@Override
					public String systemId() throws UnsupportedOperationException {
						if (giveWrongSystemId) { return "THIS-IS-SYSTEMID-FOR-ANOTHER-SYSTEM"; }
						return THIS_APPLICATIONS_SYSTEM_ID;
					}

					@Override
					public String reportFolder() throws UnsupportedOperationException {
						return null;
					}

					@Override
					public String pdfFolder() throws UnsupportedOperationException {
						return null;
					}

					@Override
					public boolean parameterLogging() {
						return false;
					}

					@Override
					public String logFolder() throws UnsupportedOperationException {
						return null;
					}

					@Override
					public String languagefileFolder() throws UnsupportedOperationException {
						return null;
					}

					@Override
					public String get(String property) throws IllegalArgumentException {
						if (property.equals(Config.LINTUVAARA_URL)) { return WhenExecutingLintuvaaraLoginFunctionality.LINTUVAARA_URL; }
						return null;
					}

					@Override
					public boolean developmentMode() {
						return developementMode;
					}

					@Override
					public boolean defines(String property) {
						return false;
					}

					@Override
					public String defaultLanguage() throws UnsupportedOperationException {
						return null;
					}

					@Override
					public ConnectionDescription connectionDescription() throws UnsupportedOperationException {
						return null;
					}

					@Override
					public String characterEncoding() {
						return null;
					}

					@Override
					public String baseURL() throws UnsupportedOperationException {
						return THIS_APPLICATIONS_BASE_URL;
					}

					@Override
					public String baseFolder() throws UnsupportedOperationException {
						return null;
					}

					@Override
					public Collection<String> supportedLanguages() throws UnsupportedOperationException {
						Collection<String> c = new HashSet<>();
						c.add("FI");
						return c;
					}

					@Override
					public String commonURL() throws UnsupportedOperationException {
						return null;
					}

					@Override
					public String kirjekyyhkyFolder() throws UnsupportedOperationException {
						return null;
					}

					@Override
					public boolean stagingMode() {
						return false;
					}

					@Override
					public int getInteger(String property) throws IllegalArgumentException, NumberFormatException {
						return 0;
					}

					@Override
					public String fontFolder() throws UnsupportedOperationException {
						return null;
					}

					@Override
					public ConnectionDescription connectionDescription(String systemName) throws UnsupportedOperationException {
						return null;
					}

					@Override
					public String languageFilePrefix() throws UnsupportedOperationException {
						return null;
					}

					@Override
					public String staticURL() throws UnsupportedOperationException {
						return null;
					}

					@Override
					public List<String> languageFiles() throws UnsupportedOperationException {
						return null;
					}

					@Override
					public boolean productionMode() {
						// Auto-generated method stub
						return false;
					}

					@Override
					public String dataFolder() throws UnsupportedOperationException {
						// Auto-generated method stub
						return null;
					}
				};
			}

			@Override
			public _Data<_Row> initData(String language, ParameterMap params) throws Exception {
				Data<_Row> data = new Data<>(new RowFactory());
				params.convertTo(data);
				return data;
			}

			@Override
			public void release(_DAO<_Row> dao) {}

			@Override
			public _KirjekyyhkyXMLGenerator kirjekyyhkyFormDataXMLGenerator(_Request<_Row> request) throws Exception {
				return null;
			}

			@Override
			public KirjekyyhkyAPIClient kirjekyyhkyAPI() throws Exception {
				return null;
			}

			@Override
			public boolean usingKirjekyyhky() {

				return false;
			}

			@Override
			public void loadKirjekyyhkyCount(_Request<_Row> request) {


			}

			@Override
			public void addHistoryAction(HistoryAction action) {

			}

			@Override
			public List<HistoryAction> getHistoryActions() {
				return null;
			}

			@Override
			public void saveHistoryActions() {
			}

			@Override
			public void loadHistoryActions() {

			}

			@Override
			public TipuAPIClient tipuAPI() throws Exception {
				return null;
			}

			@Override
			public PDFWriter pdfWriter(String outputFolder) {
				return null;
			}

			@Override
			public boolean usingTipuApi() {
				return false;
			}

			@Override
			public TipuApiResource getTipuApiResource(String resourcename) {
				return null;
			}

			@Override
			public void reportException(String message, Throwable condition) {

			}

			@Override
			public void postProsessingHook(_Request<_Row> request) {

			}

			@Override
			protected Map<String, Selection> fetchSelections(_DAO<_Row> dao) throws Exception {
				return null;
			}

		}

		private class FakeErrorReporter implements ErrorReporter {
			List<String>	reportedErrors	= new ArrayList<>();

			@Override
			public void report(String message, Throwable condition) {
				//condition.printStackTrace();
				reportedErrors.add(condition.getMessage());
			}

			@Override
			public void report(String message) {
				//System.err.println(message);
				reportedErrors.add(message);
			}
			@Override
			public String toString() {
				return reportedErrors.toString();
			}

			@Override
			public void report(Throwable condition) {
				report("", condition);
			}

		}

		@Override
		protected void setUp() throws Exception {
			session = new FakeSessionHandler();
			developementMode = true;
			giveWrongSystemId = false;
			errorReporter = new FakeErrorReporter();
			out = new ByteArrayOutputStream();
			redirecter = new FakeHttpRedirected();
			control = new Control<>(new TestFactory());
		}

		private class FakeSessionHandler implements SessionHandler {
			Set<String>		authenticatedFor	= new HashSet<>();
			private String	userid				= null;
			private String	usertype			= null;
			private String	username			= null;

			@Override
			public void authenticateFor(String system) throws IllegalStateException {
				authenticatedFor.add(system);
			}

			@Override
			public boolean isAuthenticatedFor(String system) throws IllegalStateException {
				return authenticatedFor.contains(system);
			}

			@Override
			public void removeAuthentication(String system) throws IllegalStateException {}

			@Override
			public String get(String key) throws IllegalStateException {
				return null;
			}

			@Override
			public String language() throws IllegalStateException {
				return null;
			}

			@Override
			public void put(String key, String value) throws IllegalStateException {}

			@Override
			public void remove(String key) throws IllegalStateException {}

			@Override
			public void setLanguage(String language) throws IllegalStateException {}

			@Override
			public void setUserId(String userid) throws IllegalStateException {
				this.userid = userid;
			}

			@Override
			public void setUserType(String usertype) throws IllegalStateException {
				this.usertype = usertype;
			}

			@Override
			public String userId() throws IllegalStateException {
				return userid;
			}

			@Override
			public String userType() throws IllegalStateException {
				return usertype;
			}

			@Override
			public void setUserName(String username) throws IllegalStateException {
				this.username = username;
			}

			@Override
			public String userName() throws IllegalStateException {
				return username;
			}

			@Override
			public void setFlash(String message) {
			}

			@Override
			public String getFlash() {
				return null;
			}

			@Override
			public Object getObject(String key) {
				return null;
			}

			@Override
			public void setObject(String key, Object value) {

			}

			@Override
			public boolean hasSession() {
				return false;
			}

			@Override
			public void setFlashError(String message) {
				// Auto-generated method stub

			}

			@Override
			public String getFlashError() {
				// Auto-generated method stub
				return null;
			}

			@Override
			public void setFlashSuccess(String message) {
				// Auto-generated method stub

			}

			@Override
			public String getFlashSuccess() {
				// Auto-generated method stub
				return null;
			}

			@Override
			public void setTimeout(int minutes) {
				// Auto-generated method stub

			}

			@Override
			public void invalidate() {
				// Auto-generated method stub

			}
		}

		public void test___case_1___not_authenticated___no__params() {
			Map<String, String> params = new HashMap<>();
			control.process(new ParameterMap(params), session, new PrintWriter(out), redirecter, null);
			//System.out.println(out.toString());
			//System.out.println(errorReporter.reportedErrors.toString());
			assertEquals(true, redirecter.isHttpStatusCodeSet());
			assertEquals(LINTUVAARA_URL + "/sessions?service=petoseuranta", redirecter.redirectedTo);
			assertEquals(0, errorReporter.reportedErrors.size());
			assertEquals(false, session.isAuthenticatedFor(THIS_APPLICATIONS_SYSTEM_ID));
		}

		public void test___case_2__not_authenticated__incorrect_params_given() {
			Map<String, String> params = new HashMap<>();
			params.put("key", "foo");
			params.put("iv", "foofoo");
			params.put("data", "foofoofoo");
			control.process(new ParameterMap(params), session, new PrintWriter(out), redirecter, null);
			// assertEquals("", out.toString());
			assertEquals(true, redirecter.isHttpStatusCodeSet());
			assertEquals(LINTUVAARA_URL + "/sessions?service=petoseuranta", redirecter.redirectedTo);
			assertEquals(1, errorReporter.reportedErrors.size());
			String expected = "" + "Lintuvaara Login Failed for application " + THIS_APPLICATIONS_SYSTEM_ID + " : \n" + "AES Decryption failed: java.security.InvalidKeyException: Unwrapping failed";
			String expected2 =  "\n" + "key:  foo \n" + "iv:   foofoo \n" + "data: foofoofoo \n";
			assertTrue(errorReporter.reportedErrors.get(0).startsWith(expected));
			assertTrue(errorReporter.reportedErrors.get(0).endsWith(expected2));
			assertEquals(false, session.isAuthenticatedFor(THIS_APPLICATIONS_SYSTEM_ID));
		}

		public void test___case_3___not_authenticated___correct_params_given() {
			Map<String, String> params = new HashMap<>();
			putCorrectLoginParamsTo(params);
			control.process(new ParameterMap(params), session, new PrintWriter(out), redirecter, null);
			//Utils.debug(errorReporter.reportedErrors);
			assertEquals(0, errorReporter.reportedErrors.size()); // If this fails make sure Unlimited Strength Java(TM) Cryptography Extension Policy Files are installed
			// assertEquals("", out.toString());
			assertEquals(true, redirecter.isHttpStatusCodeSet());
			assertEquals(THIS_APPLICATIONS_BASE_URL, redirecter.redirectedTo);
			assertEquals(true, session.isAuthenticatedFor(THIS_APPLICATIONS_SYSTEM_ID));
			assertEquals("admin_ep", session.userId());
			assertEquals("Esko Piirainen", session.userName());
		}

		public void test___case_3_2___not_authenticated___correct_params_given___NOT_in_developement_mode() {
			developementMode = false;
			Map<String, String> params = new HashMap<>();
			putCorrectLoginParamsTo(params);
			control.process(new ParameterMap(params), session, new PrintWriter(out), redirecter, null);
			assertEquals(1, errorReporter.reportedErrors.size());
			String expected = "" + "Lintuvaara Login Failed for application petoseuranta : \n" + "Usermodel has expired \n" + "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
					+ "<login type=\"admin\">\n" + "  <login_id>admin_ep</login_id>\n" + "  <name>Esko Piirainen</name>\n" + "  <email>esko.piirainen@cs.helsinki.fi</email>\n"
					+ "  <expires_at>1279479746</expires_at>\n" + "  <auth_for>petoseuranta</auth_for>\n" + "</login>\n" + "\n";
			assertEquals(expected, errorReporter.reportedErrors.get(0));
			// assertEquals("", out.toString());
			assertEquals(true, redirecter.isHttpStatusCodeSet());
			assertEquals(LINTUVAARA_URL + "/sessions?service=petoseuranta", redirecter.redirectedTo);
			assertEquals(false, session.isAuthenticatedFor(THIS_APPLICATIONS_SYSTEM_ID));
		}

		public void test___case_3_3___not_authenticated___correct_params_given___but_for_wrong_service() {
			giveWrongSystemId = true;
			Map<String, String> params = new HashMap<>();
			putCorrectLoginParamsTo(params);
			control.process(new ParameterMap(params), session, new PrintWriter(out), redirecter, null);
			assertEquals(1, errorReporter.reportedErrors.size());
			String expected = "" + "Lintuvaara Login Failed for application THIS-IS-SYSTEMID-FOR-ANOTHER-SYSTEM : \n" + "Usermodel was not intended for this application \n"
					+ "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" + "<login type=\"admin\">\n" + "  <login_id>admin_ep</login_id>\n" + "  <name>Esko Piirainen</name>\n"
					+ "  <email>esko.piirainen@cs.helsinki.fi</email>\n" + "  <expires_at>1279479746</expires_at>\n" + "  <auth_for>petoseuranta</auth_for>\n" + "</login>\n"
					+ "\n";
			assertEquals(expected, errorReporter.reportedErrors.get(0));
			// assertEquals("", out.toString());
			assertEquals(true, redirecter.isHttpStatusCodeSet());
			assertEquals(LINTUVAARA_URL + "/sessions?service=THIS-IS-SYSTEMID-FOR-ANOTHER-SYSTEM", redirecter.redirectedTo);
			assertEquals(false, session.isAuthenticatedFor(THIS_APPLICATIONS_SYSTEM_ID));
		}

		public void test___case_4___not_authenticated___correct_params_given___url_given() {
			Map<String, String> params = new HashMap<>();
			putCorrectLoginParamsTo(params);
			String url = Utils.urlEncode(THIS_APPLICATIONS_BASE_URL + "?page=search&searchparameters.pesa.id=1");
			params.put(LintuvaaraLoginFunctionality.SERVICE_URI, url);
			control.process(new ParameterMap(params), session, new PrintWriter(out), redirecter, null);
			assertEquals(0, errorReporter.reportedErrors.size());
			// assertEquals("", out.toString());
			assertEquals(true, redirecter.isHttpStatusCodeSet());
			assertEquals(THIS_APPLICATIONS_BASE_URL + "?page=search&searchparameters.pesa.id=1", redirecter.redirectedTo);
			assertEquals(true, session.isAuthenticatedFor(THIS_APPLICATIONS_SYSTEM_ID));
			assertEquals("admin_ep", session.userId());
			assertEquals("Esko Piirainen", session.userName());
		}

		private void putCorrectLoginParamsTo(Map<String, String> params) {
			String key = "m5k4AbmLZXyWf9sPHo1s2wivSY7ktT%2BS2Oci%2FkdLpi7wS%2F7dTgDs5iPe1n3l%0AbLStGiepy7lD22QX7GcMTxwBdM9jAjQnFHlM5mhJxWktXzTE75X12aH2yCrX%0AOgT3L67hKaXcjOcFnWL3VMja6HvsWIkPAzmKIX8odVgECkdJ%2FKo%3D%0A";
			String iv = "F7iZxLQ1ZsaXMhJUhim1T4WXQR3urXHtQO81yB3HG7Im9yHa3cjH%2FKUW4ucr%0AMlvLzJ%2BYTDeKD%2Be4U7ETbxsc2MqiEY9jKui1ZbI7wy1zw47J2dJrcJggHQk8%0A7u1MGpB8wccOa7%2B69v5Q1WKcpNoNZNLZXh5icQ6XvdJn%2FZGpHcg%3D%0A";
			String data = "3kdFwbeqlcHFbWRg05ucn5aIo14L8Ayh%2FN7p2Ub1hPOYESRbpIfUW%2FF%2BiPWN%0AAEs4Mx3YRWmuPztCOe3wtC3oODaLCTJWc%2BQDxrohqyGOy7e9eul8tI6KEnQh%0AeUvtyY9i55BEvgfxqeOmZPssjsLs4NmsBsbDEqnioUKkYU8NnROv8Cg7ZSXZ%0AtST1jyB0Et7wXE5iokakGtyDISBXCAqESZ52hyf4g%2FMQEA4bhksDeHyzWMIJ%0AtLxtaAi9ddwsymkIx60zBzLRaYe2TJ2XVcR%2BIOUft8%2BKiMLPPxHtU%2B5IBElb%0A8GHvBxhzEP8vaMm87S%2F8Z844OkPsNncPx5dtLumUaQ%3D%3D%0A";
			params.put(LintuvaaraLoginFunctionality.KEY, Utils.urlDecode(key));
			params.put(LintuvaaraLoginFunctionality.IV, Utils.urlDecode(iv));
			params.put(LintuvaaraLoginFunctionality.DATA, Utils.urlDecode(data));
		}

		public void test__case_5__already_authenticated___no_page_given__no_redirecturl_given() {
			Map<String, String> params = new HashMap<>();
			session.authenticateFor(THIS_APPLICATIONS_SYSTEM_ID);
			session.setUserId("foobar");
			control.process(new ParameterMap(params), session, new PrintWriter(out), redirecter, null);
			assertEquals(0, errorReporter.reportedErrors.size());
			assertEquals("Page output for page: " + THIS_APPLICATIONS_FRONTPAGE, out.toString());
			assertEquals(false, redirecter.isHttpStatusCodeSet());
			assertEquals(true, session.isAuthenticatedFor(THIS_APPLICATIONS_SYSTEM_ID));
			assertEquals("foobar", session.userId());
		}

		public void test__case_5_2__already_authenticated__some_page_given___no_redirect_url_given() {
			Map<String, String> params = new HashMap<>();
			params.put(Const.PAGE, SOMEPAGE);
			session.authenticateFor(THIS_APPLICATIONS_SYSTEM_ID);
			session.setUserId("foobar");
			control.process(new ParameterMap(params), session, new PrintWriter(out), redirecter, null);
			assertEquals(0, errorReporter.reportedErrors.size());
			assertEquals("Page output for page: " + SOMEPAGE, out.toString());
			assertEquals(false, redirecter.isHttpStatusCodeSet());
			assertEquals(true, session.isAuthenticatedFor(THIS_APPLICATIONS_SYSTEM_ID));
			assertEquals("foobar", session.userId());
		}

		public void test__case_5_3__already_authenticated__login_page_given___no_redirect_url_given() {
			Map<String, String> params = new HashMap<>();
			params.put(Const.PAGE, Const.LOGIN_PAGE);
			session.authenticateFor(THIS_APPLICATIONS_SYSTEM_ID);
			session.setUserId("barfoo");
			control.process(new ParameterMap(params), session, new PrintWriter(out), redirecter, null);
			assertEquals(0, errorReporter.reportedErrors.size());
			assertEquals("Page output for page: " + THIS_APPLICATIONS_FRONTPAGE, out.toString());
			assertEquals(false, redirecter.isHttpStatusCodeSet());
			assertEquals(true, session.isAuthenticatedFor(THIS_APPLICATIONS_SYSTEM_ID));
			assertEquals("barfoo", session.userId());
		}

		public void test__case_5_4___already_authenticated___login_params_given__no_page_given() {
			Map<String, String> params = new HashMap<>();
			params.put(Const.PAGE, Const.LOGIN_PAGE);
			putCorrectLoginParamsTo(params);
			session.authenticateFor(THIS_APPLICATIONS_SYSTEM_ID);
			session.setUserId("admin_foobar");
			control.process(new ParameterMap(params), session, new PrintWriter(out), redirecter, null);
			assertEquals(0, errorReporter.reportedErrors.size());
			assertEquals(true, redirecter.isHttpStatusCodeSet());
			assertEquals(THIS_APPLICATIONS_BASE_URL, redirecter.redirectedTo);
			assertEquals(true, session.isAuthenticatedFor(THIS_APPLICATIONS_SYSTEM_ID));
			assertEquals("admin_foobar", session.userId());
		}

	}

}
