package fi.hy.pese.rengastus.main.app;

import fi.hy.pese.framework.main.app.ApiServiceFactory;
import fi.hy.pese.framework.main.app.ApiServlet;
import fi.hy.pese.framework.main.app._ApplicationSpecificationFactory;
import fi.luomus.commons.db.connectivity.PreparedStatementStoringAndClosingTransactionConnection;
import fi.luomus.commons.db.connectivity.TransactionConnection;

public class RengastusApiServlet extends ApiServlet<_RengastusDomainModelRow> {

	private static final long	serialVersionUID	= -1898038058767299318L;

	@Override
	protected _ApplicationSpecificationFactory<_RengastusDomainModelRow> createFactory() throws Exception {
		ApiServiceFactory<_RengastusDomainModelRow> factory = new ApiServiceFactory<>("tulospalvelu_rengastus.config");

		TransactionConnection con = new PreparedStatementStoringAndClosingTransactionConnection(factory.config().connectionDescription());
		try {
			RengastusDatabaseStructure structure = RengastusDatabaseStructureCreator.loadFromDatabase(con);
			factory.setRowFactory(new RengastusDomainModelRowFactory(structure));
		} finally {
			con.release();
		}

		factory.loadUITexts();

		return factory;
	}



}
