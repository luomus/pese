package fi.hy.pese.merikotka.main.functionality;

import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.app.functionality.TablemanagementFunctionality;
import fi.hy.pese.framework.main.app.functionality.validate._Validator;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.merikotka.main.functionality.validation.MerikotkaTablemanagementValidator;

/**
 * Same functionality as TablemanagementFunctionality, but the validator validates reviiri name
 * if managing reviiri -table
 */
public class MerikotkaTableManagementFunctionality extends TablemanagementFunctionality<_PesaDomainModelRow> {

	public MerikotkaTableManagementFunctionality(_Request<_PesaDomainModelRow> request) {
		super(request);
	}

	@Override
	public _Validator validator() {
		return new MerikotkaTablemanagementValidator(request);
	}

}
