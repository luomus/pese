package fi.hy.pese.merikotka.main.functionality;

import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import fi.hy.pese.framework.main.app.functionality.SearchFunctionality;
import fi.hy.pese.framework.main.db.FirstResultRowToTableHandler;
import fi.hy.pese.framework.main.db.FirstResultRowToTableHandler.NoResultsException;
import fi.hy.pese.framework.main.db.RowsToListResultHandler;
import fi.hy.pese.framework.main.db._DAO;
import fi.hy.pese.framework.main.db._ResultSet;
import fi.hy.pese.framework.main.db._ResultsHandler;
import fi.hy.pese.framework.main.general._PDFDataMapGenerator;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.main.general.data._Table;
import fi.hy.pese.merikotka.main.app.MerikotkaConst;
import fi.hy.pese.merikotka.main.db.MerikotkaDAO;
import fi.hy.pese.merikotka.test.MerikotkaDataToMapForPDFPrintingTest;
import fi.luomus.commons.containers.Selection;
import fi.luomus.commons.pdf.PDFWriter;
import fi.luomus.commons.utils.Utils;

public class MerikotkaDataToMapForPDFPrintingGenerator implements _PDFDataMapGenerator {

	private final _DAO<_PesaDomainModelRow>	dao;
	private final Map<String, Selection>	selections;
	private final SearchFunctionality		searchFunctionality;
	private final Map<String, String>		uiTexts;
	private final Collection<String>		deadNests;

	public MerikotkaDataToMapForPDFPrintingGenerator(_DAO<_PesaDomainModelRow> dao, Map<String, Selection> selections, SearchFunctionality searchFunctionality, Map<String, String> uiTexts) throws Exception {
		this.dao = dao;
		this.searchFunctionality = searchFunctionality;
		this.selections = selections;
		this.uiTexts = uiTexts;
		this.deadNests =  MerikotkaDAO.deadNests(dao);
	}

	/**
	 * This function is called to build the application specific data for PDF printing (esitäytetty tarkastuslomake).
	 * Basicly it takes values from database and puts them to a map. The map is later given to pdfWriter, which will
	 * look for values from the map that match the names of the fields on the pdf.
	 * For example for meritkotka pdf there are several places where the year of inspection is printed out, so those
	 * are filled to the map to keys "tark_vuosi_1", "tark_vuosi_2"... and so on.
	 * @see MerikotkaDataToMapForPDFPrintingTest
	 */
	@Override
	public Map<String, String> tarkastusDataFor(String pesaid, String year) throws Exception {
		Map<String, String> map = new HashMap<>();

		tarkVuosiToWhereItIsNeeded(year, map);

		_Table prev_tarkastus = previousTarkastusOnlyInspected(pesaid);
		prevTarkastajaToMap(map, prev_tarkastus);
		prevPesimistulosToMap(map, prev_tarkastus);
		prevPesaKuntoToMap(map, prev_tarkastus);
		prevTarkPvmToMap(map, prev_tarkastus);

		_PesaDomainModelRow prev_tarkastus_row = previousTarkastusFor(pesaid);
		prevReviiriInfoToMap(pesaid, year, map, prev_tarkastus_row);
		replace9999withGt1000(prev_tarkastus_row);
		MerikotkaConst.removeTarkastusValuesButKeepSpecific(prev_tarkastus_row.getTarkastus());
		everythingFromRowToMap(map, prev_tarkastus_row);
		coordinatesToMap(map, prev_tarkastus_row);
		luonnonpesaVsTekopesaToMap(map, prev_tarkastus_row);
		pesanimiForSeconPageToMap(pesaid, map, prev_tarkastus_row);

		generateFilenameToMap(pesaid, year, map, prev_tarkastus_row);

		return map;
	}

	private Selection	pesimistulokset	= null;

	private Selection pesimistuloksetSelection() {
		if (pesimistulokset == null) {
			pesimistulokset = selections.get(dao.newRow().getTarkastus().get("pesimistulos").getAssosiatedSelection());
		}
		return pesimistulokset;
	}

	/**
	 * This is a legacy thing; the current system no longer supports >1000 <-> 9999, but there are plenty of
	 * 9999 values in the database
	 * @param prev_row
	 */
	private void replace9999withGt1000(_PesaDomainModelRow prev_row) {
		for (String s : MerikotkaConst.ETAISYYS_FIELDS) {
			_Column c = prev_row.getColumn(s);
			if (c.getValue().equals("9999")) c.setValue(">1000");
		}
	}

	private void pesanimiForSeconPageToMap(String pesaid, Map<String, String> map, _PesaDomainModelRow result) {
		_Table pesa = result.getPesa();
		String pesanimi = pesa.get("pesanimi").getValue() + " (" + pesaid + ")";
		map.put(MerikotkaConst.PESA_PESANIMI_2, pesanimi);
	}

	private void luonnonpesaVsTekopesaToMap(Map<String, String> map, _PesaDomainModelRow result) {
		String teko_rak_v = result.getPesa().get("rak_vuosi_teko").getValue();
		if (teko_rak_v.equals("")) {
			map.put(MerikotkaConst.PESA_LUONNONPESA_K_E, "K");
			map.put(MerikotkaConst.PESA_TEKOPESA_K_E, "E");
		} else {
			map.put(MerikotkaConst.PESA_LUONNONPESA_K_E, "E");
			map.put(MerikotkaConst.PESA_TEKOPESA_K_E, "K");
		}
	}

	private void coordinatesToMap(Map<String, String> map, _PesaDomainModelRow result) {
		_Table pesa = result.getPesa();
		String koord_tyyppi = pesa.get("koord_tyyppi").getValue();
		String leveys = "";
		String pituus = "";
		if (koord_tyyppi.equals("Y")) {
			map.put(MerikotkaConst.PESA_KOORD_TYYPPI_YHT, PDFWriter.SELECTED_CHECKBOX);
			leveys = pesa.get("yht_leveys").getValue();
			pituus = pesa.get("yht_pituus").getValue();
		} else if (koord_tyyppi.equals("E")) {
			map.put(MerikotkaConst.PESA_KOORD_TYYPPI_EUR, PDFWriter.SELECTED_CHECKBOX);
			leveys = pesa.get("eur_leveys").getValue();
			pituus = pesa.get("eur_pituus").getValue();
		} else if (koord_tyyppi.equals("A")) {
			map.put(MerikotkaConst.PESA_KOORD_TYYPPI_AST, PDFWriter.SELECTED_CHECKBOX);
			leveys = pesa.get("ast_leveys").getValue();
			pituus = pesa.get("ast_pituus").getValue();
		}
		map.put(MerikotkaConst.PESA_KOORD_LEVEYS, leveys);
		map.put(MerikotkaConst.PESA_KOORD_PITUUS, pituus);
	}

	private class ReviiriListResultHandler implements _ResultsHandler {

		private final String		pdfPesaid;
		private final String		pdfVuosi;

		public final StringBuilder	reviiriList			= new StringBuilder();
		public final StringBuilder	reviiriList_k_e		= new StringBuilder();
		public String				bestPesimistulos	= "";
		public String				bestPesa			= "";
		public String				bestPoikasCount		= "";
		private final String		reviirilista_kylla_ei_text;
		private final String		reviirilista_ei_vaihtopesia_text;

		public ReviiriListResultHandler(String pdfForPesaid, String pdfForVuosi, Map<String, String> uiTexts) {
			this.pdfPesaid = pdfForPesaid;
			this.pdfVuosi = pdfForVuosi;
			reviirilista_ei_vaihtopesia_text = uiTexts.get(MerikotkaConst.REVIIRILISTA_EI_VAIHTOPESIA_TEXT);
			reviirilista_kylla_ei_text = uiTexts.get(MerikotkaConst.REVIIRILISTA_KYLLA_EI_TEXT);
		}

		@Override
		public void process(_ResultSet rs) throws SQLException {
			boolean first = true;
			while (rs.next()) {
				String pesaid = rs.getString(1);
				if (deadNests.contains(pesaid)) continue;
				if (!pesaid.equals(pdfPesaid)) {
					if (!first) {
						reviiriList.append("\n");
						reviiriList_k_e.append("\n");
					} else
						first = false;
					StringBuilder pesatext = new StringBuilder(pesaid).append(": ").append(rs.getString(2));
					if (pesatext.length() > MerikotkaConst.REVIIRILISTA_MAX_LENGTH) {
						pesatext.delete(MerikotkaConst.REVIIRILISTA_MAX_LENGTH, pesatext.length());
					}
					reviiriList.append(pesatext);
					reviiriList_k_e.append(reviirilista_kylla_ei_text);
				}
				String tark_vuosi = rs.getString(7);
				if (tark_vuosi.equals(pdfVuosi)) {
					String thisPesimistulos = rs.getString(3);
					if (MerikotkaConst.firstPesimistulosBetterThanSecond(thisPesimistulos, bestPesimistulos)) {
						bestPesimistulos = thisPesimistulos;
						bestPesa = pesaid;
						bestPoikasCount = Utils.maxOf(rs.getString(4), rs.getString(5), rs.getString(6));
					}
				}
			}
			if (reviiriList.length() < 1) reviiriList.append("\n\n ").append(reviirilista_ei_vaihtopesia_text);
		}
	}

	private void prevReviiriInfoToMap(String pesaid, String year, Map<String, String> map, _PesaDomainModelRow result) throws SQLException {
		String prev_year = Integer.toString(Integer.valueOf(year) - 1);
		ReviiriListResultHandler resultHandler = new ReviiriListResultHandler(pesaid, prev_year, uiTexts);
		String sql =    " SELECT    vuosi.pesa_id, pesanimi, pesimistulos, elavia_lkm, lentopoik_lkm, reng_poik_lkm, vuosi.vuosi " +
				" FROM      vuosi, pesavakio, pesatarkastus                    " +
				" WHERE     vuosi.pesa_id = pesavakio.pesa_id                  " +
				" AND       vuosi.pesa_id = pesatarkastus.pesa_id              " +
				" AND       vuosi.vuosi = pesatarkastus.tark_vuosi             " +
				" AND       vuosi.reviiri_id = ?                               " +
				" AND       vuosi.vuosi = ( select max(vuosi) from vuosi v2    " +
				"                           where v2.pesa_id = vuosi.pesa_id ) " +
				" ORDER BY  pesanimi                                           ";
		String reviiri_id = result.getVuosi().get("reviiri_id").getValue();
		dao.executeSearch(sql, resultHandler, reviiri_id);

		map.put(MerikotkaConst.REVIIRILISTA, resultHandler.reviiriList.toString());
		map.put(MerikotkaConst.REVIIRILISTA_K_E, resultHandler.reviiriList_k_e.toString());

		map.put(MerikotkaConst.PREV_PARAS_TARKASTUS_VUOSI, prev_year);
		map.put(MerikotkaConst.PREV_PARAS_TARKASTUS_PESA_ID, resultHandler.bestPesa);

		StringBuilder prev_pesimistulos_text = buildPesimistulosText(resultHandler.bestPesimistulos, resultHandler.bestPoikasCount);
		map.put(MerikotkaConst.PREV_PARAS_TARKASTUS_PESIMISTULOS, prev_pesimistulos_text.toString());
	}

	private StringBuilder buildPesimistulosText(String pesimistulos, String poikasCount) {
		StringBuilder text = new StringBuilder();
		text.append(pesimistulos);
		if (poikasCount.length() > 0) text.append(" (").append(poikasCount).append(")");
		text.append(" - ");
		text.append(pesimistuloksetSelection().get(pesimistulos));
		return text;
	}

	private void everythingFromRowToMap(Map<String, String> map, _PesaDomainModelRow result) {
		tableToMap(map, result.getPesa(), "pesa.");
		tableToMap(map, result.getTarkastus(), "tarkastus.");
		tableToMap(map, result.getOlosuhde(), "olosuhde.");
		tableToMap(map, result.getReviiri(), "reviiri.");
		tableToMap(map, result.getVuosi(), "vuosi.");
	}

	private void tableToMap(Map<String, String> map, _Table table, String tablename) {
		for (_Column c : table) {
			map.put(tablename + c.getName(), c.getValue());
		}
	}

	private void generateFilenameToMap(String pesaid, String year, Map<String, String> map, _PesaDomainModelRow result) {
		StringBuilder filename = new StringBuilder("REV_");
		filename.append(result.getColumn("reviiri.reviiri_id"));
		filename.append("_PESA_").append(pesaid).append("_").append(year);
		map.put(Const.FILENAME, filename.toString());
	}

	private void prevTarkPvmToMap(Map<String, String> map, _Table tarkastus) {
		map.put(MerikotkaConst.PREV_TARKASTUS_TARK_PVM, tarkastus.get("tark_pvm").getValue());
	}

	private void prevPesaKuntoToMap(Map<String, String> map, _Table tarkastus) {
		_Column pesan_kunto = tarkastus.get("pesa_kunto");
		_Column pesa_kunto_aika = tarkastus.get("pesa_kunto_aika");
		_Column pesa_kunto_ihmisen_vaik = tarkastus.get("pesa_kunto_ihmisen_vaik");

		String prev_pesa_kunto = pesan_kunto.getValue();
		String prev_pesa_kunto_desc = selections.get(pesan_kunto.getAssosiatedSelection()).get(prev_pesa_kunto);
		prev_pesa_kunto_desc = prev_pesa_kunto + " - " + pesa_kunto_ihmisen_vaik.getValue() + " - " + pesa_kunto_aika.getValue() + " - " + prev_pesa_kunto_desc ;
		map.put(MerikotkaConst.PREV_TARKASTUS_PESA_KUNTO, prev_pesa_kunto_desc);
	}

	private void prevPesimistulosToMap(Map<String, String> map, _Table tarkastus) {
		_Column pesimistulos = tarkastus.get("pesimistulos");
		String prev_pesimistulos = pesimistulos.getValue();
		String poikasCount = searchFunctionality.nestingOutput(tarkastus);
		StringBuilder prev_pesimistulos_text = buildPesimistulosText(prev_pesimistulos, poikasCount);
		map.put(MerikotkaConst.PREV_TARKASTUS_PESIMISTULOS, prev_pesimistulos_text.toString());
	}

	private void prevTarkastajaToMap(Map<String, String> map, _Table tarkastus) {
		_Column tarkastaja = tarkastus.get("tarkastaja1_id");
		String prev_tarkastaja = tarkastaja.getValue();
		prev_tarkastaja = selections.get(tarkastaja.getAssosiatedSelection()).get(prev_tarkastaja);
		map.put(MerikotkaConst.PREV_TARKASTUS_TARKASTAJA, prev_tarkastaja);
	}

	private _PesaDomainModelRow previousTarkastusFor(String pesaid) throws SQLException {
		_PesaDomainModelRow searchParams = dao.newRow();
		searchParams.getPesa().get("pesa_id").setValue(pesaid);
		List<_PesaDomainModelRow> results = new LinkedList<>();
		dao.executeSearch(searchParams, new RowsToListResultHandler(dao, results));
		_PesaDomainModelRow result = results.get(0);
		return result;
	}

	private _Table previousTarkastusOnlyInspected(String pesaid) throws SQLException {
		_Table searchParams = dao.newRow().getTarkastus();
		searchParams.get("pesa_id").setValue(pesaid);
		searchParams.get("pesimistulos").setValue("A|K|J|M|P|R|L");
		_Table resultTable = dao.newRow().getTarkastus();
		try {
			dao.executeSearch(searchParams, new FirstResultRowToTableHandler(resultTable), "tark_vuosi DESC");
		} catch (NoResultsException e) {
			searchParams.get("pesimistulos").setValue("");
			dao.executeSearch(searchParams, new FirstResultRowToTableHandler(resultTable), "tark_vuosi DESC");
		}
		return resultTable;
	}

	private void tarkVuosiToWhereItIsNeeded(String year, Map<String, String> map) {
		map.put("tark_vuosi", year);
		map.put("tark_vuosi_1", year);
		map.put("tark_vuosi_2", year);
		map.put("tark_vuosi_3", year);
		map.put("tark_vuosi_4", year);
		map.put("tark_vuosi_5", year);
		map.put("tark_vuosi_6", year);
	}


	@Override
	public Map<String, String> tarkastusDataFor(String tarkastusid) throws Exception {
		throw new UnsupportedOperationException();
	}
}
