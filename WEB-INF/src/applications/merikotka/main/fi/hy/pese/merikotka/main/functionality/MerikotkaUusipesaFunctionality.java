package fi.hy.pese.merikotka.main.functionality;

import fi.hy.pese.framework.main.app.FunctionalityExecutor;
import fi.hy.pese.framework.main.app._Functionality;
import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.app.functionality.BaseFunctionality;
import fi.hy.pese.framework.main.app.functionality.validate._Validator;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.merikotka.main.functionality.validation.MerikotkaUusiPesaValidator;

public class MerikotkaUusipesaFunctionality extends BaseFunctionality<_PesaDomainModelRow> {

	public MerikotkaUusipesaFunctionality(_Request<_PesaDomainModelRow> request) {
		super(request);
	}

	@Override
	public _Validator validator() {
		return new MerikotkaUusiPesaValidator(request);
	}

	@Override
	public void afterSuccessfulValidation() throws Exception {
		if (data.action().equals(Const.CHECK)) {
			data.setPage(Const.TARKASTUS_PAGE);
			data.setAction(Const.FILL_FOR_INSERT_FIRST);
			_Functionality<_PesaDomainModelRow> tarkastusFunctionality = request.functionalityFor(Const.TARKASTUS_PAGE);
			FunctionalityExecutor.execute(tarkastusFunctionality);
		}
	}

}
