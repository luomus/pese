package fi.hy.pese.peto.main.app;

import java.util.Map;

import fi.hy.pese.framework.main.app.BaseFactoryForApplicationSpecificationFactory;
import fi.hy.pese.framework.main.app._Functionality;
import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.app.functionality.CoordinateRadiusSearcherFunctionality;
import fi.hy.pese.framework.main.app.functionality.FilemanagementFunctionality;
import fi.hy.pese.framework.main.app.functionality.FilemanagementFunctionality.FileListGeneratorImple;
import fi.hy.pese.framework.main.app.functionality.LintuvaaraLoginFunctionality;
import fi.hy.pese.framework.main.app.functionality.LoginFunctionalityWithSingleUser;
import fi.hy.pese.framework.main.app.functionality.LogoutFunctionality;
import fi.hy.pese.framework.main.app.functionality.PesintamanagementFunctionality;
import fi.hy.pese.framework.main.app.functionality.TablemanagementFunctionality;
import fi.hy.pese.framework.main.db._DAO;
import fi.hy.pese.framework.main.general.WarehouseRowGenerator;
import fi.hy.pese.framework.main.general._WarehouseRowGenerator;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data.ParameterMap;
import fi.hy.pese.framework.main.general.data._Data;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.main.kirjekyyhky._KirjekyyhkyXMLGenerator;
import fi.hy.pese.peto.main.functionality.PetoKirjekyyhkyManagementFunctionality;
import fi.hy.pese.peto.main.functionality.PetoKirjekyyhkyXMLGenerator;
import fi.hy.pese.peto.main.functionality.PetoSearchFunctionality;
import fi.hy.pese.peto.main.functionality.PetoTarkastusFunctionality;
import fi.hy.pese.peto.main.functionality.SeurantaruutuManagementFunctionality;
import fi.luomus.commons.config.Config;
import fi.luomus.commons.containers.Selection;
import fi.luomus.commons.containers.Selection.SelectionOption;
import fi.luomus.commons.containers.SelectionImple;
import fi.luomus.commons.containers.SelectionOptionImple;
import fi.luomus.commons.tipuapi.TipuAPIClient;
import fi.luomus.commons.utils.AESDecryptor;

public class PetoFactory extends BaseFactoryForApplicationSpecificationFactory<_PesaDomainModelRow> {
	
	private static final int	ONLY_TWO_FIRST_FIELDS	= 2;
	private static final int	ONLY_FIRST_NAME_FIELD	= 1;
	
	public PetoFactory(String configFileName) throws Exception {
		super(configFileName);
	}
	
	@Override
	public _Data<_PesaDomainModelRow> initData(String language, ParameterMap params) throws Exception {
		_Data<_PesaDomainModelRow> data = super.initData(language, params);
		data.set(Const.LINTUVAARA_URL, getLintuvaaraURL());
		return data;
	}
	
	@Override
	public String frontpage() {
		return Const.SEARCH_PAGE; // Warning: frontpage must be defined bellow (functionalityFor()) or an infinite loop will happen
	}
	
	private AESDecryptor	decryptor	= null;
	
	private AESDecryptor getAESDecryptor() {
		if (decryptor == null) {
			try {
				decryptor = new AESDecryptor(config().get(Config.LINTUVAARA_PUBLIC_RSA_KEY));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return decryptor;
	}
	
	@Override
	public _Functionality<_PesaDomainModelRow> functionalityFor(String page, _Request<_PesaDomainModelRow> request) {
		if (page.equals(Const.LOGIN_PAGE)) {
			if (request.data().contains("temporaryLoginAccount")) {
				return new LoginFunctionalityWithSingleUser<>(request, "lehtoranta", config().get("lehtoranta_password"));
			}
			if (config().developmentMode()) return new LoginFunctionalityWithSingleUser<>(request, "z", "z");
			return new LintuvaaraLoginFunctionality<>(request, getAESDecryptor(), getErrorReporter());
		}
		if (page.equals(Const.TABLEMANAGEMENT_PAGE)) return new TablemanagementFunctionality<>(request);
		if (page.equals(Const.SEARCH_PAGE)) return new PetoSearchFunctionality(request);
		if (page.equals(Const.TARKASTUS_PAGE)) return new PetoTarkastusFunctionality(request);
		if (page.equals(Const.FILEMANAGEMENT_PAGE)) return new FilemanagementFunctionality<>(request, new FileListGeneratorImple(request.config().pdfFolder(), request.config().reportFolder()));
		if (page.equals(Const.REPORTS_PAGE)) return new PetoSearchFunctionality(request);
		if (page.equals(PetoConst.SEURANTARUUTUMANAGEMENT_PAGE)) return new SeurantaruutuManagementFunctionality(request);
		if (page.equals(Const.LOGOUT_PAGE)) return new LogoutFunctionality<>(request);
		if (page.equals(Const.PESINTAMANAGEMENT_PAGE)) return new PesintamanagementFunctionality(request);
		if (page.equals(Const.COORDINATE_RADIUS_SEARCH_PAGE)) return new CoordinateRadiusSearcherFunctionality(request, new PetoSearchFunctionality(request));
		if (page.equals(Const.KIRJEKYYHKY_MANAGEMENT_PAGE)) return new PetoKirjekyyhkyManagementFunctionality(request);
		if (page.equals("luo")) {
			try {
				PetoKirjekyyhkyXMLGenerator generator = new PetoKirjekyyhkyXMLGenerator(request);
				generator.writeStructureXML();
			} catch (Exception e) {
				e.printStackTrace();
			}
			request.data().setPage(Const.FILEMANAGEMENT_PAGE);
			return functionalityFor(Const.FILEMANAGEMENT_PAGE, request);
		}
		request.data().setPage(frontpage());
		return functionalityFor(frontpage(), request);
	}
	
	@Override
	public Map<String, Selection> fetchSelections(_DAO<_PesaDomainModelRow> dao) throws Exception {
		Map<String, Selection> selections = dao.returnCodeSelections("aputaulu", "taulu", "kentta", "arvo", "selite", "ryhmittely", new String[] {"jarjestys", "selite"});
		selections.put(PetoConst.TAULUT, dao.returnSelections("aputaulu", "taulu", "taulu", new String[] {"taulu"}));
		
		TipuAPIClient tipuAPI = null;
		try {
			tipuAPI = this.tipuAPI();
			selections.put(TipuAPIClient.RINGERS, tipuAPI.getAsSelection(TipuAPIClient.RINGERS, ONLY_TWO_FIRST_FIELDS));
			selections.put(TipuAPIClient.MUNICIPALITIES, tipuAPI.getAsSelection(TipuAPIClient.MUNICIPALITIES, ONLY_FIRST_NAME_FIELD));
			selections.put(TipuAPIClient.ELY_CENTRES, tipuAPI.getAsSelection(TipuAPIClient.ELY_CENTRES));
			selections.put(TipuAPIClient.SPECIES, tipuAPI.getAsSelection(TipuAPIClient.SPECIES, ONLY_FIRST_NAME_FIELD));
		} finally {
			if (tipuAPI != null) tipuAPI.close();
		}
		
		SelectionImple pesivatLajit = new SelectionImple(PetoConst.PESIVAT_LAJIT);
		for (SelectionOption o : selections.get(TipuAPIClient.SPECIES)) {
			String laji = o.getValue();
			if (PetoConst.ALLOWED_NESTING_SPECIES.contains(laji)) {
				pesivatLajit.addOption(new SelectionOptionImple(laji, o.getText()));
			}
		}
		selections.put(PetoConst.PESIVAT_LAJIT, pesivatLajit);
		
		SelectionImple rakentavatLajit = new SelectionImple(PetoConst.RAKENTAVAT_LAJIT);
		for (SelectionOption o : selections.get(TipuAPIClient.SPECIES)) {
			String laji = o.getValue();
			if (PetoConst.ALLOWED_RAKENTANUT_LAJI.contains(laji)) {
				rakentavatLajit.addOption(new SelectionOptionImple(laji, o.getText()));
			}
		}
		selections.put(PetoConst.RAKENTAVAT_LAJIT, rakentavatLajit);
		
		SelectionImple k_e = new SelectionImple("k_e");
		k_e.addOption(new SelectionOptionImple("K", "Kyllä"));
		k_e.addOption(new SelectionOptionImple("E", "Ei"));
		selections.put(PetoConst.K_E, k_e);
		
		return selections;
	}
	
	
	@Override
	public _KirjekyyhkyXMLGenerator kirjekyyhkyFormDataXMLGenerator(_Request<_PesaDomainModelRow> request) throws Exception {
		return new PetoKirjekyyhkyXMLGenerator(request);
	}

	@Override
	public _WarehouseRowGenerator getWarehouseRowGenerator(_DAO<_PesaDomainModelRow> dao) {
		return new WarehouseRowGenerator(PetoWarehouseDefinition.instance(), dao, this);
	}

}
