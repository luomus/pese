package fi.hy.pese.framework.main.db;

import fi.hy.pese.framework.main.general.data._Data;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;

public class RowsToDataListsResultHandler extends RowCombiningResultHandler {

	private final String listname;
	private final _Data<_PesaDomainModelRow> data;

	public RowsToDataListsResultHandler(_DAO<_PesaDomainModelRow> dao, _Data<_PesaDomainModelRow> data, String listname) {
		super(dao);
		this.listname = listname;
		this.data = data;
	}

	@Override
	protected boolean handle(_PesaDomainModelRow row) {
		data.addToList(listname, row);
		return true;
	}

}
