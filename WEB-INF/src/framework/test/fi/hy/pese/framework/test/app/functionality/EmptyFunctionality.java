package fi.hy.pese.framework.test.app.functionality;

import fi.hy.pese.framework.main.app.Request;
import fi.hy.pese.framework.main.app.RequestImpleData;
import fi.hy.pese.framework.main.app._Functionality;
import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.app.functionality.validate.EmptyValidator;
import fi.hy.pese.framework.main.app.functionality.validate._Validator;
import fi.hy.pese.framework.main.general.data._Row;

public class EmptyFunctionality<T extends _Row> implements _Functionality<T> {

	@Override
	public _Validator validator() {
		return new EmptyValidator();
	}

	@Override
	public void afterFailedValidation() throws Exception {}

	@Override
	public void afterSuccessfulValidation() throws Exception {}

	@Override
	public void postProsessingHook() throws Exception {}

	@Override
	public void preProsessingHook() throws Exception {}

	@Override
	public _Request<T> functionalityRequest() {
		return new Request<>(new RequestImpleData<T>());
	}

}
