package fi.hy.pese.framework.test.general;

import fi.hy.pese.framework.main.general.data.PesaDomainModelRow;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.merikotka.main.app.MerikotkaDatabaseStructureCreator;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class RowTests {

	public static Test suite() {
		return new TestSuite(RowTests.class.getDeclaredClasses());
	}

	public static class WhenSettingGettingValues extends TestCase {

		private final _PesaDomainModelRow row = new PesaDomainModelRow(MerikotkaDatabaseStructureCreator.hardCoded());

		@Override
		public void setUp() {
			row.clearAllValues();
		}

		public void test___() {
			row.getPesa().get("pesa_id").setValue("11");
			assertEquals("11", row.getPesa().get("pesa_id").getValue());
		}

		public void test___2() {
			assertEquals("", row.getPesa().get("pesa_id").getValue());
		}

		public void test___3() {
			row.getPoikaset().get(1).get("poikanen_id").setValue(22);
			assertEquals("22", row.getPoikaset().get(1).get("poikanen_id").getValue());
		}

		public void test___4() {
			row.getPoikaset().get(18).get("poikanen_id").setValue(22);
			assertEquals("22", row.getPoikaset().get(18).get("poikanen_id").getValue());
		}


	}

}
