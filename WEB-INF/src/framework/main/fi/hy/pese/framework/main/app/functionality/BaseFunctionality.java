package fi.hy.pese.framework.main.app.functionality;

import fi.hy.pese.framework.main.app._Functionality;
import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.app.functionality.validate.EmptyValidator;
import fi.hy.pese.framework.main.app.functionality.validate._Validator;
import fi.hy.pese.framework.main.db._DAO;
import fi.hy.pese.framework.main.general.data._Data;
import fi.hy.pese.framework.main.general.data._Row;
import fi.luomus.commons.config.Config;
import fi.luomus.commons.session.SessionHandler;

public class BaseFunctionality<T extends _Row> implements _Functionality<T> {

	protected final _Request<T>		request;
	protected final _Data<T>		data;
	protected final _DAO<T>			dao;
	protected final SessionHandler	session;
	protected final Config			config;

	public BaseFunctionality(_Request<T> request) {
		this.request = request;
		this.data = request.data();
		this.dao = request.dao();
		this.session = request.session();
		this.config = request.config();
	}

	@Override
	public void preProsessingHook() throws Exception {}

	@Override
	public _Validator validator() throws Exception {
		return new EmptyValidator();
	}

	@Override
	public void afterSuccessfulValidation() throws Exception {}

	@Override
	public void afterFailedValidation() throws Exception {}

	@Override
	public void postProsessingHook() throws Exception {}

	@Override
	public _Request<T> functionalityRequest() {
		return request;
	}

}
