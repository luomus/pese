<#assign ROW_ID = 0>
<#assign UNIQUE_KEY = 1>
<#assign IMMUTABLE_KEY = 2>
<#assign NORMAL_FIELD = 3>
<#assign DATE_ADDED = 4>
<#assign DATE_MODIFIED = 5>

<#assign  VARCHAR = 1>
<#assign  INTEGER = 2>
<#assign  DECIMAL = 3>
<#assign  DATE = 4>
	
<#include "navi.ftl">
<#include "macro.ftl">

<script type="text/javascript" src="${system.baseURL}/search.js"></script>

<div class="reports">

<h1>${texts.report_title}</h1>
<br />
<@statustexts system texts />
<@errorlist errors texts />

<#if system.action == "print_report">
  <#assign folder = "report">
<#else>
  <#assign folder = "pdf">
</#if>

<#list files as file>
    <span class="file"> <a target="_new" href="${system.baseURL}?page=filemanagement&amp;action=show&amp;folder=${folder}&amp;filename=${file}">${file}</a> </span> <br />
</#list>

<br />
<br />

<form action="${system.baseURL}" method="post" name="search"  >
<input type="hidden" name="page"   value="${system.page}" />
<input type="hidden" name="action" value="print_report" />



<table style="min-width: 770px;">
  <tr>
    <td>${texts.search_quick_year}: </td>
    <td> <@inputText "searchparameters" searchparameters.tarkastus.tark_vuosi false errors /> </td> 
    <td colspan="2"> </td>
    <td rowspan="3">
      <select name="NestCondition" size="3">
        <option value="0" <#if (data.NestCondition == "0") > selected="selected" </#if> >Pesintäkuntoiset</option>
        <option value="1" <#if (data.NestCondition == "1") > selected="selected" </#if> >Tuhoutuneet</option>
        <option value="2" <#if (data.NestCondition == "2") > selected="selected" </#if> >Kaikki</option>
      </select> 
    </td>
  </tr>
  <tr>
    <td>${texts.search_quick_pesa}: </td>
    <td>${texts.search_quick_name}:       <@inputText "searchparameters" searchparameters.pesa.pesanimi false errors /> </td>
    <td>${texts.search_quick_id}:         <@inputText "searchparameters" searchparameters.pesa.pesa_id true errors  /> </td>
    <td>${texts.search_quick_pesa_kunta}: <@selection selections "searchparameters" searchparameters.vuosi.pesan_kunta 1 false true false errors /> </td>
  </tr>
  <tr>
    <td>${texts.search_quick_reviiri}: </td>
    <td>${texts.search_quick_name}:          <@inputText "searchparameters" searchparameters.reviiri.reviirinimi false errors /> </td>
    <td>${texts.search_quick_id}:            <@inputText "searchparameters" searchparameters.reviiri.reviiri_id  true errors /> </td>
    <td>${texts.search_quick_reviiri_kunta}: <@selection selections "searchparameters" searchparameters.vuosi.reviirin_kunta 1 false true false errors /> </td>
  </tr>
</table>
<table style="min-width: 770px;">
  <tr>
    <td>${texts.search_quick_rev_kunta_suuralue}: </td>
    <td> 
    	<select name="Suuralue" size="4" multiple="multiple">
    	  <option value=""></option>
    	  <#list selections["suuralueet"].options as s>
    	    <option value="${s.value}" <#if ( (data.Suuralue!"") == s.value) > selected="selected" </#if> > ${s.text} </option>
    	  </#list>
    	</select>
    </td>
    <td>${texts.search_quick_rev_kunta_ympkesk}: </td>
    <td>
      <select name="Ymparistokeskus" size="4" multiple="multiple">
          <option value=""></option>
    	  <#list selections["ely-centres"].options as s>
    	    <option value="${s.value}" <#if ( (data.Ymparistokeskus!"") == s.value) > selected="selected" </#if> > ${s.text} </option>
    	  </#list>
    	</select>
    </td>
  </tr>
</table>

<table style="min-width: 770px;">
  <tr>
    <td rowspan="2">${texts.search_quick_coordinates}: </td>
    <td>${texts.search_quick_yht}: </td>
	<td>${texts.search_quick_lat}: <@inputText "searchparameters" searchparameters.pesa.yht_leveys false errors /> </td>
	<td>${texts.search_quick_lon}: <@inputText "searchparameters" searchparameters.pesa.yht_pituus false errors /> </td>
	
	<td rowspan="2">${texts.search_quick_radius}:</td>
    <td rowspan="2">
      <select name="CoordinateSearchRadius">
        <option value=""></option>
        <option value="1"      <#if (data.CoordinateSearchRadius == "1") >      selected="selected" </#if> >1 m</option>
        <option value="100"    <#if (data.CoordinateSearchRadius == "500") >    selected="selected" </#if> >500 m</option>
        <option value="1000"   <#if (data.CoordinateSearchRadius == "1000") >   selected="selected" </#if> >1000 m</option>
        <option value="5000"   <#if (data.CoordinateSearchRadius == "5000") >   selected="selected" </#if> >5000 m</option>
        <option value="10000"  <#if (data.CoordinateSearchRadius == "10000") >  selected="selected" </#if> >10 km</option>
        <option value="50000"  <#if (data.CoordinateSearchRadius == "50000") >  selected="selected" </#if> >50 km</option>
        <option value="100000" <#if (data.CoordinateSearchRadius == "100000") > selected="selected" </#if> >100 km</option>
      </select>
    </td>
  </tr>
  <tr> 
    <td>${texts.search_quick_eur}: </td>
	<td>${texts.search_quick_lat}: <@inputText "searchparameters" searchparameters.pesa.eur_leveys false errors /> </td>
	<td>${texts.search_quick_lon}: <@inputText "searchparameters" searchparameters.pesa.eur_pituus false errors /> </td>
  </tr>
</table>






<table>
<tr>
<td style="vertical-align: top;">

<table>
  <caption>Raportin tulostus</caption>
  <tr>
    <td style="vertical-align: top;">
      <select name="report" size="4" multiple="multiple" <@checkerror errors "report" />>
        <option value="report_pesimistulokset" <#if data.report == "report_pesimistulokset"> selected="selected" </#if>> ${texts.report_name_pesimistulokset} </option>
        <option value="report_reviirit"<#if data.report == "report_reviirit"> selected="selected" </#if>> ${texts.report_name_reviirit} </option>
        <option value="report_tarkastamattomat"<#if data.report == "report_tarkastamattomat"> selected="selected" </#if>> ${texts.report_name_tarkastamattomat} </option>
        <option value="report_viranomais"<#if data.report == "report_viranomais"> selected="selected" </#if>> ${texts.report_name_viranomais} </option>
      </select>
      <br />
      <br />
      <br />      
      <br />      
      <input type="submit" value="Luo raportti" />
    </td>
  </tr>
</table>

</td>
<td style="vertical-align: top;">

<table>
  <caption>Esitäytettyjen lomakkeiden tulostus</caption>
  <tr>
    <td> Vuodelle: </td>
    <td> <input type="text" size="4" name="yyyy" value="${system.currentYear}" /> </td>
  </tr>
  <tr>
    <td> Kokoelman tiedostonimi: </td>
    <td> <input type="text" size="20" name="collection_filename" value="" />.pdf  (kokoelmaa ei luoda, jos tiedostonimeä ei anneta) </td>
  </tr>
  <tr>
    <td> Lähetetäänkö Kirjekyyhkyyn: </td>
    <td>
      <select name="GenerateKirjekyyhkyFormDataXML" size="2">
        <option value="yes" <#if (data.GenerateKirjekyyhkyFormDataXML!"") == "yes"> selected="selected" </#if> > Kyllä </option>
        <option value="no"  <#if (data.GenerateKirjekyyhkyFormDataXML!"") != "yes"> selected="selected" </#if> > Ei </option>
      </select> 
    </td>
  </tr>
  <tr>
    <td colspan="2"> <br />  <input type="submit" name="print_pdf" value="Luo/lähetä esipainetut lomakkeet" /> </td>
  </tr>
</table>

</td>
</tr>
</table>


</form>




<br />
   

</div>


<br /><br /><br /><br /><br /><br />


</body>
</html>

