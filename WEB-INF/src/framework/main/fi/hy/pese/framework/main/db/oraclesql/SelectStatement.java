package fi.hy.pese.framework.main.db.oraclesql;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._Table;
import fi.luomus.commons.containers.DateValue;
import fi.luomus.commons.db.connectivity.TransactionConnection;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.Utils;

public class SelectStatement extends Statement {

	private final List<String>	select;
	private final List<String>	from;
	private final List<String>	where;
	private final List<String>	orderBy;
	private boolean				distinct			= false;
	private boolean				onlyExactMatches	= false;

	public SelectStatement(TransactionConnection con) {
		super(con);
		select = new ArrayList<>();
		from = new ArrayList<>();
		where = new ArrayList<>();
		orderBy = new ArrayList<>();
	}

	public boolean hasResults() throws SQLException {
		ResultSet rs = executeQuery();
		boolean hasResults = rs.next();
		rs.close();
		return hasResults;
	}

	public void setDistinct(boolean state) {
		distinct = state;
	}

	public void setOnlyExactMatches(boolean state) {
		onlyExactMatches = state;
	}

	@Override
	protected String constructQuery() {
		StringBuilder query = new StringBuilder(" SELECT ");
		if (distinct) query.append(" DISTINCT ");
		toCommaSeperatedStatement(query, select);
		query.append(" FROM ");
		toCommaSeperatedStatement(query, from);
		if (!where.isEmpty()) {
			query.append(" WHERE ");
			toAndSeperatedStatement(query, where);
		}
		if (!orderBy.isEmpty()) {
			query.append(" ORDER BY ");
			toCommaSeperatedStatement(query, orderBy);
		}
		return query.toString();
	}

	public void select(String selectStatement) {
		select.add(selectStatement + as());
	}

	public void select(String... selectStatements) {
		for (String s : selectStatements) {
			select(s);
		}
	}

	private String as() {
		return (" AS f" + select.size());
	}

	public void from(String fromStatement) {
		from.add(fromStatement);
	}

	public void addToWhereClause(String whereStatement) {
		where.add(whereStatement);
	}

	public void orderBy(String orderByStatement) {
		orderBy.add(orderByStatement);
	}

	public void orderBy(String... orderByFields) {
		if (orderByFields == null) return;
		for (String s : orderByFields) {
			orderBy.add(s);
		}
	}

	public void orderBy(Vector<String> orderByFields) {
		for (String s : orderByFields) {
			orderBy.add(s);
		}
	}

	public void select(_Table table) {
		for (_Column c : table) {
			select.add(databasenameFor(c));
		}
	}

	public void selectAll_From(_Table table) {
		for (_Column c : table) {
			select.add(databasenameFor(c));
		}
		from.add(table.getName());
	}

	public void where(_Column column) {
		addToWhereStatement(column);
	}

	public void where(String clause, String value) {
		where.add(clause);
		values.add(value);
	}

	public void where(String clause) {
		where.add(clause);
	}

	public void selectAll_From_Where(_Table table) {
		for (_Column c : table) {
			select.add(databasenameFor(c));
			if (hasValueSet(c)) addToWhereStatement(c);
		}
		from.add(table.getName());
	}

	public void selectAll_From_Where_OrderBy(_Table table) {
		for (_Column c : table) {
			select.add(databasenameFor(c));
			if (hasValueSet(c)) addToWhereStatement(c);
			if (!isRowIdColumn(c) && c.getSize() < 999) orderBy.add(databasenameFor(c));
		}
		from.add(table.getName());
	}

	private void addToWhereStatement(_Column c) {
		StringBuilder whereStatement = new StringBuilder();
		if (onlyExactMatches) {
			addExactMatchStatement(c, whereStatement);
		} else if (c.getDatatype() == _Column.ROWID) {
			addExactMatchStatement(c, whereStatement);
		} else if (c.getDatatype() == _Column.DATE) {
			addDateValueStatement(c, whereStatement);
		} else if (containsListOfValuesForInStatement(c)) {
			addInStatement(c, whereStatement);
		} else if (hasSpecialValues(c)) {
			addLikeStatement(c, whereStatement);
		} else if (isIntegerDatatypeColumn(c) || isDecimalDatatypeColumn(c)) {
			addNumericValueStatement(c, whereStatement);
		} else if (hasAssosiatedSelection(c)) {
			addExactMatchStatement(c, whereStatement);
		} else if (c.isUppercaseColumn()) {
			addExactMatchStatement(c, whereStatement);
		} else {
			addLikeStatement(c, whereStatement);
		}
		where.add(whereStatement.toString());
	}

	private boolean hasSpecialValues(_Column c) {
		return c.getValue().contains("%") || c.getValue().equals(Const.VALUE_IS_NULL) || c.getValue().startsWith(Const.VALUE_IS_NOT);
	}

	private void addDateValueStatement(_Column c, StringBuilder whereStatement) {
		DateValue date = c.getDateValue();
		String year = date.getYear();
		String month = date.getMonth();
		String day = date.getDay();
		if (year.contains("-")) {
			String[] range = year.split("-");
			whereStatement.append(" to_char(  ").append(databasenameFor(c)).append(" ,  'YYYY'  )").append(" BETWEEN ? AND ? ");
			values.add(range[0]);
			values.add(range[1]);
		} else if (isNull(day) && isNull(month)) {
			whereStatement.append(" to_char(  ").append(databasenameFor(c)).append(" ,  'YYYY'  )").append(" = ? ");
			values.add(year);
		} else if (isNull(day)) {
			whereStatement.append(" to_char(  ").append(databasenameFor(c)).append(" ,  'MM.YYYY'  )").append(" = ? ");
			values.add(month + "." + year);
		} else {
			whereStatement.append(databasenameFor(c)).append(" = ? ");
			values.add(DateUtils.convertToDate(c.getDateValue()));
		}
	}

	private void addLikeStatement(_Column c, StringBuilder whereStatement) {
		String value = c.getValue();
		if (value.equals(Const.VALUE_IS_NULL)) {
			whereStatement.append(databasenameFor(c)).append(" IS NULL ");
		} else if (value.startsWith(Const.VALUE_IS_NOT)) {
			value = value.substring(1);
			whereStatement.append(" ( ").append(databasenameFor(c)).append(" IS NULL OR UPPER( ").append(databasenameFor(c)).append(" ) NOT LIKE ? ) ");
			values.add("%" + value.toUpperCase() + "%");
		} else {
			whereStatement.append(" UPPER( ").append(databasenameFor(c)).append(" ) LIKE ? ");
			values.add("%" + value.toUpperCase() + "%");
		}
	}

	private void addInStatement(_Column c, StringBuilder whereStatement) {
		whereStatement.append(" ( ");
		if (isVarcharDatatypeColumn(c)) {
			whereStatement.append(" UPPER( ").append(databasenameFor(c)).append(") IN ( ");
		} else {
			whereStatement.append(databasenameFor(c)).append(" IN ( ");
		}
		boolean includeNull = false;
		for (String v : c.getValueList()) {
			if (v.equals(Const.VALUE_IS_NULL)) {
				includeNull = true;
			} else {
				whereStatement.append(" ?,");
				if (c.hasValue() && c.getDatatype() == _Column.DECIMAL) {
					values.add(v.trim().replace(",", "."));
				} else {
					values.add(v.trim().toUpperCase());
				}
			}
		}
		removeLastComma(whereStatement);
		whereStatement.append(" ) ");
		if (includeNull) {
			whereStatement.append(" OR ").append(databasenameFor(c)).append(" IS NULL ");
		}
		whereStatement.append(" ) ");
	}

	private void addExactMatchStatement(_Column c, StringBuilder whereStatement) {
		whereStatement.append(databasenameFor(c)).append(" = ? ");
		if (c.hasValue() && c.getDatatype() == _Column.DECIMAL) {
			values.add(c.getDoubleValue());
		} else if (c.hasValue() && c.getDatatype() == _Column.DATE) {
			values.add(DateUtils.convertToDate(c.getDateValue()));
		} else {
			values.add(c.getValue());
		}
	}

	private boolean containsListOfValuesForInStatement(_Column c) {
		return c.getValue().contains("|");
	}

	private void addNumericValueStatement(_Column c, StringBuilder whereStatement) {
		if (c.getValue().startsWith(Const.VALUE_IS_NOT)) {
			addLikeStatement(c, whereStatement);
			return;
		}
		int rangeSeparatorCount = Utils.countNumberOf("-", c.getValue());
		if (noRangeGiven(c, rangeSeparatorCount)) {
			addExactMatchStatement(c, whereStatement);
		} else {
			String[] range;
			if (rangeSeparatorCount == 1) {
				range = c.getValue().split("-");
			} else {
				range = extractRange(c.getValue());
			}
			whereStatement.append(databasenameFor(c)).append(" BETWEEN ? AND ? ");
			if (c.hasValue() && c.getDatatype() == _Column.DECIMAL) {
				values.add(new Double(range[0].replace(",", ".")));
				values.add(new Double(range[1].replace(",", ".")));
			} else {
				values.add(range[0]);
				values.add(range[1]);
			}
		}
	}

	private boolean noRangeGiven(_Column c, int rangeSeparatorCount) {
		return rangeSeparatorCount == 0 || (rangeSeparatorCount == 1 && c.getValue().startsWith("-"));
	}

	private String[] extractRange(String value) {
		String[] range = new String[2];
		int separatorIndex = value.indexOf("-", 1);
		range[0] = value.substring(0, separatorIndex);
		range[1] = value.substring(separatorIndex + 1);
		return range;
	}
}
