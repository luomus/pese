package fi.hy.pese.merikotka.main.functionality.validation;

import java.sql.SQLException;

import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.app.functionality.validate.TablemanagementValidator;
import fi.hy.pese.framework.main.app.functionality.validate._Validator;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.merikotka.main.db.MerikotkaDAO;

/**
 * Same validation except for reviiri we validate the name:
 * there can't be reviiri with same name within same kunta
 */
public class MerikotkaTablemanagementValidator extends TablemanagementValidator<_PesaDomainModelRow> {

	public MerikotkaTablemanagementValidator(_Request<_PesaDomainModelRow> request) {
		super(request);
	}

	@Override
	public void validate() throws Exception {
		super.validate();

		String tablename = data.get(Const.TABLE);
		if (tablename.equals("reviiri")) {
			validateReviirinimi();
		}
	}

	private void validateReviirinimi() throws SQLException {
		_Column reviirinimi = data.updateparameters().getColumn("reviiri.reviirinimi");
		if (data.action().equals(Const.UPDATE)) {
			String reviiri_id = data.updateparameters().getColumn("reviiri.reviiri_id").getValue();
			if (MerikotkaDAO.reviirinimiExists(reviirinimi.getValue(), reviiri_id, dao)) {
				error(reviirinimi, _Validator.ALREADY_IN_USE);
			}
		}
		if (data.action().equals(Const.INSERT)) {
			if (MerikotkaDAO.reviirinimiExists(reviirinimi.getValue(), null, dao)) {
				error(reviirinimi, _Validator.ALREADY_IN_USE);
			}
		}
	}

}
