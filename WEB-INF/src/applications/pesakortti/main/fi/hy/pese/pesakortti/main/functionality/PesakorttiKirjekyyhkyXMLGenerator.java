package fi.hy.pese.pesakortti.main.functionality;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.db._DAO;
import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.main.kirjekyyhky.KirjekyyhkyXMLGeneratorUtility;
import fi.hy.pese.framework.main.kirjekyyhky.KirjekyyhkyXMLGeneratorUtility.StructureDefinition;
import fi.hy.pese.framework.main.kirjekyyhky._KirjekyyhkyXMLGenerator;
import fi.hy.pese.pesakortti.main.app.PesakorttiConst;
import fi.luomus.commons.config.Config;
import fi.luomus.commons.tipuapi.TipuAPIClient;
import fi.luomus.commons.utils.FileUtils;

public class PesakorttiKirjekyyhkyXMLGenerator implements _KirjekyyhkyXMLGenerator {

	private static final String[]	COLUMNS_TO_EXLUDE	= { "id", "tarkastus" };
	private final _Request<_PesaDomainModelRow> request;
	private final Config config;
	private final _DAO<_PesaDomainModelRow> dao;
	private final KirjekyyhkyXMLGeneratorUtility generator;
	private final List<String> producedDataXMLFiles = new LinkedList<>();

	public PesakorttiKirjekyyhkyXMLGenerator(_Request<_PesaDomainModelRow> request) throws Exception {
		this.request = request;
		this.config = request.config();
		this.dao = request.dao();
		this.generator = initGenerator();
	}

	@Override
	public File writeFormDataXML(Map<String, String> data, String filename) {
		filename += ".xml";
		List<String> owners = new ArrayList<>();
		if (data.containsKey("pesa.seur_tarkastaja")) {
			owners.add(data.get("pesa.seur_tarkastaja"));
			data.put("tarkastus.tarkastaja", data.get("pesa.seur_tarkastaja"));
		}

		File file = new File(config.kirjekyyhkyFolder(), filename);
		try {
			String xml = generator.generateDataXML(data, owners);
			FileUtils.writeToFile(file, xml);
			producedDataXMLFiles.add(filename);
		} catch (Exception e) {
			e.printStackTrace();
			request.data().addToNoticeTexts("VIRHE: " + e.getMessage() + " " + e.getStackTrace()[0]);
			return null;
		}
		return file;
	}

	@Override
	public List<String> producedFormDataXMLFiles() {
		return producedDataXMLFiles;
	}

	public void writeStructureXML() throws Exception {
		String xml = generator.generateStructureXML();
		File file = new File(config.reportFolder(), "pesakortti.xml");
		try {
			FileUtils.writeToFile(file, xml, request.config().characterEncoding());
		} catch (Exception e) {
			request.data().addToNoticeTexts("VIRHE: " + e.getMessage() + " " + e.getStackTrace()[0]);
		}
	}

	private KirjekyyhkyXMLGeneratorUtility initGenerator() throws FileNotFoundException, IOException {
		KirjekyyhkyXMLGeneratorUtility generator = new KirjekyyhkyXMLGeneratorUtility();
		StructureDefinition definition = generator.getDefinition();

		definition.typeID = "pesakortti";
		definition.title = "Pesäkorttitutkimus";

		definition.menutexts.newForm = "Täytä uusi kortti";
		definition.menutexts.printEmptyPdf = "Tulosta tyhjä kortti";
		// definition.menutexts.printInstructions = "Tulosta täyttöohje";

		definition.titleFields.add("tarkastus.vuosi");
		definition.titleFields.add("pesa.kunta");
		definition.titleFields.add("pesa.tarkempi_paikka");
		definition.titleFields.add("pesa.nimi");
		definition.titleFields.add("tarkastus.pesiva_laji");
		definition.titleFields.add("tarkastus.tarkastaja");

		definition.validation.URI = config.get("ValidatonServiceURL");
		definition.validation.username = config.get("ValidatonServiceUsername");
		definition.validation.password = config.get("ValidatonServicePassword");

		definition.coordinateFields.leveys = "pesa.leveys";
		definition.coordinateFields.pituus = "pesa.pituus";
		definition.coordinateFields.tyyppi = "pesa.koord_ilm_tyyppi";
		definition.coordinateFields.mittaustapa = "pesa.koord_mittaustapa";
		definition.coordinateFields.setTitleFields("pesa.tarkempi_paikka", "pesa.nimi", "pesa.id");

		_PesaDomainModelRow row = dao.newRow();
		definition.row = row;
		generator.excludeColumn("pesa", "yht_leveys");
		generator.excludeColumn("pesa", "yht_pituus");
		generator.excludeColumn("pesa", "ast_leveys");
		generator.excludeColumn("pesa", "ast_pituus");
		generator.excludeColumn("pesa", "des_leveys");
		generator.excludeColumn("pesa", "des_pituus");
		generator.excludeColumn("pesa", "eur_leveys");
		generator.excludeColumn("pesa", "eur_pituus");
		generator.excludeColumn("tarkastus", "id");
		generator.excludeColumn("tarkastus", "pesa");
		generator.excludeColumn("tarkastus", "vanha_kortti");
		generator.excludeColumn("tarkastus", "muninta_pvm");
		generator.excludeColumn("tarkastus", "taydellinen_munaluku");
		generator.excludeColumn("tarkastus", "yhdyskunnassa_vanha");
		generator.excludeColumn("tarkastus", "yhteispesinta");
		generator.excludeColumn("tarkastus", "epailyttava");
		generator.excludeColumn("tarkastus", "vanha_kortti");

		for (int i = 1; i<=row.getKaynnit().getStaticCount(); i++) {
			for (String c : COLUMNS_TO_EXLUDE) {
				generator.excludeColumn("kaynti."+i, c);
			}
			generator.excludeColumn("kaynti."+i, "ika_munat");
			generator.excludeColumn("kaynti."+i, "ika_poikaset");
			generator.addCustomColumn("kaynti."+i+".ika", _Column.INTEGER, 2);
			generator.includeTable("kaynti."+i);
		}
		for (int i = 1; i<=row.getAikuiset().getStaticCount(); i++) {
			for (String c : COLUMNS_TO_EXLUDE) {
				generator.excludeColumn("aikuinen."+i, c);
			}
			generator.defineTipuApiSelection("aikuinen."+i, "laji", TipuAPIClient.SPECIES);
			generator.includeTable("aikuinen."+i);
		}
		for (int i = 1; i<=row.getPoikaset().getStaticCount(); i++) {
			for (String c : COLUMNS_TO_EXLUDE) {
				generator.excludeColumn("poikanen."+i, c);
			}
			generator.includeTable("poikanen."+i);
			generator.defineTipuApiSelection("poikanen."+i, "laji", TipuAPIClient.SPECIES);
		}
		for (int i = 1; i<=row.getMunat().getStaticCount(); i++) {
			for (String c : COLUMNS_TO_EXLUDE) {
				generator.excludeColumn("muna."+i, c);
			}
			generator.includeTable("muna."+i);
			generator.defineTipuApiSelection("muna."+i, "laji", TipuAPIClient.SPECIES);
		}
		generator.excludeColumn("pesa", "koord_tarkkuus");
		generator.addCustomColumn("pesa.leveys", _Column.INTEGER, 7);
		generator.addCustomColumn("pesa.pituus", _Column.INTEGER, 7);
		generator.addCustomColumn("pesa.koord_tarkkuus", _Column.VARCHAR, 7, "pesa.koord_tarkkuus");

		generator.setCustomFieldRequired("pesa.leveys");
		generator.setCustomFieldRequired("pesa.pituus");
		generator.setCustomFieldRequired("pesa.koord_tarkkuus");

		generator.includeTable("pesa");
		generator.includeTable("tarkastus");

		definition.selections = request.data().selections();

		generator.excludeSelection(TipuAPIClient.SPECIES);
		generator.excludeSelection(TipuAPIClient.MUNICIPALITIES);
		generator.excludeSelection(TipuAPIClient.RINGERS);
		generator.excludeSelection(TipuAPIClient.ELY_CENTRES);
		generator.excludeSelection(PesakorttiConst.TAULUT);

		generator.defineTipuApiSelection("pesa", "seur_tarkastaja", TipuAPIClient.RINGERS);
		generator.defineTipuApiSelection("pesa", "kunta", TipuAPIClient.MUNICIPALITIES);
		generator.defineTipuApiSelection("pesa", "rak_laji", TipuAPIClient.SPECIES);
		// generator.defineTipuApiSelection("tarkastus", "pesiva_laji", TipuAPIClient.SPECIES);
		generator.defineTipuApiSelection("tarkastus", "pesiva_laji", TipuAPIClient.SPECIES);
		generator.defineTipuApiSelection("tarkastus", "yhdyskunta_laji", TipuAPIClient.SPECIES);
		generator.defineTipuApiSelection("tarkastus", "tarkastaja", TipuAPIClient.RINGERS);
		generator.noCodesForSelection("k_e");

		generator.userAsDefaultValue("pesa.seur_tarkastaja");
		generator.userAsDefaultValue("tarkastus.tarkastaja");

		Map<String, String> fieldTitles = new HashMap<>();
		for (Entry<String, String> e : request.data().uiTexts().entrySet()) {
			if (e.getKey().startsWith("tarkastuslomake.")) {
				// if (e.getKey().startsWith("tarkastuslomake.aikuinen.")) continue;
				fieldTitles.put(e.getKey().substring("tarkastuslomake.".length()), e.getValue());
			} else {
				if (!fieldTitles.containsKey(e.getKey())) {
					fieldTitles.put(e.getKey(), e.getValue());
				}
			}
		}
		definition.fieldTitles = fieldTitles;

		definition.layout = FileUtils.readContents(new File(config.templateFolder(), "layout.xml"));

		definition.emptyPDF = new File(config.templateFolder(), "tarkastus.pdf");
		//definition.instructionsPDF = new File(config.templateFolder(), "merikotkatutkimus-ohjeet.pdf");
		return generator;
	}

}
