package fi.hy.pese.framework.test.app;

import fi.hy.pese.framework.main.app.FunctionalityExecutor;
import fi.hy.pese.framework.main.app.Request;
import fi.hy.pese.framework.main.app.RequestImpleData;
import fi.hy.pese.framework.main.app._Functionality;
import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.app.functionality.BaseFunctionality;
import fi.hy.pese.framework.main.app.functionality.validate.Validator;
import fi.hy.pese.framework.main.app.functionality.validate._Validator;
import fi.hy.pese.framework.main.general.data.Data;
import fi.hy.pese.framework.main.general.data._Data;
import fi.hy.pese.framework.main.general.data._Row;
import fi.hy.pese.framework.test.RowFactory;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class FunctionalityExecutorTests {

	public static Test suite() {
		return new TestSuite(FunctionalityExecutorTests.class.getDeclaredClasses());
	}

	public static class ValidationErrorsAndWarnings extends TestCase {

		private static final String	AFTER_SUCCESSFUL_VALIDATION	= "after successful validation()";
		private static final String	AFTER_FAILED_VALIDATION	= "after failed validation()";

		private _Functionality<_Row> functionality;
		private _Request<_Row> request;

		@Override
		public void setUp() {
			RequestImpleData<_Row> reqData = new RequestImpleData<>();
			_Data<_Row> data = new Data<>(new RowFactory());
			reqData.setData(data);
			request = new Request<>(reqData);
			whichCalled = "";
		}

		private String whichCalled = "";

		private class Funct extends BaseFunctionality<_Row> {

			public Funct(_Request<_Row> request) {
				super(request);

			}
			@Override
			public void afterSuccessfulValidation(){
				whichCalled = AFTER_SUCCESSFUL_VALIDATION;
			}
			@Override
			public void afterFailedValidation() {
				whichCalled = AFTER_FAILED_VALIDATION;
			}
			@Override
			public _Validator validator() {
				class Valid extends Validator<_Row> {
					public Valid(_Request<_Row> request) {
						super(request);
					}
					@Override
					public void validate() throws Exception {
						warning("fieldname", "warning text");
					}
				}
				return new Valid(request);
			}
		}

		public void test___warnings() throws Exception {
			functionality = new Funct(request);
			FunctionalityExecutor.execute(functionality);
			assertEquals("warning text", request.data().warnings().get("fieldname"));
			assertEquals(AFTER_FAILED_VALIDATION, whichCalled);
		}

		public void test___warnings_2() throws Exception {
			functionality = new Funct(request);
			request.data().bypassWarning("fieldname");
			FunctionalityExecutor.execute(functionality);
			assertEquals(null, request.data().warnings().get("fieldname"));
			assertEquals(AFTER_SUCCESSFUL_VALIDATION, whichCalled);
		}

	}

}
