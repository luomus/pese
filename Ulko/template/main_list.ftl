<#include "header.ftl">

<#if data.tila??>
	<h1>${selections["pyynto.tila"][data.tila]}</h1>
<#else>
	<h1>Roskakori</h1>
</#if>

<#if results?has_content>
	<table class="tablesorter">
		<thead>
			<tr>
				<th>Diario</th>
				<th>Muokkauspvm</th>
				<th>Pyyntökirje lähetetty</th>
				<th>Löytökirje lähetetty</th>
				<th>Scheme</th>
				<th>Rengas</th>
				<th>Löytäjä</th>
				<th>Löytöpaikka</th>
				<th>Rengastuspaikka</th>
				<th>Kommentteja tapauksesta</th>
				<#if data.roskiin??>
					<th>Tila</th>
				</#if>
				<th class="{sorter: false}">&nbsp;</th>
			</tr>
		</thead>
		<tbody>
			<#list results as r>
				<tr class="linkki" onclick="location.href = '${system.baseURL}?id=${r.pyynto.id}'">
					<td>${r.pyynto.id}</td>
					<td>${r.pyynto.muutos_pvm.dateValue.year}-${r.pyynto.muutos_pvm.dateValue.month}-${r.pyynto.muutos_pvm.dateValue.day}</td>
					<#assign lahetetty = r.pyynto.rengaspyynto_lahetetty> 
					<td><#if lahetetty.value?has_content>${lahetetty.dateValue.year}-${lahetetty.dateValue.month}-${lahetetty.dateValue.day}</#if></td>
					<#assign lahetetty = r.pyynto.loytokirje_lahetetty> 
					<td><#if lahetetty.value?has_content>${lahetetty.dateValue.year}-${lahetetty.dateValue.month}-${lahetetty.dateValue.day}</#if></td>
					<td>${r.loyto.scheme}</td>
					<td>${r.loyto.rengas} ${r.loyto.rengas_selite}</td>
					<td>${r.pyynto.loytaja_nimi}<#if (r.pyynto.loytaja_renro?length > 0)> (${r.pyynto.loytaja_renro})</#if></td>
					<td>${selections["euring-provinces"][r.loyto.euring_paikka]}     ${r.loyto.kunta}     ${r.loyto.tarkempi_paikka}</td>
					<td>${selections["euring-provinces"][r.rengastus.euring_paikka]} ${r.rengastus.kunta} ${r.rengastus.tarkempi_paikka}</td>
					<td>${r.pyynto.kommentti}</td>
					<#if data.roskiin??>
						<td>${selections["pyynto.tila"][r.pyynto.tila]}</td>
					</#if>
					<td><button onclick="location.href = '${system.baseURL}?page=copy&id=${r.pyynto.id}'; var event = arguments[0] || window.event; event.stopPropagation();">Kopioi</button>
				</tr>
			</#list>
 		</tbody>
 	</table>
<#else>
	Ei mitään näytettävää.
</#if>

<#include "footer.ftl">
