package fi.hy.pese.pesakortti.main.app;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import fi.hy.pese.framework.main.db.oraclesql.Queries;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data.DatabaseColumnStructure;
import fi.hy.pese.framework.main.general.data.DatabaseTableStructure;
import fi.hy.pese.framework.main.general.data.PesaDomainModelDatabaseStructure;
import fi.hy.pese.framework.main.general.data._Table;
import fi.luomus.commons.containers.Selection;
import fi.luomus.commons.db.connectivity.TransactionConnection;
import fi.luomus.commons.tipuapi.TipuAPIClient;

public class PesakorttiDatabaseStructureCreator {
	
	private static final String	AIKUINEN_SUKUPUOLI	= "lintu.sukupuoli";
	
	public static PesaDomainModelDatabaseStructure loadFromDatabase(TransactionConnection con) throws SQLException {
		try {
			PesaDomainModelDatabaseStructure structure = new PesaDomainModelDatabaseStructure();
			
			// Taulut
			DatabaseTableStructure pesa = Queries.returnTableStructure("pesa", con);
			DatabaseTableStructure tarkastus = Queries.returnTableStructure("tarkastus", con);
			DatabaseTableStructure kaynti = Queries.returnTableStructure("kaynti", con);
			DatabaseTableStructure poikanen = Queries.returnTableStructure("poikanen", con);
			DatabaseTableStructure aikuinen = Queries.returnTableStructure("aikuinen", con);
			DatabaseTableStructure aputaulu = Queries.returnTableStructure("aputaulu", con);
			DatabaseTableStructure muutosloki = Queries.returnTableStructure("muutosloki", con);
			DatabaseTableStructure ponttoalue = Queries.returnTableStructure("ponttoalue", con);
			DatabaseTableStructure muna = Queries.returnTableStructure("muna", con);
			DatabaseTableStructure havainnoijalomake = Queries.returnTableStructure("havainnoijalomake", con);
			DatabaseTableStructure erikoistutkimus = Queries.returnTableStructure("erikoistutkimus", con);
			
			// Taulut templateen
			structure.setPesa(pesa);
			structure.setTarkastus(tarkastus);
			structure.setKaynti(kaynti);
			structure.setPoikanen(poikanen);
			structure.setAikuinen(aikuinen);
			structure.setAputaulu(aputaulu);
			structure.setLogTable(muutosloki);
			structure.setMuna(muna);
			structure.defineCustomTable(ponttoalue);
			structure.defineCustomTable(havainnoijalomake);
			structure.defineCustomTable(erikoistutkimus);
			structure.setTransactionEntry(Const.TRANSACTION_ENTRY_TABLE_STRUCTURE);
			
			// Ei käytettävät kentät
			pesa.removeColumn("pontto_luokittelu");
			tarkastus.removeColumn("yhdyskunnassa_vanha");
			
			// Liitokset selecteihin
			// Ensin aputaulussa olevan kentät automaattisesti
			Map<String, Selection> selections = Queries.returnSelectionsFromCodeTable(con, "aputaulu", "taulu", "kentta", "arvo", "selite", "ryhmittely", new String[] {"jarjestys"});
			for (String name : selections.keySet()) {
				try {
					DatabaseColumnStructure column = structure.getColumn(name);
					column.setAssosiatedSelection(name);
				} catch (Exception e) {  /* System.out.println(" Assosiation "+name + " does not match a field."); */ }
			}
			// Loput liitokset manuaalisesti
			pesa.get("koord_tarkkuus").setAssosiatedSelection("");
			pesa.get("seur_tarkastaja").setAssosiatedSelection(TipuAPIClient.RINGERS);
			pesa.get("lomake").setAssosiatedSelection(PesakorttiConst.K_E);
			pesa.get("kunta").setAssosiatedSelection(TipuAPIClient.MUNICIPALITIES);
			pesa.get("rak_laji").setAssosiatedSelection(TipuAPIClient.SPECIES);
			pesa.get("suojelu_kaytto").setAssosiatedSelection(PesakorttiConst.K_E);
			//tarkastus.get("tarkastettu").setAssosiatedSelection(PesakorttiConst.K_E);
			tarkastus.get("pesiva_laji").setAssosiatedSelection(TipuAPIClient.SPECIES);
			tarkastus.get("yhdyskunta_laji").setAssosiatedSelection(TipuAPIClient.SPECIES);
			tarkastus.get("yhdyskunnassa").setAssosiatedSelection(PesakorttiConst.K_E);
			tarkastus.get("tarkastaja").setAssosiatedSelection(TipuAPIClient.RINGERS);
			tarkastus.get("suojaus").setAssosiatedSelection(PesakorttiConst.K_E);
			aikuinen.get("laji").setAssosiatedSelection(TipuAPIClient.SPECIES);
			aikuinen.get("sukupuoli").setAssosiatedSelection(AIKUINEN_SUKUPUOLI);
			aikuinen.get("ika").setAssosiatedSelection(PesakorttiConst.IKAKOODIT_AIKUINEN);
			poikanen.get("sukupuoli").setAssosiatedSelection(AIKUINEN_SUKUPUOLI);
			poikanen.get("ika").setAssosiatedSelection(PesakorttiConst.IKAKOODIT_POIKANEN);
			poikanen.get("laji").setAssosiatedSelection(TipuAPIClient.SPECIES);
			aputaulu.get("taulu").setAssosiatedSelection(PesakorttiConst.TAULUT);
			ponttoalue.get("kunta").setAssosiatedSelection(TipuAPIClient.MUNICIPALITIES);
			ponttoalue.get("tarkastaja").setAssosiatedSelection(TipuAPIClient.RINGERS);
			muna.get("laji").setAssosiatedSelection(TipuAPIClient.SPECIES);
			tarkastus.get("epailyttava").setAssosiatedSelection(PesakorttiConst.K_E);
			erikoistutkimus.get("laji").setAssosiatedSelection(TipuAPIClient.SPECIES);
			erikoistutkimus.get("havainnoija").setAssosiatedSelection(TipuAPIClient.RINGERS);
			havainnoijalomake.get("havainnoija").setAssosiatedSelection(TipuAPIClient.RINGERS);
			
			loput(structure);
			
			//generateJavacodeFromDatabaseGeneratedDatabaseStructure(structure);
			
			return structure;
		} catch (Exception e) { e.printStackTrace(); throw new SQLException(e); }
	}
	
	private static void loput(PesaDomainModelDatabaseStructure structure) {
		
		DatabaseTableStructure pesa = structure.getPesa();
		DatabaseTableStructure tarkastus = structure.getTarkastus();
		DatabaseTableStructure kaynti = structure.getKaynti();
		DatabaseTableStructure poikanen = structure.getPoikanen();
		DatabaseTableStructure aikuinen = structure.getAikuinen();
		DatabaseTableStructure aputaulu = structure.getAputaulu();
		DatabaseTableStructure muutosloki = structure.getLogTable();
		DatabaseTableStructure muna = structure.getMuna();
		DatabaseTableStructure ponttoalue = structure.getTableStructureByName("ponttoalue");
		DatabaseTableStructure havainnoijalomake = structure.getTableStructureByName("havainnoijalomake");
		DatabaseTableStructure erikoistutkimus = structure.getTableStructureByName("erikoistutkimus");
		
		// N kpl sisältävien staattinen lukumäärä (listauksiin yms)
		structure.setPoikasetCount(15);
		structure.setAikuisetCount(3);
		structure.setKaynnitCount(15);
		structure.setMunatCount(12);
		
		// Avaimet
		pesa.get("id").setTypeToUniqueNumericIncreasingId();
		tarkastus.get("id").setTypeToUniqueNumericIncreasingId();
		kaynti.get("id").setTypeToUniqueNumericIncreasingId();
		poikanen.get("id").setTypeToUniqueNumericIncreasingId();
		aikuinen.get("id").setTypeToUniqueNumericIncreasingId();
		aputaulu.get("taulu").setTypeToImmutablePartOfAKey();
		aputaulu.get("kentta").setTypeToImmutablePartOfAKey();
		aputaulu.get("arvo").setTypeToImmutablePartOfAKey();
		muutosloki.get("id").setTypeToUniqueNumericIncreasingId();
		ponttoalue.get("id").setTypeToUniqueNumericIncreasingId();
		muna.get("id").setTypeToUniqueNumericIncreasingId();
		havainnoijalomake.get("havainnoija").setTypeToImmutablePartOfAKey();
		havainnoijalomake.get("vuosi").setTypeToImmutablePartOfAKey();
		erikoistutkimus.get("havainnoija").setTypeToImmutablePartOfAKey();
		erikoistutkimus.get("vuosi").setTypeToImmutablePartOfAKey();
		erikoistutkimus.get("laji").setTypeToImmutablePartOfAKey();
		
		// Viiteavaimet
		tarkastus.get("pesa").references(pesa.getUniqueNumericIdColumn());
		kaynti.get("tarkastus").references(tarkastus.getUniqueNumericIdColumn());
		poikanen.get("tarkastus").references(tarkastus.getUniqueNumericIdColumn());
		aikuinen.get("tarkastus").references(tarkastus.getUniqueNumericIdColumn());
		pesa.get("ponttoalue_nro").references(ponttoalue.getUniqueNumericIdColumn());
		muna.get("tarkastus").references(tarkastus.getUniqueNumericIdColumn());
		tarkastus.get("yhteispesinta").references(tarkastus.getUniqueNumericIdColumn());
		
		// Lisäyspäivämääräkentät
		pesa.get("kirj_pvm").setTypeToDateAddedColumn();
		tarkastus.get("kirj_pvm").setTypeToDateAddedColumn();
		kaynti.get("kirj_pvm").setTypeToDateAddedColumn();
		poikanen.get("kirj_pvm").setTypeToDateAddedColumn();
		aikuinen.get("kirj_pvm").setTypeToDateAddedColumn();
		muutosloki.get("kirj_pvm").setTypeToDateAddedColumn();
		ponttoalue.get("kirj_pvm").setTypeToDateAddedColumn();
		muna.get("kirj_pvm").setTypeToDateAddedColumn();
		
		// Muokkauspäivämääräkentät
		pesa.get("muutos_pvm").setTypeToDateModifiedColumn();
		tarkastus.get("muutos_pvm").setTypeToDateModifiedColumn();
		kaynti.get("muutos_pvm").setTypeToDateModifiedColumn();
		poikanen.get("muutos_pvm").setTypeToDateModifiedColumn();
		aikuinen.get("muutos_pvm").setTypeToDateModifiedColumn();
		ponttoalue.get("muutos_pvm").setTypeToDateModifiedColumn();
		muna.get("muutos_pvm").setTypeToDateModifiedColumn();
		
		// Sekalaiset
		muutosloki.get("kayttaja").setTypeToUserIdColumn();
		tarkastus.get("uusintapesinta_tunniste").setToUppercaseColumn();
		aikuinen.get("rengas").setToUppercaseColumn();
		poikanen.get("rengas").setToUppercaseColumn();
		
		// Tauluhallintaoikeudet
		aputaulu.setTablemanagementPermissions(_Table.TABLEMANAGEMENT_UPDATE_INSERT);
		muutosloki.setTablemanagementPermissions(_Table.TABLEMANAGEMENT_INSERT_ONLY);
		ponttoalue.setTablemanagementPermissions(_Table.TABLEMANAGEMENT_UPDATE_INSERT_DELETE);
		havainnoijalomake.setTablemanagementPermissions(_Table.TABLEMANAGEMENT_UPDATE_INSERT_DELETE);
		erikoistutkimus.setTablemanagementPermissions(_Table.TABLEMANAGEMENT_UPDATE_INSERT_DELETE);
		
		
		// Kirjekyyhky pakolliset
		pesa.get("tarkempi_paikka").setKirjekyyhkyRequired();
		tarkastus.get("tarkastaja").setKirjekyyhkyRequired();
		tarkastus.get("pesimistulos").setKirjekyyhkyRequired();
		pesa.get("kunta").setKirjekyyhkyRequired();
		tarkastus.get("vuosi").setKirjekyyhkyRequired();
		pesa.get("koord_ilm_tyyppi").setKirjekyyhkyRequired();
		pesa.get("koord_tarkkuus").setKirjekyyhkyRequired();
		pesa.get("koord_mittaustapa").setKirjekyyhkyRequired();
		
		// Numeric ranges
		kaynti.get("klo_h").setValueLimits(0, 23);
		kaynti.get("klo_min").setValueLimits(0, 59);
		
		// Tablemanagement ei-pakolliset
		havainnoijalomake.get("aika_maalis").setToOptionalColumn();
		havainnoijalomake.get("aika_huhti").setToOptionalColumn();
		havainnoijalomake.get("aika_touko").setToOptionalColumn();
		havainnoijalomake.get("aika_kesa").setToOptionalColumn();
		havainnoijalomake.get("aika_heina").setToOptionalColumn();
		havainnoijalomake.get("aika_elo").setToOptionalColumn();
		aputaulu.get("ryhmittely").setToOptionalColumn();
		
		// Muokataan kenttien järjestyksiä
		List<String> orderOfAputauluColumns = new ArrayList<>();
		orderOfAputauluColumns.add("taulu");
		orderOfAputauluColumns.add("kentta");
		orderOfAputauluColumns.add("jarjestys");
		orderOfAputauluColumns.add("arvo");
		orderOfAputauluColumns.add("selite");
		aputaulu.reorder(orderOfAputauluColumns);
		
		List<String> orderOfTarkastusColumns = new ArrayList<>();
		orderOfTarkastusColumns.add("vuosi");
		orderOfTarkastusColumns.add("vanha_kortti");
		orderOfTarkastusColumns.add("tarkastaja");
		orderOfTarkastusColumns.add("pesiva_laji");
		orderOfTarkastusColumns.add("pesimistulos");
		orderOfTarkastusColumns.add("pesa_korkeus");
		orderOfTarkastusColumns.add("puu_korkeus");
		orderOfTarkastusColumns.add("pesa_etaisyys_runko");
		orderOfTarkastusColumns.add("pesa_biotooppi");
		orderOfTarkastusColumns.add("pesa_biotooppi_ala");
		orderOfTarkastusColumns.add("pesa_suhde_reuna");
		orderOfTarkastusColumns.add("pesa_etaisyys_reuna");
		orderOfTarkastusColumns.add("pesa_ymp_biotooppi");
		orderOfTarkastusColumns.add("uusintapesinta_jarjestysnro");
		orderOfTarkastusColumns.add("uusintapesinta_tunniste");
		orderOfTarkastusColumns.add("uusintapesinta_perustelut");
		orderOfTarkastusColumns.add("yhdyskunnassa");
		orderOfTarkastusColumns.add("yhdyskunta_laji");
		orderOfTarkastusColumns.add("yhdyskunta_koko");
		orderOfTarkastusColumns.add("pesa_kunto");
		orderOfTarkastusColumns.add("pontto_ripustus");
		orderOfTarkastusColumns.add("suojaus");
		orderOfTarkastusColumns.add("yhteispesinta");
		orderOfTarkastusColumns.add("epailyttava");
		orderOfTarkastusColumns.add("kommentti");
		orderOfTarkastusColumns.add("muninta_pvm");
		orderOfTarkastusColumns.add("taydellinen_munaluku");
		orderOfTarkastusColumns.add("kirj_pvm");
		orderOfTarkastusColumns.add("muutos_pvm");
		orderOfTarkastusColumns.add("id");
		orderOfTarkastusColumns.add("pesa");
		tarkastus.reorder(orderOfTarkastusColumns);
		
		List<String> orderOfKayntiColumns = new ArrayList<>();
		for (DatabaseColumnStructure c : kaynti.getColumns()) {
			String name = c.getName();
			if (!name.equals("id") && !name.equals("tarkastus")) {
				orderOfKayntiColumns.add(name);
			}
		}
		kaynti.reorder(orderOfKayntiColumns);
		
		List<String> orderOfPoikanenColumns = new ArrayList<>();
		for (DatabaseColumnStructure c : poikanen.getColumns()) {
			String name = c.getName();
			if (!name.equals("id") && !name.equals("tarkastus")) {
				orderOfPoikanenColumns.add(name);
			}
		}
		poikanen.reorder(orderOfPoikanenColumns);
		
		List<String> orderOfAikuinenColumns = new ArrayList<>();
		for (DatabaseColumnStructure c : aikuinen.getColumns()) {
			String name = c.getName();
			if (!name.equals("id") && !name.equals("tarkastus")) {
				orderOfAikuinenColumns.add(name);
			}
		}
		aikuinen.reorder(orderOfAikuinenColumns);
		
		List<String> orderOfMunaColumns = new ArrayList<>();
		for (DatabaseColumnStructure c : muna.getColumns()) {
			String name = c.getName();
			if (!name.equals("id") && !name.equals("tarkastus")) {
				orderOfMunaColumns.add(name);
			}
		}
		muna.reorder(orderOfMunaColumns);
	}
	
	// ---------------------------------- ---------------------------------- ---------------------------------- ---------------------------------- ----------------------------------
	
	public static PesaDomainModelDatabaseStructure hardCoded() {
		PesaDomainModelDatabaseStructure structure = new PesaDomainModelDatabaseStructure();
		DatabaseTableStructure pesa = new DatabaseTableStructure("pesa");
		pesa.addColumn("id", 2, 22);
		pesa.addColumn("seur_tarkastaja", 2, 5);
		pesa.addColumn("lomake", 1, 1);
		pesa.addColumn("kunta", 1, 7);
		pesa.addColumn("tarkempi_paikka", 1, 35);
		pesa.addColumn("nimi", 1, 35);
		pesa.addColumn("yht_leveys", 2, 7);
		pesa.addColumn("yht_pituus", 2, 7);
		pesa.addColumn("eur_leveys", 2, 7);
		pesa.addColumn("eur_pituus", 2, 6);
		pesa.addColumn("ast_leveys", 2, 6);
		pesa.addColumn("ast_pituus", 2, 6);
		pesa.addColumn("des_leveys", 3, 10 , 7);
		pesa.addColumn("des_pituus", 3, 10 , 7);
		pesa.addColumn("koord_ilm_tyyppi", 1, 1);
		pesa.addColumn("koord_tarkkuus", 2, 8);
		pesa.addColumn("koord_mittaustapa", 1, 1);
		pesa.addColumn("suojelu_kaytto", 1, 1);
		pesa.addColumn("pesa_sijainti", 2, 2);
		pesa.addColumn("pontto_nro", 2, 5);
		pesa.addColumn("ponttoalue_nro", 2, 5);
		pesa.addColumn("loytotapa", 2, 1);
		pesa.addColumn("rak_laji", 1, 7);
		pesa.addColumn("kommentti", 1, 500);
		pesa.addColumn("kirj_pvm", 4);
		pesa.addColumn("muutos_pvm", 4);
		DatabaseTableStructure tarkastus = new DatabaseTableStructure("tarkastus");
		tarkastus.addColumn("vuosi", 2, 4);
		tarkastus.addColumn("vanha_kortti", 2, 10);
		tarkastus.addColumn("tarkastaja", 2, 5);
		tarkastus.addColumn("pesiva_laji", 1, 7);
		tarkastus.addColumn("pesimistulos", 2, 2);
		tarkastus.addColumn("pesa_korkeus", 3, 3 , 1);
		tarkastus.addColumn("puu_korkeus", 2, 2);
		tarkastus.addColumn("pesa_etaisyys_runko", 3, 3 , 1);
		tarkastus.addColumn("pesa_biotooppi", 2, 2);
		tarkastus.addColumn("pesa_biotooppi_ala", 2, 1);
		tarkastus.addColumn("pesa_suhde_reuna", 2, 1);
		tarkastus.addColumn("pesa_etaisyys_reuna", 2, 1);
		tarkastus.addColumn("pesa_ymp_biotooppi", 2, 1);
		tarkastus.addColumn("uusintapesinta_jarjestysnro", 2, 1);
		tarkastus.addColumn("uusintapesinta_tunniste", 1, 30);
		tarkastus.addColumn("uusintapesinta_perustelut", 1, 500);
		tarkastus.addColumn("yhdyskunnassa", 1, 1);
		tarkastus.addColumn("yhdyskunta_laji", 1, 7);
		tarkastus.addColumn("yhdyskunta_koko", 2, 5);
		tarkastus.addColumn("pesa_kunto", 2, 1);
		tarkastus.addColumn("pontto_ripustus", 2, 4);
		tarkastus.addColumn("suojaus", 1, 1);
		tarkastus.addColumn("yhteispesinta", 2, 22);
		tarkastus.addColumn("epailyttava", 1, 1);
		tarkastus.addColumn("kommentti", 1, 500);
		tarkastus.addColumn("muninta_pvm", 4);
		tarkastus.addColumn("taydellinen_munaluku", 2, 2);
		tarkastus.addColumn("kirj_pvm", 4);
		tarkastus.addColumn("muutos_pvm", 4);
		tarkastus.addColumn("id", 2, 22);
		tarkastus.addColumn("pesa", 2, 22);
		DatabaseTableStructure kaynti = new DatabaseTableStructure("kaynti");
		kaynti.addColumn("pvm", 4);
		kaynti.addColumn("pvm_tarkkuus", 2, 1);
		kaynti.addColumn("klo_h", 2, 2);
		kaynti.addColumn("klo_min", 2, 2);
		kaynti.addColumn("lkm_munat", 2, 2);
		kaynti.addColumn("lkm_munat_kuolleet", 2, 2);
		kaynti.addColumn("lkm_poikaset", 2, 2);
		kaynti.addColumn("lkm_poikaset_kuolleet", 2, 2);
		kaynti.addColumn("pesinnan_vaihe", 1, 2);
		kaynti.addColumn("ika_munat", 2, 3);
		kaynti.addColumn("ika_poikaset", 2, 3);
		kaynti.addColumn("kommentti", 1, 500);
		kaynti.addColumn("kirj_pvm", 4);
		kaynti.addColumn("muutos_pvm", 4);
		kaynti.addColumn("id", 2, 22);
		kaynti.addColumn("tarkastus", 2, 22);
		DatabaseTableStructure poikanen = new DatabaseTableStructure("poikanen");
		poikanen.addColumn("laji", 1, 7);
		poikanen.addColumn("rengas", 1, 10);
		poikanen.addColumn("sukupuoli", 1, 1);
		poikanen.addColumn("ika", 1, 2);
		poikanen.addColumn("kommentti", 1, 500);
		poikanen.addColumn("kirj_pvm", 4);
		poikanen.addColumn("muutos_pvm", 4);
		poikanen.addColumn("id", 2, 22);
		poikanen.addColumn("tarkastus", 2, 22);
		DatabaseTableStructure aikuinen = new DatabaseTableStructure("aikuinen");
		aikuinen.addColumn("laji", 1, 7);
		aikuinen.addColumn("rengas", 1, 10);
		aikuinen.addColumn("sukupuoli", 1, 1);
		aikuinen.addColumn("ika", 1, 2);
		aikuinen.addColumn("kommentti", 1, 500);
		aikuinen.addColumn("kirj_pvm", 4);
		aikuinen.addColumn("muutos_pvm", 4);
		aikuinen.addColumn("id", 2, 22);
		aikuinen.addColumn("tarkastus", 2, 22);
		DatabaseTableStructure aputaulu = new DatabaseTableStructure("aputaulu");
		aputaulu.addColumn("taulu", 1, 20);
		aputaulu.addColumn("kentta", 1, 30);
		aputaulu.addColumn("jarjestys", 2, 2);
		aputaulu.addColumn("arvo", 1, 2);
		aputaulu.addColumn("selite", 1, 120);
		aputaulu.addColumn("ryhmittely", 1, 120);
		DatabaseTableStructure muutosloki = new DatabaseTableStructure("muutosloki");
		muutosloki.addColumn("id", 2, 22);
		muutosloki.addColumn("pesa", 2, 22);
		muutosloki.addColumn("kirjaus", 1, 500);
		muutosloki.addColumn("kirj_pvm", 4);
		muutosloki.addColumn("kayttaja", 1, 120);
		DatabaseTableStructure muna = new DatabaseTableStructure("muna");
		muna.addColumn("laji", 1, 7);
		muna.addColumn("leveys", 3, 4 , 1);
		muna.addColumn("pituus", 3, 4 , 1);
		muna.addColumn("kommentti", 1, 500);
		muna.addColumn("kirj_pvm", 4);
		muna.addColumn("muutos_pvm", 4);
		muna.addColumn("id", 2, 22);
		muna.addColumn("tarkastus", 2, 22);
		DatabaseTableStructure ponttoalue = new DatabaseTableStructure("ponttoalue");
		ponttoalue.addColumn("id", 2, 5);
		ponttoalue.addColumn("tarkastaja", 2, 5);
		ponttoalue.addColumn("kunta", 1, 7);
		ponttoalue.addColumn("kommentti", 1, 500);
		ponttoalue.addColumn("kirj_pvm", 4);
		ponttoalue.addColumn("muutos_pvm", 4);
		DatabaseTableStructure havainnoijalomake = new DatabaseTableStructure("havainnoijalomake");
		havainnoijalomake.addColumn("havainnoija", 2, 5);
		havainnoijalomake.addColumn("vuosi", 2, 4);
		havainnoijalomake.addColumn("aika_maalis", 2, 2);
		havainnoijalomake.addColumn("aika_huhti", 2, 2);
		havainnoijalomake.addColumn("aika_touko", 2, 2);
		havainnoijalomake.addColumn("aika_kesa", 2, 2);
		havainnoijalomake.addColumn("aika_heina", 2, 2);
		havainnoijalomake.addColumn("aika_elo", 2, 2);
		DatabaseTableStructure erikoistutkimus = new DatabaseTableStructure("erikoistutkimus");
		erikoistutkimus.addColumn("havainnoija", 2, 5);
		erikoistutkimus.addColumn("vuosi", 2, 4);
		erikoistutkimus.addColumn("laji", 1, 7);
		pesa.get("seur_tarkastaja").setAssosiatedSelection(TipuAPIClient.RINGERS);
		pesa.get("lomake").setAssosiatedSelection("k_e");
		pesa.get("kunta").setAssosiatedSelection(TipuAPIClient.MUNICIPALITIES);
		pesa.get("koord_ilm_tyyppi").setAssosiatedSelection("pesa.koord_ilm_tyyppi");
		pesa.get("koord_mittaustapa").setAssosiatedSelection("pesa.koord_mittaustapa");
		pesa.get("suojelu_kaytto").setAssosiatedSelection("k_e");
		pesa.get("pesa_sijainti").setAssosiatedSelection("pesa.pesa_sijainti");
		pesa.get("loytotapa").setAssosiatedSelection("pesa.loytotapa");
		pesa.get("rak_laji").setAssosiatedSelection(TipuAPIClient.SPECIES);
		tarkastus.get("tarkastaja").setAssosiatedSelection(TipuAPIClient.RINGERS);
		tarkastus.get("pesiva_laji").setAssosiatedSelection(TipuAPIClient.SPECIES);
		tarkastus.get("pesimistulos").setAssosiatedSelection("tarkastus.pesimistulos");
		tarkastus.get("pesa_biotooppi").setAssosiatedSelection("tarkastus.pesa_biotooppi");
		tarkastus.get("pesa_biotooppi_ala").setAssosiatedSelection("tarkastus.pesa_biotooppi_ala");
		tarkastus.get("pesa_suhde_reuna").setAssosiatedSelection("tarkastus.pesa_suhde_reuna");
		tarkastus.get("pesa_etaisyys_reuna").setAssosiatedSelection("tarkastus.pesa_etaisyys_reuna");
		tarkastus.get("pesa_ymp_biotooppi").setAssosiatedSelection("tarkastus.pesa_ymp_biotooppi");
		tarkastus.get("yhdyskunnassa").setAssosiatedSelection("k_e");
		tarkastus.get("yhdyskunta_laji").setAssosiatedSelection(TipuAPIClient.SPECIES);
		tarkastus.get("pesa_kunto").setAssosiatedSelection("tarkastus.pesa_kunto");
		tarkastus.get("suojaus").setAssosiatedSelection("k_e");
		tarkastus.get("epailyttava").setAssosiatedSelection("k_e");
		kaynti.get("pvm_tarkkuus").setAssosiatedSelection("kaynti.pvm_tarkkuus");
		kaynti.get("pesinnan_vaihe").setAssosiatedSelection("kaynti.pesinnan_vaihe");
		poikanen.get("laji").setAssosiatedSelection(TipuAPIClient.SPECIES);
		poikanen.get("sukupuoli").setAssosiatedSelection("lintu.sukupuoli");
		poikanen.get("ika").setAssosiatedSelection("ikakoodit_poikanen");
		aikuinen.get("laji").setAssosiatedSelection(TipuAPIClient.SPECIES);
		aikuinen.get("sukupuoli").setAssosiatedSelection("lintu.sukupuoli");
		aikuinen.get("ika").setAssosiatedSelection("ikakoodit_aikuinen");
		aputaulu.get("taulu").setAssosiatedSelection("taulut");
		muna.get("laji").setAssosiatedSelection(TipuAPIClient.SPECIES);
		ponttoalue.get("tarkastaja").setAssosiatedSelection(TipuAPIClient.RINGERS);
		ponttoalue.get("kunta").setAssosiatedSelection(TipuAPIClient.MUNICIPALITIES);
		havainnoijalomake.get("havainnoija").setAssosiatedSelection(TipuAPIClient.RINGERS);
		erikoistutkimus.get("havainnoija").setAssosiatedSelection(TipuAPIClient.RINGERS);
		erikoistutkimus.get("laji").setAssosiatedSelection(TipuAPIClient.SPECIES);
		
		// Taulut templateen
		structure.setPesa(pesa);
		structure.setTarkastus(tarkastus);
		structure.setKaynti(kaynti);
		structure.setPoikanen(poikanen);
		structure.setAikuinen(aikuinen);
		structure.setAputaulu(aputaulu);
		structure.setLogTable(muutosloki);
		structure.setMuna(muna);
		structure.defineCustomTable(ponttoalue);
		structure.defineCustomTable(havainnoijalomake);
		structure.defineCustomTable(erikoistutkimus);
		
		loput(structure);
		return structure;
	}
	
	//	public static void generateJavacodeFromDatabaseGeneratedDatabaseStructure(PesaDomainModelDatabaseStructure structure) {
	//		for (DatabaseTableStructure t : structure.getAllTables()) {
	//			String tablename = t.getName();
	//			System.out.println("DatabaseTableStructure "+t.getName()+" = new DatabaseTableStructure(\""+t.getName()+"\");");
	//			for (DatabaseColumnStructure c : t.getColumns()) {
	//				if (c.getName().equals(Const.ROWID)) continue;
	//				if (c.getDatatype() == _Column.VARCHAR)
	//					System.out.println(tablename+".addColumn(\""+c.getName()+"\", "+c.getDatatype()+", "+c.getSize()+");");
	//				else if (c.getDatatype() == _Column.DATE)
	//					System.out.println(tablename+".addColumn(\""+c.getName()+"\", "+c.getDatatype()+");");
	//				else if (c.getDatatype() == _Column.INTEGER)
	//					System.out.println(tablename+".addColumn(\""+c.getName()+"\", "+c.getDatatype()+", "+c.getSize()+");");
	//				else if (c.getDatatype() == _Column.DECIMAL)
	//					System.out.println(tablename+".addColumn(\""+c.getName()+"\", "+c.getDatatype()+", "+c.getSize()+" , "+c.getDecimalSize()+");");
	//			}
	//		}
	//		for (DatabaseTableStructure t : structure.getAllTables()) {
	//			String tablename = t.getName();
	//			for (DatabaseColumnStructure c : t.getColumns()) {
	//				if (!c.getAssosiatedSelection().equals("")) {
	//					System.out.println(tablename+".get(\""+c.getName()+"\").setAssosiatedSelection(\""+c.getAssosiatedSelection()+"\");");
	//				}
	//			}
	//		}
	//	}
	
}
