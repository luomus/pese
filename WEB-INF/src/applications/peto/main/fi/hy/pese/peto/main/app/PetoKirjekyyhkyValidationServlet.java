package fi.hy.pese.peto.main.app;

import fi.hy.pese.framework.main.app.KirjekyyhkyValidatorServlet;
import fi.hy.pese.framework.main.app._ApplicationSpecificationFactory;
import fi.hy.pese.framework.main.general.data.PesaDomainModelDatabaseStructure;
import fi.hy.pese.framework.main.general.data.PesaDomainModelRowFactory;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.luomus.commons.db.connectivity.SimpleTransactionConnection;
import fi.luomus.commons.db.connectivity.TransactionConnection;

public class PetoKirjekyyhkyValidationServlet extends KirjekyyhkyValidatorServlet<_PesaDomainModelRow> {

	private static final long	serialVersionUID	= 1805476044253318666L;

	@Override
	protected _ApplicationSpecificationFactory<_PesaDomainModelRow> createFactory() throws Exception {
		PetoKirjekyyhkyValidationServiceFactory factory = new PetoKirjekyyhkyValidationServiceFactory("pese_peto.config");
		factory.loadUITexts();

		TransactionConnection con = new SimpleTransactionConnection(factory.config().connectionDescription());
		try {
			PesaDomainModelDatabaseStructure structure = PetoDatabaseStructureCreator.loadFromDatabase(con);
			factory.setRowFactory(new PesaDomainModelRowFactory(structure, PetoConst.JOINS, PetoConst.ORDER_BY));
		} catch (Exception e) {
			con.release();
			throw new Exception(e);
		}

		con.release();
		return factory;
	}

}
