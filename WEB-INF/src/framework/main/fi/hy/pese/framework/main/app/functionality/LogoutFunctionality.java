package fi.hy.pese.framework.main.app.functionality;

import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.app.functionality.validate.EmptyValidator;
import fi.hy.pese.framework.main.app.functionality.validate._Validator;
import fi.hy.pese.framework.main.general.data._Row;
import fi.luomus.commons.config.Config;

public class LogoutFunctionality<T extends _Row> extends BaseFunctionality<T> {

	public LogoutFunctionality(_Request<T> request) {
		super(request);
	}

	@Override
	public _Validator validator() {
		return new EmptyValidator();
	}

	@Override
	public void preProsessingHook() throws Exception {
		request.session().removeAuthentication(request.config().systemId());
		request.redirecter().redirectTo(request.config().get(Config.LINTUVAARA_URL));
	}

}
