package fi.hy.pese.framework.test.db;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import fi.hy.pese.framework.main.db._DAO;
import fi.hy.pese.framework.main.db._ResultsHandler;
import fi.hy.pese.framework.main.db.oraclesql.NothingToDeleteException;
import fi.hy.pese.framework.main.general.data._Data;
import fi.hy.pese.framework.main.general.data._IndexedTablegroup;
import fi.hy.pese.framework.main.general.data._ReferenceKey;
import fi.hy.pese.framework.main.general.data._Row;
import fi.hy.pese.framework.main.general.data._RowFactory;
import fi.hy.pese.framework.main.general.data._Table;
import fi.luomus.commons.containers.Selection;
import fi.luomus.commons.containers.SelectionImple;

public class DAOStub<T extends _Row> implements _DAO<T> {

	private final _RowFactory<T> rowFactory;

	public DAOStub() {
		rowFactory = null;
	}

	public DAOStub(_RowFactory<T> rowFactory) {
		this.rowFactory = rowFactory;
	}

	@Override
	public void commit() {
		// Auto-generated method stub

	}

	@Override
	public void rollback() {
		// Auto-generated method stub

	}

	@Override
	public SelectionImple returnSelections(String tablename, String valuefield, String descriptionfield, String[] orderBy) throws SQLException {
		// Auto-generated method stub
		return null;
	}

	@Override
	public void addSavedSearchesListingToData(_Data<T> data) throws SQLException {
		// Auto-generated method stub

	}

	@Override
	public boolean checkReferenceValueExists(String value, _ReferenceKey referenceKey) throws SQLException {
		// Auto-generated method stub
		return false;
	}

	@Override
	public boolean checkRowExists(_Table table) throws SQLException {
		// Auto-generated method stub
		return false;
	}

	@Override
	public boolean checkRowIDMatchesKeys(_Table table) throws SQLException {
		// Auto-generated method stub
		return false;
	}

	@Override
	public boolean checkValuesExists(_Table table) throws SQLException {
		// Auto-generated method stub
		return false;
	}

	@Override
	public void executeDelete(_Table table) throws SQLException {
		// Auto-generated method stub

	}

	@Override
	public void executeInsert(_Table table) throws SQLException {
		// Auto-generated method stub

	}

	@Override
	public void executeSearch(String sql, _ResultsHandler resultHandler, String... values) throws SQLException {
		// Auto-generated method stub

	}

	@Override
	public void executeSearch(_Table parameters, _ResultsHandler resultHandler) throws SQLException {
		// Auto-generated method stub

	}

	@Override
	public void executeSearch(_Table parameters, _ResultsHandler resultHandler, String... orderBy) throws SQLException {
		// Auto-generated method stub

	}

	@Override
	public int executeUpdate(String sql, String... values) throws SQLException {
		// Auto-generated method stub
		return 0;
	}

	@Override
	public void executeUpdate(_Table table) throws SQLException {
		// Auto-generated method stub

	}

	@Override
	public String returnAndSetNextId(_Table table) throws SQLException {
		// Auto-generated method stub
		return null;
	}

	@Override
	public long returnSelectionsUpdatedTime() throws SQLException {
		// Auto-generated method stub
		return 0;
	}

	@Override
	public _Table returnTableByKeys(String tablename, String... keys) throws SQLException {
		// Auto-generated method stub
		return null;
	}

	@Override
	public _Table returnTableByRowid(String tablename, String rowid) throws SQLException {
		// Auto-generated method stub
		return null;
	}

	@Override
	public String returnValueByRowid(String table, String valueColumn, String rowid) throws SQLException {
		// Auto-generated method stub
		return null;
	}

	@Override
	public void setSelectionsUpdateTime() throws SQLException {
		// Auto-generated method stub

	}

	@Override
	public void startTransaction() throws SQLException {
		// Auto-generated method stub

	}

	@Override
	public _Table newTableByName(String name) throws IllegalArgumentException {
		if (rowFactory == null) return null;
		return rowFactory.newRow().getTableByName(name);
	}

	@Override
	public T newRow() {
		if (rowFactory == null) return null;
		return rowFactory.newRow();
	}

	@Override
	public List<_Table> returnTable(String tablename) throws SQLException {
		return null;
	}

	@Override
	public PreparedStatement prepareStatement(String string) throws SQLException {
		return null;
	}

	@Override
	public List<_Table> returnTables(_Table searchparams) throws SQLException {
		return null;
	}

	@Override
	public void executeSearch(T parameters, _ResultsHandler resultHandler, String... additional) throws SQLException {
		// Auto-generated method stub

	}

	@Override
	public void executeDelete(_IndexedTablegroup tables) throws SQLException {
		//  Auto-generated method stub

	}

	@Override
	public Map<String, Selection> returnCodeSelections(String codeTable, String tableField, String fieldField, String valueField, String descField, String groupField, String[] orderBy) throws SQLException {
		// Auto-generated method stub
		return null;
	}

	@Override
	public List<_Table> returnTables(_Table searchparams, String... orderBy) throws SQLException {
		return null;
	}

	@Override
	public void markForWarehousing(int id) {
		// Auto-generated method stub

	}

	@Override
	public List<_Table> returnTables(_Table searchparams, int limit, String... orderBy) throws SQLException {
		//  Auto-generated method stub
		return null;
	}

	@Override
	public void markWarehousingAttempted(_Table transactionEntry) throws SQLException {
		// Auto-generated method stub

	}

	@Override
	public void markWarehousingSuccess(_Table transactionEntry) throws NothingToDeleteException, SQLException {
		// Auto-generated method stub

	}

	@Override
	public void markAllWarehousingUnattempted() throws SQLException {
		// Auto-generated method stub

	}

}
