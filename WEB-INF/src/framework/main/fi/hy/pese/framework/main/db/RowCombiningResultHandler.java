package fi.hy.pese.framework.main.db;

import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;

/**
 * This abstract utility class combines results from dao.executeSearch(_Row.. ) to _Row and asks the
 * user of this class to do something with the table.
 * For example if _ResultSet contains rows
 * tarkastus_id, poikanen_id, aikuinen_id
 * 1, 1, 1
 * 1, 2, 1
 * 2, -, -
 * 3, 3, 2
 * 3, 3, 3
 * 3, 4, 2
 * 3, 4, 3
 * this combines three different _Rows, of which first one has p1 and a1,
 * second has no poikanen or aikuinen
 * third has p1, p2 and a1, a2
 */
public abstract class RowCombiningResultHandler extends BaseResultsHandler {
	
	private final _DAO<_PesaDomainModelRow>			dao;
	private int					kaynti_index	= 1;
	private int					poikanen_index	= 1;
	private int					aikuinen_index	= 1;
	private int					muna_index	    = 1;
	private String				prev_rowid		= "";
	private String				current_rowid	= "";
	private _PesaDomainModelRow				row				= null;
	private final Set<String>	addedPoikanen	= new HashSet<>();
	private final Set<String>	addedAikuinen	= new HashSet<>();
	private final Set<String>	addedKaynti		= new HashSet<>();
	private final Set<String>	addedMuna		= new HashSet<>();
	
	private final boolean		kayntiInUse;
	private final boolean		poikanenInUse;
	private final boolean		aikuinenInUse;
	private final boolean		munaInUse;
	
	public RowCombiningResultHandler(_DAO<_PesaDomainModelRow> dao) {
		this.dao = dao;
		_PesaDomainModelRow row = dao.newRow();
		kayntiInUse = row.getKaynti().isInitialized();
		poikanenInUse = row.getPoikanen().isInitialized();
		aikuinenInUse = row.getAikuinen().isInitialized();
		munaInUse = row.getMuna().isInitialized();
	}
	
	/**
	 * Do something to the _Row that has been combined. If set to return false, processing is not continued to the next row.
	 * @param row
	 * @return true -> continue processing, false -> end processing (even if resultset still would have rows)
	 */
	protected abstract boolean handle(_PesaDomainModelRow row);
	
	@Override
	public void process(_ResultSet rs) throws SQLException {
		while (rs.next()) {
			current_rowid = rs.getString(1);
			if (tarkastusChanges()) {
				if (row != null) {
					boolean continueProcessing = handle(row);
					if (!continueProcessing) return;
				}
				row = dao.newRow();
				kaynti_index = 1;
				poikanen_index = 1;
				aikuinen_index = 1;
				muna_index = 1;
				prev_rowid = current_rowid;
				firstRow(rs);
			} else {
				kayntiPoikanenAikuinenMunaRow(rs);
			}
		}
		if (row != null) handle(row);
	}
	
	private boolean tarkastusChanges() {
		return !prev_rowid.equals(current_rowid);
	}
	
	private void firstRow(_ResultSet rs) throws SQLException {
		int i = 1;
		i = setResultsToTable(rs, row.getTarkastus(), i);
		
		if (kayntiInUse) {
			String kaynti_rowid = rs.getString(i);
			if (isNull(kaynti_rowid)) {
				i = skip(row.getKaynti(), i);
			} else {
				i = kayntiRow(rs, i, kaynti_rowid);
			}
		}
		
		if (poikanenInUse) {
			String poikanen_rowid = rs.getString(i);
			if (isNull(poikanen_rowid)) {
				i = skip(row.getPoikanen(), i);
			} else {
				i = poikanenRow(rs, i, poikanen_rowid);
			}
		}
		
		if (aikuinenInUse) {
			String aikuinen_rowid = rs.getString(i);
			if (isNull(aikuinen_rowid)) {
				i = skip(row.getAikuinen(), i);
			} else {
				i = aikuinenRow(rs, i, aikuinen_rowid);
			}
		}
		
		if (munaInUse) {
			String muna_rowid = rs.getString(i);
			if (isNull(muna_rowid)) {
				i = skip(row.getMuna(), i);
			} else {
				i = munaRow(rs, i, muna_rowid);
			}
		}
		
		i = setResultsToTable(rs, row.getPesa(), i);
		i = setResultsToTable(rs, row.getOlosuhde(), i);
		i = setResultsToTable(rs, row.getVuosi(), i);
		i = setResultsToTable(rs, row.getReviiri(), i);
	}
	
	private boolean isNull(String s) {
		return s == null || s.equals("");
	}
	
	private void kayntiPoikanenAikuinenMunaRow(_ResultSet rs) throws SQLException {
		int i = 1;
		i = skip(row.getTarkastus(), i);
		
		if (kayntiInUse) {
			String kaynti_rowid = rs.getString(i);
			if (isNull(kaynti_rowid) || addedKaynti.contains(kaynti_rowid)) {
				i = skip(row.getKaynti(), i);
			} else {
				i = kayntiRow(rs, i, kaynti_rowid);
			}
		}
		
		if (poikanenInUse) {
			String poikanen_rowid = rs.getString(i);
			if (isNull(poikanen_rowid) || addedPoikanen.contains(poikanen_rowid)) {
				i = skip(row.getPoikanen(), i);
			} else {
				i = poikanenRow(rs, i, poikanen_rowid);
			}
		}
		
		if (aikuinenInUse) {
			String aikuinen_rowid = rs.getString(i);
			if (isNull(aikuinen_rowid) || addedAikuinen.contains(aikuinen_rowid)) {
				i = skip(row.getAikuinen(), i);
			} else {
				i = aikuinenRow(rs, i, aikuinen_rowid);
			}
		}
		
		if (munaInUse) {
			String muna_rowid = rs.getString(i);
			if (isNull(muna_rowid) || addedMuna.contains(muna_rowid)) {
				i = skip(row.getMuna(), i);
			} else {
				i = munaRow(rs, i, muna_rowid);
			}
		}
		
		i = skip(row.getPesa(), i);
		i = skip(row.getOlosuhde(), i);
		i = skip(row.getVuosi(), i);
		i = skip(row.getReviiri(), i);
	}
	
	private int kayntiRow(_ResultSet rs, int i, String kaynti_rowid) throws SQLException {
		i = setResultsToTable(rs, row.getKaynnit().get(kaynti_index), i);
		addedKaynti.add(kaynti_rowid);
		kaynti_index++;
		return i;
	}
	
	private int aikuinenRow(_ResultSet rs, int i, String aikuinen_rowid) throws SQLException {
		i = setResultsToTable(rs, row.getAikuiset().get(aikuinen_index), i);
		addedAikuinen.add(aikuinen_rowid);
		aikuinen_index++;
		return i;
	}
	
	private int poikanenRow(_ResultSet rs, int i, String poikanen_rowid) throws SQLException {
		i = setResultsToTable(rs, row.getPoikaset().get(poikanen_index), i);
		addedPoikanen.add(poikanen_rowid);
		poikanen_index++;
		return i;
	}
	
	private int munaRow(_ResultSet rs, int i, String muna_rowid) throws SQLException {
		i = setResultsToTable(rs, row.getMunat().get(muna_index), i);
		addedMuna.add(muna_rowid);
		muna_index++;
		return i;
	}
}
