package fi.hy.pese.framework.main.app.functionality.validate;

import java.sql.SQLException;

import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.general.Utils;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.luomus.commons.tipuapi.TipuAPIClient;
import fi.luomus.commons.utils.CoordinateConverter;
import fi.luomus.commons.utils.CoordinatesInsideMunicipalityUtil;
import fi.luomus.commons.xml.Document.Node;

public class TarkastusValidator extends Validator<_PesaDomainModelRow> {
	
	public TarkastusValidator(_Request<_PesaDomainModelRow> request) {
		super(request);
	}

	@Override
	public void validate() throws Exception {
		if (data.action().equals(Const.FILL_FOR_INSERT) || data.action().equals(Const.FILL_FOR_UPDATE)) {
			checkPesaidOrTarkastusIdGivenAndExists();
		}
		if (data.action().equals(Const.INSERT_FIRST) || data.action().equals(Const.INSERT) || data.action().equals(Const.UPDATE)) {
			if (data.updateparameters().getLomakeVastaanottajat().isInitialized()) {
				validateLomakeVastaanottajat();
			}
		}
	}
	
	private void validateLomakeVastaanottajat() {
		for (Object o : data.getLists().get("lomake_vastaanottajat")) {
			String vastaanottaja = o.toString();
			if (vastaanottaja.length() == 0) continue;
			Integer rengastajaNro = Utils.onlyNumbers(vastaanottaja);
			if (rengastajaNro == null) {
				error("lomake_vastaanottajat", _Validator.INVALID_VALUE);
			} else {
				if (!data.selections().get(TipuAPIClient.RINGERS).containsOption(Integer.toString(rengastajaNro))) {
					error("lomake_vastaanottajat", _Validator.INVALID_VALUE);
				}
			}
		}
	}
	
	private void checkPesaidOrTarkastusIdGivenAndExists() throws SQLException {
		_Column pesaid = data.updateparameters().getPesa().getUniqueNumericIdColumn();
		_Column tarkastusid = data.updateparameters().getTarkastus().getUniqueNumericIdColumn();
		checkInteger(pesaid);
		checkInteger(tarkastusid);
		if (hasErrors()) {
			return;
		}

		boolean exists = false;
		if (pesaid.hasValue() && !tarkastusid.hasValue()) {
			exists = dao.checkRowExists(data.updateparameters().getPesa());
			if (!exists) {
				error(pesaid, _Validator.DOES_NOT_EXIST);
			}
		} else if (tarkastusid.hasValue()) {
			exists = dao.checkRowExists(data.updateparameters().getTarkastus());
			if (!exists) {
				error(tarkastusid, _Validator.DOES_NOT_EXIST);
			}
		} else {
			error(pesaid, _Validator.DOES_NOT_EXIST);
		}
	}

	protected void checkInsideReagion(String kulyh, _Column leveys, _Column pituus, CoordinateConverter converter) throws Exception {
		converter.convertToDecimalsOnly();
		Double pesa_des_lev = converter.getDes_leveys();
		Double pesa_des_pit = converter.getDes_pituus();

		Node kunta = request.getTipuApiResource(TipuAPIClient.MUNICIPALITIES).getById(kulyh);
		Double kuntaDesPit = Double.valueOf(kunta.getAttribute("centerpoint-lon"));
		Double kuntaDesLev = Double.valueOf(kunta.getAttribute("centerpoint-lat"));
		Double kuntaSade = Double.valueOf(kunta.getAttribute("radius"));

		if (!CoordinatesInsideMunicipalityUtil.insideMunicipality(kuntaDesLev, kuntaDesPit, kuntaSade, pesa_des_lev, pesa_des_pit)) {
			error(leveys, COORDINATES_NOT_INSIDE_REGION);
			error(pituus, COORDINATES_NOT_INSIDE_REGION);
		}
	}

	
	protected void copyAndRemoveError(String fieldname, String kirjekyyhkyFieldname) {
		if (hasErrors(fieldname)) {
			error(kirjekyyhkyFieldname, errors.get(fieldname));
			removeError(fieldname);
		}
	}
}
