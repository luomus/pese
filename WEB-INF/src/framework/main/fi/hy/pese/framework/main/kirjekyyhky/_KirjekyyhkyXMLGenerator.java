package fi.hy.pese.framework.main.kirjekyyhky;

import java.io.File;
import java.util.List;
import java.util.Map;

public interface _KirjekyyhkyXMLGenerator {

	File writeFormDataXML(Map<String, String> data, String filename) throws Exception;

	List<String> producedFormDataXMLFiles();

}
