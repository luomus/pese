package fi.hy.pese.framework.main.db;

import java.sql.SQLException;

import fi.hy.pese.framework.main.general.data._Table;

/**
 * Adds one result row from resultset to data results-leaf, either using all tables, or a single table.
 * Throws an expection if resultset does not contain any rows
 */
public class FirstResultRowToTableHandler extends BaseResultsHandler {

	public final static boolean	CHECK_ONLY_ONE_RESULT	= true;

	private final _Table		table;
	private final boolean		checkOnlyOneResult;

	public FirstResultRowToTableHandler(_Table destinationTable) {
		this.table = destinationTable;
		this.checkOnlyOneResult = false;
	}

	public FirstResultRowToTableHandler(_Table destinationTable, boolean checkOnlyOneResult) {
		this.table = destinationTable;
		this.checkOnlyOneResult = checkOnlyOneResult;
	}

	/**
	 * Adds results to the table that was given in the constructor
	 * @throws SQLException if resultset contains only one row
	 */
	@Override
	public void process(_ResultSet rs) throws SQLException {
		if (!rs.next()) throw new NoResultsException("Resultset does not contain any results");
		setResultsToTable(rs, table);
		if (checkOnlyOneResult) {
			if (rs.next()) throw new TooManyResultsException("Resultset had more than just one result");
		}
	}

	public static class NoResultsException extends SQLException {
		private static final long	serialVersionUID	= -1419974277973718996L;
		public NoResultsException(String message) {
			super(message);
		}
	}

	public static class TooManyResultsException extends SQLException {
		private static final long	serialVersionUID	= -7280385909763452666L;
		public TooManyResultsException(String message) {
			super(message);
		}
	}

}
