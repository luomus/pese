package fi.hy.pese.framework.main.app;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import fi.hy.pese.framework.main.db._DAO;
import fi.hy.pese.framework.main.db.oraclesql.OracleSQL_DAO;
import fi.hy.pese.framework.main.general.Logger;
import fi.hy.pese.framework.main.general._Logger;
import fi.hy.pese.framework.main.general._WarehouseRowGenerator;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.consts.Kirjekyyhky;
import fi.hy.pese.framework.main.general.data.Data;
import fi.hy.pese.framework.main.general.data.HistoryAction;
import fi.hy.pese.framework.main.general.data.ParameterMap;
import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._Data;
import fi.hy.pese.framework.main.general.data._IndexedTablegroup;
import fi.hy.pese.framework.main.general.data._Row;
import fi.hy.pese.framework.main.general.data._RowFactory;
import fi.hy.pese.framework.main.kirjekyyhky._KirjekyyhkyXMLGenerator;
import fi.hy.pese.framework.main.view.FreemarkerTemplateViewer;
import fi.hy.pese.framework.main.view.FunnyOwlExceptionViewer;
import fi.hy.pese.framework.main.view._ExceptionViewer;
import fi.hy.pese.framework.main.view._Viewer;
import fi.luomus.commons.config.Config;
import fi.luomus.commons.config.ConfigReader;
import fi.luomus.commons.containers.Selection;
import fi.luomus.commons.db.connectivity.PreparedStatementStoringAndClosingTransactionConnection;
import fi.luomus.commons.db.connectivity.TransactionConnection;
import fi.luomus.commons.kirjekyyhky.KirjekyyhkyAPIClient;
import fi.luomus.commons.kirjekyyhky.KirjekyyhkyAPIClientImple;
import fi.luomus.commons.languagesupport.LanguageFileReader;
import fi.luomus.commons.languagesupport.LocalizedTextsContainer;
import fi.luomus.commons.pdf.ITextPDFWriter;
import fi.luomus.commons.pdf.PDFWriter;
import fi.luomus.commons.reporting.ErrorReporter;
import fi.luomus.commons.reporting.ErrorReporterViaEmail;
import fi.luomus.commons.reporting.ErrorReportingToSystemErr;
import fi.luomus.commons.tipuapi.TipuAPIClient;
import fi.luomus.commons.tipuapi.TipuAPIClientImple;
import fi.luomus.commons.tipuapi.TipuApiResource;
import fi.luomus.commons.utils.Cached;
import fi.luomus.commons.utils.Cached.CacheLoader;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.FileUtils;
import fi.luomus.commons.utils.LogUtils;
import fi.luomus.commons.utils.SingleObjectCache;

public abstract class BaseFactoryForApplicationSpecificationFactory<T extends _Row> implements _ApplicationSpecificationFactory<T> {

	private static final int			TIPU_API_CACHE_TIMER	= 1 * 60;
	protected static final int			SECONDS_BETWEEN_KK_COUNT_LOADS	= 60;
	private static final int			NUMBER_OF_HISTORY_ACTION_ROWS	= 10;
	private static final int			INTERVAL_BETWEEN_SELECTIONS_FETCH_IN_MINUTES	= 15;
	private static final String			DEFAULT_DATABASE_LOG_FILE_PREFIX	= "database_log_";
	private static final String			DEFAULT_EXCEPTION_LOG_FILE_PREFIX	= "EXCEPTION_";

	private Config						config;
	protected LocalizedTextsContainer		uiTexts;
	protected Map<String, Selection>	selections;
	protected long						lastFetched							= 0;
	protected _RowFactory<T>			rowFactory;
	private final SingleObjectCache<String> kirjekyyhkyCountCache;
	private final Cached<String, TipuApiResource> tipuApiCachedResources;

	public BaseFactoryForApplicationSpecificationFactory() throws FileNotFoundException {
		this(null);
	}

	public BaseFactoryForApplicationSpecificationFactory(String configFileName) throws FileNotFoundException {
		if (configFileName != null) {
			String catalinaBase = System.getProperty("catalina.base");
			if (catalinaBase != null) configFileName = catalinaBase + File.separator + "app-conf" + File.separator + configFileName;
			config = new ConfigReader(configFileName);
		}
		selections = null;

		kirjekyyhkyCountCache = new SingleObjectCache<>(new SingleObjectCache.CacheLoader<String>() {
			@Override
			public String load() {
				String count = null;
				KirjekyyhkyAPIClient api = null;
				try {
					api = kirjekyyhkyAPI();
					count = Integer.toString( api.getCount() ) ;
				} catch (Exception e) {
					throw new RuntimeException(e);
				} finally {
					if (api != null) api.close();
				}
				return count;
			}}, SECONDS_BETWEEN_KK_COUNT_LOADS, TimeUnit.SECONDS);

		tipuApiCachedResources = new Cached<>(new CacheLoader<String, TipuApiResource>() {
			@Override
			public TipuApiResource load(String ressourcename) {
				TipuAPIClient api = null;
				try {
					api = tipuAPI();
					return api.get(ressourcename);
				} catch (Exception e) {
					throw new RuntimeException(e);
				} finally {
					if (api != null) api.close();
				}
			}}, TIPU_API_CACHE_TIMER, TimeUnit.SECONDS, 50);
	}

	public void setRowFactory(_RowFactory<T> rowFactory) {
		this.rowFactory = rowFactory;
	}

	// Can be overriden to do more stuff
	@Override
	public _Data<T> initData(String language, ParameterMap params) throws Exception {
		Data<T> data = new Data<>(rowFactory);
		params.convertTo(data);
		data.setBaseURL(config.baseURL());
		data.setCommonURL(config.commonURL());
		reloadUITexts();
		data.setUITexts(uiTexts().getAllTexts(language));
		data.setDevelopment(config.developmentMode());
		data.setStaging(config.stagingMode());
		return data;
	}

	// Should to be overriden in the final implementation
	@Override
	public abstract _Functionality<T> functionalityFor(String page, _Request<T> request) throws UnsupportedOperationException;

	// Should be overriden in the final implementation (or not used at all; selections directly via the public selections() -method )
	protected abstract Map<String, Selection> fetchSelections(_DAO<T> dao) throws Exception;

	// Can be overriden to use another implementation. This implementation doesn't fetch selections from
	// the database all the time, only if there are changes, and then every 15 minutes
	@Override
	public Map<String, Selection> selections(_DAO<T> dao) throws Exception {
		if (!selectionsShouldBeFetched(dao)) return this.selections;
		this.selections = fetchSelections(dao);
		return selections;
	}

	private boolean selectionsShouldBeFetched(_DAO<T> dao) throws SQLException {
		if (lastFetched < dao.returnSelectionsUpdatedTime()) {
			lastFetched = dao.returnSelectionsUpdatedTime();
			return true;
		}
		if (lastFetchedOverFifteenminutesAgo()) {
			lastFetched = DateUtils.getCurrentEpoch();
			return true;
		}
		return false;
	}

	private boolean lastFetchedOverFifteenminutesAgo() {
		return DateUtils.getCurrentEpoch() - lastFetched > INTERVAL_BETWEEN_SELECTIONS_FETCH_IN_MINUTES*60;
	}

	@Override
	public TipuApiResource getTipuApiResource(String resourcename) {
		return this.tipuApiCachedResources.get(resourcename);
	}

	// Should be overriden in the final implementation
	@Override
	public String frontpage() {
		return Const.LOGIN_PAGE;
	}

	// Should be overriden by using application
	@Override
	public _KirjekyyhkyXMLGenerator kirjekyyhkyFormDataXMLGenerator(_Request<T> request)  throws Exception {
		return null;
	}

	// Can be overriden by using application
	@Override
	public KirjekyyhkyAPIClient kirjekyyhkyAPI() throws Exception {
		return new KirjekyyhkyAPIClientImple(
				config.get(Config.KIRJEKYYHKY_API_URI),
				config.get(Config.KIRJEKYYHKY_API_USERNAME),
				config.get(Config.KIRJEKYYHKY_API_PASSWORD),
				config.systemId());
	}

	@Override
	public TipuAPIClient tipuAPI() throws Exception {
		return new TipuAPIClientImple(
				config.get(Config.TIPU_API_URI),
				config.get(Config.TIPU_API_USERNAME),
				config.get(Config.TIPU_API_PASSWORD));
	}

	// Can be overriden with another implementation (especially when building factory for tests)
	@Override
	public _DAO<T> dao(String userId) throws SQLException {
		TransactionConnection con = new PreparedStatementStoringAndClosingTransactionConnection(config().connectionDescription());
		String logFilePrefix = DEFAULT_DATABASE_LOG_FILE_PREFIX;
		if (config.defines(Config.DATABASE_LOG_FILE_PREFIX)) {
			logFilePrefix = config.get(Config.DATABASE_LOG_FILE_PREFIX);
		}
		_Logger logger = new Logger(userId, config().logFolder(), logFilePrefix);
		return new OracleSQL_DAO<>(con, rowFactory, logger);
	}

	@Override
	public void release(_DAO<T> dao) {
		((OracleSQL_DAO<T>) dao).close();
	}

	// Can be overriden to use another implementation than DefaultExceptionHandler
	@Override
	public _ExceptionViewer exceptionViewer(_Request<T> request) {
		if (request == null) return new _ExceptionViewer() {
			@Override
			public void view(Throwable condition) {
			}
		};
		return new FunnyOwlExceptionViewer(request.out());
	}

	// Can be overriden to use another implementation than FreeMarker
	@Override
	public _Viewer viewer() throws IOException {
		return new FreemarkerTemplateViewer(config.templateFolder());
	}

	// Can be overriden to use another implementation than LanguageFileReader
	@Override
	public LocalizedTextsContainer uiTexts() {
		return uiTexts;
	}

	// If not in development mode this does nothing
	@Override
	public void reloadUITexts() throws Exception {
		if (config.developmentMode()) {
			loadUITexts();
		}
	}

	public void loadUITexts() throws Exception {
		String folder = config.baseFolder() + config.get(Config.LANGUAGE_FILE_FOLDER);
		String prefix = config.get(Config.LANGUAGE_FILE_PREFIX);
		String[] languages = config.get(Config.SUPPORTED_LANGUAGES).split(",");
		LanguageFileReader r = new LanguageFileReader(folder, prefix, languages);
		uiTexts = r.readUITexts();

		if (this.rowFactory == null) return;
		T row = this.rowFactory.newRow();

		for (String language : languages) {
			Map<String, String> texts = uiTexts.getAllTexts(language);
			for (_IndexedTablegroup g : row.getGroups().values()) {
				addTextsForIndexedTablegroup(g, texts);
			}
		}
	}

	protected static void addTextsForIndexedTablegroup(_IndexedTablegroup group, Map<String, String> uiTexts) {
		for (_Column c : group.getBasetable()) {
			String text = uiTexts.get("iter."+c.getFullname());
			if (text == null) continue;
			for (int i = 1; i <= group.getStaticCount(); i++) {
				String iText = text.replace("<X>", Integer.toString(i));
				uiTexts.put(group.getBasetable().getName()+"."+i+"."+c.getName(), iText);
			}
		}
	}

	// Can be overriden to use another implementation than ConfigReader
	@Override
	public Config config() {
		return config;
	}

	// Can be overriden to use another implementation
	@Override
	public void reportException(String originalRequestURI, Throwable condition) {
		try { writeToLog(originalRequestURI, condition); } catch (Exception e) { e.printStackTrace(); }
		try { report(originalRequestURI, condition); } catch (Exception e) { e.printStackTrace(); }
	}

	private void writeToLog(String requestURI, Throwable condition) {
		LogUtils.write(requestURI, condition, config.logFolder(), DEFAULT_EXCEPTION_LOG_FILE_PREFIX);
	}

	protected void report(String requestURI, Throwable condition) {
		getErrorReporter().report(requestURI, condition);
	}

	private ErrorReporter	errorReporter	= null;

	// Can be overriden to use another implementation
	@Override
	public ErrorReporter getErrorReporter() {
		if (errorReporter == null) {
			if (config().developmentMode()) {
				errorReporter = new ErrorReportingToSystemErr();
			} else {
				try {
					Config config = config();
					errorReporter = new ErrorReporterViaEmail(config);
				} catch (Exception creatingErrorReporterException) {
					creatingErrorReporterException.printStackTrace();
				}
			}
		}
		return errorReporter;
	}

	// Can be overriden to use another implementation
	@Override
	public PDFWriter pdfWriter() {
		if (config != null) return createPdfWriter(config().pdfFolder());
		return null;
	}
	
	private PDFWriter createPdfWriter(String outputfolder) {
		if (config.defines(Config.FONT_FOLDER)) {
			return new ITextPDFWriter(config.templateFolder(), outputfolder, config.fontFolder());
		}
		return new ITextPDFWriter(config().templateFolder(), outputfolder);
	}

	// Can be overriden to use another implementation
	@Override
	public PDFWriter pdfWriter(String outputFolder) {
		if (config != null) return  createPdfWriter(outputFolder);
		return null;
	}

	@Override
	public boolean isWarehouseSource() {
		return config.defines(Const.ETL_URI);
	}

	@Override
	public _WarehouseRowGenerator getWarehouseRowGenerator(_DAO<T> dao) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean usingKirjekyyhky() {
		return config.defines(Config.KIRJEKYYHKY_API_URI);
	}

	@Override
	public boolean usingTipuApi() {
		return config.defines(Config.TIPU_API_URI);
	}



	@Override
	public void loadKirjekyyhkyCount(_Request<T> request) {
		try {
			if (request.data().page().equals(Const.TARKASTUS_PAGE) && request.data().successText().length() > 1) {
				request.data().set(Kirjekyyhky.KIRJEKYYHKY_COUNT, kirjekyyhkyCountCache.getForceReload());
			} else if (request.data().page().equals(Const.KIRJEKYYHKY_MANAGEMENT_PAGE) && request.data().action().equals(Kirjekyyhky.RETURN_FOR_CORRECTIONS)) {
				request.data().set(Kirjekyyhky.KIRJEKYYHKY_COUNT, kirjekyyhkyCountCache.getForceReload());
			}
			else{
				request.data().set(Kirjekyyhky.KIRJEKYYHKY_COUNT, kirjekyyhkyCountCache.get());
			}
		} catch (Exception e) {
			request.data().set(Kirjekyyhky.KIRJEKYYHKY_COUNT, "ERR");
		}
	}

	private List<HistoryAction> historyActions = new LinkedList<>();

	@Override
	public void addHistoryAction(HistoryAction action) {
		if (historyActions.size() >= NUMBER_OF_HISTORY_ACTION_ROWS) {
			historyActions.remove(historyActions.size()-1);
		}
		removeEntriesWithSameId(action);
		historyActions.add(0, action);
	}

	private void removeEntriesWithSameId(HistoryAction action) {
		String id = action.getId();
		Iterator<HistoryAction> i = historyActions.iterator();
		while (i.hasNext()) {
			HistoryAction a = i.next();
			if (a.getId().equals(id)) {
				i.remove();
			}
		}
	}

	@Override
	public List<HistoryAction> getHistoryActions() {
		return historyActions;
	}

	@Override
	public void saveHistoryActions() {
		FileOutputStream out = null;
		ObjectOutputStream oout = null;
		try {
			out = new FileOutputStream(historyActionsFile());
			oout = new ObjectOutputStream(out);
			oout.writeObject(historyActions);
		} catch (Exception e) {
			reportException("Saving history actions: " +historyActions.toString(), e);
		} finally {
			FileUtils.close(out, oout);
		}
	}


	protected File historyActionsFile() {
		return new File(config.baseFolder(), config.systemId() + "_history-actions.bin");
	}

	@Override
	public void loadHistoryActions() {
		FileInputStream in = null;
		ObjectInputStream oin = null;
		try {
			File file = historyActionsFile();
			if (file.exists()) {
				List<HistoryAction> historyActions = new LinkedList<>();
				in = new FileInputStream(file);
				oin = new ObjectInputStream(in);
				Object o = oin.readObject();
				if (o instanceof List<?>) {
					List<?> list = (List<?> ) o;
					Iterator<?> iterator = list.iterator();
					while (iterator.hasNext()) {
						o = iterator.next();
						if (o instanceof HistoryAction) {
							historyActions.add((HistoryAction) o);
						}
					}
				}
				this.historyActions = historyActions;
			}
		} catch (Exception e) {
			reportException("Loading history actions", e);
		} finally {
			FileUtils.close(in);
			FileUtils.close(oin);
		}
	}

	@Override
	public void postProsessingHook(_Request<T> request) {
		if (this.usingKirjekyyhky()) {
			this.loadKirjekyyhkyCount(request);
		}
	}

	public void setUiTexts(LocalizedTextsContainer uiTexts) {
		this.uiTexts = uiTexts;
	}

	protected String getLintuvaaraURL() {
		return config().get(Config.LINTUVAARA_URL);
	}

}
