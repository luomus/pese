package fi.hy.pese.framework.main.general.data;

public class RowFactory implements _RowFactory<_Row> {

	private final String joins;
	private final String orderby;
	private final PesaDomainModelDatabaseStructure	rowStructure;

	public RowFactory(PesaDomainModelDatabaseStructure rowStructure, String joins, String orderby) {
		this.joins = joins;
		this.orderby = orderby;
		this.rowStructure = rowStructure;
	}

	@Override
	public String joins() {
		return joins;
	}

	@Override
	public String orderby() {
		return orderby;
	}

	@Override
	public _Row newRow() {
		return new Row(rowStructure);
	}

	@Override
	public _Row newRow(String prefix) {
		return new Row(rowStructure, prefix);
	}

}
