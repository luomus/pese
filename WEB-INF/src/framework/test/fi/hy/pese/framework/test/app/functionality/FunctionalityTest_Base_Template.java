package fi.hy.pese.framework.test.app.functionality;

import java.io.FileNotFoundException;
import java.util.Map;

import fi.hy.pese.framework.main.app.BaseFactoryForApplicationSpecificationFactory;
import fi.hy.pese.framework.main.app.Request;
import fi.hy.pese.framework.main.app.RequestImpleData;
import fi.hy.pese.framework.main.app._Functionality;
import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.db._DAO;
import fi.hy.pese.framework.main.db.oraclesql.OracleSQL_DAO;
import fi.hy.pese.framework.main.general.data.Data;
import fi.hy.pese.framework.main.general.data._Data;
import fi.hy.pese.framework.main.general.data._Row;
import fi.hy.pese.framework.test.RowFactory;
import fi.hy.pese.framework.test.TestConfig;
import fi.luomus.commons.config.Config;
import fi.luomus.commons.containers.Selection;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class FunctionalityTest_Base_Template {
	
	public static Test suite() {
		return new TestSuite(FunctionalityTest_Base_Template.class.getDeclaredClasses());
	}
	
	public static class Base_TestCase extends TestCase {
		
		private class Base_TestFactory extends BaseFactoryForApplicationSpecificationFactory<_Row> {
			public Base_TestFactory() throws FileNotFoundException {
				super();
			}
			
			@Override
			public _Functionality<_Row> functionalityFor(String page, _Request<_Row> request) {
				// return new _____Functionality(); <- functionality to be tested goes here
				return null;
			}
			
			@Override
			public _DAO<_Row> dao(String userId) {
				return new Base_FakeDAO();
			}
			
			@Override
			public Config config() {
				try {
					return TestConfig.getConfig();
				} catch (Exception e) {
					fail();
					e.printStackTrace();
				}
				return null;
			}
			
			@Override
			protected Map<String, Selection> fetchSelections(_DAO<_Row> dao) throws Exception {
				return null;
			}
		}
		
		private class Base_FakeDAO extends OracleSQL_DAO<_Row> {
			
			public Base_FakeDAO() {
				super(null, null);
			}
			
		}
		
		private _Data<_Row>		data;
		private _Request<_Row>	request;
		
		@Override
		protected void setUp() throws Exception {
			data = new Data<>(new RowFactory());
			Base_TestFactory factory = new Base_TestFactory();
			RequestImpleData<_Row> reqData = new RequestImpleData<>().setData(data).setDao(factory.dao("userid")).setApplication(factory);
			request = new Request<>(reqData);
			data.setPage("");
		}
		
		public void test____() {
			request.data();
		}
		
	}
	
}
