package fi.hy.pese.test.warehousetests;

import java.io.File;
import java.net.URL;

import fi.hy.pese.framework.main.db._DAO;
import fi.hy.pese.framework.main.general.WarehouseRowGenerator;
import fi.hy.pese.framework.main.general.WarehouseRowGenerator.WarehouseGenerationDefinition;
import fi.hy.pese.framework.main.general.data.PesaDomainModelDatabaseStructure;
import fi.hy.pese.framework.main.general.data.PesaDomainModelRowFactory;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.test.TestConfig;
import fi.hy.pese.framework.test.db.DAOStub;
import fi.hy.pese.saaksi.main.app.SaaksiDatabaseStructureCreator;
import fi.hy.pese.saaksi.main.app.SaaksiFactory;
import fi.hy.pese.saaksi.main.app.SaaksiWarehouseDefinition;
import fi.laji.datawarehouse.etl.models.dw.DwRoot;
import fi.laji.datawarehouse.etl.models.dw.Gathering;
import fi.luomus.commons.db.connectivity.SimpleTransactionConnection;
import fi.luomus.commons.db.connectivity.TransactionConnection;
import fi.luomus.commons.utils.FileUtils;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class SaaksiWarehouseTests {
	
	public static Test suite() {
		return new TestSuite(SaaksiWarehouseTests.class.getDeclaredClasses());
	}
	
	public static class WhenGeneratingWarehouseDocument extends TestCase {
		
		public class FakeDAO<T extends _PesaDomainModelRow> extends DAOStub<T> {
			public FakeDAO() {}
			
		}
		
		private SaaksiFactory application;
		private _DAO<_PesaDomainModelRow> dao;
		private WarehouseRowGenerator generator;
		private _PesaDomainModelRow row;
		
		@Override
		protected void setUp() throws Exception {
			application = new SaaksiFactory(TestConfig.getConfigFile("pese_saaksi.config"));
			
			try (TransactionConnection con = new SimpleTransactionConnection(application.config().connectionDescription())) {
				PesaDomainModelDatabaseStructure structure = SaaksiDatabaseStructureCreator.loadFromDatabase(con);
				application.setRowFactory(new PesaDomainModelRowFactory(structure, null, null));
			}
			application.loadUITexts();
			dao = application.dao(SaaksiWarehouseTests.class.getSimpleName());
			WarehouseGenerationDefinition definition = SaaksiWarehouseDefinition.instance();
			generator = new WarehouseRowGenerator(definition, dao, application);
			row = dao.newRow();
		}
		
		@Override
		protected void tearDown() throws Exception {
			if (application != null) if (dao != null) application.release(dao);
		}
		
		public void test__delete() throws Exception {
			DwRoot root = generator.deletedEntry(123);
			assertRoot("saaksi_1_delete.json", root);
		}
		
		public void test__not_inspected() throws Exception {
			row.getPesa().getUniqueNumericIdColumn().setValue(1);
			row.getTarkastus().getUniqueNumericIdColumn().setValue(100);
			row.getTarkastus().get("vuosi").setValue(2020);
			row.getTarkastus().get("vaihtop_laiminl").setValue("J");
			DwRoot root = generator.generate(row);
			assertRoot("saaksi_2_not_inspected.json", root);
		}
		
		public void test__common() throws Exception {
			commonTestData();
			DwRoot root = generator.generate(row);
			assertRoot("saaksi_3_common.json", root);
		}
		
		private void commonTestData() {
			row.getPesa().getUniqueNumericIdColumn().setValue(1);
			row.getTarkastus().getUniqueNumericIdColumn().setValue(100);
			row.getTarkastus().get("vuosi").setValue(2022);

			row.getColumn("pesa.vanha_pesaid").setValue("FOOBAR");
			row.getPesa().get("kunta").setValue("VÅRDÖ");
			row.getPesa().get("kyla").setValue("MICKELSÖ");
			row.getPesa().get("eur_leveys").setValue(6690000);
			row.getPesa().get("eur_pituus").setValue(128000);
			row.getPesa().get("koord_tyyppi").setValue("88");
			row.getColumn("pesa.koord_mittaustapa").setValue("L");
			row.getPesa().get("kommentti").setValue("Should not sent");
			row.getColumn("olosuhde.pesan_sijainti").setValue("P");
			row.getColumn("olosuhde.pesap_elavyys").setValue("E");
			row.getOlosuhde().get("kommentti").setValue("Should not sent");

			row.getTarkastus().get("rengastaja").setValue(846);
			row.getPesa().get("seur_rengastaja").setValue(1146);
			
			row.getTarkastus().get("tark_pvm").setValue("13.8.2022");
			row.getTarkastus().get("tark_pvm_tark").setValue("1");

			row.getTarkastus().get("tarkkuus").setValue("5");
			row.getTarkastus().get("pesimistulos").setValue("L");
			row.getTarkastus().get("kunto").setValue("A");
			
			row.getTarkastus().get("nayte_munia").setValue("K");
		}

		public void test__multi_kaynti() throws Exception {
			commonTestData();

			row.getKaynnit().get(1).getUniqueNumericIdColumn().setValue(12346);
			row.getKaynnit().get(1).get("pvm").setValue("20.6.2020");
			row.getKaynnit().get(1).get("aikuisia").setValue("2");

			row.getKaynnit().get(2).getUniqueNumericIdColumn().setValue(12347);
			row.getKaynnit().get(2).get("pvm").setValue("25.6.2020");
			row.getKaynnit().get(2).get("munia").setValue("3");

			row.getKaynnit().get(3).getUniqueNumericIdColumn().setValue(12348);
			row.getKaynnit().get(3).get("pvm").setValue("13.8.2020");
			row.getKaynnit().get(3).get("lentopoik").setValue("2");

			DwRoot root = generator.generate(row);
			assertRoot("saaksi_4_multi_kaynti.json", root);
		}
		
		public void test__muulaji() throws Exception {
			commonTestData();
			row.getTarkastus().get("pesimistulos").setValue("X");
			row.getTarkastus().get("muulaji").setValue("BUCCLA");
			DwRoot root = generator.generate(row);
			assertRoot("saaksi_6_other_species.json", root);
		}
		
		public void test__date_accuracy() throws Exception {
			commonTestData();
			row.getTarkastus().get("tark_pvm_tark").setValue("2");
			DwRoot root = generator.generate(row);
			for (Gathering g : root.getPublicDocument().getGatherings()) {
				assertEquals("2022-08-01 - 2022-08-31", g.getDisplayDateTime());
			}
			
			row.getTarkastus().get("tark_pvm_tark").setValue("3");
			root = generator.generate(row);
			for (Gathering g : root.getPublicDocument().getGatherings()) {
				assertEquals("2022-01-01 - 2022-12-31", g.getDisplayDateTime());
			}
			
			row.getTarkastus().get("tark_pvm_tark").setValue("4");
			root = generator.generate(row);
			for (Gathering g : root.getPublicDocument().getGatherings()) {
				assertEquals("2022-01-01 - 2022-12-31", g.getDisplayDateTime());
			}
		}
		
		public void test__dead_nest() throws Exception {
			commonTestData();
			row.getColumn("tarkastus.epaonn").setValue("D");
			DwRoot root = generator.generate(row);
			assertRoot("saaksi_7_dead_nest.json", root);
		}

		public void test__no_name_public() throws Exception {
			commonTestData();

			row.getColumn("tarkastus.rengastaja").setValue("1146");
			DwRoot root = generator.generate(row);
			assertEquals("[Turku \"falco-Kerho\"]", root.getPrivateDocument().getGatherings().get(0).getTeam().toString());
			assertEquals("[Hidden]", root.getPublicDocument().getGatherings().get(0).getTeam().toString());
		}

		public void test__coordinate_uncertainty() throws Exception {
			commonTestData();
			
			row.getPesa().get("koord_tyyppi").setValue("91");
			
			DwRoot root = generator.generate(row);
			assertEquals("6690000.0 : 6690001.0 : 128000.0 : 128001.0 : EUREF : 1000", root.getPublicDocument().getGatherings().get(0).getCoordinates().toString());
		}

		private void assertRoot(String filename, DwRoot root) {
			URL url = this.getClass().getResource(filename);
			File file = new File(url.getFile());
			try {
				String data = FileUtils.readContents(file);
				assertEquals(data, root.toJSON().beautify());
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
		
	}
	
}
