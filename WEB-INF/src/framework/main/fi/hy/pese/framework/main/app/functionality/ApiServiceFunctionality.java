package fi.hy.pese.framework.main.app.functionality;

import java.io.PrintWriter;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.app.functionality.validate.SearchFunctionalityValidator;
import fi.hy.pese.framework.main.app.functionality.validate._Validator;
import fi.hy.pese.framework.main.db.RowsToDataResultHandler;
import fi.hy.pese.framework.main.db.RowsToData_Single_Table_ResultHandler;
import fi.hy.pese.framework.main.db._DAO;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data.Data;
import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._IndexedTablegroup;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.main.general.data._Row;
import fi.hy.pese.framework.main.general.data._Table;
import fi.hy.pese.rengastus.main.app._RengastusDomainModelRow;
import fi.luomus.commons.containers.Selection;
import fi.luomus.commons.utils.Utils;

public class ApiServiceFunctionality<T extends _Row> extends BaseFunctionality<T> {

	public ApiServiceFunctionality(_Request<T> request) {
		super(request);
	}

	@Override
	public void preProsessingHook() throws Exception {
		if (!data.searchparameters().hasValues()) {
			request.redirecter().status404();
		}
	}

	@Override
	public _Validator validator() throws Exception {
		return new SearchFunctionalityValidator<>(request);
	}

	@Override
	public void afterSuccessfulValidation() throws Exception {
		if (data.searchparameters() instanceof _PesaDomainModelRow) {
			@SuppressWarnings("unchecked")
			Data<_PesaDomainModelRow> dataM = (Data<_PesaDomainModelRow>) data;
			@SuppressWarnings("unchecked")
			_DAO<_PesaDomainModelRow> daoM = (_DAO<_PesaDomainModelRow>) dao;
			RowsToDataResultHandler handler = new RowsToDataResultHandler(dataM, daoM);
			daoM.executeSearch(dataM.searchparameters(), handler);
		} else if (data.searchparameters() instanceof _RengastusDomainModelRow) {
			RowsToData_Single_Table_ResultHandler<T> handler = new RowsToData_Single_Table_ResultHandler<>(data, "kontrolli", 50000);
			dao.executeSearch(data.searchparameters(), handler);
		} else {
			throw new UnsupportedOperationException("Not implemented for type " + data.searchparameters().getClass());
		}

		// fi.hy.pese.framework.main.general.Utils.debug(data.searchparameters());

		// Document document = new Document("results");
		Map<String, String> texts = request.data().uiTexts();
		Set<String> resultFields = resultFields(iValue("startAt"), iValue("limit"));

		PrintWriter out = request.out();
		if (data.get("format").equals("csv")) {
			printCSV(texts, resultFields, out);
		} else if (data.get("format").equals("json")) {
			printJSON(texts, resultFields, out, iValue("startAt"), iValue("limit"));
		} else {
			printXML(texts, resultFields, out, iValue("startAt"), iValue("limit"));
		}
	}

	private Integer iValue(String key) {
		if (!data.contains(key)) return 0;
		try {
			return Integer.valueOf(data.get(key));
		} catch (NumberFormatException e) {}
		return -1;
	}

	private void printXML(Map<String, String> texts, Set<String> resultFields, PrintWriter out, int startAt, int limit) {
		if (data.get("resulttype").equals("count")) {
			out.append("<results totalCount=\""+data.results().size()+"\" />");
			return;
		}
		out.append("<results totalCount=\""+data.results().size()+"\">");
		int count = -1;
		for (_Row r : data.results()) {
			count++;
			if (startAt != -1 && count < startAt) continue;
			if (startAt != -1 && limit != 0 && count >= startAt + limit) break;
			String rowID = "";
			if (r instanceof _PesaDomainModelRow) {
				_PesaDomainModelRow domainModelRow = (_PesaDomainModelRow) r;
				rowID = domainModelRow.getTarkastus().getUniqueNumericIdColumn().getValue();
			} else if (r instanceof _RengastusDomainModelRow) {
				_RengastusDomainModelRow domainModelRow = (_RengastusDomainModelRow) r;
				rowID = domainModelRow.getKontrolli().get("nimirengas").getValue();
			} else {
				throw new UnsupportedOperationException(" Not supported for " + r.toString());
			}
			out.append("<row id=\""+rowID+"\">");
			for (_Table t : r) {
				for (_Column c : t) {
					if (!resultFields.contains(c.getFullname())) continue;
					String fieldName = c.getFullname();
					if (texts.containsKey(c.getFullname())) {
						fieldName = texts.get(c.getFullname());
					}
					String value = c.getValue();
					Selection selection = data.selections().get(c.getAssosiatedSelection());
					if (selection != null && selection.containsOption(value)) {
						value = value + " - " + selection.get(value);
					}
					out.append("<c f=\""+fieldName+"\" v=\"" + escape(value) + "\" />");
				}
			}
			out.append("</row>");
		}
		out.append("</results>");
	}

	private String escape(String contents) {
		return Utils.toHTMLEntities(contents).replace("&#39;", "'"); // amp is encoded needesly
	}

	private void printJSON(Map<String, String> texts, Set<String> resultFields, PrintWriter out, int startAt, int limit) {
		if (data.get("resulttype").equals("count")) {
			out.append("{ response: { \"totalCount\": "+data.results().size()+" , \"results\": {} } } ");
			return;
		}
		out.append("{ response: { \"totalCount\": "+data.results().size()+" , \"results\": { ");
		int count = -1;
		for (_Row r : data.results()) {
			count++;
			if (startAt != -1 && count < startAt) continue;
			if (startAt != -1 && limit != 0 && count >= startAt + limit) break;
			String rowID = "";
			if (r instanceof _PesaDomainModelRow) {
				_PesaDomainModelRow domainModelRow = (_PesaDomainModelRow) r;
				rowID = domainModelRow.getTarkastus().getUniqueNumericIdColumn().getValue();
			} else if (r instanceof _RengastusDomainModelRow) {
				_RengastusDomainModelRow domainModelRow = (_RengastusDomainModelRow) r;
				rowID = domainModelRow.getKontrolli().get("nimirengas").getValue();
			} else {
				throw new UnsupportedOperationException(" Not supported for " + r.toString());
			}
			out.append(" "+rowID+": { ");
			for (_Table t : r) {
				for (_Column c : t) {
					if (!resultFields.contains(c.getFullname())) continue;
					String fieldName = c.getFullname();
					if (texts.containsKey(c.getFullname())) {
						fieldName = texts.get(c.getFullname());
					}
					String value = c.getValue();
					Selection selection = data.selections().get(c.getAssosiatedSelection());
					if (selection != null && selection.containsOption(value)) {
						value = value + " - " + selection.get(value);
					}
					out.append(" \""+fieldName+"\": \""+value.replace("\"", "")+ "\" , ");
				}
			}
			out.append(" \"endtag\": {} } , ");
		}
		out.append(" \"endtag\": {} } } } ");
	}

	private void printCSV(Map<String, String> texts, Set<String> resultFields, PrintWriter out) {
		List<String> fieldTitles = new LinkedList<>();
		for (String field : resultFields) {
			if (texts.containsKey(field)) {
				fieldTitles.add(texts.get(field));
			} else {
				fieldTitles.add(field);
			}
		}
		out.write(Utils.toCSV(fieldTitles) + "\n");
		for (_Row r : data.results()) {
			List<String> entries = new LinkedList<>();
			for (_Table t : r) {
				for (_Column c : t) {
					if (!resultFields.contains(c.getFullname())) continue;
					entries.add(c.getValue().replace("\"", "").replace("\r", "").replace("\n", ""));
				}
			}
			out.append(Utils.toCSV(entries) + "\n");
		}
	}

	private Set<String> resultFields(int startAt, int limit) {
		Set<String> resultFields = new LinkedHashSet<>();

		Set<String> givenResultfields = givenResultFields();
		Set<String> tablesThatHaveValues = tablesThatHaveValues();

		int count = -1;
		for (_Row row : data.results()) {
			count++;
			if (count < startAt) continue;
			if (limit != 0 && count >= startAt + limit) break;
			for (_Table t : row) {
				if (!tablesThatHaveValues.contains(t.getFullname())) continue;
				if (row.getGroups().keySet().contains(t.getName())) continue;
				for (_Column c : t) {
					if (!givenResultfields.isEmpty() && !givenResultfields.contains(c.getFullname())) continue;
					if (metadataField(c)) continue;
					resultFields.add(c.getFullname());
				}
			}
			for (_IndexedTablegroup group : row.getGroups().values()) {
				if (group.getStaticCount() < 1) continue;
				for (int i = 1; i <= group.getStaticCount(); i++) {
					_Table t = group.get(i);
					if (!tablesThatHaveValues.contains(t.getFullname())) continue;
					for (_Column c : t) {
						if (!givenResultfields.isEmpty() && !givenResultfields.contains(group.getBasetablename()+"."+c.getName())) continue;
						if (metadataField(c)) continue;
						resultFields.add(c.getFullname());
					}
				}
			}
		}
		return resultFields;
	}

	private Set<String> givenResultFields() {
		Set<String> givenResultfields = new HashSet<>();
		if (data.contains("resultfields")) {
			for (String field : data.get("resultfields").split(",")) {
				if (field.trim().length() > 0) {
					givenResultfields.add(field.trim().toLowerCase());
				}
			}
		}
		return givenResultfields;
	}

	private Set<String> tablesThatHaveValues() {
		Set<String> tablesThatHaveValues = new HashSet<>();
		for (_Row row : data.results()) {
			for (_Table t : row) {
				if (t.hasValues()) {
					tablesThatHaveValues.add(t.getFullname());
				}
			}
		}
		return tablesThatHaveValues;
	}

	private boolean metadataField(_Column c) {
		return c.getFieldType() > 3 || c.getFieldType() == 0;
	}

	@Override
	public void afterFailedValidation() throws Exception {
		Utils.debug(data.errors());
		request.redirecter().status500();
	}

	@Override
	public void postProsessingHook() throws Exception {
		data.setPage(Const.OUTPUT_ALREADY_PRINTED_TO_REQUEST_OUT_PAGE);
	}
}
