package fi.hy.pese.ulko.main.functionality;

import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.app.functionality.BaseFunctionality;
import fi.hy.pese.framework.main.general.data._Table;
import fi.hy.pese.ulko.main.app._UlkoDomainModelRow;
import fi.hy.pese.ulko.main.db.UlkoRowsToDataResultsHandler;

public class CopyFunctionality extends BaseFunctionality<_UlkoDomainModelRow> {

	public CopyFunctionality(_Request<_UlkoDomainModelRow> request) {
		super(request);
	}

	@Override
	public void afterSuccessfulValidation() throws Exception {
		_UlkoDomainModelRow searchParams = dao.newRow();
		searchParams.getPyynto().getUniqueNumericIdColumn().setValue(data.get("id"));
		dao.executeSearch(searchParams, new UlkoRowsToDataResultsHandler(data));

		_UlkoDomainModelRow rowToCopy = data.results().get(0);
		_Table pyynto = rowToCopy.getPyynto();
		_Table loyto = rowToCopy.getLoyto();
		_Table rengastus = rowToCopy.getRengastus();

		clearTapaaminenValues(loyto);
		clearTapaaminenValues(rengastus);

		pyynto.getUniqueNumericIdColumn().clearValue();
		pyynto.get("loyto").clearValue();
		pyynto.get("rengastus").clearValue();
		pyynto.get("tila").setValue("1");
		pyynto.get("roskiin").setValue("E");
		pyynto.get("rengaspyynto_lahetetty").clearValue();
		pyynto.get("loytokirje_lahetetty").clearValue();
		pyynto.get("kirj_pvm").clearValue();
		pyynto.get("muutos_pvm").clearValue();

		data.updateparameters().getPyynto().setValues(pyynto);
		data.updateparameters().getLoyto().setValues(loyto);
		data.updateparameters().getRengastus().setValues(rengastus);

		data.setPage("main_form");
	}

	private void clearTapaaminenValues(_Table tapaaminen) {
		tapaaminen.getUniqueNumericIdColumn().clearValue();
		tapaaminen.get("kirj_pvm").clearValue();
		tapaaminen.get("muutos_pvm").clearValue();
	}

}
