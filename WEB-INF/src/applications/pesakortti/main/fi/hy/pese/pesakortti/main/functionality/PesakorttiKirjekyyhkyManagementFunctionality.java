package fi.hy.pese.pesakortti.main.functionality;

import java.util.Map;

import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.app.functionality.KirjekyyhkyManagementFunctionality;
import fi.hy.pese.framework.main.general.consts.Kirjekyyhky;
import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.main.general.data._Table;
import fi.hy.pese.pesakortti.main.app.PesakorttiConst;
import fi.luomus.commons.config.Config;
import fi.luomus.commons.kirjekyyhky.FormData;
import fi.luomus.commons.kirjekyyhky.KirjekyyhkyAPIClient;
import fi.luomus.commons.kirjekyyhky.KirjekyyhkyAPIClientImple;

public class PesakorttiKirjekyyhkyManagementFunctionality extends KirjekyyhkyManagementFunctionality {

	public PesakorttiKirjekyyhkyManagementFunctionality(_Request<_PesaDomainModelRow> request) {
		super(request);
	}

	@Override
	protected void applicationSpecificConversions(Map<String, String> formData, _PesaDomainModelRow kirjekyyhkyparameters) {
		PesakorttiConst.kirjekyyhkyModelToData(formData, kirjekyyhkyparameters);
		data.set("tark_vuosi", kirjekyyhkyparameters.getTarkastus().get("vuosi").getValue());

		for (_Table t : kirjekyyhkyparameters.getKaynnit()) {
			_Column vaihe = t.get("pesinnan_vaihe");
			_Column ika_munat = t.get("ika_munat");
			_Column ika_poikaset = t.get("ika_poikaset");
			if (ika_poikaset.hasValue()) {
				request.data().set("PesinnanVaihe."+t.getIndex(), ika_poikaset.getValue());
			} else if (ika_munat.hasValue()) {
				request.data().set("PesinnanVaihe."+t.getIndex(), ika_munat.getValue());
			} else {
				request.data().set("PesinnanVaihe."+t.getIndex(), vaihe.getValue());
			}
		}
	}

	@Override
	protected void applicationSpecificReturnedEmptyProcedure(_PesaDomainModelRow kirjekyyhkyParameters) {
		throw new UnsupportedOperationException("Pesäkortteista ei läheteä esitäytettyjä lomakkeita, joten ei myöskään toteutettu esitäytetyn lomakkeen vastaanottoa tyhjänä.");
	}

	@Override
	protected void getListOfForms(KirjekyyhkyAPIClient api) throws Exception {
		try {
			for (FormData form : api.getForms()) {
				data.addToList(Kirjekyyhky.FORMS, form);
			}
			for (FormData form : getYhteenvetoApiClient().getForms()) {
				data.addToList(Kirjekyyhky.FORMS, form);
			}
		} catch (IllegalStateException e) {
			data.addToNoticeTexts("Loading list of forms has failed");
		}
	}

	private KirjekyyhkyAPIClient getYhteenvetoApiClient() throws Exception {
		return new KirjekyyhkyAPIClientImple(
				request.config().get(Config.KIRJEKYYHKY_API_URI),
				request.config().get(Config.KIRJEKYYHKY_API_USERNAME),
				request.config().get(Config.KIRJEKYYHKY_API_PASSWORD),
				"pesakortti-yhdyskunta");
	}

	@Override
	public void afterSuccessfulValidation() throws Exception {
		super.afterSuccessfulValidation();
		if (data.action().equals(Kirjekyyhky.RECEIVE)) {

		}
	}

	@Override
	protected void applicationSpecificComparisonDataOperations(_PesaDomainModelRow kirjekyyhkyParameters, _PesaDomainModelRow comparisonParameters) {}

}
