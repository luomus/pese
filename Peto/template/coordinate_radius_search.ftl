<!-- lahiympariston pesat --> 
<table class="infoContainer">
	<caption> Lähiympäristön&nbsp;pesät </caption>

    <tbody>

<#if ( errors?size > 0) >
<#-- 
      <tr>
        <td>
          	<#assign errorfields = errors?keys>
          	<#list errorfields as errorfield> <span class="error"> ${texts[errorfield]!errorfield}: ${texts[errors[errorfield]]!errors[errorfield]} </span> <br /> </#list>
        </td>
      </tr>
-->      
<#else>
	  <tr>
	    <th>Pesä</th>
        <th>Kylä</th>
        <th>Yht.koord.</th>
        <th>Tyyppi</th>
        <th>Korkeus</th>
        <th></th>
	  </tr>
	  <#list results as r>
	    <#if data.nestid == r.pesa.id>
          <tr class="highlightedRow">
        <#else>
          <tr>
        </#if>
        
          <td>
            <#if data.nestid != r.pesa.id>
              <a href="${system.baseURL}?page=tarkastus&amp;action=fill_for_update&amp;updateparameters.tarkastus.id=${r.tarkastus.id}">
            </#if>
            <#if r.pesa.nimi != ""> 
              ${r.pesa.nimi} (${r.pesa.id})
            <#else>
              ${r.pesa.id}
            </#if>
            <#if data.nestid != r.pesa.id>
              </a>
            </#if>
          </td>
          
          <td>${r.pesa.kyla}</td>
          <td>${r.pesa.yht_leveys} <br />  ${r.pesa.yht_pituus}</td>
          <td>${r.tarkastus.pesa_sijainti}</td>
          <td>${r.tarkastus.pesa_korkeus}</td>
          <td><a target="_karttapaikka" href="https://asiointi.maanmittauslaitos.fi/karttapaikka/?share=customMarker&n=${r.pesa.eur_leveys}&amp;e=${r.pesa.eur_pituus}&amp;title=${r.pesa.nimi.getUrlEncodedValue()}(${r.pesa.id})">Kartalle</a></td>
        </tr>
      </#list>
</#if>
	</tbody>
</table>
