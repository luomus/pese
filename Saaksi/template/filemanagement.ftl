
<#include "header.ftl">

<h1>${texts.filemanagement_title}</h1>

<@statustexts system texts />
<@errorlist errors texts />



<#if data.folder == "pdf">    <#if lists.produced_pdf_files??>    <#assign li = lists.produced_pdf_files> </#if> </#if>
<#if data.folder == "report"> <#if lists.produced_report_files??> <#assign li = lists.produced_report_files> </#if> </#if>

<#if li??>

  <table id="resultsTable" class="tablesorter"> 
    <thead> 
      <tr> 
        <th class="{sorter: false} haavi"> Haavi </th>
        <th style="min-width: 300px;"> Tiedosto </th>
        <th> Luontipvm </th>
        <th class="{sorter: false} haavi"> &nbsp; </th>
        <th class="{sorter: false} haavi"> &nbsp; </th>
      </tr>
    </thead>
    
    <tbody>
    
    <#assign i = 0>
    <#list li as file>  
      <tr> 
        <td class="haavi"> <input type="checkbox" onchange="toNet('${file.name}');" name="nest_entry_${file.name}" /> </td> 
        <td> ${file.name} </td>
        <td> ${file.date} </td>
        <td> <a target="_blank" href="${system.baseURL}?page=${system.page}&amp;action=show&amp;folder=${data.folder}&amp;filename=${file.name}"> Avaa/tallenna </a> </td>
        <td>
        		<form action="${system.baseURL}" method="post">
         		  <input type="hidden" name="page"     value="${system.page}" />
      			  <input type="hidden" name="action"   value="delete" />
      			  <input type="hidden" name="folder"   value="${data.folder}" />
          		  <input type="hidden" name="filename" value="${file.name}" />
          		  <input type="submit" value="${texts.delete}" />
          		</form>
        </td>
      </tr>
      <#assign i = i + 1> 
    </#list>
     
    </tbody>
  </table>
  
   <a href="javascript:selectAll();" style="font-size: 10px;">&raquo; lisää kaikki tiedostot haaviin</a>
   <br />
  
  <br />
  <p> <b> Suorita toiminto haavissa oleville tiedostoille: </b> </p>
  <p>     <button onclick="if (!netIsEmpty()) {getIdsFromNetTo(document.getElementById('filesToShow')); document.openSelected.submit();} else alert('Haavi on tyhjä');">
           Pakkaa ja tallenna
          </button>
  </p>
  <p>     <button onclick="if (!netIsEmpty()) {getIdsFromNetTo(document.getElementById('filesToDelete'));  document.deleteSelected.submit();} else alert('Haavi on tyhjä');">
           Poista tiedostot
          </button>
  </p>
  
  <form action="${system.baseURL}" method="post" name="openSelected">
  	  <input type="hidden" name="page"     value="${system.page}" />
  	  <input type="hidden" name="action"   value="compress_and_show" />
  	  <input type="hidden" name="folder"   value="${data.folder}" />
  	  <input type="hidden" name="filename" value="" id="filesToShow" />
  </form>
  
  <form action="${system.baseURL}" method="post" name="deleteSelected">
  	  <input type="hidden" name="page"     value="${system.page}" />
  	  <input type="hidden" name="action"   value="delete" />
  	  <input type="hidden" name="folder"   value="${data.folder}" />
  	  <input type="hidden" name="filename" value="" id="filesToDelete" />
  </form>
  
  <br /><br /><br />
  
</#if>


<#include "footer.ftl">

