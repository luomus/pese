package fi.hy.pese.ulko.main.app;

import fi.hy.pese.framework.main.general.data.Row;
import fi.hy.pese.framework.main.general.data.Table;
import fi.hy.pese.framework.main.general.data._Table;

public class UlkoDomainModelRow extends Row implements _UlkoDomainModelRow {
	
	private static final long	serialVersionUID	= -7376761190269172927L;
	
	private final Table pyynto;
	private final Table loyto;
	private final Table rengastus;
	
	public UlkoDomainModelRow(UlkoDatabaseStructure rowStructure) {
		this(rowStructure, null);
	}
	
	public UlkoDomainModelRow(UlkoDatabaseStructure rowStructure, String prefix) {
		super(rowStructure, prefix);
		pyynto = new Table(rowStructure.getPyynto());
		loyto = new Table(rowStructure.getLoyto());
		rengastus = new Table(rowStructure.getRengastus());
		defineBasetable(pyynto);
		defineBasetable(loyto);
		defineBasetable(rengastus);
	}
	
	@Override
	public _Table getPyynto() {
		return pyynto;
	}
	
	@Override
	public _Table getLoyto() {
		return loyto;
	}
	
	@Override
	public _Table getRengastus() {
		return rengastus;
	}
	
}
