package fi.hy.pese.framework.main.general;

import fi.hy.pese.framework.main.general.WarehouseRowGenerator.NestIsDead;
import fi.hy.pese.framework.main.general.WarehouseRowGenerator.NotInspected;
import fi.hy.pese.framework.main.general.WarehouseRowGenerator.ResolveAdditionalNestingSpecies;
import fi.hy.pese.framework.main.general.WarehouseRowGenerator.ResolveAtlasCode;
import fi.hy.pese.framework.main.general.WarehouseRowGenerator.ResolveCoordinates;
import fi.hy.pese.framework.main.general.WarehouseRowGenerator.ResolveCounts;
import fi.hy.pese.framework.main.general.WarehouseRowGenerator.ResolveDateAccuracy;
import fi.hy.pese.framework.main.general.WarehouseRowGenerator.ResolveLocality;
import fi.hy.pese.framework.main.general.WarehouseRowGenerator.ResolveNestCount;
import fi.hy.pese.framework.main.general.WarehouseRowGenerator.ResolveSecure;
import fi.hy.pese.framework.main.general.WarehouseRowGenerator.ResolveSpecies;
import fi.hy.pese.framework.main.general.WarehouseRowGenerator.ResolveSuspecious;
import fi.hy.pese.framework.main.general.WarehouseRowGenerator.WarehouseGenerationDefinition;
import fi.luomus.commons.containers.rdf.Qname;

public class WarehouseGenerationDefinitionBuilder {
	Qname sourceId;
	Qname collectionId;
	NestIsDead nestIsDead;
	NotInspected notInspected;
	String yearColumnFullname;
	String dateColumnFullname;
	String siteTypeColumnFullName;
	String municipalityColumnFullName;
	ResolveCoordinates coordinates;
	ResolveLocality locality;
	ResolveDateAccuracy dateAccuracy;
	ResolveSpecies nestingSpecies;
	ResolveNestCount nestCount;
	ResolveAtlasCode atlasCode;
	ResolveCounts counts;  // only needed if has kaynti
	ResolveAdditionalNestingSpecies additionalNestingSpecies = ResolveAdditionalNestingSpecies.none(); // not required
	ResolveSuspecious isSuspecious = ResolveSuspecious.none(); // not required
	ResolveSecure shouldSecure = ResolveSecure.none(); // not required

	public WarehouseGenerationDefinitionBuilder sourceId(Qname sourceId) {
		this.sourceId = sourceId;
		return this;
	}

	public WarehouseGenerationDefinitionBuilder collectionId(Qname collectionId) {
		this.collectionId = collectionId;
		return this;
	}

	public WarehouseGenerationDefinitionBuilder nestIsDead(NestIsDead nestIsDead) {
		this.nestIsDead = nestIsDead;
		return this;
	}

	public WarehouseGenerationDefinitionBuilder notInspected(NotInspected notInspected) {
		this.notInspected = notInspected;
		return this;
	}

	public WarehouseGenerationDefinitionBuilder yearColumnFullname(String yearColumnFullname) {
		this.yearColumnFullname = yearColumnFullname;
		return this;
	}

	public WarehouseGenerationDefinitionBuilder dateColumnFullname(String dateColumnFullname) {
		this.dateColumnFullname = dateColumnFullname;
		return this;
	}

	public WarehouseGenerationDefinitionBuilder siteTypeColumnFullName(String siteTypeColumnFullName) {
		this.siteTypeColumnFullName = siteTypeColumnFullName;
		return this;
	}

	public WarehouseGenerationDefinitionBuilder municipalityColumnFullName(String municipalityColumnFullName) {
		this.municipalityColumnFullName = municipalityColumnFullName;
		return this;
	}

	public WarehouseGenerationDefinitionBuilder coordinates(ResolveCoordinates coordinates) {
		this.coordinates = coordinates;
		return this;
	}

	public WarehouseGenerationDefinitionBuilder locality(ResolveLocality locality) {
		this.locality = locality;
		return this;
	}

	public WarehouseGenerationDefinitionBuilder dateAccuracy(ResolveDateAccuracy dateAccuracy) {
		this.dateAccuracy = dateAccuracy;
		return this;
	}

	public WarehouseGenerationDefinitionBuilder nestingSpecies(ResolveSpecies nestingSpecies) {
		this.nestingSpecies = nestingSpecies;
		return this;
	}

	public WarehouseGenerationDefinitionBuilder additionalNestingSpecies(ResolveAdditionalNestingSpecies additionalNestingSpecies) {
		this.additionalNestingSpecies = additionalNestingSpecies;
		return this;
	}

	public WarehouseGenerationDefinitionBuilder nestCount(ResolveNestCount nestCount) {
		this.nestCount = nestCount;
		return this;
	}

	public WarehouseGenerationDefinitionBuilder atlasCode(ResolveAtlasCode atlasCode) {
		this.atlasCode = atlasCode;
		return this;
	}

	public WarehouseGenerationDefinitionBuilder counts(ResolveCounts counts) {
		this.counts = counts;
		return this;
	}

	public WarehouseGenerationDefinitionBuilder isSuspecious(ResolveSuspecious isSuspecious) {
		this.isSuspecious = isSuspecious;
		return this;
	}

	public WarehouseGenerationDefinitionBuilder shouldSecure(ResolveSecure shouldSecure) {
		this.shouldSecure = shouldSecure;
		return this;
	}

	public WarehouseGenerationDefinition build() {
		return new WarehouseGenerationDefinition(this);
	}
}