package fi.hy.pese.framework.main.db;

import java.sql.SQLException;

import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._Table;
import fi.luomus.commons.utils.DateUtils;

public abstract class BaseResultsHandler implements _ResultsHandler {

	protected void setResultsToTable(_ResultSet rs, _Table table) throws SQLException {
		setResultsToTable(rs, table, 1);
	}

	protected int setResultsToTable(_ResultSet rs, _Table table, int i) throws SQLException {
		if (table == null) return i;
		if (!table.isInitialized()) return i;

		for (_Column column : table) {
			if (isDate(column)) {
				column.setValue(DateUtils.sqlDateToString(rs.getDate(i++)));
			} else {
				column.setValue(rs.getString(i++));
			}
		}
		return i;
	}

	private boolean isDate(_Column column) {
		return column.getDatatype() == _Column.DATE;
	}

	protected int skip(_Table table, int i) {
		if (!table.isInitialized()) return i;
		return i + table.numberOfColumns();
	}

}
