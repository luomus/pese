package fi.hy.pese.peto.main.app;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import fi.hy.pese.framework.main.db.oraclesql.Queries;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data.DatabaseColumnStructure;
import fi.hy.pese.framework.main.general.data.DatabaseTableStructure;
import fi.hy.pese.framework.main.general.data.PesaDomainModelDatabaseStructure;
import fi.hy.pese.framework.main.general.data._Table;
import fi.luomus.commons.containers.Selection;
import fi.luomus.commons.db.connectivity.TransactionConnection;
import fi.luomus.commons.tipuapi.TipuAPIClient;

public class PetoDatabaseStructureCreator {
	
	private static final String	TARKASTUS_KORKEUS_TARKKUUS	= "tarkastus.korkeus_tarkkuus";
	private static final String	TARKASTUS_MAASTO			= "tarkastus.maasto";
	private static final String KAYNTI_LUKUMAARAT_TARKKUUS  = "kaynti.lkm_tarkkuus";
	
	public static PesaDomainModelDatabaseStructure loadFromDatabase(TransactionConnection con) throws SQLException {
		
		PesaDomainModelDatabaseStructure structure = new PesaDomainModelDatabaseStructure();
		
		// Taulut
		DatabaseTableStructure pesa = Queries.returnTableStructure("pesa", con);
		DatabaseTableStructure tarkastus = Queries.returnTableStructure("tarkastus", con);
		DatabaseTableStructure kaynti = Queries.returnTableStructure("kaynti", con);
		DatabaseTableStructure searches = Queries.returnTableStructure("saved_searches", con);
		DatabaseTableStructure aputaulu = Queries.returnTableStructure("aputaulu", con);
		DatabaseTableStructure muutosloki = Queries.returnTableStructure("muutosloki", con);
		DatabaseTableStructure seurantaruutu = Queries.returnTableStructure("seurantaruutu", con);
		DatabaseTableStructure lomakeVastaanottajat = Queries.returnTableStructure("lomake_vastaanottaja", con);
		
		// N kpl sisältävien staattinen lukumäärä (listauksiin yms)
		structure.setKaynnitCount(4);
		
		
		// Taulut templateen
		structure.setPesa(pesa);
		structure.setTarkastus(tarkastus);
		structure.setKaynti(kaynti);
		structure.setSavedSearches(searches);
		structure.setAputaulu(aputaulu);
		structure.setLogTable(muutosloki);
		structure.defineCustomTable(seurantaruutu);
		structure.setLomakeVastaanottajat(lomakeVastaanottajat);
		structure.setTransactionEntry(Const.TRANSACTION_ENTRY_TABLE_STRUCTURE);
		
		// Avaimet
		pesa.get("id").setTypeToUniqueNumericIncreasingId();
		tarkastus.get("id").setTypeToUniqueNumericIncreasingId();
		kaynti.get("id").setTypeToUniqueNumericIncreasingId();
		aputaulu.get("taulu").setTypeToImmutablePartOfAKey();
		aputaulu.get("kentta").setTypeToImmutablePartOfAKey();
		aputaulu.get("arvo").setTypeToImmutablePartOfAKey();
		searches.get("id").setTypeToUniqueNumericIncreasingId();
		muutosloki.get("id").setTypeToUniqueNumericIncreasingId();
		seurantaruutu.get("id").setTypeToUniqueNumericIncreasingId();
		lomakeVastaanottajat.get("pesa").setTypeToImmutablePartOfAKey();
		lomakeVastaanottajat.get("vastaanottaja").setTypeToImmutablePartOfAKey();
		
		// Viiteavaimet
		tarkastus.get("pesa").references(pesa.getUniqueNumericIdColumn());
		
		// Lisäyspäivämääräkentät
		pesa.get("kirj_pvm").setTypeToDateAddedColumn();
		tarkastus.get("kirj_pvm").setTypeToDateAddedColumn();
		kaynti.get("kirj_pvm").setTypeToDateAddedColumn();
		muutosloki.get("kirj_pvm").setTypeToDateAddedColumn();
		seurantaruutu.get("kirj_pvm").setTypeToDateAddedColumn();
		
		// Muokkauspäivämääräkentät
		pesa.get("muutos_pvm").setTypeToDateModifiedColumn();
		tarkastus.get("muutos_pvm").setTypeToDateModifiedColumn();
		kaynti.get("muutos_pvm").setTypeToDateModifiedColumn();
		seurantaruutu.get("muutos_pvm").setTypeToDateModifiedColumn();
		
		// Sekalaiset
		muutosloki.get("kayttaja").setTypeToUserIdColumn();
		tarkastus.get("pesinnan_yhdistava_tunniste").setToUppercaseColumn();
		tarkastus.get("ref_vanhaan_dataan").setAllowWhitespace(true);
		
		// Käytöstä poistuneet kentät
		kaynti.removeColumn("lkm_tarkkuus");
		tarkastus.removeColumn("seurantaruutu");
		
		// Tauluhallintaoikeudet
		aputaulu.setTablemanagementPermissions(_Table.TABLEMANAGEMENT_UPDATE_INSERT_DELETE);
		muutosloki.setTablemanagementPermissions(_Table.TABLEMANAGEMENT_INSERT_ONLY);
		seurantaruutu.setTablemanagementPermissions(_Table.TABLEMANAGEMENT_UPDATE_INSERT_DELETE);
		aputaulu.get("ryhmittely").setToOptionalColumn();
		
		// Liitokset selecteihin
		// Ensin aputaulussa olevan kentät automaattisesti
		Map<String, Selection> selections = Queries.returnSelectionsFromCodeTable(con, "aputaulu", "taulu", "kentta", "arvo", "selite", "ryhmittely", new String[] {"jarjestys"});
		for (String name : selections.keySet()) {
			try {
				DatabaseColumnStructure column = structure.getColumn(name);
				column.setAssosiatedSelection(name);
			} catch (Exception e) { /* System.out.println(" Assosiation "+name + " does not match a field."); */
			}
		}
		// Loput liitokset manuaalisesti
		pesa.get("seur_tarkastaja").setAssosiatedSelection(TipuAPIClient.RINGERS);
		pesa.get("lomake").setAssosiatedSelection(PetoConst.K_E);
		pesa.get("kunta").setAssosiatedSelection(TipuAPIClient.MUNICIPALITIES);
		pesa.get("rak_laji").setAssosiatedSelection(PetoConst.RAKENTAVAT_LAJIT);
		pesa.get("suojelu_kaytto").setAssosiatedSelection(PetoConst.K_E);
		tarkastus.get("tarkastettu").setAssosiatedSelection(PetoConst.K_E);
		tarkastus.get("pesiva_laji").setAssosiatedSelection(PetoConst.PESIVAT_LAJIT);
		tarkastus.get("tarkastaja").setAssosiatedSelection(TipuAPIClient.RINGERS);
		tarkastus.get("pesa_korkeus_tarkkuus").setAssosiatedSelection(TARKASTUS_KORKEUS_TARKKUUS);
		tarkastus.get("puu_korkeus_tarkkuus").setAssosiatedSelection(TARKASTUS_KORKEUS_TARKKUUS);
		tarkastus.get("pesa_etaisyys_runko_tark").setAssosiatedSelection(TARKASTUS_KORKEUS_TARKKUUS);
		tarkastus.get("maasto_1").setAssosiatedSelection(TARKASTUS_MAASTO);
		tarkastus.get("maasto_2").setAssosiatedSelection(TARKASTUS_MAASTO);
		tarkastus.get("nayte_munia").setAssosiatedSelection(PetoConst.K_E);
		tarkastus.get("nayte_kuolleita").setAssosiatedSelection(PetoConst.K_E);
		tarkastus.get("nayte_sulkia").setAssosiatedSelection(PetoConst.K_E);
		tarkastus.get("nayte_saalis").setAssosiatedSelection(PetoConst.K_E);
		tarkastus.get("kuvia").setAssosiatedSelection(PetoConst.K_E);
		tarkastus.get("muu_laji").setAssosiatedSelection(TipuAPIClient.SPECIES);
		kaynti.get("aikuisia_lkm_tarkkuus").setAssosiatedSelection(KAYNTI_LUKUMAARAT_TARKKUUS);
		kaynti.get("munia_lkm_tarkkuus").setAssosiatedSelection(KAYNTI_LUKUMAARAT_TARKKUUS);
		kaynti.get("pesapoikasia_lkm_tarkkuus").setAssosiatedSelection(KAYNTI_LUKUMAARAT_TARKKUUS);
		kaynti.get("lentopoikasia_lkm_tarkkuus").setAssosiatedSelection(KAYNTI_LUKUMAARAT_TARKKUUS);
		kaynti.get("kuolleita_lkm_tarkkuus").setAssosiatedSelection(KAYNTI_LUKUMAARAT_TARKKUUS);
		kaynti.get("siruja").setAssosiatedSelection(PetoConst.K_E);
		kaynti.get("hautova_emo").setAssosiatedSelection(PetoConst.K_E);
		aputaulu.get("taulu").setAssosiatedSelection(PetoConst.TAULUT);
		
		// Kirjekyyhky pakolliset
		tarkastus.get("tarkastaja").setKirjekyyhkyRequired();
		pesa.get("lomake").setKirjekyyhkyRequired();
		pesa.get("kunta").setKirjekyyhkyRequired();
		pesa.get("kyla").setKirjekyyhkyRequired();
		pesa.get("koord_ilm_tyyppi").setKirjekyyhkyRequired();
		pesa.get("koord_ilm_mittaustapa").setKirjekyyhkyRequired();
		tarkastus.get("pesa_sijainti").setKirjekyyhkyRequired();
		tarkastus.get("vuosi").setKirjekyyhkyRequired();
		
		// Muokataan kenttien järjestyksiä
		List<String> orderOfAputauluColumns = new ArrayList<>();
		orderOfAputauluColumns.add("taulu");
		orderOfAputauluColumns.add("kentta");
		orderOfAputauluColumns.add("jarjestys");
		orderOfAputauluColumns.add("arvo");
		orderOfAputauluColumns.add("selite");
		aputaulu.reorder(orderOfAputauluColumns);
		
		List<String> orderOfTarkastusColumns = new ArrayList<>();
		for (DatabaseColumnStructure c : tarkastus.getColumns()) {
			String name = c.getName();
			if (!name.equals("id") && !name.equals("pesa")) {
				orderOfTarkastusColumns.add(name);
			}
		}
		tarkastus.reorder(orderOfTarkastusColumns);
		
		List<String> orderOfKayntiColumns = new ArrayList<>();
		for (DatabaseColumnStructure c : kaynti.getColumns()) {
			String name = c.getName();
			if (!name.equals("id") && !name.equals("tarkastus")) {
				orderOfKayntiColumns.add(name);
			}
		}
		kaynti.reorder(orderOfKayntiColumns);
		
		//		for (DatabaseTableStructure t : structure) {
		//			for (DatabaseColumnStructure c : t.getColumns()) {
		//				System.out.println(c.getFullname() + " = ");
		//			}
		//		}
		//		for (DatabaseTableStructure t : structure) {
		//			for (DatabaseColumnStructure c : t.getColumns()) {
		//				System.out.println("short."+c.getFullname() + " = ");
		//			}
		//		}
		
		return structure;
	}
	
	// ---------------------------------- ---------------------------------- ---------------------------------- ---------------------------------- ----------------------------------
	
	public static PesaDomainModelDatabaseStructure hardCoded() {
		PesaDomainModelDatabaseStructure structure = new PesaDomainModelDatabaseStructure();
		DatabaseTableStructure pesa = new DatabaseTableStructure("pesa");
		pesa.addColumn("id", 2, 10);
		
		pesa.addColumn("seur_tarkastaja", 2, 4);
		pesa.addColumn("lomake", 1, 1);
		pesa.addColumn("kunta", 1, 6);
		pesa.addColumn("kyla", 1, 30);
		pesa.addColumn("nimi", 1, 30);
		pesa.addColumn("yht_leveys", 2, 7);
		pesa.addColumn("yht_pituus", 2, 7);
		pesa.addColumn("eur_leveys", 2, 7);
		pesa.addColumn("eur_pituus", 2, 7);
		pesa.addColumn("ast_leveys", 2, 6);
		pesa.addColumn("ast_pituus", 2, 6);
		pesa.addColumn("des_leveys", 3, 10 , 7);
		pesa.addColumn("des_pituus", 3, 10 , 7);
		pesa.addColumn("koord_ilm_tyyppi", 1, 1);
		pesa.addColumn("koord_ilm_mittaustapa", 1, 1);
		pesa.addColumn("rak_laji", 1, 7);
		pesa.addColumn("rak_vuosi", 2, 4);
		pesa.addColumn("rak_vuosi_tarkkuus", 1, 1);
		pesa.addColumn("loyt_vuosi", 2, 4);
		pesa.addColumn("puu_laji", 1, 1);
		pesa.addColumn("suojelu_kaytto", 1, 1);
		pesa.addColumn("kommentti", 1, 500);
		pesa.addColumn("kirj_pvm", 4);
		pesa.addColumn("muutos_pvm", 4);
		
		DatabaseTableStructure tarkastus = new DatabaseTableStructure("tarkastus");
		tarkastus.addColumn("vuosi", 2, 4);
		tarkastus.addColumn("tarkastettu", 1, 1);
		tarkastus.addColumn("pesiva_laji", 1, 7);
		tarkastus.addColumn("tarkastaja", 2, 4);
		tarkastus.addColumn("pesimistulos", 1, 1);
		tarkastus.addColumn("pesimistulos_tarkkuus", 1, 1);
		tarkastus.addColumn("epaonnistuminen", 1, 1);
		tarkastus.addColumn("epaonnistuminen_tarkkuus", 1, 1);
		tarkastus.addColumn("pesa_sijainti", 1, 1);
		tarkastus.addColumn("pesa_korkeus", 2, 3);
		tarkastus.addColumn("pesa_korkeus_tarkkuus", 1, 1);
		tarkastus.addColumn("pesa_etaisyys_runko", 2, 3);
		tarkastus.addColumn("pesa_etaisyys_runko_tark", 1, 1);
		tarkastus.addColumn("puu_elavyys", 1, 1);
		tarkastus.addColumn("puu_korkeus", 2, 3);
		tarkastus.addColumn("puu_korkeus_tarkkuus", 1, 1);
		tarkastus.addColumn("puusto", 1, 1);
		tarkastus.addColumn("puusto_kasittelyaste", 1, 1);
		tarkastus.addColumn("maasto_1", 1, 1);
		tarkastus.addColumn("maasto_2", 1, 1);
		tarkastus.addColumn("rauhoitustaulu", 1, 1);
		tarkastus.addColumn("seurantaruutu", 1, 1);
		tarkastus.addColumn("kuoriutumattomia_lkm", 2, 2);
		tarkastus.addColumn("aikuisia_max_lkm", 2, 2);
		tarkastus.addColumn("nayte_munia", 1, 1);
		tarkastus.addColumn("nayte_kuolleita", 1, 1);
		tarkastus.addColumn("nayte_sulkia", 1, 1);
		tarkastus.addColumn("kommentti_vaihtop", 1, 500);
		tarkastus.addColumn("kommentti_saalis", 1, 500);
		tarkastus.addColumn("kommentti_saalistusalue", 1, 500);
		tarkastus.addColumn("kommentti_edellinen_laji", 1, 20);
		tarkastus.addColumn("kirj_pvm", 4);
		tarkastus.addColumn("muutos_pvm", 4);
		tarkastus.addColumn("nayte_saalis", 1, 1);
		tarkastus.addColumn("kommentti_pesimistulos", 1, 500);
		tarkastus.addColumn("kommentti_kaynnit", 1, 4000);
		tarkastus.addColumn("kommentti_renkaat", 1, 500);
		tarkastus.addColumn("id", 2, 15);
		tarkastus.addColumn("pesa", 2, 10);
		tarkastus.addColumn("ref_vanhaan_dataan", 1, 100);
		tarkastus.addColumn("oli_nipussa", 1, 30);
		tarkastus.addColumn("pesinnan_jarjestysnumero", 2, 10);
		tarkastus.addColumn("pesinnan_yhdistava_tunniste", 1, 30);
		tarkastus.addColumn("kommentti_jarjestys", 1, 500);
		tarkastus.addColumn("muu_laji", 1, 7);
		DatabaseTableStructure kaynti = new DatabaseTableStructure("kaynti");
		kaynti.addColumn("pvm", 4);
		kaynti.addColumn("pvm_tarkkuus", 1, 1);
		kaynti.addColumn("pesa_kunto", 1, 1);
		kaynti.addColumn("aikuisia_lkm", 2, 2);
		kaynti.addColumn("munia_lkm", 2, 2);
		kaynti.addColumn("pesapoikasia_lkm", 2, 2);
		kaynti.addColumn("lentopoikasia_lkm", 2, 2);
		kaynti.addColumn("kuolleita_lkm", 2, 2);
		kaynti.addColumn("siruja", 1, 1);
		kaynti.addColumn("hautova_emo", 1, 1);
		kaynti.addColumn("lkm_tarkkuus", 1, 1);
		kaynti.addColumn("max_siipi", 2, 3);
		kaynti.addColumn("max_siipi_mittaustapa", 1, 1);
		kaynti.addColumn("max_paino", 2, 4);
		kaynti.addColumn("kirj_pvm", 4);
		kaynti.addColumn("muutos_pvm", 4);
		kaynti.addColumn("id", 2, 10);
		kaynti.addColumn("tarkastus", 2, 15);
		DatabaseTableStructure aputaulu = new DatabaseTableStructure("aputaulu");
		aputaulu.addColumn("taulu", 1, 30);
		aputaulu.addColumn("kentta", 1, 30);
		aputaulu.addColumn("jarjestys", 2, 3);
		aputaulu.addColumn("arvo", 1, 10);
		aputaulu.addColumn("selite", 1, 256);
		
		// N kpl sisältävien staattinen lukumäärä (listauksiin yms)
		structure.setKaynnitCount(4);
		
		// Taulut templateen
		structure.setPesa(pesa);
		structure.setTarkastus(tarkastus);
		structure.setKaynti(kaynti);
		//structure.setSavedSearches(searches);
		structure.setAputaulu(aputaulu);
		
		// Avaimet
		pesa.get("id").setTypeToUniqueNumericIncreasingId();
		tarkastus.get("id").setTypeToUniqueNumericIncreasingId();
		kaynti.get("id").setTypeToUniqueNumericIncreasingId();
		aputaulu.get("taulu").setTypeToImmutablePartOfAKey();
		aputaulu.get("kentta").setTypeToImmutablePartOfAKey();
		aputaulu.get("arvo").setTypeToImmutablePartOfAKey();
		//searches.get("id").setTypeToUniqueNumericIncreasingId();
		
		// Viiteavaimet
		tarkastus.get("pesa").references(pesa.getUniqueNumericIdColumn());
		
		// Lisäyspäivämääräkentät
		pesa.get("kirj_pvm").setTypeToDateAddedColumn();
		tarkastus.get("kirj_pvm").setTypeToDateAddedColumn();
		kaynti.get("kirj_pvm").setTypeToDateAddedColumn();
		
		// Muokkauspäivämääräkentät
		pesa.get("muutos_pvm").setTypeToDateModifiedColumn();
		tarkastus.get("muutos_pvm").setTypeToDateModifiedColumn();
		kaynti.get("muutos_pvm").setTypeToDateModifiedColumn();
		
		// Tauluhallintaoikeudet
		aputaulu.setTablemanagementPermissions(_Table.TABLEMANAGEMENT_UPDATE_INSERT_DELETE);
		
		tarkastus.get("pesinnan_yhdistava_tunniste").setToUppercaseColumn();
		tarkastus.get("ref_vanhaan_dataan").setAllowWhitespace(true);
		
		pesa.get("seur_tarkastaja").setAssosiatedSelection(TipuAPIClient.RINGERS);
		pesa.get("lomake").setAssosiatedSelection("pesa.lomake");
		pesa.get("kunta").setAssosiatedSelection(TipuAPIClient.MUNICIPALITIES);
		pesa.get("koord_ilm_tyyppi").setAssosiatedSelection("pesa.koord_ilm_tyyppi");
		pesa.get("koord_ilm_mittaustapa").setAssosiatedSelection("pesa.koord_ilm_mittaustapa");
		pesa.get("rak_laji").setAssosiatedSelection(TipuAPIClient.SPECIES);
		pesa.get("rak_vuosi_tarkkuus").setAssosiatedSelection("pesa.rak_vuosi_tarkkuus");
		pesa.get("puu_laji").setAssosiatedSelection("pesa.puu_laji");
		pesa.get("suojelu_kaytto").setAssosiatedSelection("k_e");
		tarkastus.get("tarkastettu").setAssosiatedSelection("k_e");
		tarkastus.get("pesiva_laji").setAssosiatedSelection(TipuAPIClient.SPECIES);
		tarkastus.get("tarkastaja").setAssosiatedSelection(TipuAPIClient.RINGERS);
		tarkastus.get("pesimistulos").setAssosiatedSelection("tarkastus.pesimistulos");
		tarkastus.get("pesimistulos_tarkkuus").setAssosiatedSelection("tarkastus.pesimistulos_tarkkuus");
		tarkastus.get("epaonnistuminen").setAssosiatedSelection("tarkastus.epaonnistuminen");
		tarkastus.get("epaonnistuminen_tarkkuus").setAssosiatedSelection("tarkastus.epaonnistuminen_tarkkuus");
		tarkastus.get("pesa_sijainti").setAssosiatedSelection("tarkastus.pesa_sijainti");
		tarkastus.get("pesa_korkeus_tarkkuus").setAssosiatedSelection("tarkastus.korkeus_tarkkuus");
		tarkastus.get("puu_elavyys").setAssosiatedSelection("tarkastus.puu_elavyys");
		tarkastus.get("puu_korkeus_tarkkuus").setAssosiatedSelection("tarkastus.korkeus_tarkkuus");
		tarkastus.get("puusto").setAssosiatedSelection("tarkastus.puusto");
		tarkastus.get("puusto_kasittelyaste").setAssosiatedSelection("tarkastus.puusto_kasittelyaste");
		tarkastus.get("maasto_1").setAssosiatedSelection("tarkastus.maasto");
		tarkastus.get("maasto_2").setAssosiatedSelection("tarkastus.maasto");
		tarkastus.get("rauhoitustaulu").setAssosiatedSelection("tarkastus.rauhoitustaulu");
		tarkastus.get("seurantaruutu").setAssosiatedSelection("k_e");
		tarkastus.get("nayte_munia").setAssosiatedSelection("k_e");
		tarkastus.get("nayte_kuolleita").setAssosiatedSelection("k_e");
		tarkastus.get("nayte_sulkia").setAssosiatedSelection("k_e");
		tarkastus.get("nayte_saalis").setAssosiatedSelection("k_e");
		tarkastus.get("muu_laji").setAssosiatedSelection(TipuAPIClient.SPECIES);
		kaynti.get("pvm_tarkkuus").setAssosiatedSelection("kaynti.pvm_tarkkuus");
		kaynti.get("pesa_kunto").setAssosiatedSelection("kaynti.pesa_kunto");
		kaynti.get("siruja").setAssosiatedSelection("k_e");
		kaynti.get("hautova_emo").setAssosiatedSelection("k_e");
		kaynti.get("lkm_tarkkuus").setAssosiatedSelection("kaynti.lkm_tarkkuus");
		kaynti.get("max_siipi_mittaustapa").setAssosiatedSelection("kaynti.max_siipi_mittaustapa");
		aputaulu.get("taulu").setAssosiatedSelection("taulut");
		return structure;
	}
	
	
	//	public static void generateJavacodeFromDatabaseGeneratedDatabaseStructure(_Data data) {
	//		_Row params = data.updateparameters();
	//		for (_Table t : params) {
	//			String tablename = t.getTablename();
	//			System.out.println("DatabaseTableStructure "+t.getTablename()+" = new DatabaseTableStructure(\""+t.getTablename()+"\");");
	//			Iterator<_Column> iterator = t.getColumns();
	//			while (iterator.hasNext()) {
	//				_Column c = iterator.next();
	//				if (c.getName().equals(Const.ROWID)) continue;
	//				if (c.getDatatype() == _Column.VARCHAR)
	//					System.out.println(tablename+".addColumn(\""+c.getName()+"\", "+c.getDatatype()+", "+c.getSize()+");");
	//				else if (c.getDatatype() == _Column.DATE)
	//					System.out.println(tablename+".addColumn(\""+c.getName()+"\", "+c.getDatatype()+");");
	//				else if (c.getDatatype() == _Column.INTEGER)
	//					System.out.println(tablename+".addColumn(\""+c.getName()+"\", "+c.getDatatype()+", "+c.getSize()+");");
	//				else if (c.getDatatype() == _Column.DECIMAL)
	//					System.out.println(tablename+".addColumn(\""+c.getName()+"\", "+c.getDatatype()+", "+c.getSize()+" , "+c.getDecimalSize()+");");
	//			}
	//		}
	//		for (_Table t : params) {
	//			String tablename = t.getTablename();
	//			Iterator<_Column> iterator = t.getColumns();
	//			while (iterator.hasNext()) {
	//				_Column c = iterator.next();
	//				if (!c.getAssosiatedSelection().equals("")) {
	//					System.out.println(tablename+".get(\""+c.getName()+"\").setAssosiatedSelection(\""+c.getAssosiatedSelection()+"\");");
	//				}
	//			}
	//		}
	//	}
	
}
