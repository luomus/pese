package fi.hy.pese.framework.main.general;

import fi.laji.datawarehouse.etl.models.dw.DwRoot;

public interface _WarehouseRowGenerator {

	/**
	 * Generate warehouse data for the given row
	 * @param rowId
	 * @return
	 * @throws Exception
	 */
	DwRoot generate(int rowId) throws Exception;

}
