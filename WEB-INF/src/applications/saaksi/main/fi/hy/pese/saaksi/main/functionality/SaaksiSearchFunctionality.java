package fi.hy.pese.saaksi.main.functionality;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.app.functionality.NestSorter;
import fi.hy.pese.framework.main.app.functionality.SearchFunctionality;
import fi.hy.pese.framework.main.general.ReportWriter;
import fi.hy.pese.framework.main.general._PDFDataMapGenerator;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.main.general.data._ResultSorter;
import fi.hy.pese.framework.main.general.data._Table;
import fi.hy.pese.saaksi.main.app.SaaksiConst;
import fi.hy.pese.saaksi.main.functionality.reports.SaaksiReport_havainnoijalistaus;
import fi.hy.pese.saaksi.main.functionality.reports.SaaksiReport_kansilehdet;
import fi.hy.pese.saaksi.main.functionality.reports.SaaksiReport_pesimistulokset;
import fi.hy.pese.saaksi.main.functionality.reports.SaaksiReport_poikasmaarat;
import fi.luomus.commons.tipuapi.TipuAPIClient;
import fi.luomus.commons.utils.Utils;
import fi.luomus.commons.xml.Document.Node;

public class SaaksiSearchFunctionality extends SearchFunctionality {

	public SaaksiSearchFunctionality(_Request<_PesaDomainModelRow> request) {
		super(request);
	}

	private SaaksiDataToMapForPDFPrintingGenerator	dataToMapForPDFPrintingGenerator	= null;

	@Override
	public String nestingOutput(_Table tarkastus) {
		String lentopoik = tarkastus.get("lentopoik").getValue();
		String lop_lentopoik = tarkastus.get("lop_lentopoik").getValue();
		String pesapoik = tarkastus.get("elavia").getValue();
		String count = Utils.maxOf(nullIfNine(lentopoik), nullIfNine(lop_lentopoik), nullIfNine(pesapoik));
		if (count.equals("0")) return "";
		return count;
	}

	@Override
	public _ResultSorter<_PesaDomainModelRow> sorter() {
		return new NestSorter(comparator());
	}

	public static Comparator<_PesaDomainModelRow> comparator() {
		return new Comparator<_PesaDomainModelRow>() {

			@Override
			public int compare(_PesaDomainModelRow first, _PesaDomainModelRow second) {
				int result = first.getReviiri().get("hallintolaani").getValue().compareTo(second.getReviiri().get("hallintolaani").getValue());
				if (result != 0) return result;
				result = first.getReviiri().get("hallintokunta").getValue().compareTo(second.getReviiri().get("hallintokunta").getValue()); // jos oikeen tarkkoja halutaan olla, niin vaihdetaan tässä sorttaus tapahtumaan kulyh sijasta kunnan nimen kanssa (tipu-apista)
				if (result != 0) return result;
				result = first.getReviiri().get("reviirinimi").getValue().compareTo(second.getReviiri().get("reviirinimi").getValue());
				if (result != 0) return result;
				result = first.getReviiri().get("reviiriid").getValue().compareTo(second.getReviiri().get("reviiriid").getValue());
				if (result != 0) return result;
				result = first.getPesa().get("pesanimi").getValue().compareTo(second.getPesa().get("pesanimi").getValue());
				if (result != 0) return result;
				result = first.getPesa().get("pesaid").getValue().compareTo(second.getPesa().get("pesaid").getValue());
				return result;
			}

		};
	}

	@Override
	public _PDFDataMapGenerator pdfDataMapGenerator() {
		if (dataToMapForPDFPrintingGenerator == null) {
			dataToMapForPDFPrintingGenerator = new SaaksiDataToMapForPDFPrintingGenerator(request, this);
		}
		return dataToMapForPDFPrintingGenerator;
	}

	@Override
	public String deadNestsQuery()  {
		return " pesa.pesaid IN (select distinct pesaid from tarkastus where tarkastus.epaonn = 'D' ) ";
	}

	@Override
	public String liveNestsQuery()  {
		return " pesa.pesaid NOT IN (select distinct pesaid from tarkastus where tarkastus.epaonn = 'D' ) ";
	}

	@Override
	protected void applicationPrintReport(String reportName) {
		ReportWriter<_PesaDomainModelRow> writer = null;
		if (reportName.equals(SaaksiConst.REPORT_HAVAINNOIJALISTAUS)) {
			writer = new SaaksiReport_havainnoijalistaus(request, reportName);
		} else if (reportName.equals(SaaksiConst.REPORT_KANSILEHDET)) {
			writer = new SaaksiReport_kansilehdet(request, reportName);
		} else if (reportName.equals(SaaksiConst.REPORT_PESIMISTULOKSET)) {
			writer = new SaaksiReport_pesimistulokset(request, reportName);
		} else if (reportName.equals(SaaksiConst.REPORT_POIKASMAARAT)) {
			writer = new SaaksiReport_poikasmaarat(request, reportName);
		}
		if (writer != null) {
			writer.produce();
		} else {
			data.addToNoticeTexts(reportName + " not implemented");
		}
	}

	@Override
	protected void additionalSearchConditionsHook(List<String> additionalQueries) {
		if (data.contains(Const.YMPARISTOKESKUS)) {
			addYmparistokeskusSearchQuery(additionalQueries);
		}
		if (data.contains("EkaTarkastusVuosi")) {
			addEkaTarkastusVuosiSearchQuery(additionalQueries);
		}
	}

	private void addEkaTarkastusVuosiSearchQuery(List<String> additionalQueries) {
		String vuosivali = data.get("EkaTarkastusVuosi");
		StringBuilder statement = new StringBuilder();
		statement.append(" (SELECT MIN(vuosi) FROM tarkastus ekaVuosiTarkastus WHERE ekaVuosiTarkastus.pesaid = pesa.pesaid) ");
		if (!vuosivali.contains("-")) {
			statement.append(" = ").append(vuosivali);
		} else {
			statement.append(" BETWEEN ").append(vuosivali.split("-")[0]).append(" AND ").append(vuosivali.split("-")[1]).append(" ");
		}
		additionalQueries.add(statement.toString());
	}

	private void addYmparistokeskusSearchQuery(List<String> additionalQueries) {
		String ympkesk = data.get(Const.YMPARISTOKESKUS);
		List<String> ympkeskKunnat = new LinkedList<>();
		for (Node n : request.getTipuApiResource(TipuAPIClient.MUNICIPALITIES).getAll()) {
			if (n.getNode("ely-centre").getContents().equals(ympkesk)) {
				ympkeskKunnat.add(n.getNode("id").getContents());
			}
		}
		StringBuilder statement = new StringBuilder();
		statement.append(data.searchparameters().getReviiri().get("hallintokunta").getFullname());
		statement.append(" IN ( ");
		Utils.toCommaSeperatedStatement(statement, ympkeskKunnat, true);
		statement.append(" ) ");
		additionalQueries.add(statement.toString());
	}

	private String nullIfNine(String lentopoik) {
		if (lentopoik.equals("9")) return "";
		return lentopoik;
	}
}