drop synonym loyto;
drop synonym rengastus;
drop synonym seq_loyto;
drop synonym seq_rengastus;
drop table pyynto;
drop table tapaaminen;
drop sequence seq_tapaaminen;
drop sequence seq_pyynto;

create table tapaaminen (
id number(7) not null,
scheme varchar2(3),
rengas varchar2(70),
rengas_selite varchar2(500),
rengas_varmennettu varchar2(1),
pvm date,
klo number(2),
aika_tarkkuus varchar2(1),
euring_paikka varchar2(5),
kunta varchar2(50),
tarkempi_paikka varchar2(100),
koord_ilm_tyyppi varchar2(2),
yht_leveys number(7),
yht_pituus number(7),
eur_leveys number(7),
eur_pituus number(6),
wgs84_leveys number(6,3),
wgs84_pituus number(6,3),
wgs84_ast_leveys varchar2(7),
wgs84_ast_pituus varchar2(7),
paikka_tarkkuus varchar2(1),
laji varchar2(7),
sukupuoli varchar2(1),
ika varchar2(2),
paino number(6,1),
siipi number(5),
rasva varchar2(1),
linnun_tila varchar2(2),
loytotapa varchar2(2),
loytotapa_tarkkuus varchar2(1),
kommentti varchar2(500),
alkup_kirje_sisalto varchar2(2000),
kirj_pvm date default sysdate not null,
muutos_pvm date,
CONSTRAINT pk_tapaaminen PRIMARY KEY (id)
);
create sequence seq_tapaaminen;
create synonym seq_loyto for seq_tapaaminen;
create synonym seq_rengastus for seq_tapaaminen;

create table pyynto (
id number(7) not null,
tila varchar2(1) not null,
roskiin varchar2(1) not null,
loyto not null,
rengastus,
rengaspyynto_lahetetty date,
loytokirje_lahetetty date,
loytaja_renro number(5),
loytaja_nimi varchar2(50),
loytaja_osoite varchar2(70),
loytaja_postinumero varchar2(5),
loytaja_postitoimipaikka varchar2(30),
loytaja_puhelin varchar2(50),
loytaja_email varchar2(50),
ilmoittaja_renro number(5),
ilmoittaja_nimi varchar2(50),
ilmoittaja_osoite varchar2(70),
ilmoittaja_postinumero varchar2(5),
ilmoittaja_postitoimipaikka varchar2(30),
ilmoittaja_puhelin varchar2(50),
ilmoittaja_email varchar2(50),
kommentti varchar2(500),
kirj_pvm date default sysdate not null,
muutos_pvm date,
CONSTRAINT pk_pyynto PRIMARY KEY (id),
CONSTRAINT fk_pyynto_loyto FOREIGN KEY (loyto) REFERENCES tapaaminen(id),
CONSTRAINT fk_pyynto_rengastus FOREIGN KEY (rengastus) REFERENCES tapaaminen(id)
);
create sequence seq_pyynto start with 10000;


create synonym loyto for ulko.tapaaminen;
create synonym rengastus for ulko.tapaaminen;

drop synonym loyto;
drop synonym rengastus;
create synonym loyto for tapaaminen;
create synonym rengastus for tapaaminen;


