package fi.hy.pese.framework.main.view;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.general.data._Data;
import fi.hy.pese.framework.main.general.data._Row;
import freemarker.template.Configuration;
import freemarker.template.Template;

public class FreemarkerTemplateViewer implements _Viewer {

	private final Configuration	cfg;

	public FreemarkerTemplateViewer(String templatefolder) throws IOException {
		File folder = new File(templatefolder);
		if (!folder.isDirectory()) throw new FileNotFoundException("Template folder does not exist");

		cfg = new Configuration();
		cfg.setTemplateExceptionHandler(new FreemarkerTemplateViewerExceptionHandler());
		cfg.setDirectoryForTemplateLoading(folder);
		cfg.setDefaultEncoding("UTF-8");
	}

	@Override
	public void viewFor(_Request<? extends _Row> request) throws Exception {
		Map<Object, Object> datamodel = DataToFreemarkerDatamodelConverter.convert(request);
		viewPage(request.data().page(), datamodel, request.out());
	}

	public void viewPage(String page, _Data<? extends _Row> data, PrintWriter out) throws Exception {
		Map<Object, Object> datamodel = DataToFreemarkerDatamodelConverter.convert(data);
		viewPage(page, datamodel, out);
	}

	public void viewPage(String page, Map<Object, Object> datamodel, PrintWriter out) throws Exception {
		Template template = cfg.getTemplate(page + ".ftl");
		template.setEncoding("UTF-8");
		template.process(datamodel, out);
	}
}
