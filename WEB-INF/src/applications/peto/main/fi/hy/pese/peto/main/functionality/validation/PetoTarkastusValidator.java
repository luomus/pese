package fi.hy.pese.peto.main.functionality.validation;

import java.sql.SQLException;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.app.functionality.validate.PesimistulosValidationDefinition;
import fi.hy.pese.framework.main.app.functionality.validate.PesimistulosValidationDefinition.Havainto;
import fi.hy.pese.framework.main.app.functionality.validate.TarkastusValidator;
import fi.hy.pese.framework.main.app.functionality.validate._Validator;
import fi.hy.pese.framework.main.general.PesimisenBiologisetRajatUtil.Rajat;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.main.general.data._Table;
import fi.hy.pese.peto.main.app.PetoConst;
import fi.hy.pese.peto.main.db.PetoDAO;
import fi.luomus.commons.utils.CoordinateConverter;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.Utils;

public class PetoTarkastusValidator extends TarkastusValidator {

	private static final boolean	ACCURACY_NOT_REQUIRED	= false;
	private static final boolean	ACCURACY_REQUIRED	= true;
	//private static final String	NEST_DEAD	= "D";
	private static final String	GROUND_NEST_HAS_BECOME_UNSUITABLE	= "7";
	private static final String	TREE_HAS_BEEN_CUT_DOWN	= "6";
	private static final String	TREE_HAS_FALLEN	= "5";
	public static final Collection<String>	NESTS_NOT_IN_TREE		= Utils.collection("L", "N", "B", "V", "M", "P2", "P3", "X");
	public static final Collection<String>	MAN_MADE_NESTS				= Utils.collection("T", "P", "P2", "P3");
	private static final Collection<String>	PESIMISTULOS_PESIVA_LAJI_MUST_NOT_BE_GIVEN	= Utils.collection("A", "X");
	private static final Collection<String>	PESIMISTULOS_PESIVA_LAJI_MUST_BE_GIVEN	= Utils.collection("L", "M", "N", "O", "P", "Q", "R", "T", "V");
	private static final String MUU_LAJI_PESIMISTULOS = "X";
	private static final Collection<String> KUORIUTUMATTOMIA_SAA_ILMOITTAA = Utils.collection("M", "N", "O", "P", "Q", "R", "T", "V");

	private final _PesaDomainModelRow params;
	private final _Table pesa;
	private final _Table tarkastus;
	private final boolean kirjekyyhkyValidations;
	private final Map<String, Rajat> rajat;

	public PetoTarkastusValidator(_Request<_PesaDomainModelRow> request, Map<String, Rajat> rajat) {
		this(request, false, rajat);
	}

	public PetoTarkastusValidator(_Request<_PesaDomainModelRow> request, boolean kirjekyyhkyValidationsOnly, Map<String, Rajat> rajat) {
		super(request);
		this.params = request.data().updateparameters();
		this.pesa = params.getPesa();
		this.tarkastus = params.getTarkastus();
		this.kirjekyyhkyValidations = kirjekyyhkyValidationsOnly;
		this.rajat = rajat;
	}

	@Override
	public void validate() throws Exception {
		if (kirjekyyhkyValidations) {
			validateSendingTheForm();
			copyAndRemoveError("pesa.yht_leveys", "pesa.leveys");
			copyAndRemoveError("pesa.yht_pituus", "pesa.pituus");
			copyAndRemoveError("pesa.eur_leveys", "pesa.leveys");
			copyAndRemoveError("pesa.eur_pituus", "pesa.pituus");
		} else {
			super.validate();
			if (hasErrors()) return;
			String action = data.action();
			if (action.equals(Const.FILL_FOR_INSERT)) {
				String tark_vuosi = data.get("tark_vuosi");
				checkValidYear("tark_vuosi", tark_vuosi);
				if (hasErrors()) return;
				checkNestIsNotDead();
			} else if (action.equals(Const.UPDATE) || action.equals(Const.INSERT) || action.equals(Const.INSERT_FIRST)) {
				validateSendingTheForm();
			}
		}
	}

	private void checkNestIsNotDead() throws SQLException {
		_Column pesa_id = data.updateparameters().getPesa().getUniqueNumericIdColumn();
		String tark_vuosi = data.get("tark_vuosi");
		if (hasErrors(pesa_id) || hasErrors("tark_vuosi")) return;

		Collection<String> deadNests = PetoDAO.deadNests(dao);

		if (deadNests.contains(pesa_id.getValue())) {
			Integer yearNestMarkedDead = PetoDAO.yearNestHasDied(pesa_id.getValue(), dao);
			Integer entryYear = Integer.valueOf(tark_vuosi);
			if (entryYear > yearNestMarkedDead) {
				error("tark_vuosi", PetoConst.PESA_IS_DEAD_CAN_NOT_INSERT);
			}
		}
	}

	private void validateSendingTheForm() throws Exception {
		checkIds();
		if (hasErrors()) return;

		checkAsUpdateparameters(params);
		checkReferenceValuesOfNonNullColumns(params, dao);
		checkValidYear(tarkastus.get("vuosi"));

		checkNotNull(tarkastus.get("tarkastettu"));
		checkNotNull(tarkastus.get("tarkastaja"));
		checkNotNull(pesa.get("lomake"));
		checkNotNull(pesa.get("kunta"));
		checkNotNull(pesa.get("kyla"));
		checkNotNull(pesa.get("koord_ilm_tyyppi"));
		checkNotNull(pesa.get("koord_ilm_mittaustapa"));
		checkNotNull(tarkastus.get("pesa_sijainti"));

		if (kirjekyyhkyValidations) {
			if (data.action().equals(Const.INSERT_FIRST)) {
				warningIfNotCurrentYear();
			}
			warningIfNull(pesa.get("nimi"));
			warningIfNull(tarkastus.get("puusto"));
			if (!tarkastus.get("puusto").getValue().equals("E")) {
				warningIfNull(tarkastus.get("puusto_kasittelyaste"));
			}
			checkNotNull(tarkastus.get("pesa_sijainti"));
		}

		if (isNull(pesa.get("puu_laji")) && !isNull(tarkastus.get("puu_elavyys"))) {
			error(pesa.get("puu_laji"), PetoConst.PUULAJI_ILMOITETTAVA_JOS_ELAVYYS_ILMOITETTU);
		}

		checkCoordinates();
		checkLomake_seurTarkastaja();
		checkTarkvuosi_loytVuosi_rakVuosi();
		checkPuuPesaKorkeus();
		checkPesaSijaintiPuulajiRakLaji();

		if (hasBeenInspected(tarkastus)) {
			ifHasBeenInspected();
		} else {
			ifHasNotBeenInspected();
		}

		checkMaastoCodesAndSetThemFromCustomDataFieldToTarkastusFields();
	}

	private void warningIfNotCurrentYear() {
		_Column vuosi = tarkastus.get("vuosi");
		if (notNullNoErrors(vuosi)) {
			Integer currentYear = DateUtils.getCurrentYear();
			if (!vuosi.getIntegerValue().equals(currentYear)) {
				warning(tarkastus.get("vuosi"), _Validator.ARE_YOU_SURE);
			}
		}
	}


	private void checkMaastoCodesAndSetThemFromCustomDataFieldToTarkastusFields() {
		if (data.contains("maasto")) {
			_Column maasto_1 = tarkastus.get("maasto_1");
			_Column maasto_2 = tarkastus.get("maasto_2");

			if (kirjekyyhkyValidations) {
				warningIfNull(maasto_1);
			}

			List<String> values = Utils.valuelist(data.get("maasto"));
			if (values.size() == 1) {
				maasto_1.setValue(values.get(0));
			} else {
				maasto_1.setValue(values.get(0));
				maasto_2.setValue(values.get(1));
			}
			if (values.size() > 2) {
				error("maasto", PetoConst.MAKSIMISSAAN_KAKSI_MAASTOKOODIA);
			}
			checkAssosiatedSelectionValue(maasto_1);
			checkAssosiatedSelectionValue(maasto_2);
			if (notNullNoErrors(maasto_1) && notNullNoErrors(maasto_2)) {
				if (maasto_1.getValue().equals(maasto_2.getValue())) {
					error(maasto_2, PetoConst.MAASTOKOODIT_EIVÄT_SAA_OLLA_SAMOJA);
				}
			}

			if (notNullNoErrors(maasto_2) && isNull(maasto_1)) {
				error(maasto_2, PetoConst.MAASTOKOODIA_2_EI_SAA_ANTAA_JOS_KOODIA_1_EI_OLE_ANNETTU);
			}
		}
	}

	private void checkPesaSijaintiPuulajiRakLaji() {
		_Column pesa_sijainti = tarkastus.get("pesa_sijainti");
		_Column puulaji = pesa.get("puu_laji");
		_Column rak_laji = pesa.get("rak_laji");
		if (notNullNoErrors(pesa_sijainti)) {
			if (NESTS_NOT_IN_TREE.contains(pesa_sijainti.getValue())) {
				if (!isNull(puulaji)) {
					error(puulaji, PetoConst.PUU_LAJI_CAN_NOT_BE_GIVEN_FOR_THIS_TYPE_OF_NEST);
				}
			} else {
				if (kirjekyyhkyValidations) {
					checkNotNull(puulaji);
				}
			}
			if (MAN_MADE_NESTS.contains(pesa_sijainti.getValue())) {
				if (!isNull(rak_laji)) {
					error(rak_laji, PetoConst.RAK_LAJI_CAN_NOT_BE_GIVEN_FOR_THIS_TYPE_OF_NEST);
				}
			}
		}
	}

	private void checkPuuPesaKorkeus() {
		_Column puu_korkeus = tarkastus.get("puu_korkeus");
		_Column puu_korkeus_tarkkuus = tarkastus.get("puu_korkeus_tarkkuus");
		_Column pesa_korkeus = tarkastus.get("pesa_korkeus");
		_Column pesa_korkeus_tarkkuus = tarkastus.get("pesa_korkeus_tarkkuus");
		_Column pesa_et_runko = tarkastus.get("pesa_etaisyys_runko");
		_Column pesa_et_runko_tarkkuus = tarkastus.get("pesa_etaisyys_runko_tark");
		_Column pesa_sijainti = tarkastus.get("pesa_sijainti");

		checkValueAndAccuracyPair(puu_korkeus, puu_korkeus_tarkkuus);
		checkValueAndAccuracyPair(pesa_korkeus, pesa_korkeus_tarkkuus);
		checkValueAndAccuracyPair(pesa_et_runko, pesa_et_runko_tarkkuus);

		if (notNullNoErrors(puu_korkeus) && notNullNoErrors(pesa_korkeus)) {
			if (pesa_korkeus.getIntegerValue() > puu_korkeus.getIntegerValue()) {
				error(pesa_korkeus, PetoConst.PESA_CAN_NO_BE_HIGHER_THAN_TREE_HEIGHT);
				error(puu_korkeus, PetoConst.PESA_CAN_NO_BE_HIGHER_THAN_TREE_HEIGHT);
			}
		}

		if (notNullNoErrors(pesa_sijainti)) {
			if (NESTS_NOT_IN_TREE.contains(pesa_sijainti.getValue())) {
				checkIsNull(puu_korkeus, PetoConst.EI_SAA_ANTAA_JOS_PESA_EI_PUUSSA);
				checkIsNull(puu_korkeus_tarkkuus, PetoConst.EI_SAA_ANTAA_JOS_PESA_EI_PUUSSA);
				checkIsNull(pesa_et_runko, PetoConst.EI_SAA_ANTAA_JOS_PESA_EI_PUUSSA);
				checkIsNull(pesa_et_runko_tarkkuus, PetoConst.EI_SAA_ANTAA_JOS_PESA_EI_PUUSSA);
			}
		}
	}

	private void checkTarkvuosi_loytVuosi_rakVuosi() {
		_Column tark_vuosi = tarkastus.get("vuosi");
		_Column loyt_vuosi = pesa.get("loyt_vuosi");
		_Column rak_vuosi = pesa.get("rak_vuosi");
		_Column rak_vuosi_tarkkuus = pesa.get("rak_vuosi_tarkkuus");

		checkYearIfNotNull(loyt_vuosi);
		checkYearIfNotNull(rak_vuosi);

		if (kirjekyyhkyValidations && notNullNoErrors(rak_vuosi)) {
			checkNotNull(rak_vuosi_tarkkuus);
		}

		if (notNullNoErrors(tark_vuosi) && notNullNoErrors(loyt_vuosi)) {
			if (loyt_vuosi.getIntegerValue() > tark_vuosi.getIntegerValue()) {
				warning(loyt_vuosi, PetoConst.INVALID_LOYT_VUOSI);
			}
		}
		if (notNullNoErrors(tark_vuosi) && notNullNoErrors(rak_vuosi)) {
			if (rak_vuosi.getIntegerValue() > tark_vuosi.getIntegerValue()) {
				warning(rak_vuosi, PetoConst.INVALID_RAK_VUOSI);
			}
		}
		if (notNullNoErrors(loyt_vuosi) && notNullNoErrors(rak_vuosi)) {
			if (rak_vuosi.getIntegerValue() > loyt_vuosi.getIntegerValue()) {
				warning("rakv_loytv", PetoConst.INVALID_RAK_TAI_LOYT_VUOSI);
			}
		}
	}

	private void checkLomake_seurTarkastaja() {
		if (pesa.get("lomake").getValue().equals("K")) {
			checkNotNull(pesa.get("seur_tarkastaja"));
		}
	}

	private void ifHasNotBeenInspected() {
		errorIfValuesGiven(tarkastus);
		errorIfValuesGiven(params.getKaynnit().values());
	}

	private void ifHasBeenInspected() throws SQLException {
		_Column pesiva_laji = tarkastus.get("pesiva_laji");
		_Column tark_vuosi = tarkastus.get("vuosi");
		_Column pesimistulos = tarkastus.get("pesimistulos");
		_Column pesimistulos_tarkkuus = tarkastus.get("pesimistulos_tarkkuus");
		_Column epaonnistuminen = tarkastus.get("epaonnistuminen");
		_Column epaonnistuminen_tarkkuus = tarkastus.get("epaonnistuminen_tarkkuus");
		_Column loyt_vuosi = pesa.get("loyt_vuosi");
		_Column rak_vuosi = pesa.get("rak_vuosi");

		_Column jarjNro = tarkastus.get("pesinnan_jarjestysnumero");
		_Column jarjTunniste = tarkastus.get("pesinnan_yhdistava_tunniste");
		_Column jarjKommentit = tarkastus.get("kommentti_jarjestys");

		if (notNullNoErrors(pesiva_laji)) {
			if (!PetoConst.ALLOWED_NESTING_SPECIES.contains(pesiva_laji.getValue())) {
				error(pesiva_laji, PetoConst.NESTING_SPECIES_NOT_AMONG_ALLOWED_SPECIES);
			}
		}

		checkUusintapesinta(pesiva_laji, tark_vuosi, jarjNro, jarjTunniste, jarjKommentit);

		checkNotNull(pesimistulos);
		checkNotNull(pesimistulos_tarkkuus);
		if (isNull(epaonnistuminen)) {
			checkIsNull(epaonnistuminen_tarkkuus);
		} else {
			checkNotNull(epaonnistuminen_tarkkuus);
		}

		checkNotNull(tarkastus.get("pesa_sijainti"));
		checkPesivaLaji(pesiva_laji, pesimistulos);
		checkMuulaji(pesimistulos);


		_Column nestDeadColumn = checkKaynnit();
		checkNestDeadCodes(nestDeadColumn);



		if (okToCheckIfTarkastusAlreadyExists() && !kirjekyyhkyValidations) {
			checkTarkastusDoesNotAlreadyExists(pesa.getUniqueNumericIdColumn(), pesiva_laji, tark_vuosi);
		}

		Integer kaynti_vuosi = null;
		boolean hasKaynti = false;
		for (_Table kaynti : params.getKaynnit().values()) {
			if (!kaynti.hasValues()) continue;
			if (!kaynti.get("pvm").hasValue()) continue;
			hasKaynti = true;
			if (!noErrors(kaynti.get("pvm"))) continue;
			if (kaynti_vuosi == null) {
				kaynti_vuosi = year(kaynti);
			} else {
				if (!kaynti_vuosi.equals(year(kaynti))) {
					warning(kaynti.get("pvm"), PetoConst.ALL_KAYNTI_NOT_FROM_SAME_YEAR);
				}
			}
		}
		if (!hasKaynti) {
			error("yleisvirhe", "oltava_vahintaan_yksi_kaynti_jos_tarkastettu");
		}
		checkYearCombinations(tark_vuosi, loyt_vuosi, rak_vuosi, kaynti_vuosi);

		crossCheckWithHavainnot(pesimistulos);

		checkPoikastenMitatPoikastenLkm();

		_Column kuoriutumattomat = tarkastus.get("kuoriutumattomia_lkm");
		if (notNullNoErrors(kuoriutumattomat)) {
			if (!KUORIUTUMATTOMIA_SAA_ILMOITTAA.contains(pesimistulos.getValue())) {
				error(kuoriutumattomat, PetoConst.EI_SAA_ILMOITTAA_KUORIUTUMATTOMIA_TALLA_PESIMISTULOKSELLA);
			}
		}

		if (kirjekyyhkyValidations) {
			if (notNullNoErrors(pesimistulos) && pesimistulos.getValue().equals("T")) {
				warning(pesimistulos, _Validator.ARE_YOU_SURE);
			}
		}
	}

	private void checkPoikastenMitatPoikastenLkm() {
		for (_Table kaynti : params.getKaynnit().values()) {
			if (notNullNoErrors(kaynti.get("max_siipi"))) {
				if (isNull(kaynti.get("pesapoikasia_lkm")) && isNull(kaynti.get("lentopoikasia_lkm"))) {
					error(kaynti.get("max_siipi"), PetoConst.POIKASTEN_MAARA_ILMOITETTAVA_JOS_POIKASIA_MITATTU);
				}
			}
			if (notNullNoErrors(kaynti.get("max_paino"))) {
				if (isNull(kaynti.get("pesapoikasia_lkm")) && isNull(kaynti.get("lentopoikasia_lkm"))) {
					error(kaynti.get("max_paino"), PetoConst.POIKASTEN_MAARA_ILMOITETTAVA_JOS_POIKASIA_MITATTU);
				}
			}
		}
	}

	private void crossCheckWithHavainnot(_Column pesimistulos) {
		if (notNullNoErrors(pesimistulos)) {
			Collection<Havainto> esiintyvatHavainnot = selvitaEsiintyvatHavainnot();
			PesimistulosValidationDefinition validationRules = PetoConst.PESIMISTULOS_VALIDATION_DEFINITIONS.get(pesimistulos.getValue());
			for (Havainto havainto : esiintyvatHavainnot) {
				if (!validationRules.sallitut().contains(havainto)) {
					warning(havainto.toString(), PetoConst.PESIMISTULOS_JA_HAVAINTO_RISTIRIIDASSA);
				}
			}
			boolean meetsRequirements = false;
			for (Havainto vaadittu : validationRules.vaaditut()) {
				if (esiintyvatHavainnot.contains(vaadittu)) {
					meetsRequirements = true;
					break;
				}
			}
			if (validationRules.vaaditut().isEmpty()) {
				meetsRequirements = true;
			}
			if (!meetsRequirements) {
				warning(pesimistulos, PetoConst.EI_TARPEEKSI_HAVAINTOJA_PESIMISTULOKSELLE + pesimistulos.getValue());
			}
		}
	}

	private Collection<Havainto> selvitaEsiintyvatHavainnot() {
		Collection<Havainto> esiintyvatHavainnot = new HashSet<>();
		for (_Table kaynti : params.getKaynnit().values()) {
			if (!kaynti.hasValues()) continue;
			if (notNullNotZero(kaynti.get("lentopoikasia_lkm"))) {
				esiintyvatHavainnot.add(Havainto.LENTOP);
			}
			if (notNullNotZero(kaynti.get("pesapoikasia_lkm"))) {
				esiintyvatHavainnot.add(Havainto.PESAP);
			}
			if (notNullNotZero(kaynti.get("kuolleita_lkm"))) {
				esiintyvatHavainnot.add(Havainto.KUOLLEITA);
			}
			if (notNullNotZero(kaynti.get("munia_lkm"))) {
				esiintyvatHavainnot.add(Havainto.MUNIA);
			}
			if (kaynti.get("siruja").getValue().equals("K")) {
				esiintyvatHavainnot.add(Havainto.SIRUJA);
			}
			if (kaynti.get("hautova_emo").getValue().equals("K")) {
				esiintyvatHavainnot.add(Havainto.HAUTOVA);
			}
			if (notNullNotZero(kaynti.get("aikuisia_lkm"))) {
				esiintyvatHavainnot.add(Havainto.EMOJA);
			}
		}
		return esiintyvatHavainnot;
	}

	private boolean notNullNotZero(_Column c) {
		if (c.getValue().equals("0")) return false;
		return c.hasValue();
	}

	private void checkYearCombinations(_Column tark_vuosi, _Column loyt_vuosi, _Column rak_vuosi, Integer kaynti_vuosi) {
		if (kaynti_vuosi != null) {
			if (notNullNoErrors(tark_vuosi)) {
				if (tark_vuosi.getIntegerValue() < kaynti_vuosi - 1 || tark_vuosi.getIntegerValue() > kaynti_vuosi) {
					error(tark_vuosi, PetoConst.INVALID_TARK_VUOSI_TARK_PVM_YYYY);
				}
			}
			if (notNullNoErrors(loyt_vuosi)) {
				if (loyt_vuosi.getIntegerValue() > kaynti_vuosi) {
					error(loyt_vuosi, PetoConst.INVALID_KAYNTI_VUOSI_LOYT_VUOSI);
				}
			}
			if (notNullNoErrors(rak_vuosi)) {
				if (rak_vuosi.getIntegerValue() > kaynti_vuosi) {
					error(rak_vuosi, PetoConst.INVALID_KAYNTI_VUOSI_RAK_VUOSI);
				}
			}
		}
	}

	private void checkUusintapesinta(_Column pesiva_laji, _Column tark_vuosi, _Column jarjNro, _Column jarjTunniste, _Column jarjKommentit) throws SQLException {
		if (isNull(jarjNro)) {
			checkIsNull(jarjTunniste);
			checkIsNull(jarjKommentit);
		} else if (noErrors(jarjNro)) {
			checkRange(jarjNro, 1, 5);
			checkNotNull(jarjTunniste);
			checkNotNull(jarjKommentit);

			if (noErrors(jarjTunniste) && noErrors(tark_vuosi) && noErrors(pesiva_laji) && !kirjekyyhkyValidations) {
				int nro = Integer.valueOf(jarjNro.getValue());
				int count = PetoDAO.countOfPesinnanTunniste(jarjTunniste.toString(), nro, pesiva_laji.getValue(), tark_vuosi.getValue(), dao);
				if (data.action().equals(Const.UPDATE)) {
					if (count > 1) {
						error(jarjTunniste, PetoConst.NESTING_SEQUENCE_USED_MORE_THAN_ONCE);
						error(jarjNro, PetoConst.NESTING_SEQUENCE_USED_MORE_THAN_ONCE);
					}
				} else {
					if (count != 0) {
						error(jarjTunniste, PetoConst.NESTING_SEQUENCE_USED_MORE_THAN_ONCE);
						error(jarjNro, PetoConst.NESTING_SEQUENCE_USED_MORE_THAN_ONCE);
					}
				}
				if (nro > 1) {
					// Täytyy olla yksi (ja täsmälleen yksi) pesintä samalle vuodelle samalta lajilta samalla tunnisteella n-1 järjestysnumerolla
					count = PetoDAO.countOfPesinnanTunniste(jarjTunniste.toString(), nro-1, pesiva_laji.getValue(), tark_vuosi.getValue(), dao);
					if (count == 0) {
						error(jarjTunniste, PetoConst.MUST_HAVE_NESTING_SEQUENCE_MINUS_ONE);
					} else if (count > 1) {
						error(jarjTunniste, PetoConst.NESTING_SEQUENCE_USED_MORE_THAN_ONCE);
					}
				}
			}
		}
	}

	private void checkNestDeadCodes(_Column nestDeadColumn) {
		//		_Column epaonnistum = tarkastus.get("epaonnistuminen");
		//		if (epaonnistum.getValue().equals(NEST_DEAD)) {
		//			if (nestDeadColumn == null) {
		//				error(epaonnistum, PetoConst.PESA_ON_ILMOITETTAVA_TUHOUTUNEEKSI_TATA_EPAONNI_KOODIA_KAYTETTAESSA);
		//			}
		//		}
		if (nestDeadColumn != null) {
			_Column pesa_sijainti = tarkastus.get("pesa_sijainti");
			if (nestDeadColumn.getValue().equals(TREE_HAS_FALLEN) || nestDeadColumn.getValue().equals(TREE_HAS_BEEN_CUT_DOWN)) {
				if (NESTS_NOT_IN_TREE.contains(pesa_sijainti.getValue())) {
					error(pesa_sijainti, PetoConst.THIS_NEST_STATUS_CODE_CAN_BE_USED_ONLY_FOR_NESTS_THAT_ARE_IN_TREE);
					error(nestDeadColumn, PetoConst.THIS_NEST_STATUS_CODE_CAN_BE_USED_ONLY_FOR_NESTS_THAT_ARE_IN_TREE);
				}
			} else if (nestDeadColumn.getValue().equals(GROUND_NEST_HAS_BECOME_UNSUITABLE)) {
				if (!NESTS_NOT_IN_TREE.contains(pesa_sijainti.getValue())) {
					error(pesa_sijainti, PetoConst.THIS_NEST_STATUS_CODE_CAN_BE_USED_ONLY_FOR_NESTS_THAT_ARE_ON_GROUND);
					error(nestDeadColumn, PetoConst.THIS_NEST_STATUS_CODE_CAN_BE_USED_ONLY_FOR_NESTS_THAT_ARE_ON_GROUND);
				}
			}
		}
	}

	private void checkMuulaji(_Column pesimistulos) {
		_Column muulaji = tarkastus.get("muu_laji");
		if (pesimistulos.getValue().equals(MUU_LAJI_PESIMISTULOS)) {
			checkNotNull(muulaji);
		} else {
			checkIsNull(muulaji);
			if (!isNull(muulaji)) {
				warning(muulaji, PetoConst.MUULAJI_PITAA_OLLA_PESIMISTULOS_X);
			}
		}
	}

	private void checkPesivaLaji(_Column pesiva_laji, _Column pesimistulos) {
		if (notNullNoErrors(pesimistulos)) {
			if (PESIMISTULOS_PESIVA_LAJI_MUST_NOT_BE_GIVEN.contains(pesimistulos.getValue())) {
				if (!isNull(pesiva_laji)) {
					error(pesiva_laji, PetoConst.PESIVA_LAJI_MUST_BE_EMPTY_WITH_THIS_PESIMISTULOS);
				}
			}
			if (PESIMISTULOS_PESIVA_LAJI_MUST_BE_GIVEN.contains(pesimistulos.getValue())) {
				if (isNull(pesiva_laji)) {
					error(pesiva_laji, PetoConst.PESIVA_LAJI_REQUIRED_WITH_THIS_PESIMISTULOS);
				}
			}
		}
	}

	private _Column checkKaynnit() {
		_Column nestDeadColumn = null;
		boolean hasAtLesatOneKunto = false;
		for (_Table kaynti : params.getKaynnit().values()) {
			if (!hasValuesForKaynti(kaynti)) continue;

			_Column siipi = kaynti.get("max_siipi");
			_Column mittaustapa = kaynti.get("max_siipi_mittaustapa");
			if (isNull(siipi)) {
				checkIsNull(mittaustapa);
			} else {
				checkNotNull(mittaustapa);
			}

			_Column munia = kaynti.get("munia_lkm");
			_Column pesapoik = kaynti.get("pesapoikasia_lkm");
			_Column kuolleita = kaynti.get("kuolleita_lkm");
			_Column lentopoik = kaynti.get("lentopoikasia_lkm");

			checkCountAndAccuracy(kaynti.get("aikuisia_lkm"), kaynti.get("aikuisia_lkm_tarkkuus"));
			checkCountAndAccuracy(munia, kaynti.get("munia_lkm_tarkkuus"));
			checkCountAndAccuracy(pesapoik, kaynti.get("pesapoikasia_lkm_tarkkuus"));
			checkCountAndAccuracy(kuolleita, kaynti.get("kuolleita_lkm_tarkkuus"));
			checkCountAndAccuracy(lentopoik, kaynti.get("lentopoikasia_lkm_tarkkuus"));

			_Column pesivaLaji = data.updateparameters().getTarkastus().get("pesiva_laji");
			if (pesivaLaji.hasValue()) {
				if (rajat.containsKey(pesivaLaji.getValue())) {
					int pesyekoko = fi.hy.pese.framework.main.general.Utils.sum(munia, pesapoik, kuolleita, lentopoik);
					if (pesyekoko > rajat.get(pesivaLaji.getValue()).maxPesyekoko) {
						warning(PetoConst.COUNTS, PetoConst.LIIAN_ISO_PESYE);
					}
				} else {
					error(pesivaLaji, PetoConst.EI_RAJA_ARVOA);
				}
			}

			_Column kunto = kaynti.get("pesa_kunto");

			if (notNullNoErrors(kunto)) {
				hasAtLesatOneKunto = true;
			}
			if (PetoConst.DEAD_NEST_CODES.contains(kunto.getValue())) {
				nestDeadColumn = kunto;
			}

			if (kirjekyyhkyValidations && (munia.hasValue() || pesapoik.hasValue() || kuolleita.hasValue())) {
				checkNotNull(kunto);
			} else {
				warningIfNull(kunto);
			}

			if (PetoConst.DEAD_NEST_CODES.contains(kunto.getValue())) {
				warningIfNull(kaynti.get("pvm"));
				warningIfNull(kaynti.get("pvm_tarkkuus"));
			} else {
				checkNotNull(kaynti.get("pvm"));
				checkNotNull(kaynti.get("pvm_tarkkuus"));
			}
		}
		if (kirjekyyhkyValidations && !hasAtLesatOneKunto) {
			error("kaynti.kunto", PetoConst.AINAKIN_YKSI_KUNTO_ILMOITETTAVA);
		}
		return nestDeadColumn;
	}

	private void checkCountAndAccuracy(_Column arvo, _Column tarkkuus) {
		if (kirjekyyhkyValidations) {
			checkValueAndAccuracyPair(arvo, tarkkuus, ACCURACY_REQUIRED);
		} else {
			checkValueAndAccuracyPair(arvo, tarkkuus, ACCURACY_NOT_REQUIRED);
		}
		if (notNullNoErrors(arvo)) {
			checkRange(arvo, 0, 20);
		}
	}

	private boolean okToCheckIfTarkastusAlreadyExists() {
		return noErrors(tarkastus.get("vuosi")) && noErrors(pesa.getUniqueNumericIdColumn()) && !isNull(tarkastus.get("pesiva_laji")) && doingInsert();
	}

	private void checkIds() {
		String action = data.action();
		if (!action.equals(Const.INSERT_FIRST)) {
			if (!kirjekyyhkyValidations) {
				checkNotNull(pesa.getUniqueNumericIdColumn());
			}
		} else {
			checkIsNull(pesa.getUniqueNumericIdColumn());
		}
		if (action.equals(Const.UPDATE)) {
			checkNotNull(tarkastus.getUniqueNumericIdColumn());
		}
	}

	private Integer year(_Table kaynti) {
		return Integer.valueOf( kaynti.get("pvm").getDateValue().getYear() );
	}

	private boolean hasValuesForKaynti(_Table kaynti) {
		for (_Column c : kaynti) {
			if (c.getFieldType() == _Column.ROWID) continue;
			if (c.getName().equals("id")) continue;
			if (c.getName().equals("tarkastus")) continue;
			if (c.hasValue()) {
				return true;
			}
		}
		return false;
	}

	private boolean hasBeenInspected(_Table tarkastus) {
		return "K".equals(tarkastus.get("tarkastettu").getValue());
	}

	private void errorIfValuesGiven(Collection<_Table> kaynnit) {
		for (_Table kaynti :kaynnit) {
			errorIfKayntiValuesGiven(kaynti);
		}
	}

	private void errorIfKayntiValuesGiven(_Table kaynti) {
		for (_Column c : kaynti) {
			if (c.isRowIdColumn()) continue;
			if (c.getName().equals("id")) continue;
			if (c.getName().equals("tarkastus")) continue;
			if (c.hasValue()) {
				error(c, PetoConst.NOT_INSPECTED_BUT_VALUES_GIVEN);
			}
		}
	}

	private void errorIfValuesGiven(_Table tarkastus) {
		for (_Column c : tarkastus) {
			if (c.isRowIdColumn()) continue;
			if (c.getName().equals("id")) continue;
			if (c.getName().equals("pesa")) continue;
			if (c.getName().equals("vuosi")) continue;
			if (c.getName().equals("tarkastettu")) continue;
			if (c.getName().equals("tarkastaja")) continue;
			if (c.getName().equals("ref_vanhaan_dataan")) continue;
			if (c.getName().equals("oli_nipussa")) continue;
			if (PetoConst.TARKASTUS_FIELDS_TO_BE_PREFILLED_FOR_INSERTS.contains(c.getName())) continue;
			if (c.hasValue()) {
				error(c, PetoConst.NOT_INSPECTED_BUT_VALUES_GIVEN);
			}
		}
	}

	private void checkTarkastusDoesNotAlreadyExists(_Column pesa_id, _Column laji, _Column tark_vuosi) throws SQLException {
		boolean tarkastusAlreadyExists = PetoDAO.checkTarkastusForTheYearExists(pesa_id.getValue(), tark_vuosi.getValue(), laji.getValue(), dao);
		if (tarkastusAlreadyExists) {
			error(tark_vuosi, PetoConst.INSPECTION_FOR_YEAR_ALREADY_EXISTS);
		}
	}

	private boolean doingInsert() {
		return (data.action().equals(Const.INSERT) || data.action().equals(Const.FILL_FOR_INSERT));
	}


	//		_Column pesimistulos = params.getColumn("pesatarkastus.pesimistulos");
	//		_Column elavia_lkm = params.getColumn("pesatarkastus.elavia_lkm");
	//		_Column reng_poik_lkm = params.getColumn("pesatarkastus.reng_poik_lkm");
	//		_Column lentopoik_lkm = params.getColumn("pesatarkastus.lentopoik_lkm");
	//		if (MerikotkaConst.PESIMISTULOS_POIKANEN_NOT_ALLOWED.contains(pesimistulos.getValue())) {
	//			if (poikasetHasValues(params)) {
	//				error(pesimistulos, MerikotkaConst.INVALID_PESIMISTULOS_POIKASET);
	//			}
	//			checkIsNullOrZero(elavia_lkm);
	//			checkIsNullOrZero(reng_poik_lkm);
	//			checkIsNullOrZero(lentopoik_lkm);
	//		}
	//

	@SuppressWarnings("unused")
	private void checkIsNullOrZero(_Column c) {
		if (noErrors(c)) {
			if (!isNullOrZero(c)) {
				error(c, "INVALID_PESIMISTULOS_POIKASET");
			}
		}
	}

	private void checkYearIfNotNull(_Column c) {
		if (notNullNoErrors(c)) checkValidYear(c);
	}

	private void checkCoordinates() throws Exception {
		String koord_tyyppi = params.getColumn("pesa.koord_ilm_tyyppi").getValue();
		if (isNull(koord_tyyppi)) return;
		_Column leveys = null, pituus = null;
		int leveys_min, leveys_max, pituus_min, pituus_max;
		CoordinateConverter converter = new CoordinateConverter();
		boolean okToCheckInsideReagion = false;

		if (koord_tyyppi.equals("Y")) {
			leveys = params.getColumn("pesa.yht_leveys");
			pituus = params.getColumn("pesa.yht_pituus");
			leveys_min = _Validator.YHT_LEVEYS_MIN;
			leveys_max = _Validator.YHT_LEVEYS_MAX;
			pituus_min = _Validator.YHT_PITUUS_MIN;
			pituus_max = _Validator.YHT_PITUUS_MAX;
			checkCoordinateRanges(leveys, pituus, leveys_min, leveys_max, pituus_min, pituus_max);
			if (noErrors(leveys) && noErrors(pituus)) {
				converter.setYht_leveys(leveys.getValue());
				converter.setYht_pituus(pituus.getValue());
				okToCheckInsideReagion = true;
			}
		} else if (koord_tyyppi.equals("E")) {
			leveys = params.getColumn("pesa.eur_leveys");
			pituus = params.getColumn("pesa.eur_pituus");
			leveys_min = _Validator.EUR_LEVEYS_MIN;
			leveys_max = _Validator.EUR_LEVEYS_MAX;
			pituus_min = _Validator.EUR_PITUUS_MIN;
			pituus_max = _Validator.EUR_PITUUS_MAX;
			checkCoordinateRanges(leveys, pituus, leveys_min, leveys_max, pituus_min, pituus_max);
			if (noErrors(leveys) && noErrors(pituus)) {
				converter.setEur_leveys(leveys.getValue());
				converter.setEur_pituus(pituus.getValue());
				okToCheckInsideReagion = true;
			}
		} else if (koord_tyyppi.equals("A")) {
			leveys = params.getColumn("pesa.ast_leveys");
			pituus = params.getColumn("pesa.ast_pituus");
			leveys_min = _Validator.AST_LEVEYS_MIN;
			leveys_max = _Validator.AST_LEVEYS_MAX;
			pituus_min = _Validator.AST_PITUUS_MIN;
			pituus_max = _Validator.AST_PITUUS_MAX;
			checkCoordinateRanges(leveys, pituus, leveys_min, leveys_max, pituus_min, pituus_max);
			if (noErrors(leveys) && noErrors(pituus)) {
				converter.setAst_leveys(leveys.getValue());
				converter.setAst_pituus(pituus.getValue());
				okToCheckInsideReagion = true;
			}
		} else {
			error("pesa.koord_ilm_tyyppi", _Validator.INVALID_VALUE);
		}

		if (okToCheckInsideReagion && noErrors("pesa.kunta")) {
			checkInsideReagion(params.getColumn("pesa.kunta").getValue(), leveys, pituus, converter);
		}
	}



}
