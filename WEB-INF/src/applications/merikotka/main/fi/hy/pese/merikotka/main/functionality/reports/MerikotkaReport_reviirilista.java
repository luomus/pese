package fi.hy.pese.merikotka.main.functionality.reports;

import java.util.ArrayList;
import java.util.List;

import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.general.ReportWriter;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.luomus.commons.tipuapi.TipuAPIClient;

/**
 * Example: Note, only first three columns (kunta,reviiri_id,reviirinimi) are filled, rest are just empty fields
 * AJETTU: |24.05.2010||PARAMETREILLA: |suuralue:|E|
 * r_kunta|reviiri_id|reviiri|pesimistulos|hukassa|eläviä_pull|reng_pull|pesä_id|tark_pvm|tarkastaja|kommentti|
 * KUUSAM|396|Kangerjärvi|||||||||
 * KUUSAM|374|Kuusijärvi|||||||||
 * KUUSAM|375|Käsmäjärvi|||||||||
 * KUUSAM|79|Laajusjärvi|||||||||
 * KUUSAM|414|Maivavaara|||||||||
 * KUUSAM|73|Nuottivaara|||||||||
 * KUUSAM|324|Papuvaara|||||||||
 * KUUSAM|348|Pesosjärvi|||||||||
 * KUUSAM|74|Porosaari|||||||||
 * POSIO|78|Hankaanjärvet|||||||||
 * RANUA|75|Tervonkangas|||||||||
 * SSALMI|321|Säynäjäsuo|||||||||
 * TAIVAL|72|Kulmalampi|||||||||
 * TAIVAL|71|Mustavaara|||||||||
 */
public class MerikotkaReport_reviirilista extends ReportWriter<_PesaDomainModelRow> {

	private static final String[]	FIELDS	= {
	                             	      	   "vuosi.reviirin_kunta", "reviiri.reviiri_id", "reviiri.reviirinimi", "pesatarkastus.pesimistulos"
	};

	public MerikotkaReport_reviirilista(_Request<_PesaDomainModelRow> request, String filenamePrefix) {
		super(request, filenamePrefix);
	}

	@Override
	public boolean produceSpecific() {
		headerLine();
		newLine();
		newLine();
		columnDescriptions();
		newLine();
		newLine();
		rows();
		return true;
	}

	private void rows() {
		List<Reviiri> reviiriRows = new ArrayList<>();
		Reviiri currentReviiri = null;

		for (_PesaDomainModelRow row : request.data().results()) {
			String reviiri_id = row.getReviiri().getUniqueNumericIdColumn().getValue();
			if (reviiriChanges(currentReviiri, reviiri_id)) {
				currentReviiri = new Reviiri(row);
				reviiriRows.add(currentReviiri);
			}
		}

		for (Reviiri reviiri : reviiriRows) {
			write(reviiri);
			separator(8);
			newLine();
		}
	}

	private void columnDescriptions() {
		for (String field : FIELDS) {
			write(shortNameFromUiTexts(field));
			separator();
		}
		write("hukassa|eläviä_pull|reng_pull|pesä_id|tark_pvm|tarkastaja|kommentti|");
	}

	private void write(Reviiri reviiri) {
		write(reviiri.kunta());
		separator();
		write(reviiri.id());
		separator();
		write(reviiri.nimi());
		separator();
	}

	private boolean reviiriChanges(Reviiri currentReviiri, String reviiri_id) {
		return currentReviiri == null || !reviiri_id.equals(currentReviiri.id());
	}

	private class Reviiri {
		private final String	id;
		private final String	nimi;
		private final String	kunta;

		public Reviiri(_PesaDomainModelRow row) {
			this.id = row.getReviiri().getUniqueNumericIdColumn().getValue();
			this.nimi = row.getReviiri().get("reviirinimi").getValue();
			this.kunta = row.getVuosi().get("reviirin_kunta").getValue();
		}

		public String id() {
			return id;
		}

		public String nimi() {
			return nimi;
		}

		public String kunta() {
			return request.data().selections().get(TipuAPIClient.MUNICIPALITIES).get(kunta);
		}
	}

}
