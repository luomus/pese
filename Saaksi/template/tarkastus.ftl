
<#include "header.ftl">

<#if (data.kirjekyyhky!"") == "yes">
  <h1>${texts["tarkastus_title_kirjekyyhky_"+system.action]}</h1>
<#else>
  <h1>${texts["tarkastus_title_"+system.action]}</h1>
</#if>

<#if system.action != "insert_first">
<table>
  <tr>
  
    <#if results?has_content>
    	<td>
        	<form action="${system.baseURL}" method="post">
        	<input type="hidden" name="page"   value="${system.page}" />
        	<input type="hidden" name="action" value="fill_for_update" />
           	Aikaisemmat pesintävuodet:
           	<select name="updateparameters.tarkastus.tarkastusid">
              <#list results as i> 
                <#if i.tarkastus.tarkastusid == updateparameters.tarkastus.tarkastusid>
		          <option value="${i.tarkastus.tarkastusid}" selected="selected">${i.tarkastus.vuosi} - ${i.tarkastus.pesimistulos} ${i.tarkastus.pesimist_tark} ${i.tarkastus.kunto}</option>
		        <#else>
		          <option value="${i.tarkastus.tarkastusid}" >${i.tarkastus.vuosi} - ${i.tarkastus.pesimistulos} ${i.tarkastus.pesimist_tark} ${i.tarkastus.kunto}</option>
  		        </#if>
		      </#list>
           </select>
           <input type="submit" value="Tarkastele" />
        	</form>
    	</td>
    </#if>
    
    <#if system.action == "update">
    <td>
        <form action="${system.baseURL}" method="post">
        <input type="hidden" name="page" value="${system.page}" />
        <input type="hidden" name="action" value="fill_for_insert" />
        <input type="hidden" name="updateparameters.pesa.pesaid" value="${updateparameters.pesa.pesaid}" />
        
        Uusi tarkastus vuodelle:
        <input type="text" size="4" name="tark_vuosi" value="${data.tark_vuosi!""}" <@checkerror errors "tark_vuosi" /> /> 
        <input type="submit" value="Etene" />

        </form>
    </td>
    </#if>
    
    <#if updateparameters.pesa.pesaid != "" && !data.kirjekyyhky??>
    	<td> 
      		<a target="_karttapaikka" href="https://asiointi.maanmittauslaitos.fi/karttapaikka/?share=customMarker&n=${updateparameters.pesa.eur_leveys}&amp;e=${updateparameters.pesa.eur_pituus}&amp;title=${updateparameters.pesa.pesanimi.getUrlEncodedValue()}(${updateparameters.pesa.pesaid})">Lisää pesä kartalle</a> 
    	</td>
    </#if>
    
  </tr>
</table>
</#if>  
    
  


<@statustexts system texts />
<@errorlist errors texts />
<@warninglist warnings texts />

<#if lists.ristiriidat??>
  <br />
  <b>Pesät joille on onnistunut tarkastus tälle vuodelle: &nbsp; &nbsp;</b> 
  <#list lists.ristiriidat as pesaid>
    <a style="color:red" href="${system.baseURL}?page=tarkastus&amp;action=fill_for_update&amp;updateparameters.pesa.pesaid=${pesaid}&amp;updateparameters.tarkastus.vuosi=${updateparameters.tarkastus.vuosi}">&nbsp;${pesaid}&nbsp;</a>
     &nbsp;&nbsp;
  </#list>
</#if>

<div id="tarkastuslomake">

<#if lists.log??>
   <table style="width: 50%">
     <caption>Päivitysloki</caption>
     <tr>
       <th>Aika</th>
       <th>Käyttäjä</th>
       <th>Toiminto</th>
     </tr>
     <#list lists.log as e>
       <tr>
         <td>${e.time}</td>
         <td> <#if selections['ringers'].containsOption(e.user)>${selections['ringers'][e.user]}<#else>${e.user}</#if></td>
         <td>${e.action}</td>
       </tr>
     </#list>
   </table>
   <br />
 </#if>
 
 <#if (data.kirjekyyhky!"") == "yes">
 <form action="${system.baseURL}" method="post" onsubmit="return false;" name="palautalomake">
   <input type="hidden" name="page" value="kirjekyyhky_management" />
   <input type="hidden" name="action" value="return_for_corrections" />
   <input type="hidden" name="id" value="${data.id!""}" />
   <table style="width: 50%">
     <caption>Palauta lomake korjattavaksi</caption>
     <tr>
       <td style="vertical-align: middle;"> Viesti </td>
       <td> <textarea name="return_for_corrections_message" rows="2" cols="50"></textarea> </td>
       <td  style="vertical-align: middle;"> <button onclick="document.palautalomake.submit()">Palauta</button> </td>
     </tr>
   </table>
   </form>
 </#if>
 
<form action="${system.baseURL}" method="post" onsubmit="return false;" name="tarkastusForm">
<input type="hidden" name="page" value="${system.page}" />
<input type="hidden" name="action" value="${system.action}" />
<input type="hidden" name="kirjekyyhky" value="${data.kirjekyyhky!""}" />
<input type="hidden" name="id" value="${data.id!""}" />
<input type="hidden" name="bypassed_warnings" id="bypassed_warnings" value="${data.bypassed_warnings!""}" /> 
<@inputHidden "updateparameters" updateparameters.vuosi.rowid />
<@inputHidden "updateparameters" updateparameters.olosuhde.rowid />
<@inputHidden "updateparameters" updateparameters.pesa.rowid />
<@inputHidden "updateparameters" updateparameters.pesa.pesaid />
<@inputHidden "updateparameters" updateparameters.tarkastus.rowid />
<@inputHidden "updateparameters" updateparameters.tarkastus.tarkastusid />
<@inputHidden "updateparameters" updateparameters.getColumn("poikanen.1.rowid") />
<@inputHidden "updateparameters" updateparameters.getColumn("poikanen.2.rowid") />
<@inputHidden "updateparameters" updateparameters.getColumn("poikanen.3.rowid") />
<@inputHidden "updateparameters" updateparameters.getColumn("poikanen.4.rowid") />
<@inputHidden "updateparameters" updateparameters.getColumn("kaynti.1.rowid") />
<@inputHidden "updateparameters" updateparameters.getColumn("kaynti.2.rowid") />
<@inputHidden "updateparameters" updateparameters.getColumn("kaynti.3.rowid") />
<@inputHidden "updateparameters" updateparameters.getColumn("kaynti.4.rowid") />

 <table class="noborders sameAsBackground">
   <tr>
     <td>
        <h2>
        <!-- Toiminnon otsikko -->
        <#if system.action == "insert">
          <img src="${system.commonURL}/insert.gif" alt="Lisää" /> Uusi tarkastus: &nbsp;&nbsp; pesä: ${updateparameters.pesa.pesaid} &nbsp;&nbsp; pesintävuosi: ${updateparameters.tarkastus.vuosi}
        </#if>
        <#if system.action == "update">
          <img src="${system.commonURL}/update.gif" alt="Muokkaa" /> Tarkastuksen muokkaus: &nbsp;&nbsp; pesä: ${updateparameters.pesa.pesaid} &nbsp;&nbsp; pesintävuosi: ${updateparameters.tarkastus.vuosi}
        </#if>
        <#if system.action == "insert_first">
          <img src="${system.commonURL}/insert.gif" alt="Uusi" /> Uuden pesän lisäys
        </#if>
        </h2>
    </td> 
    <td style="vertical-align: middle;">
      <#if (data.kirjekyyhky!"") == "yes">
        <button onclick="document.tarkastusForm.submit()">${texts["tarkastus_submit_kirjekyyhky_"+system.action]}</button>
      <#else>
        <button onclick="document.tarkastusForm.submit()">${texts["tarkastus_submit_"+system.action]}</button>
      </#if>
    </td>
  </tr>
</table>

<div style="float: left;">

<table>
  <caption>Tarkastaja</caption>
  <tr>
    <th> Tarkastaja v. ${updateparameters.tarkastus.vuosi}</th> <td> <@inputSelectCombo "updateparameters" updateparameters.tarkastus.rengastaja true false errors /> </td>
  </tr>
  <tr>
    <th>Lomake</th>
    <td> 
    	<select name="updateparameters.pesa.lomake" size="2" >
    		<option value="K" <#if updateparameters.pesa.lomake == "" || updateparameters.pesa.lomake == "K"> selected="selected" </#if> >Kyllä</option>
    		<option value="E" <#if updateparameters.pesa.lomake == "E"> selected="selected" </#if> >Ei</option>
    	</select>
    	<@compare updateparameters.pesa.lomake />
    </td>
  </tr>
  <tr>
    <th> Seuraava tarkastaja </th> <td> <@inputSelectCombo "updateparameters" updateparameters.pesa.seur_rengastaja true false errors />  <@compare updateparameters.pesa.seur_rengastaja /> </td>
  </tr>
  <tr>
    <th>Muut vastaanottajat</th>
  	<td id="lomake_vastaanottajat">
		<div class="henkilot">
			<#list lists["lomake_vastaanottajat"] as vastaanottaja>
				<input name="lomake_vastaanottaja" value="${vastaanottaja}"/>
			</#list>
		</div>
		<button onclick="addVastaanottaja(); return false;"> + Lisää</button>
	</td>
  </tr>
</table>

<table>
	<caption>Perustiedot pesästä</caption>
	<tr>
		<th>Id</th> <td> ${updateparameters.pesa.pesaid} </td>
	</tr>
	<tr>
		<th>Vanha numero</th> <td> <@inputText "updateparameters" updateparameters.pesa.vanha_pesaid true errors /> </td>
	</tr>
	<tr>
		<th>Pesän kunta</th> <td> <@inputSelectCombo "updateparameters" updateparameters.pesa.kunta true false errors />  <@compare updateparameters.pesa.kunta /> </td>
	</tr>
	<tr>
		<th>Kylä (tms.)</th> <td> <@inputText "updateparameters" updateparameters.pesa.kyla true errors /> <@compare updateparameters.pesa.kyla /> </td> 
	</tr>
	<tr>
		<th>Pesän nimi</th> <td> <@inputText "updateparameters" updateparameters.pesa.pesanimi true errors /> <@compare updateparameters.pesa.pesanimi /> </td> 
	</tr>
	<tr>
		<th>Reviiri</th> 
		<td>
			<#if (data.kirjekyyhky!"") == "yes" && updateparameters.vuosi.reviiriid == "">
            	<@inputSelectCombo "updateparameters" comparisonparameters.vuosi.reviiriid true true errors />  <@compare updateparameters.reviiri.reviirinimi /> </td>
            <#else>
            	<@inputSelectCombo "updateparameters" updateparameters.vuosi.reviiriid true true errors />   
            </#if>
		</td>
	</tr>
</table>

<table>
<caption>Koordinaatit</caption>
<tr>
<td>
<table>
  <tr>
    <th> &nbsp; </th>  
    <th> Leveys </th>
    <th> Pituus </th>
  </tr>
  <tr>
    <th>Yhtenäis</th>
    <td> <@inputText "updateparameters" updateparameters.pesa.yht_leveys true errors /> <@compare updateparameters.pesa.yht_leveys /> </td>
    <td> <@inputText "updateparameters" updateparameters.pesa.yht_pituus true errors /> <@compare updateparameters.pesa.yht_pituus /> </td>
  </tr>
  <tr>
    <th>Euref</th>
    <td> <@inputText "updateparameters" updateparameters.pesa.eur_leveys true errors /> <@compare updateparameters.pesa.eur_leveys /></td>
    <td> <@inputText "updateparameters" updateparameters.pesa.eur_pituus true errors /> <@compare updateparameters.pesa.eur_pituus /></td>
  </tr>
  <tr>
    <th>Aste</th>
    <td> <@inputText "updateparameters" updateparameters.pesa.ast_leveys true errors /> <@compare updateparameters.pesa.ast_leveys /></td>
    <td> <@inputText "updateparameters" updateparameters.pesa.ast_pituus true errors /> <@compare updateparameters.pesa.ast_pituus /></td>
  </tr>
</table>
</td>
<td>
<table>
  <tr>
    <th> Tyyppi / tarkkuus </th>
  </tr>
  <tr>
    <td> <@selection selections "updateparameters" updateparameters.pesa.koord_tyyppi 2 false false false errors /> <@compare updateparameters.pesa.koord_tyyppi /></td>
  </tr>
</table>
</td>
<td>
<table>
  <tr>
    <th> Mittaustapa </th>
  </tr>
  <tr>
    <td> <@selection selections "updateparameters" updateparameters.pesa.koord_mittaustapa 2 false false false errors /> <@compare updateparameters.pesa.koord_mittaustapa /></td>
  </tr>
</table>
</td>
</tr>
</table>


<table>
  <tr>
    <th> Kommentit pesästä </th>
  </tr>
  <tr>
    <td> <@textarea "updateparameters" updateparameters.pesa.kommentti 3 100 errors /> <@compare updateparameters.pesa.kommentti />  </td>
  </tr>
</table>

<table>
	<caption>Pesäpaikan ja pesän kuvaus (${updateparameters.olosuhde.ilm_pvm})</caption>
	
	<tr>
		<th>Maastotyyppi</th>  
		<th>Käsittelyaste</th>
		<th>Pesän sijainti/alusta</th>
		<th>Pesäpuun laji</th>
	</tr>
	<tr>
		<td> <@selection selections "updateparameters" updateparameters.olosuhde.maastotyyppi 2 false true true errors /> <@compare updateparameters.olosuhde.maastotyyppi /></td>
		<td> <@selection selections "updateparameters" updateparameters.olosuhde.puust_kasaste 2 false true true errors /> <@compare updateparameters.olosuhde.puust_kasaste /></td>
		<td rowspan="3"> <@selection selections "updateparameters" updateparameters.olosuhde.pesan_sijainti 2 false true true errors /> <@compare updateparameters.olosuhde.pesan_sijainti /></td>
		<td> <@selection selections "updateparameters" updateparameters.pesa.pesap_laji 2 false true true errors /> <@compare updateparameters.pesa.pesap_laji /></td>
	</tr>
	<tr>
		<th>Lähiympäristön puusto</th>  
		<th>Puuston tiheys</th>
		<th>Pesäpuun elävyys</th>
	</tr>
	<tr>
		<td> <@selection selections "updateparameters" updateparameters.olosuhde.puusto 2 false true true errors /> <@compare updateparameters.olosuhde.puusto /></td>
		<td> <@selection selections "updateparameters" updateparameters.olosuhde.puust_tiheys 2 false true true errors /> <@compare updateparameters.olosuhde.puust_tiheys /></td>
		<td> <@selection selections "updateparameters" updateparameters.olosuhde.pesap_elavyys 2 false true true errors /> <@compare updateparameters.olosuhde.pesap_elavyys /></td>
	</tr>
</table>

<table>
	<tr>
		<th>Rauhoitustaulu</th> <td> <@selection selections "updateparameters" updateparameters.olosuhde.rauhoitustaulu 2 false true true errors /> <@compare updateparameters.olosuhde.rauhoitustaulu /></td>
	</tr>
</table>


	<table>
		<tr>
			<th colspan="4">Pesäpuun korkeus</th>
		</tr>
		<tr>
			<td><@inputText "updateparameters" updateparameters.olosuhde.pesap_korkeus true errors /> <@compare updateparameters.olosuhde.pesap_korkeus /> m</td>
			<td><@selection selections "updateparameters" updateparameters.olosuhde.pesap_kork_tark 2 false true true errors /> <@compare updateparameters.olosuhde.pesap_kork_tark /></td>
			<td colspan="2"> </td> 
		</tr>
		<tr>
			<th colspan="4">Pesäpuun latvan mitat (pesän alapuolelta)</th>
		</tr>
		<tr>
			<th colspan="2">Halkaisija</th>
			<th colspan="2">Ympärys</th>
		</tr>
		<tr>
			<td><@inputText "updateparameters" updateparameters.olosuhde.pesap_latvahalk true errors /> <@compare updateparameters.olosuhde.pesap_latvahalk /> cm </td>
			<td><@selection selections "updateparameters" updateparameters.olosuhde.pesap_latvah_tark 2 false true true errors /> <@compare updateparameters.olosuhde.pesap_latvah_tark /></td>

			<td><@inputText "updateparameters" updateparameters.olosuhde.pesap_latvaymp true errors /> <@compare updateparameters.olosuhde.pesap_latvaymp /> cm </td>
			<td><@selection selections "updateparameters" updateparameters.olosuhde.pesap_latvay_tark 2 false true true errors /> <@compare updateparameters.olosuhde.pesap_latvay_tark /></td>
		</tr>
		<tr>
			<th colspan="4">Pesäpuun tyven mitat (1m korkeudelta)</th>
		</tr>
		<tr>
			<th colspan="2">Halkaisija</th>
			<th colspan="2">Ympärys</th>
		</tr>
		<tr>
			<td><@inputText "updateparameters" updateparameters.olosuhde.pesap_tyvihalk true errors /> <@compare updateparameters.olosuhde.pesap_tyvihalk /> cm </td>
			<td><@selection selections "updateparameters" updateparameters.olosuhde.pesap_tyvih_tark 2 false true true errors /> <@compare updateparameters.olosuhde.pesap_tyvih_tark /></td>

			<td><@inputText "updateparameters" updateparameters.olosuhde.pesap_tyviymp true errors /> <@compare updateparameters.olosuhde.pesap_tyviymp /> cm </td>
			<td><@selection selections "updateparameters" updateparameters.olosuhde.pesap_tyviy_tark 2 false true true errors /> <@compare updateparameters.olosuhde.pesap_tyviy_tark /></td>
		</tr>
	</table>

<table>
	<tr>
		<td>Kommentit olosuhteista</td>
	</tr>
	<tr>
    	<td> <@textarea "updateparameters" updateparameters.olosuhde.kommentti 3 100 errors /> <@compare updateparameters.olosuhde.kommentti />  </td>
  </tr>
</table>



</div>

<div style="white-space: nowrap;">
	<#if results?has_content>
    	<table class="infoContainer">
      	  <caption> Pesän historia </caption>
          <tr>
            <th>Vuosi</th>
            <th>Tarkastaja</th> 
            <th>Pesimistulos</th>
          </tr>
          <#list results as i>
            <tr>
              <td><a href="${system.baseURL}?page=tarkastus&amp;action=fill_for_update&amp;updateparameters.tarkastus.tarkastusid=${i.tarkastus.tarkastusid}"> ${i.tarkastus.vuosi} </a></td>
              <td>${selections[i.tarkastus.rengastaja.assosiatedSelection][i.tarkastus.rengastaja.value]}</td>  
              <td>${i.tarkastus.pesimistulos} ${i.tarkastus.pesimist_tark} ${i.tarkastus.kunto}</td>
            </tr> 
          </#list>
    	</table>
	</#if>
	
	<br />
	
	<div style="display: inline-block;">
		<div id="lahiymparistonPesat"> Lataa... </div>
		
		<table class="infoContainer">
			<tr>
	    		<td colspan="4">
            		Säde:
            		<input type="text" name="CoordinateSearchRadius" value="${data.CoordinateSearchRadius!"500" }" size="7" tabindex="6666" /> m 
            		<button onclick="getLahiymparistonPesat();" tabindex="6667"> Hae </button> 
	         	</td>
	    	</tr>
		</table>
	</div>
	
</div>

<div  style="clear: both;"> </div>


<h2>Pesinnän tulos</h2>

<table>
	<tr>
		<th>Pesinnän vuosi</th>
		<td>
			<#if system.action != "insert_first">
				<@inputHidden "updateparameters" updateparameters.tarkastus.vuosi /> ${updateparameters.tarkastus.vuosi}
			<#else>
				<@inputText "updateparameters" updateparameters.tarkastus.vuosi true errors />
			</#if> 
		</td>
		<td rowspan="2"> <@selection selections "updateparameters" updateparameters.tarkastus.tark_pvm_tark 2 false true true errors /> </td> 
	</tr>
	<tr>
		<th>Tarkastuskäynnin pvm</th> 
		<td> <@date "updateparameters" updateparameters.tarkastus.tark_pvm errors /> </td>
	</tr>
</table>

	
	
  <table class="kayntitaulu">
    <caption>Tarkastuskäynnit</caption>
    <tr>
        <th> # </th>
        <th> Päivämäärä </th>
        <th> Aikuisia </th>
        <th> Munia </th>
        <th> Pesäpoik </th>
        <th> Kuolleita </th>
        <th> Lentopoik </th>
		<th> Kommentti </th>
    </tr>
    
    <#list 1..4 as i>
        <#if i = 2 || i = 4>
          <tr class="odd">
         <#else>
           <tr>
         </#if>
              <td> ${i} </td>
              
              <td> <@date "updateparameters" updateparameters.getColumn("kaynti."+i+".pvm") errors /> </td>
              <td> <@selection selections "updateparameters" updateparameters.getColumn("kaynti."+i+".aikuisia") 1 false true true errors /></td>
              <td> <@selection selections "updateparameters" updateparameters.getColumn("kaynti."+i+".munia") 1 false true true errors /></td>
              <td> <@selection selections "updateparameters" updateparameters.getColumn("kaynti."+i+".elavia") 1 false true true errors /></td>
              <td> <@selection selections "updateparameters" updateparameters.getColumn("kaynti."+i+".kuolleita") 1 false true true errors /></td>
              <td> <@selection selections "updateparameters" updateparameters.getColumn("kaynti."+i+".lentopoik") 1 false true true errors /></td>
              <td> <@textarea "updateparameters" updateparameters.getColumn("kaynti."+i+".kommentti") 1 33 errors /> </td>
        </tr> 
    </#list>
  </table>

<table class="kayntitaulu">
	<caption>Lukumäärät</caption>
    <tr>
        <th> </th>
        <th>Aikaisemmin</th>
        <th> </th>
        <th>Lopullinen<br/>määrä</th>
        <th>Tarkkuus</th>
    </tr>
    <tr>
    	<th>Aikuisia</th>
        <td> <@selection selections "updateparameters" updateparameters.tarkastus.aik_aikuisia 1 false true true errors /></td>
        <td> <@selection selections "updateparameters" updateparameters.tarkastus.aikuisia 1 false true true errors /></td>
        <td></td>
        <td rowspan="5" id="lkm_tarkkuus"><@selection selections "updateparameters" updateparameters.tarkastus.tarkkuus 2 false true true errors /></td>
    </tr>
	<tr>
    	<th>Munia</th>
        <td> <@selection selections "updateparameters" updateparameters.tarkastus.aik_munia 1 false true true errors /></td>
        <td> <@selection selections "updateparameters" updateparameters.tarkastus.munia 1 false true true errors /></td>
        <td></td>
    </tr>    
	<tr>
    	<th>Pesäpoikasia</th>
        <td> <@selection selections "updateparameters" updateparameters.tarkastus.aik_elavia 1 false true true errors /></td>
        <td> <@selection selections "updateparameters" updateparameters.tarkastus.elavia 1 false true true errors /></td>
        <td></td>
    </tr>
    <tr>
    	<th>Lentopoikasia</th>
        <td></td>
        <td> <@selection selections "updateparameters" updateparameters.tarkastus.lentopoik 1 false true true errors /></td>
        <td> <@selection selections "updateparameters" updateparameters.tarkastus.lop_lentopoik 1 false true true errors /></td>
    </tr>
    <tr>
    	<th>Kuolleita</th>
        <td></td>
        <td> <@selection selections "updateparameters" updateparameters.tarkastus.kuolleita 1 false true true errors /></td>
        <td> <@selection selections "updateparameters" updateparameters.tarkastus.lop_kuolleita 1 false true true errors /></td>
    </tr>
</table> 

<table>
	<caption>Rengastetut poikaset</caption>
		<tr>
			<th>#</th>
			<th>Rengas</th>
			<th>Siipi (mm)</th>
			<th>Paino (g)</th>
			<th>Kommentteja</th>
			<th>Rengastuspvm</th>
		</tr>
		<#list 1..4 as i>
		<tr>
			<th>${i}</th>
			<td><@inputText "updateparameters" updateparameters.getColumn("poikanen."+i+".rengas") true errors /> </td>
			<td><@inputText "updateparameters" updateparameters.getColumn("poikanen."+i+".siipi") true errors /> </td>
			<td><@inputText "updateparameters" updateparameters.getColumn("poikanen."+i+".paino") true errors /> </td>
			<td><@textarea "updateparameters" updateparameters.getColumn("poikanen."+i+".kommentti") 1 33 errors /> </td>
		    <td> <@date "updateparameters" updateparameters.getColumn("poikanen."+i+".pvm") errors /> </td>
		</tr>
		</#list>
</table>



<table id="lopputulos">
	<caption>Pesinnän lopputulos</caption>
	<tr>
		<th>Pesimistulos</th>
		<th>Tarkkuus</th>
		<th>Pesän kunto</th>
	</tr>
	<tr>
		<td> <@selection selections "updateparameters" updateparameters.tarkastus.pesimistulos 1 false true true errors /> </td>
		<td> <@selection selections "updateparameters" updateparameters.tarkastus.pesimist_tark 1 false true true errors /> </td>
		<td> <@selection selections "updateparameters" updateparameters.tarkastus.kunto 1 false true true errors /> </td>
	</tr>
	<tr>
		<th>Epäonnistuminen</th>
		<th>Tarkkuus</th>
	</tr>
	<tr>
		<td> <@selection selections "updateparameters" updateparameters.tarkastus.epaonn 1 false true true errors /> </td>
		<td> <@selection selections "updateparameters" updateparameters.tarkastus.epaonn_tark 1 false true true errors /> </td>
	</tr>
	<tr>
		<th>Vaihtopesän etsintä</th>
		<th>Muu pesivä laji</th>
	</tr>
	<tr>
		<td> <@selection selections "updateparameters" updateparameters.tarkastus.vaihtop_laiminl 1 false true true errors /> </td>
		<td> <@inputSelectCombo "updateparameters" updateparameters.tarkastus.muulaji true false errors /> </td>
	</tr>
</table>

<table>
  <tr>
    <th> Kommentit tarkastuksesta </th>
  </tr>
  <tr>
    <td> <@textarea "updateparameters" updateparameters.tarkastus.kommentti 3 100 errors /> </td>
  </tr>
</table>

<table>
	<caption>Lisätietoja</caption>
	<tr> <th>Pesän autioitumisen tai tuhoutumisen syy</th> <td> <@textarea "updateparameters" updateparameters.tarkastus.kommentti_tuho 1 60 errors /> </td> </tr>
	<tr> <th>Saalistusalueet</th> <td> <@textarea "updateparameters" updateparameters.tarkastus.kommentti_saalistusalue 1 60 errors /> </td> </tr>
	<tr> <th>Saaliskalat</th> <td> <@textarea "updateparameters" updateparameters.tarkastus.kommentti_saalis 1 60 errors /> </td> </tr>
</table>


<table>
	<caption>Näytteitä lähetetty museolle</caption>
	<tr>
		<th>Munia</th>
		<th>Kuolleita poikasia</th>
		<th>Emolintujen sulkia</th>
		<th>&nbsp;</th>
	</tr>
	<tr>
		<td> <@selection selections "updateparameters" updateparameters.tarkastus.nayte_munia 1 false true false errors /> </td>
		<td> <@selection selections "updateparameters" updateparameters.tarkastus.nayte_kuolleita 1 false true false errors /> </td>
		<td> <@selection selections "updateparameters" updateparameters.tarkastus.nayte_sulkia 1 false true false errors /> </td>
		<td> <a href="#" onclick="eiNaytteita(); return false;" id="eiNaytteitaButton">&raquo; Ei näytteitä</a> </td>
	</tr>
</table>
	 
   <br />
	 
   
      <#if (data.kirjekyyhky!"") == "yes">
        <button onclick="document.tarkastusForm.submit()">${texts["tarkastus_submit_kirjekyyhky_"+system.action]}</button>
      <#else>
        <button onclick="document.tarkastusForm.submit()">${texts["tarkastus_submit_"+system.action]}</button>
      </#if>
   
	 
  
</form>

</div>

	<script type="text/javascript">
	
	  function eiNaytteita() {
	  		$('#updateparameters\\.tarkastus\\.nayte_munia').val('E');
	  		$('#updateparameters\\.tarkastus\\.nayte_kuolleita').val('E');
	  		$('#updateparameters\\.tarkastus\\.nayte_sulkia').val('E');
	  }
	  
	  var form = document.forms['tarkastusForm'];
	  var nestID = form['updateparameters.pesa.pesaid'];
	  var yhtLeveys = form['updateparameters.pesa.yht_leveys'];
	  var yhtPituus = form['updateparameters.pesa.yht_pituus'];
	  var eurLeveys = form['updateparameters.pesa.eur_leveys'];
	  var eurPituus = form['updateparameters.pesa.eur_pituus'];
	  var radius = form['CoordinateSearchRadius'];
      getLahiymparistonPesat();
      
      var e = $('#updateparameters\\.tarkastus\\.rengastaja\\.selector');
      if (e.val() == '') {
      	e.val( $('#updateparameters\\.pesa\\.seur_rengastaja\\.input').val() );
      	e.change();
      } 
      
      var lomakeVastaanottajat = [<#list selections["ringers"].options as option>"${option.text?replace("\"","")} (${option.value})"<#if option_has_next>,</#if></#list>];
	
	</script>
	
<#include "footer.ftl">

