package fi.hy.pese.framework.main.general;

import java.sql.SQLException;
import java.text.ParseException;
import java.time.YearMonth;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import fi.hy.pese.framework.main.app._ApplicationSpecificationFactory;
import fi.hy.pese.framework.main.db.RowsToListResultHandler;
import fi.hy.pese.framework.main.db._DAO;
import fi.hy.pese.framework.main.general.WarehouseRowGenerator.ResolveCounts.Counts;
import fi.hy.pese.framework.main.general.WarehouseRowGenerator.ResolveDateAccuracy.DateAccuracy;
import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._IndexedTablegroup;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.main.general.data._Table;
import fi.laji.datawarehouse.etl.models.Securer.SecureLevel;
import fi.laji.datawarehouse.etl.models.Securer.SecureReason;
import fi.laji.datawarehouse.etl.models.dw.Annotation.Tag;
import fi.laji.datawarehouse.etl.models.dw.BaseModel;
import fi.laji.datawarehouse.etl.models.dw.Coordinates;
import fi.laji.datawarehouse.etl.models.dw.DateRange;
import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.laji.datawarehouse.etl.models.dw.DwRoot;
import fi.laji.datawarehouse.etl.models.dw.Gathering;
import fi.laji.datawarehouse.etl.models.dw.Quality;
import fi.laji.datawarehouse.etl.models.dw.Quality.Issue;
import fi.laji.datawarehouse.etl.models.dw.Quality.Source;
import fi.laji.datawarehouse.etl.models.dw.Unit;
import fi.laji.datawarehouse.etl.models.dw.Unit.AbundanceUnit;
import fi.laji.datawarehouse.etl.models.dw.Unit.LifeStage;
import fi.laji.datawarehouse.etl.models.dw.Unit.RecordBasis;
import fi.laji.datawarehouse.etl.models.exceptions.CriticalParseFailure;
import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException;
import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException.DateValidationException;
import fi.laji.datawarehouse.etl.utils.CoordinateConverter;
import fi.luomus.commons.containers.DateValue;
import fi.luomus.commons.containers.Selection;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.tipuapi.TipuAPIClient;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.Utils;
import fi.luomus.commons.xml.Document.Node;

public class WarehouseRowGenerator implements _WarehouseRowGenerator {
	
	private static final Qname ATLAS_CODE_7 = new Qname("MY.atlasCodeEnum7");
	private static final Set<String> IGNORE_FACTS = Utils.set(
			// merikotka
			"pesavakio.seur_tarkastaja",
			"pesavakio.uhat",
			"pesamuuttuva.omist_kommentti",
			"pesatarkastus.tyop_kommentti",
			"pesatarkastus.kuvaaja_lintu",
			"pesatarkastus.kuvaaja_pesa",
			"pesavakio.eur_leveys",
			"pesavakio.eur_pituus",
			"pesavakio.yht_leveys",
			"pesavakio.yht_pituus",
			"pesavakio.ast_leveys",
			"pesavakio.ast_pituus",
			"pesavakio.des_leveys",
			"pesavakio.des_pituus",
			// saaksi
			"pesa.seur_rengastaja",
			"pesa.lomake",
			"pesa.kommentti",
			"tarkastus.kommentti",
			// pesakortti
			"pesa.seur_tarkastaja",
			"pesa.lomake",
			"tarkastus.epailyttava",
			"tarkastus.kommentti",
			"pesa.kommentti",
			"tarkastus.suojaus",
			// peto
			"pesa.seur_tarkastaja",
			"tarkastus.oli_nipussa",
			"tarkastus.ref_vanhaan_dataan",
			"pesa.suojelu_kaytto",
			"pesa.kommentti",
			"pesa.kunta",
			"pesa.eur_leveys",
			"pesa.eur_pituus",
			"pesa.yht_leveys",
			"pesa.yht_pituus",
			"pesa.ast_leveys",
			"pesa.ast_pituus",
			"pesa.des_leveys",
			"pesa.des_pituus");
	
	public interface NestIsDead {
		// peto: kaynti.pesa_kunto == "5", "6", "7"
		// merikotka: pesatarkastus.pesa_kunto == "Y", "U", "D"
		// sääksi: tarkastus.epaonn = 'D'
		// pesakortti: not present
		boolean get(_PesaDomainModelRow row);
	}
	
	public interface NotInspected {
		// peto: tarkastus.tarkastettu K / E
		// merikotka: pesatarkastus.tark_tapa == 8
		// sääksi:  tarkastus.vaihtop_laiminl == J
		// pesakortti: not present
		boolean get(_PesaDomainModelRow row);
	}
	
	public interface ResolveCoordinates {
		// peto: eur_leveys, eur_pituus -> koord_ilm_mittaustapa K=100 X=1000 else 1
		// merikotka: eur_leveys, eur_pituus -> pesavakio.koord_tark 0=10 1=100 2=200 3=300 4=400 S=1000 else 5000
		// sääksi: eur_leveys, eur_pituus -> pesa.koord_tyyppi  88=1 90=100 70=1 else 1000
		// pesakortti: eur_leveys, eur_pituus -> pesa.koord_tarkkuus == accuracyInMeters else 1000
		Coordinates get(_Table pesa) throws NumberFormatException, IllegalArgumentException, DataValidationException;
	}
	
	public interface ResolveLocality {
		// peto: kyla + nimi
		// merikotka: tarkka_sijainti + pesanimi
		// sääksi: kyla + pesanimi
		// pesakortti: tarkempi_paikka + nimi
		String get(_Table pesa);

		public static class LocalityKylaNimi implements ResolveLocality {
			private final String kylaColumnName;
			private final String nimiColumnName;
			public LocalityKylaNimi(String kylaColumnName, String nimiColumnName) {
				this.kylaColumnName = kylaColumnName;
				this.nimiColumnName = nimiColumnName;
			}
			@Override
			public String get(_Table pesa) {
				String kyla = pesa.get(kylaColumnName).getValue();
				String nimi = pesa.get(nimiColumnName).getValue();
				return (kyla + " " + nimi).trim();
			}
		}
	}
	
	public interface ResolveDateAccuracy {
		// peto: kaynti.pvm_tarkkuus 1=ACCURATE 2=MONTH 3=YEAR
		// merikotka: pesatarkastus.tark_pvm_tark 1=ACCURATE 2=MONTH else YEAR
		// sääksi: tarkastus.tark_pvm_tark 1=ACCURATE 2=MONTH else YEAR
		// pesakortti: kaynti.pvm_tarkkuus 1=ACCURATE 2=MONTH 3=YEAR 4=UNKNOWN
		public static enum DateAccuracy { ACCURATE, MONTH, YEAR, UKNOWN }
		DateAccuracy get(_Table table);
	}
	
	public interface ResolveSpecies {
		// peto: pesiva_laji | muu_laji | rak_laji | kommentti_edellinen_laji | null
		// merikotka: muulaji ELSE "HALALB"
		// sääksi: muulaji ELSE "PANHAL"
		// pesakortti: pesiva_laji
		String get(_PesaDomainModelRow row);
	}
	
	public interface ResolveAdditionalNestingSpecies {
		// peto: none
		// merikotka: muulaji_vastarakki == K -> MOTALB + muulaji_rinnakkainen
		// sääksi: none
		// pesakortti: yhdyskunta_laji IF != pesiva_laji
		Set<String> get(_Table tarkastus);
		
		public static class None implements ResolveAdditionalNestingSpecies {
			@Override
			public Set<String> get(_Table tarkastus) {
				return Collections.emptySet();
			}
		}
		static ResolveAdditionalNestingSpecies none() {
			return new None();
		}
	}
	
	public interface ResolveNestCount {
		// peto: min kaynti.pesa_kunto <= 1 OR tarkastus.pesimistulos != A -> 1 ELSE 0
		// merikotka: pesatarkastus.pesa_kunto K Y U D AND nahdyt_merkit = A -> 0 ELSE 1
		// sääksi: tarkastus.kunto T A E AND pesimistulos = A -> 0 ELSE 1
		// pesakortti: 1
		int get(_PesaDomainModelRow row);
	}
	
	public interface ResolveAtlasCode {
		// peto: tarkastus.pesimistulos X=7 (only if tarkastus.pesimistulos_tarkkuus V K) B=2 D=62 E=66 F=66 K=66 L=66 M=71 N=82 O=82 P=71 Q=82 R=82 T=71 V=82 Y=66
		// merikotka: pesatarkastus.nahdyt_merkit D=66 E=66 F=63 H=66 J=66 K=66 L=66 M=71 N=71 P=71 Q=82 R=82 T=71 V=82	Y=66
		// sääksi: tarkastus.pesimistulos X=7 B=2 D=62 E=66 F=66 K=66 L=66 M=71 N=82 O=82 P=71 Q=82 R=82 T=71 V=82 Y=66
		// pesakortti: complex, see imple
		Qname get(_PesaDomainModelRow row); // non colonies
	}
	
	public interface ResolveCounts {
		// peto: aikuisia_lkm munia_lkm  pesapoikasia_lkm lentopoikasia_lkm kuolleita_lkm
		// merikotka: no käynti
		// sääksi: aikuisia munia(8 9->NULL) elavia(9->NULL) kuolleita(9->NULL) lentopoik(9->NULL)
		// pesakortti: lkm_munat lkm_munat_kuolleet lkm_poikaset lkm_poikaset_kuolleet
		public static class Counts {
			public Integer adults = null;
			public Integer liveEggs = null;
			public Integer deadEggs = null;
			public Integer livePullus = null;
			public Integer deadPullus = null;
			public Integer liveJuvenile = null;
			public Integer value(_Column c, String ... ignore) {
				if (!c.hasValue()) return null;
				String v = c.getValue();
				for (String i : ignore) {
					if (i.equals(v)) return null;
				}
				return c.getIntegerValue();
			}
			public boolean noCounts() {
				return noCount(adults) && noCount(liveEggs) && noCount(deadEggs) && noCount(livePullus) && noCount(deadPullus) & noCount(liveJuvenile);
			}
			private boolean noCount(Integer i) {
				return i == null || i == 0;
			}
		}
		Counts get(_Table kaynti);
	}
	
	public interface ResolveSuspecious {
		// peto: none
		// merikotka: none
		// sääksi: none
		// pesakortti: tarkastus.epailyttava = K
		boolean get(_PesaDomainModelRow row);
		
		public static class None implements ResolveSuspecious {
			@Override
			public boolean get(_PesaDomainModelRow row) {
				return false;
			}
		}
		
		static ResolveSuspecious none() {
			return new None();
		}
	}
	
	public interface ResolveSecure {
		// peto: pesa.suojelu_kaytto = E -> secure (regardless if public or not)
		// merikotka: none
		// sääksi: none
		// pesakortti: pesa.suojelu_kaytto = E -> secure (regardless if public or not) /  tarkastus.suojaus = K AND public=true AND occ date within 5 years of current date
		boolean get(_PesaDomainModelRow row, boolean isPublic);
		
		public static class None implements ResolveSecure {
			@Override
			public boolean get(_PesaDomainModelRow row, boolean isPublic) {
				return false;
			}
		}
		
		static ResolveSecure none() {
			return new None();
		}
	}
	
	public static class WarehouseGenerationDefinition {
		
		public WarehouseGenerationDefinition(WarehouseGenerationDefinitionBuilder builder) {
			this.sourceId = builder.sourceId;
			this.collectionId = builder.collectionId;
			this.nestIsDead = builder.nestIsDead;
			this.notInsepected = builder.notInspected;
			this.yearColumnFullname = builder.yearColumnFullname;
			this.dateColumnFullname = builder.dateColumnFullname;
			this.siteTypeColumnFullName = builder.siteTypeColumnFullName;
			this.municipalityColumnFullName = builder.municipalityColumnFullName;
			this.coordinates = builder.coordinates;
			this.locality = builder.locality;
			this.dateAccuracy = builder.dateAccuracy;
			this.nestingSpecies = builder.nestingSpecies;
			this.additionalNestingSpecies = builder.additionalNestingSpecies;
			this.nestCount = builder.nestCount;
			this.atlasCode = builder.atlasCode;
			this.counts = builder.counts;
			this.isSuspecious = builder.isSuspecious;
			this.shouldSecure = builder.shouldSecure;
		}
		
		private final Qname sourceId;
		private final Qname collectionId;
		private final NestIsDead nestIsDead;
		private final NotInspected notInsepected;

		// peto: tarkastus.vuosi
		// merikotka: pesatarkastus.tark_vuosi
		// sääksi: tarkastus.vuosi
		// pesakortti: tarkastus.vuosi
		private final String yearColumnFullname;

		// peto: --
		// merikotka: pesatarkastus.tark_pvm
		// saaksi: tarkastus.tark_pvm
		// pesakortti: --
		private final String dateColumnFullname;
		
		// peto: tarkastus.pesa_sijainti
		// merikotka: pesamuuttuva.sijainti
		// sääksi: olosuhde.pesan_sijainti
		// pesäkortti: pesa.pesa_sijainti
		private final String siteTypeColumnFullName;

		// peto: pesa.kunta
		// merikotka: vuosi.pesan_kunta
		// sääksi: pesa.kunta
		// pesäkortti: pesa.kunta
		private final String municipalityColumnFullName;

		private final ResolveCoordinates coordinates;
		private final ResolveLocality locality;
		private final ResolveDateAccuracy dateAccuracy;
		private final ResolveSpecies nestingSpecies;
		private final ResolveAdditionalNestingSpecies additionalNestingSpecies;
		private final ResolveNestCount nestCount;
		private final ResolveAtlasCode atlasCode;
		private final ResolveCounts counts;
		private final ResolveSuspecious isSuspecious;
		private final ResolveSecure shouldSecure;
		
	}
	
	private final WarehouseGenerationDefinition definition;
	private final _DAO<_PesaDomainModelRow> dao;
	private final _ApplicationSpecificationFactory<_PesaDomainModelRow> application;
	
	public WarehouseRowGenerator(WarehouseGenerationDefinition definition, _DAO<_PesaDomainModelRow> dao, _ApplicationSpecificationFactory<_PesaDomainModelRow> application) {
		this.definition = definition;
		this.dao = dao;
		this.application = application;
	}
	
	@Override
	public DwRoot generate(int rowId) throws Exception {
		_PesaDomainModelRow row = getRow(rowId);
		if (row == null) return deletedEntry(rowId);
		return generate(row);
	}
	
	public DwRoot deletedEntry(int tarkastusId) throws CriticalParseFailure {
		Qname documentId = toQname(tarkastusId, definition.collectionId);
		DwRoot root = DwRoot.createDeleteRequest(documentId, definition.sourceId);
		root.clearProcesstime();
		return root;
	}
	
	private _PesaDomainModelRow getRow(int rowId) throws SQLException {
		List<_PesaDomainModelRow> results = new LinkedList<>();
		RowsToListResultHandler handler = new RowsToListResultHandler(dao, results);
		_PesaDomainModelRow parameters = dao.newRow();
		parameters.getTarkastus().getUniqueNumericIdColumn().setValue(rowId);
		dao.executeSearch(parameters, handler);
		if (results.isEmpty()) return null; // delete
		if (results.size() != 1) throw new IllegalStateException("Seach by tarkastus id returned " + results.size() + " results!");
		return results.get(0);
	}
	
	public DwRoot generate(_PesaDomainModelRow row) throws Exception {
		DwRoot root = createRoot(row.getTarkastus());
		root.setPublicDocument(createDocument(row, root.createPublicDocument()));
		root.setPrivateDocument(createDocument(row, root.createPrivateDocument()));
		root.clearProcesstime();
		return root;
	}
	
	private DwRoot createRoot(_Table tarkastus) throws CriticalParseFailure {
		DwRoot root = new DwRoot(toQname(tarkastus.getUniqueNumericIdColumn().getIntegerValue(), definition.collectionId), definition.sourceId);
		root.setCollectionId(definition.collectionId);
		return root;
	}
	
	private Document createDocument(_PesaDomainModelRow row, Document document) throws Exception {
		List<String> ringerIds = parseDocument(row, document);
		
		if (definition.notInsepected.get(row)) {
			document.setNotes("Ei tarkastettu vuonna " + year(row));
			return document;
		}

		Gathering mainGathering = parseGathering(row, document, ringerIds);
		Unit mainUnit = parseUnit(row, document);
		if (mainUnit == null) return document;

		document.addGathering(mainGathering);
		
		if (hasKaynti(row)) {
			for (_Table kaynti : row.getKaynnit()) {
				DateRange dateRange = date(row, kaynti);
				if (dateRange == null) continue;
				
				Gathering targetGathering = mainGathering.copy();
				targetGathering.setGatheringId(toQname(kaynti.getUniqueNumericIdColumn().getIntegerValue(), document.getDocumentId(), "G."));
				targetGathering.setEventDate(dateRange);
				
				counts(kaynti, targetGathering, mainUnit.getTaxonVerbatim());
				
				facts(kaynti, targetGathering, document.isPublic());
				document.addGathering(targetGathering);
			}
		}

		mainGathering.addUnit(mainUnit);
		
		if (row.getPoikanen().isInitialized()) {
			if (row.getKaynti().isInitialized()) {
				facts(row.getPoikaset(), mainGathering, document.isPublic());
			} else {
				for (_Table poikanen : row.getPoikaset()) {
					mainGathering.addUnit(unit(poikanen, mainUnit.getTaxonVerbatim(), document));
				}
			}
		}

		if (row.getAikuinen().isInitialized()) {
			facts(row.getAikuiset(), mainGathering, document.isPublic());
		}

		if (row.getMuna().isInitialized()) {
			facts(row.getMunat(), mainGathering, document.isPublic());
		}
		
		facts(row.getOlosuhde(), mainGathering, document.isPublic());
		facts(row.getTarkastus(), mainGathering, document.isPublic());
		
		for (String species : definition.additionalNestingSpecies.get(row.getTarkastus())) {
			if (!given(species)) continue;
			Unit unit = new Unit(new Qname(document.getDocumentId().toString()+"_U."+species));
			baseUnitData(species, unit);
			unit.setAtlasCodeUsingQname(ATLAS_CODE_7);
			mainGathering.addUnit(unit);
		}

		if (!document.isPublic() && definition.isSuspecious.get(row)) {
			for (Gathering g : document.getGatherings()) {
				for (Unit u : g.getUnits()) {
					u.addSourceTag(Tag.FORMADMIN_UNCERTAIN);
				}
			}
		}

		if (definition.shouldSecure.get(row, document.isPublic())) {
			// should secure this document

			document.setSecureLevel(SecureLevel.KM10);
			document.addSecureReason(SecureReason.USER_HIDDEN);
			
			if (!document.isPublic()) {
				// if this is private document (but still should secure), need to do a bit of more custom work
				for (Gathering g : document.getGatherings()) {
					if (g.getCoordinates() == null) continue;
					Coordinates ykj = CoordinateConverter.convert(g.getCoordinates()).getYkj().conceal(SecureLevel.KM10);
					g.setCoordinates(ykj);
				}
			}
		}
		return document;

	}
	
	private Unit parseUnit(_PesaDomainModelRow row, Document document) throws CriticalParseFailure {
		Unit mainUnit = new Unit(new Qname(document.getDocumentId().toString()+"_U"));
		
		String species = definition.nestingSpecies.get(row);
		if (species == null) return null;

		mainUnit.setTaxonVerbatim(species);
		int nestCount = definition.nestCount.get(row);
		if (nestCount > 0) {
			mainUnit.setBreedingSite(true);
		}
		mainUnit.setAbundanceString(""+nestCount);
		mainUnit.setAbundanceUnit(AbundanceUnit.NESTS);
		
		mainUnit.setRecordBasis(RecordBasis.HUMAN_OBSERVATION_UNSPECIFIED);
		mainUnit.setWild(true);
		mainUnit.setAtlasCodeUsingQname(definition.atlasCode.get(row));
		return mainUnit;
	}

	private List<String> parseDocument(_PesaDomainModelRow row, Document document) throws Exception {
		document.setCollectionId(definition.collectionId);
		
		Integer pesaId = row.getPesa().getUniqueNumericIdColumn().getIntegerValue();
		document.setNamedPlaceIdUsingQname(toQname(pesaId, definition.collectionId, "MNP."));
		document.addKeyword(pesaId.toString());
		
		try {
			document.setCreatedDate(dateCreated(row));
		} catch (DateValidationException e) {}
		try {
			document.setModifiedDate(dateModified(row));
		} catch (DateValidationException e) {}
		
		List<String> ringerIds = ringerIds(row.getTarkastus());
		
		for (String id : ringerIds) {
			document.addEditorUserId(toUserId(id, document.isPublic()));
		}
		
		if (definition.notInsepected.get(row)) {
			return ringerIds;
		}
		
		if (definition.nestIsDead.get(row)) {
			document.setSiteDead(true);
			document.setSiteStatus("Tuhoutunut");
		} else {
			document.setSiteDead(false);
			document.setSiteStatus("Olemassaoleva");
		}
		String siteType = siteType(row);
		if (given(siteType)) {
			document.setSiteType("Pesä: " + siteType);
		} else {
			document.setSiteType("Pesä");
		}
		
		facts(row.getReviiri(), document, document.isPublic());
		facts(row.getPesa(), document, document.isPublic());
		return ringerIds;
	}
	
	private Gathering parseGathering(_PesaDomainModelRow row, Document document, List<String> ringerIds) throws CriticalParseFailure, Exception, ParseException {
		Gathering mainGathering = new Gathering(new Qname(document.getDocumentId().toString()+"_G"));
		
		for (String id : ringerIds) {
			mainGathering.addObserverUserId(toUserId(id, document.isPublic()));
		}
		for (String id : ringerIds) {
			mainGathering.addTeamMember(getRingerName(id, document.isPublic()));
		}
		try {
			mainGathering.setCoordinates(definition.coordinates.get(row.getPesa()));
		} catch (Exception e) {
			mainGathering.createQuality().setLocationIssue(new Quality(Issue.INVALID_EUREF_COORDINATES, Source.AUTOMATED_FINBIF_VALIDATION, e.getMessage()));
		}
		mainGathering.setLocality(definition.locality.get(row.getPesa()));
		mainGathering.setMunicipality(municipality(row));
		
		try {
			mainGathering.setEventDate(parseEventDate(row));
		} catch (DateValidationException e) {
			mainGathering.createQuality().setTimeIssue(new Quality(e.getIssue(), Source.AUTOMATED_FINBIF_VALIDATION, e.getMessage()));
		}
		return mainGathering;
	}

	private DateRange parseEventDate(_PesaDomainModelRow row) throws DateValidationException, ParseException {
		if (hasKaynti(row)) {
			TreeSet<Date> dates = dates(row.getKaynnit());
			if (!dates.isEmpty()) {
				DateRange range = new DateRange(dates.first(), dates.last());
				if (notFirstOfJan(range)) return range;
			}
			return yearRange(row);
		}
		
		DateAccuracy accuracy = definition.dateAccuracy.get(row.getTarkastus());

		if (DateAccuracy.YEAR == accuracy) return yearRange(row);
		
		if (DateAccuracy.ACCURATE == accuracy) {
			if (given(definition.dateColumnFullname)) {
				DateValue d = row.getColumn(definition.dateColumnFullname).getDateValue();
				return date(d);
			}
		}
		if (DateAccuracy.MONTH == accuracy) {
			if (given(definition.dateColumnFullname)) {
				DateValue d = row.getColumn(definition.dateColumnFullname).getDateValue();
				return month(d);
			}
		}
		return null;
	}

	private boolean notFirstOfJan(DateRange range) {
		return !firstOfJan(range.getBegin()) || !firstOfJan(range.getEnd());
	}
	
	private boolean firstOfJan(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.get(Calendar.DAY_OF_MONTH) == 1 && calendar.get(Calendar.MONTH) == Calendar.JANUARY;
	}
	
	private void baseUnitData(String species, Unit unit) {
		unit.setRecordBasis(RecordBasis.HUMAN_OBSERVATION_UNSPECIFIED);
		unit.setTaxonVerbatim(species);
		unit.setAbundanceString("1");
		unit.setAbundanceUnit(AbundanceUnit.NESTS);
		unit.setBreedingSite(true);
		unit.setWild(true);
	}
	
	private void counts(_Table kaynti, Gathering targetGathering, String species) throws CriticalParseFailure {
		Counts counts = definition.counts.get(kaynti);
		unit(counts.adults, LifeStage.ADULT, true, species, targetGathering, "AD");
		unit(counts.liveEggs, LifeStage.EGG, true, species, targetGathering, "EG");
		unit(counts.deadEggs, LifeStage.EGG, false, species, targetGathering, "DEG");
		unit(counts.livePullus, LifeStage.IMMATURE, true, species, targetGathering, "PUL");
		unit(counts.deadPullus, LifeStage.IMMATURE, false, species, targetGathering, "DPUL");
		unit(counts.liveJuvenile, LifeStage.JUVENILE, true, species, targetGathering, "JUV");
	}
	
	private void unit(Integer count, LifeStage lifeStage, Boolean alive, String species, Gathering gathering, String idPrefix) throws CriticalParseFailure {
		if (count == null) return;
		Unit unit = new Unit(new Qname(gathering.getGatheringId().toString() + "_" + idPrefix));
		baseUnitData(species, unit);
		unit.setAbundanceString(count.toString());
		unit.setAbundanceUnit(AbundanceUnit.INDIVIDUAL_COUNT);
		unit.setLifeStage(lifeStage);
		unit.setAlive(alive);
		gathering.addUnit(unit);
	}
	
	private Unit unit(_Table poikanen, String species, Document document) throws Exception {
		Unit unit = new Unit(new Qname(document.getDocumentId().toString() + "_P" + poikanen.getUniqueNumericIdColumn().getValue()));
		baseUnitData(species, unit);
		unit.setAbundanceUnit(AbundanceUnit.INDIVIDUAL_COUNT);
		unit.setLifeStage(LifeStage.IMMATURE);
		unit.setAlive(true);
		facts(poikanen, unit, document.isPublic());
		return unit;
	}
	
	private DateRange date(_PesaDomainModelRow row, _Table kaynti) {
		_Column dateColumn = kaynti.get("pvm");
		if (!dateColumn.hasValue()) return null;
		try {
			DateValue date = dateColumn.getDateValue();
			DateAccuracy accuracy = definition.dateAccuracy.get(kaynti);
			
			if (accuracy == null) return null;
			
			if (accuracy == DateAccuracy.ACCURATE) return date(date);
			
			if (accuracy == DateAccuracy.MONTH) return month(date);
			
			if (accuracy == DateAccuracy.YEAR)
				return yearRange(row);
			
			return null;
		} catch (Exception e) {
			return null;
		}
	}
	
	private DateRange month(DateValue date) throws DateValidationException {
		return new DateRange(
				DateUtils.convertToDate(new DateValue(1, date.getMonth(), date.getYear())),
				lastDayOfMonth(date));
	}
	
	private DateRange date(DateValue date) throws DateValidationException {
		return new DateRange(DateUtils.convertToDate(date));
	}
	
	private Date lastDayOfMonth(DateValue value) {
		int month = value.getMonthAsInt();
		int year = value.getYearAsInt();
		return DateUtils.convertToDate(new DateValue(
				YearMonth.of(year, month).atEndOfMonth().getDayOfMonth(),
				month,
				year));
	}
	
	private int year(_PesaDomainModelRow row) {
		return row.getColumn(definition.yearColumnFullname).getIntegerValue();
	}

	private DateRange yearRange(_PesaDomainModelRow row) throws DateValidationException, ParseException {
		int year = year(row);
		return new DateRange(DateUtils.convertToDate("1.1."+year), DateUtils.convertToDate("31.12."+year));
	}
	
	private TreeSet<Date> dates(_IndexedTablegroup kaynnit) {
		TreeSet<Date> set = new TreeSet<>();
		for (_Table kaynti : kaynnit) {
			_Column dateColumn = kaynti.get("pvm");
			if (!dateColumn.hasValue()) continue;
			try {
				DateValue date = dateColumn.getDateValue();
				DateAccuracy accuracy = definition.dateAccuracy.get(kaynti);
				if (accuracy == DateAccuracy.ACCURATE) {
					set.add(DateUtils.convertToDate(date));
				} else if (accuracy == DateAccuracy.MONTH) {
					set.add(DateUtils.convertToDate(new DateValue(1, date.getMonth(), date.getYear())));
					set.add(lastDayOfMonth(date));
				}
			} catch (Exception e) {}
		}
		return set;
	}
	
	private boolean hasKaynti(_PesaDomainModelRow row) {
		return row.getKaynti().isInitialized() && row.getKaynnit().hasValues();
	}
	
	private String municipality(_PesaDomainModelRow row) throws Exception {
		_Column c = row.getColumn(definition.municipalityColumnFullName);
		if (!c.hasValue()) return null;
		return selectionValue(c);
	}
	
	private List<String> ringerIds(_Table t) {
		List<String> ids = new ArrayList<>();
		if (!t.isInitialized()) return ids;
		for (_Column c : t) {
			if (TipuAPIClient.RINGERS.equals(c.getAssosiatedSelection())) {
				if (IGNORE_FACTS.contains(c.getFullname())) continue; // ignore "seuraava tarkastaja"
				if (c.hasValue()) {
					if (!ids.contains(c.getValue())) ids.add(c.getValue());
				}
			}
		}
		return ids;
	}
	
	private void facts(_IndexedTablegroup tablegroup, BaseModel model, boolean publicDocument) throws Exception {
		for (_Table table : tablegroup) {
			if (!table.hasValues()) continue;
			facts(table, model, publicDocument);
		}
	}
	
	private void facts(_Table table, BaseModel model, boolean publicDocument) throws Exception {
		if (!table.isInitialized()) return;
		for (_Column c : table) {
			if (!c.hasValue()) continue;
			if (c.isRowIdColumn()) continue;
			if (c.getFieldType() == _Column.UNIQUE_NUMERIC_INCREASING_ID) continue;
			if (c.getFieldType() == _Column.DATE_ADDED_COLUMN) continue;
			if (c.getFieldType() == _Column.DATE_MODIFIED_COLUMN) continue;
			if (c.getReferenceColumn() != null) continue;
			if (IGNORE_FACTS.contains(c.getFullname())) continue;
			if (c.getInputSize() > 100) continue;
			if (c.getFullname().equals(definition.municipalityColumnFullName)) continue;
			String label = application.uiTexts().getAllTexts("FI").get(c.getFullname());
			String name = label == null ? c.getFullname() : label;
			String value = factValue(c, publicDocument);
			model.addFact(name, value);
		}
	}
	
	private String factValue(_Column c, boolean publicDocument) throws Exception {
		if (!c.hasAssosiatedSelection()) return c.getValue();
		
		if (c.getAssosiatedSelection().equals(TipuAPIClient.RINGERS)) {
			return getRingerName(c.getValue(), publicDocument);
		}
		return selectionValue(c);
	}
	
	private String selectionValue(_Column c) throws Exception {
		Selection s = application.selections(dao).get(c.getAssosiatedSelection());
		if (s == null) throw new IllegalStateException(c.getFullname() + " should have selection " + c.getAssosiatedSelection());
		if (s.containsOption(c.getValue())) {
			return s.get(c.getValue());
		}
		return c.getValue();
	}
	
	private String siteType(_PesaDomainModelRow row) throws Exception {
		if (!given(definition.siteTypeColumnFullName)) return null;
		return selectionValue(row.getColumn(definition.siteTypeColumnFullName));
	}
	
	private Date dateModified(_PesaDomainModelRow row) {
		Date max = null;
		for (_Table t : row) {
			if (!t.isInitialized()) continue;
			for (_Column c : t) {
				if (c.getFieldType() == _Column.DATE_MODIFIED_COLUMN) {
					if (max == null) {
						max = date(c);
					} else {
						Date d = date(c);
						if (d != null) {
							if (d.after(max)) max = d;
						}
					}
				}
			}
		}
		return max;
	}
	
	private Date date(_Column c) {
		if (!c.hasValue()) return null;
		return DateUtils.convertToDate(c.getDateValue());
	}
	
	private Date dateCreated(_PesaDomainModelRow row) {
		for (_Column c : row.getTarkastus()) {
			if (c.getFieldType() == _Column.DATE_ADDED_COLUMN) {
				return date(c);
			}
		}
		return null;
	}
	
	private String toUserId(String ringer, boolean publicDocument) {
		Node data = application.getTipuApiResource(TipuAPIClient.RINGERS).getById(ringer);
		if (data == null) return null;
		if (publicDocument && !allowsNamePublish(data)) {
			return "lintuvaara:"+ringer;
		}
		return null;
	}

	private String getRingerName(String ringer, boolean publicDocument) {
		Node data = application.getTipuApiResource(TipuAPIClient.RINGERS).getById(ringer);
		if (data == null) return "Unknown";
		String firstName = data.getNode("firstname").getContents();
		String lastName = data.getNode("lastname").getContents();
		String name = given(firstName) ? firstName + " " : "";
		if (given(lastName)) name += lastName;
		if (publicDocument && !allowsNamePublish(data)) {
			return "Hidden";
		}
		return Utils.capitalizeName(name);
	}
	
	private boolean allowsNamePublish(Node ringerData) {
		if (!ringerData.hasAttribute("FinBIF-permission")) return false;
		return "1".equals(ringerData.getAttribute("FinBIF-permission"));
	}
	
	private Qname toQname(int id, Qname base) {
		return Qname.fromURI(base.toURI() + "/" + id);
	}
	
	private Qname toQname(int id, Qname base, String prefix) {
		return Qname.fromURI(base.toURI() + "/" + prefix + id);
	}
	
	private boolean given(String s) {
		return s != null && !s.isEmpty();
	}
	
}
