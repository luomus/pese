package fi.hy.pese.framework.main.app.functionality.validate;



import java.util.HashSet;
import java.util.Set;

import fi.luomus.commons.utils.Utils;

/**
 * Vaaditut = joukko Havainto-arvoja joista vähintään yksi on oltava annettu
 * Sallitut = joukko sallituista Havainto-arvoista; jos Havainto-arvo ei ole sallittu, sitä ei saa antaa. Kaikki vaaditut ovat automaattisesti sallittuja
 */
public class PesimistulosValidationDefinition {
	
	public static enum Havainto {EMOJA, HAUTOVA, SIRUJA, MUNIA, KUOLLEITA, PESAP, LENTOP}
	
	/**
	 * Vaaditut = joukko Havainto-arvoja joista vähintään yksi on oltava annettu
	 * Sallitut = joukko sallituista Havainto-arvoista; jos Havainto-arvo ei ole sallittu, sitä ei saa antaa. Kaikki vaaditut ovat automaattisesti sallittuja
	 */
	public PesimistulosValidationDefinition() {}
	
	private Set<Havainto> sallitut = new HashSet<>();
	private Set<Havainto> vaaditut = new HashSet<>();
	
	/**
	 * Joukko sallituista Havainto-arvoista; jos Havainto-arvo ei ole sallittu, sitä ei saa antaa
	 */
	public Set<Havainto> sallitut() {
		Set<Havainto> set = new HashSet<>(sallitut);
		set.addAll(vaaditut);
		return set;
	}
	
	/**
	 * Joukko Havainto-arvoja joista vähintään yksi on oltava annettu
	 */
	public Set<Havainto> vaaditut() {
		return vaaditut;
	}
	
	/**
	 * Joukko Havainto-arvoja joista vähintään yksi on oltava annettu
	 */
	public PesimistulosValidationDefinition setVaaditut(Havainto...havainnot) {
		vaaditut = Utils.set(havainnot);
		return this;
	}
	
	/**
	 * Joukko sallituista Havainto-arvoista; jos Havainto-arvo ei ole sallittu, sitä ei saa antaa. Kaikki vaaditut ovat automaattisesti sallittuja.
	 */
	public PesimistulosValidationDefinition setSallitut(Havainto...havainnot) {
		sallitut = Utils.set(havainnot);
		return this;
	}
	
}
