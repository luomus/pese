//package fi.hy.pese.framework.test.testswithdb;
//
//import fi.hy.pese.framework.main.app.Request;
//import fi.hy.pese.framework.main.app.RequestImpleData;
//import fi.hy.pese.framework.main.app.functionality.validate.Validator;
//import fi.hy.pese.framework.main.app.functionality.validate._Validator;
//import fi.hy.pese.framework.main.db.RowsToData_Single_Table_ResultHandler;
//import fi.hy.pese.framework.main.db._DAO;
//import fi.hy.pese.framework.main.db.oraclesql.OracleSQL_DAO;
//import fi.hy.pese.framework.main.db.oraclesql.Queries;
//import fi.hy.pese.framework.main.general.data.Data;
//import fi.hy.pese.framework.main.general.data.DatabaseTableStructure;
//import fi.hy.pese.framework.main.general.data.PesaDomainModelDatabaseStructure;
//import fi.hy.pese.framework.main.general.data.PesaDomainModelRowFactory;
//import fi.hy.pese.framework.main.general.data.ReferenceKey;
//import fi.hy.pese.framework.main.general.data.Table;
//import fi.hy.pese.framework.main.general.data._Column;
//import fi.hy.pese.framework.main.general.data._Data;
//import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
//import fi.hy.pese.framework.main.general.data._ReferenceKey;
//import fi.hy.pese.framework.main.general.data._RowFactory;
//import fi.hy.pese.framework.main.general.data._Table;
//import fi.hy.pese.framework.test.TestConfig;
//import fi.luomus.commons.containers.Selection;
//import fi.luomus.commons.containers.Selection.SelectionOption;
//import fi.luomus.commons.db.connectivity.PreparedStatementStoringAndClosingTransactionConnection;
//import fi.luomus.commons.db.connectivity.TransactionConnection;
//
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
//import java.sql.SQLException;
//import java.util.Iterator;
//
//import junit.framework.Test;
//import junit.framework.TestCase;
//import junit.framework.TestSuite;
// 
//public class QueriesTest {
//	
//	public static Test suite() {
//		return new TestSuite(QueriesTest.class.getDeclaredClasses());
//	}
//
//	public static class QueriesTesting extends TestCase {
//		
//		private TransactionConnection	con;
//		private _DAO<_PesaDomainModelRow>		dao;
//		
//		@Override
//		protected void setUp() throws Exception {
//			Thread.sleep(10); // Oracle 10g XE doesn't like opening connections too rapidly. NOTE: ALTER SYSTEM SET PROCESSES=150 SCOPE=SPFILE HELPS !!!
//			con = new PreparedStatementStoringAndClosingTransactionConnection(TestConfig.getConfig().connectionDescription());
//			con.startTransaction();
//			PesaDomainModelDatabaseStructure structure = new PesaDomainModelDatabaseStructure();
//			DatabaseTableStructure pesa = new DatabaseTableStructure("pesa");
//			pesa.addColumn("pesa_id", _Column.INTEGER, 5);
//			pesa.addColumn("pesanimi", _Column.VARCHAR, 25);
//			structure.setPesa(pesa);
//			_RowFactory<_PesaDomainModelRow> rowFactory = new PesaDomainModelRowFactory(structure, "", "");
//			dao = new OracleSQL_DAO<_PesaDomainModelRow>(con, rowFactory);
//		}
//		
//		@Override
//		protected void tearDown() {
//			try {
//				con.rollbackTransaction();
//			} catch (SQLException e) {
//				fail("Couldn't rollback");
//			}
//			con.release();
//			con = null;
//		}
//		
//		public void test_returnValueByRowid() throws SQLException {
//			PreparedStatement p = con.prepareStatement(" delete from pesa ");
//			p.execute();
//			p = con.prepareStatement(" insert into pesa (pesa_id, pesanimi) values (1, 'testi') ");
//			p.execute();
//			p = con.prepareStatement(" select rowid from pesa where pesa_id = 1");
//			ResultSet rs = p.executeQuery();
//			rs.next();
//			String rowid = rs.getString(1);
//			rs.close();
//			
//			String result = Queries.returnValueByRowid("pesa", "pesanimi", rowid, con);
//			assertEquals("testi", result);
//		}
//		
//		public void test_returnTableByRowid() throws SQLException {
//			PreparedStatement p = con.prepareStatement(" delete from pesa ");
//			p.execute();
//			p = con.prepareStatement(" insert into pesa (pesa_id, pesanimi) values (1, 'testi') ");
//			p.execute();
//			p = con.prepareStatement(" select rowid from pesa where pesa_id = 1");
//			ResultSet rs = p.executeQuery();
//			rs.next();
//			String rowid = rs.getString(1);
//			rs.close();
//			
//			_Table result = dao.returnTableByRowid("pesa", rowid);
//			assertEquals("1", result.get("pesa_id").getValue());
//			assertEquals("testi", result.get("pesanimi").getValue());
//		}
//		
//		public void test__checkValueExists_query() throws Exception {
//			PreparedStatement p = con.prepareStatement(" delete from pesa ");
//			p.execute();
//			p = con.prepareStatement(" insert into pesa (pesa_id) values (1) ");
//			p.execute();
//			_ReferenceKey reference = new ReferenceKey("pesa", "pesa_id");
//			assertEquals(false, Queries.checkReferenceValueExists("999", reference, con));
//			assertEquals(true, Queries.checkReferenceValueExists("1", reference, con));
//		}
//		
//		public void test_tableInfoQuery() throws Exception {
//			DatabaseTableStructure pesa = Queries.returnTableStructure("pesa", con);
//			DatabaseTableStructure kunta = Queries.returnTableStructure("kunta", con);
//			assertEquals("pesa", pesa.getName());
//			assertEquals("kunta", kunta.getName());
//			assertEquals(_Column.INTEGER, pesa.get("pesa_id").getDatatype());
//			assertEquals(20, pesa.get("pesanimi").getSize());
//			assertEquals(2, kunta.get("ku_ast_lev").getDecimalSize());
//			assertEquals(_Column.DATE, pesa.get("muutos_pvm").getDatatype());
//		}
//		
//		public void test_selectionsQuery() throws Exception {
//			initKunta_testdata();
//			
//			Selection selection = Queries.returnSelection(con, "kunta", "kulyh", "kunimi", new String[] {"kunimi"});
//			//assertEquals("kunta", selection.getName());
//			Iterator<SelectionOption> i = selection.getOptions().iterator();
//			SelectionOption option = i.next();
//			assertEquals("HEL", option.getValue());
//			assertEquals("Helsinki", option.getText());
//			option = i.next();
//			assertEquals("VAN", option.getValue());
//			assertEquals("Vantaa", option.getText());
//			if (i.hasNext()) fail("Too many results");
//		}
//		
//		private Data<_PesaDomainModelRow> initKunta_testdata() throws SQLException {
//			deleteAll("kunta");
//			PreparedStatement s = con.prepareStatement("insert into kunta (kulyh, kunimi) values (?, ?)");
//			s.setString(1, "van");
//			s.setString(2, "Vantaa");
//			s.execute();
//			s.setString(1, "hel");
//			s.setString(2, "Helsinki");
//			s.execute();
//			
//			PesaDomainModelDatabaseStructure template = new PesaDomainModelDatabaseStructure();
//			DatabaseTableStructure kunta = Queries.returnTableStructure("kunta", con);
//			template.setPesa(kunta);
//			_RowFactory<_PesaDomainModelRow> rowFactory = new PesaDomainModelRowFactory(template, "", "");
//			Data<_PesaDomainModelRow> data = new Data<_PesaDomainModelRow>(rowFactory);
//			return data;
//		}
//		
//		public void test_tablesearch_no_where_parameters_ordersby_column_order() throws Exception {
//			Data<_PesaDomainModelRow> data = initKunta_testdata();
//			Queries.executeSearch(data.searchparameters().getTableByName("kunta"), new RowsToData_Single_Table_ResultHandler<_PesaDomainModelRow>(data, "kunta"), con);
//			assertEquals(2, data.results().size());
//			assertEquals("Helsinki", data.results().get(0).getPesa().get("kunimi").getValue());
//		}
//		
//		public void test_tablesearch__search_ignores_case_for_non_numeric_searchfields() throws Exception {
//			Data<_PesaDomainModelRow> data = initKunta_testdata();
//			data.searchparameters().getPesa().get("kulyh").setValue("VAN");
//			Queries.executeSearch(data.searchparameters().getPesa(), new RowsToData_Single_Table_ResultHandler<_PesaDomainModelRow>(data, "kunta"), con);
//			assertEquals(1, data.results().size());
//			assertEquals("Vantaa", data.results().get(0).getPesa().get("kunimi").getValue());
//		}
//		
//		public void test_tablesearch__search_uses_wildcards_both_ends_of_search_parameter_and_ignores_uppercase() throws Exception {
//			Data<_PesaDomainModelRow> data = initKunta_testdata();
//			data.searchparameters().getPesa().get("kulyh").setValue("a");
//			Queries.executeSearch(data.searchparameters().getPesa(), new RowsToData_Single_Table_ResultHandler<_PesaDomainModelRow>(data, "kunta"), con);
//			assertEquals(1, data.results().size());
//			assertEquals("Vantaa", data.results().get(0).getPesa().get("kunimi").getValue());
//		}
//		
//		public void test_tablesearch__search_requires_exact_match_of_search_paramter_for_numeric_id_types() throws Exception {
//			Data<_PesaDomainModelRow> data = initPesa_testdata();
//			data.searchparameters().getPesa().get("pesa_id").setValue("1");
//			Queries.executeSearch(data.searchparameters().getPesa(), new RowsToData_Single_Table_ResultHandler<_PesaDomainModelRow>(data, "pesa"), con);
//			assertEquals(1, data.results().size());
//			assertEquals("Pesa 1", data.results().get(0).getPesa().get("pesanimi").getValue());
//		}
//		
//		private Data<_PesaDomainModelRow> initPesa_testdata() throws SQLException {
//			deleteAll("pesa");
//			PreparedStatement s = con.prepareStatement("insert into pesa (pesa_id, pesanimi) values (?, ?)");
//			s.setString(1, "1");
//			s.setString(2, "Pesa 1");
//			s.execute();
//			s.setString(1, "12");
//			s.setString(2, "Pesa 12");
//			s.execute();
//			s.setString(1, "212");
//			s.setString(2, "Pesa 212");
//			s.execute();
//			
//			PesaDomainModelDatabaseStructure template = new PesaDomainModelDatabaseStructure();
//			DatabaseTableStructure pesa = Queries.returnTableStructure("pesa", con);
//			template.setPesa(pesa);
//			pesa.get("pesa_id").setTypeToUniqueNumericIncreasingId();
//			_RowFactory<_PesaDomainModelRow> rowFactory = new PesaDomainModelRowFactory(template, "", "");
//			Data<_PesaDomainModelRow> data = new Data<_PesaDomainModelRow>(rowFactory);
//			return data;
//		}
//		
//		public void test_rowIdMatchesKeys______unique_numeric_key() throws SQLException {
//			DatabaseTableStructure reviiriStructure = new DatabaseTableStructure("reviiri");
//			reviiriStructure.addColumn("reviiriid", _Column.INTEGER, 5);
//			reviiriStructure.get("reviiriid").setTypeToUniqueNumericIncreasingId();
//			
//			_Table reviiri = new Table(reviiriStructure);
//			
//			PreparedStatement s = con.prepareStatement("insert into reviiri (reviiriid, nimi) (select max(reviiriid)+1, 'uuden nimi' from reviiri) ");
//			s.execute();
//			s = con.prepareStatement("select rowid, reviiriid from reviiri where reviiriid = (select max(reviiriid) from reviiri) ");
//			ResultSet rs = s.executeQuery();
//			rs.next();
//			String rowid = rs.getString(1);
//			String id = rs.getString(2);
//			
//			reviiri.setRowidValue(rowid);
//			reviiri.get("reviiriid").setValue(id);
//			assertTrue("Row should exist", Queries.checkRowIdMatchesKeys(reviiri, con));
//			
//			reviiri.get("reviiriid").setValue("99966");
//			assertFalse("Row should not exist", Queries.checkRowIdMatchesKeys(reviiri, con));
//			
//			reviiri.get("reviiriid").setValue("");
//			assertFalse("Row should not exist", Queries.checkRowIdMatchesKeys(reviiri, con));
//			
//			reviiri.setRowidValue("FOOO");
//			reviiri.get("reviiriid").setValue(id);
//			try {
//				assertFalse("Row should not exist", Queries.checkRowIdMatchesKeys(reviiri, con));
//			} catch (SQLException e) {
//			}
//		}
//		
//		public void test_rowIdMatchesKeys______one_nonnumeric_key() throws SQLException {
//			DatabaseTableStructure lajiStructure = new DatabaseTableStructure("laji");
//			lajiStructure.addColumn("id", _Column.VARCHAR, 2);
//			lajiStructure.get("id").setTypeToImmutablePartOfAKey();
//			
//			_Table laji = new Table(lajiStructure);
//			
//			PreparedStatement s = con.prepareStatement("insert into laji (id, nimi) values ('XZ', 'uuden nimi') ");
//			s.execute();
//			s = con.prepareStatement("select rowid, id from laji where id = 'XZ' ");
//			ResultSet rs = s.executeQuery();
//			rs.next();
//			String rowid = rs.getString(1);
//			String id = rs.getString(2);
//			
//			laji.setRowidValue(rowid);
//			laji.get("id").setValue(id);
//			assertTrue("Row should exist", Queries.checkRowIdMatchesKeys(laji, con));
//			
//			laji.get("id").setValue("AA");
//			assertFalse("Row should not exist", Queries.checkRowIdMatchesKeys(laji, con));
//			
//			laji.get("id").setValue(id);
//			deleteAll("laji");
//			assertFalse("Row should not exist", Queries.checkRowIdMatchesKeys(laji, con));
//			
//			laji.setRowidValue("FOOO");
//			try {
//				assertFalse("Row should not exist", Queries.checkRowIdMatchesKeys(laji, con));
//			} catch (SQLException e) {
//			}
//		}
//		
//		public void test_rowIdMatchesKeys______several_nonnumeric_keys() throws SQLException {
//			DatabaseTableStructure aputauluStructure = Queries.returnTableStructure("aputaulu", con);
//			aputauluStructure.get("taulu").setTypeToImmutablePartOfAKey();
//			aputauluStructure.get("kentta").setTypeToImmutablePartOfAKey();
//			aputauluStructure.get("koodi").setTypeToImmutablePartOfAKey();
//			
//			_Table aputaulu = new Table(aputauluStructure);
//			
//			PreparedStatement s;
//			deleteAll("aputaulu");
//			
//			s = con.prepareStatement("insert into aputaulu (taulu, kentta, koodi, kieli, selite, jarjestys) " + "values ('PESA', 'KOORD_TYYPPI', 'Y', 'FI', 'Yhtenäis', 0) ");
//			s.execute();
//			s = con.prepareStatement("select rowid from aputaulu where taulu = 'PESA' and kentta='KOORD_TYYPPI' and koodi='Y' ");
//			ResultSet rs = s.executeQuery();
//			if (!rs.next()) fail();
//			String rowid = rs.getString(1);
//			
//			aputaulu.setRowidValue(rowid);
//			aputaulu.get("taulu").setValue("pesa");
//			aputaulu.get("kentta").setValue("koord_tyyppi");
//			aputaulu.get("koodi").setValue("Y");
//			assertTrue("Row should exist", Queries.checkRowIdMatchesKeys(aputaulu, con));
//			
//			aputaulu.get("taulu").setValue("pes");
//			assertFalse("Row should not exist", Queries.checkRowIdMatchesKeys(aputaulu, con));
//			
//			aputaulu.get("koodi").setValue("E");
//			assertFalse("Row should not exist", Queries.checkRowIdMatchesKeys(aputaulu, con));
//			
//			aputaulu.get("koodi").setValue("Y");
//			deleteAll("aputaulu");
//			assertFalse("Row should not exist", Queries.checkRowIdMatchesKeys(aputaulu, con));
//			
//			aputaulu.setRowidValue("FOOO");
//			try {
//				Queries.checkRowIdMatchesKeys(aputaulu, con);
//				fail("Should throw sqlexception");
//			} catch (SQLException e) {
//			}
//		}
//		
//		private void deleteAll(String table) throws SQLException {
//			PreparedStatement s = con.prepareStatement("delete from " + table);
//			s.execute();
//		}
//		
//		public void test_rowExists________by_rowId() throws SQLException {
//			DatabaseTableStructure reviiriStructure = Queries.returnTableStructure("reviiri", con);
//			reviiriStructure.get("reviiriid").setTypeToUniqueNumericIncreasingId();
//			
//			_Table reviiri = new Table(reviiriStructure);
//			
//			PreparedStatement s = con.prepareStatement("insert into reviiri (reviiriid, nimi) (select max(reviiriid)+1, 'uuden nimi' from reviiri) ");
//			s.execute();
//			s = con.prepareStatement("select rowid, reviiriid from reviiri where reviiriid = (select max(reviiriid) from reviiri) ");
//			ResultSet rs = s.executeQuery();
//			rs.next();
//			String rowid = rs.getString(1);
//			reviiri.setRowidValue(rowid);
//			
//			assertTrue("Row should exist", Queries.checkRowExists(reviiri, con));
//			deleteAll("reviiri");
//			assertFalse("Row should not exist", Queries.checkRowExists(reviiri, con));
//		}
//		
//		public void test_rowExists________by_numericId() throws SQLException {
//			DatabaseTableStructure reviiriStructure = Queries.returnTableStructure("reviiri", con);
//			reviiriStructure.get("reviiriid").setTypeToUniqueNumericIncreasingId();
//			
//			_Table reviiri = new Table(reviiriStructure);
//			
//			PreparedStatement s = con.prepareStatement("insert into reviiri (reviiriid, nimi) (select max(reviiriid)+1, 'uuden nimi' from reviiri) ");
//			s.execute();
//			s = con.prepareStatement("select rowid, reviiriid from reviiri where reviiriid = (select max(reviiriid) from reviiri) ");
//			ResultSet rs = s.executeQuery();
//			rs.next();
//			String id = rs.getString(2);
//			reviiri.get("reviiriid").setValue(id);
//			
//			assertTrue("Row should exist", Queries.checkRowExists(reviiri, con));
//			deleteAll("reviiri");
//			assertFalse("Row should not exist", Queries.checkRowExists(reviiri, con));
//		}
//		
//		public void test_rowExists________by_several_nonumeric_keys() throws SQLException {
//			DatabaseTableStructure aputauluStructure = Queries.returnTableStructure("aputaulu", con);
//			aputauluStructure.get("taulu").setTypeToImmutablePartOfAKey();
//			aputauluStructure.get("kentta").setTypeToImmutablePartOfAKey();
//			aputauluStructure.get("koodi").setTypeToImmutablePartOfAKey();
//			
//			_Table aputaulu = new Table(aputauluStructure);
//			
//			PreparedStatement s;
//			deleteAll("aputaulu");
//			
//			s = con.prepareStatement("insert into aputaulu (taulu, kentta, koodi, kieli, selite, jarjestys) " + "values ('PESA', 'KOORD_TYYPPI', 'Y', 'FI', 'Yhtenäis', 0) ");
//			s.execute();
//			
//			aputaulu.get("taulu").setValue("pesa");
//			aputaulu.get("kentta").setValue("koord_tyyppi");
//			aputaulu.get("koodi").setValue("Y");
//			assertTrue("Row should exist", Queries.checkRowExists(aputaulu, con));
//			
//			aputaulu.get("koodi").setValue("E");
//			assertFalse("Row should not exist", Queries.checkRowExists(aputaulu, con));
//			
//			aputaulu.get("koodi").setValue("Y");
//			deleteAll("aputaulu");
//			assertFalse("Row should not exist", Queries.checkRowExists(aputaulu, con));
//		}
//		
//		public void test_delete() throws SQLException {
//			initReviiri();
//			
//			PreparedStatement getrowid = con.prepareStatement(" SELECT rowid FROM reviiri WHERE reviiriid = 1 ");
//			ResultSet rs = getrowid.executeQuery();
//			if (!rs.next()) fail();
//			String rowid = rs.getString(1);
//			rs.close();
//			
//			DatabaseTableStructure tableStructure = Queries.returnTableStructure("reviiri", con);
//			Table table = new Table(tableStructure);
//			
//			table.setRowidValue(rowid);
//			Queries.executeDelete(table, con);
//			rs = getrowid.executeQuery();
//			if (rs.next()) fail("Should not exist anymore");
//			rs.close();
//		}
//		
//		private void initReviiri() throws SQLException {
//			deleteAll("reviiri");
//			PreparedStatement insert = con.prepareStatement(" INSERT INTO reviiri (reviiriid, nimi) values (1, 'nimi') ");
//			insert.execute();
//		}
//		
//		public void test_insert() throws SQLException {
//			initReviiri();
//			DatabaseTableStructure tableStructure = Queries.returnTableStructure("reviiri", con);
//			tableStructure.get("reviiriid").setTypeToImmutablePartOfAKey();
//			
//			Table table = new Table(tableStructure);
//			table.get("reviiriid").setValue("2");
//			table.get("nimi").setValue("uusi");
//			
//			Queries.executeInsert(table, con);
//			
//			PreparedStatement resultsStatement = con.prepareStatement(" SELECT rowid, reviiriid, nimi, kirj_pvm, muutos_pvm FROM reviiri WHERE reviiriid = ? ");
//			resultsStatement.setInt(1, 2);
//			ResultSet rs = resultsStatement.executeQuery();
//			if (!rs.next()) fail();
//			assertEquals("uusi", rs.getString(3));
//			assertEquals(null, rs.getString(4));
//			assertEquals(null, rs.getString(5));
//			assertEquals(rs.getString(1), table.getRowidValue());
//			
//			tableStructure = Queries.returnTableStructure("reviiri", con);
//			tableStructure.get("reviiriid").setTypeToUniqueNumericIncreasingId();
//			tableStructure.get("kirj_pvm").setTypeToDateAddedColumn();
//			tableStructure.get("muutos_pvm").setTypeToDateModifiedColumn();
//			
//			table = new Table(tableStructure);
//			table.get("reviiriid").setValue("3");
//			table.get("nimi").setValue("toinenkin");
//			
//			Queries.executeInsert(table, con);
//			
//			resultsStatement.setInt(1, 3);
//			rs = resultsStatement.executeQuery();
//			if (!rs.next()) fail();
//			assertEquals("toinenkin", rs.getString(3));
//			assertEquals(getSysdate(), rs.getString(4));
//			assertEquals(getSysdate(), rs.getString(5));
//			assertEquals(rs.getString(1), table.getRowidValue());
//			
//			rs.close();
//		}
//		
//		public void test_update_by_rowid() throws SQLException {
//			initReviiri();
//			DatabaseTableStructure tableStructure = Queries.returnTableStructure("reviiri", con);
//			
//			PreparedStatement resultStatement = con.prepareStatement(" SELECT rowid, nimi, kirj_pvm, muutos_pvm FROM reviiri WHERE reviiriid = 1 ");
//			ResultSet rs = resultStatement.executeQuery();
//			rs.next();
//			String rowid = rs.getString(1);
//			
//			Table table = new Table(tableStructure);
//			table.setRowidValue(rowid);
//			table.get("reviiriid").setValue("1");
//			table.get("nimi").setValue("uusi nimi");
//			Queries.executeUpdate(table, con);
//			
//			rs = resultStatement.executeQuery();
//			rs.next();
//			assertEquals("uusi nimi", rs.getString(2));
//			assertEquals(null, rs.getString(3));
//			assertEquals(null, rs.getString(4));
//			
//			tableStructure.get("kirj_pvm").setTypeToDateAddedColumn();
//			tableStructure.get("muutos_pvm").setTypeToDateModifiedColumn();
//			
//			table = new Table(tableStructure);
//			table.setRowidValue(rowid);
//			table.get("reviiriid").setValue("1");
//			table.get("nimi").setValue("uusi nimi");
//			
//			Queries.executeUpdate(table, con);
//			rs = resultStatement.executeQuery();
//			rs.next();
//			assertEquals(null, rs.getString(3));
//			assertEquals(getSysdate(), rs.getString(4));
//		}
//		
//		private String getSysdate() throws SQLException {
//			PreparedStatement getSysdate = con.prepareStatement(" SELECT SYSDATE FROM dual");
//			ResultSet rs = getSysdate.executeQuery();
//			rs.next();
//			String date = rs.getString(1);
//			rs.close();
//			return date;
//		}
//		
//	}
//	
//	public static class QueriesTesting___AdvancedSearch extends TestCase {
//		
//		private TransactionConnection	con;
//		private Data<_PesaDomainModelRow>		data;
//		private _Validator	validator;
//		
//		private class TestValidator extends Validator<_PesaDomainModelRow> {
//			public TestValidator(_Data<_PesaDomainModelRow> data) {
//				super(new Request<_PesaDomainModelRow>(new RequestImpleData<_PesaDomainModelRow>().setData(data)));
//			}
//			
//			@Override
//			public void validate() {
//				checkAsSearchparameters(data.searchparameters());
//			}
//		}
//		
//		@Override
//		protected void setUp() throws Exception {
//			Thread.sleep(10); // Oracle 10g XE doesn't like opening connections too rapidly. NOTE: ALTER SYSTEM SET PROCESSES=150 SCOPE=SPFILE HELPS !!!
//			con = new PreparedStatementStoringAndClosingTransactionConnection(TestConfig.getConfig().connectionDescription());
//			con.startTransaction();
//			
//			PesaDomainModelDatabaseStructure template = new PesaDomainModelDatabaseStructure();
//			template.setPesa(new DatabaseTableStructure("adv_search_test_table"));
//			template.getPesa().addColumn("pesaid", _Column.INTEGER, 5);
//			template.getPesa().addColumn("des_leveys", _Column.DECIMAL, 5, 2);
//			template.getPesa().addColumn("sijainti", _Column.VARCHAR, 1);
//			template.getPesa().addColumn("loytopvm", _Column.DATE);
//			data = new Data<_PesaDomainModelRow>(new PesaDomainModelRowFactory(template, "", ""));
//			validator = new TestValidator(data);
//		}
//		
//		@Override
//		protected void tearDown() {
//			try {
//				con.rollbackTransaction();
//			} catch (SQLException e) {
//				fail("Couldn't rollback");
//			}
//			con.release();
//		}
//		
//		public void test__integer__() throws Exception {
//			data.searchparameters().getColumn("adv_search_test_table.pesaid").setValue("5");
//			validator.validate();
//			assertEquals(null, validator.errors().get("adv_search_test_table.pesaid"));
//			Queries.executeSearch(data.searchparameters().getPesa(), new RowsToData_Single_Table_ResultHandler<_PesaDomainModelRow>(data, "adv_search_test_table"), con);
//			assertEquals(1, data.results().size());
//		}
//		
//		public void test__integer__negative() throws Exception {
//			data.searchparameters().getColumn("adv_search_test_table.pesaid").setValue("-5");
//			validator.validate();
//			assertEquals(null, validator.errors().get("adv_search_test_table.pesaid"));
//			Queries.executeSearch(data.searchparameters().getPesa(), new RowsToData_Single_Table_ResultHandler<_PesaDomainModelRow>(data, "adv_search_test_table"), con);
//			assertEquals(1, data.results().size());
//		}
//		
//		public void test__integer__valid_range() throws Exception {
//			data.searchparameters().getColumn("adv_search_test_table.pesaid").setValue("0-5");
//			validator.validate();
//			assertEquals(null, validator.errors().get("adv_search_test_table.pesaid"));
//			Queries.executeSearch(data.searchparameters().getPesa(), new RowsToData_Single_Table_ResultHandler<_PesaDomainModelRow>(data, "adv_search_test_table"), con);
//			assertEquals(4, data.results().size());
//		}
//		
//		public void test__integer__valid_range2() throws Exception {
//			data.searchparameters().getColumn("adv_search_test_table.pesaid").setValue("0-0");
//			validator.validate();
//			assertEquals(null, validator.errors().get("adv_search_test_table.pesaid"));
//			Queries.executeSearch(data.searchparameters().getPesa(), new RowsToData_Single_Table_ResultHandler<_PesaDomainModelRow>(data, "adv_search_test_table"), con);
//			assertEquals(1, data.results().size());
//		}
//		
//		public void test__decimal__negative() throws Exception {
//			data.searchparameters().getColumn("adv_search_test_table.des_leveys").setValue("-5,44");
//			validator.validate();
//			assertEquals(null, validator.errors().get("adv_search_test_table.des_leveys"));
//			Queries.executeSearch(data.searchparameters().getPesa(), new RowsToData_Single_Table_ResultHandler<_PesaDomainModelRow>(data, "adv_search_test_table"), con);
//			assertEquals(1, data.results().size());
//		}
//		
//		public void test__decimal__valid_range() throws Exception {
//			data.searchparameters().getColumn("adv_search_test_table.des_leveys").setValue("0,0-5,6");
//			validator.validate();
//			assertEquals(null, validator.errors().get("adv_search_test_table.des_leveys"));
//			Queries.executeSearch(data.searchparameters().getPesa(), new RowsToData_Single_Table_ResultHandler<_PesaDomainModelRow>(data, "adv_search_test_table"), con);
//			assertEquals(3, data.results().size());
//		}
//		
//		public void test__decimal__valid_range3() throws Exception {
//			data.searchparameters().getColumn("adv_search_test_table.des_leveys").setValue("-1,0--1,0");
//			validator.validate();
//			assertEquals(null, validator.errors().get("adv_search_test_table.des_leveys"));
//			Queries.executeSearch(data.searchparameters().getPesa(), new RowsToData_Single_Table_ResultHandler<_PesaDomainModelRow>(data, "adv_search_test_table"), con);
//			assertEquals(1, data.results().size());
//		}
//		
//		public void test__valid_list() throws Exception {
//			data.searchparameters().getColumn("adv_search_test_table.pesaid").setValue("1|2|6");
//			validator.validate();
//			assertEquals(null, validator.errors().get("adv_search_test_table.pesaid"));
//			Queries.executeSearch(data.searchparameters().getPesa(), new RowsToData_Single_Table_ResultHandler<_PesaDomainModelRow>(data, "adv_search_test_table"), con);
//			assertEquals(2, data.results().size());
//		}
//		
//		public void test__validates_types__decimal_ok() throws Exception {
//			data.searchparameters().getColumn("adv_search_test_table.des_leveys").setValue("13,2|5.0|2.63");
//			validator.validate();
//			assertEquals(null, validator.errors().get("adv_search_test_table.des_leveys"));
//			Queries.executeSearch(data.searchparameters().getPesa(), new RowsToData_Single_Table_ResultHandler<_PesaDomainModelRow>(data, "adv_search_test_table"), con);
//			assertEquals(2, data.results().size());
//		}
//		
//		public void test__validates_types__varchar_ok() throws Exception {
//			data.searchparameters().getColumn("adv_search_test_table.sijainti").setValue("A|B|C");
//			validator.validate();
//			assertEquals(null, validator.errors().get("adv_search_test_table.sijainti"));
//			Queries.executeSearch(data.searchparameters().getPesa(), new RowsToData_Single_Table_ResultHandler<_PesaDomainModelRow>(data, "adv_search_test_table"), con);
//			assertEquals(2, data.results().size());
//		}
//		
//		public void test__valid() throws Exception {
//			data.searchparameters().getColumn("adv_search_test_table.loytopvm").setValue("1.1.2005");
//			validator.validate();
//			assertEquals(null, validator.errors().get("adv_search_test_table.loytopvm"));
//			Queries.executeSearch(data.searchparameters().getPesa(), new RowsToData_Single_Table_ResultHandler<_PesaDomainModelRow>(data, "adv_search_test_table"), con);
//			assertEquals(1, data.results().size());
//		}
//		
//		public void test__valid2() throws Exception {
//			data.searchparameters().getColumn("adv_search_test_table.loytopvm").setValue("..2005");
//			validator.validate();
//			assertEquals(null, validator.errors().get("adv_search_test_table.loytopvm"));
//			Queries.executeSearch(data.searchparameters().getPesa(), new RowsToData_Single_Table_ResultHandler<_PesaDomainModelRow>(data, "adv_search_test_table"), con);
//			assertEquals(6, data.results().size());
//		}
//		
//		public void test__valid3() throws Exception {
//			data.searchparameters().getColumn("adv_search_test_table.loytopvm").setValue("..2005-2007");
//			validator.validate();
//			assertEquals(null, validator.errors().get("adv_search_test_table.loytopvm"));
//			Queries.executeSearch(data.searchparameters().getPesa(), new RowsToData_Single_Table_ResultHandler<_PesaDomainModelRow>(data, "adv_search_test_table"), con);
//			assertEquals(8, data.results().size());
//		}
//		
//		public void test__valid5() throws Exception {
//			data.searchparameters().getColumn("adv_search_test_table.loytopvm").setValue(".8. 2005");
//			validator.validate();
//			assertEquals(null, validator.errors().get("adv_search_test_table.loytopvm"));
//			Queries.executeSearch(data.searchparameters().getPesa(), new RowsToData_Single_Table_ResultHandler<_PesaDomainModelRow>(data, "adv_search_test_table"), con);
//			assertEquals(4, data.results().size());
//		}
//		
//	}
//}
