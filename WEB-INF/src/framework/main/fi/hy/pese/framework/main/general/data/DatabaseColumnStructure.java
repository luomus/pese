package fi.hy.pese.framework.main.general.data;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class DatabaseColumnStructure implements Serializable {
	
	private static final long	serialVersionUID	= -8257148438866006529L;
	private final DatabaseTableStructure	parentTable;
	private final String					name;
	private int								type;
	private final int						datatype;
	private final int[]						size;
	private String							assosiatedSelectionName;
	private boolean							isUppercaseColumn;
	private boolean							allowWhitespace;
	private _ReferenceKey					referenceColumn;
	private Set<_ReferenceKey>				referencingColumns;
	private boolean							isRequired;
	private double							minValue = Double.NEGATIVE_INFINITY;
	private double							maxValue = Double.POSITIVE_INFINITY;
	private static final int				TEXT_OR_TEXTAREA_INPUT_TYPE_THRESHOLD	= 100;
	private static final int				DATE_SIZE								= "dd.mm.yyyy".length();
	private static final int				ROWID_SIZE								= 100;
	private boolean kirjekyyhkyRequired = false;
	
	/**
	 *
	 * @param parentTable
	 * @param name
	 * @param datatype
	 * @param size
	 * @throws IllegalArgumentException
	 */
	public DatabaseColumnStructure(DatabaseTableStructure parentTable, String name, int datatype, int... size) throws IllegalArgumentException {
		this.parentTable = parentTable;
		this.name = name;
		this.type = _Column.NORMAL_CHANGEABLE_FIELD;
		if (datatype == _Column.ROWID) this.type = _Column.ROWID;
		
		if (datatype == _Column.DATE)
			this.size = new int[] {
			                       DATE_SIZE, 0
		};
		else if (datatype == _Column.ROWID)
			this.size = new int[] {
			                       ROWID_SIZE, 0
		};
		else
			this.size = size;
		
		validateSizes(datatype, size);
		
		this.datatype = datatype;
		this.assosiatedSelectionName = "";
		this.isUppercaseColumn = false;
		this.allowWhitespace = false;
		this.referenceColumn = null;
		this.referencingColumns = new HashSet<>(3);
		this.isRequired = true;
	}
	
	private void validateSizes(int datatype, int... size) {
		switch (datatype) {
			case _Column.VARCHAR:
				onlyOneSizeParameterAllowed(size);
				break;
			case _Column.INTEGER:
				onlyOneSizeParameterAllowed(size);
				break;
			case _Column.DECIMAL:
				hasToHaveTwoSizeParameters(size);
				break;
			case _Column.DATE:
				break;
			case _Column.ROWID:
				break;
			default:
				throw new IllegalArgumentException("Invalid type");
		}
	}
	
	public void setToUppercaseColumn() {
		this.isUppercaseColumn = true;
	}
	
	private void onlyOneSizeParameterAllowed(int... size) {
		if (size.length > 1) throw new IllegalArgumentException("type has only one size");
	}
	
	private void hasToHaveTwoSizeParameters(int... size) {
		if (size.length != 2) throw new IllegalArgumentException("Decimal value has to have two size parameters");
	}
	
	public String getName() {
		return name;
	}
	
	public String getTablename() {
		return parentTable.getName();
	}
	
	private boolean columnIsVarcharColumn() {
		return getDatatype() == _Column.VARCHAR;
	}
	
	public void setTypeToUniqueNumericIncreasingId() {
		if (datatype != _Column.INTEGER) throw new IllegalStateException("Not numeric");
		if (parentTable.hasUniqueNumericIdColumn()) {
			if (parentTable.getUniqueNumericIdColumn().getName().equals(getName())) return;
			throw new IllegalStateException("Table already has unique key");
		}
		if (parentTable.hasImmutableKeys()) throw new IllegalStateException("Table already has immutable keys; can't set unique key");
		
		type = _Column.UNIQUE_NUMERIC_INCREASING_ID;
		parentTable.setIdColumn(this.name);
	}
	
	public void setTypeToImmutablePartOfAKey() {
		if (parentTable.hasUniqueNumericIdColumn()) throw new IllegalStateException("Table already has unique key");
		type = _Column.IMMUTABLE_PART_OF_A_KEY;
		parentTable.addToImmutableKeys(this.name);
	}
	
	public void setTypeToNormalChangeableField() {
		type = _Column.NORMAL_CHANGEABLE_FIELD;
	}
	
	public void setTypeToDateModifiedColumn() {
		if (datatype != _Column.DATE) throw new IllegalStateException("Not a date");
		type = _Column.DATE_MODIFIED_COLUMN;
	}
	
	public void setTypeToDateAddedColumn() {
		if (datatype != _Column.DATE) throw new IllegalStateException("Not a date");
		type = _Column.DATE_ADDED_COLUMN;
	}
	
	public void setTypeToPasswordColumn() {
		type = _Column.PASSWORD_COLUMN;
	}
	
	public void setTypeToUserIdColumn() {
		type = _Column.USER_ID_COLUMN;
	}
	
	public int getFieldType() {
		return type;
	}
	
	public int getDatatype() {
		return datatype;
	}
	
	public int getSize() {
		return size[0];
	}
	
	public int getDecimalSize() throws UnsupportedOperationException {
		exceptionifNotDecimal();
		return size[1];
	}
	
	private void exceptionifNotDecimal() {
		if (type != _Column.DECIMAL) throw new UnsupportedOperationException("Not a decimal column");
	}
	
	public String getModifiability() {
		if (getFieldType() != _Column.NORMAL_CHANGEABLE_FIELD && getFieldType() != _Column.PASSWORD_COLUMN)
			return _Column.MODIFIABLE_NO;
		return _Column.MODIFIABLE_YES;
	}
	
	public String getInputType() {
		if (isPasswordColumn()) return _Column.INPUT_TYPE_PASSWORD;
		if (hasAssosiatedSelection()) return _Column.INPUT_TYPE_SELECTION;
		if (canContainLongTexts()) return _Column.INPUT_TYPE_TEXTAREA;
		if (isDateColumn()) return _Column.INPUT_TYPE_DATE;
		if (isRowidColumn()) return _Column.INPUT_TYPE_ROWID;
		return _Column.INPUT_TYPE_TEXT;
	}
	
	private boolean isRowidColumn() {
		return getDatatype() == _Column.ROWID;
	}
	
	private boolean isDateColumn() {
		return getDatatype() == _Column.DATE;
	}
	
	private boolean isPasswordColumn() {
		return getFieldType() == _Column.PASSWORD_COLUMN;
	}
	
	public boolean hasAssosiatedSelection() {
		return (assosiatedSelectionName.length() > 0);
	}
	
	private boolean canContainLongTexts() {
		return columnIsVarcharColumn() && getSize() > TEXT_OR_TEXTAREA_INPUT_TYPE_THRESHOLD;
	}
	
	public void setAssosiatedSelection(String selectionName) {
		if (selectionName == null) selectionName = "";
		this.assosiatedSelectionName = selectionName;
		if (selectionName.length() > 0) {
			this.isUppercaseColumn = true;
		} else {
			this.isUppercaseColumn = false;
		}
	}
	
	public String getAssosiatedSelection() {
		return assosiatedSelectionName;
	}
	
	public int getInputSize() {
		if (getDatatype() == _Column.DATE) return 1;
		if (getDatatype() == _Column.DECIMAL) return getSize() + 1 + getDecimalSize();
		return getSize();
	}
	
	@Override
	public String toString() {
		return super.toString() + " " + getFullname();
	}
	
	private String getFullname() {
		return parentTable.getName() + "." + getName();
	}
	
	public boolean isUppercaseColumn() {
		return isUppercaseColumn;
	}
	
	public DatabaseColumnStructure references(DatabaseColumnStructure otherColumn) {
		this.referenceColumn = new ReferenceKey(otherColumn.getTablename(), otherColumn.getName());
		otherColumn.referencedBy(this);
		return this;
	}
	
	public void referencedBy(DatabaseColumnStructure otherColumn) {
		if (otherColumn.getReferenceColumn() == null || !otherColumn.getReferenceColumn().equals(this.getFullname())) { throw new IllegalStateException("References should be set using references(Column otherColumn) -method, not the other way around."); }
		referencingColumns.add(new ReferenceKey(otherColumn.getTablename(), otherColumn.getName()));
	}
	
	public _ReferenceKey getReferenceColumn() {
		return referenceColumn;
	}
	
	public Set<_ReferenceKey> getReferencingColumns() {
		return referencingColumns;
	}
	
	public void copyReferences(DatabaseColumnStructure sourceColumn) {
		this.referenceColumn = sourceColumn.getReferenceColumn();
		this.referencingColumns = sourceColumn.getReferencingColumns();
	}
	
	public boolean isRequired() {
		return isRequired;
	}
	
	public void setToOptionalColumn() {
		this.isRequired = false;
	}
	
	public void setValueLimits(double minValue, double maxValue) {
		if (notNumeric()) throw new UnsupportedOperationException("Not a numeric value column");
		this.minValue = minValue;
		this.maxValue = maxValue;
	}
	
	private boolean notNumeric() {
		return datatype != _Column.DECIMAL && datatype != _Column.INTEGER;
	}
	
	public double getMinValue() {
		return minValue;
	}
	
	public double getMaxValue() {
		return maxValue;
	}
	
	public void setAllowWhitespace(boolean b) {
		this.allowWhitespace = b;
	}
	
	public boolean allowWhitespace() {
		return allowWhitespace;
	}
	
	public void setKirjekyyhkyRequired() {
		this.kirjekyyhkyRequired = true;
	}
	
	public boolean kirjekyyhkyRequired() {
		return this.kirjekyyhkyRequired;
	}
	
}
