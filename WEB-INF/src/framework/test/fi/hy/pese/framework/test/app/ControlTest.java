package fi.hy.pese.framework.test.app;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import fi.hy.pese.framework.main.app.BaseFactoryForApplicationSpecificationFactory;
import fi.hy.pese.framework.main.app.Control;
import fi.hy.pese.framework.main.app._Functionality;
import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.app.functionality.validate._Validator;
import fi.hy.pese.framework.main.db._DAO;
import fi.hy.pese.framework.main.db.oraclesql.OracleSQL_DAO;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data.ParameterMap;
import fi.hy.pese.framework.main.general.data.PesaDomainModelDatabaseStructure;
import fi.hy.pese.framework.main.general.data.PesaDomainModelRowFactory;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.main.general.data._Row;
import fi.hy.pese.framework.main.view._ExceptionViewer;
import fi.hy.pese.framework.main.view._Viewer;
import fi.hy.pese.framework.test.TestConfig;
import fi.hy.pese.framework.test.app.functionality.EmptyFunctionality;
import fi.hy.pese.framework.test.app.functionality.FakeHttpRedirected;
import fi.luomus.commons.containers.Selection;
import fi.luomus.commons.session.SessionHandler;
import fi.luomus.commons.session.SessionHandlerImple;
import fi.luomus.java.tests.commons.HttpSessionStub;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class ControlTest {

	public static Test suite() {
		return new TestSuite(ControlTest.class.getDeclaredClasses());
	}

	public static class DAOandExceptions extends TestCase {

		private static final String			FUNCTIONALITY_THAT_ALWAYS_THROWS_EXCEPTION	= "throwException";
		private Control<_PesaDomainModelRow>					control;
		private PrintWriter					out;
		private PrintStream					defaultSystemOut;
		private PrintStream					defaultSystemErr;
		private ControlTestDAO				fakeDao;
		private ControlTestExceptionViewer	fakeExceptionViewer;
		private SessionHandler				session;

		private final PrintStream			stream = new PrintStream(new OutputStream() {
			@Override
			public void write(int b) throws IOException {}
		});

		private class ControlTestFactory extends BaseFactoryForApplicationSpecificationFactory<_PesaDomainModelRow> {

			public ControlTestFactory(String configFileName) throws FileNotFoundException {
				super(configFileName);
			}

			public int		timesAskedForDao	= 0;
			public boolean	daoReleased			= false;

			@Override
			public _DAO<_PesaDomainModelRow> dao(String userId) {
				timesAskedForDao++;
				return fakeDao;
			}

			@Override
			public void release(_DAO<_PesaDomainModelRow> dao) {
				daoReleased = true;
			}

			@Override
			public _Functionality<_PesaDomainModelRow> functionalityFor(String page, _Request<_PesaDomainModelRow> request) {
				if (page.equals(FUNCTIONALITY_THAT_ALWAYS_THROWS_EXCEPTION)) { return new IThrowException(); }
				return new EmptyFunctionality<>();
			}

			@Override
			public _ExceptionViewer exceptionViewer(_Request<_PesaDomainModelRow> request) {
				return fakeExceptionViewer;
			}

			@Override
			public _Viewer viewer() {
				return new ControlTestViewer();
			}

			@Override
			public Map<String, Selection> selections(_DAO<_PesaDomainModelRow> dao) {
				return selections;
			}

			@Override
			public void reportException(String message, Throwable condition) {

			}

			@Override
			protected Map<String, Selection> fetchSelections(_DAO<_PesaDomainModelRow> dao) throws Exception {
				return null;
			}
		}

		private class ControlTestViewer implements _Viewer {

			@Override
			public void viewFor(_Request<? extends _Row> request) throws Exception {
			}
		}

		private class IThrowException implements _Functionality<_PesaDomainModelRow> {
			@Override
			public void afterFailedValidation() throws Exception {}

			@Override
			public void afterSuccessfulValidation() throws Exception {}

			@Override
			public void postProsessingHook() throws Exception {}

			@Override
			public void preProsessingHook() throws Exception {
				throw new Exception();
			}

			@Override
			public _Validator validator() {
				return null;
			}

			@Override
			public _Request<_PesaDomainModelRow> functionalityRequest() {
				return null;
			}
		}

		private class ControlTestDAO extends OracleSQL_DAO<_PesaDomainModelRow> {

			public boolean	rolledBack;
			public boolean	commited;
			public boolean	transactionStarted;

			public ControlTestDAO() {
				super(null, new PesaDomainModelRowFactory(new PesaDomainModelDatabaseStructure(), "", ""));
				rolledBack = false;
				commited = false;
				transactionStarted = false;
			}

			@Override
			public void commit() throws SQLException {
				commited = true;
			}

			@Override
			public void rollback() throws SQLException {
				rolledBack = true;
			}

			@Override
			public void startTransaction() {
				transactionStarted = true;
			}
		}

		private class ControlTestExceptionViewer implements _ExceptionViewer {

			public boolean	hasBeenCalled;

			public ControlTestExceptionViewer() {
				hasBeenCalled = false;
			}

			@Override
			public void view(Throwable condition) {
				hasBeenCalled = true;
				condition.printStackTrace();
			}

		}

		private ControlTestFactory	factory;

		@Override
		protected void setUp() throws Exception {
			defaultSystemOut = System.out;
			defaultSystemErr = System.err;

			fakeExceptionViewer = new ControlTestExceptionViewer();
			fakeDao = new ControlTestDAO();
			factory = new ControlTestFactory(TestConfig.getConfigFile());
			factory.setRowFactory(new PesaDomainModelRowFactory(new PesaDomainModelDatabaseStructure(), "", ""));
			control = new Control<>(factory);
			out = new PrintWriter(stream);
			session = new SessionHandlerImple(new HttpSessionStub(), factory.config().systemId());
			session.authenticateFor(factory.config().systemId());
			System.setOut(stream);
			System.setErr(stream);
		}

		@Override
		protected void tearDown() {
			System.setOut(defaultSystemOut);
			System.setErr(defaultSystemErr);
		}

		public void test_it_doesnt_throw_exceptions() {
			makeCallThatDoesntThrowException();
			assertFalse("Should not throw exception", fakeExceptionViewer.hasBeenCalled);
		}

		private void makeCallThatDoesntThrowException() {
			control.process(new ParameterMap(new HashMap<String, String>()), session, out, new FakeHttpRedirected(), null);
		}

		public void test_it_throws_exception() {
			makeCallThatThrowsException();
			assertTrue("Should thrown exception", fakeExceptionViewer.hasBeenCalled);
		}

		private void makeCallThatThrowsException() {
			Map<String, String> params = new HashMap<>();
			params.put(Const.PAGE, FUNCTIONALITY_THAT_ALWAYS_THROWS_EXCEPTION);
			control.process(new ParameterMap(params), session, out, new FakeHttpRedirected(), null);
		}

		public void test_it_calls_rollback_for_dao_if_exception_thrown() {
			makeCallThatThrowsException();
			assertTrue("Has been rolled back", fakeDao.rolledBack);
		}

		public void test_it_doesnt_rollback_if_no_exception() {
			makeCallThatDoesntThrowException();
			assertFalse(fakeDao.rolledBack);
		}

		public void test_it_commits_if_no_exception_thrown() {
			makeCallThatDoesntThrowException();
			assertTrue(fakeDao.commited);
		}

		public void test_it_doesnt_commit_if_exception_thrown() {
			makeCallThatThrowsException();
			assertFalse(fakeDao.commited);
		}

		public void test_it_releases_dao_after_call___no_exception() {
			makeCallThatDoesntThrowException();
			assertTrue(factory.daoReleased);
		}

		public void test_it_releases_dao_after_call___exception() {
			makeCallThatThrowsException();
			assertTrue(factory.daoReleased);
		}

		public void test_it_starts_transactoin() {
			makeCallThatThrowsException();
			assertTrue(fakeDao.transactionStarted);
			fakeDao.transactionStarted = false;
			makeCallThatDoesntThrowException();
			assertTrue(fakeDao.transactionStarted);
		}

		public void test_it___fetches_a_new_dao_from_the_factory_for_each_call() {
			// Each request uses it's own DAO that is commited/rolled back and released.
			// Requests must not share the same DAO == database connection

			makeCallThatDoesntThrowException();
			assertEquals(1, factory.timesAskedForDao);
			makeCallThatDoesntThrowException();
			assertEquals(2, factory.timesAskedForDao);
			makeCallThatThrowsException();
			assertEquals(3, factory.timesAskedForDao);
		}

		public void test_if_language_given_not_found_from_configfile_uses_default_language() {
			Map<String, String> params = new HashMap<>();
			params.put(Const.LANGUAGE, "FOoo");
			control.process(new ParameterMap(params), session, out, new FakeHttpRedirected(), null);
			assertEquals("FI", session.get(Const.LANGUAGE));
		}

		public void test_if_language_given_sets_language_to_session() {
			Map<String, String> params = new HashMap<>();
			params.put(Const.LANGUAGE, "EN");
			control.process(new ParameterMap(params), session, out, new FakeHttpRedirected(), null);
			assertEquals("EN", session.get(Const.LANGUAGE));
		}

		// More tests: applications.testapp.test

	}

	/*
	 * public static class Tests extends TestCase {
	 * protected void setUp() throws Exception {
	 * }
	 * protected void tearDown() throws Exception {
	 * }
	 * public void test_() {
	 * }
	 * }
	 */
}
