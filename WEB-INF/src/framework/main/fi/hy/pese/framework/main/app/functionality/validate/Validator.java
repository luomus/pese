package fi.hy.pese.framework.main.app.functionality.validate;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;

import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.db._DAO;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data._Column;
import fi.hy.pese.framework.main.general.data._Data;
import fi.hy.pese.framework.main.general.data._ReferenceKey;
import fi.hy.pese.framework.main.general.data._Row;
import fi.hy.pese.framework.main.general.data._Table;
import fi.luomus.commons.containers.DateValue;
import fi.luomus.commons.containers.Selection;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.Utils;

public abstract class Validator<T extends _Row> implements _Validator {

	// TODO checkreferencevalues pitäisi olla checkasupdateparameters ja ei kutsua missään erikseen (?)

	
	private final static Collection<Character>		DIGITS		= Utils.collection('0', '1', '2', '3', '4', '5', '6', '7', '8', '9');
	private final static Collection<Character>		ALPHABETS	= initAlphabets();
	private final static Collection<Character>		ALLOWED_SPECIALS	= Utils.collection('-', ' ', '+', '?', '.', ',');

	private static Collection<Character> initAlphabets() {
		Collection<Character> alphabets = new HashSet<>();
		for (Character ch : Utils.collection('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'Å', 'Ä', 'Ö')) {
			alphabets.add(ch);
			alphabets.add(ch.toString().toLowerCase().charAt(0));
		}
		return alphabets;
	}

	protected final LinkedHashMap<String, String>	errors;
	protected final LinkedHashMap<String, String>	warnings;
	protected final _Data<T>						data;
	protected final _DAO<T>						dao;
	protected final _Request<T>					request;

	public Validator(_Request<T> request) {
		this.errors = new LinkedHashMap<>();
		this.warnings = new LinkedHashMap<>();
		this.data = request.data();
		this.dao = request.dao();
		this.request = request;
	}

	@Deprecated
	public Validator() {
		this.errors = new LinkedHashMap<>();
		this.warnings = new LinkedHashMap<>();
		this.data = null;
		this.dao = null;
		this.request = null;
	}

	@Override
	public abstract void validate() throws Exception;

	@Override
	public boolean hasErrors() {
		return !errors.isEmpty();
	}

	protected boolean hasErrors(_Table table) {
		for (String fieldName : errors.keySet()) {
			if (!fieldName.contains(".")) continue;
			String tableNamePart = fieldName.substring(0, fieldName.lastIndexOf("."));
			if (tableNamePart.equals(table.getFullname())) {
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean noErrors() {
		return !hasErrors();
	}

	@Override
	public LinkedHashMap<String, String> errors() {
		return errors;
	}

	@Override
	public boolean hasWarnings() {
		return !warnings.isEmpty();
	}

	@Override
	public boolean noWarnings() {
		return !hasWarnings();
	}

	@Override
	public Map<String, String> warnings() {
		return warnings;
	}

	protected void removeError(_Column c) {
		errors.remove(c.getFullname());
	}

	protected void removeError(String field) {
		errors.remove(field);
	}

	protected void error(String field, String error) {
		errors.put(field, error);
	}

	protected void error(_Column c, String error) {
		errors.put(c.getFullname(), error);
	}

	protected String getError(_Column c) {
		String error = errors.get(c.getFullname());
		if (error == null) return "";
		return error;
	}

	protected void removeWarning(_Column c) {
		warnings.remove(c.getFullname());
	}

	protected void removeWarning(String field) {
		warnings.remove(field);
	}

	protected void warning(String field, String warning) {
		if (!data.bypassedWarnings().contains(field)) {
			warnings.put(field, warning);
		}
	}

	protected void warning(_Column c, String warning) {
		this.warning(c.getFullname(), warning);
	}

	protected String getWarning(_Column c) {
		String warning = warnings.get(c.getFullname());
		if (warning == null) return "";
		return warning;
	}

	protected void removeErrorsAndWarnings(_Column c) {
		removeError(c);
		removeWarning(c);
	}

	protected void checkAsSearchparameters(T parameters) {
		for (_Table table : parameters) {
			if (table.hasValues()) checkAsSearchparameters(table);
		}
	}

	protected void checkAsUpdateparameters(T parameters) {
		for (_Table table : parameters) {
			checkAsUpdateparameters(table);
		}
	}

	protected void checkAsSearchparameters(_Table table) {
		for (_Column column : table) {
			checkAsSearchparameter(column);
		}
	}

	protected void checkAsUpdateparameters(_Table table) {
		for (_Column column : table) {
			checkAsUpdateparameter(column);
		}
	}

	protected void checkAsSearchparameter(_Column c) {
		if (c.getValue().contains("=")) {
			error(c, CAN_NOT_CONTAIN_EQUALS_SIGN);
			return;
		}
		if (c.getDatatype() == _Column.DATE) {
			checkDateAsSearchparameter(c);
			return;
		}
		if (c.getValue().contains("|")) {
			checkAsValueList(c);
			return;
		}
		if (c.getDatatype() == _Column.VARCHAR) { return; }
		if (c.getDatatype() == _Column.INTEGER) {
			checkAsNumericSearchRange(c);
			return;
		}
		if (c.getDatatype() == _Column.DECIMAL) {
			checkAsNumericSearchRange(c);
			return;
		}
	}

	protected void checkDateAsSearchparameter(_Column c) {
		checkDateAsSearchparameter(c.getFullname(), c.getDateValue());
	}

	protected void checkDateAsSearchparameter(String fieldName, DateValue date) {
		String day = date.getDay();
		String month = date.getMonth();
		String year = date.getYear();
		if (isNull(day) && isNull(month) && isNull(year)) return;
		for (String v : Utils.iterable(day, month, year)) {
			if (!checkDoesNotContainSeparator(fieldName, v)) return;
		}
		if (!isInteger(day) || !isInteger(month)) {
			error(fieldName, INVALID_DATE);
			return;
		}
		if (isNull(year)) {
			error(fieldName, INVALID_DATE);
			return;
		}
		if (!isNull(day) && isNull(month)) {
			error(fieldName, INVALID_DATE);
			return;
		}
		if (year.contains("-")) {
			if (!isNull(day) || !isNull(month)) {
				error(fieldName, INVALID_DATE_RANGE);
				return;
			}
			String[] yearRange = year.split("-");
			if (yearRange.length != 2) {
				error(fieldName, INVALID_DATE_RANGE);
				return;
			}
			if (isNull(yearRange[0]) || isNull(yearRange[1])) {
				error(fieldName, INVALID_DATE_RANGE);
				return;
			}
			if (!isInteger(yearRange[0]) || !isInteger(yearRange[1])) {
				error(fieldName, INVALID_DATE);
				return;
			}
			if (Integer.parseInt(yearRange[0]) > Integer.parseInt(yearRange[1])) {
				error(fieldName, INVALID_DATE_RANGE);
				return;
			}
		} else {
			if (!isInteger(year)) {
				error(fieldName, INVALID_DATE);
				return;
			}
		}
	}

	protected void checkAsValueList(_Column c) {
		if (c.getValueList().size() >= 999) {
			error(c, _Validator.TOO_MANY_SELECTED);
			return;
		}
		if (checkAsValueList(c.getFullname(), c.getValue())) {
			if (c.getDatatype() == _Column.INTEGER)
				checkAsIntegerValueList(c.getFullname(), c.getValue());
			else if ((c.getDatatype() == _Column.DECIMAL)) {
				checkAsDecimalValueList(c.getFullname(), c.getValue());
			}
		}
	}

	protected boolean checkAsValueList(String fieldName, String value) {
		if (value.startsWith("|") || value.endsWith("|") || value.contains("||")) {
			error(fieldName, INVALID_VALUE_LIST);
			return false;
		}
		return true;
	}

	protected void checkAsIntegerValueList(String fieldName, String value) {
		for (String v : value.split("\\|")) {
			if (v.equals(Const.VALUE_IS_NULL)) continue;
			if (!checkInteger(fieldName, v)) return;
		}
	}

	protected void checkAsDecimalValueList(String fieldName, String value) {
		for (String v : value.split("\\|")) {
			if (v.equals(Const.VALUE_IS_NULL)) continue;
			if (!checkDecimal(fieldName, v)) return;
		}
	}

	private interface NumericRangeValidator {
		boolean check(String fieldName, String value);

		boolean firstLargerThanSecond(String[] range);
	}

	protected class IntegerSearchParameterRangeValidator implements NumericRangeValidator {
		@Override
		public boolean check(String fieldName, String value) {
			return checkInteger(fieldName, value);
		}

		@Override
		public boolean firstLargerThanSecond(String[] range) {
			return firstIsLargerThanSecond(range[0], range[1]);
		}

	}

	protected boolean firstIsLargerThanSecond(_Column first, _Column second) {
		return firstIsLargerThanSecond(first.getValue(), second.getValue());
	}

	protected boolean firstIsLargerThanSecond(String first, String second) {
		first = first.replace(",", ".");
		second = second.replace(",", ".");
		return Double.parseDouble(first) > Double.parseDouble(second);
	}

	protected  class DecimalSearchParameterRangeValidator implements NumericRangeValidator {
		@Override
		public boolean check(String fieldName, String value) {
			return checkDecimal(fieldName, value);
		}

		@Override
		public boolean firstLargerThanSecond(String[] range) {
			return Double.parseDouble(range[0].replace(",", ".")) > Double.parseDouble(range[1].replace(",", "."));
		}
	}

	protected boolean checkAsNumericSearchRange(_Column c) {
		String value = c.getValue();
		if (value.equals(Const.VALUE_IS_NULL)) return false;
		if (value.startsWith(Const.VALUE_IS_NOT)) {
			value = value.substring(1);
		}
		NumericRangeValidator typeValidator = null;
		if (c.getDatatype() == _Column.INTEGER) {
			typeValidator = new IntegerSearchParameterRangeValidator();
		}
		if (c.getDatatype() == _Column.DECIMAL) {
			typeValidator = new DecimalSearchParameterRangeValidator();
		}
		return checkAsNumericSearchRange(c.getFullname(), value, typeValidator);
	}

	protected boolean checkAsNumericSearchRange(String fieldName, String value, NumericRangeValidator typeValidator) {
		int count = Utils.countNumberOf("-", value);
		if (count == 0) {
			return typeValidator.check(fieldName, value);
		}
		if (count == 1 && value.startsWith("-")) {
			return typeValidator.check(fieldName, value);
		}
		String[] range;
		if (count == 1) {
			range = value.split("-");
			if (range.length != 2) {
				error(fieldName, INVALID_RANGE);
				return false;
			}
		} else if (count == 2) {
			if (!value.startsWith("-")) {
				error(fieldName, INVALID_RANGE);
				return false;
			}
			range = extractRange(value);
		} else if (count == 3) {
			range = extractRange(value);
		} else {
			error(fieldName, INVALID_RANGE);
			return false;
		}
		if (!typeValidator.check(fieldName, range[0]) || !typeValidator.check(fieldName, range[1])) { return false; }
		if (typeValidator.firstLargerThanSecond(range)) { error(fieldName, INVALID_RANGE); return false; }
		return true;
	}

	private String[] extractRange(String value) {
		String[] range = new String[2];
		int separatorIndex = value.indexOf("-", 1);
		range[0] = value.substring(0, separatorIndex);
		range[1] = value.substring(separatorIndex + 1);
		return range;
	}

	protected void checkAsUpdateparameter(_Column c) {
		if (!c.hasValue()) return;

		checkDoesNotContainSeparator(c);
		checkDoesNotStartWithNegation(c);
		checkAssosiatedSelectionValue(c);
		if (c.getDatatype() == _Column.INTEGER) {
			checkInteger(c);
		} else if (c.getDatatype() == _Column.DECIMAL) {
			checkDecimal(c);
		} else if (c.getDatatype() == _Column.DATE) {
			checkDate(c);
		}
		checkSize(c);
		if (c.isUppercaseColumn()) {
			checkIsAlphaNumeric(c);
		}
	}

	protected boolean checkAssosiatedSelectionValue(_Column c) {
		if (c.hasAssosiatedSelection() && c.hasValue() && noErrors(c)) {
			Selection selection = data.selections().get(c.getAssosiatedSelection());
			if (selection == null) {
				throw new IllegalStateException("Selection ("+c.getAssosiatedSelection()+") defined for " +c.getFullname() + " but not found in data");
			}
			if (!selection.containsOption(c.getValue())) {
				error(c, ASSOSIATED_SELECTION_DOES_NOT_CONTAIN_THE_VALUE);
				return false;
			}
		}
		return true;
	}

	private boolean checkDoesNotContainSeparator(_Column c) {
		return checkDoesNotContainSeparator(c.getFullname(), c.getValue());
	}

	private boolean checkDoesNotContainSeparator(String fieldName, String value) {
		if (value.contains("|")) {
			error(fieldName, CAN_NOT_CONTAIN_SEPARATOR);
			return false;
		}
		return true;
	}

	private boolean checkDoesNotStartWithNegation(_Column c) {
		return checkDoesNotStartWithNegation(c.getFullname(), c.getValue());
	}

	private boolean checkDoesNotStartWithNegation(String fieldName, String value) {
		if (value.startsWith(Const.VALUE_IS_NOT)) {
			error(fieldName, CAN_NOT_START_WITH_NEGATION);
			return false;
		}
		return true;
	}

	protected void checkSize(_Column c) {
		if (c.getDatatype() == _Column.DECIMAL)
			checkDecimalSize(c.getFullname(), c.getValue(), c.getSize(), c.getDecimalSize());
		else
			checkSize(c.getFullname(), c.getValue(), c.getSize());
	}

	protected void checkDecimalSize(String fieldName, String value, int precision, int scale) {
		String[] values = Utils.splitDecimal(value);
		if (values[0].length() > precision - scale) error(fieldName, TOO_LONG);
		if (values[1].length() > scale) error(fieldName, TOO_MANY_DECIMALS);
	}

	protected void checkSize(String fieldName, String value, int maxSize) {
		if (value.length() > maxSize) {
			error(fieldName, TOO_LONG);
		}
	}

	protected boolean checkDate(_Column c) {
		return checkDate(c.getFullname(), c.getDateValue());
	}

	protected boolean checkDate(String fieldName, String value) {
		if ("".equals(value)) return true;
		return checkDate(fieldName, DateUtils.convertToDateValue(value));
	}

	protected boolean checkDate(String fieldName, DateValue date) {
		if (date.isEmpty()) return true;
		if (date.hasEmptyFields()) {
			error(fieldName, INVALID_DATE);
			return false;
		}
		if (!containsIntegers(Utils.collection(date.getYear(), date.getMonth(), date.getDay()))) {
			error(fieldName, INVALID_DATE);
			return false;
		}
		int dd = Integer.parseInt(date.getDay());
		int mm = Integer.parseInt(date.getMonth());
		int yyyy = Integer.parseInt(date.getYear());

		if (dd < 1 || dd > 31) {
			error(fieldName, INVALID_DATE);
			return false;
		}
		if (mm < 1 || mm > 12) {
			error(fieldName, INVALID_DATE);
			return false;
		}
		if ((yyyy < MIN_YEAR || yyyy > MAX_YEAR) && yyyy != SPECIAL_YEAR_VALUE_FOR_UNKNOWN_YEAR) {
			error(fieldName, INVALID_DATE);
			return false;
		}

		Calendar k = Calendar.getInstance();
		k.setLenient(false);
		try {
			k.set(yyyy, mm - 1, dd);
			k.getTime();
		} catch (IllegalArgumentException e) {
			error(fieldName, NOT_A_CALENDAR_DATE);
			return false;
		}
		if (k.after(now())) {
			error(fieldName, CAN_NOT_BE_IN_THE_FUTURE);
			return false;
		}
		return true;
	}

	private Calendar now() {
		Calendar k = Calendar.getInstance();
		k.add(Calendar.SECOND, 1); // So that <somedate>.after(now()) will retun true if <somedate> and <now()> are the exact same moment
		return k;
	}

	protected boolean containsIntegers(Collection<String> values) {
		for (String value : values) {
			if (!isInteger(value)) return false;
		}
		return true;
	}

	protected boolean isNull(_Column c) {
		return isNull(c.getValue());
	}

	protected boolean isNull(_Column ... columns) {
		for (_Column c : columns) {
			if (isNull(c)) continue;
			return false;
		}
		return true;
	}

	protected boolean isNull(String value) {
		return value == null || value.length() < 1;
	}

	protected boolean checkDecimal(_Column c) {
		boolean valid = checkDecimal(c.getFullname(), c.getValue());
		if (!valid) return false;
		return checkNumericRange(c);
	}

	protected boolean checkDecimal(String fieldName, String value) {
		if (value.endsWith(",") || value.endsWith(".")) {
			error(fieldName, INVALID_VALUE);
			return false;
		}
		String[] decimalValues = Utils.splitDecimal(value);
		if (!isInteger(decimalValues[0]) || !isInteger(decimalValues[1])) {
			error(fieldName, MUST_BE_DECIMAL);
			return false;
		}
		if (decimalValues[1].length() > 0 && decimalValues[0].length() < 1) {
			error(fieldName, INVALID_VALUE);
			return false;
		}
		return true;
	}

	protected boolean checkInteger(_Column c) {
		boolean valid = checkInteger(c.getFullname(), c.getValue());
		if (!valid) return false;
		return checkNumericRange(c);
	}

	private boolean checkNumericRange(_Column c) {
		if (!c.hasValue()) return true;
		if (hasErrors(c)) return false;
		try {
			double value = c.getDoubleValue();
			if (value < c.getMinValue()) {
				error(c, _Validator.TOO_SMALL);
				return false;
			}
			if (value > c.getMaxValue()) {
				error(c, _Validator.TOO_LARGE);
				return false;
			}
		} catch (NumberFormatException e) {
			error(c, _Validator.INVALID_VALUE);
			return false;
		}
		return true;
	}

	protected boolean checkInteger(String fieldName, String value) {
		if (value == null) return true;
		if (value.length() < 1) return true;
		if (!isInteger(value)) {
			error(fieldName, MUST_BE_INTEGER);
			return false;
		}
		//		if (nonZeroIntegerStartsWithZero(value)) {
		//			error(fieldName, INVALID_VALUE);
		//			return false;
		//		}
		return true;
	}

	//	private boolean nonZeroIntegerStartsWithZero(String value) {
	//		return value.length() > 1 && value.charAt(0) == '0';
	//	}

	protected boolean isInteger(String value) {
		if (value == null) return true;
		if (value.length() < 1) return true;
		if (value.startsWith("-")) {
			if (value.equals("-")) return false;
			value = value.substring(1);
		}
		for (char ch : value.toCharArray()) {
			if (!DIGITS.contains(ch)) { return false; }
		}
		return true;
	}

	protected boolean checkIsAlphaNumeric(_Column c) {
		return checkIsAlphaNumeric(c.getFullname(), c.getValue());
	}

	protected boolean checkIsAlphaNumeric(String fieldName, String value) {
		if (!isAlphaNumeric(value)) {
			error(fieldName, CAN_NOT_CONTAIN_SPECIAL_CHARS);
			return false;
		}
		return true;
	}

	protected boolean isAlphaNumeric(String value) {
		for (char ch : value.toCharArray()) {
			if (!DIGITS.contains(ch) && !ALPHABETS.contains(ch) && !ALLOWED_SPECIALS.contains(ch)) { return false; }
		}
		return true;
	}

	protected boolean isAlphabet(char ch) {
		return ALPHABETS.contains(ch);
	}

	protected void checkRequiredNormalValuesAreGiven(_Table table) {
		for (_Column c : table) {
			if (c.getFieldType() == _Column.UNIQUE_NUMERIC_INCREASING_ID) continue;
			if (c.getFieldType() == _Column.DATE_ADDED_COLUMN) continue;
			if (c.getFieldType() == _Column.DATE_MODIFIED_COLUMN) continue;
			if (c.getFieldType() == _Column.ROWID) continue;
			if (c.getFieldType() == _Column.USER_ID_COLUMN) continue;
			if (c.isRequired()) {
				checkNotNull(c);
			}
		}
	}

	protected void checkNotNull(_Column ... columns) {
		for (_Column c : columns) {
			checkNotNull(c);
		}
	}

	protected boolean checkNotNull(_Column c) {
		return checkNotNull(c.getFullname(), c.getValue());
	}

	protected boolean checkNotNull(String fieldName, String value) {
		if (isNull(value)) {
			error(fieldName, CAN_NOT_BE_NULL);
			return false;
		}
		return true;
	}

	protected boolean checkValidYear(_Column c) {
		return checkValidYear(c.getFullname(), c.getValue());
	}

	protected void warnYearRange(_Column c) {
		warnYearRange(c.getFullname(), c.getValue());
	}

	protected void warnYearRange(String fieldName, String value) {
		if (hasErrors(fieldName)) return;
		int year = Integer.valueOf(value);
		int currentYear = DateUtils.getCurrentYear();
		if (year <= currentYear - YEAR_WARNING_THRESHOLD) {
			warning(fieldName, ARE_YOU_SURE);
		}
	}

	protected boolean checkValidYear(String fieldName, String value) {
		if (!checkNotNull(fieldName, value)) return false;
		if (!checkInteger(fieldName, value)) return false;

		int year = Integer.valueOf(value);
		int currentYear = DateUtils.getCurrentYear();
		if (year > currentYear) {
			error(fieldName, CAN_NOT_BE_IN_THE_FUTURE);
			return false;
		}
		if (year < MIN_YEAR && year != SPECIAL_YEAR_VALUE_FOR_UNKNOWN_YEAR) {
			error(fieldName, TOO_SMALL);
			return false;
		}

		return true;
	}

	protected boolean noErrors(_Column c) {
		return noErrors(c.getFullname());
	}

	protected boolean noErrors(String fieldName) {
		return !errors.containsKey(fieldName);
	}

	protected boolean hasErrors(_Column c) {
		return !noErrors(c);
	}

	protected boolean hasErrors(String fieldname) {
		return !noErrors(fieldname);
	}

	protected boolean hasErrors(_Column ... columns) {
		for (_Column c : columns) {
			if (hasErrors(c)) return true;
		}
		return false;
	}

	protected boolean noErrors(_Column ... columns) {
		for (_Column c : columns) {
			if (hasErrors(c)) return false;
		}
		return true;
	}

	protected void checkRange(_Column c, double minValue, double maxValue) {
		checkRange(c.getFullname(), c.getValue(), minValue, maxValue);
	}

	protected void checkRange(String fieldName, String value, double minValue, double maxValue) {
		if (hasErrors(fieldName)) return;
		double doubleValue = Double.valueOf(value.replace(",", "."));
		if (doubleValue < minValue || doubleValue > maxValue) {
			error(fieldName, _Validator.INVALID_VALUE);
		}
	}

	protected void checkNoReferencesToThisRowAndRowCanBeDeleted(_Table table, _DAO<T> dao) throws SQLException {
		for (_Column c : table) {
			for (_ReferenceKey ref : c.getReferencingColumns()) {
				String valueToBeDeleted = dao.returnValueByRowid(c.getDatabaseTablename(), c.getName(), table.getRowidValue());
				if (dao.checkReferenceValueExists(valueToBeDeleted, ref)) error(c.getDatabaseTablename(), CAN_NOT_DELETE_IS_BEING_USED);
			}
		}
	}

	protected void checkReferenceValuesOfNonNullColumns(T params, _DAO<T> dao) {
		for (_Table t : params) {
			for (_Column c : t) {
				if (!isNull(c) && !hasErrors(c)) {
					checkReferenceValueExists(c, dao);
				}
			}
		}
	}

	protected void checkReferenceValuesExists(T params, _DAO<T> dao) {
		for (_Table t : params) {
			checkReferenceValuesExists(t, dao);
		}
	}

	protected void checkReferenceValuesExists(_Table table, _DAO<T> dao) {
		for (_Column c : table) {
			if (noErrors(c)) {
				checkReferenceValueExists(c, dao);
			}
		}
	}

	protected void checkReferenceValueExists(_Column c, _DAO<T> dao) {
		if (c.getReferenceColumn() != null) {
			try {
				if (!dao.checkReferenceValueExists(c.getValue(), c.getReferenceColumn())) {
					error(c, INVALID_REFERENCE_VALUE);
				}
			} catch (SQLException e) {
				error(c, _Validator.INVALID_VALUE);
				e.printStackTrace();
			}
		}
	}

	protected void checkImmutableKeyCombinationDoesntAlreadyExist(_Table table, _DAO<T> dao) throws SQLException {
		if (dao.checkRowExists(table)) {
			for (_Column c : table) {
				if (c.getFieldType() == _Column.IMMUTABLE_PART_OF_A_KEY) {
					error(c.getFullname(), ALREADY_IN_USE);
				}
			}
		}
	}

	protected void checkUniqueNumericIdNotGiven(_Table table) {
		if (table.hasUniqueNumericIdColumn()) {
			if (!isNull(table.getUniqueNumericIdColumn().getValue())) {
				error(table.getFullname(), _Validator.SYSTEM_ERROR);
			}
		}
	}

	protected void checkRowidIsNull(_Table table) {
		if (!isNull(table.getRowidValue())) {
			error(table.getFullname(), _Validator.SYSTEM_ERROR);
		}
	}

	protected void checkRowIdExists(_Table table, _DAO<T> dao) throws SQLException {
		if (!dao.checkRowExists(table)) {
			error(table.get(Const.ROWID).getFullname(), _Validator.NO_LONGER_EXISTS);
		}
	}

	protected void checkRowIdMatchesKeys(_Table table, _DAO<T> dao) throws SQLException {
		if (!dao.checkRowIDMatchesKeys(table)) {
			error(table.getFullname(), _Validator.NO_LONGER_EXISTS);
		}
	}

	protected void checkRowIdIsNotNull(_Table table) {
		if (isNull(table.getRowidValue())) {
			error(table.getFullname(), SYSTEM_ERROR);
		}
	}

	protected boolean checkIsNull(_Column c) {
		return checkIsNull(c.getFullname(), c.getValue());
	}

	protected void checkIsNull(_Column ... columns) {
		for (_Column c : columns) {
			checkIsNull(c);
		}
	}

	protected void checkIsNullOrZero(_Column ... columns) {
		for (_Column c : columns) {
			if (hasErrors(c)) continue;
			if (!isNullOrZero(c)) {
				error(c, MUST_BE_NULL_OR_ZERO);
			}
		}
	}

	protected void checkIsNotNullOrZero(_Column c) {
		if (hasErrors(c)) return;
		if (isNullOrZero(c)) {
			error(c, MUST_NOT_BE_NULL_OR_ZERO);
		}
	}

	protected void checkIsPositiveNumber(_Column c) {
		if (notNullNoErrors(c)) {
			if (!(c.getDoubleValue() > 0.0)) {
				error(c, MUST_BE_POSITIVE_NUMBER);
			}
		}
	}

	protected void checkIsNotNegativeNumber(_Column c) {
		if (notNullNoErrors(c)) {
			if (c.getDoubleValue() < 0.0) {
				error(c, MUST_NOT_BE_NEGATIVE_NUMBER);
			}
		}
	}

	protected void checkIsPositiveOrZero(_Column c) {
		if (notNullNoErrors(c)) {
			if (c.getDoubleValue() < 0.0) {
				error(c, MUST_BE_POSITIVE_NUMBER);
			}
		}
	}

	private final String ZERO = "0";

	protected boolean isNullOrZero(_Column c) {
		return isNull(c) || c.getValue().equals(ZERO);
	}

	protected boolean checkIsNull(String fieldName, String value) {
		if (isNull(value)) return true;
		error(fieldName, MUST_BE_NULL);
		return false;
	}

	protected void checkHour(_Column c) {
		checkHour(c.getFullname(), c.getValue());
	}

	protected void checkHour(String fieldName, String value) {
		if (isNull(value)) return;
		if (!isInteger(value)) {
			error(fieldName, MUST_BE_INTEGER);
			return;
		}
		int hour = Integer.valueOf(value);
		if (hour > 23 || hour < 0) {
			error(fieldName, INVALID_VALUE);
		}
	}

	protected void checkCoordinateRanges(_Column leveys, _Column pituus, double leveys_min, double leveys_max, double pituus_min, double pituus_max) {
		checkNotNull(leveys);
		checkNotNull(pituus);
		if (noErrors(leveys)) {
			checkRange(leveys, leveys_min, leveys_max);
		}
		if (noErrors(pituus)) {
			checkRange(pituus, pituus_min, pituus_max);
		}
	}

	protected boolean valueIsOneOf(String value, Collection<String> acceptedValues) {
		return acceptedValues.contains(value);
	}

	protected boolean notNullNoErrors(_Column c) {
		return !isNull(c) && noErrors(c);
	}

	protected boolean isNullOrHasErrors(_Column c) {
		return !notNullNoErrors(c);
	}

	/**
	 * Checks either c1 and c2 are given or neither is given
	 * @param c1
	 * @param c2
	 * @param errormessage
	 * @return true if all ok, false if errors
	 */
	protected boolean checkPair(_Column c1, _Column c2, String errormessage) {
		if (isNull(c1) && isNull(c2)) {
			return true;
		}
		if (!isNull(c1) && !isNull(c2)) {
			return true;
		}
		error(c1, errormessage);
		error(c2, errormessage);
		return false;
	}

	protected boolean checkValueAndAccuracyPair(_Column value, _Column accuracy) {
		if (isNull(value)) {
			return checkIsNull(accuracy);
		}
		return true;
	}

	protected boolean checkValueAndAccuracyPair(_Column value, _Column accuracy, boolean accuracyRequired) {
		if (isNull(value)) {
			return checkIsNull(accuracy);
		}
		if (accuracyRequired) {
			if (!isNull(value)) {
				return checkNotNull(accuracy);
			}
		}
		return true;
	}

	protected void checkRange(_Column c, NumericRange<?> range) {
		if (c == null || range == null) return;
		Double maxValue = null;
		Double minValue = null;
		if (range.getMaxValue() instanceof Double) {
			maxValue = (Double) range.getMaxValue();
			minValue = (Double) range.getMinValue();
		} else if (range.getMaxValue() instanceof Integer){
			maxValue = Double.valueOf(range.getMaxValue().toString());
			minValue = Double.valueOf(range.getMinValue().toString());
		} else {
			throw new UnsupportedOperationException("Not supported for this type of range");
		}
		Double value = c.getDoubleValue();
		if (value > maxValue) {
			error(c, TOO_LARGE);
		} else if (value < minValue) {
			error(c, _Validator.TOO_SMALL);
		}
	}
	
	protected void warningIfNull(_Column column) {
		if (isNull(column)) {
			warning(column, ARE_YOU_SURE_VALUE_IS_NULL);
		}
	}

	protected void checkNotNull(_Column column, String errorMessage) {
		if (isNull(column)) {
			error(column, errorMessage);
		}
	}

	protected void checkIsNull(_Column column, String errorMessage) {
		if (!isNull(column)) {
			error(column, errorMessage);
		}
	}

	protected void checkIsNull(String errorMessage, _Column ... columns) {
		for (_Column c : columns) {
			checkIsNull(c, errorMessage);
		}
	}

	protected void checkIsPositiveIfHasValue(_Column ... columns) {
		for (_Column c : columns) {
			if (c.hasValue()) {
				checkIsPositiveNumber(c);
			}
		}
	}

	protected void checkIsNotNegativeIfHasValue(_Column ... columns) {
		for (_Column c : columns) {
			if (c.hasValue()) {
				checkIsNotNegativeNumber(c);
			}
		}
	}

	protected void checkIsPositiveOrZeroIfHasValue(_Column ... columns) {
		for (_Column c : columns) {
			if (c.hasValue()) {
				checkIsPositiveOrZero(c);
			}
		}
	}

	protected void checkIsNullOrZero(_Column c, String errorMessage) {
		if (noErrors(c)) {
			if (!isNullOrZero(c)) {
				error(c, errorMessage);
			}
		}
	}

	protected void validateEmail(_Column email) {
		if (email.hasValue()) {
			if (!email.getValue().contains("@") || email.getValue().length() < 5 || !email.getValue().contains(".") || email.getValue().contains(" ")) {
				error(email, _Validator.INVALID_EMAIL);
			}
		}
	}
	
}
