package fi.hy.pese.merikotka.test;

import java.sql.Date;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import fi.hy.pese.framework.main.app.Request;
import fi.hy.pese.framework.main.app.RequestImpleData;
import fi.hy.pese.framework.main.app.functionality.SearchFunctionality;
import fi.hy.pese.framework.main.db._DAO;
import fi.hy.pese.framework.main.db._ResultSet;
import fi.hy.pese.framework.main.db._ResultsHandler;
import fi.hy.pese.framework.main.general.consts.Const;
import fi.hy.pese.framework.main.general.data.DatabaseColumnStructure;
import fi.hy.pese.framework.main.general.data.PesaDomainModelDatabaseStructure;
import fi.hy.pese.framework.main.general.data.PesaDomainModelRow;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.main.general.data._Table;
import fi.hy.pese.framework.test.db.DAOStub;
import fi.hy.pese.merikotka.main.app.MerikotkaConst;
import fi.hy.pese.merikotka.main.app.MerikotkaDatabaseStructureCreator;
import fi.hy.pese.merikotka.main.functionality.MerikotkaDataToMapForPDFPrintingGenerator;
import fi.hy.pese.merikotka.main.functionality.MerikotkaSearchFunctionality;
import fi.luomus.commons.containers.Selection;
import fi.luomus.commons.containers.SelectionImple;
import fi.luomus.commons.containers.SelectionOptionImple;
import fi.luomus.commons.tipuapi.TipuAPIClient;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class MerikotkaDataToMapForPDFPrintingTest {
	
	public static Test suite() {
		return new TestSuite(MerikotkaDataToMapForPDFPrintingTest.class.getDeclaredClasses());
	}
	
	public static class TarkastusdataMapGeneration_for_pdf_printing__ComparingPesimistulos extends TestCase {
		
		public void test__comparing_pesimistulos() {
			assertTrue("U better than empty string", MerikotkaConst.firstPesimistulosBetterThanSecond("U", ""));
			assertTrue("R better than A", MerikotkaConst.firstPesimistulosBetterThanSecond("R", "A"));
			assertTrue("L better than R", MerikotkaConst.firstPesimistulosBetterThanSecond("L", "R"));
			assertFalse("K not better than M", MerikotkaConst.firstPesimistulosBetterThanSecond("K", "M"));
			assertFalse("empty string not better than U", MerikotkaConst.firstPesimistulosBetterThanSecond("", "U"));
			assertTrue("empty string better than empty string", MerikotkaConst.firstPesimistulosBetterThanSecond("", ""));
			
			try {
				MerikotkaConst.firstPesimistulosBetterThanSecond("FOO", "K");
				fail("Should throw exception");
			} catch (IllegalStateException e) {
			}
			
			try {
				MerikotkaConst.firstPesimistulosBetterThanSecond("K", "FOO");
				fail("Should throw exception");
			} catch (IllegalStateException e) {
			}
		}
	}
	
	public static class TarkastusdataMapGeneration_for_pdf_printing extends TestCase {
		
		public class FakeDAO extends DAOStub<_PesaDomainModelRow> {
			@Override
			public void executeSearch(_Table parameters, _ResultsHandler resultHandler, String ... orderBy) throws SQLException {
				assertEquals("tark_vuosi DESC", orderBy[0]);
				if (!parameters.get("pesimistulos").getValue().equals("A|K|J|M|P|R|L")) { throw new SQLException("This mock is only for search where pesimistulos = A|K|J|M|P|R|L"); }
				resultHandler.process(new _ResultSet() {
					boolean	first	= true;
					
					@Override
					public boolean next() throws SQLException {
						if (first) {
							first = false;
							return true;
						}
						return false;
					}
					
					@Override
					public String getString(int i) throws SQLException {
						int columnIndex = 1;
						for (DatabaseColumnStructure c : structure.getTarkastus().getColumns()) {
							if (columnIndex == i) {
								if (c.getName().equals("tarkastaja1_id")) return "543";
								if (c.getName().equals("pesimistulos")) return "R";
								if (c.getName().equals("reng_poik_lkm")) return "2";
								if (c.getName().equals("pesa_kunto")) return "K";
								if (c.getName().equals("pesa_kunto_aika")) return "J";
								if (c.getName().equals("pesa_kunto_ihmisen_vaik")) return "J";
								return c.getName();
							}
							columnIndex++;
						}
						throw new SQLException("end reached");
					}
					
					@Override
					public String getString(String columnLabel) throws SQLException {
						return null;
					}
					
					@Override
					public Date getDate(String columnLabel) throws SQLException {
						return null;
					}
					
					@Override
					public Date getDate(int i) throws SQLException {
						return new Date(1234567890L * 1000);
					}
				});
			}
			
			@Override
			public void executeSearch(String sql, _ResultsHandler resultHandler, String... values) throws SQLException {
				if (sql.trim().startsWith("SELECT DISTINCT pesa_id FROM pesatarkastus WHERE pesa_kunto IN")) { return; }
				resultHandler.process(new _ResultSet() {
					int	row	= 0;
					
					@Override
					public boolean next() throws SQLException {
						if (++row <= 3) return true;
						return false;
					}
					
					// select vuosi.pesa_id, pesanimi, pesimistulos, elavia_lkm, lentopoik_lkm, reng_poik_lkm
					@Override
					public String getString(int i) throws SQLException {
						if (row == 1) {
							if (i == 1) return "1";
							if (i == 2) return "Pesanimi";
							if (i == 3) return "R";
							if (i == 4) return "2";
							if (i == 5) return "";
							if (i == 6) return "2";
							if (i == 7) return "2009";
						}
						if (row == 2) {
							if (i == 1) return "2";
							if (i == 2) return "Reviirin toinen pesä";
							if (i == 3) return "A";
							if (i == 4) return "";
							if (i == 5) return "";
							if (i == 6) return "";
							if (i == 7) return "2009";
						}
						if (row == 3) {
							if (i == 1) return "7";
							if (i == 2) return "Reviirin kolmas pesä";
							if (i == 3) return "U";
							if (i == 4) return "";
							if (i == 5) return "";
							if (i == 6) return "";
							if (i == 7) return "2009";
						}
						throw new SQLException("end reached");
					}
					
					@Override
					public String getString(String columnLabel) throws SQLException {
						return null;
					}
					
					@Override
					public Date getDate(String columnLabel) throws SQLException {
						return null;
					}
					
					@Override
					public Date getDate(int i) throws SQLException {
						return null;
					}
				});
			}
			
			private String	pesaid	= "";
			
			@Override
			public void executeSearch(_PesaDomainModelRow parameters, _ResultsHandler resultHandler, String... additional) throws SQLException {
				pesaid = parameters.getPesa().getUniqueNumericIdColumn().getValue();
				resultHandler.process(new _ResultSet() {
					private boolean	first	= true;
					
					@Override
					public boolean next() throws SQLException {
						if (first) {
							first = false;
							return true;
						}
						return false;
					}
					
					@Override
					public String getString(int i) throws SQLException {
						int columnIndex = 1;
						for (DatabaseColumnStructure c : structure.getTarkastus().getColumns()) {
							if (columnIndex == i) {
								if (c.getName().equals("tarkastaja1_id")) return "543";
								if (c.getName().equals("pesimistulos")) return "R";
								if (c.getName().equals("reng_poik_lkm")) return "2";
								if (c.getName().equals("pesa_kunto")) return "K";
								return c.getName();
							}
							columnIndex++;
						}
						for (DatabaseColumnStructure c : structure.getPoikanen().getColumns()) {
							if (columnIndex == i) { return c.getName(); }
							columnIndex++;
						}
						for (DatabaseColumnStructure c : structure.getAikuinen().getColumns()) {
							if (columnIndex == i) { return c.getName(); }
							columnIndex++;
						}
						for (DatabaseColumnStructure c : structure.getPesa().getColumns()) {
							if (columnIndex == i) {
								if (c.getName().equals("pesa_id")) return "1";
								if (c.getName().equals("pesanimi")) return "Pesanimi";
								if (c.getName().equals("koord_tyyppi")) return "E";
								if (c.getName().equals("eur_leveys")) return "7890";
								if (c.getName().equals("eur_pituus")) return "3456";
								if (c.getName().equals("et_meri")) return "500";
								if (c.getName().equals("et_jarvi")) return "9999";
								if (c.getName().equals("rak_vuosi_teko")) {
									if (pesaid.equals("1"))
										return "";
									return "2005";
								}
								return c.getName();
							}
							columnIndex++;
						}
						for (DatabaseColumnStructure c : structure.getOlosuhde().getColumns()) {
							if (columnIndex == i) { return c.getName(); }
							columnIndex++;
						}
						for (DatabaseColumnStructure c : structure.getVuosi().getColumns()) {
							if (columnIndex == i) {
								if (c.getName().equals("reviirin_kunta")) {
									return "HELSIN";
								}
								return c.getName(); }
							columnIndex++;
						}
						for (DatabaseColumnStructure c : structure.getReviiri().getColumns()) {
							if (columnIndex == i) {
								if (c.getName().equals("reviiri_id")) return "78";
								return c.getName();
							}
							columnIndex++;
						}
						throw new SQLException("end reached");
					}
					
					@Override
					public String getString(String columnLabel) throws SQLException {
						return null;
					}
					
					@Override
					public Date getDate(String columnLabel) throws SQLException {
						return null;
					}
					
					@Override
					public Date getDate(int i) throws SQLException {
						return new Date(1234567890L * 1000);
					}
				});
			}
			
			@Override
			public _PesaDomainModelRow newRow() {
				return new PesaDomainModelRow(structure);
			}
		}
		
		private final PesaDomainModelDatabaseStructure						structure	= MerikotkaDatabaseStructureCreator.hardCoded();
		private Map<String, String>							map;
		private MerikotkaDataToMapForPDFPrintingGenerator	generator;
		
		private Map<String, Selection> selections() {
			Map<String, Selection> selections = new HashMap<>();
			SelectionImple tarkastajat = new SelectionImple(TipuAPIClient.RINGERS);
			tarkastajat.addOption(new SelectionOptionImple("543", "ANSSI ANSSILA (543)"));
			selections.put(TipuAPIClient.RINGERS, tarkastajat);
			
			SelectionImple pesimistulokset = new SelectionImple(structure.getTarkastus().get("pesimistulos").getAssosiatedSelection());
			pesimistulokset.addOption(new SelectionOptionImple("R", "Rengastettuja poikasia"));
			selections.put(structure.getTarkastus().get("pesimistulos").getAssosiatedSelection(), pesimistulokset);
			
			SelectionImple pesankunnot = new SelectionImple(structure.getTarkastus().get("pesa_kunto").getAssosiatedSelection());
			pesankunnot.addOption(new SelectionOptionImple("K", "Kunnolla kuusessa kaiketi kallellaan"));
			selections.put(structure.getTarkastus().get("pesa_kunto").getAssosiatedSelection(), pesankunnot);
			
			return selections;
		}
		
		private Map<String, String> uiTexts() {
			Map<String, String> uiTexts = new HashMap<>();
			uiTexts.put(MerikotkaConst.REVIIRILISTA_EI_VAIHTOPESIA_TEXT, "Ei vaihtopesia");
			uiTexts.put(MerikotkaConst.REVIIRILISTA_KYLLA_EI_TEXT, "Kyllä / Ei");
			return uiTexts;
		}
		
		@Override
		protected void setUp() throws Exception {
			map = new HashMap<>();
			_DAO<_PesaDomainModelRow> dao = new FakeDAO();
			SearchFunctionality searchFunctionality = new MerikotkaSearchFunctionality(new Request<>(new RequestImpleData<_PesaDomainModelRow>().setDao(dao)));
			generator = new MerikotkaDataToMapForPDFPrintingGenerator(dao, selections(), searchFunctionality, uiTexts());
		}
		
		public void test___adds_tark_vuosi() throws Exception {
			map = generator.tarkastusDataFor("1", "2010");
			assertEquals("2010", map.get("tark_vuosi_1"));
			assertEquals("2010", map.get("tark_vuosi_2"));
			assertEquals("2010", map.get("tark_vuosi_3"));
			assertEquals("2010", map.get("tark_vuosi_4"));
			assertEquals("2010", map.get("tark_vuosi_5"));
			assertEquals("2010", map.get("tark_vuosi_6"));
		}
		
		public void test___adds_filename() throws Exception {
			map = generator.tarkastusDataFor("1", "2010");
			assertEquals("REV_78_PESA_1_2010", map.get(Const.FILENAME));
		}
		
		public void test__adds_pesanimi_etc() throws Exception {
			map = generator.tarkastusDataFor("1", "2010");
			assertEquals("Pesanimi", map.get("pesa.pesanimi"));
			assertEquals("1", map.get("pesa.pesa_id"));
			assertEquals("HELSIN", map.get("vuosi.reviirin_kunta"));
		}
		
		public void test__adds_specific_tarkastusdata_but_not_all() throws Exception {
			map = generator.tarkastusDataFor("1", "2010");
			assertEquals("as_lkm_500", map.get("tarkastus.as_lkm_500"));
			assertEquals("", map.get("tarkastus.pesimistulos"));
		}
		
		public void test__adds_specific_tarkastusdata_but_no_aikuinen_or_poikanen() {
			// TODO This is implemeted but not tested
		}
		
		public void test__adds_prev_tarkastus_info() throws Exception {
			map = generator.tarkastusDataFor("1", "2010");
			assertEquals("14.02.2009", map.get(MerikotkaConst.PREV_TARKASTUS_TARK_PVM));
			assertEquals("ANSSI ANSSILA (543)", map.get(MerikotkaConst.PREV_TARKASTUS_TARKASTAJA));
			assertEquals("R (2) - Rengastettuja poikasia", map.get(MerikotkaConst.PREV_TARKASTUS_PESIMISTULOS));
			assertEquals("K - J - J - Kunnolla kuusessa kaiketi kallellaan", map.get(MerikotkaConst.PREV_TARKASTUS_PESA_KUNTO));
		}
		
		public void test___adds_prev_reviiri_info_() throws Exception {
			map = generator.tarkastusDataFor("1", "2010");
			assertEquals("2: Reviirin toinen pesä\n" + "7: Reviirin kolmas pesä", map.get(MerikotkaConst.REVIIRILISTA));
			assertEquals("Kyllä / Ei\n" + "Kyllä / Ei", map.get(MerikotkaConst.REVIIRILISTA_K_E));
			assertEquals("2009", map.get(MerikotkaConst.PREV_PARAS_TARKASTUS_VUOSI));
			assertEquals("R (2) - Rengastettuja poikasia", map.get(MerikotkaConst.PREV_PARAS_TARKASTUS_PESIMISTULOS));
			assertEquals("1", map.get(MerikotkaConst.PREV_PARAS_TARKASTUS_PESA_ID));
		}
		
		public void test___selects_coorinate_type_checkbox___adds_coordinates() throws Exception {
			map = generator.tarkastusDataFor("1", "2010");
			assertEquals(null, map.get(MerikotkaConst.PESA_KOORD_TYYPPI_YHT));
			assertEquals("Yes", map.get(MerikotkaConst.PESA_KOORD_TYYPPI_EUR));
			assertEquals(null, map.get(MerikotkaConst.PESA_KOORD_TYYPPI_AST));
			
			assertEquals("7890", map.get(MerikotkaConst.PESA_KOORD_LEVEYS));
			assertEquals("3456", map.get(MerikotkaConst.PESA_KOORD_PITUUS));
		}
		
		public void test___adds__luonnonpesa_or_tekopesa() throws Exception {
			map = generator.tarkastusDataFor("1", "2010");
			assertEquals("K", map.get(MerikotkaConst.PESA_LUONNONPESA_K_E));
			assertEquals("E", map.get(MerikotkaConst.PESA_TEKOPESA_K_E));
		}
		
		public void test___adds__luonnonpesa_or_tekopesa2() throws Exception {
			map = generator.tarkastusDataFor("1000", "2010");
			assertEquals("E", map.get(MerikotkaConst.PESA_LUONNONPESA_K_E));
			assertEquals("K", map.get(MerikotkaConst.PESA_TEKOPESA_K_E));
		}
		
		public void test__adds__pesanimi_for_the_second_page() throws Exception {
			map = generator.tarkastusDataFor("1", "2010");
			assertEquals("Pesanimi (1)", map.get(MerikotkaConst.PESA_PESANIMI_2));
		}
		
		public void test__replaces_9999_with_gt1000() throws Exception {
			map = generator.tarkastusDataFor("1", "2010");
			assertEquals("500", map.get("pesa.et_meri"));
			assertEquals(">1000", map.get("pesa.et_jarvi"));
		}
		
	}
}
