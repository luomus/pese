package fi.hy.pese.saaksi.main.functionality.reports;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fi.hy.pese.framework.main.app._Request;
import fi.hy.pese.framework.main.general.ReportWriter;
import fi.hy.pese.framework.main.general.data._PesaDomainModelRow;
import fi.hy.pese.framework.main.general.data._Row;
import fi.luomus.commons.pdf.PDFWriter;
import fi.luomus.commons.tipuapi.TipuAPIClient;
import fi.luomus.commons.tipuapi.TipuApiResource;
import fi.luomus.commons.xml.Document.Node;

/**
 * Lists all people who are marked as "seur_tarkastaja" for nests that match the search criteria and prints out a cover sheet
 */
public class SaaksiReport_kansilehdet extends ReportWriter<_PesaDomainModelRow> {

	public SaaksiReport_kansilehdet(_Request<_PesaDomainModelRow> request, String filenamePrefix) {
		super(request, filenamePrefix);
	}

	@Override
	public boolean produceSpecific() {
		Set<Integer> havainnoijat = new HashSet<>();

		for (_Row row : request.data().results()) {
			if (row.getColumn("pesa.seur_rengastaja").hasValue()) {
				havainnoijat.add(row.getColumn("pesa.seur_rengastaja").getIntegerValue());
			}
		}

		List<Integer> sortedHavainnoijat = new LinkedList<>();
		sortedHavainnoijat.addAll(havainnoijat);
		Collections.sort(sortedHavainnoijat);

		PDFWriter pdfWriter = request.pdfWriter(request.config().reportFolder());

		try {
			for (Integer havainnoija : sortedHavainnoijat) {
				TipuApiResource ringers = request.getTipuApiResource(TipuAPIClient.RINGERS);
				if (!ringers.containsUnitById(Integer.toString(havainnoija))) {
					request.data().addToNoticeTexts("No info found for " + havainnoija);
					continue;
				}
				Node ringer = ringers.getById(Integer.toString(havainnoija));
				Node address = ringer.getNode("address");
				StringBuilder label = new StringBuilder();
				label.append(ringer.getNode("firstname").getContents()).append(" ");
				label.append(ringer.getNode("lastname").getContents()).append("\n");
				label.append(address.getNode("street").getContents()).append("\n");
				label.append(address.getNode("postcode").getContents()).append(" ");
				label.append(address.getNode("city").getContents());
				Map<String, String> data = new HashMap<>();
				data.put("kansilehti", label.toString());
				pdfWriter.writePdf("kansilehti", data, havainnoija.toString());
			}
		} catch (Exception e) {
			request.data().addToNoticeTexts(e.toString() + " " + e.getStackTrace()[0]);
		}

		pdfWriter.createCollection("kansilehdet");
		request.data().producedFiles().addAll(pdfWriter.producedFiles());
		return false; // do not create .txt file..
	}

}
